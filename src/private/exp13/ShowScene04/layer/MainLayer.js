exp13.ShowMainLayer04 = cc.Layer.extend({
	lead:null,
	real:null,
	ctor:function () {
		this._super();
		this.init();
		return true;
	},
	init:function(){//
		var sv = new pub.ScrollView(this);
		sv.setOffestPos(cc.p(152,45 + 15));

		this.addlayout1(sv);	
		this.addlayout2(sv);	
	},
	addlayout1:function(parent){
		var layout = new ccui.Layout();// 第一页
		layout.setContentSize(cc.size(996, 598));// 设置layout的大小
		parent.addLayer(layout);
		
		var text1 = $.format("蒸腾作用、光合作用、呼吸作用有什么区别：", 980,30);
		var label1 = new cc.LabelTTF(text1,gg.fontName,gg.fontSize4);
		label1.setColor(cc.color(0, 0, 0, 250));
		label1.setPosition(460,gg.height-label1.height/2 - 130);
		this.addChild(label1);	
		
		var text = $.format("蒸腾作用：" +
				"\n（1）含义：水分以气体的形式从植物体内散失到体外的过程。" +
				"\n（2）蒸腾作用的意义：①是根吸水的动力 ②能促进根从土壤中吸收水分 ③可降低叶面的温度 。" , 980, 30);
		this.addText(layout, text, cc.p(0,420));
		
		text = $.format("光合作用：" +
				"\n（1）含义：绿色植物通过叶绿体利用光能,把二氧化碳和水转变成贮存着能量的有机物（主要是淀粉）," +
				"并且释放出氧气的过程叫光合作用。"
				, 980, 30);
		this.addText(layout, text, cc.p(0,250));
		
		text = $.format("光能" , 980, 30);
		this.addText(layout, text, cc.p(540,120), true).setColor(cc.color(255, 0, 0));
		
		text = $.format("（2）光合作用的公式：\n　　　　　　　　水　＋　二氧化碳　 →　葡萄糖　＋　氧气 " , 980, 30);
		this.addText(layout, text, cc.p(0,100));
		
		text = $.format("叶绿体" , 980, 30);
		this.addText(layout, text, cc.p(540,50), true).setColor(cc.color(0, 255, 0));
	},
	addText:function(p, text, pos, defaultAp){
		var label = new cc.LabelTTF(text, gg.fontName, gg.fontSize4);
		label.setPosition(pos);
		if(!defaultAp){
			label.setAnchorPoint(0, 0.5);
		}
		label.setColor(cc.color(0, 0, 0, 250));
		p.addChild(label);
		return label;
	},
	addlayout2:function(parent){
		var layout2 = new ccui.Layout();// 第一页
		layout2.setContentSize(cc.size(996, 598));// 设置layout的大小
		parent.addLayer(layout2);

		var text = $.format("呼吸作用：" +
				"\n（1）含义：生物体利用氧气将体内的有机物氧化分解生成水和二氧化碳,并且释放出能量的过程叫呼吸作用。" +
				"\n（2）呼吸作用的公式：有机物（储存能量）+氧气 → 二氧化碳+水+能量 线粒体： 光合作用与呼吸作用的区别:" +
				"①场所:叶绿体活细胞②条件（与光的关系）:在光下才能进行,有光无光都能进行" +
				"③气体变化：吸收二氧化碳放出氧气，吸入氧气呼出二氧化碳④物质变化：制造有机物，分解有机物" +
				"⑤能量变化：贮存能量，释放能量。" +
				"\n联系:呼吸作用所分解的有机物,正是光合作用的产物；呼吸作用释放的能量,正是光合作用储藏在有机物中的能量。" , 980, 30);
		this.label4 = new cc.LabelTTF(text,gg.fontName,gg.fontSize4);
		this.label4.setPosition(500,300);
		this.label4.setColor(cc.color(0, 0, 0, 250));
		layout2.addChild(this.label4);	
	},
	callback:function(p){		
		switch(p.getTag()){
		case TAG_MENU4:
			gg.runNext(p.getTag());
			break;
		}
	}

});
