exp13.ShowMainLayer01 = cc.Layer.extend({
	lead:null,
	real:null,
	ctor:function () {
		this._super();
		this.init();
		return true;
	},
	init:function(){//
		this.sv = new pub.ScrollView(this);
		this.sv.setOffestPos(cc.p(152,45 + 15));
		
		this.addlayout1();
		this.addlayout2();	
	},
	addlayout1:function(){
		var layout = new ccui.Layout();// 第一页
		layout.setContentSize(cc.size(996, 598));// 设置layout的大小
		this.sv.addLayer(layout);

		var text = $.format("各式各样的叶：" +
				"\n\n\t\t\t\t自然界中，不同植物的叶的大小和形状差异很大。叶的形态、结构和功能特征与植物生长的环境密切相关。虽然叶的形态是多种多样的，它的基本结构却是相似的。" , 980, 30);
		var label = new cc.LabelTTF(text,gg.fontName,gg.fontSize4);
		label.setPosition(490,layout.height-label.height/2);
		label.setColor(cc.color(0, 0, 0, 250));
		layout.addChild(label);
		this.addimage(layout, "#fengye.png", cc.p(185,165), "枫叶", cc.p(185,40));
		this.addimage(layout, "#putao.jpg", cc.p(500,165), "葡萄叶", cc.p(500,40));
		this.addimage(layout, "#yinxing.jpg", cc.p(800,165), "隐形叶", cc.p(800,40));
	},
	addlayout2:function(){
		var layout = new ccui.Layout();// 第一页
		layout.setContentSize(cc.size(996, 598));// 设置layout的大小
		this.sv.addLayer(layout);

		var text = $.format("　　实验（叶的结构示意图）：", 980,30);
		var label = new cc.LabelTTF(text,gg.fontName,gg.fontSize4);
		label.setAnchorPoint(0,0.5);
		label.setColor(cc.color(0, 0, 0, 250));
		label.setPosition(0,layout.height-label.height/2);		
		layout.addChild(label);	
		
		var leaf = this.addSprite(layout, "#leaf.png", cc.p(300, 250));
		var m1 = this.addSprite(layout, "#leaf2.png", cc.p(700, 250));
		var m2 = this.addSprite(layout, "#leaf3.png", cc.p(700, 250));
		var m3 = this.addSprite(layout, "#leaf4.png", cc.p(700, 250));
		var m4 = this.addSprite(layout, "#leaf5.png", cc.p(700, 250));
		var m5 = this.addSprite(layout, "#leaf6.png", cc.p(700, 250));
		this.objArr2 = [m1, m2, m3, m4, m5];

		var button1 = this.addButton(layout, TAG_BUTTON1, "上表皮",cc.p(60, 400));
		var button2 = this.addButton(layout, TAG_BUTTON2, "叶　肉",cc.p(60, 320));
		var button3 = this.addButton(layout, TAG_BUTTON3, "叶　脉",cc.p(60, 240));
		var button4 = this.addButton(layout, TAG_BUTTON4, "下表皮",cc.p(60, 160));
		var button5 = this.addButton(layout, TAG_BUTTON5, "气　孔",cc.p(60, 80));
	},
	addSprite:function(p, img, pos){
		var sprite = new cc.Sprite(img);
		if(pos){
			sprite.setPosition(pos);
		}
		p.addChild(sprite);
		return sprite;
	},
	addButton:function(parent,tag,str,pos){
		var s9 = new pub.S9Button(parent, str,this.callback,this);
		s9.setPosition(pos);
		s9.setTag(tag);
		return s9;
	},
	addimage:function(parent,str,pos,str2,pos2){
		var image = new cc.Sprite(str);
		parent.addChild(image);
		image.setPosition(pos);
		var label = new cc.LabelTTF(str2,gg.fontName,gg.fontSize);
		label.setColor(cc.color(0, 0, 0, 250));
		parent.addChild(label);
		label.setPosition(pos2);
	},
	callback:function(p){
		for(var i = 0; i < this.objArr2.length; i++){
			var obj = this.objArr2[i];
			obj.stopAllActions();
			obj.setOpacity(30);
		}
		var seq = cc.sequence(cc.fadeTo(1.2, 255));
		switch(p.getTag()){
		case TAG_BUTTON1:
			this.objArr2[0].runAction(seq);
			break;
		case TAG_BUTTON2:
			this.objArr2[1].runAction(seq);
			break;
		case TAG_BUTTON3:
			this.objArr2[2].runAction(seq);
			break;
		case TAG_BUTTON4:
			this.objArr2[3].runAction(seq);
			break;
		case TAG_BUTTON5:
			this.objArr2[4].runAction(seq);
			break;
		}
	}

});
