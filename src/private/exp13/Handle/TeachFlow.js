var exp13 = exp13||{};
exp13.startRunNext = function(tag){
	switch(tag){//具体哪个菜单执行什么操作，这里修改
	case TAG_MENU1:
		pub.ch.gotoShow(TAG_EXP_12, tag, g_resources_show12, null, new exp13.ShowScene01(tag));
		break;
	case TAG_MENU2:
		pub.ch.gotoShow(TAG_EXP_12, tag, g_resources_show12, null, new exp13.ShowScene02(tag));
		break;
	case TAG_MENU3:
		gg.teach_type = TAG_LEAD;
		gg.teach_flow = exp13.teach_flow12;
		pub.ch.gotoRun(TAG_EXP_12, tag, g_resources_run12, null, new exp13.RunScene01(tag));	
		break;
	case TAG_MENU4:
		pub.ch.gotoShow(TAG_EXP_12, tag, g_resources_show12, null, new exp13.ShowScene05(tag));
		break;
	case TAG_MENU5:
		pub.ch.gotoShow(TAG_EXP_12, tag, g_resources_show12, null, new exp13.ShowScene04(tag));
		break;
	case TAG_MENU6:
		pub.ch.gotoShow(TAG_EXP_12, tag, g_resources_show12, null, new exp13.ShowScene03(tag));
		break;
	case TAG_ABOUT:
		pub.ch.gotoAbout(TAG_EXP_12, g_resources_public_about, null, res_public_about.about_j);
		break;
	}
}

// 任务流
exp13.teach_flow12 = 
	[
	 {
		 tip:"选择铁架台",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG3,	   
		      ],
		      canClick:true		 
	 },
	 {
		 tip:"选择植物",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG5,	   
		      ],
		      canClick:true		 
	 },
	 {
		 tip:"选择气压计",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG7,	   
		      ],
		      canClick:true		 
	 },



	 {
		 tip:"点击确定，开始实验",
		 tag:[
		      TAG_LIB,
		      TAG_SURE
		      ]
	 },
	 {
		 tip:"观察各种环境下的实验现象",
		 tag:[
		      TAG_BASE,
		      TAG_BASE_INK4

		      ],action:ACTION_DO1
	 },


	 {
		 tip:"退出实验演示",
		 over:true

	 }
	 ]




