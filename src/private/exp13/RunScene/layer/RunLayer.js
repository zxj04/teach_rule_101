var RunLayer1901 = cc.Layer.extend({
	arr:null,
	scene:null,
	clock:null,
	ctor:function (parent) {
		this._super();
		this.scene = parent;
		this.scene.addChild(this, 10);
		gg.main = this;
		this.init();
	},
	init:function () {
		this.callNext = cc.callFunc(function(){
			gg.flow.next();
		}, this);
		this.callKill = cc.callFunc(function(p){
			var seq = cc.sequence(cc.fadeOut(0.5),cc.callFunc(function(){
				p.removeFromParent(true);	
			}, this));
			p.runAction(seq);
		}, this); 

		this.callNext.retain();
		this.callKill.retain();
		this.loadButton(true);	
		//时钟
		this.clock = new Clock(this);
		// 物品库
		this.lib = new Lib(this);

		this.lib.loadBg([{
			tag:TAG_LIB_NHCLBEAKER,
			//checkright:true,	//实验所需器材添加checkright标签		
		},
		{
			tag:TAG_LIB_BOTTLE,
		},
		{
			tag:TAG_LIB_MACHINE,
			checkright:true,
		},
		{
			tag:TAG_LIB_AMPULLA,
			//checkright:true,
		},
		{
			tag:TAG_LIB_PLANT,
			checkright:true,
		},
		{
			tag:TGA_LIB_GLASS,
		},
		{
			tag:TAG_LIB_PRESSURE,
			checkright:true,
		},
		{
			tag:TAG_LIB_BEAKER,
			//checkright:true,
		},
		{
			tag:TAG_LIB_NNA2CO3BEAKER,
			//checkright:true,
		},
		{
			tag:TAG_LIB_CONICAL,
		}
		]);



		this.base = new Base(this);
		this.base.setVisible(false);
		this.base.setPosition(370,550);
		
		this.shinei = new Shinei(this);
		this.shinei.setOpacity(0);

		this.feng = new Feng(this);
		this.feng.setOpacity(0);

		this.guangzhao = new Guangzhao(this);
		this.guangzhao.setOpacity(0);

		this.chaoshi = new Chaoshi(this);
		this.chaoshi.setOpacity(0);

		this.getChildByTag(TAG_BUTTON_RESULT).setVisible(false);
		this.getChildByTag(TAG_BUTTON_CHAOSHI).setVisible(false);
		this.getChildByTag(TAG_BUTTON_SHINEI).setVisible(false);
		this.getChildByTag(TAG_BUTTON_GUANGZHAO).setVisible(false);

		this.getChildByTag(TAG_BUTTON_FENG).setVisible(false);

		this.result = new Jieguo(this);
		this.result.setOpacity(0);
		//this.result.setPosition(3370,550);



	},
	Button1:function()
	{

	},
	checkVisible:function(next){
		//是否可见
		var checkVisible = [];
		var node1 = ll.run.base;
		var node2 = ll.run.shinei;
		var node3 = ll.run.guangzhao;
		var node4 = ll.run.chaoshi;
		var node5 = ll.run.feng;
		var node6=this.getChildByTag(TAG_BUTTON_RESULT);
		var node7=this.getChildByTag(TAG_BUTTON_CHAOSHI);
		var node8=this.getChildByTag(TAG_BUTTON_SHINEI);
		var node9=this.getChildByTag(TAG_BUTTON_GUANGZHAO);
		var node10=this.getChildByTag(TAG_BUTTON_FENG);
		var node11 = ll.run.result;
		checkVisible.push(node1,node2,node3,node4,node5,node6,node7,node8,node9,node10,node11);

		//checkVisible.push(node1);
		for(var i in checkVisible){	
			if(checkVisible[i] !== null){				
				checkVisible[i].setVisible(next);

			}			
		}
	},
	loadInLib:function(obj, pos, tarPos,delay){
		obj.setPosition(pos);
		if(delay == null){
			delay = 1;
		}
		var ber = $.bezier(pos, tarPos, delay);
		var seq = cc.sequence(ber, this.callNext);
		obj.runAction(seq);
	},
	kill:function(obj){
		var fade = cc.fadeTo(0.5,0);
		var func = cc.callfunc(function(){
			obj.removeFromParent(true);
		},this);
		var seq = cc.sequence(fade,func);
		obj.runAction(seq)
	},

	callback:function (p){
		var func = cc.callFunc(this.actionDone, this);
		var action=gg.flow.flow.action;
		switch(p.getTag()){

		}
	},
	actionDone:function(p){
		var func = cc.callFunc(this.actionDone, this);
		switch(p.getTag()){

		}
	},
	flowNext:function(){
		gg.flow.next();
	},
	onExit:function(){
		this._super();
		this.callNext.release();
		this.callKill.release();
	},
	//4个选择按钮
	loadButton:function(enable){
		//室内环境
		this.shinei = new Button(this,10,TAG_BUTTON_SHINEI,"#expuncheck.png",this.callback);
		this.shinei.setPosition(cc.p(1054 + this.shinei.width * 0.5, 535 + this.shinei.height * 0.5));
		this.loadName("室内环境", this.shinei);
		this.shinei.setEnable(true);
		//	this.shinei.setScale(0.9,0.3);
		//潮湿环境
		this.chaoshi = new Button(this,10,TAG_BUTTON_CHAOSHI,"#expuncheck.png",this.callback);
		this.chaoshi.setPosition(cc.p(1054 + this.shinei.width * 0.5, 430+ this.chaoshi.height * 0.5));
		this.loadName("潮湿环境", this.chaoshi);
		this.chaoshi.setEnable(true);
		//this.chaoshi.setScale(0.9,0.3);
		//光照环境
		this.guangzhao = new Button(this,10,TAG_BUTTON_GUANGZHAO,"#expuncheck.png",this.callback);
		this.guangzhao.setPosition(cc.p(1054 + this.shinei.width * 0.5, 325 + this.guangzhao.height * 0.5));
		this.loadName("光照环境", this.guangzhao);
		this.guangzhao.setEnable(true);
		//	this.guangzhao.setScale(0.9,0.3);
		//自然风环境
		this.feng = new Button(this,10,TAG_BUTTON_FENG,"#expuncheck.png",this.callback);
		this.feng.setPosition(cc.p(1054 + this.shinei.width * 0.5, 220 + this.feng.height * 0.5));		
		this.feng.setEnable(true);
		//this.feng.setScale(0.9,0.3);
		this.loadName("自然风环境 ", this.feng);

		//结果对比
		this.jieguo = new Button(this,10,TAG_BUTTON_RESULT,"#expuncheck.png",this.callback);
		this.jieguo.setPosition(cc.p(1054 + this.shinei.width * 0.5, 115 + this.feng.height * 0.5));		
		this.jieguo.setEnable(true);
		//this.jieguo.setScale(0.9,0.3);
		this.loadName("结果对比 ", this.jieguo);
	},
	//按钮中的文字
	loadName:function(name,pare){
		var str = new cc.LabelTTF(name,gg.fontName,28);
		pare.addChild(str, 2);
		str.setColor(cc.color(255,255,255));
		str.setPosition(cc.p(pare.width*0.5, pare.height*0.55));
	},

	//点击
	touch:function(){
		this.getChildByTag(TAG_BUTTON_SHINEI).setEnable(true);
		this.getChildByTag(TAG_BUTTON_FENG).setEnable(true);
		this.getChildByTag(TAG_BUTTON_GUANGZHAO).setEnable(true);
		this.getChildByTag(TAG_BUTTON_CHAOSHI).setEnable(true);
		this.getChildByTag(TAG_BUTTON_RESULT).setEnable(true);
	},
	callback:function (p){
		switch (p.getTag()) {
		case TAG_BUTTON_SHINEI:
			this.getChildByTag(TAG_BUTTON_SHINEI).setSpriteFrame("expuncheck1.png");
			this.getChildByTag(TAG_BUTTON_FENG).setSpriteFrame("expuncheck.png");
			this.getChildByTag(TAG_BUTTON_GUANGZHAO).setSpriteFrame("expuncheck.png");
			this.getChildByTag(TAG_BUTTON_CHAOSHI).setSpriteFrame("expuncheck.png");
			this.getChildByTag(TAG_BUTTON_RESULT).setSpriteFrame("expuncheck.png");
			//风
			ll.run.getChildByTag(TAG_FENG).getChildByTag(TAG_BASE_INK11).stopAllActions();
			ll.run.getChildByTag(TAG_FENG).getChildByTag(TAG_BASE_PRESSUREBIG2).stopAllActions();
			ll.run.getChildByTag(TAG_FENG).getChildByTag(TAG_BASE_INK7).stopAllActions();
			ll.run.getChildByTag(TAG_FENG).getChildByTag(TAG_BASE_INK8).stopAllActions();
		
			//光照
			ll.run.getChildByTag(TAG_GUANGZHAO).getChildByTag(TAG_BASE_INK11111).stopAllActions();
			ll.run.getChildByTag(TAG_GUANGZHAO).getChildByTag(TAG_BASE_PRESSUREBIG2).stopAllActions();
			ll.run.getChildByTag(TAG_GUANGZHAO).getChildByTag(TAG_BASE_INK7).stopAllActions();
			ll.run.getChildByTag(TAG_GUANGZHAO).getChildByTag(TAG_BASE_INK8).stopAllActions();
			
			//潮湿
			ll.run.getChildByTag(TAG_CHAOSHI).getChildByTag(TAG_BASE_INK111).stopAllActions();
			ll.run.getChildByTag(TAG_CHAOSHI).getChildByTag(TAG_BASE_PRESSUREBIG2).stopAllActions();
			ll.run.getChildByTag(TAG_CHAOSHI).getChildByTag(TAG_BASE_INK7).stopAllActions();
			ll.run.getChildByTag(TAG_CHAOSHI).getChildByTag(TAG_BASE_INK8).stopAllActions();
			
			
			
			
			
			ll.run.feng.setOpacity(0);
			
			ll.run.guangzhao.setOpacity(0);
			ll.run.base.setOpacity(0);

			//	ll.run.guangzhao.stopAllAction();
			ll.run.chaoshi.setOpacity(0);

			ll.run.result.setOpacity(0);
			ll.run.shinei.setOpacity(255);
			ll.run.shinei.action();
			//this.guangzhao.setVisible(false);
			ll.run.shinei.setPosition(370,550);
			this.getChildByTag(TAG_BUTTON_SHINEI).setEnable(false);
			this.getChildByTag(TAG_BUTTON_FENG).setEnable(true);
			this.getChildByTag(TAG_BUTTON_GUANGZHAO).setEnable(true);
			this.getChildByTag(TAG_BUTTON_CHAOSHI).setEnable(true);
			this.getChildByTag(TAG_BUTTON_RESULT).setEnable(true);
			break;
		case TAG_BUTTON_FENG:	
			this.getChildByTag(TAG_BUTTON_SHINEI).setSpriteFrame("expuncheck.png");
			this.getChildByTag(TAG_BUTTON_FENG).setSpriteFrame("expuncheck1.png");
			this.getChildByTag(TAG_BUTTON_GUANGZHAO).setSpriteFrame("expuncheck.png");
			this.getChildByTag(TAG_BUTTON_CHAOSHI).setSpriteFrame("expuncheck.png");
		    this.getChildByTag(TAG_BUTTON_RESULT).setSpriteFrame("expuncheck.png");
			
		    //室内
		    ll.run.getChildByTag(TAG_SHINEI).getChildByTag(TAG_BASE_INK1111).stopAllActions();
		    ll.run.getChildByTag(TAG_SHINEI).getChildByTag(TAG_BASE_PRESSUREBIG1).stopAllActions();
		    ll.run.getChildByTag(TAG_SHINEI).getChildByTag(TAG_BASE_INK6).stopAllActions();
		    ll.run.getChildByTag(TAG_SHINEI).getChildByTag(TAG_BASE_INK5).stopAllActions();
			
		    //光照
		    ll.run.getChildByTag(TAG_GUANGZHAO).getChildByTag(TAG_BASE_INK11111).stopAllActions();
		    ll.run.getChildByTag(TAG_GUANGZHAO).getChildByTag(TAG_BASE_PRESSUREBIG2).stopAllActions();
		    ll.run.getChildByTag(TAG_GUANGZHAO).getChildByTag(TAG_BASE_INK7).stopAllActions();
		    ll.run.getChildByTag(TAG_GUANGZHAO).getChildByTag(TAG_BASE_INK8).stopAllActions();

		    //潮湿
		    ll.run.getChildByTag(TAG_CHAOSHI).getChildByTag(TAG_BASE_INK111).stopAllActions();
		    ll.run.getChildByTag(TAG_CHAOSHI).getChildByTag(TAG_BASE_PRESSUREBIG2).stopAllActions();
		    ll.run.getChildByTag(TAG_CHAOSHI).getChildByTag(TAG_BASE_INK7).stopAllActions();
		    ll.run.getChildByTag(TAG_CHAOSHI).getChildByTag(TAG_BASE_INK8).stopAllActions();
		    ll.run.shinei.setOpacity(0);
			ll.run.base.setOpacity(0);
			
			ll.run.guangzhao.setOpacity(0);
			ll.run.chaoshi.setOpacity(0);
			ll.run.result.setOpacity(0);
			ll.run.feng.setOpacity(255);
			ll.run.feng.action();
			ll.run.feng.setPosition(370,550);
			this.getChildByTag(TAG_BUTTON_SHINEI).setEnable(true);
			this.getChildByTag(TAG_BUTTON_FENG).setEnable(false);
			this.getChildByTag(TAG_BUTTON_GUANGZHAO).setEnable(true);
			this.getChildByTag(TAG_BUTTON_CHAOSHI).setEnable(true);
			this.getChildByTag(TAG_BUTTON_RESULT).setEnable(true);
			break;
		case TAG_BUTTON_GUANGZHAO:	
			this.getChildByTag(TAG_BUTTON_SHINEI).setSpriteFrame("expuncheck.png");
			this.getChildByTag(TAG_BUTTON_FENG).setSpriteFrame("expuncheck.png");
			this.getChildByTag(TAG_BUTTON_GUANGZHAO).setSpriteFrame("expuncheck1.png");
			this.getChildByTag(TAG_BUTTON_CHAOSHI).setSpriteFrame("expuncheck.png");
			this.getChildByTag(TAG_BUTTON_RESULT).setSpriteFrame("expuncheck.png");

			
			//室内
			ll.run.getChildByTag(TAG_SHINEI).getChildByTag(TAG_BASE_INK1111).stopAllActions();
			ll.run.getChildByTag(TAG_SHINEI).getChildByTag(TAG_BASE_PRESSUREBIG1).stopAllActions();
			ll.run.getChildByTag(TAG_SHINEI).getChildByTag(TAG_BASE_INK6).stopAllActions();
			ll.run.getChildByTag(TAG_SHINEI).getChildByTag(TAG_BASE_INK5).stopAllActions();

			//风
			ll.run.getChildByTag(TAG_FENG).getChildByTag(TAG_BASE_INK11).stopAllActions();
			ll.run.getChildByTag(TAG_FENG).getChildByTag(TAG_BASE_PRESSUREBIG2).stopAllActions();
			ll.run.getChildByTag(TAG_FENG).getChildByTag(TAG_BASE_INK7).stopAllActions();
			ll.run.getChildByTag(TAG_FENG).getChildByTag(TAG_BASE_INK8).stopAllActions();

			//潮湿
			ll.run.getChildByTag(TAG_CHAOSHI).getChildByTag(TAG_BASE_INK111).stopAllActions();
			ll.run.getChildByTag(TAG_CHAOSHI).getChildByTag(TAG_BASE_PRESSUREBIG2).stopAllActions();
			ll.run.getChildByTag(TAG_CHAOSHI).getChildByTag(TAG_BASE_INK7).stopAllActions();
			ll.run.getChildByTag(TAG_CHAOSHI).getChildByTag(TAG_BASE_INK8).stopAllActions();
			ll.run.shinei.setOpacity(0);
			
			ll.run.base.setOpacity(0);
			ll.run.shinei.setOpacity(0);
			ll.run.getChildByTag(TAG_SHINEI).stopAllActions();
			ll.run.getChildByTag(TAG_FENG).stopAllActions();
			ll.run.getChildByTag(TAG_CHAOSHI).stopAllActions();
			ll.run.feng.setOpacity(0);
			ll.run.chaoshi.setOpacity(0);
			ll.run.result.setOpacity(0);
			ll.run.guangzhao.setOpacity(255);
			ll.run.guangzhao.action();
			ll.run.guangzhao.setPosition(370,550);
			this.getChildByTag(TAG_BUTTON_SHINEI).setEnable(true);
			this.getChildByTag(TAG_BUTTON_FENG).setEnable(true);
			this.getChildByTag(TAG_BUTTON_GUANGZHAO).setEnable(false);
			this.getChildByTag(TAG_BUTTON_CHAOSHI).setEnable(true);
			this.getChildByTag(TAG_BUTTON_RESULT).setEnable(true);
			break;
		case TAG_BUTTON_CHAOSHI:
			this.getChildByTag(TAG_BUTTON_SHINEI).setSpriteFrame("expuncheck.png");
			this.getChildByTag(TAG_BUTTON_FENG).setSpriteFrame("expuncheck.png");
			this.getChildByTag(TAG_BUTTON_GUANGZHAO).setSpriteFrame("expuncheck.png");
			this.getChildByTag(TAG_BUTTON_CHAOSHI).setSpriteFrame("expuncheck1.png");
			this.getChildByTag(TAG_BUTTON_RESULT).setSpriteFrame("expuncheck.png");
			
			//室内
			ll.run.getChildByTag(TAG_SHINEI).getChildByTag(TAG_BASE_INK1111).stopAllActions();
			ll.run.getChildByTag(TAG_SHINEI).getChildByTag(TAG_BASE_PRESSUREBIG1).stopAllActions();
			ll.run.getChildByTag(TAG_SHINEI).getChildByTag(TAG_BASE_INK6).stopAllActions();
			ll.run.getChildByTag(TAG_SHINEI).getChildByTag(TAG_BASE_INK5).stopAllActions();

			//风
			ll.run.getChildByTag(TAG_FENG).getChildByTag(TAG_BASE_INK11).stopAllActions();
			ll.run.getChildByTag(TAG_FENG).getChildByTag(TAG_BASE_PRESSUREBIG2).stopAllActions();
			ll.run.getChildByTag(TAG_FENG).getChildByTag(TAG_BASE_INK7).stopAllActions();
			ll.run.getChildByTag(TAG_FENG).getChildByTag(TAG_BASE_INK8).stopAllActions();

			//光照
			ll.run.getChildByTag(TAG_GUANGZHAO).getChildByTag(TAG_BASE_INK11111).stopAllActions();
			ll.run.getChildByTag(TAG_GUANGZHAO).getChildByTag(TAG_BASE_PRESSUREBIG2).stopAllActions();
			ll.run.getChildByTag(TAG_GUANGZHAO).getChildByTag(TAG_BASE_INK7).stopAllActions();
			ll.run.getChildByTag(TAG_GUANGZHAO).getChildByTag(TAG_BASE_INK8).stopAllActions();

			ll.run.shinei.setOpacity(0);
			ll.run.base.setOpacity(0);
			ll.run.feng.setOpacity(0);
			ll.run.guangzhao.setOpacity(0);
			ll.run.result.setOpacity(0);
			ll.run.chaoshi.setOpacity(255);
			ll.run.chaoshi.action();
//			this.chaoshi = new Chaoshi(this);
			//this.chaoshi.setVisible(false);
			ll.run.chaoshi.setPosition(370,550);
			this.getChildByTag(TAG_BUTTON_SHINEI).setEnable(true);
			this.getChildByTag(TAG_BUTTON_FENG).setEnable(true);
			this.getChildByTag(TAG_BUTTON_GUANGZHAO).setEnable(true);
			this.getChildByTag(TAG_BUTTON_CHAOSHI).setEnable(false);
			this.getChildByTag(TAG_BUTTON_RESULT).setEnable(true);
			break;
		case TAG_BUTTON_RESULT:
			this.getChildByTag(TAG_BUTTON_SHINEI).setSpriteFrame("expuncheck.png");
			this.getChildByTag(TAG_BUTTON_FENG).setSpriteFrame("expuncheck.png");
			this.getChildByTag(TAG_BUTTON_GUANGZHAO).setSpriteFrame("expuncheck.png");
			this.getChildByTag(TAG_BUTTON_CHAOSHI).setSpriteFrame("expuncheck.png");
			this.getChildByTag(TAG_BUTTON_RESULT).setSpriteFrame("expuncheck1.png");

			//室内
			ll.run.getChildByTag(TAG_SHINEI).getChildByTag(TAG_BASE_INK1111).stopAllActions();
			ll.run.getChildByTag(TAG_SHINEI).getChildByTag(TAG_BASE_PRESSUREBIG1).stopAllActions();
			ll.run.getChildByTag(TAG_SHINEI).getChildByTag(TAG_BASE_INK6).stopAllActions();
			ll.run.getChildByTag(TAG_SHINEI).getChildByTag(TAG_BASE_INK5).stopAllActions();

			//风
			ll.run.getChildByTag(TAG_FENG).getChildByTag(TAG_BASE_INK11).stopAllActions();
			ll.run.getChildByTag(TAG_FENG).getChildByTag(TAG_BASE_PRESSUREBIG2).stopAllActions();
			ll.run.getChildByTag(TAG_FENG).getChildByTag(TAG_BASE_INK7).stopAllActions();
			ll.run.getChildByTag(TAG_FENG).getChildByTag(TAG_BASE_INK8).stopAllActions();

			//光照
			ll.run.getChildByTag(TAG_GUANGZHAO).getChildByTag(TAG_BASE_INK11111).stopAllActions();
			ll.run.getChildByTag(TAG_GUANGZHAO).getChildByTag(TAG_BASE_PRESSUREBIG2).stopAllActions();
			ll.run.getChildByTag(TAG_GUANGZHAO).getChildByTag(TAG_BASE_INK7).stopAllActions();
			ll.run.getChildByTag(TAG_GUANGZHAO).getChildByTag(TAG_BASE_INK8).stopAllActions();
			
			//潮湿
			ll.run.getChildByTag(TAG_CHAOSHI).getChildByTag(TAG_BASE_INK111).stopAllActions();
			ll.run.getChildByTag(TAG_CHAOSHI).getChildByTag(TAG_BASE_PRESSUREBIG2).stopAllActions();
			ll.run.getChildByTag(TAG_CHAOSHI).getChildByTag(TAG_BASE_INK7).stopAllActions();
			ll.run.getChildByTag(TAG_CHAOSHI).getChildByTag(TAG_BASE_INK8).stopAllActions();
			if(gg.cloud&&gg.wind&&gg.sun&&gg.house)
			{
				
				ll.run.shinei.setOpacity(0);
				ll.run.feng.setOpacity(0);
				ll.run.guangzhao.setOpacity(0);
				ll.run.chaoshi.setOpacity(0);
				ll.run.result.setOpacity(255);
				ll.run.result.action();
				//this.chaoshi.setVisible(false);
				ll.run.result.setPosition(370,550);
				this.getChildByTag(TAG_BUTTON_SHINEI).setEnable(true);
				this.getChildByTag(TAG_BUTTON_FENG).setEnable(true);
				this.getChildByTag(TAG_BUTTON_GUANGZHAO).setEnable(true);
				this.getChildByTag(TAG_BUTTON_CHAOSHI).setEnable(true);
				this.getChildByTag(TAG_BUTTON_RESULT).setEnable(false);
			}else{
				var show=new WinFrame(ll.tip,2,"\t\t\t\t请先观看\n四种环境下的\n\t\t\t\t演示实验！");
				show.open2();


				this.getChildByTag(TAG_BUTTON_SHINEI).setEnable(true);
				this.getChildByTag(TAG_BUTTON_FENG).setEnable(true);
				this.getChildByTag(TAG_BUTTON_GUANGZHAO).setEnable(true);
				this.getChildByTag(TAG_BUTTON_CHAOSHI).setEnable(true);
				this.getChildByTag(TAG_BUTTON_RESULT).setEnable(false);
			}


			break;

		default:
			break;
		}
	},
});