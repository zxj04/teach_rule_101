var RunMainLayer19 = cc.Layer.extend({
	lead:null,
	real:null,
	ctor:function () {
		this._super();
		this.loadTip();
		this.loadRun();
		this.loadTool();
		this.loadFlow();
 
		return true;
	},
	
	loadTip:function(){
		ll.tip = new TipLayer(this);
	},
	loadTool:function(){
		ll.tool = new ToolLayer(this);
	},
	loadRun:function(){
		ll.run = new RunLayer1901(this);
	},
	loadFlow:function(){
		gg.flow.setMain(this);
		gg.flow.start();
	},
	over: function (){
		ll.tip.over();
		ll.tip.back_frame.open();
		// 提交成绩
		net.saveScore();
	},
});
