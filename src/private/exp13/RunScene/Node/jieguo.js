Jieguo = cc.Node.extend({
	ctor:function(p){
		this._super();
		p.addChild(this, 10, TAG_RESULT);
		this.init();
	},
	init : function(){
		this.setCascadeOpacityEnabled(true);
		var machine= new Button(this, 10, TAG_RESULT_WHOLE, "#whole.png",this.callback);
		machine.setScale(0.3);
		machine.setPosition(-145,-200);
		machine.setCascadeOpacityEnabled(true);
		this.loadName("室内环境 ", machine);
		var pressure= new Button(this, 12, TAG_RESULT_PRESSURE, "#exp19pressurebig1.png",this.callback,this);
		pressure.setPosition(-50,-172);
		pressure.setScale(0.4);
		var ink= new Button(this, 13, TAG_RESULT_INK, "#exp19ink3.png",this.callback,this);
		ink.setPosition(-13,-219);
		ink.setScale(0.4);
		var ink00= new Button(this, 13, TAG_RESULT_INK00, "#exp19ink4.png",this.callback,this);
		ink00.setPosition(-13,-214);
		ink00.setScale(0.4,0.75);
		ink00.setAnchorPoint(0.5,0);
		var machine1= new Button(this, 10, TAG_RESULT_WHOLE1, "#whole.png",this.callback);
		machine1.setScale(0.3);
		machine1.setPosition(70,-200);
		machine1.setCascadeOpacityEnabled(true);
		this.loadName("潮湿环境 ", machine1);
		var pressure1= new Button(this, 12, TAG_RESULT_PRESSURE1, "#exp19pressurebig1.png",this.callback,this);
		pressure1.setPosition(150,-172);
		pressure1.setScale(0.4);
		var ink1= new Button(this, 13, TAG_RESULT_INK1, "#exp19ink3.png",this.callback,this);
		ink1.setPosition(187,-219);
		ink1.setScale(0.4);
		var ink11= new Button(this, 13, TAG_RESULT_INK11, "#exp19ink4.png",this.callback,this);
		ink11.setPosition(187,-214);
		ink11.setScale(0.4,0.75);
		ink11.setAnchorPoint(0.5,0);
		var machine2= new Button(this, 10, TAG_RESULT_WHOLE2, "#whole.png",this.callback);
		machine2.setScale(0.3);
		machine2.setPosition(285,-200);
		machine2.setCascadeOpacityEnabled(true);
		this.loadName("光照环境 ", machine2);
		var pressure2= new Button(this, 12, TAG_RESULT_PRESSURE2, "#exp19pressurebig2.png",this.callback,this);
		pressure2.setPosition(365,-172);
		pressure2.setScale(0.4);
		var ink2= new Button(this, 13, TAG_RESULT_INK2, "#exp19ink3.png",this.callback,this);
		ink2.setPosition(401,-219);
		ink2.setScale(0.4);
		var ink22= new Button(this, 13, TAG_RESULT_INK22, "#exp19ink4.png",this.callback,this);
		ink22.setPosition(401,-214);
		ink22.setScale(0.4,0.75);
		ink22.setAnchorPoint(0.5,0);						
		var machine3= new Button(this, 10, TAG_RESULT_WHOLE3, "#whole.png",this.callback);
		machine3.setScale(0.3);
		machine3.setPosition(500,-200);
		machine3.setCascadeOpacityEnabled(true);
		this.loadName("自然风环境 ", machine3);
		var pressure3= new Button(this, 12, TAG_RESULT_PRESSURE3, "#exp19pressurebig2.png",this.callback,this);
		pressure3.setPosition(580,-172);
		pressure3.setScale(0.4);
		var ink3= new Button(this, 13, TAG_RESULT_INK3, "#exp19ink3.png",this.callback,this);
		ink3.setPosition(617,-219);
		ink3.setScale(0.4);
		var ink33= new Button(this, 13, TAG_RESULT_INK33, "#exp19ink4.png",this.callback,this);
		ink33.setPosition(617,-214);
		ink33.setScale(0.4,0.75);
		ink33.setAnchorPoint(0.5,0);


		//	this.action();
	},

	action:function(){

		this.getChildByTag(TAG_RESULT_INK00).setScale(0.4,0.75);
		this.getChildByTag(TAG_RESULT_INK11).setScale(0.4,0.75);
		this.getChildByTag(TAG_RESULT_INK22).setScale(0.4,0.75);
		this.getChildByTag(TAG_RESULT_INK33).setScale(0.4,0.75);
		
		this.getChildByTag(TAG_RESULT_INK00).runAction(cc.scaleTo(4,0.4,0.25));
		this.getChildByTag(TAG_RESULT_INK11).runAction(cc.scaleTo(4,0.4,0.55));
		this.getChildByTag(TAG_RESULT_INK22).runAction(cc.scaleTo(4,0.4,0.45));
		this.getChildByTag(TAG_RESULT_INK33).runAction(cc.scaleTo(4,0.4,0.15));

	},
	//按钮中的文字
	loadName:function(name,pare){
		var str = new cc.LabelTTF(name,gg.fontName,90);
		pare.addChild(str, 2);
		str.setColor(cc.color(255,255,255));
		str.setPosition(cc.p(142,-150));
	},
	callback:function(p){
		var action = gg.flow.flow.action;
		var func = cc.callFunc(this.actionDone, this);	
		switch(p.getTag()){	


		}
	},
	flowNext:function(){
		gg.flow.next();
	},
});