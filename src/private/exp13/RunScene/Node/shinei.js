Shinei = cc.Node.extend({
	ctor:function(p){
		this._super();
		p.addChild(this, 10, TAG_SHINEI);
		this.init();
	},
	init : function(){
		this.setCascadeOpacityEnabled(true);
		var machine= new Button(this, 10, TAG_BASE_MACHINE, "#exp19machine.png",this.callback);
		machine.setScale(0.8);
		machine.setPosition(70,-180);

		var machine1= new Button(this, 12, TAG_BASE_MACHINE1, "#exp19machine2.png",this.callback,this);
		machine1.setPosition(86,-252);
		machine1.setScale(0.8);


		var plant= new Button(this, 5, TAG_BASE_PLANT, "#show/plant1.png",this.callback,this);
		plant.setPosition(90,-60);
		plant.setScale(0.8);
		plant.setOpacity(220);

		var pressure= new Button(this, 11, TAG_BASE_PRESSURE, "#exp19pressure.png",this.callback,this);
		pressure.setPosition(105,-270);
		pressure.setScale(0.8);

		var ink1= new Button(this, 11, TAG_BASE_INK1111, "#exp19ink2.png",this.callback,this);
		ink1.setPosition(105,-353);
		ink1.setScale(0.8);

		var ink2= new Button(this, 11, TAG_BASE_INK2, "#exp19ink2.png",this.callback,this);
		ink2.setPosition(85,-252);
		ink2.setScale(0.8,1.5);

		var ink3= new Button(this, 11, TAG_BASE_INK3, "#exp19ink1.png",this.callback,this);
		ink3.setPosition(100,-370);
		ink3.setScale(0.8);
		var ink4= new Button(this, 11, TAG_BASE_INK5, "#exp19ink3.png",this.callback,this);
		ink4.setPosition(324,-363);
		ink4.setScale(0.8);
		ink4.setOpacity(0);
		var ink5= new Button(this, 11, TAG_BASE_INK6, "#exp19ink4.png",this.callback,this);
		ink5.setPosition(324,-353);
		ink5.setScale(0.8,2);
		ink5.setOpacity(0);
		ink5.setAnchorPoint(0.5,0);

		var pressurebig1= new Button(this, 10, TAG_BASE_PRESSUREBIG1, "#exp19pressurebig1.png",this.callback,this);
		pressurebig1.setPosition(250,-270);
		pressurebig1.setScale(0.8);
		pressurebig1.setOpacity(0);
		// this.action();

	},


	action:function(){
		if(gg.showTip!==null){
			gg.showTip.removeFromParent();
			gg.showTip=null;
		}
		
		this.getChildByTag(TAG_BASE_INK1111).setScale(0.8);
		this.getChildByTag(TAG_BASE_INK6).setScale(0.8,2);
		this.getChildByTag(TAG_BASE_INK6).setOpacity(0);
		this.getChildByTag(TAG_BASE_INK5).setOpacity(0);
		this.getChildByTag(TAG_BASE_PRESSUREBIG1).setOpacity(0);
		
		this.getChildByTag(TAG_BASE_INK1111).setAnchorPoint(0.5,0);
		this.getChildByTag(TAG_BASE_INK1111).runAction(cc.scaleTo(6,1,0.6));
		this.getChildByTag(TAG_BASE_PRESSUREBIG1).runAction(cc.sequence(cc.fadeTo(0.5, 255),cc.delayTime(8.5),cc.fadeTo(0.5, 0)));
		this.getChildByTag(TAG_BASE_INK6).runAction(cc.sequence(cc.fadeTo(0.5,255),cc.scaleTo(6,0.8,0.55),cc.delayTime(2),cc.fadeTo(0.5,0)));
		this.getChildByTag(TAG_BASE_INK5).runAction(cc.sequence(cc.fadeTo(0.5,255),cc.delayTime(8),cc.fadeTo(0.5,0),cc.delayTime(0.5),cc.callFunc(function(){
			gg.showTip = new ShowTip("--水柱会下降\n--因为此时植物会进行蒸腾作用",cc.p(700,270));
		},this)));


		gg.house=true;
	},
	callback:function(p){
		var action = gg.flow.flow.action;
		var func = cc.callFunc(this.actionDone, this);	
		switch(p.getTag()){	


		}
	},
	flowNext:function(){
		gg.flow.next();
	},
});
