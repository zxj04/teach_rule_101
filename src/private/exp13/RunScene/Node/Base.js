Base = cc.Node.extend({
	ctor:function(p){
		this._super();
		p.addChild(this, 10, TAG_BASE);
		this.init();
	},
	init : function(){
		this.setCascadeOpacityEnabled(true);
		var machine= new Button(this, 10, TAG_BASE_MACHINE, "#exp19machine.png",this.callback);
		machine.setScale(0.8);
		machine.setPosition(70,-180);

		var machine1= new Button(this, 12, TAG_BASE_MACHINE1, "#exp19machine2.png",this.callback,this);
		machine1.setPosition(86,-252);
		machine1.setScale(0.8);
		
        
		var plant= new Button(this, 5, TAG_BASE_PLANT, "#show/plant1.png",this.callback,this);
		plant.setPosition(90,-60);
		plant.setScale(0.8);
	
 
		var pressure= new Button(this, 11, TAG_BASE_PRESSURE, "#exp19pressure.png",this.callback,this);
		pressure.setPosition(105,-270);
		pressure.setScale(0.8);
		
		var ink1= new Button(this, 11, TAG_BASE_INK1, "#exp19ink2.png",this.callback,this);
		ink1.setPosition(105,-298);
		ink1.setScale(0.8);
		
		var ink2= new Button(this, 11, TAG_BASE_INK2, "#exp19ink2.png",this.callback,this);
		ink2.setPosition(85,-252);
		ink2.setScale(0.8,1.5);
		
		var ink3= new Button(this, 11, TAG_BASE_INK3, "#exp19ink1.png",this.callback,this);
		ink3.setPosition(100,-370);
		ink3.setScale(0.8);
		
		var ink4= new Button(this, 11, TAG_BASE_INK4, "#exp19ink1.png",this.callback,this);
		ink4.setPosition(2100,-370);
		ink4.setScale(0.1);
		
		
		
		
				
	},
	addpaomo :function(){
		var paomo = new cc.Sprite("#paomo.png");
		paomo.setRotationX(180);
		paomo.setPosition(118,208);
		paomo.setOpacity(0);
		this.conflask1.addChild(paomo,10);
		var seq = cc.sequence(cc.delayTime(0.5),cc.fadeIn(0.5));
		paomo.runAction(seq);


		var paomo1 = new cc.Sprite("#paomo1.png");
		paomo1.setPosition(350,25);
		paomo1.setAnchorPoint(0, 0.5);
		paomo1.setScale(0);
		this.conflask.addChild(paomo1,10);
		var se = cc.sequence(cc.delayTime(0.5),cc.scaleTo(1,1));
		paomo1.runAction(se);

	},

	callback:function(p){
		var action = gg.flow.flow.action;
		var func = cc.callFunc(this.actionDone, this);	
		switch(p.getTag()){	
//		case TAG_LID:
//			if(action == ACTION_DO1){
//				var move = cc.moveTo(0.5,cc.p(110,370));
//				var move1 = cc.moveTo(0.5,cc.p(0,350));
//				var move2 = cc.moveTo(1,cc.p(-50,60));
//				var rota  = cc.rotateTo(1,-180);
//				var sp = cc.spawn(move2,rota);
//				var seq = cc.sequence(move,move1,sp,cc.callFunc(function(){
//					p.setSpriteFrame("#sanjiaolid1.png");
//					p.setRotation(0);
//					this.flowNext();
//				},this));
//			}else if(action == ACTION_DO2){
//				p.setSpriteFrame("sanjiaolid2.png");
//				var move = cc.moveTo(1,cc.p(0,350));
//				var rota  = cc.rotateTo(1,0);
//				var sp = cc.spawn(move,rota);
//				var move1 = cc.moveTo(0.5,cc.p(110,370));				
//				var move2 = cc.moveTo(0.5,cc.p(110,310));
//				var seq = cc.sequence(sp,move1,move2,cc.callFunc(function(){				
//					this.flowNext();
//				},this));				
//			}			
//			p.runAction(seq);
//			break;
//		case TAG_SG:
//			var move = cc.moveTo(1.5,cc.p(105,450));
//			var move1 = cc.moveTo(1,cc.p(105,380));
//			var rota = cc.rotateTo(1,-23);
//			var sp = cc.spawn(move1,rota);
//
//			var move2 = cc.moveTo(0.5,cc.p(105,290));
//			var move3 = cc.moveTo(0.5,cc.p(120,250));
//			var move4 = cc.moveTo(1,cc.p(100,100));
//			var seq = cc.sequence(move,sp,cc.callFunc(function(){
//				this.line.setSpriteFrame("exp10sgline2.png");
//				this.line.setPosition(100,280);
//				this.line.setRotation(23);
//			},this),move2,move3,move4,cc.callFunc(function(){
//				this.line.setSpriteFrame("exp10sgline3.png");
//				this.line.setPosition(25,218);
//				this.line.setRotation(23);
//				this.flowNext();
//			},this));
//			p.runAction(seq);
//
//			var sq =cc.sequence(cc.delayTime(1.5),cc.rotateTo(1,23)); 
//			this.sgline.runAction(sq);
//			break;
//		case TAG_XCONFLASK:
//			this.conflask.setSpriteFrame("sanjiaoflask2.png");
//			this.conflask.setRotationX(180);
//			this.conflask.setPosition(190,166);
//
//			var rota  = cc.rotateTo(1,180);
//			var seq = cc.sequence(cc.callFunc(this.addpaomo(),this),rota,cc.callFunc(function(){
//
//
//				this.conflask1.getChildByTag(TAG_LID).setSpriteFrame("sanjiaolid1.png");
//				this.conflask1.getChildByTag(TAG_LID).setRotation(180);
//
//				this.line.setSpriteFrame("exp10sgline4.png");
//				this.line.setPosition(85,185);
//				this.line.setRotation(203);
//
//				this.conflaskline.setRotationX(180);
//			},this),cc.delayTime(3),cc.callFunc(this.flowNext ,this));
//			this.runAction(seq);
//			this.sg.runAction(cc.moveTo(1,cc.p(130,230)));
//			this.conflaskline.runAction(cc.spawn(cc.moveTo(1,cc.p(110,155)),cc.scaleTo(1,0.8)));
//			break;
//
		}
	},
	flowNext:function(){
		gg.flow.next();
	},
});