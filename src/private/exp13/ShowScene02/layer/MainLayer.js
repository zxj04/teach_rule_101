exp13.ShowMainLayer02 = cc.Layer.extend({
	lead:null,
	real:null,
	curIndex: 0,
	cur:0,
	cell:50,
	ctor:function () {
		this._super();
		this.init();		
		return true;
	},
	init:function(){
		this.sv = new pub.ScrollView(this);
		this.sv.setOffestPos(cc.p(152,45 + 15));
		
		this.addlayout1();				
		this.addlayout2();
	},
	addlayout1:function(){
		var layout = new ccui.Layout();// 第一页
		layout.setContentSize(cc.size(996, 598));// 设置layout的大小
		this.sv.addLayer(layout);

		var text = $.format("实验（蒸腾作用）", 820, 30);
		this.addText(layout, text, cc.p(498, 520), true);
		
		var bg = new cc.LayerColor();
		bg.setColor(cc.color(0,0,0));
		bg.setContentSize(cc.size(700, 400));
		bg.setPosition(150, 50);
		layout.addChild(bg);
		
		var show1 = this.addSprite(layout, "#s201.png", cc.p(498,250));
		var show2 = this.addSprite(layout, "#s202.png", cc.p(498,250));
		var show3 = this.addSprite(layout, "#s203.png", cc.p(498,250));
		show3.setOpacity(0);
		this.show3 = show3;
		
		this.addButton(layout, TAG_BUTTON1, "开始\n实验", cc.p(80, 300));
		this.time = this.addText(layout, "", cc.p(498, 480));
		
		var cloud = new exp13.CloudLabel(layout, "  仔细观察,塑料袋上出现\n水珠,说明植物有蒸腾作用", 
				exp04.CLOUD_RIGHT_UP);
		cloud.setVisible(false);
		cloud.setPosition(820, 480);
		this.cloud = cloud;
	},
	addlayout2:function(){
		var layout = new ccui.Layout();// 第一页
		layout.setContentSize(cc.size(996, 598));// 设置layout的大小
		this.sv.addLayer(layout);

		var text = $.format("　　蒸腾作用是指水分从活的植物表面以气体状态散失到大气的过程。", 820, 30);
		this.addText(layout, text, cc.p(50, 550));

		new exp13.CloudLabel(layout, "根吸收的水分,99%\n通过蒸腾作用散发出去", 
				exp13.CLOUD_RIGHT_UP).setPos(820, 400);

		new exp13.CloudLabel(layout, "在温度偏高时有\n效降低叶片温度", 
				exp13.CLOUD_RIGHT_DOWN).setPos(820, 100);

		new exp13.CloudLabel(layout, "我是根部吸水的主要动力", 
				exp13.CLOUD_LEFT_UP).setPos(180, 400);

		new exp13.CloudLabel(layout, "利于植物对水的吸收和运输\n　利于水中无机盐的运输", 
				exp13.CLOUD_LEFT_DOWN).setPos(180, 100);

		var text = $.format("蒸腾作用对植物自身\n具有非常重要的意义。", 820, 30);
		this.addText(layout, text, cc.p(498, 250), true);
	},
	addSprite:function(p, img, pos, rect){
		var sprite = new cc.Sprite(img, rect);
		if(pos){
			sprite.setPosition(pos);
		}
// sprite.setAnchorPoint(0, 0.5);
		p.addChild(sprite);
		return sprite;
	},
	addButton:function(parent,tag,str,pos){
		var s9 = new pub.S9Button(parent, str,this.callback,this);
		s9.setPosition(pos);
		s9.setTag(tag);
		return s9;
	},
	addText:function(p, text, pos, defaultAp){
		var label = new cc.LabelTTF(text, gg.fontName, gg.fontSize4);
		label.setPosition(pos);
		if(!defaultAp){
			label.setAnchorPoint(0, 0.5);
		}
		label.setColor(cc.color(0, 0, 0, 250));
		p.addChild(label);
		return label;
	},
	minute:1,
	callback:function(p){
		switch(p.getTag()){
		case TAG_BUTTON1:
			if(this.minute == 182){
				this.minute = 1;
				this.show3.setOpacity(0);
				this.cloud.setVisible(false);
			} else if(this.minute == 1){
			} else {
				break;
			}
			this.show3.runAction(cc.fadeIn(3.6));
			this.schedule(function(){
				this.time.setString("" + this.minute++ + "分钟");
				if(this.minute == 180){
					this.cloud.setVisible(true);
				}
			}, 0.02, 185, 1);
			break;
		}
	}
});
