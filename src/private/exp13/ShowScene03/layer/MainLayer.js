exp13.ShowMainLayer03 = cc.Layer.extend({
	lead:null,
	real:null,
	ctor:function () {
		this._super();
		this.init();
		return true;
	},
	init:function(){
		var sv = new pub.ScrollView(this);
		sv.setOffestPos(cc.p(152,45 + 15));

	//	this.addlayout1(sv);
		this.addlayout2(sv);
		this.addlayout3(sv);
		this.addlayout4(sv);
//		this.loadNextButton();
	},	
	loadNextButton:function(){		
		var label = new Label(this,"1.2电生磁>>",this.callback);
		label.setLocalZOrder(5);
		label.setTag(TAG_EXP_12);
		label.setAnchorPoint(1, 0);
		label.setColor(cc.color(19, 98, 27, 250));
		label.setPosition(gg.width - 40,25);
	},
	
	addlayout2:function(parent){
		this.layout2 = new ccui.Layout();//第二页
		this.layout2.setContentSize(cc.size(996, 598));
		parent.addLayer(this.layout2);

		var text1 = $.format("\t\t\t\t在观察蚕豆叶的下表皮实验中，用镊子撕下一小块蚕豆叶的下表皮，制成装片，用低倍镜观察蚕豆下" +
				"表皮细胞，下列关于它的特点说法不正确的是（）", 1000,30);
		var label1 = new cc.LabelTTF(text1,gg.fontName,gg.fontSize4);

		label1.setAnchorPoint(0,0.5);
		label1.setPosition(0,525.5);
		label1.setColor(cc.color(0, 0, 0, 250));
		this.layout2.addChild(label1);	
		

		this.addimage(this.layout2, "#select.png", TAG_BUTTON2, cc.p(60,410), "A.细胞形状不规则，彼此嵌合", TAG_LABEL, cc.p(270,410));
		this.sel = new cc.Sprite("#wrong.png");
		this.sel.setPosition(15,15);
		this.layout2.getChildByTag(TAG_BUTTON2).addChild(this.sel);
		this.sel.setVisible(false);

		this.addimage(this.layout2, "#select.png", TAG_BUTTON3, cc.p(60,360), "B.表皮细胞中间分布着成对的半月形保卫细胞", TAG_LABEL, cc.p(358,360));
		this.sel1 = new cc.Sprite("#right.png");
		this.sel1.setPosition(15,15);
		this.layout2.getChildByTag(TAG_BUTTON3).addChild(this.sel1);
		this.sel1.setVisible(false);

		this.addimage(this.layout2, "#select.png", TAG_BUTTON4, cc.p(60,310), "C.表皮细胞内含有叶绿体，呈现绿色", TAG_LABEL, cc.p(308,310));
		this.sel2 = new cc.Sprite("#wrong.png");
		this.sel2.setPosition(15,15);
		this.layout2.getChildByTag(TAG_BUTTON4).addChild(this.sel2);
		this.sel2.setVisible(false);

		this.addimage(this.layout2, "#select.png", TAG_BUTTON5, cc.p(60,260), "D.表皮细胞无色透明", TAG_LABEL, cc.p(222,260));
		this.sel3 = new cc.Sprite("#wrong.png");
		this.sel3.setPosition(15,15);
		this.layout2.getChildByTag(TAG_BUTTON5).addChild(this.sel3);
		this.sel3.setVisible(false);

		var text2 = $.format("\t\t\t\t易错分析：表皮由一层活细胞组成，形状不规则，排列紧密，没有细胞间隙，一般没有叶绿体，不能进行光合作用，无色透明。" +
				"表皮内含有成对的半月形保卫细胞，两个保卫细胞之间形成气孔，是气体进出叶的通道。" , 980,30);
		this.label5 = new cc.LabelTTF(text2,gg.fontName,gg.fontSize4);

		this.label5.setPosition(500,130);
		this.label5.setColor(cc.color(0, 0, 0, 250));
		this.label5.setVisible(false);
		this.layout2.addChild(this.label5);		
	},
	addlayout3:function(parent){
		this.layout3 = new ccui.Layout();//第二页
		this.layout3.setContentSize(cc.size(996, 598));
		parent.addLayer(this.layout3);

		var text1 = $.format("\t\t\t\t一般来说，植物叶的上、下表皮都有保卫细胞，并且保卫细胞的数量关系是（）", 1000,30);
		var label1 = new cc.LabelTTF(text1,gg.fontName,gg.fontSize4);
		label1.setAnchorPoint(0,0.5);
		label1.setPosition(0,525.5);
		label1.setColor(cc.color(0, 0, 0, 250));
		this.layout3.addChild(label1);	

		this.addimage(this.layout3, "#select.png", TAG_BUTTON6, cc.p(60,400), "A.上表皮多", TAG_LABEL, cc.p(170,400));
		this.sel4 = new cc.Sprite("#wrong.png");
		this.sel4.setPosition(15,15);
		this.layout3.getChildByTag(TAG_BUTTON6).addChild(this.sel4);
		this.sel4.setVisible(false);

		this.addimage(this.layout3, "#select.png", TAG_BUTTON7, cc.p(60,350), "B.下表皮多", TAG_LABEL, cc.p(170,350));
		this.sel5 = new cc.Sprite("#right.png");
		this.sel5.setPosition(15,15);
		this.layout3.getChildByTag(TAG_BUTTON7).addChild(this.sel5);
		this.sel5.setVisible(false);

		this.addimage(this.layout3, "#select.png", TAG_BUTTON8, cc.p(60,300), "C.上、下表皮一样多", TAG_LABEL, cc.p(220,300));
		this.sel6 = new cc.Sprite("#wrong.png");
		this.sel6.setPosition(15,15);
		this.layout3.getChildByTag(TAG_BUTTON8).addChild(this.sel6);
		this.sel6.setVisible(false);

		this.addimage(this.layout3, "#select.png", TAG_BUTTON9, cc.p(60,250), "D.不能确定", TAG_LABEL, cc.p(170,250));
		this.sel7 = new cc.Sprite("#wrong.png");
		this.sel7.setPosition(15,15);
		this.layout3.getChildByTag(TAG_BUTTON9).addChild(this.sel7);
		this.sel7.setVisible(false);
		
//		this.sel8 = new cc.Sprite("#titu.png");
//		this.sel8.setPosition(692,400);
//		this.layout3.addChild(this.sel8);


		var text2 = $.format("\t\t\t\t易错分析：每对保卫细胞围成一个气孔，因此保卫细胞的数量与气孔数量成正比。叶片中的水分散失时，水分" +
				"比较容易从上表皮气孔逸出，如果上表皮气孔多于下表皮气孔，水分散失较快，在缺少水分的时期对植物生长很不利。在长期的进化过程中。" +
				"叶片结构趋于合理化，上表皮气孔少，有利于适应干旱的环境;相应的，上表皮的保卫细胞也较少。", 980,30);
		this.label6 = new cc.LabelTTF(text2,gg.fontName,gg.fontSize4);

		this.label6.setPosition(500,90);
		this.label6.setColor(cc.color(0, 0, 0, 250));
		this.label6.setVisible(false);
		this.layout3.addChild(this.label6);		
	},
	
	addlayout4:function(parent){
		this.layout4 = new ccui.Layout();//第三页
		this.layout4.setContentSize(cc.size(996, 598));
		parent.addLayer(this.layout4);
		var text1 = $.format("\t\t\t\t移栽植物的时候，适当剪除一些叶片有利于被移裁植物的成活，其主要目的是（）", 1000,30);
		var label1 = new cc.LabelTTF(text1,gg.fontName,gg.fontSize4);
		label1.setAnchorPoint(0,0.5);
		label1.setPosition(0,525.5);
		label1.setColor(cc.color(0, 0, 0, 250));
		this.layout4.addChild(label1);	

		this.addimage(this.layout4, "#select.png", TAG_BUTTON10, cc.p(60,420), "A.降低呼吸作用对有机物的消耗", TAG_LABEL, cc.p(290,420));
		this.sel8 = new cc.Sprite("#wrong.png");
		this.sel8.setPosition(15,15);
		this.layout4.getChildByTag(TAG_BUTTON10).addChild(this.sel8);
		this.sel8.setVisible(false);

		this.addimage(this.layout4, "#select.png", TAG_BUTTON11, cc.p(60,370), "B.避免蒸腾作用过多失水", TAG_LABEL, cc.p(250,370));
		this.sel9 = new cc.Sprite("#wrong.png");
		this.sel9.setPosition(15,15);
		this.layout4.getChildByTag(TAG_BUTTON11).addChild(this.sel9);
		this.sel9.setVisible(false);

		this.addimage(this.layout4, "#select.png", TAG_BUTTON12, cc.p(60,320), "C.避免大风吹倒新栽的植物", TAG_LABEL, cc.p(260,320));
		this.sel10 = new cc.Sprite("#right.png");
		this.sel10.setPosition(15,15);
		this.layout4.getChildByTag(TAG_BUTTON12).addChild(this.sel10);
		this.sel10.setVisible(false);

		this.addimage(this.layout4, "#select.png", TAG_BUTTON13, cc.p(60,270), "D.使移栽操作更方便", TAG_LABEL, cc.p(225,270));
		this.sel11 = new cc.Sprite("#wrong.png");
		this.sel11.setPosition(15,15);
		this.layout4.getChildByTag(TAG_BUTTON13).addChild(this.sel11);
		this.sel11.setVisible(false);




		var text2 = $.format("\t\t\t\t易错分析：移栽植物时，多少会伤及植物的根。而植物主要是通过根吸收水分的。如果根受损，叶子的蒸腾作用又强，植物就会因失水" +
				"过多而死亡。适当剪除一些叶片有利于移栽植物的成活--主要是减少失水。待植物的根恢复生机后，自身就能适应各种环境变化了。", 980,30);
		this.label7 = new cc.LabelTTF(text2,gg.fontName,gg.fontSize4);

		this.label7.setPosition(500,130);
		this.label7.setColor(cc.color(0, 0, 0, 250));
		this.label7.setVisible(false);
		this.layout4.addChild(this.label7);		
	},
	addimage:function(parent,str,tag,pos,str2,tag2,pos2){
		var image = new ButtonScale(parent,str,this.callback,this);
		image.setPosition(pos);
		image.setTag(tag);
		var label = new cc.LabelTTF(str2,gg.fontName,gg.fontSize);
		label.setColor(cc.color(0, 0, 0, 250));
		parent.addChild(label);
		label.setPosition(pos2);
		label.setTag(tag2);
	},
	addbutton:function(parent,tag,str,pos){
		var button = new ButtonScale(parent,"#unsel.png",this.callback,this);
		button.setPosition(pos);
		button.setLocalZOrder(5);
		button.setTag(tag);
		var label = new cc.LabelTTF(str,gg.fontName,30);
		label.setPosition(button.width/2,button.height/2);		
		label.setColor(cc.color(0, 0, 0, 250));
		button.addChild(label);
	},
	callback:function(p){
		switch(p.getTag()){		
		case TAG_BUTTON:
			p.setEnable(false);
			//p.setSpriteFrame("sel.png");
			var move = cc.moveTo(1.5,cc.p(600,230));
			var ber = cc.bezierTo(1.5, [cc.p(360,228),cc.p(460,160),cc.p(545,135)]);
			var seq = cc.sequence(cc.callFunc(function(){
				this.line1.setVisible(false);
				this.qiu1.setVisible(false);
				this.label4.setVisible(false);
				this.line2.setVisible(false);
				this.qiu2.setVisible(false);
				this.label5.setVisible(false);
				this.citie.setVisible(false);
			},this),move,cc.callFunc(function(){
				this.line1.setVisible(true);
				this.qiu1.setVisible(true);
				this.label4.setVisible(true);
				this.qiu.setPosition(180,230);
			},this),cc.delayTime(0.5),cc.callFunc(function(){
				this.citie.setVisible(true);
			},this),ber,cc.callFunc(function(){
				this.line2.setVisible(true);
				this.qiu2.setVisible(true);
				this.label5.setVisible(true);
				this.qiu.setPosition(180,230);
				p.setEnable(true);
				//p.setSpriteFrame("unsel.png");
			},this));
			this.qiu.runAction(seq);
			break;
		case TAG_BUTTON2:
		case TAG_BUTTON3:
		case TAG_BUTTON4:
		case TAG_BUTTON5:
			this.sel.setVisible(true);
			this.sel1.setVisible(true);
			this.sel2.setVisible(true);
			this.sel3.setVisible(true);
			this.label5.setVisible(true);
			break;
		case TAG_BUTTON6:
		case TAG_BUTTON7:
		case TAG_BUTTON8:
		case TAG_BUTTON9:
			this.sel4.setVisible(true);
			this.sel5.setVisible(true);
			this.sel6.setVisible(true);
			this.sel7.setVisible(true);
			this.label6.setVisible(true);
			break;
		case TAG_BUTTON10:
		case TAG_BUTTON11:
		case TAG_BUTTON12:
		case TAG_BUTTON13:
			this.sel8.setVisible(true);
			this.sel9.setVisible(true);
			this.sel10.setVisible(true);
			this.sel11.setVisible(true);
			this.label7.setVisible(true);
			break;
			case TAG_EXP_12:
			pub.ch.run(p.getTag());
			break;

		}
	}
	
});
