exp01.ShowMainLayer04 = cc.Layer.extend({
	lead:null,
	real:null,
	ctor:function () {
		this._super();
		this.init();
		return true;
	},
	init:function(){//
		var sv = new pub.ScrollView(this);
		sv.setOffestPos(cc.p(152,45 + 15));

		this.addlayout1(sv);	
		
//		this.loadNextButton();
	},
	loadNextButton:function(){		
		var label = new Label(this,"难题解析>>",this.callback);
		label.setLocalZOrder(5);
		label.setTag(TAG_MENU4);
		label.setAnchorPoint(1, 0);
		label.setColor(cc.color(19, 98, 27, 250));
		label.setPosition(gg.width - 40,25);
	},
	addlayout1:function(parent){
		var layout1 = new ccui.Layout();//第一页
		layout1.setContentSize(cc.size(996, 598));//设置layout的大小
		parent.addLayer(layout1);
		
		var text1 = $.format("电磁起重机原理：电磁起重机的主要部分是磁铁。接通电流，电磁铁便把钢铁物品牢牢吸住，吊"+
				"运到指定的地方。切断电流，磁性消失，钢铁物品就放下来了。" +
				"\n使用注意事项：电磁起重机的起重电磁铁应当由专用电路供电。电磁起重机工作" +
				"时因失电，其吊运的物品坠落可能造成危害时，必须能够保证电磁吸盘供电。", 980,30);
		var label1 = new cc.LabelTTF(text1,gg.fontName,gg.fontSize4);
		label1.setColor(cc.color(0, 0, 0, 250));
		label1.setPosition(gg.c_width,gg.height-label1.height/2 - 130);
		this.addChild(label1);	

		var sprite = new cc.Sprite("#yuanli.jpg"); //牛顿
		sprite.setPosition(375,265);
		this.addChild(sprite);

		var sprite1 = new cc.Sprite("#qizhong.png"); //牛顿
		sprite1.setPosition(875,265);
		sprite1.setScale(0.5,0.34);
		this.addChild(sprite1);
//		var text2 = $.format("　　英国物理学家牛顿指出：一切物体在没有受到外力作用时，它们的运动状态保持不变" +
//				"包括保持静止或保持匀速直线运动状态——牛顿第一定律", 510,30);
//		label2 = new cc.LabelTTF(text2,gg.fontName,gg.fontSize4);
//		this.addChild(label2);
//		label2.setAnchorPoint(1,1);
//		label2.setColor(cc.color(0, 0, 0, 250));
//		label2.setPosition(1125,400);
	},
	callback:function(p){		
		switch(p.getTag()){
		case TAG_MENU4:
			gg.runNext(p.getTag());
			break;
		}
	}

});
