exp01.ShowMainLayer09 = cc.Layer.extend({
	lead:null,
	real:null,
	curIndex: 0,
	cur:0,
	cell:50,
	ctor:function () {
		this._super();
		this.init();
		return true;
	},
	init:function(){
			
		var sv = new pub.ScrollView(this);
		sv.setOffestPos(cc.p(152,45 + 15));
		
		this.addlayout1(sv);
		this.addlayout2(sv);				
		this.addlayout3(sv);
		
	
		var label = new Label(this,"力是改变物体状态的原因>>",this.callback);
		label.setLocalZOrder(5);
		label.setTag(TAG_MENU1);
		label.setAnchorPoint(1, 0);
		label.setColor(cc.color(19, 98, 27, 250));
		label.setPosition(gg.width - 40,25);
		
		
	
	},
	addlayout1:function(parent){
		var layout1 = new ccui.Layout();//第一页
		layout1.setContentSize(cc.size(996, 598));//设置layout的大小
		parent.addLayer(layout1);

		var text = $.format("惯性　：一个运动物体，在不受外力时，将保持自己原有速度的带下和方向一直运动下去；" +
				"一个静止的物体，在不受外力作用时，将保持自己的静止状态。" +
				"\n　　一切物体都有惯性，无论其是运动还是静止，其大小只取决于物体的质量。" +
				"\n　　当物体运动状态发生改变时，人们能够体验到物体抵抗状态改变的这种能力，即惯性。", 980, 30);
		this.label1 = new cc.LabelTTF(text,gg.fontName,gg.fontSize4);
		this.label1.setPosition(490,layout1.height-this.label1.height/2);
		this.label1.setColor(cc.color(0, 0, 0, 250));
		layout1.addChild(this.label1);	
		
		var muban = new cc.Sprite("#muban.png"); //木板
		muban.setPosition(300,180);
		muban.setScale(0.65);
		layout1.addChild(muban);
		
		this.zhi = new cc.Sprite("#zhi1.png"); //纸
		this.zhi.setPosition(800,150);
		this.zhi.setAnchorPoint(1,0.5);
		this.zhi.setScale(0.6,1);
		muban.addChild(this.zhi);

		var bihe = new cc.Sprite("#bihe.png"); //笔盒
		bihe.setPosition(580,150);
		bihe.setRotation(5);
		muban.addChild(bihe);
		
		this.label = new cc.LabelTTF("笔盒由于惯性，\n没有从桌面掉下",gg.fontName,gg.fontSize4);
		this.label.setPosition(850,250);
		this.label.setColor(cc.color(0, 0, 0, 250));
		layout1.addChild(this.label);
		this.label.setVisible(false);
		
		this.addbutton(layout1, TAG_BUTTON1, "播放", cc.p(930,100));
	   
	},
	addlayout2:function(parent){
		var layout2 = new ccui.Layout();//第二页
		layout2.setContentSize(cc.size(996, 598));
//		parent.addChild(layout2);
		parent.addLayer(layout2);
		
		var shuibei = new cc.Sprite("#shuibei.png"); //水杯
		shuibei.setPosition(200,400);
		shuibei.setScale(0.65);
		layout2.addChild(shuibei,5);

		this.zhi1 = new cc.Sprite("#zhi2.png"); //纸
		this.zhi1.setPosition(130,300);
		shuibei.addChild(this.zhi1,5);

		this.yingbi = new cc.Sprite("#yingbi.png"); //硬币
		this.yingbi.setPosition(115,300);
		shuibei.addChild(this.yingbi,5);
			
		this.addbutton(layout2, TAG_BUTTON2, "播放", cc.p(490,300));
			
		var text = $.format("　　卡片受到手指弹动而飞出，但硬币没有飞出，说明硬币由于惯性，要保持原有静止状态，" +
				"但由于没有卡片支撑而落入杯中。", 400,30);
		this.label2 = new cc.LabelTTF(text,gg.fontName,gg.fontSize4);
		this.label2.setColor(cc.color(0, 0, 0, 250));
		this.label2.setPosition(750,450);
		layout2.addChild(this.label2);	
		
		var text = $.format("　　生活中还有很多惯性现象。比如行驶中的汽车，关闭油门是发动机停止运行，但汽车由于惯性" +
				"还会继续前行。\n　　汽车突然起动时，车内乘客会后仰，汽车在试行驶中突然刹车制动时，乘客会前冲。", 980,30);
		this.label2 = new cc.LabelTTF(text,gg.fontName,gg.fontSize4);
		this.label2.setColor(cc.color(0, 0, 0, 250));
		this.label2.setPosition(490,150);
		layout2.addChild(this.label2);	
		
		var bglayer = new cc.LayerColor(cc.color(40, 46, 57, 250),550,350);
		bglayer.setPosition(0 , 250);
		layout2.addChild(bglayer);
	},
	addlayout3:function(parent){
		this.layout3 = new ccui.Layout();//第三页
		this.layout3.setContentSize(cc.size(996, 598));
		parent.addLayer(this.layout3);

		var text3 = $.format("　　生活中有很多防止惯性和利用惯性的例子。\n　　防止惯性：", 980,30);
		this.label3 = new cc.LabelTTF(text3,gg.fontName,gg.fontSize4);
		this.label3.setAnchorPoint(0,0.5);
		this.label3.setColor(cc.color(0, 0, 0, 250));
		this.label3.setPosition(0,this.layout3.height-this.label3.height/2);		
		this.layout3.addChild(this.label3);	
		
		this.addimage(this.layout3, "#anquandai.png", cc.p(185,420), "汽车安全带", cc.p(190,310));
		this.addimage(this.layout3, "#qnquanjuli.png", cc.p(500,420), "汽车行驶安全距离", cc.p(500,310));
		this.addimage(this.layout3, "#zhidongqi.png", cc.p(800,420), "自行车下坡减速", cc.p(800,310));

		var text4 = $.format("　　利用惯性：", 980,30);
		this.label4 = new cc.LabelTTF(text4,gg.fontName,gg.fontSize4);
		this.label4.setPosition(0,250);
		this.label4.setColor(cc.color(0, 0, 0, 250));
		this.label4.setAnchorPoint(0,0.5);
		this.layout3.addChild(this.label4);	
		
		this.addimage(this.layout3, "#poshui.png", cc.p(185,135), "用盆泼水", cc.p(185,30));
		this.addimage(this.layout3, "#tiaoyuan.png", cc.p(500,135), "跳远助跑", cc.p(500,30));
		this.addimage(this.layout3, "#motuoche.png", cc.p(800,135), "摩托车飞跃表演", cc.p(800,30));
	},
	addbutton:function(parent,tag,str,pos,labelx){
		var button = new ButtonScale(parent,"#unsel.png",this.callback,this);
		button.setPosition(pos);
		button.setLocalZOrder(5);
		button.setTag(tag);
		var label = new cc.LabelTTF(str,gg.fontName,30);
		label.setPosition(button.width/2,button.height/2);		
		label.setColor(cc.color(0, 0, 0, 250));
		button.addChild(label);
	},
	addimage:function(parent,str,pos,str2,pos2){
		var image = new cc.Sprite(str);
		parent.addChild(image);
		image.setPosition(pos);
		var label = new cc.LabelTTF(str2,gg.fontName,gg.fontSize);
		label.setColor(cc.color(0, 0, 0, 250));
		parent.addChild(label);
		label.setPosition(pos2);
		
	},
	callback:function(p){		
		switch(p.getTag()){
		case TAG_BUTTON1:
			p.setEnable(false);
			this.label.setVisible(true);
			var move = cc.moveTo(0.3,cc.p(980,150));
			var move1 = cc.moveTo(0.5,cc.p(1050,100));
			var seq= cc.sequence(move,move1,cc.delayTime(1),cc.callFunc(function(){
				p.setEnable(true);
				this.zhi.setPosition(800,150);
				this.zhi.setRotation(0);
			},this));
			this.zhi.runAction(seq);
			break;
		case TAG_BUTTON2:
			p.setEnable(false);
			var move = cc.moveTo(0.3,cc.p(250,300));
			var move1 = cc.moveTo(0.5,cc.p(400,300));
			var move2 = cc.moveTo(0.5,cc.p(450,200));
			var seq = cc.sequence(move,cc.callFunc(function(){
				var rota = cc.rotateTo(0.2,-90);
				var mov = cc.moveTo(0.2,cc.p(115,260));
				var sp = cc.spawn(rota,mov);
				
				var mov1 = cc.moveTo(0.4,cc.p(115,180));
				var mov2 = cc.moveTo(0.8,cc.p(115,100));
				
				var mov3 = cc.moveTo(0.5,cc.p(115,50));
				var rota1 = cc.rotateTo(0.6,0);
				var sp1= cc.spawn(rota1,mov3);
				var se = cc.sequence(sp,mov1,mov2,sp1);
				this.yingbi.runAction(se);
				
			},this),move1,move2,cc.delayTime(1.5),cc.callFunc(function(){
				p.setEnable(true);
				this.yingbi.setPosition(115,300);
				this.zhi1.setPosition(130,300);
			},this));
			this.zhi1.runAction(seq);
			break;
		case TAG_MENU1:
			gg.runNext(p.getTag());
		break;
		}
	}
	
});
