exp01.ShowMainLayer02 = cc.Layer.extend({
	lead:null,
	real:null,
	curIndex: 0,
	cur:0,
	cell:50,
	ctor:function () {
		this._super();
		this.init();		
		return true;
	},
	init:function(){

		var sv = new pub.ScrollView(this);
		sv.setOffestPos(cc.p(152,45 + 15));

		this.addlayout1(sv);
//		this.addlayout2(sv);				
//		this.addlayout3(sv);
//		this.addlayout4(sv);
//		this.addlayout5(sv);
//		this.addlayout6(sv);

	},

	addlayout1:function(parent){
		var layout1 = new ccui.Layout();//第一页
		layout1.setContentSize(cc.size(996, 598));//设置layout的大小
		parent.addLayer(layout1);

		var text = $.format("移液管的使用： " , 980, 30);
		this.label1 = new cc.LabelTTF(text,gg.fontName,gg.fontSize4);
		this.label1.setPosition(110,layout1.height-this.label1.height/2);
		this.label1.setColor(cc.color(0, 0, 0, 250));
		layout1.addChild(this.label1);	
		
		var text2 = $.format("实验目的： " +
				"准确移取25.00mL0.1mol/L烧碱溶液至锥形瓶中。" , 980, 30);
		this.label2 = new cc.LabelTTF(text2,gg.fontName,gg.fontSize4);
		this.label2.setPosition(415,478);
		this.label2.setColor(cc.color(0, 0, 0, 250));
		layout1.addChild(this.label2);	
//
		var text3 = $.format("实验器材及药品：" +
				"移液管、锥形瓶、烧杯、洗耳球、蒸馏水、0.1mol/L的" +
				"\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\tHCL。" , 980, 30);
		this.label3 = new cc.LabelTTF(text3,gg.fontName,gg.fontSize4);
		this.label3.setPosition(485,378);
		this.label3.setColor(cc.color(0, 0, 0, 250));
		layout1.addChild(this.label3);
		
		var text3 = $.format("操作步骤：" , 980, 30);
		this.label3 = new cc.LabelTTF(text3,gg.fontName,gg.fontSize4);
		this.label3.setPosition(80,278);
		this.label3.setColor(cc.color(0, 0, 0, 250));
		layout1.addChild(this.label3);
		
		var buzou = new cc.Sprite("#buzou.png"); //支架
		buzou.setPosition(480,150);
		buzou.setScale(0.8);
		layout1.addChild(buzou,5);
//
//		this.needle = new cc.Sprite("#needle.png"); //指针
//		this.needle.setPosition(501,98);
//		this.needle.setAnchorPoint(0.5,0.5);
//		this.needle.setScale(1);
//		whole.addChild(this.needle);
//		
//		this.start1 = new cc.Sprite("#start.png"); //开关
//		this.start1.setPosition(75,160);
//		this.start1.setAnchorPoint(0,0);
//		this.start1.setScale(0.6);
//		this.start1.setRotation(-8);
//		layout1.addChild(this.start1,4);
//
//		var line = new cc.Sprite("#line.png"); //线
//		line.setPosition(485,120);
//		line.setScale(1);
//		
//		whole.addChild(line);
//
//		this.label = new cc.LabelTTF("奥斯特实验告诉我们通\n电导体周围存在磁场.",gg.fontName,gg.fontSize4);
//		this.label.setPosition(700,250);
//		this.label.setColor(cc.color(0, 0, 0, 250));
//		layout1.addChild(this.label);
//		this.label.setVisible(false);
//
//		this.addbutton(layout1, TAG_BUTTON1, "播放", cc.p(290,150));

	},
	addlayout2:function(parent){
		var layout2 = new ccui.Layout();//第二页
		layout2.setContentSize(cc.size(996, 598));
//		parent.addChild(layout2);
		parent.addLayer(layout2,6);

		this.ciwhole = new cc.Sprite("#cifenwhole.png"); //水杯
		this.ciwhole.setPosition(260,400);
		this.ciwhole.setScale(0.65);
		layout2.addChild(this.ciwhole,5);

		this.start2 = new cc.Sprite("#start.png"); //纸
		this.start2.setPosition(40,300);
		this.start2.setRotation(-8);
		this.start2.setScale(0.7);
		this.start2.setAnchorPoint(0,0);
		layout2.addChild(this.start2,4);

		this.bigcifen= new cc.Sprite("#bigcifen.png"); //硬币
		this.bigcifen.setPosition(275,400);
		this.bigcifen.setOpacity(0);
		layout2.addChild(this.bigcifen,4);
		this.cifen1 = new cc.Sprite("#cifen1.png"); //磁粉
		this.cifen1.setPosition(207,180);
		this.cifen1.setOpacity(0);
		this.bigcifen.addChild(this.cifen1,5);
		this.cifen2 = new cc.Sprite("#cifen2.png"); //磁粉
		this.cifen2.setPosition(207,180);
		this.cifen2.setOpacity(0);
		this.bigcifen.addChild(this.cifen2,5);
		this.cifen3 = new cc.Sprite("#cifen3.png"); //磁粉
		this.cifen3.setPosition(207,180);
		this.cifen3.setOpacity(0);
		this.bigcifen.addChild(this.cifen3,5);
		this.cifen4 = new cc.Sprite("#cifen4.png"); //硬币
		this.cifen4.setPosition(207,180);
		this.cifen4.setOpacity(0);
		this.bigcifen.addChild(this.cifen4,5);
		this.hammer = new cc.Sprite("#hammer.png"); //硬币
		this.hammer.setPosition(415,150);
		this.hammer.setOpacity(0);
		this.hammer.setScale(0.6);
		this.bigcifen.addChild(this.hammer,5);
		
		var text = $.format("通电导线的磁场： " +
				"\n\n\t\t直线电流周围的磁感线分布规律是：以直导线上各点为圆心的一些同心圆。这些同心圆在与导线垂直的平面上。越靠近通电导线，磁场越强。" , 400, 30);
		this.label1 = new cc.LabelTTF(text,gg.fontName,gg.fontSize4);
		this.label1.setPosition(790,layout2.height-this.label1.height/2);
		this.label1.setColor(cc.color(0, 0, 0, 250));
		layout2.addChild(this.label1);	
		

		this.addbutton(layout2, TAG_BUTTON2, "播放", cc.p(470,200));

		var text1 = $.format("总结：" +
				"\n\t\t通电导体存在磁场，磁场方向与电流方向有关、磁场的强弱与电流大小有关、可以使用安培定则判断磁场的方向。", 1000,30);
		this.label2 = new cc.LabelTTF(text1,gg.fontName,gg.fontSize4);
		this.label2.setColor(cc.color(0, 0, 0, 250));
		this.label2.setPosition(488,100);
		layout2.addChild(this.label2);	



		var bglayer = new cc.LayerColor(cc.color(40, 46, 57, 250),550,430);
		bglayer.setPosition(0 , 170);
		layout2.addChild(bglayer);
	},
	addlayout3:function(parent){
		var layout3 = new ccui.Layout();//第三页
		layout3.setContentSize(cc.size(996, 598));
		parent.addLayer(layout3);
        //基础通电螺线管
		var wholeBase = new cc.Sprite("#wholenail.png"); //支架
		wholeBase.setPosition(240,390);
		wholeBase.setScale(0.7);
		layout3.addChild(wholeBase,5);
		wholeBase.setCascadeOpacityEnabled(true);
		
		this.nail = new cc.Sprite("#nail.png"); //钉
		this.nail.setPosition(-50,50);
		wholeBase.addChild(this.nail,5);
		this.nail1 = new cc.Sprite("#nail.png"); //钉
		this.nail1.setPosition(-30,50);		
		this.nail1.setRotation(40);
		wholeBase.addChild(this.nail1,5);
		
		this.base = new cc.Sprite("#base.png"); //底座
		this.base.setPosition(257,-30);
		this.base.setOpacity(0);
		wholeBase.addChild(this.base,5);
		
		this.base1 = new cc.Sprite("#base.png"); //底座
		this.base1.setPosition(527,30);
		wholeBase.addChild(this.base1,5);
		
		this.neddle1 = new cc.Sprite("#needle2.png"); //磁针
		this.neddle1.setPosition(256,0);	
		this.neddle1.setOpacity(0);
		wholeBase.addChild(this.neddle1,5);
	
		this.neddle2 = new cc.Sprite("#needle2.png"); //磁针
		this.neddle2.setPosition(527,60);	
		wholeBase.addChild(this.neddle2,5);    
		
		this.start3 = new cc.Sprite("#start.png"); //开关
		this.start3.setPosition(130,495);
		this.start3.setAnchorPoint(0,0);
		this.start3.setScale(0.6);
		this.start3.setRotation(-8);
		layout3.addChild(this.start3,4);
		
		//电流反向
		
		var wholeFan = new cc.Sprite("#wholefan.png"); //支架
		wholeFan.setPosition(740,390);
		wholeFan.setScale(0.7);
		layout3.addChild(wholeFan,5);
		
		this.nail2 = new cc.Sprite("#nail.png"); //钉
		this.nail2.setPosition(-50,50);
		wholeFan.addChild(this.nail2,5);
		this.nail3 = new cc.Sprite("#nail.png"); //钉
		this.nail3.setPosition(-30,50);		
		this.nail3.setRotation(40);
		wholeFan.addChild(this.nail3,5);

		this.base2 = new cc.Sprite("#base.png"); //底座
		this.base2.setPosition(257,-30);
		this.base2.setOpacity(0);
		wholeFan.addChild(this.base2,5);

		this.base3 = new cc.Sprite("#base.png"); //底座
		this.base3.setPosition(537,30);
		wholeFan.addChild(this.base3,5);

		this.neddle3 = new cc.Sprite("#needle2.png"); //磁针
		this.neddle3.setPosition(256,0);	
		this.neddle3.setOpacity(0);
		wholeFan.addChild(this.neddle3,5);

		this.neddle4 = new cc.Sprite("#needle2.png"); //磁针
		this.neddle4.setPosition(537,60);	
		wholeFan.addChild(this.neddle4,5);

		this.start4 = new cc.Sprite("#start.png"); //开关
		this.start4.setPosition(635,495);
		this.start4.setAnchorPoint(0,0);
		this.start4.setScale(0.6);
		this.start4.setRotation(-8);
		layout3.addChild(this.start4,4);

		
		this.addbutton(layout3, TAG_BUTTON3, "播放", cc.p(490,200));
		

		
		var text3 = $.format("电流方向改变：" +
				"\n\t\t从观察到小磁针两次方向的变化，证明磁场方向与电流的方向有关，从铁钉吸引的程度，证明磁场强弱与电流方向无关。", 1000,30);
		this.label3 = new cc.LabelTTF(text3,gg.fontName,gg.fontSize4);
		this.label3.setColor(cc.color(0, 0, 0, 250));
		this.label3.setPosition(488,100);
		this.label3.setOpacity(0);
		layout3.addChild(this.label3);	
		

	},
	addlayout4:function(parent){
		var layout4 = new ccui.Layout();//第四页
		layout4.setContentSize(cc.size(996, 598));
		parent.addLayer(layout4);
		//基础通电螺线管
		var wholeBase2 = new cc.Sprite("#wholenail.png"); //支架
		wholeBase2.setPosition(240,390);
		wholeBase2.setScale(0.7);
		layout4.addChild(wholeBase2,5);
		

		this.nail4 = new cc.Sprite("#nail.png"); //钉
		this.nail4.setPosition(-50,50);
		wholeBase2.addChild(this.nail4,5);
		this.nail5 = new cc.Sprite("#nail.png"); //钉
		this.nail5.setPosition(-30,50);		
		this.nail5.setRotation(40);
		wholeBase2.addChild(this.nail5,5);

		this.base4 = new cc.Sprite("#base.png"); //底座
		this.base4.setPosition(257,-30);
		this.base4.setOpacity(0);
		wholeBase2.addChild(this.base4,5);

		this.base5 = new cc.Sprite("#base.png"); //底座
		this.base5.setPosition(527,30);
		wholeBase2.addChild(this.base5,5);

		this.neddle5 = new cc.Sprite("#needle2.png"); //磁针
		this.neddle5.setPosition(256,0);
		this.neddle5.setOpacity(0);
		wholeBase2.addChild(this.neddle5,5);

		this.neddle6 = new cc.Sprite("#needle2.png"); //磁针
		this.neddle6.setPosition(527,60);	
		wholeBase2.addChild(this.neddle6,5);

		this.start5 = new cc.Sprite("#start.png"); //开关
		this.start5.setPosition(130,495);
		this.start5.setAnchorPoint(0,0);
		this.start5.setScale(0.6);
		this.start5.setRotation(-8);
		layout4.addChild(this.start5,4);

	

//		增加电池

		var wholebattery = new cc.Sprite("#wholezeng.png"); //支架
		wholebattery.setPosition(740,390);
		wholebattery.setScale(0.7);
		layout4.addChild(wholebattery,5);

//		this.battery = new cc.Sprite("#battery.png"); //钉
//		this.battery.setPosition(825,502);
//		this.battery.setScale(0.7);
//		layout4.addChild(this.battery,4);
		
		this.nail6 = new cc.Sprite("#nail.png"); //钉
		this.nail6.setPosition(30,50);
		wholebattery.addChild(this.nail6,5);
		this.nail7 = new cc.Sprite("#nail.png"); //钉
		this.nail7.setPosition(40,50);		
		this.nail7.setRotation(40);
		wholebattery.addChild(this.nail7,5);

		this.base6 = new cc.Sprite("#base.png"); //底座
		this.base6.setPosition(257,-30);
		this.base6.setOpacity(0);
		wholebattery.addChild(this.base6,5);

		this.base7 = new cc.Sprite("#base.png"); //底座
		this.base7.setPosition(557,40);
		wholebattery.addChild(this.base7,5);

		this.neddle7 = new cc.Sprite("#needle2.png"); //磁针
		this.neddle7.setPosition(256,0);	
		this.neddle7.setOpacity(0);
		wholebattery.addChild(this.neddle7,5);

		this.neddle8 = new cc.Sprite("#needle2.png"); //磁针
		this.neddle8.setPosition(557,70);	
		wholebattery.addChild(this.neddle8,5);

		this.start6 = new cc.Sprite("#start.png"); //开关
		this.start6.setPosition(565,495);
		this.start6.setAnchorPoint(0,0);
		this.start6.setScale(0.6);
		this.start6.setRotation(-8);
		layout4.addChild(this.start6,4);

		this.addbutton(layout4, TAG_BUTTON4, "播放", cc.p(490,200));
	    
		var text3 = $.format("电流增大：" +
				"\n\t\t线圈匝数不变增加电流，从铁钉吸引的程度证明磁场强弱与电流的大小有关，电流越大磁场越强。", 1000,30);
		this.label4 = new cc.LabelTTF(text3,gg.fontName,gg.fontSize4);
		this.label4.setColor(cc.color(0, 0, 0, 250));
		this.label4.setPosition(488,100);
		this.label4.setOpacity(0);
		layout4.addChild(this.label4);	


	},
	
	addlayout5:function(parent){
		var layout5 = new ccui.Layout();//第五页
		layout5.setContentSize(cc.size(996, 598));
		parent.addLayer(layout5);
		//基础通电螺线管
		var wholeBase3 = new cc.Sprite("#wholeline2.png"); //支架
		wholeBase3.setPosition(510,390);
		wholeBase3.setScale(0.9);
		layout5.addChild(wholeBase3,5);


		this.nail8 = new cc.Sprite("#nail.png"); //钉
		this.nail8.setPosition(-50,50);
		wholeBase3.addChild(this.nail8,5);
		this.nail9 = new cc.Sprite("#nail.png"); //钉
		this.nail9.setPosition(-30,50);		
		this.nail9.setRotation(40);
		wholeBase3.addChild(this.nail9,5);

		this.nail10 = new cc.Sprite("#nail.png"); //钉
		this.nail10.setPosition(870,50);
		wholeBase3.addChild(this.nail10,5);
		this.nail11 = new cc.Sprite("#nail.png"); //钉
		this.nail11.setPosition(880,50);		
		this.nail11.setRotation(40);
		wholeBase3.addChild(this.nail11,5);
//		this.base8 = new cc.Sprite("#base.png"); //底座
//		this.base8.setPosition(257,-30);
//		wholeBase3.addChild(this.base8,5);
//
//		this.base9 = new cc.Sprite("#base.png"); //底座
//		this.base9.setPosition(527,30);
//		wholeBase3.addChild(this.base9,5);
//
//		this.neddle9 = new cc.Sprite("#needle2.png"); //磁针
//		this.neddle9.setPosition(256,0);	
//		wholeBase3.addChild(this.neddle9,5);
//
//		this.neddle10 = new cc.Sprite("#needle2.png"); //磁针
//		this.neddle10.setPosition(527,60);	
//		wholeBase3.addChild(this.neddle10,5);

		this.start7 = new cc.Sprite("#start.png"); //开关
		this.start7.setPosition(230,520);
		this.start7.setAnchorPoint(0,0);
		this.start7.setScale(0.8);
		this.start7.setRotation(-8);
		layout5.addChild(this.start7,4);



//		增加线圈

//		var wholeline = new cc.Sprite("#wholeline.png"); //支架
//		wholeline.setPosition(740,390);
//		wholeline.setScale(0.7);
//		layout5.addChild(wholeline,5);
//
//	
//
//		this.nail10 = new cc.Sprite("#nail.png"); //钉
//		this.nail10.setPosition(-50,50);
//		wholeline.addChild(this.nail10,5);
//		this.nail11 = new cc.Sprite("#nail.png"); //钉
//		this.nail11.setPosition(-30,50);		
//		this.nail11.setRotation(40);
//		wholeline.addChild(this.nail11,5);
//
//		this.base10 = new cc.Sprite("#base.png"); //底座
//		this.base10.setPosition(257,-30);
//		wholeline.addChild(this.base10,5);
//
//		this.base11 = new cc.Sprite("#base.png"); //底座
//		this.base11.setPosition(537,30);
//		wholeline.addChild(this.base11,5);
//
//		this.neddle11 = new cc.Sprite("#needle2.png"); //磁针
//		this.neddle11.setPosition(256,0);	
//		wholeline.addChild(this.neddle11,5);
//
//		this.neddle12 = new cc.Sprite("#needle2.png"); //磁针
//		this.neddle12.setPosition(537,60);	
//		wholeline.addChild(this.neddle12,5);
//
//		this.start8 = new cc.Sprite("#start.png"); //开关
//		this.start8.setPosition(635,495);
//		this.start8.setAnchorPoint(0,0);
//		this.start8.setScale(0.6);
//		this.start8.setRotation(-8);
//		layout5.addChild(this.start8,4);

		this.addbutton(layout5, TAG_BUTTON5, "播放", cc.p(490,200));
		var text3 = $.format("增加线圈：" +
				"\n\t\t电流保持不变线圈匝数增加，根据铁钉吸引程度证明磁场随线圈匝数的增加而增大，线圈匝数越多磁场越强。", 1000,30);
		this.label5 = new cc.LabelTTF(text3,gg.fontName,gg.fontSize4);
		this.label5.setColor(cc.color(0, 0, 0, 250));
		this.label5.setPosition(488,100);
		this.label5.setOpacity(0);
		layout5.addChild(this.label5);	


	},
	
	addlayout6:function(parent){
		var layout6 = new ccui.Layout();//第五页
		layout6.setContentSize(cc.size(996, 598));
		parent.addLayer(layout6);
		//基础通电螺线管
		var wholeBase4 = new cc.Sprite("#wholenail.png"); //支架
		wholeBase4.setPosition(230,390);
		wholeBase4.setScale(0.7);
		layout6.addChild(wholeBase4,5);


		this.nail12 = new cc.Sprite("#nail.png"); //钉
		this.nail12.setPosition(-50,50);
		wholeBase4.addChild(this.nail12,5);
		this.nail13 = new cc.Sprite("#nail.png"); //钉
		this.nail13.setPosition(-30,50);		
		this.nail13.setRotation(40);
		wholeBase4.addChild(this.nail13,5);

		this.base12 = new cc.Sprite("#base.png"); //底座
		this.base12.setPosition(257,-30);
		this.base12.setOpacity(0);
		wholeBase4.addChild(this.base12,5);

		this.base13 = new cc.Sprite("#base.png"); //底座
		this.base13.setPosition(527,30);
		wholeBase4.addChild(this.base13,5);

		this.neddle13 = new cc.Sprite("#needle2.png"); //磁针
		this.neddle13.setPosition(256,0);
		this.neddle13.setOpacity(0);
		wholeBase4.addChild(this.neddle13,5);

		this.neddle14 = new cc.Sprite("#needle2.png"); //磁针
		this.neddle14.setPosition(527,60);	
		wholeBase4.addChild(this.neddle14,5);

		this.start9 = new cc.Sprite("#start.png"); //开关
		this.start9.setPosition(130,495);
		this.start9.setAnchorPoint(0,0);
		this.start9.setScale(0.6);
		this.start9.setRotation(-8);
		layout6.addChild(this.start9,4);



//		增加铁芯

		var wholeFe = new cc.Sprite("#wholeFe.png"); //支架
		wholeFe.setPosition(740,390);
		wholeFe.setScale(0.7);
		layout6.addChild(wholeFe,5);

		this.Fe = new cc.Sprite("#Fe.png"); //支架
		this.Fe.setPosition(740,310);
		this.Fe.setScale(0.7);
		layout6.addChild(this.Fe,4);
		
	

		this.nail14 = new cc.Sprite("#nail.png"); //钉
		this.nail14.setPosition(-50,50);
		wholeFe.addChild(this.nail14,5);
		this.nail15 = new cc.Sprite("#nail.png"); //钉
		this.nail15.setPosition(-30,50);		
		this.nail15.setRotation(40);
		wholeFe.addChild(this.nail15,5);

		this.base14 = new cc.Sprite("#base.png"); //底座
		this.base14.setPosition(257,-30);
		this.base14.setOpacity(0);
		wholeFe.addChild(this.base14,5);

		this.base15 = new cc.Sprite("#base.png"); //底座
		this.base15.setPosition(537,30);
		wholeFe.addChild(this.base15,5);

		this.neddle15 = new cc.Sprite("#needle2.png"); //磁针
		this.neddle15.setPosition(256,0);
		this.neddle15.setOpacity(0);
		wholeFe.addChild(this.neddle15,5);

		this.neddle16 = new cc.Sprite("#needle2.png"); //磁针
		this.neddle16.setPosition(537,60);	
		wholeFe.addChild(this.neddle16,5);

		this.start10 = new cc.Sprite("#start.png"); //开关
		this.start10.setPosition(635,495);
		this.start10.setAnchorPoint(0,0);
		this.start10.setScale(0.6);
		this.start10.setRotation(-8);
		layout6.addChild(this.start10,4);

		this.addbutton(layout6, TAG_BUTTON6, "播放", cc.p(490,200));

		var text3 = $.format("增加铁芯：" +
				"\n\t\t电流和线圈匝数保持不变增加铁芯，根据铁钉吸引程度证明铁芯被磁化后相当于磁铁增强磁场。", 1000,30);
		this.label6 = new cc.LabelTTF(text3,gg.fontName,gg.fontSize4);
		this.label6.setColor(cc.color(0, 0, 0, 250));
		this.label6.setPosition(488,100);
		this.label6.setOpacity(0);
		layout6.addChild(this.label6);	
	},
	
	
	addbutton:function(parent,tag,str,pos,labelx){
		var button = new ButtonScale(parent,"#unsel.png",this.callback,this);
		button.setPosition(pos);
		button.setLocalZOrder(5);
		button.setTag(tag);
		var label = new cc.LabelTTF(str,gg.fontName,30);
		label.setPosition(button.width/2,button.height/2);		
		label.setColor(cc.color(0, 0, 0, 250));
		button.addChild(label);
	},
	addimage:function(parent,str,pos,str2,pos2){
		var image = new cc.Sprite(str);
		parent.addChild(image);
		image.setPosition(pos);
		var label = new cc.LabelTTF(str2,gg.fontName,gg.fontSize);
		label.setColor(cc.color(0, 0, 0, 250));
		parent.addChild(label);
		label.setPosition(pos2);

	},
	callback:function(p){		
		switch(p.getTag()){
		case TAG_BUTTON1:
			p.setEnable(false);
			this.start1.setRotation(-8);
			this.needle.setRotation(0);
			this.label.setVisible(true);
			var rota=cc.rotateTo(1,10);
			var seq=cc.sequence(rota,cc.callFunc(function(){
				this.needle.runAction(cc.sequence(cc.rotateTo(0.5,20),cc.rotateTo(0.2,40),cc.rotateTo(0.5,70),cc.rotateTo(0.5,30),cc.rotateTo(0.5,90),cc.rotateTo(0.5,120),cc.rotateTo(0.5,280)));

			},this),cc.callFunc(function(){
				p.setEnable(true);
			},this));
			this.start1.runAction(seq);
			break;
		case TAG_BUTTON2:
			p.setEnable(false);
			this.start2.setRotation(-8);
			this.ciwhole.setOpacity(255);
			this.start2.setOpacity(255);
			this.bigcifen.setOpacity(0);
			this.cifen1.setOpacity(0);
			this.hammer.setOpacity(0);
			this.cifen4.setOpacity(0);
			var rota1=cc.rotateTo(1.5,8);
			var seq=cc.sequence(rota1,cc.delayTime(1),cc.callFunc(function(){
				this.ciwhole.setOpacity(0);
				this.start2.setOpacity(0);
				this.bigcifen.setOpacity(255);
				this.cifen1.setOpacity(255);
				this.hammer.setOpacity(255);
				this.hammer.runAction(cc.repeat(cc.sequence(cc.rotateTo(0.2, -5),cc.rotateTo(0.2,0)), 40));
			},this),cc.callFunc(function(){
				this.cifen1.runAction(cc.sequence(cc.spawn(cc.fadeTo(3.5, 50),cc.callFunc(function(){
					this.cifen2.runAction(cc.fadeTo(3.5, 255));
				},this)),cc.spawn(cc.callFunc(function(){
					this.cifen2.runAction(cc.fadeTo(3.5, 0));
				},this),cc.callFunc(function(){
					this.cifen3.runAction(cc.fadeTo(3.5, 255));
				},this)),cc.spawn(cc.callFunc(function(){
					this.cifen3.runAction(cc.fadeTo(3.5, 0));
				},this),cc.callFunc(function(){
					this.cifen4.runAction(cc.sequence(cc.fadeTo(3.5, 255),cc.callFunc(function(){
						p.setEnable(true);
					},this)));
					
				},this))
					
				));
			},this));
			this.start2.runAction(seq);

			break;
		case TAG_BUTTON3:
			p.setEnable(false);
			var rot=cc.rotateTo(1.5,10);
			//中间磁针
			spa1=cc.spawn(cc.rotateTo(1.5,90),cc.scaleTo(1.5,0.7));
			spa2=cc.spawn(cc.rotateTo(1.5,180),cc.scaleTo(1.5,1));
			spa3=cc.spawn(cc.rotateTo(1.5,90),cc.scaleTo(1.5,0.7));
			spa4=cc.spawn(cc.rotateTo(1.5,0),cc.scaleTo(1.5,1));
			//右边磁针
			spa5=cc.spawn(cc.rotateTo(1,10),cc.scaleTo(1.5,0.9));
			spa6=cc.spawn(cc.rotateTo(1,0),cc.scaleTo(1.5,1));
			spa7=cc.spawn(cc.rotateTo(1,-10),cc.scaleTo(1.5,0.9));
			spa8=cc.spawn(cc.rotateTo(1,0),cc.scaleTo(1.5,1));
			//螺线管基础状态动作
			var ret=cc.sequence(spa5,spa6,spa7,spa8);
			ret.retain();
			var ret1=cc.sequence(spa1,spa2,spa3,spa4);
			ret1.retain();
			var seq=cc.sequence(rot,cc.delayTime(1),cc.spawn(cc.spawn(cc.callFunc(function(){
				this.nail1.runAction(cc.moveTo(1, cc.p(30,50)));	
			},this),cc.callFunc(function(){		
				this.nail.runAction(cc.moveTo(1, cc.p(-40,50)));
			},this)),cc.callFunc(function(){
				this.label3.runAction(cc.fadeTo(1, 255));
				this.neddle2.runAction(cc.repeatForever(ret));
				this.neddle1.runAction(cc.repeatForever(ret1));
				
			},this)));
			this.start3.runAction(seq);
			//螺线管电流反向
			var rot1=cc.rotateTo(1.5,10);
			spa12=cc.spawn(cc.rotateTo(1.5,90),cc.scaleTo(1.5,0.7));
			spa22=cc.spawn(cc.rotateTo(1.5,180),cc.scaleTo(1.5,1));
			spa32=cc.spawn(cc.rotateTo(1.5,90),cc.scaleTo(1.5,0.7));
			spa42=cc.spawn(cc.rotateTo(1.5,0),cc.scaleTo(1.5,1));
			//右边磁针
			spa52=cc.spawn(cc.rotateTo(1,190),cc.scaleTo(1,0.9));
			spa62=cc.spawn(cc.rotateTo(1,180),cc.scaleTo(1,1));
			spa72=cc.spawn(cc.rotateTo(1,170),cc.scaleTo(1,0.9));
			spa82=cc.spawn(cc.rotateTo(1,180),cc.scaleTo(1,1));
			var ret2=cc.sequence(spa52,spa62,spa72,spa82);
			ret2.retain();
			var ret3=cc.sequence(spa12,spa22,spa32,spa42);
			ret3.retain();
			var seq1=cc.sequence(rot1,cc.delayTime(1),cc.spawn(cc.spawn(cc.callFunc(function(){
				this.nail3.runAction(cc.moveTo(1, cc.p(40,50)));	
			},this),cc.callFunc(function(){		
				this.nail2.runAction(cc.moveTo(1, cc.p(-30,50)));
			},this)),cc.callFunc(function(){
				this.neddle4.runAction(cc.repeatForever(ret2));
				this.neddle3.runAction(cc.repeatForever(ret3));

			},this)));
			
			this.start4.runAction(seq1);
			break;
		case TAG_BUTTON4:
			p.setEnable(false);
			var rot=cc.rotateTo(1.5,10);
			//中间磁针
			spa1=cc.spawn(cc.rotateTo(1.5,90),cc.scaleTo(1.5,0.7));
			spa2=cc.spawn(cc.rotateTo(1.5,180),cc.scaleTo(1.5,1));
			spa3=cc.spawn(cc.rotateTo(1.5,90),cc.scaleTo(1.5,0.7));
			spa4=cc.spawn(cc.rotateTo(1.5,0),cc.scaleTo(1.5,1));
			//右边磁针
			spa5=cc.spawn(cc.rotateTo(1,10),cc.scaleTo(1.5,0.9));
			spa6=cc.spawn(cc.rotateTo(1,0),cc.scaleTo(1.5,1));
			spa7=cc.spawn(cc.rotateTo(1,-10),cc.scaleTo(1.5,0.9));
			spa8=cc.spawn(cc.rotateTo(1,0),cc.scaleTo(1.5,1));
			var ret=cc.sequence(spa5,spa6,spa7,spa8);
			ret.retain();
			var ret1=cc.sequence(spa1,spa2,spa3,spa4);
			ret1.retain();
			//螺线管基础状态动作
			var seq=cc.sequence(rot,cc.delayTime(1),cc.spawn(cc.spawn(cc.callFunc(function(){
				this.nail5.runAction(cc.moveTo(1, cc.p(30,50)));	
			},this),cc.callFunc(function(){		
				this.nail4.runAction(cc.moveTo(1, cc.p(-40,50)));
			},this)),cc.callFunc(function(){
				this.neddle6.runAction(cc.repeatForever(ret));
				this.neddle5.runAction(cc.repeatForever(ret1));

			},this)));
			this.start5.runAction(seq);
			//螺线管电流反向
			var rot1=cc.rotateTo(1.5,10);
			spa12=cc.spawn(cc.rotateTo(0.5,90),cc.scaleTo(0.5,0.7));
			spa22=cc.spawn(cc.rotateTo(0.5,180),cc.scaleTo(0.5,1));
			spa32=cc.spawn(cc.rotateTo(0.5,90),cc.scaleTo(0.5,0.7));
			spa42=cc.spawn(cc.rotateTo(0.5,0),cc.scaleTo(0.5,1));
			//右边磁针
			spa52=cc.spawn(cc.rotateTo(0.5,15),cc.scaleTo(0.5,0.9));
			spa62=cc.spawn(cc.rotateTo(0.5,0),cc.scaleTo(0.5,1));
			spa72=cc.spawn(cc.rotateTo(0.5,-15),cc.scaleTo(0.5,0.9));
			spa82=cc.spawn(cc.rotateTo(0.5,0),cc.scaleTo(0.5,1));
			var ret2=cc.sequence(spa52,spa62,spa72,spa82);
			ret2.retain();
			var ret3=cc.sequence(spa12,spa22,spa32,spa42);
			ret3.retain();
			var seq1=cc.sequence(rot1,cc.delayTime(1),cc.spawn(cc.spawn(cc.callFunc(function(){
				this.nail7.runAction(cc.moveTo(1, cc.p(100,50)));	
			},this),cc.callFunc(function(){		
				this.nail6.runAction(cc.moveTo(1, cc.p(100,50)));
			},this)),cc.callFunc(function(){
				this.neddle8.runAction(cc.repeatForever(ret2));
				this.neddle7.runAction(cc.repeatForever(ret3));
				this.label4.runAction(cc.fadeTo(1, 255));
			},this)));

			this.start6.runAction(seq1);
			break;
		case TAG_BUTTON5:
			p.setEnable(false);
			var rot=cc.rotateTo(1.5,10);
//			//中间磁针
//			spa1=cc.spawn(cc.rotateTo(1.5,90),cc.scaleTo(1.5,0.7));
//			spa2=cc.spawn(cc.rotateTo(1.5,180),cc.scaleTo(1.5,1));
//			spa3=cc.spawn(cc.rotateTo(1.5,90),cc.scaleTo(1.5,0.7));
//			spa4=cc.spawn(cc.rotateTo(1.5,0),cc.scaleTo(1.5,1));
//			//右边磁针
//			spa5=cc.spawn(cc.rotateTo(1,10),cc.scaleTo(1.5,0.9));
//			spa6=cc.spawn(cc.rotateTo(1,0),cc.scaleTo(1.5,1));
//			spa7=cc.spawn(cc.rotateTo(1,-10),cc.scaleTo(1.5,0.9));
//			spa8=cc.spawn(cc.rotateTo(1,0),cc.scaleTo(1.5,1));
//			var ret=cc.sequence(spa5,spa6,spa7,spa8);
//			ret.retain();
//			var ret1=cc.sequence(spa1,spa2,spa3,spa4);
//			ret1.retain();
//			//螺线管基础状态动作
//			var seq=cc.sequence(rot,cc.delayTime(1),cc.spawn(cc.spawn(cc.callFunc(function(){
//				this.nail9.runAction(cc.moveTo(1, cc.p(30,50)));	
//			},this),cc.callFunc(function(){		
//				this.nail8.runAction(cc.moveTo(1, cc.p(-40,50)));
//			},this)),cc.callFunc(function(){
//				this.neddle10.runAction(cc.repeatForever(ret));
//				this.neddle9.runAction(cc.repeatForever(ret1));
//
//			},this)));
//			this.start7.runAction(seq);
//			//螺线管电流反向
//			var rot1=cc.rotateTo(1.5,10);
//			spa12=cc.spawn(cc.rotateTo(0.5,90),cc.scaleTo(0.5,0.7));
//			spa22=cc.spawn(cc.rotateTo(0.5,180),cc.scaleTo(0.5,1));
//			spa32=cc.spawn(cc.rotateTo(0.5,90),cc.scaleTo(0.5,0.7));
//			spa42=cc.spawn(cc.rotateTo(0.5,0),cc.scaleTo(0.5,1));
//			//右边磁针
//			spa52=cc.spawn(cc.rotateTo(0.5,15),cc.scaleTo(0.5,0.9));
//			spa62=cc.spawn(cc.rotateTo(0.5,0),cc.scaleTo(0.5,1));
//			spa72=cc.spawn(cc.rotateTo(0.5,-15),cc.scaleTo(0.5,0.9));
//			spa82=cc.spawn(cc.rotateTo(0.5,0),cc.scaleTo(0.5,1));
//			var ret2=cc.sequence(spa52,spa62,spa72,spa82);
//			ret2.retain();
//			var ret3=cc.sequence(spa12,spa22,spa32,spa42);
//			ret3.retain();
//			
//			var seq1=cc.sequence(rot1,cc.delayTime(1),cc.spawn(cc.spawn(cc.callFunc(function(){
//				this.nail10.runAction(cc.moveTo(1, cc.p(40,50)));	
//			},this),cc.callFunc(function(){		
//				this.nail11.runAction(cc.moveTo(1, cc.p(40,50)));
//			},this)),cc.callFunc(function(){
//				this.neddle12.runAction(cc.repeatForever(ret2));
//				this.neddle11.runAction(cc.repeatForever(ret3));
//				this.label5.runAction(cc.fadeTo(1, 255));
//			},this)));
//
//			this.start8.runAction(seq1);
			
			this.start7.runAction(cc.sequence(rot,cc.callFunc(function(){
				this.nail9.runAction(cc.moveTo(1, cc.p(30,50)));	
				this.nail8.runAction(cc.moveTo(1, cc.p(-40,50)));
				this.nail10.runAction(cc.moveTo(1, cc.p(810,50)));
				this.nail11.runAction(cc.moveTo(1, cc.p(820,50)));
				this.label5.runAction(cc.fadeTo(1, 255));
				},this) ));
			break;
			
		case TAG_BUTTON6:
			p.setEnable(false);
			var rot=cc.rotateTo(1.5,10);
			//中间磁针
			spa1=cc.spawn(cc.rotateTo(1.5,90),cc.scaleTo(1.5,0.7));
			spa2=cc.spawn(cc.rotateTo(1.5,180),cc.scaleTo(1.5,1));
			spa3=cc.spawn(cc.rotateTo(1.5,90),cc.scaleTo(1.5,0.7));
			spa4=cc.spawn(cc.rotateTo(1.5,0),cc.scaleTo(1.5,1));
			//右边磁针
			spa5=cc.spawn(cc.rotateTo(1,10),cc.scaleTo(1.5,0.9));
			spa6=cc.spawn(cc.rotateTo(1,0),cc.scaleTo(1.5,1));
			spa7=cc.spawn(cc.rotateTo(1,-10),cc.scaleTo(1.5,0.9));
			spa8=cc.spawn(cc.rotateTo(1,0),cc.scaleTo(1.5,1));
			var ret=cc.sequence(spa5,spa6,spa7,spa8);
			ret.retain();
			var ret1=cc.sequence(spa1,spa2,spa3,spa4);
			ret1.retain();
			//螺线管基础状态动作
			var seq=cc.sequence(rot,cc.delayTime(1),cc.spawn(cc.spawn(cc.callFunc(function(){
				this.nail13.runAction(cc.moveTo(1, cc.p(30,50)));	
			},this),cc.callFunc(function(){		
				this.nail12.runAction(cc.moveTo(1, cc.p(-40,50)));
			},this)),cc.callFunc(function(){
				this.neddle14.runAction(cc.repeatForever(ret));
				this.neddle13.runAction(cc.repeatForever(ret1));

			},this)));
			this.start9.runAction(seq);
			//螺线管电流反向
			var rot1=cc.rotateTo(1.5,10);
			spa12=cc.spawn(cc.rotateTo(0.5,90),cc.scaleTo(0.5,0.7));
			spa22=cc.spawn(cc.rotateTo(0.5,180),cc.scaleTo(0.5,1));
			spa32=cc.spawn(cc.rotateTo(0.5,90),cc.scaleTo(0.5,0.7));
			spa42=cc.spawn(cc.rotateTo(0.5,0),cc.scaleTo(0.5,1));
			//右边磁针
			spa52=cc.spawn(cc.rotateTo(0.5,15),cc.scaleTo(0.5,0.9));
			spa62=cc.spawn(cc.rotateTo(0.5,0),cc.scaleTo(0.5,1));
			spa72=cc.spawn(cc.rotateTo(0.5,-15),cc.scaleTo(0.5,0.9));
			spa82=cc.spawn(cc.rotateTo(0.5,0),cc.scaleTo(0.5,1));
			
			var ret2=cc.sequence(spa52,spa62,spa72,spa82);
			ret2.retain();
			var ret3=cc.sequence(spa12,spa22,spa32,spa42);
			ret3.retain();
			var seq1=cc.sequence(rot1,cc.delayTime(1),cc.spawn(cc.spawn(cc.callFunc(function(){
				this.nail14.runAction(cc.moveTo(1, cc.p(40,50)));	
			},this),cc.callFunc(function(){		
				this.nail15.runAction(cc.moveTo(1, cc.p(40,50)));
			},this)),cc.callFunc(function(){
				this.neddle16.runAction(cc.repeatForever(ret2));
				this.neddle15.runAction(cc.repeatForever(ret3));
				this.label6.runAction(cc.fadeTo(1, 255));
			},this)));

			this.start10.runAction(seq1);
			break;
		case TAG_MENU1:
			gg.runNext(p.getTag());
			break;
		}
	}

});
