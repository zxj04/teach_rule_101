exp01.ShowLayer01 = cc.Layer.extend({
	backgroundLayer : null,
	mainLayar : null,
	ctor:function (menuTag) {
		this._super();
		this.initFrames();
		this.loadBackground(menuTag);
		this.loadMainLayer();
	},
	initFrames : function(){
		cc.spriteFrameCache.addSpriteFrames(res_show01.show_p);
		gg.curRunSpriteFrame.push(res_show01.show_p);
	},
	loadBackground : function(menuTag){
		this.backgroundLayer = new ShowBackgroundLayer(menuTag);
		this.addChild(this.backgroundLayer);
	},
	loadMainLayer : function(){
		this.mainLayar = new exp01.ShowMainLayer01();
		this.addChild(this.mainLayar);
	}
});
exp01.ShowScene01 = PScene.extend({
	ctor:function(menuTag){
		this._super();
		this.menuTag=menuTag;
	},
	onEnter:function () {
		this._super();
		var layer = new exp01.ShowLayer01(this.menuTag);
		this.addChild(layer);
	}
});
