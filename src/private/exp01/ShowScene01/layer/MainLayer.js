exp01.ShowMainLayer01 = cc.Layer.extend({
	lead:null,
	real:null,
	ctor:function () {
		this._super();
		this.init();
		return true;
	},
	init:function(){//
		var sv = new pub.ScrollView(this);
		sv.setOffestPos(cc.p(152,45 + 15));

		this.addlayout1(sv);	
		
//		this.loadNextButton();
	},
	loadNextButton:function(){		
		var label = new Label(this,"生活应用：电磁起重机>>",this.callback);
		label.setLocalZOrder(5);
		label.setTag(TAG_MENU2);
		label.setAnchorPoint(1, 0);
		label.setColor(cc.color(19, 98, 27, 250));
		label.setPosition(gg.width - 40,25);
	},
	addlayout1:function(parent){
		var layout1 = new ccui.Layout();//第一页
		layout1.setContentSize(cc.size(996, 598));//设置layout的大小
		parent.addLayer(layout1);
		
		var text1 = $.format("　　安培定则，也叫右手螺旋定则，是表示电流和电流激发磁场的磁感线方向" +
				"间关系的定则。" +
				"\n\n　　" +
				"通电直导线中的安培定则（安培定则一）：用右手握住通电直导线，让大拇指指向电流的方向，那么四指的指向就是磁感线的环绕方向。", 980,30);
		var label1 = new cc.LabelTTF(text1,gg.fontName,gg.fontSize4);
		label1.setColor(cc.color(0, 0, 0, 250));
		label1.setPosition(gg.c_width,gg.height-label1.height/2 - 130);
		this.addChild(label1);	

		var sprite = new cc.Sprite("#anpei.png"); //牛顿
		sprite.setPosition(375,265);
		sprite.setScale(0.7);
		this.addChild(sprite);

		var text2 = $.format("　　通电螺线管中的安培定则（安培定则二）：用右手握住通电螺线管，让四指指向电流" +
				"的方向，那么大拇指所指的那一端是通电螺线管的N极。", 510,30);
		label2 = new cc.LabelTTF(text2,gg.fontName,gg.fontSize4);
		this.addChild(label2);
		label2.setAnchorPoint(1,1);
		label2.setColor(cc.color(0, 0, 0, 250));
		label2.setPosition(1125,400);
	},
	callback:function(p){		
		switch(p.getTag()){
		case TAG_MENU2:
			gg.runNext(p.getTag());
			break;
		}
	}

});
