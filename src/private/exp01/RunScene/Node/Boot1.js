Boot1 = cc.Node.extend({
	ctor:function(p){
		this._super();
		p.addChild(this, 10, TAG_BOOT_NODE1);
		this.init();
	},
	init : function(){
		this.setCascadeOpacityEnabled(true);
		var boot= new Button(this, 10, TAG_BOOT, "#boot.png",this.callback);
		boot.setPosition(-106,-227);
		boot.setScale(0.5);

		var chudian= new Button(this, 11, TAG_CHUDIAN, "#chudian.png",this.callback);
		chudian.setPosition(75,202);
		chudian.setScale(0.5);

		this.daoxian= new Button(this, 10, TAG_DAOXIAN, "#daoxian1.png",this.callback);
		this.daoxian.setPosition(120,-50);
		this.daoxian.setScale(0.5);
		this.daoxian.setVisible(false);
		
		this.daoxian1= new Button(this, 10, TAG_DAOXIAN, "#citixian2.png",this.callback);
		this.daoxian1.setPosition(-65,60);
		this.daoxian1.setScale(0.5)

		this.dengpao= new Button(this, 9, TAG_DENGPAO, "#dengpao.png",this.callback);
		this.dengpao.setPosition(285,-25);
		this.dengpao.setScale(0.5);

		this.dengpao1= new Button(this, 9, TAG_DENGPAO1, "#dengpao.png",this.callback);
		this.dengpao1.setPosition(298,243);
		this.dengpao1.setScale(0.5);

		var dianji= new Button(this, 9, TAG_DIANJI, "#dianji.png",this.callback);
		dianji.setPosition(507,-40);
		dianji.setScale(0.5);

		var dianji1= new Button(this, 11, TAG_DIANJI1, "#dianji1.png",this.callback);
		dianji1.setPosition(507,-40);
		dianji1.setScale(0.5);

		this.shanye= new Button(this, 10, TAG_SHANYE, "#shanye.png",this.callback);
		this.shanye.setPosition(507,-52);
		this.shanye.setScale(0.5);

		var citi= new Button(this, 9, TAG_CITI, "#citi.png",this.callback);
		citi.setPosition(-103,102);
		citi.setScale(0.5);

		var dianyuan= new Button(this, 9, TAG_DIANYUAN, "#dianyuan1.png",this.callback);
		dianyuan.setPosition(-300,-235);
		dianyuan.setScale(0.5);

		this.tanpian= new Button(this, 10, TAG_TANPIAN, "#tanpian3.png",this.callback);
		this.tanpian.setPosition(-75,113);
		this.tanpian.setScale(0.5);	
		
		var gaoya = new cc.Sprite("#gaoya.png");
		gaoya.setPosition(325,-310);
		this.addChild(gaoya);
		gaoya.setScale(0.5);

		this.label = new cc.LabelTTF("高压电源","微软雅黑",25);
		this.label.setPosition(322,-235);
		this.label.setColor(cc.color(0,0,0));
		this.addChild(this.label);
		
		var quan = new Button(this, 10, TAG_QUAN, "#quan.png",this.callback);
		quan.setPosition(155-157,135 +2);
		quan.setScale(0.5);

		var quan1 = new Button(this, 10, TAG_QUAN1, "#quan.png",this.callback);
		quan1.setPosition(-21,57);
		quan1.setScale(0.5);

		var quan2 = new Button(this, 10, TAG_QUAN2, "#quan.png",this.callback);
		quan2.setPosition(98,234);
		quan2.setScale(0.5);

		var quan3 = new Button(this, 10, TAG_QUAN3, "#quan.png",this.callback);
		quan3.setPosition(98,166);
		quan3.setScale(0.5);

		var quan4 = new Button(this, 10, TAG_QUAN4, "#quan.png",this.callback);
		quan4.setPosition(480,16);
		quan4.setScale(0.5);

		var quan5 = new Button(this, 10, TAG_QUAN5, "#quan.png",this.callback);
		quan5.setPosition(540,16);
		quan5.setScale(0.5);

		var quan6 = new Button(this, 10, TAG_QUAN6, "#quan.png",this.callback);
		quan6.setPosition(30,200);
		quan6.setScale(0.5);
		
		var quan7 = new Button(this, 10, TAG_QUAN7, "#quan.png",this.callback);
		quan7.setPosition(270,-293);
		quan7.setScale(0.5);
		
		var quan8 = new Button(this, 10, TAG_QUAN8, "#quan.png",this.callback);
		quan8.setPosition(378,-293);
		quan8.setScale(0.5);

		var dred = new Button(this, 10, TAG_DRED, "#red1.png",this.callback);
		dred.setPosition(332,214);
		dred.setScale(0.5);

		var dred1 = new Button(this, 10, TAG_DRED1, "#red1.png",this.callback);
		dred1.setPosition(319,-54);
		dred1.setScale(0.5);
		
		var dred2 = new Button(this, 10, TAG_DRED2, "#red1.png",this.callback);
		dred2.setPosition(-83,-237);
		dred2.setScale(0.5);

		var dblack = new Button(this, 10, TAG_DBLACK, "#black1.png",this.callback);
		dblack.setPosition(260,214);
		dblack.setScale(0.5);

		var dblack1 = new Button(this, 10, TAG_DBLACK1, "#black1.png",this.callback);
		dblack1.setPosition(247,-55);
		dblack1.setScale(0.5);
		
		var dblack2 = new Button(this, 10, TAG_DBLACK2, "#black1.png",this.callback);
		dblack2.setPosition(-146,-237);
		dblack2.setScale(0.5);

		var red = new Button(this, 10, TAG_RED, "#red.png",this.callback);
		red.setPosition(-265,-200);
		red.setScale(0.5);

		var blue = new Button(this, 10, TAG_BLUE, "#blue.png",this.callback);
		blue.setPosition(-335,-200);
		blue.setScale(0.5);		
	},
	qiaoda:function(){
		var frames=[];
		for (i=1;i<=2;i++){
			var str ="tanpian"+i+".png";
			var frame=cc.spriteFrameCache.getSpriteFrame(str);
			frames.push(frame);
		}
		var animation = new cc.Animation(frames,0.05);//负责动画序列
		var action = new cc.Animate(animation);//帧动画的动作创建
		return action;	
	},
	Line:function(tag,line,time,pos,scalex,rotation){
		var line = new cc.Sprite(line);
		line.setPosition(pos);
		line.setAnchorPoint(1,0.5);
		line.setScale(0.5,0.5);
		line.setRotation(rotation);
		this.addChild(line,10,tag);
		line.runAction(cc.scaleTo(time,scalex,0.5));
	},
	firstPoint:function(obj){
		var seq = cc.sequence(cc.scaleTo(0.2,0.8),cc.scaleTo(0.2,0.5));
		obj.runAction(seq.repeatForever());
		this.flowNext();
	},
	stopScale:function(tag){
		this.getChildByTag(tag).setScale(0.5);
		this.getChildByTag(tag).stopAllActions();
	},
	callback:function(p){
		var action = gg.flow.flow.action;
		var func = cc.callFunc(this.actionDone, this);	
		switch(p.getTag()){	
		case TAG_QUAN:
		case TAG_QUAN1:
		case TAG_DBLACK2:
		case TAG_QUAN2:
		case TAG_QUAN3:
		case TAG_DRED:
		case TAG_DRED1:
		case TAG_QUAN5:
		case TAG_QUAN6:
			this.firstPoint(p);
			break;
		case TAG_BLUE:
			this.stopScale(TAG_QUAN);
			var seq = cc.sequence(cc.callFunc(function(){
				this.Line(TAG_LINE1,"#line1.png",2,cc.p(-2,139),10.1,-90);
			},this),cc.delayTime(2),cc.callFunc(function(){
				this.Line(TAG_LINE2,"#line1.png",2,cc.p(-2,-292),8.62,0);
			},this),cc.delayTime(2),cc.callFunc(function(){
				this.Line(TAG_LINE3,"#line1.png",0.5,cc.p(-372,-292),2,90);
			},this),cc.delayTime(0.5),cc.callFunc(function(){
				this.Line(TAG_LINE4,"#line1.png",0.5,cc.p(-372,-207),1,180);
			},this),cc.delayTime(0.5),cc.callFunc(function() {
				gg.teach_type = TAG_LEAD;
				gg.teach_flow = exp01.teach_flow01;
				pub.ch.gotoRun(TAG_EXP_01, tag, g_resources_run01, null, new exp01.RunScene02(tag));
				
			},this));
			p.runAction(seq);
		break;
		case TAG_DRED2:
			this.stopScale(TAG_QUAN1);
			var seq = cc.sequence(cc.callFunc(function(){
				this.Line(TAG_LINE5,"#line1.png",1.5,cc.p(-20,58),7.05,-90);
			},this),cc.delayTime(1.5),cc.callFunc(function(){
				this.Line(TAG_LINE6,"#line1.png",0.5,cc.p(-20,-244),1.5,0);
			},this),cc.delayTime(0.5),cc.callFunc(this.flowNext,this));
			p.runAction(seq);
			break;
		case TAG_RED:
			this.stopScale(TAG_DBLACK2);
			var seq = cc.sequence(cc.callFunc(function(){
				this.Line(TAG_LINE7,"#line1.png",0.5,cc.p(-150,-244),2.05,0);
			},this),cc.delayTime(0.5),cc.callFunc(function(){
				this.Line(TAG_LINE8,"#line1.png",0.3,cc.p(-238,-244),0.87,90);
			},this),cc.delayTime(0.3),cc.callFunc(function(){
				this.Line(TAG_LINE9,"#line1.png",0.3,cc.p(-238,-207),0.8,0);
			},this),cc.delayTime(0.3),cc.callFunc(this.flowNext,this));
			p.runAction(seq);
			break;
		case TAG_DBLACK:
			this.stopScale(TAG_QUAN2);
			var seq = cc.sequence(cc.callFunc(function(){
				this.Line(TAG_LINE10,"#line1.png",0.5,cc.p(100,235),2.1,-180);
			},this),cc.delayTime(0.5),cc.callFunc(function(){
				this.Line(TAG_LINE11,"#line1.png",0.3,cc.p(189,235),0.6,-90);
			},this),cc.delayTime(0.3),cc.callFunc(function(){
				this.Line(TAG_LINE12,"#line1.png",0.3,cc.p(189,209),1.7,-180);
			},this),cc.delayTime(0.3),cc.callFunc(this.flowNext,this));
			p.runAction(seq);
			break;
		case TAG_QUAN8:
			if(action == ACTION_DO1){
				this.stopScale(TAG_DRED);
				var seq = cc.sequence(cc.callFunc(function(){
					this.Line(TAG_LINE13,"#line1.png",1,cc.p(330,209),6.64,-180);
				},this),cc.delayTime(1),cc.callFunc(function(){
					this.Line(TAG_LINE14,"#line1.png",2,cc.p(615,209),11.7,-90);
				},this),cc.delayTime(2),cc.callFunc(function(){
					this.Line(TAG_LINE15,"#line1.png",0.8,cc.p(615,-291),5.5,0);
				},this),cc.delayTime(0.8),cc.callFunc(this.flowNext,this));
				p.runAction(seq);
			}else if(action == ACTION_DO2){
				this.stopScale(TAG_QUAN5);
				var seq = cc.sequence(cc.callFunc(function(){
					this.Line(TAG_LINE16,"#line1.png",0.5,cc.p(540,17),1.75,180);
				},this),cc.delayTime(0.5),cc.callFunc(function(){
					this.point = new cc.Sprite("#blackpoint.png");
					this.point.setPosition(615,15);
					this.point.setScale(0.5);
					this.addChild(this.point,10);				
				},this),cc.callFunc(this.flowNext,this));
				p.runAction(seq);
			}			
			break;
		case TAG_DBLACK1:
			this.stopScale(TAG_QUAN3);
			var seq = cc.sequence(cc.callFunc(function(){
				this.Line(TAG_LINE17,"#line1.png",0.5,cc.p(100,167),1.65,-180);
			},this),cc.delayTime(0.5),cc.callFunc(function(){
				this.Line(TAG_LINE18,"#line1.png",0.8,cc.p(169,167),5.3,-90);
			},this),cc.delayTime(0.8),cc.callFunc(function(){
				this.Line(TAG_LINE19,"#line1.png",0.3,cc.p(169,-60),1.8,-180);
			},this),cc.delayTime(0.3),cc.callFunc(this.flowNext,this));
			p.runAction(seq);
			break;
		case TAG_QUAN4:
			this.stopScale(TAG_DRED1);
			var seq = cc.sequence(cc.callFunc(function(){
				this.Line(TAG_LINE20,"#line1.png",0.5,cc.p(315,-60),2.8,180);
			},this),cc.delayTime(0.5),cc.callFunc(function(){
				this.Line(TAG_LINE21,"#line1.png",0.3,cc.p(435,-60),1.8,90);
			},this),cc.delayTime(0.3),cc.callFunc(function(){
				this.Line(TAG_LINE22,"#line1.png",0.3,cc.p(435,15),1,-180);
			},this),cc.delayTime(0.3),cc.callFunc(this.flowNext,this));
			p.runAction(seq);
			break;
		case TAG_QUAN7:
			this.stopScale(TAG_QUAN6);
			var seq = cc.sequence(cc.callFunc(function(){
				this.Line(TAG_LINE23,"#line1.png",2,cc.p(30,200),11.5,-90);
			},this),cc.delayTime(2),cc.callFunc(function(){
				this.Line(TAG_LINE24,"#line1.png",1,cc.p(30,-291),5.58,-180);
			},this),cc.delayTime(1),cc.callFunc(function(){
				this.getChildByTag(TAG_LINE10).setSpriteFrame("line2.png");
				this.getChildByTag(TAG_LINE11).setSpriteFrame("line2.png");
				this.getChildByTag(TAG_LINE12).setSpriteFrame("line2.png");
				this.getChildByTag(TAG_LINE13).setSpriteFrame("line2.png");
				this.getChildByTag(TAG_LINE14).setSpriteFrame("line2.png");
				this.getChildByTag(TAG_LINE15).setSpriteFrame("line2.png");
				this.getChildByTag(TAG_LINE23).setSpriteFrame("line2.png");
				this.getChildByTag(TAG_LINE24).setSpriteFrame("line2.png");
				this.dengpao1.setSpriteFrame("dengpao1.png");
				this.flowNext();
			},this));
			p.runAction(seq);
			break;
		case TAG_BOOT:
			this.daoxian.setVisible(true);
			if(action==ACTION_DO1){
				for (i=0;i<24;i++){
					var TAG_LINE = TAG_LINE1 + i;
					this.getChildByTag(TAG_LINE).setVisible(false);					
				}
				this.getChildByTag(TAG_QUAN).setVisible(false);	
				this.getChildByTag(TAG_QUAN1).setVisible(false);
				this.getChildByTag(TAG_QUAN2).setVisible(false);
				this.getChildByTag(TAG_QUAN3).setVisible(false);
				this.getChildByTag(TAG_QUAN4).setVisible(false);
				this.getChildByTag(TAG_QUAN5).setVisible(false);
				this.getChildByTag(TAG_QUAN6).setVisible(false);
				this.daoxian1.setVisible(false);
				
				this.showTip1 = new ShowTip01("电磁铁通电" +
						"\n吸引衔铁",cc.p(480,650),TAG_SHOW);
				this.showTip2 = new ShowTip01("高压电路工作",cc.p(940,230),TAG_SHOW1);
				p.setSpriteFrame("boot1.png");
				this.tanpian.setSpriteFrame("tanpian4.png");
				this.daoxian.setSpriteFrame("daoxian2.png");
				this.dengpao.setSpriteFrame("dengpao2.png");
				this.dengpao1.setSpriteFrame("dengpao.png");

				this.shanye.runAction((cc.rotateBy(0.2,360).repeatForever()));
				var seq = cc.sequence(cc.delayTime(5),cc.callFunc(this.flowNext,this));
				p.runAction(seq);
			}else if(action==ACTION_DO2){
				this.showTip1 = new ShowTip01("电磁铁断电" +
						"\n衔铁复原",cc.p(480,650),TAG_SHOW);
				this.showTip2.removeFromParent(true);
				this.showTip2 = new ShowTip01("高压电路停止工作",cc.p(940,230),TAG_SHOW1);		
				p.setSpriteFrame("boot.png");
				this.tanpian.setSpriteFrame("tanpian3.png");
				this.daoxian.setSpriteFrame("daoxian1.png");
				this.dengpao.setSpriteFrame("dengpao.png");
				this.dengpao1.setSpriteFrame("dengpao1.png");

				this.shanye.stopAllActions();
				var seq1 = cc.sequence(cc.rotateBy(0.4,360),cc.rotateBy(0.6,360),cc.rotateBy(0.8,360),cc.rotateBy(1,360),cc.rotateBy(1.2,360),cc.rotateBy(1.4,360));
				this.shanye.runAction(seq1);

				var seq = cc.sequence(cc.delayTime(6),cc.callFunc(this.flowNext,this));
				p.runAction(seq);
			}
		}
	},
	flowNext:function(){
		gg.flow.next();
	},
});