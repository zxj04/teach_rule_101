exp01.ShowMainLayer03 = cc.Layer.extend({
	lead:null,
	real:null,
	ctor:function () {
		this._super();
		this.init();
		return true;
	},
	init:function(){
		var sv = new pub.ScrollView(this);
		sv.setOffestPos(cc.p(152,45 + 15));

	//	this.addlayout1(sv);
		this.addlayout2(sv);
		this.addlayout3(sv);
//		this.loadNextButton();
	},	
	loadNextButton:function(){		
		var label = new Label(this,"1.2电生磁>>",this.callback);
		label.setLocalZOrder(5);
		label.setTag(TAG_EXP_01);
		label.setAnchorPoint(1, 0);
		label.setColor(cc.color(19, 98, 27, 250));
		label.setPosition(gg.width - 40,25);
	},
	addlayout1:function(parent){
		var layout1 = new ccui.Layout();//第一页
		layout1.setContentSize(cc.size(996, 598));//设置layout的大小
		parent.addLayer(layout1);

		var text1 = $.format("　　当要求物体运动速度的大小或方向发生变化时，都必须对物体施加力的作用。" +
				"物体速度大小的变化或方向的变化，称之为物体运动状态的改变。", 980,30);
		label1 = new cc.LabelTTF(text1,gg.fontName,gg.fontSize4);
		label1.setColor(cc.color(0, 0, 0, 250));
		label1.setPosition(500,layout1.height - label1.height/2 );
		layout1.addChild(label1);	

		var muban = new cc.Sprite("#muban.png"); //木板
		muban.setPosition(235,350);
		muban.setScale(0.65);
		layout1.addChild(muban);

		var qiu = new cc.Sprite("#qiu.png"); //A点球
		qiu.setPosition(180,230);
		muban.addChild(qiu);

		this.qiu = new cc.Sprite("#qiu.png"); //A点球
		this.qiu.setPosition(180,230);
		muban.addChild(this.qiu);

		this.label3 = new cc.LabelTTF("A",gg.fontName,gg.fontSize4);
		muban.addChild(this.label3);
		this.label3.setPosition(170,200);

		this.line1 = new cc.Sprite("#line1.png"); //直线
		this.line1.setPosition(390,225);
		this.line1.setVisible(false);
		muban.addChild(this.line1);

		this.qiu1 = new cc.Sprite("#qiu.png"); //B点球
		this.qiu1.setPosition(600,230);
		this.qiu1.setVisible(false);
		muban.addChild(this.qiu1);

		this.label4 = new cc.LabelTTF("B",gg.fontName,gg.fontSize4);
		this.label4.setVisible(false);
		muban.addChild(this.label4);
		this.label4.setPosition(580,210);

		this.line2 = new cc.Sprite("#line2.png"); //曲线
		this.line2.setPosition(360,185);
		this.line2.setVisible(false);
		muban.addChild(this.line2);

		this.qiu2 = new cc.Sprite("#qiu.png"); //C点球
		this.qiu2.setPosition(545,135);
		this.qiu2.setVisible(false);
		muban.addChild(this.qiu2);

		this.label5 = new cc.LabelTTF("C",gg.fontName,gg.fontSize4);
		this.label5.setVisible(false);
		muban.addChild(this.label5);
		this.label5.setPosition(525,110);

		this.citie = new cc.Sprite("#citie.png"); //citie
		this.citie.setPosition(360,100);
		this.citie.setVisible(false);
		muban.addChild(this.citie);

//		this.button = new ButtonScale(this,"#unsel.png",this.callback);
//		this.button.setPosition(350,200);
//		var label = new cc.LabelTTF("播放",gg.fontName,30);
//		label.setColor(cc.color(0, 0, 0, 250));
//		label.setPosition(this.button.width/2,this.button.height/2);
//		this.button.addChild(label);
		
		this.addbutton(layout1, TAG_BUTTON, "播放", cc.p(350,200));

		var text2 = $.format("　　光滑的水平桌面上运动的小球，在不受外力作用时，沿直线从A点运动到B点。" +
				"若在其原先运动轨迹的侧面放置一条形磁铁，小球会向磁铁方向偏转，小球的运动方向发生改变" +
				"这是因为小球受到与其运动方向不同的侧向磁铁吸引力的缘故。", 510,30);
		label2 = new cc.LabelTTF(text2,gg.fontName,gg.fontSize4);
		layout1.addChild(label2);
		label2.setColor(cc.color(0, 0, 0, 250));
		label2.setAnchorPoint(1,1);
		label2.setPosition(990,440);

	},
	addlayout2:function(parent){
		this.layout2 = new ccui.Layout();//第二页
		this.layout2.setContentSize(cc.size(996, 598));
		parent.addLayer(this.layout2);

		label1 = new cc.LabelTTF("　　要使电磁铁的两个磁极对调,可采取的方法是()" ,gg.fontName,gg.fontSize4);
		label1.setAnchorPoint(0,0.5);
		label1.setPosition(0,this.layout2.height-label1.height/2);
		label1.setColor(cc.color(0, 0, 0, 250));
		this.layout2.addChild(label1);	

		this.addimage(this.layout2, "#select.png", TAG_BUTTON2, cc.p(60,480), "A.改变电流方向", TAG_LABEL, cc.p(180,480));
		this.sel = new cc.Sprite("#right.png");
		this.sel.setPosition(15,15);
		this.layout2.getChildByTag(TAG_BUTTON2).addChild(this.sel);
		this.sel.setVisible(false);

		this.addimage(this.layout2, "#select.png", TAG_BUTTON3, cc.p(60,430), "B.增加螺线管的匝数", TAG_LABEL, cc.p(204,430));
		this.sel1 = new cc.Sprite("#wrong.png");
		this.sel1.setPosition(15,15);
		this.layout2.getChildByTag(TAG_BUTTON3).addChild(this.sel1);
		this.sel1.setVisible(false);

		this.addimage(this.layout2, "#select.png", TAG_BUTTON4, cc.p(60,380), "C.减小电流", TAG_LABEL, cc.p(155,380));
		this.sel2 = new cc.Sprite("#wrong.png");
		this.sel2.setPosition(15,15);
		this.layout2.getChildByTag(TAG_BUTTON4).addChild(this.sel2);
		this.sel2.setVisible(false);

		this.addimage(this.layout2, "#select.png", TAG_BUTTON5, cc.p(60,330), "D.将铁芯从螺线管中拔出", TAG_LABEL, cc.p(230,330));
		this.sel3 = new cc.Sprite("#wrong.png");
		this.sel3.setPosition(15,15);
		this.layout2.getChildByTag(TAG_BUTTON5).addChild(this.sel3);
		this.sel3.setVisible(false);

		this.label5 = new cc.LabelTTF("　　分析：电磁铁的磁极与通过电磁铁电流的方向和电磁铁中导线的绕线"+
				"\n\t\t\t\t\t\t\t\t\t\t\t\t方向有关．要想改变电磁铁的磁极,可以不改变电流的方向来改"+
				"\n\t\t\t\t\t\t\t\t\t\t\t\t变电磁铁导线的绕线方向；或者在电磁铁的导线绕线方向不变"+
				"\n\t\t\t\t\t\t\t\t\t\t\t\t来改变通过电磁铁的电流方向．与螺线管的匝数、电流的大小、"+
				"\n\t\t\t\t\t\t\t\t\t\t\t\t是否有铁芯无关．所以B、C、D三项不符合题意。" +
				"\n　　点评：此题主要考查了决定电磁铁磁极的因素．明确电磁铁的磁极是"+
				                      "\n\t\t\t\t\t\t\t\t\t\t\t\t由通过电磁铁的电流方向和电磁铁的线圈绕线方向决定是解答"+
				                      "\n\t\t\t\t\t\t\t\t\t\t\t\t问题的关键．",gg.fontName,gg.fontSize4);
		this.label5.setAnchorPoint(0,0.5);
		this.label5.setPosition(0,150);
		this.label5.setColor(cc.color(0, 0, 0, 250));
		this.label5.setVisible(false);
		this.layout2.addChild(this.label5);		
	},
	addlayout3:function(parent){
		this.layout3 = new ccui.Layout();//第二页
		this.layout3.setContentSize(cc.size(996, 598));
		parent.addLayer(this.layout3);

		label1 = new cc.LabelTTF("\t\t\t\t法国科学家阿尔贝•费尔和德国科学家彼得•格林贝格尔由于发现了巨磁电" +
				"\n阻（GMR）效应，荣获了2007年诺贝尔物理学奖.如图是研究巨磁电阻特性" +
				"\n的原理示意图，实验发现，当闭合S1、S2后使滑片P向左滑动过程中，指示" +
				"\n灯明显变亮；下列说法正确的是()" ,gg.fontName,gg.fontSize4);
		label1.setAnchorPoint(0,0.5);
		label1.setPosition(0,525.5);
		label1.setColor(cc.color(0, 0, 0, 250));
		this.layout3.addChild(label1);	

		this.addimage(this.layout3, "#select.png", TAG_BUTTON6, cc.p(60,400), "A.电磁铁右端为N级", TAG_LABEL, cc.p(205,400));
		this.sel4 = new cc.Sprite("#wrong.png");
		this.sel4.setPosition(15,15);
		this.layout3.getChildByTag(TAG_BUTTON6).addChild(this.sel4);
		this.sel4.setVisible(false);

		this.addimage(this.layout3, "#select.png", TAG_BUTTON7, cc.p(60,350), "B.滑片P向左滑动过程中电磁铁的磁性减弱", TAG_LABEL, cc.p(325,350));
		this.sel5 = new cc.Sprite("#wrong.png");
		this.sel5.setPosition(15,15);
		this.layout3.getChildByTag(TAG_BUTTON7).addChild(this.sel5);
		this.sel5.setVisible(false);

		this.addimage(this.layout3, "#select.png", TAG_BUTTON8, cc.p(60,300), "C.巨磁电阻的阻值随磁场的增强而明显增大", TAG_LABEL, cc.p(330,300));
		this.sel6 = new cc.Sprite("#wrong.png");
		this.sel6.setPosition(15,15);
		this.layout3.getChildByTag(TAG_BUTTON8).addChild(this.sel6);
		this.sel6.setVisible(false);

		this.addimage(this.layout3, "#select.png", TAG_BUTTON9, cc.p(60,250), "D.巨磁电阻的阻值随磁场的增强而明显减小", TAG_LABEL, cc.p(330,250));
		this.sel7 = new cc.Sprite("#right.png");
		this.sel7.setPosition(15,15);
		this.layout3.getChildByTag(TAG_BUTTON9).addChild(this.sel7);
		this.sel7.setVisible(false);
		
		this.sel8 = new cc.Sprite("#titu.png");
		this.sel8.setPosition(692,400);
		this.layout3.addChild(this.sel8);

		this.label6 = new cc.LabelTTF("　　分析：滑片P向左滑动过程中，电阻变小，电流变大，通电螺线管的磁"+
				"\n\t\t\t\t\t\t\t\t\t\t\t\t性增强，而指示灯明显变亮，说明右边电路的电流变大，巨磁"+
				"\n\t\t\t\t\t\t\t\t\t\t\t\t电阻的电阻变小，所以D的说法正确。"+
			
				"\n　　考点：此题主要考查了电生磁和电磁铁。",gg.fontName,gg.fontSize4);
		this.label6.setAnchorPoint(0,0.5);
		this.label6.setPosition(0,130);
		this.label6.setColor(cc.color(0, 0, 0, 250));
		this.label6.setVisible(false);
		this.layout3.addChild(this.label6);		
	},
	addimage:function(parent,str,tag,pos,str2,tag2,pos2){
		var image = new ButtonScale(parent,str,this.callback,this);
		image.setPosition(pos);
		image.setTag(tag);
		var label = new cc.LabelTTF(str2,gg.fontName,gg.fontSize);
		label.setColor(cc.color(0, 0, 0, 250));
		parent.addChild(label);
		label.setPosition(pos2);
		label.setTag(tag2);
	},
	addbutton:function(parent,tag,str,pos){
		var button = new ButtonScale(parent,"#unsel.png",this.callback,this);
		button.setPosition(pos);
		button.setLocalZOrder(5);
		button.setTag(tag);
		var label = new cc.LabelTTF(str,gg.fontName,30);
		label.setPosition(button.width/2,button.height/2);		
		label.setColor(cc.color(0, 0, 0, 250));
		button.addChild(label);
	},
	callback:function(p){
		switch(p.getTag()){		
		case TAG_BUTTON:
			p.setEnable(false);
			//p.setSpriteFrame("sel.png");
			var move = cc.moveTo(1.5,cc.p(600,230));
			var ber = cc.bezierTo(1.5, [cc.p(360,228),cc.p(460,160),cc.p(545,135)]);
			var seq = cc.sequence(cc.callFunc(function(){
				this.line1.setVisible(false);
				this.qiu1.setVisible(false);
				this.label4.setVisible(false);
				this.line2.setVisible(false);
				this.qiu2.setVisible(false);
				this.label5.setVisible(false);
				this.citie.setVisible(false);
			},this),move,cc.callFunc(function(){
				this.line1.setVisible(true);
				this.qiu1.setVisible(true);
				this.label4.setVisible(true);
				this.qiu.setPosition(180,230);
			},this),cc.delayTime(0.5),cc.callFunc(function(){
				this.citie.setVisible(true);
			},this),ber,cc.callFunc(function(){
				this.line2.setVisible(true);
				this.qiu2.setVisible(true);
				this.label5.setVisible(true);
				this.qiu.setPosition(180,230);
				p.setEnable(true);
				//p.setSpriteFrame("unsel.png");
			},this));
			this.qiu.runAction(seq);
			break;
		case TAG_BUTTON2:
		case TAG_BUTTON3:
		case TAG_BUTTON4:
		case TAG_BUTTON5:
			this.sel.setVisible(true);
			this.sel1.setVisible(true);
			this.sel2.setVisible(true);
			this.sel3.setVisible(true);
			this.label5.setVisible(true);
			break;
		case TAG_BUTTON6:
		case TAG_BUTTON7:
		case TAG_BUTTON8:
		case TAG_BUTTON9:
			this.sel4.setVisible(true);
			this.sel5.setVisible(true);
			this.sel6.setVisible(true);
			this.sel7.setVisible(true);
			this.label6.setVisible(true);
			break;
			case TAG_EXP_01:
			pub.ch.run(p.getTag());
			break;

		}
	}
	
});
