exp02.ShowMainLayer02 = cc.Layer.extend({
	curIndex: 0,
	ctor:function () {
		this._super();
		this.init();
		return true;
	},
	init:function(){
		var sv = new pub.ScrollView(this);
		sv.setOffestPos(cc.p(152,45 + 15));
		this.sv = sv;

	//	this.addlayout1(sv);
		this.addlayout2(sv);				
		this.addlayout3(sv);
	//	this.addlayout4(sv);
	},
	addlayout2:function(parent){
		this.layout2 = new ccui.Layout();//第二页
		this.layout2.setContentSize(cc.size(996, 598));
		parent.addLayer(this.layout2);

		var text1 = $.format("\t\t\t\t某同学连接的电铃电路，开关闭合后，电路中始终有电流，但电铃只响一声就不再响了，原因是（）", 1000,30);
		var label1 = new cc.LabelTTF(text1,gg.fontName,gg.fontSize4);

		label1.setAnchorPoint(0,0.5);
		label1.setPosition(0,525.5);
		label1.setColor(cc.color(0, 0, 0, 250));
		this.layout2.addChild(label1);	

		this.addimage(this.layout2, "#select.png", TAG_BUTTON2, cc.p(60,420), "A. 电磁铁始终没有磁性", TAG_LABEL, cc.p(230,420));
		this.sel = new cc.Sprite("#wrong.png");
		this.sel.setPosition(15,15);
		this.layout2.getChildByTag(TAG_BUTTON2).addChild(this.sel);
		this.sel.setVisible(false);

		this.addimage(this.layout2, "#select.png", TAG_BUTTON3, cc.p(60,370), "B. 衔铁没有向下运动", TAG_LABEL, cc.p(215,370));
		this.sel1 = new cc.Sprite("#wrong.png");
		this.sel1.setPosition(15,15);
		this.layout2.getChildByTag(TAG_BUTTON3).addChild(this.sel1);
		this.sel1.setVisible(false);

		this.addimage(this.layout2, "#select.png", TAG_BUTTON4, cc.p(60,320), "C. 衔铁一直被电磁铁吸着不能回弹", TAG_LABEL, cc.p(289,320));
		this.sel2 = new cc.Sprite("#right.png");
		this.sel2.setPosition(15,15);
		this.layout2.getChildByTag(TAG_BUTTON4).addChild(this.sel2);
		this.sel2.setVisible(false);

		this.addimage(this.layout2, "#select.png", TAG_BUTTON5, cc.p(60,270), "D. 电池正、负极接反了", TAG_LABEL, cc.p(227,270));
		this.sel3 = new cc.Sprite("#wrong.png");
		this.sel3.setPosition(15,15);
		this.layout2.getChildByTag(TAG_BUTTON5).addChild(this.sel3);
		this.sel3.setVisible(false);

		var text2 = $.format("\t\t\t\t易错分析：当开关闭合后，电路中有电流，电磁铁有磁性，吸引衔铁，衔铁被吸下的同时，打击铃碗，发出声音．" +
				"此时电路始终接通，电磁铁始终吸引衔铁，衔铁不会回弹，所以钉锤不能在重复打击铃碗．综上分析，故选C。" , 980,30);
		this.label5 = new cc.LabelTTF(text2,gg.fontName,gg.fontSize4);

		this.label5.setPosition(500,130);
		this.label5.setColor(cc.color(0, 0, 0, 250));
		this.label5.setVisible(false);
		this.layout2.addChild(this.label5);		
	},
	addlayout3:function(parent){
		this.layout3 = new ccui.Layout();//第二页
		this.layout3.setContentSize(cc.size(996, 598));
		parent.addLayer(this.layout3);
		var text1 = $.format("\t\t\t\t下列器材中没有应用磁性材料的是（）", 1000,30);
		var label1 = new cc.LabelTTF(text1,gg.fontName,gg.fontSize4);
		label1.setAnchorPoint(0,0.5);
		label1.setPosition(0,525.5);
		label1.setColor(cc.color(0, 0, 0, 250));
		this.layout3.addChild(label1);	

		this.addimage(this.layout3, "#select.png", TAG_BUTTON6, cc.p(60,410), "A.VCD播放器用的光碟", TAG_LABEL, cc.p(230,410));
		this.sel4 = new cc.Sprite("#right.png");
		this.sel4.setPosition(15,15);
		this.layout3.getChildByTag(TAG_BUTTON6).addChild(this.sel4);
		this.sel4.setVisible(false);

		this.addimage(this.layout3, "#select.png", TAG_BUTTON7, cc.p(60,360), "B.电话用的IC卡", TAG_LABEL, cc.p(190,360));
		this.sel5 = new cc.Sprite("#wrong.png");
		this.sel5.setPosition(15,15);
		this.layout3.getChildByTag(TAG_BUTTON7).addChild(this.sel5);
		this.sel5.setVisible(false);

		this.addimage(this.layout3, "#select.png", TAG_BUTTON8, cc.p(60,310), "C.录音机内的录音带", TAG_LABEL, cc.p(215,310));
		this.sel6 = new cc.Sprite("#wrong.png");
		this.sel6.setPosition(15,15);
		this.layout3.getChildByTag(TAG_BUTTON8).addChild(this.sel6);
		this.sel6.setVisible(false);

		this.addimage(this.layout3, "#select.png", TAG_BUTTON9, cc.p(60,260), "D.计算机的软盘", TAG_LABEL, cc.p(190,260));
		this.sel7 = new cc.Sprite("#wrong.png");
		this.sel7.setPosition(15,15);
		this.layout3.getChildByTag(TAG_BUTTON9).addChild(this.sel7);
		this.sel7.setVisible(false);




		var text2 = $.format("\t\t\t\t易错分析：易错B,录音机的磁带应用了磁性材料。IC卡中的金属片也是磁性材料，这一点易被忽视。光碟、光盘不是磁性材料。", 980,30);
		this.label6 = new cc.LabelTTF(text2,gg.fontName,gg.fontSize4);

		this.label6.setPosition(500,130);
		this.label6.setColor(cc.color(0, 0, 0, 250));
		this.label6.setVisible(false);
		this.layout3.addChild(this.label6);		
	},

	addlayout4:function(parent){
		this.layout4 = new ccui.Layout();//第三页
		this.layout4.setContentSize(cc.size(996, 598));
		parent.addLayer(this.layout4);
		var text1 = $.format("\t\t\t\t下列现象中，属于利用惯性的是（）", 1000,30);
		var label1 = new cc.LabelTTF(text1,gg.fontName,gg.fontSize4);
		label1.setAnchorPoint(0,0.5);
		label1.setPosition(0,525.5);
		label1.setColor(cc.color(0, 0, 0, 250));
		this.layout4.addChild(label1);	

		this.addimage(this.layout4, "#select.png", TAG_BUTTON10, cc.p(60,420), "A.汽车急刹车时，乘客的身体会向前倾", TAG_LABEL, cc.p(320,420));
		this.sel8 = new cc.Sprite("#wrong.png");
		this.sel8.setPosition(15,15);
		this.layout4.getChildByTag(TAG_BUTTON10).addChild(this.sel8);
		this.sel8.setVisible(false);

		this.addimage(this.layout4, "#select.png", TAG_BUTTON11, cc.p(60,370), "B.火车启动后，速度越来越大", TAG_LABEL, cc.p(270,370));
		this.sel9 = new cc.Sprite("#wrong.png");
		this.sel9.setPosition(15,15);
		this.layout4.getChildByTag(TAG_BUTTON11).addChild(this.sel9);
		this.sel9.setVisible(false);

		this.addimage(this.layout4, "#select.png", TAG_BUTTON12, cc.p(60,320), "C.从枪口射出的子弹在空中飞行，并击中目标", TAG_LABEL, cc.p(357,320));
		this.sel10 = new cc.Sprite("#right.png");
		this.sel10.setPosition(15,15);
		this.layout4.getChildByTag(TAG_BUTTON12).addChild(this.sel10);
		this.sel10.setVisible(false);

		this.addimage(this.layout4, "#select.png", TAG_BUTTON13, cc.p(60,270), "D.百米赛跑运动员到达终点后，不能立即停下来", TAG_LABEL, cc.p(369,270));
		this.sel11 = new cc.Sprite("#wrong.png");
		this.sel11.setPosition(15,15);
		this.layout4.getChildByTag(TAG_BUTTON13).addChild(this.sel11);
		this.sel11.setVisible(false);




		var text2 = $.format("\t\t\t\t易错分析：出错的原因主要是对题目中“利用”二字的误解，其实选项A或者D中的现象都与惯性有关，" +
				"但是它们只是惯性现象，不是人们有意识的利用惯性，所以选项A或者D是不正确的。选项C是利用惯性，使子弹击中目标。", 980,30);
		this.label7 = new cc.LabelTTF(text2,gg.fontName,gg.fontSize4);

		this.label7.setPosition(500,130);
		this.label7.setColor(cc.color(0, 0, 0, 250));
		this.label7.setVisible(false);
		this.layout4.addChild(this.label7);		
	},
	addimage:function(parent,str,tag,pos,str2,tag2,pos2){
		var image = new ButtonScale(parent,str,this.callback,this);
		image.setPosition(pos);
		image.setTag(tag);
		var label = new cc.LabelTTF(str2,gg.fontName,gg.fontSize);
		label.setColor(cc.color(0, 0, 0, 250));
		parent.addChild(label);
		label.setPosition(pos2);
		label.setTag(tag2);
	},
	addbutton:function(parent,tag,str,pos){
		var button = new ButtonScale(parent,"#unsel1.png",this.callback,this);
		button.setPosition(pos);
		button.setLocalZOrder(5);
		button.setTag(tag);
		var label = new cc.LabelTTF(str,gg.fontName,30);
		label.setPosition(button.width/2,button.height/2);		
		label.setColor(cc.color(0, 0, 0, 250));
		button.addChild(label);
	},
	callback:function(p){
		switch(p.getTag()){		
		case TAG_BUTTON:
			p.setEnable(false);
			//p.setSpriteFrame("sel.png");
			var move = cc.moveTo(1.5,cc.p(600,230));
			var ber = cc.bezierTo(1.5, [cc.p(360,228),cc.p(460,160),cc.p(545,135)]);
			var seq = cc.sequence(cc.callFunc(function(){
				this.line1.setVisible(false);
				this.qiu1.setVisible(false);
				this.label4.setVisible(false);
				this.line2.setVisible(false);
				this.qiu2.setVisible(false);
				this.label5.setVisible(false);
				this.citie.setVisible(false);
			},this),move,cc.callFunc(function(){
				this.line1.setVisible(true);
				this.qiu1.setVisible(true);
				this.label4.setVisible(true);
				this.qiu.setPosition(180,230);
			},this),cc.delayTime(0.5),cc.callFunc(function(){
				this.citie.setVisible(true);
			},this),ber,cc.callFunc(function(){
				this.line2.setVisible(true);
				this.qiu2.setVisible(true);
				this.label5.setVisible(true);
				this.qiu.setPosition(180,230);
				p.setEnable(true);
				//p.setSpriteFrame("unsel.png");
			},this));
			this.qiu.runAction(seq);
			break;
		case TAG_BUTTON2:
		case TAG_BUTTON3:
		case TAG_BUTTON4:
		case TAG_BUTTON5:
			this.sel.setVisible(true);
			this.sel1.setVisible(true);
			this.sel2.setVisible(true);
			this.sel3.setVisible(true);
			this.label5.setVisible(true);
			break;
		case TAG_BUTTON6:
		case TAG_BUTTON7:
		case TAG_BUTTON8:
		case TAG_BUTTON9:
			this.sel4.setVisible(true);
			this.sel5.setVisible(true);
			this.sel6.setVisible(true);
			this.sel7.setVisible(true);
			this.label6.setVisible(true);
			break;
		case TAG_BUTTON10:
		case TAG_BUTTON11:
		case TAG_BUTTON12:
		case TAG_BUTTON13:
			this.sel8.setVisible(true);
			this.sel9.setVisible(true);
			this.sel10.setVisible(true);
			this.sel11.setVisible(true);
			this.label7.setVisible(true);
			break;
		case TAG_EXP_02:
			ch.run(p.getTag());
			break;

		}
	}

});
