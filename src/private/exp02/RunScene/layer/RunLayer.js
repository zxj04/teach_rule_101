var RunLayer301 = cc.Layer.extend({
	arr:null,
	scene:null,
	clock:null,
	ctor:function (parent) {
		this._super();
		this.scene = parent;
		this.scene.addChild(this, 10);
		gg.main = this;
		this.init();
	},
	init:function () {
		this.callNext = cc.callFunc(function(){
			gg.flow.next();
		}, this);
		this.callKill = cc.callFunc(function(p){
			var seq = cc.sequence(cc.fadeOut(0.5),cc.callFunc(function(){
				p.removeFromParent(true);	
			}, this));
			p.runAction(seq);
		}, this); 

		this.callNext.retain();
		this.callKill.retain();
		//时钟
		this.clock = new Clock(this);
		// 物品库
	//	this.lib = new Lib(this);
//		this.lib.loadBg([{	
//			tag:TAG_LIB_BULB,
//			checkright:true,
//		},
//		{
//			tag:TAG_LIB_BOTTLE,
//		},
//		{	
//			tag:TAG_LIB_JIDIANQI,
//			checkright:true,
//		},
//		{
//			tag:TAG_LIB_BOOT,
//			checkright:true,
//		},
//		{	
//			tag:TAG_LIB_DIANJI,
//			checkright:true,
//		},
//		{
//			tag:TAG_LIB_CITIE,
//		},
//		{
//			tag:TAG_LIB_DAOXIAN,
//			checkright:true,
//		},
//		{
//			tag:TAG_LIB_DIANYUAN,
//			checkright:true,
//		},
//		{
//			tag:TAG_LIB_DIANLING,
//		},
//		{
//			tag:TAG_LIB_TANPIAN,
//		}
//		]);
		
		this.boot = new Boot1(this);
		this.boot.setVisible(true);
		this.boot.setPosition(540,384);

	},
	checkVisible:function(next){
		//是否可见
		var checkVisible = [];
		var node1 = ll.run.boot;

		checkVisible.push(node1);

		for(var i in checkVisible){
			if(checkVisible[i] !== null){				
				checkVisible[i].setVisible(next);				
			}			
		}
	},
	loadInLib:function(obj, pos, tarPos,delay){
		obj.setPosition(pos);
		if(delay == null){
			delay = 1;
		}
		var ber = $.bezier(pos, tarPos, delay);
		var seq = cc.sequence(ber, this.callNext);
		obj.runAction(seq);
	},
	kill:function(obj){
		var fade = cc.fadeTo(0.5,0);
		var func = cc.callfunc(function(){
			obj.removeFromParent(true);
		},this);
		var seq = cc.sequence(fade,func);
		obj.runAction(seq)
	},

	callback:function (p){
		var func = cc.callFunc(this.actionDone, this);
		var action=gg.flow.flow.action;
		switch(p.getTag()){

		}
	},
	actionDone:function(p){
		var func = cc.callFunc(this.actionDone, this);
		switch(p.getTag()){

		}
	},
	flowNext:function(){
		gg.flow.next();
	},
	onExit:function(){
		this._super();
		this.callNext.release();
		this.callKill.release();
	}
});