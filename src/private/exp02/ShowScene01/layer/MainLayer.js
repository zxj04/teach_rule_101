exp02.ShowMainLayer01 = cc.Layer.extend({
	curIndex: 0,
	ctor:function () {
		this._super();
		this.init();
		return true;
	},
	init:function(){
		var sv = new pub.ScrollView(this);
		sv.setOffestPos(cc.p(152,45 + 15));
		this.sv = sv;
	
		this.addlayout1(sv);
					
		this.addlayout3(sv);
		this.addlayout2(sv);
		this.addlayout4(sv);
	},	
	addlayout1:function(parent){
		var layout1 = new ccui.Layout();//第一页
		layout1.setContentSize(cc.size(996, 598));//设置layout的大小
		parent.addLayer(layout1);

		var text = $.format("\t\t\t\电磁铁在日常生活中有极其广泛的应用。 电磁铁是电流磁效应（电生磁）的一个应用，与生活联系紧密，如电磁继电器、电磁起重机、磁悬浮列车等。" +
				"电磁铁可以分为直流电磁铁和交流电磁铁两大类型。按照用途来划分电磁铁，主要可分成以下五种：" +
				"\n\n（1）牵引电磁铁—主要用来牵引机械装置、开启或关闭各种阀门，以执行自动控制任务。"+
				"\n（2）起重电磁铁—用作起重装置来吊运钢锭、钢材、铁砂等铁磁性材料。"+
				"\n（3）制动电磁铁—主要用于对电动机进行制动以达到准确停车的目的。"+
				"\n（4）自动电器的电磁系统—如电磁继电器和接触器的电磁系统、自动开关的电磁脱扣器及操作电磁铁等。" +
				"\n（5）其他用途的电磁铁—如磨床的电磁吸盘以及电磁振动器等。" , 980, 30);
		this.label1 = new cc.LabelTTF(text,gg.fontName,gg.fontSize4);
		this.label1.setPosition(490,layout1.height-this.label1.height/2);
		this.label1.setColor(cc.color(0, 0, 0, 250));
		layout1.addChild(this.label1);	
//		this.addimage(layout1, "#woping.jpg", cc.p(185,195), "增加压力", cc.p(185,70));
//		this.addimage(layout1, "#luntai.jpg", cc.p(500,195), "滚动转滑动", cc.p(500,70));
//		this.addimage(layout1, "#xie.jpg", cc.p(800,195), "增大粗糙程度", cc.p(800,70));



	},
	addlayout2:function(parent){
		var layout2 = new ccui.Layout();//第一页
		layout2.setContentSize(cc.size(996, 598));//设置layout的大小
		parent.addLayer(layout2);

		var text = $.format("磁悬浮列车：" +
				"\n\n\t\t\t\t磁悬浮列车就是利用“同名磁极相互排斥，异名磁极相互吸引”的原理，通过轨道上的强电磁铁与列车上的电磁铁之间的排斥" +
				"或吸引作用而悬浮起来，使列车与轨道分离，消除了列车与轨道间的摩擦。磁悬浮列车运行时的阻力很小，因此运行速度很快。" , 980, 30);
		this.label2 = new cc.LabelTTF(text,gg.fontName,gg.fontSize4);
		this.label2.setPosition(490,layout2.height-this.label2.height/2);
		this.label2.setColor(cc.color(0, 0, 0, 250));
		layout2.addChild(this.label2);	
		this.addimage(layout2, "#chao.jpg", cc.p(185,165), "中低速磁悬浮列车", cc.p(185,40));
		this.addimage(layout2, "#zhong.jpg", cc.p(500,165), "超级磁悬浮列车", cc.p(500,40));
		this.addimage(layout2, "#yuan.jpg", cc.p(800,165), "磁悬浮列车原理", cc.p(800,40));
	},
	
	addlayout3:function(parent){
		this.layout3 = new ccui.Layout();//第三页
		this.layout3.setContentSize(cc.size(996, 598));
		parent.addLayer(this.layout3);

		var text3 = $.format("　　电磁铁在日常生活中的广泛的应用：", 980,30);
		this.label3 = new cc.LabelTTF(text3,gg.fontName,gg.fontSize4);
		this.label3.setAnchorPoint(0,0.5);
		this.label3.setColor(cc.color(0, 0, 0, 250));
		this.label3.setPosition(0,this.layout3.height-this.label3.height/2);		
		this.layout3.addChild(this.label3);	

		this.addimage(this.layout3, "#qianyin.jpg", cc.p(185,420), "牵引电磁铁", cc.p(190,310));
		this.addimage(this.layout3, "#qizhong.jpg", cc.p(500,420), "起重电磁铁", cc.p(500,310));
		this.addimage(this.layout3, "#zhidong.jpg", cc.p(800,420), "制动电磁铁", cc.p(800,310));

//		var text4 = $.format("　　利用惯性来完成的活动：", 980,30);
//		this.label4 = new cc.LabelTTF(text4,gg.fontName,gg.fontSize4);
//		this.label4.setPosition(0,250);
//		this.label4.setColor(cc.color(0, 0, 0, 250));
//		this.label4.setAnchorPoint(0,0.5);
//		this.layout3.addChild(this.label4);	

		this.addimage(this.layout3, "#xitong.jpg", cc.p(185,135), "自动电磁系统", cc.p(185,30));
		this.addimage(this.layout3, "#xipan.jpg", cc.p(500,135), "电磁吸盘", cc.p(500,30));
		this.addimage(this.layout3, "#zhendong.jpg", cc.p(800,135), "电磁振动器", cc.p(800,30));
	},
	
	addlayout4:function(parent){
		var layout4 = new ccui.Layout();//第一页
		layout4.setContentSize(cc.size(996, 598));//设置layout的大小
		parent.addLayer(layout4);

		var text = $.format("信息的磁记录：" +
				"\n\n\t\t\t\t硬盘是信息磁记录的重要器件，它广泛应用于计算机、移动电话等数码产品。硬盘主要是由磁记录片、读写磁头以及其他配件组成。盘片的表面均匀地涂有一层极薄的磁性颗粒" +
				"读写磁头实际上就是一块磁铁" , 980, 30);
		this.label4 = new cc.LabelTTF(text,gg.fontName,gg.fontSize4);
		this.label4.setPosition(490,layout4.height-this.label4.height/2);
		this.label4.setColor(cc.color(0, 0, 0, 250));
		layout4.addChild(this.label4);	
		this.addimage(layout4, "#ying.jpg", cc.p(185,165), "硬盘", cc.p(185,40));
		this.addimage(layout4, "#gou.jpg", cc.p(500,165), "硬盘构造", cc.p(500,40));
		this.addimage(layout4, "#cun.png", cc.p(800,165), "存储原理", cc.p(800,40));
	},

	
	addbutton:function(parent,tag,str,pos){
		var button = new ButtonScale(parent,"#unsel.png",this.callback,this);
		button.setPosition(pos);
		button.setTag(tag);
		var label = new cc.LabelTTF(str,gg.fontName,30);
		label.setColor(cc.color(0, 0, 0, 250));
		label.setPosition(button.width/2,button.height/2);		
		button.addChild(label);
	},
	addimage:function(parent,str,pos,str2,pos2){
		var image = new cc.Sprite(str);
		parent.addChild(image);
		image.setPosition(pos);
		var label = new cc.LabelTTF(str2,gg.fontName,gg.fontSize);
		label.setColor(cc.color(0, 0, 0, 250));
		parent.addChild(label);
		label.setPosition(pos2);

	},
	
	callback:function(p){
		switch(p.getTag()){	
		case TAG_BUTTON:
		break;
		case TAG_BUTTON1:
			break;
		case TAG_MENU2:
			gg.runNext(p.getTag());
			break;
		
		}
	}

});
