exp02.ShowLayer01 = cc.Layer.extend({
	backgroundLayer : null,
	mainLayar : null,
	ctor:function (menuTag) {
		this._super();
		this.initFrames();
		this.loadBackground(menuTag);
		this.loadMainLayer();
	},
	initFrames : function(){
		cc.spriteFrameCache.addSpriteFrames(exp02.res_show.show_p);
		gg.curRunSpriteFrame.push(exp02.res_show.show_p);
	},
	loadBackground : function(menuTag){
		this.backgroundLayer = new ShowBackgroundLayer(menuTag);
		this.addChild(this.backgroundLayer);
	},
	loadMainLayer : function(){
		this.mainLayar = new exp02.ShowMainLayer01();
		this.addChild(this.mainLayar);
	}
});


exp02.ShowScene01 = PScene.extend({
	ctor:function(menuTag){
		this._super();
		this.menuTag=menuTag;
	},
	onEnter:function () {
		this._super();
		var layer = new exp02.ShowLayer01(this.menuTag);
		this.addChild(layer);
	}
});
