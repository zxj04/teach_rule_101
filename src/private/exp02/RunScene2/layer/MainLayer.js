var RunMainLayer03 = cc.Layer.extend({
	lead:null,
	real:null,
	ctor:function () {
		this._super();
		this.loadTip();
		this.loadRun();
		this.loadTool();
		this.loadFlow();
 
		return true;
	},
	
	loadTip:function(){
		ll.tip = new TipLayer(this);
	},
	loadTool:function(){
		ll.tool = new ToolLayer(this);
		ll.tool.getChildByTag(TAG_BUTTON_LIB).setVisible(false);
		ll.tool.getChildByTag(TAG_BUTTON_LIB).setEnable(false);
	},
	loadRun:function(){
		ll.run = new RunLayer202(this);
	},
	loadFlow:function(){
		gg.flow.setMain(this);
		gg.flow.start();
	},
	over: function (){
		ll.tip.over();
	
//		this.scheduleOnce(function(){
//			$.runScene(new FinishScene());
//		},2);
		
		ll.tip.back_frame.open();
		// 提交成绩
		net.saveScore();
	},
});
