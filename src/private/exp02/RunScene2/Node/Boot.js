Boot = cc.Node.extend({
	open_flag:false,
	ctor:function(p){
		this._super();
		p.addChild(this, 10, TAG_BOOT_NODE);
		this.init();
	},
	init : function(){
		this.setCascadeOpacityEnabled(true);
		this.boot= new Button(this, 10, TAG_BOOT, "#boot.png",this.callback);
		this.boot.setPosition(-190,168);
		this.boot.setScale(0.5);
		this.boot.setEnable(true);

		this.citie= new Button(this, 10, TAG_CHITIE, "#citie2.png",this.callback);
		this.citie.setPosition(8,5);
		this.citie.setScale(0.5);

		var dianling= new Button(this, 10, TAG_DIANLIANG, "#dianling.png",this.callback);
		dianling.setPosition(405,5);
		dianling.setScale(0.5);

		var dianyuan= new Button(this, 9, TAG_DIANYUAN, "#dianyuan.png",this.callback);
		dianyuan.setPosition(-155,-160);
		dianyuan.setScale(0.5);

		this.tanpian= new Button(this, 10, TAG_TANPIAN, "#tanpian1.png",this.callback);
		this.tanpian.setPosition(155,160);
		this.tanpian.setScale(0.5);		

		this.Select = new Button(this, 10, TAG_SELECT, "#sel.png",this.callback);
		this.Select.setPosition(650,100);
		this.Select.setScale(0.6);	
		this.label = new cc.LabelTTF("正常","微软雅黑",50);
		this.label.setPosition(this.Select.width*0.5,this.Select.height*0.5);
		this.Select.addChild(this.label);

		this.Select1 = new Button(this, 10, TAG_SELECT1, "#unsel.png",this.callback);
		this.Select1.setPosition(650,0);
		this.Select1.setScale(0.6);	
		this.label1 = new cc.LabelTTF("放慢","微软雅黑",50);
		this.label1.setPosition(this.Select1.width*0.5,this.Select1.height*0.5);
		this.Select1.addChild(this.label1);
		this.Select1.setEnable(true);
	},
	qiaoda:function(time){
		var frames=[];
		for (i=1;i<=2;i++){
			var str ="tanpian"+i+".png";
			var frame=cc.spriteFrameCache.getSpriteFrame(str);
			frames.push(frame);
		}

		var animation = new cc.Animation(frames,time);//负责动画序列
		var action = new cc.Animate(animation);//帧动画的动作创建
		this.tanpian.stopAllActions();
		this.tanpian.runAction(action.repeatForever());	
	},
	xianlu:function(time){
		var frames=[];
		for (i=1;i<=2;i++){
			var str ="citie"+i+".png";
			var frame=cc.spriteFrameCache.getSpriteFrame(str);
			frames.push(frame);
		}

		var animation = new cc.Animation(frames,time);//负责动画序列
		var action = new cc.Animate(animation);//帧动画的动作创建
		this.citie.stopAllActions();
		this.citie.runAction(action.repeatForever());	

	},
	callback:function(p){
		var action = gg.flow.flow.action;
		var func = cc.callFunc(this.actionDone, this);	
		switch(p.getTag()){	
		case TAG_BOOT:
			if(!this.open_flag){
				p.setSpriteFrame("boot1.png");
				this.qiaoda(0.05);
				this.xianlu(0.05);
				this.open_flag = !this.open_flag;
				p.setEnable(true);

			}else {
				p.setSpriteFrame("boot.png");
				this.citie.stopAllActions();
				this.citie.setSpriteFrame("citie2.png");
				this.tanpian.stopAllActions();
				this.tanpian.setSpriteFrame("tanpian1.png");
				this.open_flag = !this.open_flag;
				p.setEnable(true);
			}	
			ll.tip.arr.pos(p);
			break;
		case TAG_SELECT:
			p.setSpriteFrame("sel.png");
			this.Select1.setSpriteFrame("unsel.png");

			if(this.open_flag){
				this.qiaoda(0.05);
				this.xianlu(0.05);
				cc.log("正常");
			}
			this.Select1.setEnable(true);
			break;
		case TAG_SELECT1:
			p.setSpriteFrame("sel.png");
			this.Select.setSpriteFrame("unsel.png");
			if(this.open_flag){
				this.qiaoda(0.5);
				this.xianlu(0.5);
				cc.log("放慢");
			}
			this.Select.setEnable(true);
			break;
		}
	},
	flowNext:function(){
		gg.flow.next();
	},
});