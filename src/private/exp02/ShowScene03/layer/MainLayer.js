exp02.ShowMainLayer03 = cc.Layer.extend({
	lead:null,
	real:null,
	objArr:null,
	ctor:function () {
		this._super();
		this.init();
		return true;
	},
	init:function(){
		this.objArr = [];
		this.sv = new pub.ScrollView(this);
		this.sv.setOffestPos(cc.p(152,45 + 15));

		this.addlayout1();
	},	
	addlayout1:function(){
		var layout = new ccui.Layout();// 第一页
		layout.setContentSize(cc.size(996, 598));// 设置layout的大小
		this.sv.addLayer(layout);

		var text = $.format("翻翻看吧", 980, 30);
		this.addText(layout, text, cc.p(50, 580));
		
		var posArr = [];
		for(var i = 0; i < 5; i++){
			var pos = cc.p(100 + 200 * i, 420);
			posArr.push(pos);
		}
		for(var i = 0; i < 5; i++){
			var pos = cc.p(100 + 200 * i, 160);
			posArr.push(pos);
		}
		var nameArr = ["氢","氦","锂","铍","硼","碳","氮","氧","氟","氖","钠","镁","铝","硅","磷","硫","氯","氩"];
		var letterArr = ["H","He","Li","Be","B","C","N","O","F","Ne","Na","Mg","Al","Si","P","S","Cl","Ar"];
		
		var sel = [];
		// 获得五个随机数
		for(var i = 0; i < 5; i++){
			var r = Math.round(Math.random() * (letterArr.length - 1));
			sel.push({tag: i, name: nameArr[r]}, {tag: i, name: letterArr[r]});
			nameArr.splice(r, 1);
			letterArr.splice(r, 1);
		}
		sel.sort(function(){ return 0.5 - Math.random();});// 打乱数组
		
		this.cardArr = [];
		for(var i = 0; i < 10; i++){
			var card = new exp02.Card(layout, this.cardCallback, this);
			card.addContent(card.TYPE_STR, sel[i].name, "沃赛\n精品").setPosition(posArr[i]);
			card.setTag(sel[i].tag);
			this.cardArr.push(card);
		}
	},
	cardCallback:function(card){
		// 卡牌回调
		var cardIsPositive = card.isPositive();
		if(!cardIsPositive){
			return;
		}
		for(var i = 0; i < this.cardArr.length; i++){
			var obj = this.cardArr[i];
			if(obj === card){
				continue;
			}
			if(cardIsPositive
				&& obj.isPositive()){
				this.enableAllCard(false);
				this.scheduleOnce(function(){
					if(card.getTag() == obj.getTag()){
						card.setVisible(false);
						obj.setVisible(false);
					}else {
						card.runAction(cc.sequence(cc.delayTime(0.5), cc.callFunc(function(){
							card.setEnable(true);
						}, this)));
						obj.runAction(cc.sequence(cc.delayTime(0.5), cc.callFunc(function(){
							obj.setEnable(true);
						}, this)));
					}
					this.enableAllCard(true);
				}, 1);
				break;
			} 
		}
	},
	enableAllCard:function(enable){
		for(var i = 0; i < this.cardArr.length; i++){
			var obj = this.cardArr[i];
			obj.setEnable(enable);
		}
	},
	exeResult:function(){
		
	},
	addText:function(p, text, pos){
		var label = new cc.LabelTTF(text, gg.fontName, gg.fontSize4);
		label.setPosition(pos);
		label.setAnchorPoint(0, 0.5);
		label.setColor(cc.color(0, 0, 0, 250));
		p.addChild(label);
		return label;
	},
	callback:function(p){
		switch(p.getTag()){
		case TAG_BUTTON1:
			break;
		case TAG_BUTTON2:
			break;
		}
	}
	
});
