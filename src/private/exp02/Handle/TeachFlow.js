exp02.startRunNext = function(tag){
	switch(tag){//具体哪个菜单执行什么操作，这里修改
	case TAG_MENU1:
		pub.ch.gotoShow(TAG_EXP_02, tag, exp02.g_resources_show, null, new exp02.ShowScene01(tag));
		break;
	case TAG_MENU2:
		gg.teach_type = TAG_LEAD;
		gg.teach_flow = exp02.teach_flow01;
		pub.ch.gotoRun(TAG_EXP_02, tag, exp02.g_resources_run, null, new exp02.RunScene01(tag));
		break;
	case TAG_MENU3:
		gg.teach_type = TAG_LEAD;
		gg.teach_flow = exp02.teach_flow02;
		pub.ch.gotoRun(TAG_EXP_02, tag, exp02.g_resources_run, null, new exp02.RunScene02(tag));
		break;
	case TAG_MENU4:
		gg.teach_type = TAG_LEAD;
		gg.teach_flow = exp02.teach_flow03;
		pub.ch.gotoRun(TAG_EXP_02, tag, exp02.g_resources_run, null, new exp02.RunScene03(tag));
		break;
	case TAG_MENU5:
		pub.ch.gotoShow(TAG_EXP_02, tag, exp02.g_resources_show, null, new exp02.ShowScene02(tag));
		break;
	
	case TAG_ABOUT:
		pub.ch.gotoAbout(TAG_EXP_02, tag, g_resources_public_about, null, res_public_about.about_j);
		break;
	}
}

// 任务流
exp02.teach_flow01 = 
	[{
		tip:"用导线连接电磁铁一端和电源负极",
		tag:[
		     TAG_BOOT_NODE1,
		     TAG_QUAN
		     ]
	},
	{
		tip:"用导线连接电磁铁一端和电源负极",
		tag:[
		     TAG_BOOT_NODE1,
		     TAG_BLUE
		     ]
	},
	{
		tip:"用导线连接电磁铁一端和开关一端",
		tag:[
		     TAG_BOOT_NODE1,
		     TAG_QUAN1
		     ]
	},
	{
		tip:"用导线连接电磁铁一端和开关一端",
		tag:[
		     TAG_BOOT_NODE1,
		     TAG_DRED2
		     ]
	},
	{
		tip:"用导线连接电原正极和开关另一端",
		tag:[
		     TAG_BOOT_NODE1,
		     TAG_DBLACK2
		     ]
	},
	{
		tip:"用导线连接电原正极和另开关一端",
		tag:[
		     TAG_BOOT_NODE1,
		     TAG_RED
		     ]
	},
	{
		tip:"用导线连接红灯负极和触点一端",
		tag:[
		     TAG_BOOT_NODE1,
		     TAG_QUAN2
		     ]
	},
	{
		tip:"用导线连接红灯负极和触点一端",
		tag:[
		     TAG_BOOT_NODE1,
		     TAG_DBLACK
		     ]
	},
	{
		tip:"用导线连接红灯正极和高压电源一端",
		tag:[
		     TAG_BOOT_NODE1,
		     TAG_DRED
		     ]
	},
	{
		tip:"用导线连接红灯正极和高压电源一端",
		tag:[
		     TAG_BOOT_NODE1,
		     TAG_QUAN8
		     ],action:ACTION_DO1
	},
	{
		tip:"用导线连接弹片和高压电源另一端",
		tag:[
		     TAG_BOOT_NODE1,
		     TAG_QUAN6
		     ]
	},
	{
		tip:"用导线连接弹片和高压电源另一端",
		tag:[
		     TAG_BOOT_NODE1,
		     TAG_QUAN7
		     ]
	},
	{
		tip:"用导线连接绿灯负极和触点另一端",
		tag:[
		     TAG_BOOT_NODE1,
		     TAG_QUAN3
		     ]
	},
	{
		tip:"用导线连接绿灯负极和触点另一端",
		tag:[
		     TAG_BOOT_NODE1,
		     TAG_DBLACK1
		     ]
	},
	
	{
		tip:"用导线连接绿灯正极和电动机一端",
		tag:[
		     TAG_BOOT_NODE1,
		     TAG_DRED1
		     ]
	},
	{
		tip:"用导线连接绿灯正极和电动机一端",
		tag:[
		     TAG_BOOT_NODE1,
		     TAG_QUAN4
		     ]
	},
	{
		tip:"用导线连接电动机另一端和高压电源",
		tag:[
		     TAG_BOOT_NODE1,
		     TAG_QUAN5
		     ]
	},
	{
		tip:"用导线连接电动机另一端和高压电源",
		tag:[
		     TAG_BOOT_NODE1,
		     TAG_QUAN8
		     ],action:ACTION_DO2
	},
	{
		tip:"闭合电源开关，观察现象",
		tag:[
		     TAG_BOOT_NODE1,
		     TAG_BOOT
		     ],action:ACTION_DO1
	},
	 {
		tip:"闭合电源开关，观察现象",
		tag:[
		     TAG_BOOT_NODE1,
		     TAG_BOOT
		     ],action:ACTION_DO1
	},
	{
		tip:"打开电源开关，观察现象",
		tag:[
		     TAG_BOOT_NODE1,
		     TAG_BOOT
		     ],action:ACTION_DO2

	},
	 {
		 tip:"恭喜过关",
		 over:true
	 }
	 ]

exp02.teach_flow02 = 
	[
	 
	 {
		 tip:"用导线连接电磁继电器一端和金属片Ａ",
		 tag:[
		      TAG_BODY_NODE,
		      TAG_QUAN1
		      ]
	 },
	 {
		 tip:"用导线连接电磁继电器一端和金属片Ａ",
		 tag:[
		      TAG_BODY_NODE,
		      TAG_QUAN2
		      ]
	 },
	 {
		 tip:"用导线连接电磁继电器另一端和电源正极",
		 tag:[
		      TAG_BODY_NODE,
		      TAG_QUAN
		      ]

	 },
	 {
		 tip:"用导线连接电磁继电器另一端和电源正极",
		 tag:[
		      TAG_BODY_NODE,
		      TAG_RED
		      ]

	 },
	 {
		 tip:"用导线连接电源负极和金属片B",
		 tag:[
		      TAG_BODY_NODE,
		      TAG_BLUE
		      ]

	 },
	 {
		 tip:"用导线连接电源负极和金属片B",
		 tag:[
		      TAG_BODY_NODE,
		      TAG_QUAN3
		      ]

	 },
	 {
		 tip:"用导线连接绿灯负极和触点一端",
		 tag:[
		      TAG_BODY_NODE,
		      TAG_DBLACK
		      ]

	 },
	 {
		 tip:"用导线连接绿灯负极和触点一端",
		 tag:[
		      TAG_BODY_NODE,
		      TAG_QUAN5
		      ]

	 },
	 {
		 tip:"用导线连接绿灯正极和电源正极",
		 tag:[
		      TAG_BODY_NODE,
		      TAG_DRED
		      ]

	 },
	 {
		 tip:"用导线连接绿灯正极和电源正极",
		 tag:[
		      TAG_BODY_NODE,
		      TAG_RED1
		      ],action:ACTION_DO1

	 },
	 {
		 tip:"用导线连接红灯负极和触点另一端",
		 tag:[
		      TAG_BODY_NODE,
		      TAG_DBLACK1
		      ]

	 },
	 {
		 tip:"用导线连接红灯负极和触点另一端",
		 tag:[
		      TAG_BODY_NODE,
		      TAG_QUAN4
		      ]

	 },
	 {
		 tip:"用导线连接红灯正极和电源正极",
		 tag:[
		      TAG_BODY_NODE,
		      TAG_DRED1
		      ]

	 },
	 {
		 tip:"用导线连接红灯正极和电源正极",
		 tag:[
		      TAG_BODY_NODE,
		      TAG_RED1
		      ],action:ACTION_DO2

	 },
	 {
		 tip:"用导线连接电源负极和弹片",
		 tag:[
		      TAG_BODY_NODE,
		      TAG_BLUE1
		      ]

	 },
	 {
		 tip:"用导线连接电源负极和弹片",
		 tag:[
		      TAG_BODY_NODE,
		      TAG_QUAN6
		      ]

	 },
	 {
		 tip:"往烧杯中注水，观察现象",
		 tag:[
		      TAG_BODY_NODE,
		      TAG_WATERPIPE
		      ],action:ACTION_DO1

	 },
	 {
		 tip:"从烧杯中吸水，观察现象",
		 tag:[
		      TAG_BODY_NODE,
		      TAG_WATERPIPE
		      ],action:ACTION_DO2

	 },
	 {
		 tip:"恭喜过关",
		 over:true

	 }	  
	 ]
exp02.teach_flow03 = 
	[

	 {
		 tip:"关闭、打开电源开关，观察现象",
		 tag:[
		      TAG_BOOT_NODE,
		      TAG_BOOT
		      ]
	 },

	 {
		 tip:"恭喜过关",
		 over:true

	 }
	 ]


