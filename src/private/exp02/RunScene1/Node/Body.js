Body = cc.Node.extend({
	lineArr:[],
	ctor:function(p){
		this._super();
		p.addChild(this, 10, TAG_BODY_NODE);
		this.init();
	},
	init : function(){
		this.lineArr = [];
		this.setCascadeOpacityEnabled(true);
		var beaker= new Button(this, 10, TAG_BEAKER, "#beaker.png",this.callback);
		beaker.setPosition(-308,-205);
		beaker.setScale(0.5);

		var beakerunder = new cc.Sprite("#beakerunder.png");
		beakerunder.setPosition(-302,-285);
		beakerunder.setScale(0.5);
		this.addChild(beakerunder,8);

		this.water= new Button(this, 9, TAG_WATER, "#water.png",this.callback);
		this.water.setPosition(-302,-295);
		this.water.setAnchorPoint(0.5,0);
		this.water.setScale(0.5,0.15);

		var waterpipe = new cc.Sprite("#waterpipe1.png");
		waterpipe.setPosition(-350,-198);
		waterpipe.setScale(0.5);
		this.addChild(waterpipe,8);

		var waterpipe1= new Button(this, 10, TAG_WATERPIPE, "#waterpipe2.png",this.callback);
		waterpipe1.setPosition(-409,-66);
		waterpipe1.setScale(0.5);

		this.sluga= new Button(this, 8, TAG_SLUGA, "#sluga1.png",this.callback);
		this.sluga.setPosition(-326,-140);
		this.sluga.setScale(0.5);

		this.slugb= new Button(this, 8, TAG_SLUGB, "#slugb1.png",this.callback);
		this.slugb.setPosition(-267.5,-195);
		this.slugb.setScale(0.5);				

		var chudian= new Button(this, 11, TAG_CHUDIAN, "#chudian.png",this.callback);
		chudian.setPosition(230,200);
		chudian.setScale(0.5);

		this.daoxian= new Button(this, 10, TAG_DAOXIAN, "#daoxian3.png",this.callback);
		this.daoxian.setPosition(118,12);
		this.daoxian.setScale(0.5);
		this.daoxian.setVisible(false);

		this.dengpao= new Button(this, 9, TAG_DENGPAO, "#dengpao.png",this.callback);
		this.dengpao.setPosition(459,46);
		this.dengpao.setScale(0.5);

		this.dengpao1= new Button(this, 9, TAG_DENGPAO1, "#dengpao.png",this.callback);
		this.dengpao1.setPosition(462,288);
		this.dengpao1.setScale(0.5);

		var citi= new Button(this, 9, TAG_CITI, "#citi.png",this.callback);
		citi.setPosition(53,99);
		citi.setScale(0.5);

		var dianyuan= new Button(this, 9, TAG_DIANYUAN, "#dianyuan1.png",this.callback);
		dianyuan.setPosition(37,-262);
		dianyuan.setScale(0.5);

		var dianyuan1= new Button(this, 9, TAG_DIANYUAN1, "#dianyuan1.png",this.callback);
		dianyuan1.setPosition(444,-258);
		dianyuan1.setScale(0.5);

		this.tanpian= new Button(this, 10, TAG_TANPIAN, "#tanpian3.png",this.callback);
		this.tanpian.setPosition(80,110);
		this.tanpian.setScale(0.5);		

		this.citixian = new cc.Sprite("#citixian.png");
		this.citixian.setPosition(89.5,57);
		this.citixian.setScale(0.5);
		this.addChild(this.citixian,10);

		this.line1 = new cc.Sprite("#line1.png");
		this.line1.setPosition(-328,-72);
		this.line1.setScale(2.2,0.5);
		this.line1.setRotation(90);
		this.addChild(this.line1,10);

		this.line2 = new cc.Sprite("#line1.png");
		this.line2.setPosition(-267,-89);
		this.line2.setScale(1.5,0.5);
		this.line2.setRotation(90);
		this.addChild(this.line2,10);

		this.line3 = new cc.Sprite("#line1.png");
		this.line3.setPosition(297,165);
		this.line3.setScale(2.05,0.5);
		this.addChild(this.line3,10);

		this.line4 = new cc.Sprite("#line1.png");
		this.line4.setPosition(298,232);
		this.line4.setScale(2.07,0.5);
		this.addChild(this.line4,10);

		this.draw = new cc.DrawNode();
		this.addChild(this.draw,20);

		var quan = new Button(this, 10, TAG_QUAN, "#quan.png",this.callback);
		quan.setPosition(155,135);
		quan.setScale(0.5);

		var quan1 = new Button(this, 10, TAG_QUAN1, "#quan.png",this.callback);
		quan1.setPosition(138,55);
		quan1.setScale(0.5);

		var quan2 = new Button(this, 10, TAG_QUAN2, "#quan.png",this.callback);
		quan2.setPosition(-328,-25);
		quan2.setScale(0.5);

		var quan3 = new Button(this, 10, TAG_QUAN3, "#quan.png",this.callback);
		quan3.setPosition(-266,-57);
		quan3.setScale(0.5);

		var quan4 = new Button(this, 10, TAG_QUAN4, "#quan.png",this.callback);
		quan4.setPosition(340,165);
		quan4.setScale(0.5);

		var quan5 = new Button(this, 10, TAG_QUAN5, "#quan.png",this.callback);
		quan5.setPosition(342,231);
		quan5.setScale(0.5);

		var quan6 = new Button(this, 10, TAG_QUAN6, "#quan.png",this.callback);
		quan6.setPosition(185,195);
		quan6.setScale(0.5);

		var dred = new Button(this, 10, TAG_DRED, "#red1.png",this.callback);
		dred.setPosition(496,259);
		dred.setScale(0.5);

		var dred1 = new Button(this, 10, TAG_DRED1, "#red1.png",this.callback);
		dred1.setPosition(492.5,17);
		dred1.setScale(0.5);

		var dblack = new Button(this, 10, TAG_DBLACK, "#black1.png",this.callback);
		dblack.setPosition(424,259);
		dblack.setScale(0.5);

		var dblack1 = new Button(this, 10, TAG_DBLACK1, "#black1.png",this.callback);
		dblack1.setPosition(421,17);
		dblack1.setScale(0.5);

		var red = new Button(this, 10, TAG_RED, "#red.png",this.callback);
		red.setPosition(73,-227);
		red.setScale(0.5);

		var red1 = new Button(this, 10, TAG_RED1, "#red.png",this.callback);
		red1.setPosition(480,-223);
		red1.setScale(0.5);

		var blue = new Button(this, 10, TAG_BLUE, "#blue.png",this.callback);
		blue.setPosition(1,-228);
		blue.setScale(0.5);

		var blue1 = new Button(this, 10, TAG_BLUE1, "#blue.png",this.callback);
		blue1.setPosition(408,-224);
		blue1.setScale(0.5);

	},
	Line:function(tag,line,time,pos,scalex,rotation){
		var line = new cc.Sprite(line);
		line.setPosition(pos);
		line.setAnchorPoint(1,0.5);
		line.setScale(0.5,0.5);
		line.setRotation(rotation);
		this.addChild(line,10,tag);
		line.runAction(cc.scaleTo(time,scalex,0.5));

	},
	firstPoint:function(obj){
		var seq = cc.sequence(cc.scaleTo(0.2,0.8),cc.scaleTo(0.2,0.5));
		obj.runAction(seq.repeatForever());
		this.flowNext();
	},
	stopScale:function(tag){
		this.getChildByTag(tag).setScale(0.5);
		this.getChildByTag(tag).stopAllActions();
	},
	callback:function(p){
		var action = gg.flow.flow.action;
		var func = cc.callFunc(this.actionDone, this);	
		switch(p.getTag()){	
		case TAG_QUAN:
		case TAG_QUAN1:
		case TAG_BLUE:
		case TAG_DBLACK:
		case TAG_DRED:		
		case TAG_DBLACK1:
		case TAG_DRED1:
		case TAG_BLUE1:
			this.firstPoint(p);
			break;
		case TAG_QUAN2:
			this.stopScale(TAG_QUAN1);
			var seq = cc.sequence(cc.callFunc(function(){
				this.Line(TAG_LINE17,"#line1.png",0.5,cc.p(139,55),1.86,-90);
			},this),cc.delayTime(0.5),cc.callFunc(function(){
				this.Line(TAG_LINE1,"#line1.png",2,cc.p(140,-24),10.92,0);
			},this),cc.delayTime(2),cc.callFunc(this.flowNext,this));
			p.runAction(seq);
			break;
		case TAG_RED:
			this.stopScale(TAG_QUAN);
			var seq = cc.sequence(cc.callFunc(function(){
				this.Line(TAG_LINE2,"#line1.png",1.5,cc.p(156.5,135),8.6,-90);
			},this),cc.delayTime(1.5),cc.callFunc(function(){
				this.Line(TAG_LINE3,"#line1.png",1,cc.p(156.5,-234),2.15,0);
			},this),cc.delayTime(1),cc.callFunc(this.flowNext,this));
			p.runAction(seq);
			break;
		case TAG_QUAN3:
			this.stopScale(TAG_BLUE);
			var seq = cc.sequence(cc.callFunc(function(){
				this.Line(TAG_LINE4,"#line1.png",0.8,cc.p(9,-234),1.75,0);
			},this),cc.delayTime(0.8),cc.callFunc(function(){
				this.Line(TAG_LINE5,"#line1.png",1,cc.p(-65,-234),4.14,90);
			},this),cc.delayTime(1),cc.callFunc(function(){
				this.Line(TAG_LINE6,"#line1.png",1,cc.p(-65,-57),4.7,0);
			},this),cc.delayTime(1),cc.callFunc(this.flowNext,this));
			p.runAction(seq);	
			break;
		case TAG_QUAN5:
			this.stopScale(TAG_DBLACK);
			var seq = cc.sequence(cc.callFunc(function(){
				this.Line(TAG_LINE7,"#line1.png",1,cc.p(425,253),1.94,0);				
			},this),cc.delayTime(1),cc.callFunc(function(){
				this.Line(TAG_LINE9,"#line1.png",1,cc.p(342,253),0.5,-90);
			},this),cc.delayTime(1),cc.callFunc(this.flowNext,this));
			p.runAction(seq);	
			break;
		case TAG_RED1:
			if(action ==ACTION_DO1){
				this.stopScale(TAG_DRED);
				var seq = cc.sequence(cc.callFunc(function(){				
					this.Line(TAG_LINE8,"#line1.png",1,cc.p(495,253),1.55,180);
				},this),cc.delayTime(1),cc.callFunc(function(){
					this.Line(TAG_LINE10,"#line1.png",3,cc.p(561,255),11.3,-90);
				},this),cc.delayTime(3),cc.callFunc(function(){
					this.Line(TAG_LINE11,"#line1.png",1,cc.p(561,-230),2.1,0);
				},this),cc.delayTime(1),cc.callFunc(this.flowNext,this));
				p.runAction(seq);
			}else if(action ==ACTION_DO2){
				this.stopScale(TAG_DRED1);
				var seq = cc.sequence(cc.callFunc(function(){			
					this.Line(TAG_LINE13,"#line1.png",1,cc.p(490,10),1.65,180);
				},this),cc.delayTime(1),cc.callFunc(function(){
					this.point = new cc.Sprite("#blackpoint.png");
					this.point.setPosition(560,10);
					this.point.setScale(0.5);
					this.addChild(this.point,10);				
				},this),cc.callFunc(this.flowNext,this));
				p.runAction(seq);	
			}				
			break;
		case TAG_QUAN4:
			this.stopScale(TAG_DBLACK1);
			var seq = cc.sequence(cc.callFunc(function(){
				this.Line(TAG_LINE12,"#line1.png",1,cc.p(421,10),1.92,0);
			},this),cc.delayTime(1),cc.callFunc(function(){
				this.Line(TAG_LINE14,"#line1.png",1,cc.p(340,10),3.6,90);
			},this),cc.delayTime(1),cc.callFunc(this.flowNext,this));
			p.runAction(seq);	
			break;
		case TAG_QUAN6:
			this.stopScale(TAG_BLUE1);
			var seq = cc.sequence(cc.callFunc(function(){
				this.Line(TAG_LINE15,"#line1.png",1,cc.p(417,-230),5.4,0);				
			},this),cc.delayTime(1),cc.callFunc(function(){				
				this.Line(TAG_LINE16,"#line1.png",2,cc.p(186.5,-230),10,90);
			},this),cc.delayTime(2.2),cc.callFunc(function(){
				this.line4.setSpriteFrame("line2.png");
				this.getChildByTag(TAG_LINE7).setSpriteFrame("line2.png");
				this.getChildByTag(TAG_LINE8).setSpriteFrame("line2.png");
				this.getChildByTag(TAG_LINE9).setSpriteFrame("line2.png");
				this.getChildByTag(TAG_LINE10).setSpriteFrame("line2.png");
				this.getChildByTag(TAG_LINE11).setSpriteFrame("line2.png");
				this.getChildByTag(TAG_LINE15).setSpriteFrame("line2.png");
				this.getChildByTag(TAG_LINE16).setSpriteFrame("line2.png");
				this.getChildByTag(TAG_DENGPAO1).setSpriteFrame("dengpao2.png");
			},this),cc.callFunc(this.flowNext,this));
			p.runAction(seq);	
			break;
		case TAG_WATERPIPE:
			this.daoxian.setVisible(true);
			if(action==ACTION_DO1){
				for (i=0;i<17;i++){
					var TAG_LINE = TAG_LINE1 + i;
					this.getChildByTag(TAG_LINE).setVisible(false);					
				}
				this.getChildByTag(TAG_QUAN).setVisible(false);	
				this.getChildByTag(TAG_QUAN1).setVisible(false);
				this.getChildByTag(TAG_QUAN2).setVisible(false);
				this.getChildByTag(TAG_QUAN3).setVisible(false);
				this.getChildByTag(TAG_QUAN4).setVisible(false);
				this.getChildByTag(TAG_QUAN5).setVisible(false);
				this.getChildByTag(TAG_QUAN6).setVisible(false);

				this.line1.setVisible(false);	
				this.line2.setVisible(false);	
				this.line3.setVisible(false);	
				this.line4.setVisible(false);
				this.citixian.setVisible(false);
				this.showTip = new ShowTip01("往烧杯中注水" +
						"\n观察现象",cc.p(110,420),TAG_SHOW);
				var seq = cc.sequence(cc.scaleTo(3,0.5,0.38),cc.callFunc(function(){
					this.showTip1 = new ShowTip01("电磁铁通电" +
							"\n吸引衔铁",cc.p(650,660),TAG_SHOW1);
					this.showTip2 = new ShowTip01("红灯亮",cc.p(1000,330),TAG_SHOW2);
					this.daoxian.setSpriteFrame("daoxian4.png");
					this.dengpao.setSpriteFrame("dengpao1.png");
					this.dengpao1.setSpriteFrame("dengpao.png");
					this.tanpian.setSpriteFrame("tanpian4.png");
					this.sluga.setSpriteFrame("sluga.png");
					this.slugb.setSpriteFrame("slugb.png");
				},this),cc.scaleTo(1,0.5,0.5),cc.delayTime(5),cc.callFunc(this.flowNext,this));					
				this.water.runAction(seq);					
			}else if(action==ACTION_DO2){
				this.showTip = new ShowTip01("从烧杯中抽水" +
						"\n观察现象",cc.p(110,420),TAG_SHOW);		
				var seq = cc.sequence(cc.scaleTo(1,0.5,0.38),cc.callFunc(function(){
					this.showTip1.content.setString("电磁铁断电" +
					"\n衔铁复原");
					this.showTip2.content.setString("绿灯亮");
					this.showTip2.bg.setPosition(cc.p(1000,570));
					this.daoxian.setSpriteFrame("daoxian3.png");
					this.dengpao.setSpriteFrame("dengpao.png");
					this.dengpao1.setSpriteFrame("dengpao2.png");
					this.tanpian.setSpriteFrame("tanpian3.png");
					this.sluga.setSpriteFrame("sluga1.png");
					this.slugb.setSpriteFrame("slugb1.png");
				},this),cc.scaleTo(3,0.5,0.15),cc.delayTime(5),cc.callFunc(this.flowNext,this));					
				this.water.runAction(seq);	
			}
			break;
		}
	},
	flowNext:function(){
		gg.flow.next();
	},
	
});

