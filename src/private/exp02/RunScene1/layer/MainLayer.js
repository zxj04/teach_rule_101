var RunMainLayer01 = cc.Layer.extend({
	lead:null,
	real:null,
	ctor:function () {
		this._super();
	
		this.loadTip();
		this.loadRun();
		this.loadTool();
		this.loadFlow();
 
		return true;
	},
	
	loadTip:function(){
		ll.tip = new TipLayer(this);
		//ll.tip.setPosition(0,-80);
	},
	loadTool:function(){
		ll.tool = new ToolLayer01(this);
		ll.tool.getChildByTag(TAG_BUTTON_LIB).setVisible(false);
		ll.tool.getChildByTag(TAG_BUTTON_LIB).setEnable(false);
	},
	loadRun:function(){
		ll.run = new RunLayer101(this);
	},
	loadFlow:function(){
		gg.flow.setMain(this);
		gg.flow.start();
	},
	over: function (){
		ll.tip.over();
		ll.tip.back_frame.open();
		// 提交成绩
		net.saveScore();
	},
});
