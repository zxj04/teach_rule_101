var exp05 = exp05||{};
//start
exp05.res_start = {
		//start_png :"res/private/exp05/start/startpng.png"
};

exp05.g_resources_start = [];
for (var i in exp05.res_start) {
	exp05.g_resources_start.push(exp05.res_start[i]);
}

//run
exp05.res_run = {
		run_p : "res/private/exp05/run.plist",
		run_g : "res/private/exp05/run.png",
};

exp05.g_resources_run = [];
for (var i in exp05.res_run) {
	exp05.g_resources_run.push(exp05.res_run[i]);
}

//show02
exp05.res_show = {
		show_p : "res/private/exp05/show.plist",
		show_g : "res/private/exp05/show.png"
};

exp05.g_resources_show = [];
for (var i in exp05.res_show) {
	exp05.g_resources_show.push(exp05.res_show[i]);
}


