exp05.ShowMainLayer05 = cc.Layer.extend({
	lead:null,
	real:null,
	curIndex: 0,
	reset:true,
	actionnum:1,
	ctor:function () {
		this._super();
		this.init();
		return true;
	},
	init:function(){
		var sv = new pub.ScrollView(this);
		sv.setOffestPos(cc.p(152,45 + 15));

		this.addlayout1(sv);
		this.addlayout2(sv);
	},	
	addlayout1:function(parent){
		this.layout1 = new ccui.Layout();//第一页
		this.layout1.setContentSize(cc.size(996, 598));//设置layout的大小
		parent.addLayer(this.layout1);

		var text = $.format("　　化合反应：参加反应的物质有两种或以上，产生的物质只有一种，比如硫和氧气反应生成二氧化硫。", 960, 30);		
		label1 = new cc.LabelTTF(text,gg.fontName,gg.fontSize4);
		label1.setAnchorPoint(0,0.5);
		label1.setPosition(0,550);
		label1.setColor(cc.color(0, 0, 0, 250));
		this.layout1.addChild(label1);	

		this.addimage(this.layout1, "#man.png", cc.p(500,200));

		this.addimage(this.layout1, "#cloud1.png", cc.p(200,400));

		this.addlabel(this.layout1, "反应物两种及以上", cc.p(200,400), 30);

		this.addimage(this.layout1, "#cloud2.png", cc.p(800,400));
		this.addlabel(this.layout1, "生成物只有一种", cc.p(800,400), 30);		
	},
	addlayout2:function(parent){
		this.layout2 = new ccui.Layout();//第四页
		this.layout2.setContentSize(cc.size(996, 598));
		parent.addLayer(this.layout2);

		var text = $.format("　　分解反应：参加反应的物质只有一种，产生的物质有两种或以上，比如过氧化氢分解产生水和氧气。", 960, 30);		
		label1 = new cc.LabelTTF(text,gg.fontName,gg.fontSize4);
		label1.setAnchorPoint(0,0.5);
		label1.setPosition(0,550);
		label1.setColor(cc.color(0, 0, 0, 250));
		this.layout2.addChild(label1);	

		this.addimage(this.layout2, "#man.png", cc.p(500,200));

		this.addimage(this.layout2, "#cloud1.png", cc.p(200,400));

		this.addlabel(this.layout2, "反应物只有一种", cc.p(200,400), 30);

		this.addimage(this.layout2, "#cloud2.png", cc.p(800,400));
		this.addlabel(this.layout2, "生成物两种及以上", cc.p(800,400), 30);		
	},
	addimage:function(parent,str,pos){
		var showimage = new cc.Sprite(str);
		showimage.setPosition(pos);
		parent.addChild(showimage,5);
	},
	addlabel:function(parent,str,pos,fontsize){
		var label = new cc.LabelTTF(str,gg.fontName,fontsize);	
		label.setPosition(pos);
		label.setColor(cc.color(0, 0, 0, 250));
		parent.addChild(label,8);	
	},
	
});
