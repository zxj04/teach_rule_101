exp05.ShowMainLayer03 = cc.Layer.extend({
	lead:null,
	real:null,
	curIndex: 0,
	reset:true,
	reset1:true,
	reset2:true,
	actionnum:1,
	ctor:function () {
		this._super();
		this.init();
		return true;
	},
	init:function(){
		var sv = new pub.ScrollView(this);
		sv.setOffestPos(cc.p(152,45 + 15));

		this.addlayout1(sv);
		this.addlayout2(sv);
		this.addlayout3(sv);
		this.addlayout4(sv);
	},	
	addlayout1:function(parent){
		this.layout1 = new ccui.Layout();//第一页
		this.layout1.setContentSize(cc.size(996, 598));//设置layout的大小
		parent.addLayer(this.layout1);

		var text = $.format("　　氧气不易溶于水，常温下1升水中大约能溶解30毫升的氧气。氧气时水生生物得以生存的必要条件之一。" +
				"在压强为1.02*10^5帕的条件下，氧气在-183℃是会变成蓝色的液态氧，在-218℃时会凝固成雪花状的蓝色固体", 960, 30);
		
		label1 = new cc.LabelTTF(text,gg.fontName,gg.fontSize4);
		label1.setAnchorPoint(0,0.5);
		label1.setPosition(0,500);
		label1.setColor(cc.color(255, 255, 255, 250));
		this.layout1.addChild(label1,10);	
		
		var bg = new cc.Sprite(res_public.run_back);
		this.layout1.addChild(bg);
		bg.setScale(0.8);
		bg.setPosition(cc.p(this.layout1.width /2, this.layout1.height / 2));
		
		this.addImage(this.layout1, 5, "#paper.png", cc.p(300,270), 0.7);
		this.addImage(this.layout1, 6, "#bottle2.png", cc.p(300,270), 0.7);
		
		var man = new cc.Sprite("#man/1.png");
		man.setPosition(cc.p(700,270));
		this.layout1.addChild(man,3);
    	man.runAction(this.addAction("man/", 3,0.2));
		
    	var feng = new cc.Sprite("#cloud/1.png");
    	feng.setPosition(cc.p(670,300));
    	this.layout1.addChild(feng,5);
    	feng.runAction(this.addAction("cloud/", 14,0.05));
    	
    	var feng1 = new cc.Sprite("#cloud/1.png");
    	feng1.setPosition(cc.p(670,290));
    	this.layout1.addChild(feng1,5);
    	feng1.runAction(this.addAction("cloud/", 14,0.05));
    	
    	label1 = new cc.LabelTTF("　　氧气是一种无色无味的气体，那它还有哪些性质呢？",gg.fontName,gg.fontSize4);
    	label1.setAnchorPoint(0,0.5);
    	label1.setPosition(0,80);
    	label1.setColor(cc.color(255, 255, 255, 250));
    	this.layout1.addChild(label1,10);	
		
	},
	addlayout2:function(parent){
		this.layout2 = new ccui.Layout();//第四页
		this.layout2.setContentSize(cc.size(996, 598));
		parent.addLayer(this.layout2);

		label1 = new cc.LabelTTF("　　将带火星的木条伸入充满氧气的集气瓶里，观察现象。",gg.fontName,gg.fontSize4);
		label1.setAnchorPoint(0,0.5);
		label1.setPosition(0,this.layout2.height-label1.height/2);
		label1.setColor(cc.color(255, 255, 255, 250));
		this.layout2.addChild(label1,10);	
		
		var bg = new cc.Sprite(res_public.run_back);
		this.layout2.addChild(bg);
		bg.setScale(0.8);
		bg.setPosition(cc.p(this.layout2.width /2, this.layout2.height / 2));
		
		this.addImage(this.layout2, 5, "#lamp.png", cc.p(650,200), 0.5);
		this.addImage(this.layout2, 5, "#match3.png", cc.p(800,180), 0.5);
		this.addImage(this.layout2, 5, "#bottle2.png", cc.p(400,255), 0.65);
		
		this.match = new cc.Sprite("#match4.png");
		this.match.setPosition(cc.p(855,235));
		this.layout2.addChild(this.match,5);
		this.match.setScale(0.5);
		this.match.setVisible(false);
		
		this.glass = new cc.Sprite("#glass.png");
		this.glass.setPosition(cc.p(400,362));
		this.layout2.addChild(this.glass,5);
		this.glass.setScale(0.7);
		
		this.fire = new cc.Sprite("#fire/1.png");
		this.fire.setPosition(cc.p(652,277));
		this.layout2.addChild(this.fire,5);
		this.fire.setVisible(false);
		this.fire.runAction(this.addAction("fire/", 3, 0.05));
		
		this.lamplid = new cc.Sprite("#lamplid.png");
		this.lamplid.setPosition(cc.p(652,255));
		this.layout2.addChild(this.lamplid,5);
		this.lamplid.setScale(0.5);
		
		this.mutiao = new cc.Sprite("#mutiao.png");
		this.mutiao.setPosition(cc.p(580,450));
		this.layout2.addChild(this.mutiao,3);
		this.mutiao.setScale(0.5);
		
		this.mfire = new cc.Sprite("#fire4/1.png");
		this.mfire.setPosition(cc.p(15,45));
		this.mutiao.addChild(this.mfire,5);		
		this.mfire.setVisible(false);
		this.mfire.runAction(this.addAction("fire4/", 2, 0.1));
				
		this.addbutton(this.layout2, TAG_BUTTON, "播放", cc.p(250,180));
		this.layout2.getChildByTag(TAG_BUTTON).getChildByTag(TAG_BUTTON).setColor(cc.color(255, 255, 255, 250));
		
		this.label1 = new cc.LabelTTF("现象：带火星的木条在氧气瓶中复燃。",gg.fontName,gg.fontSize4);
		this.label1.setColor(cc.color(255, 0, 0, 250));
		this.label1.setAnchorPoint(0,0.5);
		this.label1.setPosition(80,80);
		this.layout2.addChild(this.label1);
		this.label1.setOpacity(0);		
	},
	addlayout3:function(parent){
		this.layout3 = new ccui.Layout();//第四页
		this.layout3.setContentSize(cc.size(996, 598));
		parent.addLayer(this.layout3);

		label1 = new cc.LabelTTF("　　将装有燃烧的硫的燃烧匙伸入充满氧气的集气瓶里，观察现象。",gg.fontName,gg.fontSize4);
		label1.setAnchorPoint(0,0.5);
		label1.setPosition(0,this.layout3.height-label1.height/2);
		label1.setColor(cc.color(255, 255, 255, 250));
		this.layout3.addChild(label1,10);	

		var bg = new cc.Sprite(res_public.run_back);
		this.layout3.addChild(bg);
		bg.setScale(0.8);
		bg.setPosition(cc.p(this.layout3.width /2, this.layout3.height / 2));

		this.addImage(this.layout3, 5, "#lamp.png", cc.p(650,200), 0.5);
		this.addImage(this.layout3, 5, "#match3.png", cc.p(800,180), 0.5);
		this.addImage(this.layout3, 5, "#botttle.png", cc.p(400,255), 0.88);

		this.match1 = new cc.Sprite("#match4.png");
		this.match1.setPosition(cc.p(855,235));
		this.layout3.addChild(this.match1,5);
		this.match1.setScale(0.5);

		this.glass1 = new cc.Sprite("#glass.png");
		this.glass1.setPosition(cc.p(400,362));
		this.layout3.addChild(this.glass1,5);
		this.glass1.setScale(0.7);

		this.fire1 = new cc.Sprite("#fire/1.png");
		this.fire1.setPosition(cc.p(652,277));
		this.layout3.addChild(this.fire1,5);
		this.fire1.setVisible(false);
		this.fire1.runAction(this.addAction("fire/", 3, 0.05));

		this.lamplid1 = new cc.Sprite("#lamplid.png");
		this.lamplid1.setPosition(cc.p(652,255));
		this.layout3.addChild(this.lamplid1,5);
		this.lamplid1.setScale(0.5);

		this.spoon = new cc.Sprite("#spoon3.png");
		this.spoon.setPosition(cc.p(580,450));
		this.layout3.addChild(this.spoon,3);
		this.spoon.setScale(0.5);

		this.sfire = new cc.Sprite("#kongqi/1.png");
		this.sfire.setPosition(cc.p(30,60));
		this.spoon.addChild(this.sfire,5);		
		this.sfire.setVisible(false);
		this.sfire.setScale(1.5);
		
		this.sfire1 = new cc.Sprite("#yangqi/8.png");
		this.sfire1.setPosition(cc.p(30,60));
		this.spoon.addChild(this.sfire1,4);		
		this.sfire1.setOpacity(0);
		this.sfire1.setScale(0.5);	

		this.addbutton(this.layout3, TAG_BUTTON1, "播放", cc.p(250,180));
		this.layout3.getChildByTag(TAG_BUTTON1).getChildByTag(TAG_BUTTON1).setColor(cc.color(255, 255, 255, 250));

		this.label2 = new cc.LabelTTF("现象：发出蓝色光，生成了一种具有刺激性气味的气体，\n　　并放出大量热。",gg.fontName,gg.fontSize4);
		this.label2.setColor(cc.color(255, 0, 0, 250));
		this.label2.setAnchorPoint(0,0.5);
		this.label2.setPosition(80,80);
		this.layout3.addChild(this.label2);
		this.label2.setOpacity(0);		
		
		
		
	},
	addlayout4:function(parent){
		this.layout4 = new ccui.Layout();//第一页
		this.layout4.setContentSize(cc.size(996, 598));//设置layout的大小
		parent.addLayer(this.layout4);

		label1 = new cc.LabelTTF("　　取一根光亮的细铁丝绕成螺旋状，一端系上一根火柴，另一端系在一\n根粗铁丝上，点燃火柴，待火柴将要烧完时，" +
				"立即伸入盛有氧气，瓶底预\n先装有少量水的集气瓶中，观察现象。",gg.fontName,gg.fontSize4);
		label1.setAnchorPoint(0,0.5);
		label1.setPosition(0,this.layout4.height-label1.height/2);
		label1.setColor(cc.color(255, 255, 255, 250));
		this.layout4.addChild(label1,10);	

		var bg = new cc.Sprite(res_public.run_back);
		this.layout4.addChild(bg);
		bg.setScale(0.8);
		bg.setPosition(cc.p(this.layout4.width /2, this.layout4.height / 2));

		this.addImage(this.layout4, 5, "#lamp.png", cc.p(650,200), 0.5);
		this.addImage(this.layout4, 5, "#match3.png", cc.p(800,180), 0.5);
		this.addImage(this.layout4, 5, "#botttle.png", cc.p(400,255), 0.88);

		this.match2 = new cc.Sprite("#match4.png");
		this.match2.setPosition(cc.p(855,235));
		this.layout4.addChild(this.match2,5);
		this.match2.setScale(0.5);

		this.glass2 = new cc.Sprite("#glass.png");
		this.glass2.setPosition(cc.p(400,362));
		this.layout4.addChild(this.glass2,5);
		this.glass2.setScale(0.7);

		this.fire2 = new cc.Sprite("#fire/1.png");
		this.fire2.setPosition(cc.p(652,277));
		this.layout4.addChild(this.fire2,5);
		this.fire2.setVisible(false);
		this.fire2.runAction(this.addAction("fire/", 3, 0.05));

		this.lamplid2 = new cc.Sprite("#lamplid.png");
		this.lamplid2.setPosition(cc.p(652,255));
		this.layout4.addChild(this.lamplid2,5);
		this.lamplid2.setScale(0.5);
		
		this.jiazi = new cc.Sprite("#jiazi.png");
		this.jiazi.setPosition(cc.p(580,450));
		this.layout4.addChild(this.jiazi,4);
		this.jiazi.setScale(0.7);
		
		this.addImage(this.jiazi, 5, "#jiazi1.png", cc.p(120,150), 1);
		
		this.tiesi = new cc.Sprite("#tiesi.png");
		this.tiesi.setPosition(cc.p(25,25));
		this.jiazi.addChild(this.tiesi,3);
		this.tiesi.setAnchorPoint(0.5,1);
	
		this.huocai = new cc.Sprite("#match.png");
		this.huocai.setPosition(cc.p(20,-60));
		this.jiazi.addChild(this.huocai,5);
		this.huocai.setCascadeOpacityEnabled(true);
		
		this.huocai1 = new cc.Sprite("#match1.png");
		this.huocai1.setPosition(cc.p(20,-60));
		this.jiazi.addChild(this.huocai1,5);
		this.huocai1.setOpacity(0);
		
		this.hfire = new cc.Sprite("#fire4/1.png");
		this.hfire.setPosition(cc.p(5,15));
		this.huocai.addChild(this.hfire,5);
		this.hfire.setOpacity(0);
		this.hfire.setScale(0.3);
		this.hfire.runAction(this.addAction("fire4/", 2, 0.1));
		
		this.tfire = new cc.Sprite("#fire2/1.png");
		this.tfire.setPosition(cc.p(15,35));
		this.tiesi.addChild(this.tfire,5);
		this.tfire.setOpacity(0);
		this.tfire.runAction(this.addAction("fire2/", 2, 0.1));
		
		this.tfire1 = new cc.Sprite("#fire1/1.png");
		this.tfire1.setPosition(cc.p(400,260));
		this.layout4.addChild(this.tfire1,3);
		this.tfire1.setScale(0.8);
		this.tfire1.setOpacity(0);
		this.tfire1.runAction(this.addAction("fire1/", 2, 0.1));

		this.black = new cc.Sprite("#black.png");
		this.black.setPosition(cc.p(400,155));
		this.layout4.addChild(this.black,10);		
		this.black.setOpacity(0);
		this.black.setScale(0.6);
		
		this.addbutton(this.layout4, TAG_BUTTON2, "播放", cc.p(250,180));
		this.layout4.getChildByTag(TAG_BUTTON2).getChildByTag(TAG_BUTTON2).setColor(cc.color(255, 255, 255, 250));

		this.label3 = new cc.LabelTTF("现象：铁丝在氧气瓶里剧烈燃烧，火光四溅，并放出大量热，\n　　　生成一种黑色的固体。" +
				"\n结论：三个实验证明氧气时活泼气体，可以跟某些单质发生化合反应。",gg.fontName,gg.fontSize4);
		this.label3.setColor(cc.color(255, 0, 0, 250));
		this.label3.setAnchorPoint(0,0.5);
		this.label3.setPosition(80,80);
		this.layout4.addChild(this.label3);
		this.label3.setOpacity(0);		

     

	},
	moveMtch:function(obj,obj1,obj2){
		obj.setVisible(true);
		var seq = cc.sequence(cc.moveTo(0.5,cc.p(910,205)),cc.callFunc(function(){
			obj.setSpriteFrame("match5.png");
			var se = cc.sequence(cc.moveTo(0.5,cc.p(650,290)),cc.moveTo(0.5,cc.p(580,270)),cc.moveTo(0.5,cc.p(560,170)));
			obj1.runAction(se);
		},this),cc.moveTo(1,cc.p(692,275)),cc.delayTime(0.5),cc.callFunc(function(){
			obj.setVisible(false);
			obj.setSpriteFrame("match4.png");
			obj2.setVisible(true);
		
		},this));
		obj.runAction(seq);
	},
	moveMutiao:function(){
		var seq = cc.sequence(cc.moveTo(0.5,cc.p(678,335)),cc.callFunc(function(){
			this.mutiao.setSpriteFrame("mutiao1.png");
			this.glass.runAction(cc.moveTo(0.5,cc.p(350,362)));
		},this),cc.delayTime(1),cc.moveTo(1,cc.p(470,470)),cc.moveTo(0.3,cc.p(450,420)),cc.callFunc(function(){		
			this.mfire.setVisible(true);
			
		},this),cc.moveTo(0.5,cc.p(430,350)));
		this.mutiao.runAction(seq);
	},
	moveSpoon:function(){
		var seq = cc.sequence(cc.moveTo(0.5,cc.p(650,380)),cc.callFunc(function(){
			this.sfire.stopAllActions();
			this.sfire.runAction(this.addAction("kongqi/", 7, 0.1));
			this.sfire.setVisible(true);
			this.glass1.runAction(cc.moveTo(0.5,cc.p(350,362)));
		},this),cc.delayTime(1),cc.moveTo(1,cc.p(430,480)),cc.moveTo(0.3,cc.p(420,430)),cc.callFunc(function(){	
			this.sfire.stopAllActions();
			this.sfire.runAction(this.addAction("yangqi/", 7, 0.1));
			this.sfire.setPosition(cc.p(30,130));
			this.sfire1.setOpacity(255);
			this.sfire1.runAction(cc.spawn(this.addfire(),cc.moveTo(0.5,cc.p(10,50)),cc.scaleTo(0.5,1.3)));
		},this),cc.moveTo(0.5,cc.p(410,360)),cc.delayTime(1),cc.callFunc(function(){
			this.sfire1.runAction(cc.fadeTo(1,100));
		},this));
		this.spoon.runAction(seq);
	},
	moveJiazi:function(){
		var seq = cc.sequence(cc.moveTo(0.5,cc.p(735,440)),cc.callFunc(function(){
			this.huocai.runAction(cc.fadeOut(3));
			this.huocai1.runAction(cc.fadeIn(3));
			this.hfire.setOpacity(255);
			
			this.hfire.setVisible(true);
			this.glass2.runAction(cc.moveTo(0.5,cc.p(330,362)));
		},this),cc.delayTime(1),cc.moveTo(1,cc.p(500,530)),cc.delayTime(0.5),cc.moveTo(0.5,cc.p(480,450)),cc.callFunc(function(){	
			this.huocai1.runAction(cc.fadeTo(0.2,0));
			this.tfire.setOpacity(255);			
			this.tfire1.runAction(cc.spawn(cc.fadeIn(0.5),this.addAction("fire1/", 2, 0.1)));
		},this),cc.moveTo(0.5,cc.p(460,380)),cc.callFunc(function(){
			this.tiesi.runAction(cc.scaleTo(2,1,0.5));
			this.tfire.runAction(cc.spawn(cc.moveTo(2,cc.p(15,90)),cc.scaleTo(2,1,1.5)));
			this.tfire1.runAction(cc.moveTo(2,cc.p(400,280)));
			this.black.runAction(cc.fadeIn(2));
		},this));
		this.jiazi.runAction(seq);
	},
	addAction:function(str1,num,time){
		var frames = [];
		for(var i =1 ;i<=num;i++){
			var str = str1+i+".png";
			var frame=cc.spriteFrameCache.getSpriteFrame(str);
			frames.push(frame);		
		}
		var animation = new cc.Animation(frames,time);//负责动画序列
		var action = new cc.Animate(animation);//帧动画的动作创建
		return action.repeatForever();
	},
	addfire:function(){
		var frames = [];
		for(var i =8 ;i<=12;i++){
			var str = "yangqi/"+i+".png";
			var frame=cc.spriteFrameCache.getSpriteFrame(str);
			frames.push(frame);		
		}
		var animation = new cc.Animation(frames,0.1);//负责动画序列
		var action = new cc.Animate(animation);//帧动画的动作创建
		return action;
	},
	addImage:function(parent,index,str,pos,scale){
		var image = new cc.Sprite(str);
		parent.addChild(image,index);
		image.setPosition(pos);
		if(scale !=null){
			image.setScale(scale);
		}else{
			image.setScale(1);
		}
	},
	addbutton:function(parent,tag,str,pos){
		var button = new ButtonScale(parent,"#unsel.png",this.callback,this);
		button.setPosition(pos);
		button.setLocalZOrder(5);
		button.setTag(tag);
		var label = new cc.LabelTTF(str,gg.fontName,30);
		label.setPosition(button.width/2,button.height/2);		
		label.setColor(cc.color(0, 0, 0, 250));
		button.addChild(label);
		label.setTag(tag);
	},
	callback:function(p){
		switch(p.getTag()){	
		case TAG_BUTTON:
			this.reset = !this.reset;
			if(!this.reset){
				p.setEnable(false);
				var seq = cc.sequence(cc.callFunc(function(){
					this.moveMtch(this.match,this.lamplid,this.fire);
				},this),cc.delayTime(2),cc.callFunc(function(){
					this.moveMutiao();
				},this),cc.delayTime(3),cc.callFunc(function(){
					this.layout2.getChildByTag(TAG_BUTTON).getChildByTag(TAG_BUTTON).setString("复原");
					this.layout2.getChildByTag(TAG_BUTTON).setEnable(true);
					this.label1.setOpacity(255);
				},this));
				p.runAction(seq);
					
			}else{
				this.layout2.getChildByTag(TAG_BUTTON).getChildByTag(TAG_BUTTON).setString("播放");
				this.fire.setVisible(false);
				this.mfire.setVisible(false);
				this.lamplid.setPosition(cc.p(652,255));
				this.match.setPosition(cc.p(855,235));
				this.mutiao.setPosition(cc.p(580,450));
				this.mutiao.setSpriteFrame("mutiao.png");
				this.glass.setPosition(cc.p(400,362));			
			}
		break;
		case TAG_BUTTON1:
			this.reset1 = !this.reset1;
			if(!this.reset1){
				p.setEnable(false);
				var seq = cc.sequence(cc.callFunc(function(){
					this.moveMtch(this.match1,this.lamplid1,this.fire1);
				},this),cc.delayTime(2),cc.callFunc(function(){
					this.moveSpoon();
				},this),cc.delayTime(5),cc.callFunc(function(){
					this.layout3.getChildByTag(TAG_BUTTON1).getChildByTag(TAG_BUTTON1).setString("复原");
					this.layout3.getChildByTag(TAG_BUTTON1).setEnable(true);
					this.label2.setOpacity(255);
				},this));
				p.runAction(seq);

			}else{
				this.layout3.getChildByTag(TAG_BUTTON1).getChildByTag(TAG_BUTTON1).setString("播放");
				this.fire1.setVisible(false);
				this.sfire.setVisible(false);
				this.sfire.setPosition(30,60);
				this.sfire1.setOpacity(0);
				this.sfire1.setScale(0.5);
				this.sfire1.setPosition(30,60);
				this.lamplid1.setPosition(cc.p(652,255));
				this.match1.setPosition(cc.p(855,235));
				this.glass1.setPosition(cc.p(400,362));
				this.spoon.setPosition(cc.p(580,450));
	
			}
			break;
		case TAG_BUTTON2:
			this.reset2 = !this.reset2;
			if(!this.reset2){
				p.setEnable(false);
				var seq = cc.sequence(cc.callFunc(function(){
					this.moveMtch(this.match2,this.lamplid2,this.fire2);
				},this),cc.delayTime(2),cc.callFunc(function(){
					this.moveJiazi();
				},this),cc.delayTime(5),cc.callFunc(function(){
					this.layout4.getChildByTag(TAG_BUTTON2).getChildByTag(TAG_BUTTON2).setString("复原");
					this.layout4.getChildByTag(TAG_BUTTON2).setEnable(true);
					this.label3.setOpacity(255);
				},this));
				p.runAction(seq);

			}else{
				this.layout4.getChildByTag(TAG_BUTTON2).getChildByTag(TAG_BUTTON2).setString("播放");		
				this.black.setOpacity(0);
				this.jiazi.setPosition(cc.p(580,450));
				this.huocai.setOpacity(255);
				//this.huocai.setSpriteFrame("match.png");
				this.hfire.setOpacity(0);
				this.tiesi.setScale(1);
				this.tfire.setOpacity(0);
				this.tfire.setScale(1);
				this.tfire.setPosition(cc.p(15,35));
				this.tfire1.setOpacity(0);
				this.tfire1.setPosition(400,260);
				this.glass2.setPosition(cc.p(400,362));
				this.lamplid2.setPosition(cc.p(652,255));
				this.match2.setPosition(cc.p(855,235));
				this.fire2.setVisible(false);
			}
			break;
		}
	}	
});
