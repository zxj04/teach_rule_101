exp05.Beaker06 = cc.Node.extend({
	ctor:function(p){
		this._super();
		p.addChild(this, 10, TAG_BEAKER_NODE);
		this.init();
	},
	init : function(){
		this.setCascadeOpacityEnabled(true);
		var beaker= new Button(this, 8, TAG_BEAKER, "#beaker.png",this.callback);
		beaker.setPosition(0,50);
		beaker.setScale(0.6);


		this.musai1= new cc.Sprite("#musai21.png");		
		this.musai1.setPosition(0,150);
		this.musai1.setScale(0.6,0.1);
		this.addChild(this.musai1,10);

		this.musai2= new cc.Sprite("#musai22.png");		
		this.musai2.setPosition(0,85);
		this.musai2.setScale(0.6,0.1);
		this.addChild(this.musai2,6);
		
		this.waterline= new cc.Sprite("#beakerline.png");		
		this.waterline.setPosition(5,80);
		this.waterline.setScale(0.6);
		this.addChild(this.waterline,5);

	},


	callback:function(p){
		var action = gg.flow.flow.action;
		var func = cc.callFunc(this.actionDone, this);	
		switch(p.getTag()){	
		case TAG_BEAKER:
			var seq  = cc.sequence(cc.callFunc(function(){
				var seq = cc.sequence(cc.callFunc(function(){
					this.waterline.runAction(cc.scaleTo(2,1,0.6));
				},this),cc.spawn(cc.moveTo(1,cc.p(0,50)),cc.rotateTo(1,50)));
				this.waterline.runAction(seq);
			},this),cc.spawn(cc.moveTo(1,cc.p(1000,320)),cc.rotateTo(1,-50)),cc.callFunc(function(){
				var flow =  new cc.Sprite("#flow_right.png");
				flow.setAnchorPoint(1,1);
				flow.setPosition(845,345);
				flow.setRotation(5);
				flow.setScale(0);
				this.getParent().addChild(flow);
				var seq = cc.sequence(cc.scaleTo(0.5,2),cc.delayTime(0.8),cc.fadeOut(0.5));
				flow.runAction(seq);
				ll.run.bottle.upbottleline();
			},this),cc.delayTime(2),cc.callFunc(function(){
				var seq = cc.sequence(cc.callFunc(function(){
					this.waterline.runAction(cc.scaleTo(0.5,0.6,0.6));
				},this),cc.spawn(cc.moveTo(1,cc.p(5,50)),cc.rotateTo(1,0)));
				this.waterline.runAction(seq);
			},this),cc.spawn(cc.moveTo(1,cc.p(1152,152)),cc.rotateTo(1,0)));			
			this.runAction(seq);
			
			var seq1 = cc.sequence(cc.delayTime(4),cc.callFunc(this.flowNext,this));
			p.runAction(seq1);
			break;
		}
	},
	flowNext:function(){
		gg.flow.next();
	},
});