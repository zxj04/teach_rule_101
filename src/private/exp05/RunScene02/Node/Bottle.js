exp05.Bottle1 = cc.Node.extend({
	ctor:function(p){
		this._super();
		p.addChild(this, 10, TAG_BOTTLE_NODE);
		this.init();
		//this.addpartline();
	},
	init : function(){
		this.setCascadeOpacityEnabled(true);
		this.bottle= new Button(this, 10, TAG_BOTTLE, "#bottle.png",this.callback);
		this.bottle.setPosition(0,50);
		this.bottle.setScale(0.6);

        this.musainode = new exp05.Musai(this);
        this.musainode.setPosition(6,200);
//		this.musai= new Button(this, 8, TAG_MUSAI, "#musai1.png",this.callback);
//		this.musai.setPosition(6,195);
//		this.musai.setScale(0.6);
//		
//		this.jiazi= new Button(this.musai, 8, TAG_JIAZI, "#jiazi.png",this.callback,this);
//		this.jiazi.setPosition(920,480);
//		this.jiazi.setScale(0.7);
//		
//		this.jiazibg= new cc.Sprite("#jiazibg.png");
//		this.jiazibg.setPosition(920,478);
//		this.jiazibg.setScale(0.7);
//		this.musai.addChild(this.jiazibg);
		
		this.bottleline= new cc.Sprite("#pbottleline.png");		
		this.bottleline.setPosition(3,-65);
		this.bottleline.setScale(0.55);
		this.bottleline.setVisible(false);
		this.addChild(this.bottleline,7);//加水
		
	},
    upbottleline:function(){
    	this.bottleline.setVisible(true);
    	var seq = cc.sequence(cc.spawn(cc.moveTo(1,cc.p(3,-37)),cc.scaleTo(0.2,0.6)));
    	this.bottleline.runAction(seq);
	
    },
    addpartline:function(){
    	this.partline= new cc.Sprite("#partline.png");		
    	this.partline.setPosition(0,30);
    	this.partline.setScale(0.6);
    	this.addChild(this.partline,11);
    },   
    fire:function(){
    	var frames=[];
    	for (i=1;i<=2;i++){
    		var str ="fire4/"+i+".png";
    		var frame=cc.spriteFrameCache.getSpriteFrame(str);
    		frames.push(frame);
    	}
    	var animation = new cc.Animation(frames,0.1);//负责动画序列
    	var action = new cc.Animate(animation);//帧动画的动作创建
    	return action;
    },
    smoke:function(){
    	var frames=[];
    	for (i=1;i<=11;i++){
    		var str ="smoke2/"+i+".png";
    		var frame=cc.spriteFrameCache.getSpriteFrame(str);
    		frames.push(frame);
    	}
    	var animation = new cc.Animation(frames,0.1);//负责动画序列
    	var action = new cc.Animate(animation);//帧动画的动作创建
    	return action;
    },
	callback:function(p){
		var action = gg.flow.flow.action;
		var func = cc.callFunc(this.actionDone, this);	
		switch(p.getTag()){	
		case TAG_MUSAI:
			if(action==ACTION_DO1){	
				//this.jiazi.runAction(cc.moveBy(1,cc.p(0,200)));
				//this.jiazibg.runAction(cc.moveBy(1,cc.p(0,200)));
				var seq = cc.sequence(cc.moveBy(0.5,cc.p(0,90)),cc.callFunc(function(){
					ll.run.beaker.musai2.setVisible(false);
				},this),cc.moveBy(0.3,cc.p(0,70)),cc.callFunc(function(){
					ll.run.beaker.musai1.setVisible(false);
				},this),cc.moveBy(0.2,cc.p(0,40)),cc.callFunc(this.flowNext,this));			
			}else if(action==ACTION_DO2){
				var seq = cc.sequence(cc.moveTo(1,cc.p(-220,345)),cc.delayTime(1),cc.callFunc(function(){
					var fire = new cc.Sprite("#fire4/1.png");
					fire.setPosition(cc.p(560,195));
					p.addChild(fire,1,TAG_SHOW);
					fire.runAction(this.fire().repeatForever());
				},this),cc.delayTime(0.5),cc.moveTo(1,cc.p(6,395)),cc.moveBy(0.2,cc.p(0,-40)),cc.callFunc(function(){
					var smoke = new cc.Sprite("#smoke2/1.png");
					smoke.setPosition(cc.p(4,61));
					smoke.setScale(0.6);
					this.addChild(smoke,9,TAG_SHOW);
					smoke.runAction(this.smoke());
					
					ll.run.beaker.musai1.setVisible(true);
				},this),cc.moveBy(0.3,cc.p(0,-70)),cc.callFunc(function(){
					ll.run.beaker.musai2.setPosition(0,55);
					ll.run.beaker.musai2.setVisible(true);
					
				},this),cc.moveBy(0.5,cc.p(0,-90)),cc.delayTime(1),cc.callFunc(this.flowNext,this));
			}
			p.runAction(seq);
			break;
		case TAG_BOTTLE:
			this.addpartline();
			gg.flow.next();
			break;
		}
	},
	flowNext:function(){
		gg.flow.next();
	},
});