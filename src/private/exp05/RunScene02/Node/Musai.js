exp05.Musai = cc.Node.extend({
	ctor:function(p){
		this._super();
		p.addChild(this, 9, TAG_MUSAI_NODE);
		this.init();
	},
	init : function(){
		this.setCascadeOpacityEnabled(true);	

		this.musai= new Button(this, 8, TAG_MUSAI, "#musai1.png",this.callback);
		this.musai.setScale(0.6);

		this.jiazi= new Button(this, 9, TAG_JIAZI, "#jiazi.png",this.callback);
		this.jiazi.setPosition(189,57);
		this.jiazi.setScale(0.7);

		this.jiazibg= new cc.Sprite("#jiazibg.png");
		this.jiazibg.setPosition(189,55);
		this.jiazibg.setScale(0.7);
		this.addChild(this.jiazibg);


	},
	fire:function(){
		var frames=[];
		for (i=1;i<=2;i++){
			var str ="fire4/"+i+".png";
			var frame=cc.spriteFrameCache.getSpriteFrame(str);
			frames.push(frame);
		}
		var animation = new cc.Animation(frames,0.1);//负责动画序列
		var action = new cc.Animate(animation);//帧动画的动作创建
		return action;
	},
	smoke:function(){
		var frames=[];
		for (i=1;i<=11;i++){
			var str ="smoke2/"+i+".png";
			var frame=cc.spriteFrameCache.getSpriteFrame(str);
			frames.push(frame);
		}
		var animation = new cc.Animation(frames,0.1);//负责动画序列
		var action = new cc.Animate(animation);//帧动画的动作创建
		return action;
	},
	smokeend:function(){
		var frames=[];
		for (i=1;i<=11;i++){
			var str ="smokeend/"+i+".png";
			var frame=cc.spriteFrameCache.getSpriteFrame(str);
			frames.push(frame);
		}
		var animation = new cc.Animation(frames,0.1);//负责动画序列
		var action = new cc.Animate(animation);//帧动画的动作创建
		return action;
	},
	callback:function(p){
		var action = gg.flow.flow.action;
		var func = cc.callFunc(this.actionDone, this);	
		switch(p.getTag()){	
		case TAG_MUSAI:
			if(action==ACTION_DO1){	
				var seq = cc.sequence(cc.moveBy(0.5,cc.p(0,90)),cc.callFunc(function(){
					ll.run.beaker.musai2.setVisible(false);
				},this),cc.moveBy(0.3,cc.p(0,70)),cc.callFunc(function(){
					ll.run.beaker.musai1.setVisible(false);
				},this),cc.moveBy(0.2,cc.p(0,40)),cc.callFunc(this.flowNext,this));			
			}else if(action==ACTION_DO2){
				var seq = cc.sequence(cc.moveTo(1,cc.p(-220,345)),cc.delayTime(1),cc.callFunc(function(){
					var fire = new cc.Sprite("#fire4/1.png");
					fire.setPosition(cc.p(560,195));
					p.addChild(fire,1,TAG_SHOW);
					fire.runAction(this.fire().repeatForever());
				},this),cc.delayTime(0.5),cc.moveTo(1,cc.p(6,395)),cc.moveBy(0.2,cc.p(0,-40)),cc.callFunc(function(){
					var smoke = new cc.Sprite("#smoke2/1.png");
					smoke.setPosition(cc.p(4,163));
					smoke.setAnchorPoint(0.5, 1);
					smoke.setScale(0.6);
					this.getParent().addChild(smoke,9,TAG_SHOW);
					smoke.runAction(this.smoke());

					ll.run.beaker.musai1.setVisible(true);
				},this),cc.moveBy(0.3,cc.p(0,-70)),cc.callFunc(function(){
					ll.run.beaker.musai2.setPosition(0,55);
					ll.run.beaker.musai2.setVisible(true);
				},this),cc.moveBy(0.5,cc.p(0,-90)),cc.delayTime(1),cc.callFunc(function(){
					p.getChildByTag(TAG_SHOW).removeFromParent(true);
					p.getChildByTag(TAG_SHOW3).removeFromParent(true);
					gg.flow.next();
				},this));
			}
			this.runAction(seq);
			break;
		case TAG_JIAZI:
		    
		    this.jiazibg.runAction(cc.moveBy(0.5,cc.p(-60,0)));
		    var seq = cc.sequence(cc.moveBy(0.5,cc.p(-60,0)),cc.callFunc(function(){
		    	ll.run.bottle.bottleline.runAction(cc.moveBy(1,cc.p(0,33)));
		    	ll.run.beaker.waterline.runAction(cc.moveBy(1,cc.p(0,-15)));
		    	ll.run.bottle.getChildByTag(TAG_SHOW).runAction(cc.spawn(this.smokeend(),cc.fadeTo(1,100)));
		    	
		    	var flow = new cc.Sprite("#flow.png");
		    	flow.setPosition(26,137);
		    	ll.run.bottle.addChild(flow);
		    	flow.setAnchorPoint(0.5,1);
		    	flow.setScale(0);
		    	var seq = cc.sequence(cc.scaleTo(0.5,2),cc.delayTime(1),cc.fadeOut(0.5));
		    	flow.runAction(seq);
		    },this),cc.delayTime(5),cc.callFunc(function(){
		    	gg.flow.next();
		    },this));
		    p.runAction(seq);
			break;
		}
	},
	flowNext:function(){
		gg.flow.next();
	},
});