exp05.Pbottle = cc.Node.extend({
	ctor:function(p){
		this._super();
		p.addChild(this, 10, TAG_PBOTTLE_NODE);
		this.init();
	},
	init : function(){
		this.setCascadeOpacityEnabled(true);
		this.pbottle= new Button(this, 11, TAG_PBOTTLE, "#pbottle.png",this.callback);
		this.pbottle.setPosition(0,50);
		this.pbottle.setScale(0.6);


		var lid= new Button(this, 10, TAG_LID, "#plid.png",this.callback);
		lid.setPosition(0,140);
		lid.setScale(0.75);

		var spoon= new Button(this, 9, TAG_SPOON, "#spoon1.png",this.callback);
		spoon.setPosition(0,470);
		spoon.setScale(0.6);

		
	},
	

	callback:function(p){
		var action = gg.flow.flow.action;
		var func = cc.callFunc(this.actionDone, this);	
		switch(p.getTag()){	
		case TAG_LID:
			if(action==ACTION_DO1){				
				var seq = cc.sequence(cc.moveTo(0.3,cc.p(0,200)),cc.moveTo(0.2,cc.p(-50,200)),cc.spawn(cc.moveTo(1,cc.p(-160,-20)),cc.rotateTo(1,-180)),
						cc.callFunc(this.flowNext,this));			
			}else if(action==ACTION_DO2){
				var seq = cc.sequence(cc.spawn(cc.moveTo(1,cc.p(-50,200)),cc.rotateTo(1,0)),cc.moveTo(0.2,cc.p(0,200)),cc.moveTo(0.3,cc.p(0,140)),
						cc.callFunc(this.flowNext,this));
			}
			p.runAction(seq);
			break;
		case TAG_SPOON:
			var seq = cc.sequence(cc.callFunc(function(){
				this.pbottle.runAction(cc.rotateTo(0.5,-20));
			},this),cc.moveTo(1,cc.p(-105,250)),cc.spawn(cc.moveTo(0.8,cc.p(-10,90)),cc.rotateTo(0.8,5)),cc.callFunc(function(){
				var p1 = new cc.Sprite("#p1.png");
				p1.setPosition(250,65);
				p.addChild(p1,1,TAG_SHOW);
			},this),cc.delayTime(0.5),cc.spawn(cc.moveTo(0.8,cc.p(-105,250)),cc.rotateTo(0.8,0)),cc.callFunc(function(){
				this.pbottle.runAction(cc.rotateTo(0.5,0));
			},this),cc.moveTo(1,cc.p(350,350)),cc.rotateTo(0.3,0,20),cc.callFunc(function(){
				p.getChildByTag(TAG_SHOW).removeFromParent(true);
				var p2 = new cc.Sprite("#p2.png");
				p2.setPosition(560,107);
				p2.setScale(2);
				ll.run.bottle.musainode.musai.addChild(p2,10,TAG_SHOW3);
			},this),cc.spawn(cc.moveTo(1,cc.p(0,470)),cc.rotateTo(0.3,0)),cc.callFunc(function(){
				this.setOpacity(0);
				//this.removeFromParent(true);
				gg.flow.next();
			},this));
			p.runAction(seq);
		break;
		}
	},
	flowNext:function(){
		gg.flow.next();
	},
});