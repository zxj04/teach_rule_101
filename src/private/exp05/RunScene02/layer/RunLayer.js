exp05.RunLayer2 = cc.Layer.extend({
	arr:null,
	scene:null,
	clock:null,
	ctor:function (parent) {
		this._super();
		this.scene = parent;
		this.scene.addChild(this, 10);
		gg.main = this;
		this.init();
	},
	init:function () {
		this.callNext = cc.callFunc(function(){
			gg.flow.next();
		}, this);
		this.callKill = cc.callFunc(function(p){
			var seq = cc.sequence(cc.fadeOut(0.5),cc.callFunc(function(){
				p.removeFromParent(true);	
			}, this));
			p.runAction(seq);
		}, this); 

		this.callNext.retain();
		this.callKill.retain();
		//时钟
		this.clock = new Clock(this);
		// 物品库
		this.lib = new Lib(this);
		this.lib.loadBg([{	
			tag:TAG_LIB_CUSPOON,
			checkright:true,
		},
		{
			tag:TAG_LIB_BOTTLE,
		},
		{		
			tag:TAG_LIB_MATCH,
			checkright:true,
		},
		{
			tag:TAG_LIB_AMPULLA,
			checkright:true,
		},
		{	
			tag:TGA_LIB_GLASS,
		},
		{
			tag:TAG_LIB_PBOTTLE,
			checkright:true,
		},
		{
			tag:TAG_LIB_TESTTUBE,
		},
		{	
			tag:TAG_LIB_BEAKER,
			checkright:true,
		},
		{
			tag:TAG_LIB_LAMP,
			checkright:true,
		},
		{
			tag:TAG_LIB_CONICAL,
		}
		]);

		this.pbottle = new exp05.Pbottle(this);
		this.pbottle.setVisible(false);
		this.pbottle.setPosition(370,150);

		this.match = new exp05.Match1(this);
		this.match.setVisible(false);
		this.match.setPosition(600,600);

		this.lamp = new exp05.Lamp2(this);
		this.lamp.setVisible(false);
		this.lamp.setPosition(550,220);

		this.bottle = new exp05.Bottle1(this);
		this.bottle.setVisible(false);
		this.bottle.setPosition(800,160);

		this.beaker = new exp05.Beaker06(this);
		this.beaker.setVisible(false);
		this.beaker.setPosition(1152,152);

	},
	checkVisible:function(next){
		//是否可见
		var checkVisible = [];
		var node1 = ll.run.pbottle;
		var node2 = ll.run.match;
		var node3 = ll.run.lamp;
		var node4 = ll.run.bottle;
		var node5 = ll.run.beaker;

		checkVisible.push(node1,node2,node3,node4,node5);

		for(var i in checkVisible){
			if(checkVisible[i] !== null){				
				checkVisible[i].setVisible(next);				
			}			
		}
	},
	loadInLib:function(obj, pos, tarPos,delay){
		obj.setPosition(pos);
		if(delay == null){
			delay = 1;
		}
		var ber = $.bezier(pos, tarPos, delay);
		var seq = cc.sequence(ber, this.callNext);
		obj.runAction(seq);
	},
	kill:function(obj){
		var fade = cc.fadeTo(0.5,0);
		var func = cc.callfunc(function(){
			obj.removeFromParent(true);
		},this);
		var seq = cc.sequence(fade,func);
		obj.runAction(seq)
	},

	callback:function (p){
		var func = cc.callFunc(this.actionDone, this);
		var action=gg.flow.flow.action;
		switch(p.getTag()){

		}
	},
	actionDone:function(p){
		var func = cc.callFunc(this.actionDone, this);
		switch(p.getTag()){

		}
	},
	flowNext:function(){
		gg.flow.next();
	},
	onExit:function(){
		this._super();
		this.callNext.release();
		this.callKill.release();
	}
});