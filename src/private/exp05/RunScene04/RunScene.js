exp05.RunLayer04 = cc.Layer.extend({
	backgroundLayer : null,
	mainLayar : null,
	ctor:function () {
		this._super();
		this.initFrames();
		this.loadBackground();
		this.loadMainLayer();
	},
	initFrames : function(){
		cc.spriteFrameCache.addSpriteFrames(exp05.res_run.run_p);
		gg.curRunSpriteFrame.push(exp05.res_run.run_p);
	},
	loadBackground : function(){
		this.backgroundLayer = new RunBackgroundLayer();
		this.addChild(this.backgroundLayer);
	},
	loadMainLayer : function(){
		this.mainLayar = new exp05.RunMainLayer04();
		this.addChild(this.mainLayar);
	}
});

exp05.RunScene04 = PScene.extend({
	onEnter:function () {
		this._super();
		gg.initTeach();
		var layer = new exp05.RunLayer04();
		this.addChild(layer);
	},
	
});
