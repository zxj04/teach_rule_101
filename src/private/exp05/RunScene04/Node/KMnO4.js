exp05.KMnO4 = cc.Node.extend({
	ctor:function(p){
		this._super();
		p.addChild(this, 10, TAG_K2MNO4_NODE);
		this.init();
	},
	init : function(){
		this.setCascadeOpacityEnabled(true);
		var lid= new Button(this, 8, TAG_K2MNO4LID, "#kmnlid.png",this.callback);
		lid.setPosition(0,63);
		lid.setScale(0.8);

		this.mno2bottle= new Button(this, 10, TAG_K2MNO4BOTTLE, "#kmn.png",this.callback);
		this.mno2bottle.setScale(0.8);
		this.mno2bottle.setCascadeOpacityEnabled(true);
		
		this.mno2bottle1 = new cc.Sprite("#kmn1.png");
		this.addChild(this.mno2bottle1);
		this.mno2bottle1.setScale(0.8);
		
		//"NH₃ + CO₂ + H₂O = NH₄HCO₃\n" 
		var label = new cc.LabelTTF("KMnO₄","微软雅黑",22);
		label.setPosition(56,77);
		label.setColor(cc.color(0,0,0));
		this.mno2bottle.addChild(label,11);
		
		var zhi= new Button(this, 10, TAG_PAPER, "#zhi.png",this.callback);
		zhi.setScale(0.8,1);
		zhi.setPosition(300,-60);
		
		var spoon= new Button(this,9, TAG_KMNSPOON, "#kmnspoon.png",this.callback);
		spoon.setScale(0.45);
		spoon.setRotation(50);
		spoon.setPosition(200,100);
		lid.setScale(0.8);
		
	},
	addfenmo : function(){
		var kmnfenmo = new cc.Sprite("#fenmo1.png");
		kmnfenmo.setPosition(55,15);
		this.getChildByTag(TAG_PAPER).addChild(kmnfenmo,1,TAG_SHOW);
	},	
	addzhi : function(){
		var zhi = new cc.Sprite("#zhi1.png");
		zhi.setPosition(275,42);
		zhi.setScale(1.33,1.67);
		ll.run.iron.getChildByTag(TAG_SG_NODE).getChildByTag(TAG_SG).addChild(zhi,5,TAG_SHOW);
		
//		var sg = new cc.Sprite("#sg.png");
//		sg.setPosition(190,49);
//		
//		//sg.setScale(1.33,1.67);
//		ll.run.iron.getChildByTag(TAG_SG_NODE).getChildByTag(TAG_SG).addChild(sg,2,TAG_SHOW1);
	},
	callback:function(p){
		var action = gg.flow.flow.action;
		var func = cc.callFunc(this.actionDone, this);	
		switch(p.getTag()){	
		case TAG_K2MNO4LID:
			var seq = cc.sequence(cc.moveTo(0.5,cc.p(0,103)),cc.moveTo(0.5,cc.p(-50,93)),cc.spawn(cc.moveTo(1,cc.p(-100,-60)),cc.rotateTo(1,-180)),
					cc.callFunc(function(){
						p.setSpriteFrame("kmnlid1.png");
						p.setRotation(0);
						this.flowNext();
					},this));
			p.runAction(seq);
			break;
		case TAG_KMNSPOON:
			this.mno2bottle.runAction(cc.rotateTo(1,40));
			this.mno2bottle1.runAction(cc.rotateTo(1,40));
			
			var sp = cc.spawn(cc.moveTo(1,cc.p(105,110)),cc.rotateTo(1,10));			
			var move = cc.moveTo(1,cc.p(40,60));			
			var move1 = cc.moveTo(1,cc.p(105,110));			
			var move2 = cc.moveTo(1,cc.p(225,20));
			var seq = cc.sequence(sp,move,cc.callFunc(function(){
				p.setSpriteFrame("kmnspoon1.png");
			},this),move1,move2,cc.callFunc(function(){
				p.setSpriteFrame("kmnspoon.png");
				this.addfenmo();
			},this),cc.delayTime(1),cc.callFunc(function(){
				this.getChildByTag(TAG_K2MNO4LID).removeFromParent(true);
				this.getChildByTag(TAG_KMNSPOON).removeFromParent(true);
				this.mno2bottle.removeFromParent(true);
				this.mno2bottle1.removeFromParent(true);
				this.flowNext();
			},this));
			p.runAction(seq);
			break;
		case TAG_PAPER:
			var move = cc.moveTo(1,cc.p(500,-60));
			var move1 = cc.moveTo(1,cc.p(480,-183));
			var move2 = cc.moveTo(1,cc.p(270,-183));
			var move3 = cc.moveTo(1,cc.p(223,60));
			var seq = cc.sequence(move,move1,move2,cc.callFunc(function(){
				this.addzhi();
				p.setVisible(false);
				var rota = cc.rotateTo(1,-90);
				var delay = cc.delayTime(1);
				var rota1 = cc.rotateTo(1,0);
				var se = cc.sequence(rota,cc.callFunc(function(){
					p.setPosition(223,-130);
					p.setRotation(-90);
					p.setVisible(true);
					ll.run.iron.getChildByTag(TAG_SG_NODE).getChildByTag(TAG_SG).getChildByTag(TAG_SHOW).removeFromParent(true);
					ll.run.iron.getChildByTag(TAG_SG_NODE).getChildByTag(TAG_SG).getChildByTag(TAG_SHOW2).runAction(cc.fadeIn(0.5));
					p.getChildByTag(TAG_SHOW).runAction(cc.fadeOut(0.5));
					p.runAction(cc.moveTo(1,cc.p(223,60)));
				},this),delay,rota1,cc.callFunc(function(){
					ll.run.iron.getChildByTag(TAG_SG_NODE).getChildByTag(TAG_SG).getChildByTag(TAG_SHOW2).setSpriteFrame("kmno.png");
					ll.run.iron.getChildByTag(TAG_SG_NODE).getChildByTag(TAG_SG).getChildByTag(TAG_SHOW2).setPosition(125,52);
					ll.run.iron.getChildByTag(TAG_SG_NODE).getChildByTag(TAG_SG).getChildByTag(TAG_SHOW2).setRotation(-3);
				},this));
				ll.run.iron.getChildByTag(TAG_SG_NODE).getChildByTag(TAG_SG).runAction(se);
			
			},this),cc.delayTime(2),cc.callFunc(function(){		
				this.setOpacity(0);
					//this.removeFromParent(true);
                    gg.flow.next();
			},this));
			p.runAction(seq);
			break;
		
		}
	},
	flowNext:function(){
		gg.flow.next();
	},
});