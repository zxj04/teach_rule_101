exp05.Lv = cc.Node.extend({
	ctor:function(p){
		this._super();
		p.addChild(this, 8, TAG_LV_NODE);
		this.init();
//		this.lvfire();
//		this.addfirebg();

	},
	init : function(){
		this.setCascadeOpacityEnabled(true);

		this.lv= new Button(this,10, TAG_LV, "#lv.png",this.callback);
		this.lv.setScale(0.4);
		this.lv.setRotation(50);

		


	},
	lvfire :function(){
		var frames=[];
		for (i=1;i<=17;i++){
			var str ="lvfire/"+i+".png";
			var frame=cc.spriteFrameCache.getSpriteFrame(str);
			frames.push(frame);
		}
		var animation = new cc.Animation(frames,0.2);//负责动画序列
		var action = new cc.Animate(animation);//帧动画的动作创建
		//return action;
		
		var fire = new cc.Sprite("#lvfire/1.png");
		fire.setPosition(25,30);
		fire.setRotation(-10);
		fire.setScale(0.8);
		this.lv.addChild(fire,1,TAG_SHOW1);
		var seq = cc.sequence(cc.delayTime(0.2),action,cc.callFunc(this.flowNext ,this));
		fire.runAction(seq);
	},
	lvfire1 :function(){
		var frames=[];
		for (i=1;i<=2;i++){
			var str ="exp08fire2/"+i+".png";
			var frame=cc.spriteFrameCache.getSpriteFrame(str);
			frames.push(frame);
		}
		var animation = new cc.Animation(frames,0.05);//负责动画序列
		var action = new cc.Animate(animation);//帧动画的动作创建
//		return action;
		
		var fire = new cc.Sprite("#exp08fire2/1.png");
		fire.setPosition(10,35);
		fire.setScale(0.8);
		this.lv.addChild(fire,1,TAG_SHOW);
		fire.runAction(action.repeatForever());
		
	},
	addfirebg :function(){
		var fire = new cc.Sprite("#firebg/04.png");
		fire.setPosition(-55,-50);
		fire.setRotation(-10);
		fire.setScale(0);
		this.addChild(fire,1,TAG_SHOW);
		var seq = cc.sequence(cc.delayTime(0.2),cc.scaleTo(0.4,1),cc.delayTime(0.4),cc.callFunc(function(){
			fire.setSpriteFrame("firebg/01.png");
		},this),cc.delayTime(1),cc.callFunc(function(){
			fire.setSpriteFrame("firebg/02.png");
		},this),cc.scaleTo(1.6,0));
		fire.runAction(seq);
	},

	callback:function(p){
		var action = gg.flow.flow.action;
		var func = cc.callFunc(this.actionDone, this);	
		switch(p.getTag()){	
		case TAG_LV:
			var move = cc.moveTo(1,cc.p(315,300));
			var rota = cc.rotateTo(1,-40);
			var sp = cc.spawn(move,rota);
			var move1 = cc.moveTo(1,cc.p(800,400));
			var move2 = cc.moveTo(0.5,cc.p(935,320));
			var move3 = cc.moveTo(0.5,cc.p(925,250));
			var seq = cc.sequence(sp,cc.callFunc(this.lvfire1 ,this),cc.delayTime(0.5),cc.callFunc(function(){
				ll.run.iron.getChildByTag(TAG_BOTTLE1).getChildByTag(TAG_GLASS1).runAction(cc.moveTo(0.5,cc.p(0,420)));
			},this),move1,move2,cc.callFunc(function(){
				
				this.lvfire();
				this.addfirebg();
				this.lv.getChildByTag(TAG_SHOW).removeFromParent();
			},this),move3);
			
			this.runAction(seq);
			
			break;

		}
	},
	flowNext:function(){
		gg.flow.next();
	},
});