exp05.Sg = cc.Node.extend({
	ctor:function(p){
		this._super();
		p.addChild(this, 9, TAG_SG_NODE);
		this.init();
		this.ironfalse();
	},
	init : function(){
		this.setCascadeOpacityEnabled(true);


		var sg= new Button(this, 10, TAG_SG, "#sg.png",this.callback);
		sg.setScale(0.6);
		sg.setOpacity(1);
		
		var sg1 = new cc.Sprite("#sg.png");
		sg1.setPosition(190,49);
		this.getChildByTag(TAG_SG).addChild(sg1,10);
		
		var fenmo = new cc.Sprite("#fenmo2.png");
		fenmo.setPosition(120,50);
		fenmo.setRotation(90);
		fenmo.setOpacity(0);
		this.getChildByTag(TAG_SG).addChild(fenmo,6,TAG_SHOW2);
		
		
		var dg= new Button(this,6, TAG_DAOGUAN, "#daoguan1.png",this.callback);
		dg.setScale(0.6);
		dg.setPosition(180,-116);
		
		this.iron1 = new cc.Sprite("#ironup.png");
		this.iron1.setScale(0.6,1);
		this.iron1.setPosition(44,16);
		this.addChild(this.iron1,11);

		this.iron2 = new cc.Sprite("#irondown.png");
		this.iron2.setScale(0.6,1);
		this.iron2.setPosition(46,-11);
		this.addChild(this.iron2,11);


	},
	fenmo:function(){
		var fenmo = new cc.Sprite("#cu.png");
		fenmo.setPosition(0,0);
		fenmo.setRotation(0);
		this.getChildByTag(TAG_SG).addChild(fenmo,6,TAG_SHOW2);
	},
	bubble:function(time,pos,po1,scale){
		var bubble = new cc.Sprite("#bubble2.png");
		bubble.setPosition(pos);
		bubble.setScale(0.4);
		this.addChild(bubble,12);
		var move = cc.moveTo(time,po1);
		var sca = cc.scaleTo(time,scale);
		var sp = cc.spawn(move,sca);
		var seq = cc.sequence(sp,cc.callFunc(function(){
			bubble.removeFromParent(true);
		},this));
		bubble.runAction(seq);
	},
	bubble1:function(time,posy){
		var bubble = new cc.Sprite("#bubble2.png");
		bubble.setPosition(247,-230);
		bubble.setScale(0.4);
		this.addChild(bubble,12);
		var move = cc.moveTo(time,cc.p(247,posy));
		var scale = cc.scaleTo(time,0.8);
		var sp = cc.spawn(move,scale);
		var seq = cc.sequence(sp,cc.callFunc(function(){
			bubble.removeFromParent(true);
		},this));
		bubble.runAction(seq);
	},
	bubble2:function(time,pos,pos2){
		var bubble = new cc.Sprite("#bubble2.png");
		bubble.setPosition(pos);
		bubble.setScale(0.4);
		this.addChild(bubble,12);
		var move = cc.moveTo(time,pos2);
		var scale = cc.scaleTo(time,0.8);
		var sp = cc.spawn(move,scale);
		var seq = cc.sequence(sp,cc.callFunc(function(){
			bubble.removeFromParent(true);
		},this));
		bubble.runAction(seq);
	},
	irontrue :function(){
		this.iron1.setVisible(true);
		this.iron2.setVisible(true);
		
	},
	ironfalse :function(){
		this.iron1.setVisible(false);
		this.iron2.setVisible(false);

	},
	Toblack:function(){
		var black = new cc.Sprite("#black.png");
		black.setPosition(180,50);
		black.setRotation(-2);
		black.setOpacity(0);
		this.getChildByTag(TAG_SG).addChild(black);
		black.runAction(cc.fadeIn(2));

	},
	Daoguan:function(){
		var frames=[];
		for (i=1;i<=3;i++){
			var str ="daoguan"+i+".png";
			var frame=cc.spriteFrameCache.getSpriteFrame(str);
			frames.push(frame);
		}
		var animation = new cc.Animation(frames,0.2);//负责动画序列
		var action = new cc.Animate(animation);//帧动画的动作创建
		return action;
	},
	callback:function(p){
		var action = gg.flow.flow.action;
		var func = cc.callFunc(this.actionDone, this);	
		switch(p.getTag()){	
		case TAG_SG:
			if(action == ACTION_DO1){
				var move = cc.moveTo(1,cc.p(120,80));
				var move1 = cc.moveTo(1,cc.p(250,170));
				var move2 = cc.moveTo(1,cc.p(280,80));
				var seq = cc.sequence(move,move1,move2,cc.callFunc(function(){
					this.showtip = new ShowTip("用手握住试管，导管有气" +
							"\n泡产生，试管气密性完好",cc.p(900,450));
					this.schedule(function(){
						this.bubble(0.8,cc.p(247,-230),cc.p(247,-205),0.8);
					}, 1, 1000,2);
				},this),cc.delayTime(3),cc.callFunc(this.flowNext,this));	
				this.runAction(seq);
			}else if(action == ACTION_DO2){	
				this.pause();
				var move1 = cc.moveTo(1,cc.p(250,170));
				var move2 = cc.moveTo(0.5,cc.p(200,170));
				var move3 = cc.moveTo(0.5,cc.p(150,120));
							
				var seq = cc.sequence(move1,move2,move3,cc.callFunc(function(){
					var move4 = cc.moveTo(1,cc.p(200,-118));
					var move5 = cc.moveTo(1,cc.p(150,-220));	
					//var move5 = cc.moveTo(1,cc.p(150,-150));	
					var se = cc.sequence(move4,cc.callFunc(function(){
						this.getChildByTag(TAG_DAOGUAN).setSpriteFrame("daoguan4.png");
					},this),move5);
					this.getChildByTag(TAG_DAOGUAN).runAction(se);
				},this),cc.delayTime(2),cc.callFunc(this.flowNext,this));	
				this.runAction(seq);
			}else if(action == ACTION_DO3){
			     var move = cc.moveTo(0.5,cc.p(150,80));	
			     var move1 = cc.moveTo(1.5,cc.p(-55,80));	
			     var rota = cc.rotateTo(0.5,5);
			     var move2 = cc.moveTo(1.5,cc.p(-55,82));
			     var sp = cc.spawn(rota,move2);
			     var seq = cc.sequence(move,move1,cc.callFunc(function(){
			    	 ll.run.iron.iron1.runAction(cc.rotateTo(0.5,5));
			    	 ll.run.iron.iron2.runAction(cc.rotateTo(0.5,5));
			     },this),sp,cc.callFunc(this.flowNext,this));			     
			     this.runAction(seq);			 
			}else if(action == ACTION_DO4){
				var bottle = new cc.Sprite("#bottle.png");
				bottle.setPosition(500,-150);
				bottle.setScale(0.2);
				ll.run.iron.addChild(bottle,11,TAG_BOTTLE1);
				
				var glass = new cc.Sprite("#glass1.png");
				glass.setPosition(135,420);
				glass.setScale(1.4,1.3);
				bottle.addChild(glass,11,TAG_GLASS1);
				
				var line = new cc.Sprite("#bottleline1.png");
				line.setPosition(145,60);
				line.setScale(2,1);
				bottle.addChild(line);
				
				this.showtip = new ShowTip("1、试管内壁变黑" +
				"\n2、棉花变黑",cc.p(450,450));
			    this.flowNext();			 
			}
			
        
		break;
		case TAG_DAOGUAN:
			if(action == ACTION_DO1){
				p.setSpriteFrame("daoguan1.png");
				var move = cc.moveTo(1,cc.p(210,-116));
				var move1 = cc.moveTo(1,cc.p(180,-116));
				var seq = cc.sequence(move,move1,cc.callFunc(this.flowNext,this));
				p.runAction(seq);
				
			}else if(action == ACTION_DO2){
//				this.showtip = new ShowTip("导管要先从水槽中取出，防止" +
//						"\n试管变冷，水倒吸使试管破裂",cc.p(800,500));
//				var move = cc.moveBy(0.8,cc.p(0,100));
//				var move1 = cc.moveBy(0.8,cc.p(0,-100));
//				var seq = cc.sequence(cc.callFunc(function(){
//					ll.run.iron.pause();
//					this.irontrue();
//					ll.run.iron.iron1.setVisible(false);
//					ll.run.iron.iron2.setVisible(false);
//				},this),move,cc.callFunc(function(){
//					ll.run.iron.flume.removeFromParent(true);	
//					ll.run.iron.flume1.removeFromParent(true);	
//					ll.run.iron.flumeline.removeFromParent(true);						
//				},this),cc.delayTime(0.5),move1,cc.callFunc(function(){
//					this.ironfalse();
//					ll.run.iron.iron1.setVisible(true);
//					ll.run.iron.iron2.setVisible(true);
//					this.flowNext();
//				},this));
//				this.runAction(seq);
				
				ll.run.iron.pause();
				this.showtip = new ShowTip("导管要先从水槽中取出，防止" +
				"\n试管变冷，水倒吸使试管破裂",cc.p(800,500));
				var action = this.Daoguan();
				var seq = cc.sequence(action,cc.delayTime(0.5),cc.callFunc(function(){
					ll.run.iron.flume.removeFromParent(true);	
					ll.run.iron.flume1.removeFromParent(true);	
					ll.run.iron.flumeline.removeFromParent(true);	
				},this),action.reverse(),cc.callFunc(this.flowNext , this));
				p.runAction(seq);
			}
			
		break;
		}
	},
	flowNext:function(){
		gg.flow.next();
	},
});