exp05.Lamp08 = cc.Node.extend({
	ctor:function(p){
		this._super();
		p.addChild(this, 10, TAG_LAMP_NODE);
		this.init();
	},
	init : function(){
		this.setCascadeOpacityEnabled(true);
		var lamplid= new Button(this, 11, TAG_LAMP_LID, "#lampLid.png",this.callback);
		lamplid.setPosition(0,40);
		lamplid.setScale(0.4);
	

		this.lamp= new Button(this, 10, TAG_LAMP, "#lampBottle.png",this.callback);
		this.lamp.setPosition(0,0);
		this.lamp.setScale(0.4);
		

		var match= new Button(this, 1, TAG_MATCH, "#libmatch.png",this.callback);
		match.setScale(0.3);
		match.setPosition(0-100,100);
		
		var mk= new Button(this, 1, TAG_MUKUAI, "#mk.png",this.callback);
		mk.setScale(0.7,1);
		mk.setPosition(160,-20);
	},
	fire:function(){
		var frames=[];
		for (i=1;i<=3;i++){
			var str ="fire/"+i+".png";
			var frame=cc.spriteFrameCache.getSpriteFrame(str);
			frames.push(frame);
		}
		var animation = new cc.Animation(frames,0.05);//负责动画序列
		var action = new cc.Animate(animation);//帧动画的动作创建
		return action;
	},
    
	callback:function(p){
		var action = gg.flow.flow.action;
		var func = cc.callFunc(this.actionDone, this);	
		switch(p.getTag()){	
		case TAG_LAMP_LID:
			if(action==ACTION_DO1){				
				var seq = cc.sequence(cc.moveTo(0.5,cc.p(0,80)),cc.moveTo(0.5,cc.p(80,40)),cc.moveTo(0.5,cc.p(90,-40)),
						cc.callFunc(this.flowNext,this));			
			}else if(action==ACTION_DO2){
				var seq = cc.sequence(cc.moveTo(0.5,cc.p(80,40)),cc.moveTo(0.5,cc.p(0,120)),cc.callFunc(function(){
					this.lamp.getChildByTag(TAG_SHOW).runAction(cc.scaleTo(0.5,0));
				},this),cc.moveTo(0.5,cc.p(0,40)),cc.moveTo(0.5,cc.p(0,120)),cc.moveTo(0.5,cc.p(0,40)),
						cc.callFunc(function(){
							//this.removeFromParent(true);
							this.flowNext();
						},this));
			}
			p.runAction(seq);
			break;	
		case TAG_MATCH:
			var match = new cc.Sprite("#match1.png");
			match.setPosition(-20,170);
			match.setScale(0.3);
			match.setAnchorPoint(1,1);
			this.addChild(match,2);
			var seq = cc.sequence(cc.moveTo(0.3,cc.p(20,150)),cc.callFunc(function(){
				match.setSpriteFrame("match2.png");
			},this),cc.moveTo(0.8,cc.p(75,90)),cc.callFunc(function(){
				var fire = new cc.Sprite("#fire/1.png");
				fire.setPosition(165,295);
				fire.setAnchorPoint(0.5,0);
				fire.setScale(3.3);
				ll.run.lamp.lamp.addChild(fire,1,TAG_SHOW);
				fire.runAction(ll.run.lamp.fire().repeatForever());
				match.removeFromParent(true);
				gg.flow.next();
			},this));

			match.runAction(seq);
			break;
		case TAG_LAMP:
			if (action == ACTION_DO1){
				var move = cc.moveTo(1,cc.p(100,100));
				var move1 = cc.moveTo(1,cc.p(200,100));
				var move2 = cc.moveTo(1,cc.p(140,100));
				var move3 = cc.moveTo(0.5,cc.p(160,90));
				var seq = cc.sequence(move,move1,move2,move1,move2,move1,move3,cc.callFunc(function(){
					ll.run.iron.getChildByTag(TAG_SG_NODE).Toblack();
					this.showtip = new ShowTip("1、2KMnO₄  ≜ K₂MnO₄ + MnO₂ + O₂↑" +
							"\n2、观察导管口以及试管现象",cc.p(500,500));
					this.schedule(function(){
						ll.run.iron.getChildByTag(TAG_SG_NODE).bubble(0.8,cc.p(247,-230),cc.p(247,-180),0.8);
					}, 1, 10000,2);
					this.flowNext();
				},this),cc.delayTime(2),cc.callFunc(function(){
					ll.run.iron.getChildByTag(TAG_SG_NODE).getChildByTag(TAG_SG).getChildByTag(TAG_SHOW1).setSpriteFrame("mianhua1.png");
				},this));				
			}else if(action == ACTION_DO2){
				var move = cc.moveTo(1,cc.p(100,100));
				var move1 = cc.moveTo(1,cc.p(0,0));
				var seq = cc.sequence(move1,move1,cc.callFunc(function(){
					ll.run.iron.iron.removeFromParent(true);
					ll.run.iron.iron1.removeFromParent(true);
					ll.run.iron.iron2.removeFromParent(true);
					ll.run.iron.getChildByTag(TAG_SG_NODE).removeFromParent(true);

					this.flowNext(); 
				},this));
			}
			
			p.runAction(seq);
		break;

		}
	},
	flowNext:function(){
		gg.flow.next();
	},
});