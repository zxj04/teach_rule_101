exp05.Cbottle = cc.Node.extend({
	ctor:function(p){
		this._super();
		p.addChild(this, 10, TAG_CBOTTLE_NODE);
		this.init();
		
	},
	init : function(){
		this.setCascadeOpacityEnabled(true);
		var lid= new Button(this, 8, TAG_CLID, "#mutanlid1.png",this.callback);
		lid.setPosition(0,70);
		lid.setScale(0.8);

		this.cbottle= new Button(this, 10, TAG_CBOTTLE, "#mutanbottle2.png",this.callback);
		this.cbottle.setScale(0.8);

		this.cbottle1 = new cc.Sprite("#mutanbottle1.png");
		this.addChild(this.cbottle1);
		this.cbottle1.setScale(0.8);

		//"NH₃ + CO₂ + H₂O = NH₄HCO₃\n" 
		var label = new cc.LabelTTF("碳 ","微软雅黑",22);
		label.setPosition(56,80);
		label.setColor(cc.color(0,0,0));
		this.cbottle.addChild(label,11);
       
		var ganguojia = new Ganguojia(this);
		ganguojia.setPosition(180,0);

	

	},
    addTan:function(){
    	var tan = new cc.Sprite("#mutan.png");
    	tan.setPosition(10,30);
    	this.ganguojia1.addChild(tan,1,TAG_SHOW);
		
	},
	callback:function(p){
		var action = gg.flow.flow.action;
		var func = cc.callFunc(this.actionDone, this);	
		switch(p.getTag()){	
		case TAG_CLID:
			var seq = cc.sequence(cc.moveTo(0.5,cc.p(0,100)),cc.moveTo(0.5,cc.p(-50,100)),cc.spawn(cc.moveTo(1,cc.p(-100,-50)),cc.rotateTo(1,-180)),
					cc.callFunc(function(){
						p.setSpriteFrame("mutanlid2.png");
						p.setRotation(0);
						this.flowNext();
					},this));
			p.runAction(seq);
			break;
//		case TAG_GANGUOJIA2:
//			this.cbottle.runAction(cc.rotateTo(0.8,50));
//			this.cbottle1.runAction(cc.rotateTo(0.8,50));
//			var move = cc.moveTo(1,cc.p(180,100));
//			var move1 = cc.moveTo(0.6,cc.p(60,35));
//			var move2 = cc.moveTo(0.6,cc.p(180,100));
//			var rota = cc.rotateTo(0.2,5);
//			var seq = cc.sequence(move,move1,cc.callFunc(function(){
//				this.addTan();
//			},this),rota,move2);
//			p.runAction(seq);
//			
//			var se = cc.sequence(move.clone(),move1.clone(),cc.delayTime(0.2),move2.clone());
//			this.ganguojia1.runAction(se);
//			
//			
//		break;

		}
	},
	flowNext:function(){
		gg.flow.next();
	},
});