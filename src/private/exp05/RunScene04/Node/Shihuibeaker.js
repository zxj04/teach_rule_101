exp05.Shihuibeaker = cc.Node.extend({
	ctor:function(p){
		this._super();
		p.addChild(this, 9, TAG_SHIHUIBEAKER_NODE);
		this.init();

	},
	init : function(){
		this.setCascadeOpacityEnabled(true);

		this.shihuibeaker= new Button(this,10, TAG_SHIHUIBEAKER, "#shihuibeaker.png",this.callback);
		this.shihuibeaker.setScale(0.3);
		this.shihuibeaker.setCascadeOpacityEnabled(true);
	
		//"NH₃ + CO₂ + H₂O = NH₄HCO₃\n" 
		var label = new cc.LabelTTF("Ca(OH)₂","微软雅黑",22);
		label.setPosition(155,175);
		label.setColor(cc.color(0,0,0));
		label.setScale(2);
		this.shihuibeaker.addChild(label,11);
		
		this.beakerline= new Button(this, 6, TAG_BEAKER_LINE, "#beakerline.png",this.callback);
		this.beakerline.setScale(0.3);
		this.beakerline.setPosition(2,15);



	},
	addhunzuo:function(obj,pos,scale){
		var hunzuo = new cc.Sprite("#shihuihunzuo.png");
		hunzuo.setPosition(pos);
		hunzuo.setScale(scale);
		hunzuo.setOpacity(0);
		obj.addChild(hunzuo);
		hunzuo.runAction(cc.fadeTo(1.5,200));
	},
	callback:function(p){
		var action = gg.flow.flow.action;
		var func = cc.callFunc(this.actionDone, this);	
		switch(p.getTag()){	
		case TAG_SHIHUIBEAKER:
			var move = cc.moveTo(1.5,cc.p(750,300));
			var move1 = cc.moveTo(0.5,cc.p(730,260));
			var rota = cc.rotateTo(0.5,-60);
			var sp = cc.spawn(move1,rota);
			var seq = cc.sequence(move,cc.callFunc(function(){
				var mov = cc.moveTo(0.5,cc.p(-10,0));
				var rot = cc.rotateTo(0.5,60);
				var sca = cc.scaleTo(0.5,0.5);
				
				var sp1 = cc.spawn(mov,rot,sca);
				this.getChildByTag(TAG_BEAKER_LINE).runAction(sp1);
			},this),sp,cc.callFunc(function(){
				var flow = new cc.Sprite("#flow_right.png");
				flow.setAnchorPoint(1,1);
				flow.setPosition(15,320);
				flow.setRotation(50);
				flow.setScale(0);
				p.addChild(flow);
				var se = cc.sequence(cc.scaleTo(0.5,5),cc.delayTime(0.5),cc.fadeOut(0.5));
				flow.runAction(se);				
				ll.run.iron.bottleline.setPosition(145,25);
				ll.run.iron.bottleline.runAction(cc.spawn(cc.moveTo(0.5,cc.p(145,100)),cc.scaleTo(0.5,1.9)));		
				this.showtip = new ShowTip("1、澄清石灰水变浑浊",cc.p(650,420));
				
				this.addhunzuo(ll.run.iron.bottle,cc.p(142,65),2.4);
				
			},this),cc.delayTime(1),cc.callFunc(function(){
				this.setOpacity(0);
				//this.removeFromParent(true);
				this.flowNext();
			},this));
			this.runAction(seq);
			break;

		}
	},
	flowNext:function(){
		gg.flow.next();
	},
});