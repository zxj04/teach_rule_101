exp05.Iron08 = cc.Node.extend({
	ctor:function(p){
		this._super();
		p.addChild(this, 10, TAG_IRON_NODE);
		this.init();
	},
	init : function(){
		this.setCascadeOpacityEnabled(true);

		this.iron= new Button(this, 8, TAG_IRON, "#iron2.png",this.callback);
		this.iron.setScale(0.6);
		this.iron.setPosition(0,25);

		this.iron1 = new cc.Sprite("#ironup.png");
		this.iron1.setScale(0.6,1);
		this.iron1.setPosition(-10,95);
		this.addChild(this.iron1,11);
		
		this.iron2 = new cc.Sprite("#irondown.png");
		this.iron2.setScale(0.6,1);
		this.iron2.setPosition(-10,68);
		this.addChild(this.iron2,11);
		
		var mianhua= new Button(this, 8, TAG_MIANHUA, "#mianhua.png",this.callback);
		mianhua.setPosition(120,-170);
		mianhua.setScale(0.6);

		this.flume= new Button(this, 15, TAG_FLUME, "#flume1.png",this.callback);
		this.flume.setPosition(550,-140);
		this.flume.setScale(0.5);

		this.flume1= new Button(this, 9, TAG_FLUME1, "#flume2.png",this.callback);
		this.flume1.setPosition(550,-140.4);
		this.flume1.setScale(0.5);
		

		
		this.flumeline= new Button(this, 10, TAG_FLUMElINE1, "#flumeline.png",this.callback);
		this.flumeline.setPosition(550,-115);
		this.flumeline.setScale(0.5);
		
		this.bottle = new Button(this,11 ,TAG_BOTTLE, "#bottle.png",this.callback);
		this.bottle.setScale(0.2);
		this.bottle.setPosition(570,-140);
		
		this.bottleline = new cc.Sprite("#bottleline1.png");
		this.bottleline.setPosition(145,30);
		this.bottleline.setScale(2);
		this.bottle.addChild(this.bottleline);
		this.bottleline.setVisible(false);

		this.glass= new Button(this, 11, TAG_GLASS, "#glass1.png",this.callback);
		this.glass.setPosition(570,-180);
		this.glass.setScale(0.3);


//		var flask= new Flask(this);//第10层
//		flask.setPosition(160,0);


		var sg= new exp05.Sg(this);//第7层
		sg.setPosition(0,80);
		

	},
	callback:function(p){
		var action = gg.flow.flow.action;
		var func = cc.callFunc(this.actionDone, this);	
		switch(p.getTag()){	
		case TAG_FLUME:
			var move = cc.moveBy(1,cc.p(-350,0));
			var move1 = cc.moveBy(0.8,cc.p(0,100));
			var move2 = cc.moveBy(0.8,cc.p(0,-100));
			var seq = cc.sequence(move,cc.delayTime(2.5),cc.callFunc(this.flowNext , this));
//			var sq = cc.sequence(cc.callFunc(function(){
//				ll.run.iron.getChildByTag(TAG_SG_NODE).irontrue();
//				this.iron1.setVisible(false);
//				this.iron2.setVisible(false);
//			},this),move1,cc.delayTime(0.5),move2,cc.callFunc(function(){
//				ll.run.iron.getChildByTag(TAG_SG_NODE).ironfalse();
//				this.iron1.setVisible(true);
//				this.iron2.setVisible(true);
//			},this));
			this.flume.runAction(seq);
			
			this.flume1.runAction(move.clone());
			this.bottle.runAction(move.clone());
			this.glass.runAction(move.clone());
			this.flumeline.runAction(move.clone());
			
		//	ll.run.iron.getChildByTag(TAG_SG_NODE).runAction(sq);
			
			
			
			
			var action = ll.run.iron.getChildByTag(TAG_SG_NODE).Daoguan();
			var seq = cc.sequence(action,cc.delayTime(0.5),action.reverse());
			ll.run.iron.getChildByTag(TAG_SG_NODE).getChildByTag(TAG_DAOGUAN).runAction(seq);
			
			break;      
		case TAG_BOTTLE:
			if(action == ACTION_DO1){
				var rota = cc.rotateTo(1,-90);
				var rota1 = cc.rotateTo(1,0);
				var seq = cc.sequence(rota,cc.delayTime(1),rota1,cc.callFunc(this.flowNext,this));	
				p.runAction(seq);
			}else if(action == ACTION_DO2){
				var move = cc.moveTo(0.5,cc.p(210,-110));
				var move1 = cc.moveTo(1.5,cc.p(175,-130));
				var seq = cc.sequence(move,move1,cc.callFunc(function(){
					this.bottleline.setVisible(true);
					var se = cc.sequence(cc.moveTo(10,cc.p(145,300)),cc.spawn(cc.moveTo(2,cc.p(145,350)),cc.scaleTo(2,1.7)),
							cc.moveTo(2,cc.p(145,400)),cc.callFunc(this.flowNext,this));
					this.bottleline.runAction(se);
					ll.run.lamp.pause();
					var posy = -180;
					this.schedule(function(){
						ll.run.iron.getChildByTag(TAG_SG_NODE).bubble1(0.8,posy);
						posy = posy - 3;
					}, 1, 12);
				},this),cc.delayTime(12),cc.callFunc(function(){
					p.schedule(function(){
						ll.run.iron.getChildByTag(TAG_SG_NODE).bubble2(0.5,cc.p(270,-230),cc.p(280,-180));
					}, 1, 1000);	
				},this));	
				p.runAction(seq);
			}else if(action == ACTION_DO3){
				var move = cc.moveTo(1,cc.p(50,-110));
				var sca = cc.scaleTo(1,0.4);
				var sp = cc.spawn(move,sca);
				var move1 = cc.moveTo(1,cc.p(300,-110));
				var sp1 = cc.spawn(move1,sca.clone());
				var seq = cc.sequence(sp,cc.callFunc(this.flowNext ,this));
				p.runAction(seq);
				
				ll.run.iron.getChildByTag(TAG_BOTTLE1).runAction(sp1);
				
			}
			
			
		    break;
		case TAG_GLASS:
			if(action ==ACTION_DO1){
				p.setVisible(false);
				var glass = new cc.Sprite("#glass1.png");
				glass.setPosition(135,15);
				glass.setScale(1.4);
				this.bottle.addChild(glass,1,TAG_SHOW);

				var move = cc.moveTo(1,cc.p(280,15));			
				var rota = cc.rotateTo(0.5,-30);

				var rota1 = cc.rotateTo(1,-90);					
				var move1 = cc.moveTo(1.5,cc.p(350,450));
				var sp = cc.spawn(rota1,move1);

				var rota2 = cc.rotateTo(1,-180);
				var move2 = cc.moveTo(1.5,cc.p(0,420));
				var sp1 = cc.spawn(rota2,move2);
				var move3 = cc.moveTo(1,cc.p(135,420));
				var seq = cc.sequence(move,rota,sp,cc.delayTime(0.5),sp1,cc.delayTime(0.5),move3,cc.delayTime(1),cc.callFunc(function(){
					this.bottle.runAction(cc.rotateTo(1,180));
				},this),cc.delayTime(1),cc.callFunc(function(){
					p.setVisible(true);
					//this.bottle.getChildByTag(TAG_SHOW).removeFromParent(true);
					this.bottle.getChildByTag(TAG_SHOW).setVisible(false);
					this.flowNext();
				},this));
				glass.runAction(seq);				
			}else if(action == ACTION_DO2){
				this.getChildByTag(TAG_BOTTLE).pause();
				var move = cc.moveTo(0.5,cc.p(220,-110));
				var move1 = cc.moveTo(1,cc.p(220,20));
				var rota  = cc.rotateTo(1.5,0);
				var move2 = cc.moveTo(1.5,cc.p(300,-50));
				var spawn = cc.spawn(rota,move2);
				var move3 = cc.moveTo(2,cc.p(400,-150));
				var seq = cc.sequence(move,cc.callFunc(function(){
					this.schedule(function(){
						ll.run.iron.getChildByTag(TAG_SG_NODE).bubble1(0.8,-180);
					}, 1, 200);
					this.bottle.getChildByTag(TAG_SHOW).setVisible(true);
				},this),move1,spawn,move3,cc.callFunc(function(){
//					this.pause();
//					this.schedule(function(){
//						ll.run.iron.getChildByTag(TAG_SG_NODE).bubble(0.8,cc.p(247,-230),cc.p(247,-205),0.8);
//					}, 1, 100);
					this.flowNext();
				},this));
				this.bottle.runAction(seq);
				
				var mov = cc.moveTo(0.5,cc.p(220,-150));
				var se = cc.sequence(mov,cc.callFunc(function(){
//					this.showtip = new ShowTip("1、试管内壁变黑" +
//							"\n2、棉花变黑",cc.p(450,450));
					
					this.showtip = new ShowTip("同样方法，再" +
							"\n收集一瓶氧气",cc.p(850,400));
					p.setVisible(false);
					p.setPosition(400,-110);
				},this));
				p.runAction(se);
			}
			
	    	break;
		case TAG_MIANHUA:
			var move = cc.moveTo(1,cc.p(300,122));
			var move1 = cc.moveTo(0.5,cc.p(230,122));
			var seq = cc.sequence(move,move1,cc.callFunc(function(){
				var mianhua = new cc.Sprite("#mianhua.png");
				mianhua.setPosition(326,51);
				ll.run.iron.getChildByTag(TAG_SG_NODE).getChildByTag(TAG_SG).addChild(mianhua,5,TAG_SHOW1);
				p.removeFromParent(true);
				this.flowNext();
			},this));
			p.runAction(seq);
			break;
		}
	},
	flowNext:function(){
		gg.flow.next();
	},
});