exp05.Ganguojia = cc.Node.extend({
	ctor:function(p){
		this._super();
		p.addChild(this, 9, TAG_GANGUOJIA_NODE);
		this.init();

	},
	init : function(){
		this.setCascadeOpacityEnabled(true);

		this.ganguojia1= new Button(this,5, TAG_GANGUOJIA1, "#ganguoqian1.png",this.callback);
		this.ganguojia1.setScale(0.5);
		this.ganguojia1.setCascadeOpacityEnabled(true);
		
		var tan = new cc.Sprite("#mutan.png");
		tan.setPosition(10,30);
		this.ganguojia1.addChild(tan,1,TAG_SHOW);


		this.ganguojia2= new Button(this, 6, TAG_GANGUOJIA2, "#ganguoqian2.png",this.callback);
		this.ganguojia2.setScale(0.5);
		this.ganguojia2.setRotation(5);



	},
	fire: function() {
		var frames=[];
		for (i=3;i<=4;i++){
			var str ="tanfire/"+i+".png";
			var frame=cc.spriteFrameCache.getSpriteFrame(str);
			frames.push(frame);
		}
		var animation = new cc.Animation(frames,0.2);//负责动画序列
		var action = new cc.Animate(animation);//帧动画的动作创建
		return action;
	},
	callback:function(p){
		var action = gg.flow.flow.action;
		var func = cc.callFunc(this.actionDone, this);	
		switch(p.getTag()){	
		case TAG_GANGUOJIA1:		
			var move = cc.moveTo(1.5,cc.p(330,330));
			var rota = cc.rotateTo(0.8,-50);
			var sp = cc.spawn(move,rota);
			var delay = cc.delayTime(0.5);
			var move1 = cc.moveTo(1,cc.p(600,380));
			var move2 = cc.moveTo(0.5,cc.p(700,350));
			var move3 = cc.moveTo(1,cc.p(690,280));
			var action = this.fire();
			var seq = cc.sequence(sp,delay,cc.callFunc(function(){
				p.getChildByTag(TAG_SHOW).setSpriteFrame("mutan1.png");
			},this),move1,move2,move3,cc.callFunc(function(){
				this.showtip = new ShowTip("木炭剧烈燃烧",cc.p(780,250));
				p.getChildByTag(TAG_SHOW).setScale(2);
				p.getChildByTag(TAG_SHOW).runAction(action.repeatForever());
			},this),cc.delayTime(5),cc.callFunc(function() {
				this.setOpacity(0);
				//this.removeFromParent(true);
				this.flowNext(); 
			}, this));
			this.runAction(seq);

            ll.run.iron.bottle.getChildByTag(TAG_SHOW).runAction(cc.moveTo(1,cc.p(0,420)));
			break;

		}
	},
	flowNext:function(){
		gg.flow.next();
	},
});