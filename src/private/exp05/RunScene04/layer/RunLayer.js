exp05.RunLayer4 = cc.Layer.extend({
	arr:null,
	scene:null,
	clock:null,
	ctor:function (parent) {
		this._super();
		this.scene = parent;
		this.scene.addChild(this, 10);
		gg.main = this;
		this.init();
	},
	init:function () {
		this.callNext = cc.callFunc(function(){
			gg.flow.next();
		}, this);
		this.callKill = cc.callFunc(function(p){
			var seq = cc.sequence(cc.fadeOut(0.5),cc.callFunc(function(){
				p.removeFromParent(true);	
			}, this));
			p.runAction(seq);
		}, this); 

		this.callNext.retain();
		this.callKill.retain();
		//时钟
		this.clock = new Clock(this);
		// 物品库
		this.lib = new Lib(this);
		this.lib.loadBg([{	
			tag:TAG_LIB_FLUME,
			checkright:true,
		},
		{
			tag:TAG_LIB_BOTTLE,
		},
		{		
			tag:TAG_LIB_BEAKER,
			checkright:true,
		},
		{
			tag:TAG_LIB_AMPULLA,
			checkright:true,
		},
		{	
			tag:TAG_LIB_FLASK,
		},
		{
			tag:TAG_LIB_KMNO4,
			checkright:true,
		},
		{
			tag:TAG_LIB_TESTTUBE,
			checkright:true,
		},
		{	
			tag:TAG_LIB_MNO2,
		},
		{
			tag:TAG_LIB_LAMP,
			checkright:true,
		},
		{
			tag:TAG_LIB_IRON,
			checkright:true,
		}
		]);
		this.lv = new exp05.Lv(this);
		this.lv.setVisible(false);
		this.lv.setPosition(350,570);

		this.lamp = new exp05.Lamp08(this);
		this.lamp.setVisible(false);
		this.lamp.setPosition(300,165);

		this.kbottle = new exp05.KMnO4(this);
		this.kbottle.setVisible(false);
		this.kbottle.setPosition(530,570);

		this.iron = new exp05.Iron08(this);
		this.iron.setVisible(false);
		this.iron.setPosition(600,270);

		this.ganguojia = new exp05.Ganguojia(this);
		this.ganguojia.setVisible(false);
		this.ganguojia.setPosition(200,570);

		this.shihuibeaker = new exp05.Shihuibeaker(this);
		this.shihuibeaker.setVisible(false);
		this.shihuibeaker.setPosition(1100,550);

	},
	checkVisible:function(next){
		//是否可见
		var checkVisible = [];
		var node1 = ll.run.ganguojia;
		var node2 = ll.run.lamp;
		var node3 = ll.run.kbottle;
		var node4 = ll.run.iron;
		var node5 = ll.run.shihuibeaker;
		var node6 = ll.run.lv;

		checkVisible.push(node1,node2,node3,node4,node5,node6);

		for(var i in checkVisible){
			if(checkVisible[i] !== null){				
				checkVisible[i].setVisible(next);				
			}			
		}
	},
	loadInLib:function(obj, pos, tarPos,delay){
		obj.setPosition(pos);
		if(delay == null){
			delay = 1;
		}
		var ber = $.bezier(pos, tarPos, delay);
		var seq = cc.sequence(ber, this.callNext);
		obj.runAction(seq);
	},
	kill:function(obj){
		var fade = cc.fadeTo(0.5,0);
		var func = cc.callfunc(function(){
			obj.removeFromParent(true);
		},this);
		var seq = cc.sequence(fade,func);
		obj.runAction(seq)
	},

	callback:function (p){
		var func = cc.callFunc(this.actionDone, this);
		var action=gg.flow.flow.action;
		switch(p.getTag()){

		}
	},
	actionDone:function(p){
		var func = cc.callFunc(this.actionDone, this);
		switch(p.getTag()){

		}
	},
	flowNext:function(){
		gg.flow.next();
	},
	onExit:function(){
		this._super();
		this.callNext.release();
		this.callKill.release();
	}
});