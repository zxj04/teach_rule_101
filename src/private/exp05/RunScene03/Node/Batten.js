exp05.Batten1 = cc.Node.extend({
	ctor:function(p){
		this._super();
		p.addChild(this, 8, TAG_BATTEN_NODE);
		this.init();
	},
	init : function(){
		this.setCascadeOpacityEnabled(true);
		var batten= new Button(this, 11, TAG_BATTEN, "#expo7batten.png",this.callback);
		batten.setScale(0.35);
		


		
	},
	fire:function(){
		var frames=[];
		for (i=1;i<=6;i++){
			var str ="exp07fire/"+i+".png";
			var frame=cc.spriteFrameCache.getSpriteFrame(str);
			frames.push(frame);
		}
		var animation = new cc.Animation(frames,0.05);//负责动画序列
		var action = new cc.Animate(animation);//帧动画的动作创建
		return action;
	},
	callback:function(p){
		var action = gg.flow.flow.action;
		var func = cc.callFunc(this.actionDone, this);	
		switch(p.getTag()){	
		case TAG_BATTEN:
			var move = cc.moveTo(1,cc.p(1035,210));
			var rote = cc.rotateTo(1,-10);
			var sp = cc.spawn(move,rote);
			var move1 = cc.moveTo(1,cc.p(1035,180));
			var seq = cc.sequence(sp,cc.callFunc(function(){
				var fire = new cc.Sprite("#exp07fire/1.png");
				fire.setPosition(25,55);
				fire.setScale(0.5);
				this.getChildByTag(TAG_BATTEN).addChild(fire);
				var action = this.fire();
				var sq = cc.sequence(cc.delayTime(0.2),action);
				fire.runAction(sq);
				this.showtip = new ShowTip("带火星的木条复燃",cc.p(1020,300));
			},this),move1,cc.delayTime(5),cc.callFunc(this.flowNext , this));
			
			this.runAction(seq);
			var move2 = cc.moveTo(1,cc.p(0,420));
		    ll.run.iron.bottle.getChildByTag(TAG_SHOW).runAction(move2);
			break;											
		}
	},
	flowNext:function(){
		gg.flow.next();
	},
});