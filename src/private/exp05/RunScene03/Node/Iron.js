exp05.Iron = cc.Node.extend({
	ctor:function(p){
		this._super();
		p.addChild(this, 10, TAG_IRON_NODE);
		this.init();
	},
	init : function(){
		this.setCascadeOpacityEnabled(true);

		var iron= new Button(this, 8, TAG_IRON, "#iron.png",this.callback);
		iron.setScale(0.5);

		var iron1 = new cc.Sprite("#iron1.png");
		iron1.setScale(0.5);
		iron1.setPosition(-3,0);
		this.addChild(iron1,11);
		
		var musai= new Button(this, 8, TAG_MUSAI, "#musai3.png",this.callback);
//		musai.setPosition(360,50);
		musai.setPosition(272,-50);
		musai.setScale(0.5);

		this.flume= new Button(this, 15, TAG_FLUME, "#flume1.png",this.callback);
		this.flume.setPosition(550,-140);
		this.flume.setScale(0.5);

		this.flume1= new Button(this, 9, TAG_FLUME1, "#flume2.png",this.callback);
		this.flume1.setPosition(550,-140.4);
		this.flume1.setScale(0.5);
		

		
		this.flumeline= new Button(this, 10, TAG_FLUMElINE1, "#flumeline.png",this.callback);
		this.flumeline.setPosition(550,-115);
		this.flumeline.setScale(0.5);
		
		this.bottle = new Button(this,11 ,TAG_BOTTLE, "#bottle.png",this.callback);
		this.bottle.setScale(0.2);
		this.bottle.setPosition(570,-140);
		
		this.bottleline = new cc.Sprite("#bottleline1.png");
		this.bottleline.setPosition(145,30);
		this.bottleline.setScale(2);
		this.bottle.addChild(this.bottleline);
		this.bottleline.setVisible(false);

		this.glass= new Button(this, 11, TAG_GLASS, "#glass1.png",this.callback);
		this.glass.setPosition(570,-180);
		this.glass.setScale(0.3);


		var flask= new exp05.Flask(this);//第10层
		flask.setPosition(160,0);


		var fenye= new exp05.Fenye(this);//第7层
		//fenye.setPosition(-250,-150);
		fenye.setPosition(168,70);
		//fenye.setRotation(90);


	},
	callback:function(p){
		var action = gg.flow.flow.action;
		var func = cc.callFunc(this.actionDone, this);	
		switch(p.getTag()){	
		case TAG_H2O2LID:
			if(action==ACTION_DO1){				
				var seq = cc.sequence(cc.moveBy(0.5,cc.p(0,80)),cc.spawn(cc.moveTo(1,cc.p(160,-80)),cc.rotateTo(1,180)),
						cc.callFunc(this.flowNext,this));			
			}else if(action==ACTION_DO2){
				var seq = cc.sequence(cc.spawn(cc.moveTo(1,cc.p(-2,160)),cc.rotateTo(1,0)),cc.moveBy(0.5,cc.p(0,-80)),
						cc.callFunc(this.flowNext,this));
			}
			p.runAction(seq);
			break;
		case TAG_FLUME:
			var move = cc.moveBy(1,cc.p(-300,0));
			var seq = cc.sequence(move,cc.delayTime(0.5),cc.callFunc(this.flowNext,this));
			this.flume.runAction(seq);
			this.flume1.runAction(move.clone());
			this.bottle.runAction(move.clone());
			this.glass.runAction(move.clone());
			this.flumeline.runAction(move.clone());
			break;
       
		case TAG_BOTTLE:
			if(action == ACTION_DO1){
				var rota = cc.rotateTo(1,-90);
				var rota1 = cc.rotateTo(1,0);
				var seq = cc.sequence(rota,cc.callFunc(function(){
					this.bottleline.setScale(3,2);
					this.bottleline.setRotation(-90);
					this.bottleline.setPosition(70,200);
					this.bottleline.setVisible(true);
					
					var mov = cc.moveTo(1,cc.p(200,200));
					var mov1 = cc.moveTo(0.5,cc.p(250,200));
					var sca = cc.scaleTo(0.1,2.5,2);
					var sp = cc.spawn(sca,mov1);
					var se = cc.sequence(mov,sp);
					this.bottleline.runAction(se);
					
				},this),cc.delayTime(2),rota1,cc.callFunc(function(){
					this.flowNext();
					this.bottleline.setVisible(false);
					this.bottleline.setPosition(145,00);
					this.bottleline.setScale(2);
					this.bottleline.setRotation(0);					
				},this));				
			}else if(action == ACTION_DO2){
				var move = cc.moveTo(1.5,cc.p(230,-110));
				var move1 = cc.moveTo(0.5,cc.p(230,-130));
				var seq = cc.sequence(move,move1,cc.callFunc(function(){
					this.bottleline.setVisible(true);				
					this.showtip = new ShowTip("用排水集气法，\n收集氧气",cc.p(1100,200));
					var se = cc.sequence(cc.moveTo(10,cc.p(145,300)),cc.spawn(cc.moveTo(2,cc.p(145,350)),cc.scaleTo(2,1.6)),
							cc.moveTo(2,cc.p(145,400)),cc.callFunc(this.flowNext,this));
					this.bottleline.runAction(se);
					ll.run.iron.getChildByTag(TAG_FENYE_NODE).pause();
					var posy = -105;
					this.schedule(function(){
						ll.run.iron.getChildByTag(TAG_FLASK_NODE).bubble1(0.5,posy);
						posy = posy - 5;
					}, 1, 12);	
//					
//					this.bottleline.runAction(se);
				},this),cc.delayTime(12),cc.callFunc(function(){
					p.schedule(function(){
						ll.run.iron.getChildByTag(TAG_FLASK_NODE).bubble2(0.5,cc.p(255,-170),cc.p(265,-120));
					}, 1, 1000);	
				},this));				
			}
			
			p.runAction(seq);
		    break;
		case TAG_GLASS:
			if (action == ACTION_DO1){
				p.setVisible(false);
				var glass = new cc.Sprite("#glass.png");
				glass.setPosition(135,15);
				glass.setScale(1.4);
				this.bottle.addChild(glass,1,TAG_SHOW);

				var move = cc.moveTo(1,cc.p(280,15));			
				var rota = cc.rotateTo(0.5,-30);

				var rota1 = cc.rotateTo(1,-90);					
				var move1 = cc.moveTo(1.5,cc.p(350,450));
				var sp = cc.spawn(rota1,move1);

				var rota2 = cc.rotateTo(1,-180);
				var move2 = cc.moveTo(1.5,cc.p(0,420));
				var sp1 = cc.spawn(rota2,move2);
				var move3 = cc.moveTo(1,cc.p(135,420));
				var seq = cc.sequence(move,rota,sp,cc.delayTime(0.5),sp1,cc.delayTime(0.5),move3,cc.delayTime(1),cc.callFunc(function(){
					this.bottle.runAction(cc.rotateTo(1,180));
				},this),cc.delayTime(1),cc.callFunc(function(){
					p.setVisible(true);
					//this.bottle.getChildByTag(TAG_SHOW).removeFromParent(true);
					this.bottle.getChildByTag(TAG_SHOW).setVisible(false);
					this.flowNext();
				},this));
				glass.runAction(seq);
			}else if (action == ACTION_DO2){
				this.getChildByTag(TAG_BOTTLE).pause();
				var move = cc.moveTo(0.5,cc.p(260,-110));
				var move1 = cc.moveTo(1,cc.p(260,20));
				var rota  = cc.rotateTo(1.5,-0);
				var move2 = cc.moveTo(1.5,cc.p(380,-50));
				var spawn = cc.spawn(rota,move2);
				var move3 = cc.moveTo(2,cc.p(420,-150));
				var seq = cc.sequence(move,cc.callFunc(function(){
					
					this.schedule(function(){
						ll.run.iron.getChildByTag(TAG_FLASK_NODE).bubble1(0.5,-105);
						//posy = posy - 5;
					}, 1, 200);	
					
					this.bottle.getChildByTag(TAG_SHOW).setVisible(true);
				},this),move1,spawn,move3,cc.callFunc(function(){
					this.flowNext();
				},this));
				this.bottle.runAction(seq);

				var mov = cc.moveTo(0.5,cc.p(260,-150));
				var se = cc.sequence(mov,cc.callFunc(function(){
					p.setVisible(false);
					p.setPosition(420,-110);
				},this));
				p.runAction(se);
			}
			
		break;
		case TAG_MUSAI:			
			var move = cc.moveTo(1,cc.p(109,30));
			var move1 = cc.moveTo(0.5,cc.p(109,-50));
			var seq = cc.sequence(move,cc.callFunc(function(){
				p.setSpriteFrame("musai3.png");	
			},this),cc.delayTime(0.5),move1,cc.callFunc(this.flowNext,this));
			p.runAction(seq);
			break;
	
		}
	},
	flowNext:function(){
		gg.flow.next();
	},
});