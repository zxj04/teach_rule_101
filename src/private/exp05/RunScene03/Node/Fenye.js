exp05.Fenye = cc.Node.extend({
	ctor:function(p){
		this._super();
		p.addChild(this, 7, TAG_FENYE_NODE);
		this.init();
	},
	init : function(){
		this.setCascadeOpacityEnabled(true);
		
		
		var fenye= new Button(this, 9, TAG_FENYE, "#fenye.png",this.callback);
		fenye.setScale(0.5);
		
		var fenyelid= new Button(this, 8, TAG_FENYELID, "#fenyelid.png",this.callback);
		fenyelid.setScale(0.5);
		fenyelid.setPosition(1,44);
		
		var fenyeline = new cc.Sprite("#beakerline.png");
		fenyeline.setPosition(0,55);
		fenyeline.setScale(0.05);
		this.addChild(fenyeline, 8, TAG_SHOW);
		
		
		//"NH₃ + CO₂ + H₂O = NH₄HCO₃\n" 	
	},
    upline:function(){
	var line = this.getChildByTag(TAG_SHOW);
	var move = cc.moveTo(2,cc.p(0,85));
	var scale = cc.scaleTo(2,0.25,0.5);
	var sp = cc.spawn(move,scale);
	line.runAction(sp);
    },
    downline:function(){
    	var line = this.getChildByTag(TAG_SHOW);
    	var move = cc.moveTo(1,cc.p(0,60));
    	var scale = cc.scaleTo(1,0.1,0.4);
    	var sp = cc.spawn(move,scale);
    	line.runAction(sp);
    },
	callback:function(p){
		var action = gg.flow.flow.action;
		var func = cc.callFunc(this.actionDone, this);	
		switch(p.getTag()){	
		case TAG_FENYELID:
			if(action == ACTION_DO1){
				p.setSpriteFrame("fenyelid1.png");
				this.downline();
				ll.run.iron.getChildByTag(TAG_FLASK_NODE).upline();
				var seq = cc.sequence(cc.delayTime(0.5),cc.callFunc(function(){
					//this.showtip = new ShowTip("\n\n2H₂O₂ == 2H₂O + O₂↑\n",cc.p(500,500));
					//this.showtip = new ShowTip("MnO₂",cc.p(520,525));
					this.schedule(function(){
						ll.run.iron.getChildByTag(TAG_FLASK_NODE).bubble(0.2);
					}, 0.05, 20);			
					this.flowNext();
				},this),cc.delayTime(1),cc.callFunc(function(){
					var flaskzq = new cc.Sprite("#flaskzq.png");
					flaskzq.setPosition(8,-104);
					flaskzq.setOpacity(0);
					flaskzq.setScale(0.5);
					ll.run.iron.addChild(flaskzq,10);
					flaskzq.runAction(cc.fadeTo(2,200));
					this.schedule(function(){
						ll.run.iron.getChildByTag(TAG_FLASK_NODE).bubble(0.1);
					}, 0.001, 100000);
				},this));				
			}else if(action == ACTION_DO2) {
				p.setSpriteFrame("fenyelid.png");
				var seq = cc.sequence(cc.delayTime(3),cc.callFunc(function(){
					this.schedule(function(){
						ll.run.iron.getChildByTag(TAG_FLASK_NODE).bubble1(0.5,-105);
					}, 1, 200);	
				},this),cc.delayTime(3),cc.callFunc(this.flowNext,this));
			}
			p.runAction(seq);
			break;
		case TAG_FENYE:
			var ber = cc.bezierTo(1.5, [cc.p(-200,70),cc.p(-80,300),cc.p(5,350)]);
			var move = cc.moveTo(1,cc.p(5,70));
			var seq = cc.sequence(ber,move,cc.callFunc(this.flowNext,this));
			this.runAction(seq);
		break;
		}
	},
	flowNext:function(){
		gg.flow.next();
	},
});