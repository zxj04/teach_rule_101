exp05.MnO2 = cc.Node.extend({
	ctor:function(p){
		this._super();
		p.addChild(this, 10, TAG_MNO2_NODE);
		this.init();
	},
	init : function(){
		this.setCascadeOpacityEnabled(true);
		var lid= new Button(this, 8, TAG_MNO2LID, "#mnlid.png",this.callback);
		lid.setPosition(0,75);

		this.mno2bottle= new Button(this, 10, TAG_MNO2BOTTLE, "#mn.png",this.callback);		
		this.mno2bottle.setCascadeOpacityEnabled(true);
		
		this.mno2bottle1 = new cc.Sprite("#mn1.png");
		this.addChild(this.mno2bottle1);
		
		
		//"NH₃ + CO₂ + H₂O = NH₄HCO₃\n" 
		var label = new cc.LabelTTF("MnO₂","微软雅黑",22);
		label.setPosition(56,77);
		label.setColor(cc.color(0,0,0));
		this.mno2bottle.addChild(label,11);
		
		var spoon= new Button(this,9, TAG_MNSPOON, "#mnspoon.png",this.callback);
		spoon.setScale(0.6);
		spoon.setRotation(50);
		spoon.setPosition(200,-60);
		
	},

	callback:function(p){
		var action = gg.flow.flow.action;
		var func = cc.callFunc(this.actionDone, this);	
		switch(p.getTag()){	
		case TAG_MNO2LID:
			var seq = cc.sequence(cc.moveTo(0.5,cc.p(0,120)),cc.moveTo(0.5,cc.p(-70,100)),cc.spawn(cc.moveTo(1,cc.p(-120,-70)),cc.rotateTo(1,-180)),
					cc.callFunc(function(){
						p.setSpriteFrame("mnlid1.png");
						p.setRotation(0);
						this.flowNext();
					},this));
			p.runAction(seq);
			break;
		case TAG_MNSPOON:
			this.mno2bottle.runAction(cc.rotateTo(1,30));
			this.mno2bottle1.runAction(cc.rotateTo(1,30));
			var sp = cc.spawn(cc.moveTo(1,cc.p(130,170)),cc.rotateTo(1,10));
			var move = cc.moveTo(1,cc.p(50,80));
			var move1 = cc.moveTo(1,cc.p(130,170));
			var move2 = cc.moveTo(1,cc.p(180,-170+50));
			var seq = cc.sequence(sp,move,cc.callFunc(function(){
				p.setSpriteFrame("mnspoon1.png");
			},this),move1,move2,cc.callFunc(function(){
				p.setSpriteFrame("mnspoon.png");
				ll.run.iron.getChildByTag(TAG_FLASK_NODE).fenmodisplay();
			},this),cc.delayTime(2),cc.callFunc(function(){
				this.setOpacity(0);
				//this.removeFromParent(true);
				
			},this),cc.delayTime(2),cc.callFunc(this.flowNext,this));
			p.runAction(seq);
			
			var mov = cc.moveBy(1,cc.p(0,120));
			var mov1 = cc.moveBy(1,cc.p(130,30));
			var mov3 = cc.moveBy(1,cc.p(-0,-120));
			var mov2 = cc.moveBy(1,cc.p(-130,-30));
			ll.run.iron.getChildByTag(TAG_FENYE_NODE).runAction(cc.sequence(mov.clone(),mov1.clone(),cc.delayTime(4),mov2.clone(),mov3.clone()));
			ll.run.iron.getChildByTag(TAG_MUSAI).runAction(cc.sequence(mov.clone(),mov1.clone(),cc.delayTime(4),mov2.clone(),mov3.clone()));
			break;
		}
	},
	flowNext:function(){
		gg.flow.next();
	},
});