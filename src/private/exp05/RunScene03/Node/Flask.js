exp05.Flask = cc.Node.extend({
	ctor:function(p){
		this._super();
		p.addChild(this, 10, TAG_FLASK_NODE);
		this.init();
	},
	init : function(){
		this.setCascadeOpacityEnabled(true);

		var flask= new Button(this, 9, TAG_FLASK, "#flask.png",this.callback);
		flask.setPosition(12,-70);
		flask.setScale(0.5);


		var mnfenmo = new cc.Sprite("#mnfenmo.png");
		mnfenmo.setPosition(12,30);
		mnfenmo.setScale(0.25,0.5);
		mnfenmo.setVisible(false);
		this.addChild(mnfenmo,11,TAG_SHOW);
		
		var flaskline = new cc.Sprite("#beakerline.png");
		flaskline.setPosition(12,-170);
		flaskline.setScale(0.2);
		this.addChild(flaskline, 8, TAG_LINE1);


	},
	upline:function(){
		var line = this.getChildByTag(TAG_LINE1);
		var move = cc.moveTo(2,cc.p(12,-160));
		var scale = cc.scaleTo(2,0.4,0.5);
		var sp = cc.spawn(move,scale);
		line.runAction(sp);
	},
	bubble:function(time){
		var bubble = new cc.Sprite("#bubble2.png");
		bubble.setPosition(30 - Math.random()*36,-170);
		bubble.setScale(0.2);
		this.addChild(bubble,12);
		var move = cc.moveTo(time,cc.p(30 - Math.random()*36,-165));
		var scale = cc.scaleTo(time,0.6);
		var sp = cc.spawn(move,scale);
		var seq = cc.sequence(sp,cc.callFunc(function(){
			bubble.removeFromParent(true);
		},this));
		bubble.runAction(seq);
	},
	bubble1:function(time,posy){
		var bubble = new cc.Sprite("#bubble2.png");
		bubble.setPosition(225 ,-160);
		bubble.setScale(0.4);
		this.addChild(bubble,12);
		var move = cc.moveTo(time,cc.p(225,posy));
		var scale = cc.scaleTo(time,1);
		var sp = cc.spawn(move,scale);
		var seq = cc.sequence(sp,cc.callFunc(function(){
			bubble.removeFromParent(true);
		},this));
		bubble.runAction(seq);
	},
	bubble2:function(time,pos,pos2){
		var bubble = new cc.Sprite("#bubble2.png");
		bubble.setPosition(pos);
		bubble.setScale(0.4);
		this.addChild(bubble,12);
		var move = cc.moveTo(time,pos2);
		var scale = cc.scaleTo(time,1);
		var sp = cc.spawn(move,scale);
		var seq = cc.sequence(sp,cc.callFunc(function(){
			bubble.removeFromParent(true);
		},this));
		bubble.runAction(seq);
	},
	movemusai:function(){
		var musai = this.getChildByTag(TAG_MUSAI);
		var move = cc.moveTo(0.5,cc.p(112,-10));
		var ber = cc.bezierTo(1,[cc.p(122,0),cc.p(142,-30),cc.p(152,-60)]);
		var delay = cc.delayTime(2.5);
		var move1 = cc.moveTo(0.5,cc.p(112,-50));
		var ber1 = cc.bezierTo(1,[cc.p(142,-30),cc.p(122,0),cc.p(112,-10)]);
		var seq = cc.sequence(move,ber,delay,ber1,move1,cc.callFunc(this.flowNext,this));
		musai.runAction(seq);
	},
	fenmodisplay:function(){
		var fenmo = this.getChildByTag(TAG_SHOW);
		fenmo.setVisible(true);
		var move = cc.moveTo(1,cc.p(12,-70));
		var sp = cc.spawn(cc.moveTo(1,cc.p(12,-170)),cc.scaleTo(1,0.5,0.3));
		var seq = cc.sequence(move,sp);
		fenmo.runAction(seq);
	},
	callback:function(p){
		var action = gg.flow.flow.action;
		var func = cc.callFunc(this.actionDone, this);	
		switch(p.getTag()){	
		case TAG_H2O2LID:
			if(action==ACTION_DO1){				
				var seq = cc.sequence(cc.moveBy(0.5,cc.p(0,80)),cc.spawn(cc.moveTo(1,cc.p(160,-80)),cc.rotateTo(1,180)),
						cc.callFunc(this.flowNext,this));			
			}else if(action==ACTION_DO2){
				var seq = cc.sequence(cc.spawn(cc.moveTo(1,cc.p(-2,160)),cc.rotateTo(1,0)),cc.moveBy(0.5,cc.p(0,-80)),
						cc.callFunc(this.flowNext,this));
			}
			p.runAction(seq);
			break;
		case TAG_FLASK:
			if(action ==ACTION_DO1){
				var move = cc.moveBy(1,cc.p(100,100));
				var move1 = cc.moveBy(0.5,cc.p(50,-100));
				var seq = cc.sequence(move,move1,cc.callFunc(function(){
					this.schedule(function(){
						this.bubble1(0.7,-120);
					}, 0.5, 200);
				},this),cc.delayTime(3),cc.callFunc(this.flowNext,this));
				this.runAction(seq);
				this.getParent().getChildByTag(TAG_FENYE_NODE).runAction(cc.sequence(move.clone(),move1.clone()));
				this.getParent().getChildByTag(TAG_MUSAI).runAction(cc.sequence(move.clone(),move1.clone()));
			}else if(action ==ACTION_DO2){
				this.pause();
				var move1 = cc.moveBy(1,cc.p(-100,-100));
				var move = cc.moveBy(0.5,cc.p(-50,100));
				var seq = cc.sequence(cc.delayTime(0.5),move,move1,cc.callFunc(this.flowNext,this));
				this.runAction(seq);
				this.getParent().getChildByTag(TAG_FENYE_NODE).runAction(cc.sequence(cc.delayTime(0.5),move.clone(),move1.clone()));
				this.getParent().getChildByTag(TAG_MUSAI).runAction(cc.sequence(cc.delayTime(0.5),move.clone(),move1.clone()));
			}else{
				var move = cc.moveBy(1,cc.p(-163,0));
				var seq = cc.sequence(move,cc.callFunc(this.flowNext,this));
				this.runAction(seq);
				this.getParent().getChildByTag(TAG_FENYE_NODE).runAction(move.clone());
				this.getParent().getChildByTag(TAG_MUSAI).runAction(move.clone());
			}
			
			break;
		}
	},
	flowNext:function(){
		gg.flow.next();
	},
});