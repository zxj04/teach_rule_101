exp05.H2O2 = cc.Node.extend({
	ctor:function(p){
		this._super();
		p.addChild(this, 10, TAG_H2O2_NODE);
		this.init();
	},
	init : function(){
		this.setCascadeOpacityEnabled(true);
		var lid= new Button(this, 9, TAG_H2O2LID, "#h2o2lid.png",this.callback);
		lid.setPosition(0,85);
		lid.setScale(0.6);

		this.h2o2bottle= new Button(this, 10, TAG_H2O2BOTTLE, "#h2o2.png",this.callback);
		this.h2o2bottle.setScale(0.6);
		this.h2o2bottle.setCascadeOpacityEnabled(true);
		
		this.h2o2bottle1 = new cc.Sprite("#h2o21.png");
		this.h2o2bottle1.setScale(0.6);
		this.addChild(this.h2o2bottle1);
		
		var label = new cc.LabelTTF("H₂O₂","微软雅黑",36);
		label.setPosition(100,140);
		label.setColor(cc.color(0,0,0));
		this.h2o2bottle.addChild(label,11);
	},
	xuanzhuan:function(){
		var frames=[];
		for (i=1;i<=8;i++){
			var str ="H2O2/"+i+".png";
			var frame=cc.spriteFrameCache.getSpriteFrame(str);
			frames.push(frame);
		}
		var animation = new cc.Animation(frames,0.05);//负责动画序列
		var action = new cc.Animate(animation);//帧动画的动作创建
		return action;
	},

	callback:function(p){
		var action = gg.flow.flow.action;
		var func = cc.callFunc(this.actionDone, this);	
		switch(p.getTag()){	
		case TAG_H2O2LID:
			var seq = cc.sequence(cc.moveTo(0.5,cc.p(0,120)),cc.moveTo(0.5,cc.p(-70,100)),cc.spawn(cc.moveTo(1,cc.p(-150,-70)),cc.rotateTo(1,180)),
					cc.callFunc(function(){
						p.setSpriteFrame("h2o2lid1.png");
						p.setRotation(0);
						this.flowNext();
					},this));
			p.runAction(seq);
			break;
		case TAG_H2O2BOTTLE:
			var move = cc.moveTo(1,cc.p(200,80));
			var move1 = cc.moveTo(1,cc.p(300,50));
			var move2 = cc.moveTo(1,cc.p(330,-40));
			var rota = cc.rotateTo(1,-80);
			var sp = cc.spawn(move2,rota);
			
			var bottle = new cc.Sprite("#H2O2/1.png");
			bottle.setPosition(0,0);
			bottle.setScale(1.06);
			this.addChild(bottle,10);
			var sq = cc.sequence(this.xuanzhuan(),move,move1,sp);
			bottle.runAction(sq);
			
            this.h2o2bottle.setVisible(false);
            this.h2o2bottle1.setVisible(false);
            
            var move1 = cc.moveTo(2,cc.p(-350,0));
            var rota1 = cc.rotateTo(2,0);
            var sp1 = cc.spawn(move1,rota1);
            var seq = cc.sequence(cc.delayTime(3.5),cc.callFunc(function(){
            	ll.run.iron.getChildByTag(TAG_FENYE_NODE).upline();
            },this),cc.delayTime(2),cc.callFunc(function(){
            	this.setOpacity(0);
            //	this.removeFromParent(true);
            	gg.flow.next();
            },this));
            ll.run.iron.getChildByTag(TAG_FENYE_NODE).runAction(seq);
		break;
		}
	},
	flowNext:function(){
		gg.flow.next();
	},
});