exp05.RunLayer3 = cc.Layer.extend({
	arr:null,
	scene:null,
	clock:null,
	ctor:function (parent) {
		this._super();
		this.scene = parent;
		this.scene.addChild(this, 10);
		gg.main = this;
		this.init();
	},
	init:function () {
		this.callNext = cc.callFunc(function(){
			gg.flow.next();
		}, this);
		this.callKill = cc.callFunc(function(p){
			var seq = cc.sequence(cc.fadeOut(0.5),cc.callFunc(function(){
				p.removeFromParent(true);	
			}, this));
			p.runAction(seq);
		}, this); 

		this.callNext.retain();
		this.callKill.retain();
		//时钟
		this.clock = new Clock(this);
		// 物品库
		this.lib = new Lib(this);
		this.lib.loadBg([{	
			tag:TAG_LIB_H2O2,
			checkright:true,
		},
		{
			tag:TAG_LIB_BOTTLE,
		},
		{		
			tag:TAG_LIB_MATCH,
		},
		{
			tag:TAG_LIB_MNO2,
			checkright:true,
		},
		{	
			tag:TAG_LIB_AMPULLA,
			checkright:true,
		},
		{
			tag:TAG_LIB_PBOTTLE,
		},
		{
			tag:TAG_LIB_FENYE,
			checkright:true,
		},
		{	
			tag:TAG_LIB_FLUME,
			checkright:true,
		},
		{
			tag:TAG_LIB_LAMP,
		},
		{
			tag:TAG_LIB_FLASK,
			checkright:true,
		}
		]);

		this.hbottle = new exp05.H2O2(this);
		this.hbottle.setVisible(false);
		this.hbottle.setPosition(370,530);

		this.mbottle = new exp05.MnO2(this);
		this.mbottle.setVisible(false);
		this.mbottle.setPosition(650,520);

		this.iron = new exp05.Iron(this);
		this.iron.setVisible(false);
		this.iron.setPosition(600,270);

		this.batten = new exp05.Batten1(this);
		this.batten.setVisible(false);
		this.batten.setPosition(1150,500);

	},
	checkVisible:function(next){
		//是否可见
		var checkVisible = [];
		var node1 = ll.run.hbottle;
		var node2 = ll.run.mbottle;
		var node3 = ll.run.iron;
		var node4 = ll.run.batten;

		checkVisible.push(node1,node2,node3,node4);

		for(var i in checkVisible){
			if(checkVisible[i] !== null){				
				checkVisible[i].setVisible(next);				
			}			
		}
	},
	loadInLib:function(obj, pos, tarPos,delay){
		obj.setPosition(pos);
		if(delay == null){
			delay = 1;
		}
		var ber = $.bezier(pos, tarPos, delay);
		var seq = cc.sequence(ber, this.callNext);
		obj.runAction(seq);
	},
	kill:function(obj){
		var fade = cc.fadeTo(0.5,0);
		var func = cc.callfunc(function(){
			obj.removeFromParent(true);
		},this);
		var seq = cc.sequence(fade,func);
		obj.runAction(seq)
	},

	callback:function (p){
		var func = cc.callFunc(this.actionDone, this);
		var action=gg.flow.flow.action;
		switch(p.getTag()){

		}
	},
	actionDone:function(p){
		var func = cc.callFunc(this.actionDone, this);
		switch(p.getTag()){

		}
	},
	flowNext:function(){
		gg.flow.next();
	},
	onExit:function(){
		this._super();
		this.callNext.release();
		this.callKill.release();
	}
});