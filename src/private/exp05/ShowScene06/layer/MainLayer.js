exp05.ShowMainLayer06 = cc.Layer.extend({
	lead:null,
	real:null,
	curIndex: 0,
	ctor:function () {
		this._super();
		this.init();
		return true;
	},
	init:function(){
		var sv = new pub.ScrollView(this);
		sv.setOffestPos(cc.p(152,45 + 15));

		this.addlayout1(sv);
		this.addlayout2(sv);
		this.addlayout3(sv);
	},	
	addlayout1:function(parent){
		this.layout1 = new ccui.Layout();//第一页
		this.layout1.setContentSize(cc.size(996, 598));//设置layout的大小
		parent.addLayer(this.layout1);

		var text = $.format("　　下列变化一定是化学变化的是（）", 860, 30);

		label1 = new cc.LabelTTF(text,gg.fontName,gg.fontSize4);
		label1.setAnchorPoint(0,0.5);
		label1.setPosition(0,this.layout1.height-label1.height/2);
		label1.setColor(cc.color(0, 0, 0, 250));
		this.layout1.addChild(label1);	

		this.addSelImage(this.layout1, "#select.png", TAG_BUTTON, cc.p(60,480 ),
				"A.燃烧","#right.png",TAG_SEL);

		this.addSelImage(this.layout1, "#select.png", TAG_BUTTON1, cc.p(60,430),
				"B.爆炸","#wrong.png",TAG_SEL1);

		this.addSelImage(this.layout1, "#select.png", TAG_BUTTON2, cc.p(60,380),
				"C.升华","#wrong.png",TAG_SEL2);

		this.addSelImage(this.layout1, "#select.png", TAG_BUTTON3, cc.p(60,330), 
				"D.变色","#wrong.png",TAG_SEL3);
		var text = $.format("　　解析：不能只凭发生变化时的现象进行判断，这样易出现判断不正确的情况，避免出错的关键只有透过现象抓住本质" +
				"即关键是看有没有新的物质的生成", 920, 25);

		this.label1 = new cc.LabelTTF(text,gg.fontName,25);
		this.label1.setAnchorPoint(0,0.5);
		this.label1.setPosition(0,180);
		this.label1.setColor(cc.color(0, 0, 0, 250));
		this.label1.setVisible(false);
		this.layout1.addChild(this.label1);	
	},
	addlayout2:function(parent){
		this.layout2 = new ccui.Layout();//第四页
		this.layout2.setContentSize(cc.size(996, 598));
		parent.addLayer(this.layout2);

		var text = $.format("　　下列关于催化剂的说法正确的是（）", 960, 30);
		label1 = new cc.LabelTTF(text ,gg.fontName,gg.fontSize4);
		label1.setAnchorPoint(0,0.5);
		label1.setPosition(0,this.layout2.height-label1.height/2);
		label1.setColor(cc.color(0, 0, 0, 250));
		this.layout2.addChild(label1);	

		this.addSelImage(this.layout2, "#select.png", TAG_BUTTON4, cc.p(60,480 ),
				"A.加热氯酸钾制备氧气时，加入催化剂可以使生成的氧气质量增加","#wrong.png",TAG_SEL4);

		this.addSelImage(this.layout2, "#select.png", TAG_BUTTON5, cc.p(60,430 ),
				"B.催化剂必加快其他物质的反应速率","#wrong.png",TAG_SEL5);

		this.addSelImage(this.layout2, "#select.png", TAG_BUTTON6, cc.p(60,380 ),
				"C.二氧化锰只能做催化剂","#wrong.png",TAG_SEL6);

		this.addSelImage(this.layout2, "#select.png", TAG_BUTTON7, cc.p(60,330 ), 
				"D.加入催化剂，氯酸钾发生分解反应温度降低，放出氧气速率增大","#right.png",TAG_SEL7);
		var text = $.format("　　分析：干扰选项是A、B，催化剂的作用是影响反应速率，可以加快可以减慢，对反应的结果并不影响。所以A、B错误。" +
				"而答案C则是以偏概全，二氧化锰在别的反应中可以使反应物也可以是生成物，比如二氧化锰可以和浓盐酸反应生成氯气。", 920, 25);

		this.label2 = new cc.LabelTTF(text,gg.fontName,25);
		this.label2.setAnchorPoint(0,0.5);
		this.label2.setPosition(0,180);
		this.label2.setColor(cc.color(0, 0, 0, 250));
		this.label2.setVisible(false);
		this.layout2.addChild(this.label2);				
	},
	addlayout3:function(parent){
		this.layout3 = new ccui.Layout();//第四页
		this.layout3.setContentSize(cc.size(996, 598));
		parent.addLayer(this.layout3);
        
		var text = $.format("　　同学们在做氧气性质实验时，将点燃的木炭伸入集气瓶内，有的现象明显，有的却不明显。导致现象不明显的原因可能是（）", 960, 30)
		label1 = new cc.LabelTTF(text ,gg.fontName,gg.fontSize4);
		label1.setAnchorPoint(0,0.5);
		label1.setPosition(0,this.layout2.height-label1.height/2);
		label1.setColor(cc.color(0, 0, 0, 250));
		this.layout3.addChild(label1);	

		this.addSelImage(this.layout3, "#select.png", TAG_BUTTON8, cc.p(60,480),
				"A.排水法收集前未将集气瓶灌满水","#right.png",TAG_SEL8);

		this.addSelImage(this.layout3, "#select.png", TAG_BUTTON9, cc.p(60,430),
				"B.导管口连续放出气泡时开始收集氧气","#wrong.png",TAG_SEL9);

		this.addSelImage(this.layout3, "#select.png", TAG_BUTTON10, cc.p(60,380),
				"C.收集满后盖上毛玻璃片拿出水面","#wrong.png",TAG_SEL10);

		this.addSelImage(this.layout3, "#select.png", TAG_BUTTON11, cc.p(60,330), 
				"D.收集满氧气的集气瓶正放于桌面","#wrong.png",TAG_SEL11);
		var text = $.format("　　分析：氧气的纯度会影响氧气助燃实验现象明显程度，用排水法收集氧气灌满水，可以使收集到的氧气纯度较高。 故本题选A。", 960, 25);

		this.label3 = new cc.LabelTTF(text,gg.fontName,25);
		this.label3.setAnchorPoint(0,0.5);
		this.label3.setPosition(0,180);
		this.label3.setColor(cc.color(0, 0, 0, 250));
		this.label3.setVisible(false);
		this.layout3.addChild(this.label3);				
	},

	addSelImage:function(parent,str,tag,pos,str2,str3,tag2){
		var image = new ButtonScale(parent,str,this.callback,this);
		image.setPosition(pos);
		image.setTag(tag);

		var sel = new cc.Sprite(str3);
		sel.setPosition(15,15);
		image.addChild(sel);
		sel.setVisible(false);
		sel.setTag(tag2);

		var label = new cc.LabelTTF(str2,gg.fontName,gg.fontSize);
		label.setColor(cc.color(0, 0, 0, 250));
		label.setAnchorPoint(0,0.5);
		parent.addChild(label);
		label.setPosition(image.x + 20,image.y);
	},
	callback:function(p){
		switch(p.getTag()){	
		case TAG_BUTTON:
		case TAG_BUTTON1:
		case TAG_BUTTON2:
		case TAG_BUTTON3:
			this.layout1.getChildByTag(TAG_BUTTON).getChildByTag(TAG_SEL).setVisible(true);
			this.layout1.getChildByTag(TAG_BUTTON1).getChildByTag(TAG_SEL1).setVisible(true);
			this.layout1.getChildByTag(TAG_BUTTON2).getChildByTag(TAG_SEL2).setVisible(true);
			this.layout1.getChildByTag(TAG_BUTTON3).getChildByTag(TAG_SEL3).setVisible(true);
			this.label1.setVisible(true);	
			break;

		case TAG_BUTTON4:
		case TAG_BUTTON5:
		case TAG_BUTTON6:
		case TAG_BUTTON7:
			this.layout2.getChildByTag(TAG_BUTTON4).getChildByTag(TAG_SEL4).setVisible(true);
			this.layout2.getChildByTag(TAG_BUTTON5).getChildByTag(TAG_SEL5).setVisible(true);
			this.layout2.getChildByTag(TAG_BUTTON6).getChildByTag(TAG_SEL6).setVisible(true);
			this.layout2.getChildByTag(TAG_BUTTON7).getChildByTag(TAG_SEL7).setVisible(true);
			this.label2.setVisible(true);
			break;		
		case TAG_BUTTON8:
		case TAG_BUTTON9:
		case TAG_BUTTON10:
		case TAG_BUTTON11:
			this.layout3.getChildByTag(TAG_BUTTON8).getChildByTag(TAG_SEL8).setVisible(true);
			this.layout3.getChildByTag(TAG_BUTTON9).getChildByTag(TAG_SEL9).setVisible(true);
			this.layout3.getChildByTag(TAG_BUTTON10).getChildByTag(TAG_SEL10).setVisible(true);
			this.layout3.getChildByTag(TAG_BUTTON11).getChildByTag(TAG_SEL11).setVisible(true);
			this.label3.setVisible(true);
			break;	
		}
	}	
});
