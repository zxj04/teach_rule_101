exp05.ShowMainLayer02 = cc.Layer.extend({
	lead:null,
	real:null,
	reset:true,
	ctor:function () {
		this._super();
		this.init();
		return true;
	},
	init:function(){	
		var sv = new pub.ScrollView(this);
		sv.setOffestPos(cc.p(152,45 + 15));

		
		this.addlayout1(sv);	
		this.addlayout2(sv);	
		this.addlayout3(sv);
	},	
	addlayout1:function(parent){
		this.layout1 = new ccui.Layout();//第一页
		this.layout1.setContentSize(cc.size(996, 598));//设置layout的大小
		parent.addLayer(this.layout1);

		var text = $.format("　　空气是一种重要的天然资源，在人们的生活中有着广泛的用途。" +
				"\n　　氧气跟人们的生活及生产关系最为密切。", 980, 30);
		label1 = new cc.LabelTTF(text,gg.fontName,gg.fontSize4);
		label1.setAnchorPoint(0,0.5);
		label1.setPosition(0,this.layout1.height-label1.height/2);
		label1.setColor(cc.color(0, 0, 0, 250));
		this.layout1.addChild(label1);	
		
		
		this.addshowimage(this.layout1, "#huxi.png", cc.p(250,300), "供给呼吸");
		this.addshowimage(this.layout1, "#qiege.png", cc.p(750,300), "切割金属");
		
		label1 = new cc.LabelTTF("你还知道氧气有哪些用途吗？",gg.fontName,gg.fontSize4);
		label1.setAnchorPoint(0,0.5);
		label1.setPosition(0,100);
		label1.setColor(cc.color(0, 0, 0, 250));
		this.layout1.addChild(label1);	
		
	},
	addlayout2:function(parent){
		this.layout2 = new ccui.Layout();//第四页
		this.layout2.setContentSize(cc.size(996, 598));
		parent.addLayer(this.layout2);
        
		var text = $.format("　　氮气是一种无色、无味、性质较不活泼的气体。在灯泡中充氮气可以延长寿命，" +
				"氮气还是制造化肥和炸药的重要原料。\n　　由于液氮汽化吸收大量热，因此可做冷冻剂。", 980, 30);
		label1 = new cc.LabelTTF(text,gg.fontName,gg.fontSize4);
		label1.setAnchorPoint(0,0.5);
		label1.setPosition(0,this.layout2.height-label1.height/2);
		label1.setColor(cc.color(0, 0, 0, 250));
		this.layout2.addChild(label1);	

		this.addshowimage(this.layout2, "#huafei.png", cc.p(150,300), "化肥");
		this.addshowimage(this.layout2, "#zhayao.png", cc.p(500,300), "炸药");
		this.addshowimage(this.layout2, "#yedan.png", cc.p(850,300), "冷冻剂");
		
		label1 = new cc.LabelTTF("你还知道氮气有哪些用途吗？",gg.fontName,gg.fontSize4);
		label1.setAnchorPoint(0,0.5);
		label1.setPosition(0,100);
		label1.setColor(cc.color(0, 0, 0, 250));
		this.layout2.addChild(label1);	
		
	},
	addlayout3:function(parent){
		this.layout3 = new ccui.Layout();//第四页
		this.layout3.setContentSize(cc.size(996, 598));
		parent.addLayer(this.layout3);

		var text = $.format("　　稀有气体的化学性质不活泼，常用作保护气，如焊接金属时用隔绝空气。" +
				"\n　　由于通电时发出不同颜色的光，人们用它做成各种光源，有些稀有气体可以用于激光技术。", 980, 30);
		label1 = new cc.LabelTTF(text,gg.fontName,gg.fontSize4);
		label1.setAnchorPoint(0,0.5);
		label1.setPosition(0,this.layout2.height-label1.height/2);
		label1.setColor(cc.color(0, 0, 0, 250));
		this.layout3.addChild(label1);	

		this.addshowimage(this.layout3, "#nihongdeng.png", cc.p(250,300), "霓虹灯");
		this.addshowimage(this.layout3, "#jiguang.png", cc.p(750,300), "激光");

		label1 = new cc.LabelTTF("你还知道稀有气体有哪些用途吗？",gg.fontName,gg.fontSize4);
		label1.setAnchorPoint(0,0.5);
		label1.setPosition(0,100);
		label1.setColor(cc.color(0, 0, 0, 250));
		this.layout3.addChild(label1)
	},
	addshowimage:function(parent,str,pos,str2){
		var showimage = new cc.Sprite(str);
		showimage.setPosition(pos);
		parent.addChild(showimage,5);
		var label = new cc.LabelTTF(str2,gg.fontName,gg.fontSize);
		label.setColor(cc.color(0, 0, 0, 250));
		parent.addChild(label);
		label.setPosition(showimage.x,showimage.y - showimage.height * 0.5 -20);
	},
	addbutton:function(parent,tag,str,pos){
		var button = new ButtonScale(parent,"#unsel.png",this.callback,this);
		button.setPosition(pos);
		button.setLocalZOrder(5);
		button.setTag(tag);
		var label = new cc.LabelTTF(str,gg.fontName,30);
		label.setPosition(button.width/2,button.height/2);		
		label.setColor(cc.color(0, 0, 0, 250));
		button.addChild(label);
		label.setTag(tag);
	},
	callback:function(p){
		switch(p.getTag()){	

		}
	}
});
