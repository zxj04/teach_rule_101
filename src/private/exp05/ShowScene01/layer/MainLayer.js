exp05.ShowMainLayer01 = cc.Layer.extend({
	lead:null,
	real:null,
	reset:false,
	reset1:false,
	open_flag:false,
	donum:0,
	ctor:function () {
		this._super();
		this.init();
		return true;
	},
	init:function(){	
		var sv = new pub.ScrollView(this);
		sv.setOffestPos(cc.p(152,45 + 15));

		this.addlayout1(sv);	
		this.addlayout2(sv);	
	},	
	addlayout1:function(parent){
		this.layout1 = new ccui.Layout();//第一页
		this.layout1.setContentSize(cc.size(996, 598));//设置layout的大小
		parent.addLayer(this.layout1);
	
		var text = $.format("　　人们生活离不开空气，那么空气中到底含有什么呢？" , 980,30);
		label = new cc.LabelTTF(text,gg.fontName,gg.fontSize4);
		label.setColor(cc.color(0, 0, 0, 250));
		label.setAnchorPoint(0, 0.5);
		label.setPosition(0,this.layout1.height-label.height/2);
		this.layout1.addChild(label);	

		this.addImage(this.layout1,"#kongqi1.png",cc.p(250,360));
		this.addImage(this.layout1,"#kongqi.png",cc.p(750,360));

		this.addbutton(this.layout1, TAG_BUTTON, "实验", cc.p(500,200));

		var text = $.format("　　科学表明，①二氧化碳可以使澄清石灰水变浑浊，②氧气具有助燃性，氧气溶度越高，燃烧越旺盛。" +
				"我们通过实验来说明空气中含有二氧化碳和氧气。", 980,30);
		label = new cc.LabelTTF(text,gg.fontName,gg.fontSize4);
		label.setColor(cc.color(0, 0, 0, 250));
		label.setAnchorPoint(0, 0.5);
		label.setPosition(0,100);
		this.layout1.addChild(label);							
	},
	addlayout2:function(parent){
	    this.layout2 = new ccui.Layout();//第一页
	    this.layout2.setContentSize(cc.size(996, 598));//设置layout的大小
	    parent.addLayer(this.layout2);

	    var text = $.format("　　法国化学家拉瓦锡首先通过实验得出了空气是由氮气和氧气组成，其中氧气含量约占空气体积的1/5的结论。" +
	    		"\n　　19世纪末，科学家又通过大量实验发现空气中还有氦、氩等稀有气体", 980,30);
	    label = new cc.LabelTTF(text,gg.fontName,gg.fontSize4);
	    label.setColor(cc.color(0, 0, 0, 250));
	    label.setAnchorPoint(0, 0.5);
	    label.setPosition(0,this.layout2.height-label.height/2);
	    this.layout2.addChild(label);	
	     
	    this.addImage(this.layout2,"#hlrs.png",cc.p(500,290));
        this.addbutton(this.layout2, TAG_BUTTON1, "实验", cc.p(800,150));
        
        var text = $.format("　　大量实验表明，空气中各成分的体积大约是：氮气78%，氧气20%，稀有气体0.93%，" +
        		"二氧化碳0.04%水蒸气及其他气体和杂质0.03%。", 960, 30)
        		this.label1 = new cc.LabelTTF(text,gg.fontName,gg.fontSize4);
        this.label1.setColor(cc.color(255, 0, 0, 250));
        this.label1.setAnchorPoint(0,0.5);
        this.label1.setPosition(0,70);
        this.layout2.addChild(this.label1);
	},
	addImage:function(parent,str,pos){
		var image = new cc.Sprite(str);
		parent.addChild(image,5);
		image.setPosition(pos);
	},
	addbutton:function(parent,tag,str,pos){
		var button = new ButtonScale(parent,"#unsel.png",this.callback,this);
		button.setPosition(pos);
		button.setLocalZOrder(5);
		button.setTag(tag);
		var label = new cc.LabelTTF(str,gg.fontName,30);
		label.setPosition(button.width/2,button.height/2);		
		label.setColor(cc.color(0, 0, 0, 250));
		button.addChild(label);
		label.setTag(tag);
	},
	callback:function(p){
		switch(p.getTag()){	
		case TAG_BUTTON:			
			gg.teach_type = TAG_LEAD;
			gg.teach_flow = exp05.teach_flow01;
			pub.ch.gotoRun(TAG_EXP_05, TAG_MENU2,exp05.g_resources_run, null, new exp05.RunScene01());	
			break;
		case TAG_BUTTON1:			
			gg.teach_type = TAG_LEAD;
			gg.teach_flow = exp05.teach_flow02;
			pub.ch.gotoRun(TAG_EXP_05, TAG_MENU3,exp05.g_resources_run, null, new exp05.RunScene02());
			break;
			
		}
	}
});
