exp05.Tongqi = cc.Node.extend({
	ctor:function(p){
		this._super();
		p.addChild(this, 10, TAG_TONGQI_NODE);
		this.init();
	},
	init : function(){
		this.setCascadeOpacityEnabled(true);
		
		var flaskbeaker= new Button(this, 10, TAG_FLASKBEAKER, "#flaskbeaker.png",this.callback);
		flaskbeaker.setScale(0.45);

		var musai= new Button(this, 9, TAG_MUSAI, "#musai.png",this.callback);
		musai.setScale(0.45);
		musai.setPosition(18,105);

		var musaixia= new Button(this, 7, TAG_MUSAIXIA, "#musaixia.png",this.callback);
		musaixia.setScale(0.45);
		musaixia.setPosition(-10,-55);

		var xiangjiaoguan= new Button(this, 10, TAG_XIANGJIAOGUAN, "#xiangjiaoguan.png",this.callback);
		xiangjiaoguan.setScale(0.45);
		xiangjiaoguan.setPosition(-190,188);

		var inoculator1= new Button(this, 10, TAG_INOCULATOR1, "#inoculator1.png",this.callback);
		inoculator1.setScale(0.45);
		inoculator1.setPosition(-345,170);

		var inoculator2= new Button(this, 9, TAG_INOCULATOR2, "#inoculator2.png",this.callback);
		inoculator2.setScale(0.45);
		inoculator2.setPosition(-450,170);

		var inoculator3= new Button(this, 8, TAG_INOCULATOR3, "#inoculator3.png",this.callback);
		inoculator3.setScale(0.45);
		inoculator3.setPosition(-345,170);

		var waterline= new Button(this, 8, TAG_WATERLINE, "#waterline.png",this.callback);
		waterline.setScale(0.45);
		waterline.setPosition(-3,-26);
		
	},
	bubble:function(){
		var bubble = new cc.Sprite("#bubble2.png");
		bubble.setPosition(-6,-95);
		this.addChild(bubble,6);
		var ber = cc.bezierTo(0.8, [cc.p(10,-125),cc.p(20,-80),cc.p(30,-45)]);
		var seq = cc.sequence(cc.spawn(ber,cc.scaleTo(0.8,1.5)),cc.callFunc(function(){
			bubble.removeFromParent(true);
		},this));
		bubble.runAction(seq);
	},
	callback:function(p){
		var action = gg.flow.flow.action;
		var func = cc.callFunc(this.actionDone, this);	
		switch(p.getTag()){	
		case TAG_INOCULATOR2:
		var  seq = cc.sequence(cc.callFunc(function(){
			var hunzhuo = new cc.Sprite("#hunzhuo.png");
			hunzhuo.setPosition(-2,-70);
			hunzhuo.setScale(0.45);
			hunzhuo.setOpacity(0);
			this.addChild(hunzhuo,7);
			hunzhuo.runAction(cc.fadeTo(3,100));
			this.schedule(function(){
				this.bubble();
			}, 0.2, 15);
		},this),cc.moveBy(2,cc.p(110,0)),cc.callFunc(function(){
			this.showTip = new ShowTip("澄清石灰水变浑浊",cc.p(1100,480));
			if(ll.run.lib.isOpen()){
				this.showTip.fadeout();
			}else{
				this.showTip.fadein();
			}
			
		},this),cc.delayTime(5),cc.callFunc(function(){
			this.setOpacity(0);
			//this.removeFromParent(true);
			ll.run.getChildByTag(TAG_SHOW).removeFromParent(true);
			gg.flow.next();
		},this));
		p.runAction(seq);
        break;
		}
	},
	flowNext:function(){
		gg.flow.next();
	},
});