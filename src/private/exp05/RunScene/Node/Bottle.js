exp05.Bottle05 = cc.Node.extend({
	ctor:function(p){
		this._super();
		p.addChild(this, 10, TAG_BOTTLE_NODE);
		this.init();
	},
	init : function(){
		this.setCascadeOpacityEnabled(true);

		var oxygen= new Button(this, 10, TAG_OXYGEN, "#oxygen.png",this.callback);
		oxygen.setScale(0.5);

		this.glass1= new Button(this, 9, TAG_GLASS, "#glass.png",this.callback);
		this.glass1.setScale(0.7);
		this.glass1.setPosition(0,99);
		
		var air= new Button(this, 10, TAG_AIR, "#air.png",this.callback);
		air.setScale(0.5);
		air.setPosition(250,0);
		
		this.glass2= new Button(this, 9, TAG_GLASS1, "#glass.png",this.callback);
		this.glass2.setScale(0.7);
		this.glass2.setPosition(250,99);

		

	},

	callback:function(p){
		var action = gg.flow.flow.action;
		var func = cc.callFunc(this.actionDone, this);	
		switch(p.getTag()){	
		case TAG_GLASS:
			
			break;
		case TAG_GLASS1:

			break;
		}
	},
	flowNext:function(){
		gg.flow.next();
	},
});