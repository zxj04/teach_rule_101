exp05.Match05 = cc.Node.extend({
	ctor:function(p){
		this._super();
		p.addChild(this, 6, TAG_MATCH_NODE);
		this.init();
	},
	init : function(){
		this.setCascadeOpacityEnabled(true);


		var match= new Button(this, 1, TAG_MATCH, "#libmatch.png",this.callback);
	    match.setScale(0.6);


	},

	callback:function(p){
		var action = gg.flow.flow.action;
		var func = cc.callFunc(this.actionDone, this);	
		switch(p.getTag()){	
		case TAG_MATCH:
			var match = new cc.Sprite("#match1.png");
			match.setPosition(180,130);
			match.setScale(0.6);
			match.setAnchorPoint(1,1);
			this.addChild(match,2);
			var seq = cc.sequence(cc.moveTo(0.3,cc.p(240,100)),cc.callFunc(function(){
				match.setSpriteFrame("match2.png");
			},this),cc.moveTo(1.2,cc.p(100,-235)),cc.callFunc(function(){
				var fire = new cc.Sprite("#fire/1.png");
				fire.setPosition(165,350);
				fire.setScale(2.5);
				ll.run.lamp.lamp.addChild(fire);
				fire.runAction(ll.run.lamp.fire().repeatForever());
				this.setOpacity(0);
			//	this.removeFromParent(true);
				gg.flow.next();
			},this));
			
			match.runAction(seq);
			break;

		}
	},
	flowNext:function(){
		gg.flow.next();
	},
});