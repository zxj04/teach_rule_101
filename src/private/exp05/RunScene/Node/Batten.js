exp05.Batten = cc.Node.extend({
	ctor:function(p){
		this._super();
		p.addChild(this, 9, TAG_BATTEN_NODE);
		this.init();
	},
	init : function(){
		this.setCascadeOpacityEnabled(true);
		var batten= new Button(this, 11, TAG_BATTEN, "#mutiao.png",this.callback);
		batten.setPosition(100,0);
		batten.setScale(0.5);
		
		
		var batten1= new Button(this, 11, TAG_BATTEN1, "#mutiao.png",this.callback);		
		batten1.setScale(0.5);

		
	},
	fire:function(){
		var frames=[];
		for (i=1;i<=3;i++){
			var str ="fire/"+i+".png";
			var frame=cc.spriteFrameCache.getSpriteFrame(str);
			frames.push(frame);
		}
		var animation = new cc.Animation(frames,0.05);//负责动画序列
		var action = new cc.Animate(animation);//帧动画的动作创建
		return action;
	},
	fire1:function(){
		var frames=[];
		for (i=1;i<=2;i++){
			var str ="fire2/"+i+".png";
			var frame=cc.spriteFrameCache.getSpriteFrame(str);
			frames.push(frame);
		}
		var animation = new cc.Animation(frames,0.05);//负责动画序列
		var action = new cc.Animate(animation);//帧动画的动作创建
		return action;
	},
	fire2:function(){
		var frames=[];
		for (i=3;i<=11;i++){
			var str ="fire3/"+i+".png";
			var frame=cc.spriteFrameCache.getSpriteFrame(str);
			frames.push(frame);
		}
		var animation = new cc.Animation(frames,0.1);//负责动画序列
		var action = new cc.Animate(animation);//帧动画的动作创建
		return action;
	},
	callback:function(p){
		var action = gg.flow.flow.action;
		var func = cc.callFunc(this.actionDone, this);	
		switch(p.getTag()){	
		case TAG_BATTEN:
			var seq = cc.sequence(cc.moveTo(1,cc.p(385,-230)),cc.callFunc(function(){
				var battenblack = new cc.Sprite("#battenblack.png");
				battenblack.setPosition(100,67);
				battenblack.setOpacity(0);
				battenblack.runAction(cc.fadeIn(0.5));
				p.addChild(battenblack);

				var fire = new cc.Sprite("#fire2/1.png");
				fire.setPosition(80,70);
				fire.setOpacity(0);
				p.addChild(fire);
				var seq = cc.sequence(cc.fadeIn(0.5),cc.callFunc(function(){
					fire.runAction(this.fire1().repeatForever());
				},this));
				fire.runAction(seq);									
			},this),cc.delayTime(1),cc.callFunc(function(){
				ll.run.bottle.glass2.runAction(cc.moveBy(0.5,cc.p(-60,0)));
			},this),cc.moveTo(0.5,cc.p(210,-210)),cc.spawn(cc.moveTo(0.5,cc.p(170,-380)),cc.rotateTo(0.5,-10)),cc.callFunc(this.flowNext,this));									
			p.runAction(seq);
			break;	
		case TAG_BATTEN1:
			var seq = cc.sequence(cc.moveTo(1,cc.p(385,-230)),cc.callFunc(function(){
				var battenblack = new cc.Sprite("#battenblack.png");
				battenblack.setPosition(100,67);
				battenblack.setOpacity(0);
				battenblack.runAction(cc.fadeIn(0.5));
				p.addChild(battenblack);

				this.fire = new cc.Sprite("#fire2/1.png");
				this.fire.setPosition(80,70);
				this.fire.setOpacity(0);
				p.addChild(this.fire);
				var seq = cc.sequence(cc.fadeIn(0.5),cc.callFunc(function(){
					this.fire.runAction(this.fire1().repeatForever());
				},this));
				this.fire.runAction(seq);									
			},this),cc.delayTime(1),cc.callFunc(function(){
				ll.run.bottle.glass1.runAction(cc.moveBy(0.5,cc.p(-60,0)));
			},this),cc.moveTo(1,cc.p(-50,-210)),cc.callFunc(function(){
				var fire = new cc.Sprite("#fire3/1.png");
				fire.setPosition(100,110);					
				p.addChild(fire);
				var seq = cc.sequence(cc.delayTime(0.5),cc.callFunc(function(){
					var firebg = new cc.Sprite("#firebg.png");
					firebg.setPosition(-100,-450);
					firebg.setOpacity(0);
					firebg.setScale(0.5);
					this.addChild(firebg);
					firebg.runAction(cc.fadeIn(0.5));
					fire.runAction(this.fire2().repeatForever());
				},this));
				fire.runAction(seq);
			},this),cc.spawn(cc.moveTo(0.5,cc.p(-80,-380)),cc.rotateTo(0.5,-10)),cc.callFunc(function(){
				this.showTip = new ShowTip("把燃烧的木条放入空气瓶中无变化，" +
						"\n把燃烧的木条放入氧气瓶中燃烧更剧烈",cc.p(430,420));
			},this),cc.delayTime(5),cc.callFunc(this.flowNext,this));			

			p.runAction(seq);
			break;											
		}
	},
	flowNext:function(){
		gg.flow.next();
	},
});