exp05.Lamp05 = cc.Node.extend({
	ctor:function(p){
		this._super();
		p.addChild(this, 10, TAG_LAMP_NODE);
		this.init();
	},
	init : function(){
		this.setCascadeOpacityEnabled(true);
		var lamplid= new Button(this, 11, TAG_LAMP_LID, "#lampLid.png",this.callback);
		lamplid.setPosition(0,50);
		lamplid.setScale(0.6);
	

		this.lamp= new Button(this, 10, TAG_LAMP, "#lampBottle.png",this.callback);
		this.lamp.setPosition(0,-10);
		this.lamp.setScale(0.6);
		
	},
	fire:function(){
		var frames=[];
		for (i=1;i<=3;i++){
			var str ="fire/"+i+".png";
			var frame=cc.spriteFrameCache.getSpriteFrame(str);
			frames.push(frame);
		}
		var animation = new cc.Animation(frames,0.05);//负责动画序列
		var action = new cc.Animate(animation);//帧动画的动作创建
		return action;
	},

	callback:function(p){
		var action = gg.flow.flow.action;
		var func = cc.callFunc(this.actionDone, this);	
		switch(p.getTag()){	
		case TAG_LAMP_LID:
			if(action==ACTION_DO1){				
				var seq = cc.sequence(cc.moveBy(0.5,cc.p(0,70)),cc.moveTo(0.5,cc.p(120,40)),cc.moveTo(0.5,cc.p(150,-80)),
						cc.callFunc(this.flowNext,this));			
			}else if(action==ACTION_DO2){
				var seq = cc.sequence(cc.moveTo(1,cc.p(120,40)),cc.moveTo(1,cc.p(0,120)),cc.moveBy(0.5,cc.p(0,-70)),
						cc.callFunc(this.flowNext,this));
			}
			p.runAction(seq);
			break;	
		}
	},
	flowNext:function(){
		gg.flow.next();
	},
});