exp05.ShowMainLayer04 = cc.Layer.extend({
	lead:null,
	real:null,
	curIndex: 0,
	ctor:function () {
		this._super();
		this.init();
		return true;
	},
	init:function(){
		var sv = new pub.ScrollView(this);
		sv.setOffestPos(cc.p(152,45 + 15));

		this.addlayout1(sv);
	},	
	addlayout1:function(parent){
		this.layout1 = new ccui.Layout();//第一页
		this.layout1.setContentSize(cc.size(996, 598));//设置layout的大小
		parent.addLayer(this.layout1);

		var text = $.format("　　实验室里的氧气可以加热分解高锰酸钾(KMnO₄)或氯酸钾(KClO₃)的方法来制取，也可以用分解过氧化氢(H₂O₂)的方法来制取。", 960, 30);

		label1 = new cc.LabelTTF(text,gg.fontName,gg.fontSize4);
		label1.setAnchorPoint(0,0.5);
		label1.setPosition(0,this.layout1.height-label1.height/2);
		label1.setColor(cc.color(0, 0, 0, 250));
		this.layout1.addChild(label1);	


		this.addImage(this.layout1,"#huaxue.png",cc.p(430,330));
		this.addbutton(this.layout1, TAG_BUTTON1, "实验", cc.p(720,460));
		this.addbutton(this.layout1, TAG_BUTTON, "实验", cc.p(880,230));

		var text = $.format("　　上面化学式中的二氧化锰(MnO₂)只起改变反应速率的作用，本身的质量和化学性质在反应前后不改变。" +
				"这种物质叫做催化剂，工业上又叫触媒，在反应中起催化作用。", 960, 30);
		this.label1 = new cc.LabelTTF(text,gg.fontName,30);
		this.label1.setAnchorPoint(0,0.5);
		this.label1.setPosition(0,100);
		this.label1.setColor(cc.color(0, 0, 0, 250));
		this.layout1.addChild(this.label1);	
	},
	addImage:function(parent,str,pos,scale){
		var image = new cc.Sprite(str);
		parent.addChild(image,5);
		image.setPosition(pos);
		if(scale !=null){
			image.setScale(scale);
		}else{
			image.setScale(1);
		}
	},
	addbutton:function(parent,tag,str,pos){
		var button = new ButtonScale(parent,"#unsel.png",this.callback,this);
		button.setPosition(pos);
		button.setLocalZOrder(5);
		button.setTag(tag);
		var label = new cc.LabelTTF(str,gg.fontName,30);
		label.setPosition(button.width/2,button.height/2);		
		label.setColor(cc.color(0, 0, 0, 250));
		button.addChild(label);
		label.setTag(tag);
	},
	callback:function(p){
		switch(p.getTag()){	
		case TAG_BUTTON:			
			gg.teach_type = TAG_LEAD;
			gg.teach_flow = exp05.teach_flow04;
			pub.ch.gotoRun(TAG_EXP_05, TAG_MENU8,exp05.g_resources_run, null, new exp05.RunScene04());	
			break;
		case TAG_BUTTON1:			
			gg.teach_type = TAG_LEAD;
			gg.teach_flow = exp05.teach_flow03;
			pub.ch.gotoRun(TAG_EXP_05, TAG_MENU7,exp05.g_resources_run, null, new exp05.RunScene03());
			break;
		}
	}	
});
