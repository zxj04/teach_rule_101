exp05.startRunNext = function(tag){
	switch(tag){//具体哪个菜单执行什么操作，这里修改
	case TAG_MENU1:
		pub.ch.gotoShow(TAG_EXP_05,tag , exp05.g_resources_show, null, new exp05.ShowScene01(tag));
		break;
	case TAG_MENU2:
		gg.teach_type = TAG_LEAD;
		gg.teach_flow = exp05.teach_flow01;
		pub.ch.gotoRun(TAG_EXP_05, tag,exp05.g_resources_run, null, new exp05.RunScene01());
	//	ch.gotoShow(TAG_EXP_05,TAG_MENU2 ,g_resources_show05, null, new exp05.ShowScene02(tag));
		break;
	case TAG_MENU3:
		gg.teach_type = TAG_LEAD;
		gg.teach_flow = exp05.teach_flow02;
		pub.ch.gotoRun(TAG_EXP_05, tag,exp05.g_resources_run, null, new exp05.RunScene02());
		break;
	case TAG_MENU4:
		pub.ch.gotoShow(TAG_EXP_05, tag, exp05.g_resources_show, null, new exp05.ShowScene02(tag));
		break;
	case TAG_MENU5:
		pub.ch.gotoShow(TAG_EXP_05, tag, exp05.g_resources_show, null, new exp05.ShowScene03(tag));
		break;
	case TAG_MENU6:
		pub.ch.gotoShow(TAG_EXP_05, tag, exp05.g_resources_show, null, new exp05.ShowScene04(tag));
		break;
	case TAG_MENU7:
		gg.teach_type = TAG_LEAD;
		gg.teach_flow = exp05.teach_flow03;
		pub.ch.gotoRun(TAG_EXP_05, tag,exp05.g_resources_run, null, new exp05.RunScene03());
		break;
	case TAG_MENU8:
		gg.teach_type = TAG_LEAD;
		gg.teach_flow = exp05.teach_flow04;
		pub.ch.gotoRun(TAG_EXP_05, tag,exp05.g_resources_run, null, new exp05.RunScene04());
		break;
	case TAG_MENU9:
		pub.ch.gotoShow(TAG_EXP_05, tag, exp05.g_resources_show, null, new exp05.ShowScene05(tag));
		break;
	case TAG_MENU10:
		pub.ch.gotoShow(TAG_EXP_05, tag, exp05.g_resources_show, null, new exp05.ShowScene06(tag));
		break;
	case TAG_ABOUT:
		ch.gotoAbout(TAG_EXP_05, g_resources_public_about, null, res_public_about.about_j);
		break;
	}
}

// 任务流
exp05.teach_flow01 = 
	[
	 {
		 tip:"选择火柴",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG1,	   
		      ],
		      canClick:true		 
	 },
	 {
		 tip:"选择通气装置",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG2,	   
		      ],
		      canClick:true		 
	 },
	 {
		 tip:"选择玻璃片",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG3,	   
		      ],
		      canClick:true		 
	 },
	 {
		 tip:"选择注射器",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG5,	   
		      ],
		      canClick:true		 
	 },
	 {
		 tip:"选择酒精灯",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG8,	   
		      ],
		      canClick:true		 
	 },
	 {
		 tip:"选择集气瓶",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG7,	   
		      ],
		      canClick:true		 
	 },

	 {
		 tip:"点击确定，开始实验",
		 tag:[
		      TAG_LIB,
		      TAG_SURE
		      ]
	 },
	 {
		 tip:"用注射器往锥形瓶通入空气，观察现象",
		 tag:[
		      TAG_TONGQI_NODE,
		      TAG_INOCULATOR2
		      ]
	 },
	 {
		 tip:"取下酒精灯灯帽",
		 tag:[
		      TAG_LAMP_NODE,
		      TAG_LAMP_LID],action:ACTION_DO1

	 },
	 {
		 tip:"用火柴点燃酒精灯",
		 tag:[
		      TAG_MATCH_NODE,
		      TAG_MATCH
		      ]
	 },
	 {
		 tip:"取一根木条，用酒精灯上点燃，放入空气中",
		 tag:[
		      TAG_BATTEN_NODE,
		      TAG_BATTEN
		      ]
	 },
	 {
		 tip:"再取一根木条，用酒精灯上点燃，放入氧气中",
		 tag:[
		      TAG_BATTEN_NODE,
		      TAG_BATTEN1
		      ]
	 },
	 {
		 tip:"恭喜过关",
		 over:true

	 }
	 ]
//任务流
exp05.teach_flow02 = 
	[
	 {
		 tip:"选择燃烧匙",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG1,	   
		      ],
		      canClick:true		 
	 },
	 {
		 tip:"选择火柴",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG3,	   
		      ],
		      canClick:true		 
	 },
	 {
		 tip:"选择集气瓶",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG4,	   
		      ],
		      canClick:true		 
	 },
	 {
		 tip:"选择酒精灯",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG9,	   
		      ],
		      canClick:true		 
	 },
	 {
		 tip:"选择烧杯",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG8,	   
		      ],
		      canClick:true		 
	 },
	 {
		 tip:"选择红磷",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG6,	   
		      ],
		      canClick:true		 
	 },

	 {
		 tip:"点击确定，开始实验",
		 tag:[
		      TAG_LIB,
		      TAG_SURE
		      ]
	 },
	 {
		 tip:"拿出集气瓶上的木塞",
		 tag:[TAG_BOTTLE_NODE
		      ,TAG_MUSAI_NODE,TAG_MUSAI],action:ACTION_DO1
	 },
	 {
		 tip:"用烧杯往集气瓶里加入少量水",
		 tag:[
		      TAG_BEAKER_NODE,
		      TAG_BEAKER
		      ]
	 },
	 {
		 tip:"将集气瓶剩余容积分成五等分，做上标记",
		 tag:[
		      TAG_BOTTLE_NODE,
		      TAG_BOTTLE
		      ]
	 },
	 {
		 tip:"打开红磷样品盖子",
		 tag:[
		      TAG_PBOTTLE_NODE,
		      TAG_LID],action:ACTION_DO1
	 },
	 {
		 tip:"用药勺取足量的红磷，放到燃烧匙中",
		 tag:[
		      TAG_PBOTTLE_NODE,
		      TAG_SPOON
		      ]
	 },
	 {
		 tip:"取下酒精灯灯冒",
		 tag:[
		      TAG_LAMP_NODE,
		      TAG_LAMP_LID],action:ACTION_DO1
	 },
	 {
		 tip:"用火柴点燃酒精灯",
		 tag:[
		      TAG_MATCH_NODE,
		      TAG_MATCH
		      ]
	 },
	 {
		 tip:"将红磷放到酒精灯上点燃，快速放入装有水的集气瓶中",
		 tag:[
		      TAG_BOTTLE_NODE,
		      TAG_MUSAI_NODE,
		      TAG_MUSAI],action:ACTION_DO2
	 },
	 {
		 tip:"熄灭酒精灯,等待集气瓶至冷却",
		 tag:[
		      TAG_LAMP_NODE,
		      TAG_LAMP_LID],action:ACTION_DO2
	 },
	 {
		 tip:"打开导管夹，观察现象",
		 tag:[
		      TAG_BOTTLE_NODE,
		      TAG_MUSAI_NODE,
		      TAG_JIAZI
		      ]
	 },
	 {
		 tip:"恭喜过关",
		 over:true

	 }
	 ]


//任务流
exp05.teach_flow03 = 
	[
	 {
		 tip:"选择过氧化氢",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG1,	   
		      ],
		      canClick:true		 
	 },
	 {
		 tip:"选择二氧化锰",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG4,	   
		      ],
		      canClick:true		 
	 },
	 {
		 tip:"选择集气瓶",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG5,	   
		      ],
		      canClick:true		 
	 },
	 {
		 tip:"选择烧瓶",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG10,	   
		      ],
		      canClick:true		 
	 },
	 {
		 tip:"选择水槽",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG8,	   
		      ],
		      canClick:true		 
	 },
	 {
		 tip:"选择分液漏斗",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG7,	   
		      ],
		      canClick:true		 
	 },

	 {
		 tip:"点击确定，开始实验",
		 tag:[
		      TAG_LIB,
		      TAG_SURE
		      ]
	 },
	 {
		 tip:"检查发生装置气密性",
		 tag:[TAG_IRON_NODE,
		      TAG_FLASK_NODE,
		      TAG_FLASK],action :ACTION_DO1
	 },
	 {
		 tip:"将发生装置拿出",
		 tag:[TAG_IRON_NODE,
		      TAG_FLASK_NODE,
		      TAG_FLASK],action :ACTION_DO2
	 },
	 {
		 tip:"取下二氧化锰样品盖子",
		 tag:[TAG_MNO2_NODE,
		      TAG_MNO2LID]
	 },
	 {
		 tip:"用药匙取足量二氧化锰",
		 tag:[TAG_MNO2_NODE,
		      TAG_MNSPOON]
	 },
	 {
		 tip:"将烧瓶装到铁架台上",
		 tag:[
		      TAG_IRON_NODE,
		      TAG_FLASK_NODE,
		      TAG_FLASK
		      ],action :ACTION_DO3
	 },
	 {
		 tip:"将水槽移到铁架台处",
		 tag:[
		      TAG_IRON_NODE,
		      TAG_FLUME
		      ]
	 },
//	 {
//		 tip:"将导管装到烧瓶上",
//		 tag:[
//		      TAG_IRON_NODE,
//		      // TAG_FLASK_NODE,
//		      TAG_MUSAI
//		      ]
//	 },
	 {
		 tip:"将集气瓶装满水",
		 tag:[
		      TAG_IRON_NODE,
		      TAG_BOTTLE
		      ],action:ACTION_DO1
	 },
	 {
		 tip:"用玻璃片盖住集气瓶口，并将集气瓶倒扣在水槽中",
		 tag:[
		      TAG_IRON_NODE,
		      TAG_GLASS
		      ],action:ACTION_DO1
	 },
	 {
		 tip:"取下过氧化氢试剂瓶盖子",
		 tag:[TAG_H2O2_NODE,
		      TAG_H2O2LID]
	 },
	 {
		 tip:"在分液漏斗中倒入足量的过氧化氢",
		 tag:[TAG_H2O2_NODE,
		      TAG_H2O2BOTTLE]
	 },
//	 {
//		 tip:"将分液漏斗装到铁架台上",
//		 tag:[
//		      TAG_IRON_NODE,
//		      TAG_FENYE_NODE,
//		      TAG_FENYE
//		      ]
//	 },
	 {
		 tip:"打开分液漏斗活塞",
		 tag:[
		      TAG_IRON_NODE,
		      TAG_FENYE_NODE,
		      TAG_FENYELID
		      ],action:ACTION_DO1
	 },
	 {
		 tip:"关闭分液漏斗活塞",
		 tag:[
		      TAG_IRON_NODE,
		      TAG_FENYE_NODE,
		      TAG_FENYELID
		      ],action:ACTION_DO2
	 },
	 {
		 tip:"待气泡有规则的排出时，将集气瓶扣在导管口",
		 tag:[
		      TAG_IRON_NODE,
		      TAG_BOTTLE
		      ],action:ACTION_DO2
	 },
	 {
		 tip:"用玻璃片盖住集气瓶口，并将集气瓶从水槽中取出",
		 tag:[
		      TAG_IRON_NODE,
		      TAG_GLASS
		      ],action:ACTION_DO2
	 },
	 {
		 tip:"将带火星的木条放到集气瓶中，观察现象",
		 tag:[
		      TAG_BATTEN_NODE,
		      TAG_BATTEN
		      ]
	 },
	 {
		 tip:"恭喜过关",
		 over:true

	 }
	 ]


//任务流
exp05.teach_flow04 = 
	[
	 {
		 tip:"选择水槽",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG1,	   
		      ],
		      canClick:true		 
	 },
	 {
		 tip:"选择烧杯",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG3,	   
		      ],
		      canClick:true		 
	 },
	 {
		 tip:"选择集气瓶",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG4,	   
		      ],
		      canClick:true		 
	 },
	 {
		 tip:"选择铁架台",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG10,	   
		      ],
		      canClick:true		 
	 },
	 {
		 tip:"选择酒精灯",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG9,	   
		      ],
		      canClick:true		 
	 },
	 {
		 tip:"选择试管",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG7,	   
		      ],
		      canClick:true		 
	 },
	 {
		 tip:"选择高锰酸钾",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG6,	   
		      ],
		      canClick:true		 
	 },

	 {
		 tip:"点击确定，开始实验",
		 tag:[
		      TAG_LIB,
		      TAG_SURE
		      ]
	 },
	 {
		 tip:"双手捂住试管，检查试管气密性",
		 tag:[TAG_IRON_NODE,
		      TAG_SG_NODE,
		      TAG_SG],action:ACTION_DO1
	 },
	 {
		 tip:"取出试管，拔下导管",
		 tag:[TAG_IRON_NODE,
		      TAG_SG_NODE,
		      TAG_SG],action:ACTION_DO2
	 },
	 {
		 tip:"取出高锰酸钾样品盖子",
		 tag:[TAG_K2MNO4_NODE,
		      TAG_K2MNO4LID]
	 },
	 {
		 tip:"用药匙取适量高锰酸钾放到纸槽中",
		 tag:[TAG_K2MNO4_NODE,
		      TAG_KMNSPOON]
	 },
	 {
		 tip:"将纸槽中的高锰酸钾放到试管中",
		 tag:[TAG_K2MNO4_NODE,
		      TAG_PAPER]
	 },
	 {
		 tip:"将棉花放到试管口",
		 tag:[TAG_IRON_NODE,
		      TAG_MIANHUA]
	 },
	 {
		 tip:"将导管装到试管上",
		 tag:[TAG_IRON_NODE,
		      TAG_SG_NODE,
		      TAG_DAOGUAN],action:ACTION_DO1
	 },

	 {
		 tip:"将试管上装到铁架台上",
		 tag:[TAG_IRON_NODE,
		      TAG_SG_NODE,
		      TAG_SG],action:ACTION_DO3
	 },	
	 {
		 tip:"将水槽移到铁架台处",
		 tag:[TAG_IRON_NODE,
		      TAG_FLUME]
	 },
	 {
		 tip:"将集气瓶装满水",
		 tag:[
		      TAG_IRON_NODE,
		      TAG_BOTTLE
		      ],action:ACTION_DO1
	 },
	 {
		 tip:"用玻璃片盖住集气瓶口，并将集气瓶倒扣在水槽中",
		 tag:[
		      TAG_IRON_NODE,
		      TAG_GLASS
		      ],action:ACTION_DO1
	 },
	 {
		 tip:"取下酒精灯帽",
		 tag:[
		      TAG_LAMP_NODE,
		      TAG_LAMP_LID
		      ],action:ACTION_DO1
	 },
	 {
		 tip:"用火柴点燃酒精灯",
		 tag:[
		      TAG_LAMP_NODE,
		      TAG_MATCH
		      ]
	 },
	 {
		 tip:"用酒精灯在试管底部来回移动，使试管受热均匀",
		 tag:[
		      TAG_LAMP_NODE,
		      TAG_LAMP
		      ],action:ACTION_DO1
	 },
	 {
		 tip:"待气泡有规则的排出时，将集气瓶扣在导管口",
		 tag:[
		      TAG_IRON_NODE,
		      TAG_BOTTLE
		      ],action:ACTION_DO2
	 },

	 {
		 tip:"用玻璃片盖住集气瓶口，并将集气瓶从水槽中取出",
		 tag:[
		      TAG_IRON_NODE,
		      TAG_GLASS
		      ],action:ACTION_DO2
	 },
	 {
		 tip:"观察试管现象",
		 tag:[TAG_IRON_NODE,
		      TAG_SG_NODE,
		      TAG_SG],action:ACTION_DO4
	 },
	 {
		 tip:"取出导管",
		 tag:[
		      TAG_IRON_NODE,
		      TAG_SG_NODE,
		      TAG_DAOGUAN
		      ],action:ACTION_DO2
	 },
	 {
		 tip:"取下酒精灯",
		 tag:[
		      TAG_LAMP_NODE,
		      TAG_LAMP
		      ],action:ACTION_DO2
	 },
	 {
		 tip:"将集气瓶移到中间",
		 tag:[
		      TAG_IRON_NODE,
		      TAG_BOTTLE
		      ],action:ACTION_DO3
	 },
	 {
		 tip:"将夹有木炭的坩埚夹，放到酒精灯上燃烧至发红，放入集气瓶中",
		 tag:[
		      TAG_GANGUOJIA_NODE,
		      TAG_GANGUOJIA1
		      ],action:ACTION_DO1
	 },

	 {
		 tip:"将石灰水倒入集气瓶中",
		 tag:[
		      TAG_SHIHUIBEAKER_NODE,
		      TAG_SHIHUIBEAKER
		      ]
	 },
	 {
		 tip:"将带火柴的铝箔加热，放到集气瓶中，观察现象",
		 tag:[
		      TAG_LV_NODE,
		      TAG_LV
		      ]
	 },
	 {
		 tip:"熄灭酒精灯",
		 tag:[
		      TAG_LAMP_NODE,
		      TAG_LAMP_LID
		      ],action:ACTION_DO2
	 },
	 {
		 tip:"恭喜过关",
		 over:true

	 }
	 ]