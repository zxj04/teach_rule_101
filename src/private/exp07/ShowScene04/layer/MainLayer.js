exp07.ShowMainLayer04 = cc.Layer.extend({
	lead:null,
	real:null,
	curIndex: 0,
	ctor:function () {
		this._super();
		this.init();
		return true;
	},
	init:function(){
		var sv = new pub.ScrollView(this);
		sv.setOffestPos(cc.p(152,45 + 15));

		this.addlayout1(sv);
		this.addlayout2(sv);
	},	
	addlayout1:function(parent){
		this.layout1 = new ccui.Layout();//第一页
		this.layout1.setContentSize(cc.size(996, 598));//设置layout的大小
		parent.addLayer(this.layout1);

		var text = $.format("　　用盐酸用原料制取二氧化碳时，制得的气体中常混有的杂质是（ ）", 860, 30);

		label1 = new cc.LabelTTF(text,gg.fontName,gg.fontSize4);
		label1.setAnchorPoint(0,0.5);
		label1.setPosition(0,this.layout1.height-label1.height/2);
		label1.setColor(cc.color(0, 0, 0, 250));
		this.layout1.addChild(label1);	

		this.addSelImage(this.layout1, "#select.png", TAG_BUTTON, cc.p(60,480 ),
				"A.氮气和稀有气体","#wrong.png",TAG_SEL);

		this.addSelImage(this.layout1, "#select.png", TAG_BUTTON1, cc.p(60,430),
				"B.氮气和氯化氢","#wrong.png",TAG_SEL1);

		this.addSelImage(this.layout1, "#select.png", TAG_BUTTON2, cc.p(60,380),
				"C.水蒸气和氯化氢","#right.png",TAG_SEL2);

		this.addSelImage(this.layout1, "#select.png", TAG_BUTTON3, cc.p(60,330), 
				"D.空气和水蒸气","#wrong.png",TAG_SEL3);
		var text = $.format("　　解析：因对实验室制取CO₂的反应原理理解不透，对生成的CO₂气体混有什么杂质就不" +
				"能正确判断。制取CO₂所用的药品是石灰石（或大理石）与稀盐酸，两者混合，产生的CO₂从溶液中逸出，"+
				"因此含有水蒸气；又因盐酸具有挥发性，挥发出HCl气体，因此制取的气体中还有氯化氢气体。正确答案是C", 920, 25);

		this.label1 = new cc.LabelTTF(text,gg.fontName,25);
		this.label1.setAnchorPoint(0,0.5);
		this.label1.setPosition(0,180);
		this.label1.setColor(cc.color(0, 0, 0, 250));
		this.label1.setVisible(false);
		this.layout1.addChild(this.label1);	
	},
	addlayout2:function(parent){
		this.layout2 = new ccui.Layout();//第四页
		this.layout2.setContentSize(cc.size(996, 598));
		parent.addLayer(this.layout2);

		var text = $.format("　　下列说法正确的是（）", 960, 30);
		label1 = new cc.LabelTTF(text ,gg.fontName,gg.fontSize4);
		label1.setAnchorPoint(0,0.5);
		label1.setPosition(0,this.layout2.height-label1.height/2);
		label1.setColor(cc.color(0, 0, 0, 250));
		this.layout2.addChild(label1);	

		this.addSelImage(this.layout2, "#select.png", TAG_BUTTON4, cc.p(60,480 ),
				"A.二氧化碳气体能使紫色石蕊试液变红","#wrong.png",TAG_SEL4);

		this.addSelImage(this.layout2, "#select.png", TAG_BUTTON5, cc.p(60,430 ),
				"B.能使燃着的木条熄灭的气体一定是CO₂气体","#wrong.png",TAG_SEL5);

		this.addSelImage(this.layout2, "#select.png", TAG_BUTTON6, cc.p(60,380 ),
				"C.充满二氧化碳的集气瓶，盖好玻璃片后应该正放在桌面上","#right.png",TAG_SEL6);

		this.addSelImage(this.layout2, "#select.png", TAG_BUTTON7, cc.p(60,330 ), 
				"D.干冰就是二氧化碳","#wrong.png",TAG_SEL7);
		var text = $.format("　　分析：错选A是因为没了解使石蕊试液变色的本质，真正使其变色的是二氧化碳和水反应产生的碳酸；因为二氧化碳气体既不能燃烧也不支持燃烧" +
				"故认为燃着的木条熄灭的气体一定是二氧化碳气体而错选B；错选D是因为只想到干冰就是二氧化碳而没说是固体的二氧化碳。所以正确答案是C" , 920, 25);

		this.label2 = new cc.LabelTTF(text,gg.fontName,25);
		this.label2.setAnchorPoint(0,0.5);
		this.label2.setPosition(0,180);
		this.label2.setColor(cc.color(0, 0, 0, 250));
		this.label2.setVisible(false);
		this.layout2.addChild(this.label2);				
	},
	addSelImage:function(parent,str,tag,pos,str2,str3,tag2){
		var image = new ButtonScale(parent,str,this.callback,this);
		image.setPosition(pos);
		image.setTag(tag);

		var sel = new cc.Sprite(str3);
		sel.setPosition(15,15);
		image.addChild(sel);
		sel.setVisible(false);
		sel.setTag(tag2);

		var label = new cc.LabelTTF(str2,gg.fontName,gg.fontSize);
		label.setColor(cc.color(0, 0, 0, 250));
		label.setAnchorPoint(0,0.5);
		parent.addChild(label);
		label.setPosition(image.x + 20,image.y);
	},
	callback:function(p){
		switch(p.getTag()){	
		case TAG_BUTTON:
		case TAG_BUTTON1:
		case TAG_BUTTON2:
		case TAG_BUTTON3:
			this.layout1.getChildByTag(TAG_BUTTON).getChildByTag(TAG_SEL).setVisible(true);
			this.layout1.getChildByTag(TAG_BUTTON1).getChildByTag(TAG_SEL1).setVisible(true);
			this.layout1.getChildByTag(TAG_BUTTON2).getChildByTag(TAG_SEL2).setVisible(true);
			this.layout1.getChildByTag(TAG_BUTTON3).getChildByTag(TAG_SEL3).setVisible(true);
			this.label1.setVisible(true);	
			break;

		case TAG_BUTTON4:
		case TAG_BUTTON5:
		case TAG_BUTTON6:
		case TAG_BUTTON7:
			this.layout2.getChildByTag(TAG_BUTTON4).getChildByTag(TAG_SEL4).setVisible(true);
			this.layout2.getChildByTag(TAG_BUTTON5).getChildByTag(TAG_SEL5).setVisible(true);
			this.layout2.getChildByTag(TAG_BUTTON6).getChildByTag(TAG_SEL6).setVisible(true);
			this.layout2.getChildByTag(TAG_BUTTON7).getChildByTag(TAG_SEL7).setVisible(true);
			this.label2.setVisible(true);
			break;		

		}
	}	
});
