var exp07 = exp07||{};
//start
exp07.res_start = {
	//	start_png :"res/private/exp07/start/startpng.png"
};

exp07.g_resources_start = [];
for (var i in exp07.res_start) {
	exp07.g_resources_start.push(exp07.res_start[i]);
}

//run
exp07.res_run = {
		run_p : "res/private/exp07/run.plist",
		run_g : "res/private/exp07/run.png",
		run_p2 : "res/private/exp07/run2.plist",
		run_g2 : "res/private/exp07/run2.png",
		run_musai:"res/private/exp07/musai.png"
};

exp07.g_resources_run = [];
for (var i in exp07.res_run) {
	exp07.g_resources_run.push(exp07.res_run[i]);
}

//show02
exp07.res_show = {
		show_p : "res/private/exp07/show.plist",
		show_g : "res/private/exp07/show.png"
};

exp07.g_resources_show = [];
for (var i in exp07.res_show) {
	exp07.g_resources_show.push(exp07.res_show[i]);
}


