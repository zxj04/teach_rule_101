exp07.startRunNext = function(tag){
	switch(tag){//具体哪个菜单执行什么操作，这里修改
	case TAG_MENU1:
		pub.ch.gotoShow(TAG_EXP_07,tag , exp07.g_resources_show, null, new exp07.ShowScene01(tag));
		break;
	case TAG_MENU2:

		pub.ch.gotoShow(TAG_EXP_07,tag , exp07.g_resources_show, null, new exp07.ShowScene02(tag));
		break;
	case TAG_MENU3:
		gg.teach_type = TAG_LEAD;
		gg.teach_flow = exp07.teach_flow01;
		pub.ch.gotoRun(TAG_EXP_07, tag,exp07.g_resources_run, null, new exp07.RunScene01());
		break;
	case TAG_MENU4:
		pub.ch.gotoShow(TAG_EXP_07,tag , exp07.g_resources_show, null, new exp07.ShowScene03(tag));
		break;
	case TAG_MENU5:
		gg.teach_type = TAG_LEAD;
		gg.teach_flow = exp07.teach_flow02;
		pub.ch.gotoRun(TAG_EXP_07, tag,exp07.g_resources_run, null, new exp07.RunScene02());
		break;
	case TAG_MENU6:
		pub.ch.gotoShow(TAG_EXP_07,tag , exp07.g_resources_show, null, new exp07.ShowScene04(tag));
		break;
	case TAG_ABOUT:
		ch.gotoAbout(TAG_EXP_07, g_resources_public_about, null, res_public_about.about_j);
		break;
	}
}

// 任务流
exp07.teach_flow01 =
	[
	 {
		 tip:"选择集气瓶",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG3,	   
		      ],
		      canClick:true		 
	 },
	 {
		 tip:"选择烧杯",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG4,	   
		      ],
		      canClick:true		 
	 },
	 {
		 tip:"选择长颈漏斗",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG10,	   
		      ],
		      canClick:true		 
	 },
	 {
		 tip:"选择稀盐酸",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG7,	   
		      ],
		      canClick:true		 
	 },
	 {
		 tip:"选择试管",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG6,	   
		      ],
		      canClick:true		 
	 },
	 {
		 tip:"点击确定，开始实验",
		 tag:[
		      TAG_LIB,
		      TAG_SURE
		      ]
	 },
	 {
		 tip:"将长颈漏斗插入锥形瓶中",
		 tag:[
		      TAG_BODY_NODE,
		      TAG_LOUDOU
		      ]
		  
	 },
	 {
		 tip:"通过长颈漏斗加入适量的稀盐酸",
		 tag:[
		      TAG_BODY_NODE,
		      TAG_HCL
		      ]

	 },
	 {
		 tip:"片刻后把燃着的火柴放到集气瓶口上，验证CO₂是否收集满",
		 tag:[
		      TAG_BODY_NODE,
		      TAG_MATCH
		      ]

	 },
	 {
		 tip:"取出集气瓶，盖上毛玻璃",
		 tag:[
		      TAG_BODY_NODE,
		      TAG_BOTTLE
		      ],action:ACTION_DO1

	 },
	 {
		 tip:"将澄清石灰水套在导气管口，观察现象",
		 tag:[
		      TAG_BODY_NODE,
		      TAG_SG
		      ]

	 },
	 {
		 tip:"将滴有紫色石蕊试液的试管套在导气管口，观察现象",
		 tag:[
		      TAG_BODY_NODE,
		      TAG_SG1
		      ]

	 },
	 {
		 tip:"向点燃蜡烛的烧杯中，倾倒二氧化碳，观察现象",
		 tag:[
		      TAG_BODY_NODE,
		      TAG_BOTTLE
		      ],action:ACTION_DO2

	 },
	 {
		 tip:"恭喜过关",
		 over:true
		
	 }
	 ]
exp07.teach_flow02 = 
	[
	 {
		 tip:"选择浓盐酸",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG1,	   
		      ],
		      canClick:true		 
	 },
	 {
		 tip:"选择三角烧瓶",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG3,	   
		      ],
		      canClick:true		 
	 },
	 {
		 tip:"选择洗涤剂",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG5,	   
		      ],
		      canClick:true		 
	 },
	 {
		 tip:"选择饱和碳酸钠",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG9,	   
		      ],
		      canClick:true		 
	 },
	 {
		 tip:"选择试管",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG7,	   
		      ],
		      canClick:true		 
	 },


	 {
		 tip:"点击确定，开始实验",
		 tag:[
		      TAG_LIB,
		      TAG_SURE
		      ]
	 },
	 {
		 tip:"取下三角烧瓶的橡皮塞",
		 tag:[
		      TAG_CONFLASK_NODE,
		      TAG_XCONFLASK,
		      TAG_LID
		      ],action:ACTION_DO1
	 },
	 {
		 tip:"往三角烧瓶中倒入适量饱和碳酸钠溶液",
		 tag:[
		      TAG_BEAKER_NODE1,
		      TAG_BEAKER
		      ]
	 },
	 {
		 tip:"往三角烧瓶中滴加少量洗涤剂",
		 tag:[
		      TAG_XIDIJI_NODE,
		      TAG_DROP
		      ]
	 },
	 {
		 tip:"往试管中倒入适量的浓盐酸",
		 tag:[
		      TAG_BEAKER_NODE,
		      TAG_BEAKER
		      ]
	 },
	 {
		 tip:"将试管放到三角烧瓶中",
		 tag:[
		      TAG_CONFLASK_NODE,
		      TAG_XCONFLASK,
		      TAG_SG
		      ]
	 },
	 {
		 tip:"盖上三角烧瓶的橡皮塞",
		 tag:[
		      TAG_CONFLASK_NODE,
		      TAG_XCONFLASK,
		      TAG_LID
		      ],action:ACTION_DO2
	 },
	 {
		 tip:"将三角烧瓶翻转过来，观察现象",
		 tag:[
		      TAG_CONFLASK_NODE,
		      TAG_XCONFLASK,
		      ]
	 },

	 {
		 tip:"恭喜过关",
		 over:true

	 }
	 ]