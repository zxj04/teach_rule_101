exp07.Body = cc.Node.extend({
	ctor:function(p){
		this._super();
		p.addChild(this, 10, TAG_BODY_NODE);
		this.init();
	},
	init : function(){
		this.setCascadeOpacityEnabled(true);
		this.flask= new Button(this, 1, TAG_FLASK, "#flask.png",this.callback);
		this.flask.setScale(0.8);

		this.flask2= new Button(this, 11, TAG_FLASK2, "#flask2.png",this.callback);
		this.flask2.setScale(0.8);
		
		this.flaskline= new cc.Sprite("#water1.png");
		this.flaskline.setPosition(110,40);
		this.flask.addChild(this.flaskline,10);
		this.flaskline.setScale(1.2);
		
		this.fanying= new cc.Sprite("#fanying.png");
		this.fanying.setPosition(110,55);
		this.flask2.addChild(this.fanying,10);
		this.fanying.setOpacity(0);
				
		var musai = new cc.SpriteFrame(exp07.res_run.run_musai,cc.rect(0,15,81,56));
		this.musai1= new cc.Sprite(musai);
		this.musai1.setPosition(0,110);
		this.addChild(this.musai1,10);
		this.musai1.setScale(0.8);
		
		this.musai2= new Button(this, 2, TAG_MUSAI1, "#musai.png",this.callback);
		this.musai2.setScale(0.8);
		this.musai2.setPosition(cc.p(0,115));
		
		this.daoguan= new Button(this,5, TAG_DAOGUAN, "#daoguan.png",this.callback);
		this.daoguan.setScale(0.8);
		this.daoguan.setPosition(cc.p(120,20));
		
//		this.jiazi= new Button(this, 6, TAG_MUSAI, "#jiazi1.png",this.callback);
//		this.jiazi.setScale(0.8);
//		this.jiazi.setPosition(cc.p(120,150));
//		
//		this.jiazi2= new Button(this, 4, TAG_MUSAI1, "#jiazi2.png",this.callback);
//		this.jiazi2.setScale(0.8);
//		this.jiazi2.setPosition(cc.p(120,150));
		
		this.bottle= new Button(this, 7, TAG_BOTTLE, "#bottle.png",this.callback);
		this.bottle.setScale(0.8);
		this.bottle.setPosition(cc.p(195,-60));
		
		this.glass= new Button(this.bottle, 4, TAG_GLASS, "#glass.png",this.callback,this);
		//this.glass.setScale(0.8);
		this.glass.setPosition(cc.p(10,230));
					
		this.changjing= new Button(this, 5, TAG_LOUDOU, "#loudou.png",this.callback);
		this.changjing.setScale(0.8);
		this.changjing.setPosition(cc.p(-250,-80));
		this.changjing.setRotation(-90);
		
		this.cline= new cc.Sprite("#water.png");
		this.cline.setPosition(45,290);
		this.changjing.addChild(this.cline,10);
		this.cline.setScale(0.5);
		this.cline.setVisible(false);
		
		this.hcl= new Button(this, 5, TAG_HCL, "#hcl/action1.png",this.callback);
		this.hcl.setScale(0.8);
		this.hcl.setPosition(cc.p(-250,300));
		
		this.lid = new cc.Sprite("#lid1.png");
		this.lid.setPosition(cc.p(-250,370));
        this.addChild(this.lid,4);
        this.lid.setScale(0.7);
        
        this.hline= new cc.Sprite("#water.png");
        this.hline.setPosition(156,100);
        this.hcl.addChild(this.hline,10);
        this.hline.setScale(1.2);

        
        this.dalishi = new cc.Sprite("#dalishi.png");
        this.dalishi.setPosition(cc.p(0,-100));
        this.addChild(this.dalishi,4);
        this.dalishi.setScale(0.7);
        
        var sg = new  Button(this, 5, TAG_SG, "#sg.png",this.callback);
        sg.setScale(0.7);
        sg.setPosition(cc.p(300,300));
        
        this.sline = new cc.Sprite("#water.png");
        this.sline.setPosition(cc.p(25,137));
        sg.addChild(this.sline,4);
        this.sline.setScale(0.5);
        
        this.hunzhuo = new cc.Sprite("#hunzhuo.png");
        this.hunzhuo.setPosition(cc.p(25,77));
        sg.addChild(this.hunzhuo,4);
        this.hunzhuo.setOpacity(0);
        
        var sg1 = new  Button(this, 5, TAG_SG1, "#sg.png",this.callback);
        sg1.setScale(0.7);
        sg1.setPosition(cc.p(400,300));

        this.zise = new cc.Sprite("#zise.png");
        this.zise.setPosition(cc.p(25,77));
        sg1.addChild(this.zise,4);
        
        this.red = new cc.Sprite("#red.png");
        this.red.setPosition(cc.p(25,77));
        sg1.addChild(this.red,4);
        this.red.setOpacity(0);

        this.match = new  Button(this, 5, TAG_MATCH, "#match/1.png",this.callback);
        this.match.setPosition(cc.p(400,77));
        this.match.setVisible(false);
              
        this.beaker = new cc.Sprite("#beaker.png");
        this.beaker.setPosition(cc.p(-200,0));
        this.addChild(this.beaker,1);
        this.beaker.setScale(0.6);
        this.beaker.setVisible(false);
        
        this.beaker1 = new cc.Sprite("#beaker1.png");
        this.beaker1.setPosition(cc.p(150,205));
        this.beaker.addChild(this.beaker1,10);
        
        this.jiazi = new cc.Sprite("#jiazi.png");
        this.jiazi.setPosition(cc.p(75,215));
        this.beaker.addChild(this.jiazi,2);
        
        this.lazhu = new cc.Sprite("#lazhu.png");
        this.lazhu.setPosition(cc.p(125,115));
        this.beaker.addChild(this.lazhu,3);
        
        this.fire = new cc.Sprite("#fire/1.png");
        this.fire.setPosition(cc.p(125,192));
        this.beaker.addChild(this.fire,4);
        this.fire.setScale(1.6);	
        this.fire.runAction(this.addAction("fire/", 3, 0.05).repeatForever());
	},
	addpaomo :function(posx){
		var paomo = new cc.Sprite("#paomo.png");
		paomo.setPosition(posx,208);
		paomo.setOpacity(0);
		this.conflask1.addChild(paomo,10);
		var seq = cc.sequence(cc.delayTime(0.5),cc.fadeIn(0.5));
		paomo.runAction(seq);


		var paomo1 = new cc.Sprite("#paomo1.png");
		paomo1.setPosition(350,25);
		paomo1.setAnchorPoint(0, 0.5);
		paomo1.setScale(0);
		this.conflask.addChild(paomo1,10);
		var se = cc.sequence(cc.delayTime(0.5),cc.scaleTo(1,1));
		paomo1.runAction(se);

	},
	addAction:function(str1,num,time){
		var frames = [];
		for(var i =1 ;i<=num;i++){
			var str = str1+i+".png";
			var frame=cc.spriteFrameCache.getSpriteFrame(str);
			frames.push(frame);		
		}
		var animation = new cc.Animation(frames,time);//负责动画序列
		var action = new cc.Animate(animation);//帧动画的动作创建
		return action;
	},
	callback:function(p){
		var action = gg.flow.flow.action;
		var func = cc.callFunc(this.actionDone, this);	
		switch(p.getTag()){	
		case TAG_LOUDOU:
			var spawn = cc.spawn(cc.rotateTo(1,0),cc.moveTo(1,cc.p(-180,20)));
			var move = cc.moveTo(1,cc.p(0,300));
			var move1 = cc.moveTo(1,cc.p(0,100));
			var seq = cc.sequence(spawn,move,move1,cc.callFunc(this.flowNext,this));
			p.runAction(seq);
		break;
		case TAG_HCL:
			this.lid.runAction(cc.sequence(cc.moveBy(0.5,cc.p(0,50)),cc.spawn(cc.moveBy(1,cc.p(80,-170)),
					cc.rotateTo(1,180)),cc.callFunc(function(){
						this.lid.setSpriteFrame("lid2.png");
						this.lid.setRotation(0);
			},this)));
			var move = cc.moveTo(1.5,cc.p(100,300));
			var action = this.addAction("hcl/action",5,0.2);
			var spawn = cc.spawn(cc.moveTo(1,cc.p(80,280)),cc.rotateTo(1,-90),action);
			var seq = cc.sequence(cc.delayTime(1.5),move,spawn,cc.callFunc(function(){
				this.flaskline.runAction(cc.spawn(cc.moveTo(1,cc.p(110,100)),cc.scaleTo(1,0.95)));			
			},this),cc.delayTime(0.5),cc.callFunc(function(){
				this.fanying.runAction(cc.fadeIn(2));
			},this),cc.delayTime(2),cc.callFunc(function(){
				this.hcl.setVisible(false);
				this.hline.setVisible(false);
				this.lid.setVisible(false);
				this.match.setVisible(true);
				this.flowNext();
			},this));
			p.runAction(seq);
			
			var spawn1 = cc.spawn(cc.moveTo(1,cc.p(156,130)),cc.rotateTo(1,90),cc.scaleTo(1,1.7));
			var seq1 = cc.sequence(cc.delayTime(3),spawn1);
			this.hline.runAction(seq1);
			
			var seq2 = cc.sequence(cc.delayTime(5),cc.callFunc(function(){
				this.cline.setVisible(true);
			},this),cc.spawn(cc.moveTo(1,cc.p(45,310)),cc.scaleTo(1,0.8)));
			this.cline.runAction(seq2);
		break;
		case TAG_MATCH:
			var move = cc.moveTo(1,cc.p(270,60));
			var action = this.addAction("match/", 3, 0.2);
			var seq = cc.sequence(move,action,cc.callFunc(function(){
				this.flowNext();
				this.showtip = new ShowTip("火柴熄灭，CO₂收集完毕",cc.p(1000,280));
			},this),cc.delayTime(0.5),cc.callFunc(function(){
				p.setVisible(false);
			},this));
			p.runAction(seq);
		break;
		case TAG_BOTTLE:
			if(action ==ACTION_DO1){
				var move = cc.moveBy(1,cc.p(0,50));
				this.daoguan.runAction(move);
				this.flask.runAction(move.clone());
				this.flask2.runAction(move.clone());
				this.changjing.runAction(move.clone());
				this.musai1.runAction(move.clone());
				this.musai2.runAction(move.clone());
				this.dalishi.runAction(move.clone());
				var seq = cc.sequence(cc.moveBy(1,cc.p(0,-80)),cc.moveBy(1,cc.p(305,80)),cc.callFunc(function(){
					this.flowNext();
				},this));
				p.runAction(seq);

				this.glass.runAction(cc.sequence(cc.delayTime(2),cc.moveBy(0.5,cc.p(60,0))));
			}else if(action ==ACTION_DO2){
				this.beaker.runAction(cc.moveTo(1,cc.p(100,0)));
				
				var move = cc.moveTo(1,cc.p(250,200));
				var spawn = cc.spawn(cc.moveTo(1,cc.p(200,200)),cc.rotateTo(1,-100));
				var  seq = cc.sequence(move,spawn,cc.delayTime(1),cc.callFunc(function(){
					this.fire.runAction(cc.sequence(cc.spawn(cc.moveTo(1,cc.p(125,182)),cc.scaleTo(1,1)),cc.callFunc(function(){
						this.fire.setVisible(false);
					},this)));
				},this),cc.delayTime(2),cc.callFunc(function(){
					this.showtip = new ShowTip("蜡烛熄灭了",cc.p(920,400));
				},this),cc.delayTime(2),cc.callFunc(function(){
					this.flowNext();
				},this));
				p.runAction(seq);
				
				var seq1 = cc.sequence(cc.delayTime(1),cc.spawn(cc.moveTo(1,cc.p(80,250)),cc.rotateTo(1,30)));
				this.glass.runAction(seq1);
			}
			
		break;
		case TAG_SG:
			var move = cc.moveTo(1,cc.p(220,-180));
			var move1 = cc.moveTo(1,cc.p(220,20));
			var seq = cc.sequence(move,move1,cc.callFunc(function(){
				this.hunzhuo.runAction(cc.fadeIn(2));
			},this),cc.delayTime(2),cc.callFunc(function(){
				this.showtip = new ShowTip("澄清石灰水变浑浊",cc.p(920,400));
				this.flowNext();
			},this));
			p.runAction(seq);
		break;
		case TAG_SG1:
			this.getChildByTag(TAG_SG).setVisible(false);
			var move = cc.moveTo(1,cc.p(220,-180));
			var move1 = cc.moveTo(1,cc.p(220,20));
			var seq = cc.sequence(move,move1,cc.callFunc(function(){
				this.red.runAction(cc.fadeIn(2));
				this.zise.runAction(cc.fadeOut(2));
			},this),cc.delayTime(2),cc.callFunc(function(){
				this.showtip = new ShowTip("石蕊试液变红",cc.p(920,400));
				
			},this),cc.delayTime(3),cc.callFunc(function(){
				this.flask.setVisible(false);
				this.flask2.setVisible(false);
				this.daoguan.setVisible(false);
				this.dalishi.setVisible(false);
				this.changjing.setVisible(false);
				this.musai1.setVisible(false);
				this.musai2.setVisible(false);
				
				this.getChildByTag(TAG_SG1).setVisible(false);
				this.showtip.setVisible(false);
				this.beaker.setVisible(true);
				this.flowNext();
			},this));
			p.runAction(seq);
			break;
    	
		}
	},
	flowNext:function(){
		gg.flow.next();
	},
});