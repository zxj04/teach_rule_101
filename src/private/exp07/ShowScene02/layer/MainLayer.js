exp07.ShowMainLayer02 = cc.Layer.extend({
	lead:null,
	real:null,
	reset:true,
	reset1:true,
	reset2:true,
	ctor:function () {
		this._super();
		this.init();
		return true;
	},
	init:function(){	
		var sv = new pub.ScrollView(this);
		sv.setOffestPos(cc.p(152,45 + 15));
	
		this.addlayout1(sv);	
		this.addlayout2(sv);	
		this.addlayout3(sv);
		this.addlayout4(sv);
	},	
	addlayout1:function(parent){
		this.layout1 = new ccui.Layout();//第一页
		this.layout1.setContentSize(cc.size(996, 598));//设置layout的大小
		parent.addLayer(this.layout1);

		var text = $.format("　　向烧杯里倾倒二氧化碳，注意观察蜡烛火焰的变化。", 980, 30);
		label1 = new cc.LabelTTF(text,gg.fontName,gg.fontSize4);
		label1.setAnchorPoint(0,0.5);
		label1.setPosition(0,this.layout1.height-label1.height/2);
		label1.setColor(cc.color(255, 255, 255, 250));
		this.layout1.addChild(label1,10);	
		
		var bg = new cc.Sprite(res_public.run_back);
		this.layout1.addChild(bg);
		bg.setScale(0.8);
		bg.setPosition(cc.p(this.layout1.width /2, this.layout1.height / 2 +30));
		
		
		this.addImage(this.layout1, 2,"#beaker.png", cc.p(500 - 50,250), 0.7);
		this.addImage(this.layout1, 5,"#beaker1.png", cc.p(500 - 50,250), 0.7);
		this.addImage(this.layout1, 3,"#jiazi.png", cc.p(440 - 50,260), 0.7);
		this.addImage(this.layout1, 3,"#lazhu.png", cc.p(445 - 50,285 -4), 0.7);
		this.addImage(this.layout1, 3,"#lazhu.png", cc.p(475 - 50,195 -4), 0.7);
		
		this.fire = new cc.Sprite("#fire/1.png");
		this.fire.setPosition(cc.p(445 - 50,340 -4));
		this.layout1.addChild(this.fire,4);
		this.fire.runAction(this.addAction("fire/", 3, 0.05));
		
		this.fire1 = new cc.Sprite("#fire/1.png");
		this.fire1.setPosition(cc.p(475 - 50,250 -4));
		this.layout1.addChild(this.fire1,4);
		this.fire1.runAction(this.addAction("fire/", 3, 0.05).repeatForever());
				
		this.bottle = new cc.Sprite("#bottle.png");
		this.layout1.addChild(this.bottle);
		this.bottle.setPosition(cc.p(700,300));
		
		this.glass = new cc.Sprite("#glass.png");
		this.glass.setPosition(cc.p(80,240));
		this.bottle.addChild(this.glass);
		
		var label1 = new pub.CF(this.bottle,"CO","2","").setColor(cc.color(255,255,255));
		label1.setPosition(cc.p(50,120));
		label1.setScale(1.5);
//		label1 = new cc.LabelTTF("CO₂",gg.fontName,45);
//		label1.setAnchorPoint(0,0.5);
//		label1.setPosition(cc.p(40,120));
//		label1.setColor(cc.color(255, 255, 255, 250));
//		this.bottle.addChild(label1,10);	
	
		this.addbutton(this.layout1, TAG_BUTTON1, "播放", cc.p(250,180));
		this.layout1.getChildByTag(TAG_BUTTON1).getChildByTag(TAG_BUTTON1).setColor(cc.color(255, 255, 255, 250));
			
		this.label1 = new pub.RichTTF(this.layout1,gg.fontName,gg.fontSize4)
		.append(new pub.CF(this.alyout1,"结论：CO","2","").setColor(cc.color(255,0,0)),cc.color(255, 0, 0))
		.append(new pub.CF(this.alyout1,"不能燃烧也不支持燃烧并且CO","2","").setColor(cc.color(255,0,0)),cc.color(255, 0, 0))
		.append("的密度比空气大。",cc.color(255,0,0))
		.setVisit(false)
		.setPosition1(cc.p(80,80));
		//this.label1.setPosition(80,80);
		//this.label1.setVisible(false);
//		this.label1 = new cc.LabelTTF("结论：CO₂不能燃烧也不支持燃烧并且CO₂的密度比空气大。",gg.fontName,gg.fontSize4);
//		this.label1.setAnchorPoint(0,0.5);
//		this.label1.setPosition(80,80);
//		this.label1.setColor(cc.color(255, 0, 0, 250));
//		this.layout1.addChild(this.label1,10);
//		this.label1.setVisible(false);
		
	},
	addlayout2:function(parent){
		this.layout2 = new ccui.Layout();//第四页
		this.layout2.setContentSize(cc.size(996, 598));
		parent.addLayer(this.layout2);
		var text = $.format("　　在一个充满二氧化碳的软塑料瓶里，迅速倒入少量的水，立即将瓶塞塞紧振荡，观察有什么现象。", 980, 30);
		label1 = new cc.LabelTTF(text,gg.fontName,gg.fontSize4);
		label1.setAnchorPoint(0,0.5);
		label1.setPosition(0,this.layout2.height-label1.height/2);
		label1.setColor(cc.color(255, 255, 255, 250));
		this.layout2.addChild(label1,10);	
		
		var bg = new cc.Sprite(res_public.run_back);
		this.layout2.addChild(bg);
		bg.setScale(0.8);
		bg.setPosition(cc.p(this.layout2.width /2, this.layout2.height / 2 +30));

		this.pingzi = new cc.Sprite("#pingzi.png");
		this.pingzi.setPosition(cc.p(500,250));
    	this.layout2.addChild(this.pingzi,5);
    	this.pingzi.setScale(0.8);
    	
    	
    	this.pingzi1 = new cc.Sprite("#pingzi1.png");
    	this.pingzi1.setPosition(cc.p(70,185));
    	this.pingzi.addChild(this.pingzi1,5);
    	this.pingzi1.setCascadeOpacityEnabled(true);
    	this.pingzi1.setOpacity(0);
    	
    	this.gaizi = new cc.Sprite("#lid.png");
    	this.gaizi.setPosition(cc.p(70,340));
    	this.pingzi.addChild(this.gaizi,5);
    	
    	this.gaizi1 = new cc.Sprite("#lid.png");
    	this.gaizi1.setPosition(cc.p(69,330));
    	this.pingzi1.addChild(this.gaizi1,5);
    	
    	this.beaker = new cc.Sprite("#beaker.png");
    	this.beaker.setPosition(cc.p(300,350));
    	this.layout2.addChild(this.beaker,5);
    	this.beaker.setScale(0.5);
    	
    	this.beaker1 = new cc.Sprite("#beaker1.png");
    	this.beaker1.setPosition(cc.p(300,350));
    	this.layout2.addChild(this.beaker1,5);
    	this.beaker1.setScale(0.5);
    	
    	this.water = new cc.Sprite("#water.png");
    	this.water.setPosition(cc.p(130,250));
    	this.beaker.addChild(this.water,5);
    	this.water.setScale(2);
    	
    	this.pwater = new cc.Sprite("#water.png");
    	this.pwater.setPosition(cc.p(70,35));
    	this.pingzi.addChild(this.pwater,5);
    	this.pwater.setVisible(false);
    	
    	this.pwater1 = new cc.Sprite("#water.png");
    	this.pwater1.setPosition(cc.p(69,290));
    	this.pingzi1.addChild(this.pwater1,5);
    	this.pwater1.setScale(0.8);
		
		this.addbutton(this.layout2, TAG_BUTTON2, "播放", cc.p(700,180));
		this.layout2.getChildByTag(TAG_BUTTON2).getChildByTag(TAG_BUTTON2).setColor(cc.color(255, 255, 255, 250));
		
		this.label2 = new cc.LabelTTF("二氧化碳易溶于水，使瓶内气压减小。",gg.fontName,gg.fontSize4);
		this.label2.setAnchorPoint(0,0.5);
		this.label2.setPosition(80,100);
		this.label2.setColor(cc.color(255, 0, 0, 250));
		this.layout2.addChild(this.label2,5);	
		this.label2.setOpacity(0);
		
	},
	addlayout3:function(parent){
		this.layout3 = new ccui.Layout();//第四页
		this.layout3.setContentSize(cc.size(996, 598));
		parent.addLayer(this.layout3);

		var text = $.format("　　1号试管装有蒸馏水，2号试管装有变扁瓶中的液体，分别滴家少量紫色石蕊试液，观察现象。", 980, 30);
		label1 = new cc.LabelTTF(text,gg.fontName,gg.fontSize4);
		label1.setAnchorPoint(0,0.5);
		label1.setPosition(0,this.layout2.height-label1.height/2);
		label1.setColor(cc.color(255, 255, 255, 250));
		this.layout3.addChild(label1,10);	
		
		var bg = new cc.Sprite(res_public.run_back);
		this.layout3.addChild(bg);
		bg.setScale(0.8);
		bg.setPosition(cc.p(this.layout3.width /2, this.layout3.height / 2 +30));
		
		this.addImage(this.layout3, 5, "#shirui.png", cc.p(350,300), 0.8);
		
		this.drop = new cc.Sprite("#drop/1.png");
		this.drop.setPosition(cc.p(350,350));
		this.layout3.addChild(this.drop,4);
		this.drop.setScale(0.7);
		
		this.sg = new cc.Sprite("#sg.png");
		this.sg.setPosition(cc.p(500,250));
		this.layout3.addChild(this.sg,5);
		this.sg.setScale(0.6);
		
		this.addImage(this.sg, 5, "#water1.png", cc.p(30,120), 1);
		
		this.sg1 = new cc.Sprite("#sg.png");
		this.sg1.setPosition(cc.p(600,250));
		this.layout3.addChild(this.sg1,5);
		this.sg1.setScale(0.6);
		
		this.addImage(this.sg1, 5, "#water1.png", cc.p(30,120), 1);
		
		this.lamp = new cc.Sprite("#lampBottle.png");
		this.lamp.setPosition(cc.p(800,200));
		this.layout3.addChild(this.lamp,5);
		this.lamp.setScale(0.3);
		
		var fire = new cc.Sprite("#fire/1.png");
		fire.setPosition(cc.p(170,350));
		this.lamp.addChild(fire);
		fire.setScale(2.5);
		fire.runAction(this.addAction("fire/", 3, 0.05).repeatForever());
		
		this.zise = new cc.Sprite("#zise.png");
		this.zise.setPosition(cc.p(500,170));
		this.layout3.addChild(this.zise,5);
		this.zise.setScale(0.55);
		this.zise.setOpacity(0);
		
		this.red = new cc.Sprite("#red.png");
		this.red.setPosition(cc.p(600,170));
		this.layout3.addChild(this.red,5);
		this.red.setScale(0.55);
		this.red.setOpacity(0);
		
		this.zise1 = new cc.Sprite("#zise.png");
		this.zise1.setPosition(cc.p(600,270));
		this.layout3.addChild(this.zise1,5);
		this.zise1.setScale(0.55);
		this.zise1.setOpacity(0);

		this.addbutton(this.layout3, TAG_BUTTON3, "播放", cc.p(250,180));
		this.layout3.getChildByTag(TAG_BUTTON3).getChildByTag(TAG_BUTTON3).setColor(cc.color(255, 255, 255, 250));
		
		this.label3 = new cc.LabelTTF("结论：二氧化碳溶于水，形成碳酸，碳酸使紫色石蕊试液变红色，" +
				"\n　　　碳酸受热分解成二氧化碳和水，红色的石蕊又变成红色。",gg.fontName,gg.fontSize4);
		this.label3.setAnchorPoint(0,0.5);
		this.label3.setPosition(80,80);
		this.label3.setColor(cc.color(255, 0, 0, 250));
		this.layout3.addChild(this.label3);
		this.label3.setOpacity(0);
	},
	addlayout4:function(parent){
		this.layout4 = new ccui.Layout();//第一页
		this.layout4.setContentSize(cc.size(996, 598));//设置layout的大小
		parent.addLayer(this.layout4);

		var text = $.format("　　二氧化碳是一种无色无味的气体，它的密度比空气的密度大。在加压降温的情况下，CO₂能变成无色液体，甚至变成雪状固体，" +
				"通常人们把固体的CO₂叫做“干冰”。通常1体积水中能溶解1体积的CO₂，增大压强使其溶解的更多。" +
				"\n　　二氧化碳溶解在水里时，跟水发生化学反应生成碳酸。碳酸呈酸性，能使紫色石蕊试液变成红色。" , 980,30);
		label = new cc.LabelTTF(text,gg.fontName,gg.fontSize4);
		label.setColor(cc.color(0, 0, 0, 250));
		label.setAnchorPoint(0, 0.5);
		label.setPosition(0,this.layout4.height-label.height/2);
		this.layout4.addChild(label);	
		
		this.addImage(this.layout4, 2, "#gongshi.png", cc.p(500,300), 1);		
		
		label = new cc.LabelTTF("碳酸很不稳定，很容易分解。当加热时，碳酸会分解，逸出二氧化碳，剩下\n中性的水，因此，红色的石蕊又变成紫色",gg.fontName,gg.fontSize4);
		label.setColor(cc.color(0, 0, 0, 250));
		label.setAnchorPoint(0, 0.5);
		label.setPosition(0,200);
		this.layout4.addChild(label);
		
		this.addImage(this.layout4, 2, "#gongshi1.png", cc.p(500,100), 1);
	},
	addShuidi:function(pos,pos1){		
		var shuidi = new cc.Sprite("#shuidi.png");
		shuidi.setPosition(pos);
		shuidi.setScale(0.5);
		this.layout3.addChild(shuidi,5);
		var seq = cc.sequence(cc.spawn(cc.scaleTo(0.3,1),cc.moveTo(0.3,pos1)),cc.callFunc(function(){
			shuidi.removeFromParent(true);
		},this));
		shuidi.runAction(seq);
	},
	addAction:function(str1,num,time){
		var frames = [];
		for(var i =1 ;i<=num;i++){
			var str = str1+i+".png";
			var frame=cc.spriteFrameCache.getSpriteFrame(str);
			frames.push(frame);		
		}
		var animation = new cc.Animation(frames,time);//负责动画序列
		var action = new cc.Animate(animation);//帧动画的动作创建
		return action;
	},
	addImage:function(parent,index,str,pos,scale){
		var image = new cc.Sprite(str);
		parent.addChild(image,5);
		image.setPosition(pos);
		if(scale != null){
			image.setScale(scale);
			
		}
		image.setLocalZOrder(index);
	},
	addbutton:function(parent,tag,str,pos){
		var button = new ButtonScale(parent,"#unsel.png",this.callback,this);
		button.setPosition(pos);
		button.setLocalZOrder(5);
		button.setTag(tag);
		var label = new cc.LabelTTF(str,gg.fontName,30);
		label.setPosition(button.width/2,button.height/2);		
		label.setColor(cc.color(0, 0, 0, 250));
		button.addChild(label);
		label.setTag(tag);
	},
	callback:function(p){
		switch(p.getTag()){	
		case TAG_BUTTON1:
			this.reset = !this.reset;
			if(!this.reset){
				p.setEnable(false);				
				var move = cc.moveTo(1,cc.p(650,480));
		        var move1 = cc.moveTo(1,cc.p(550,480));
		        var rota = cc.rotateTo(1,-110);
		        var spawn = cc.spawn(move1,rota);
		        			
		        var seq = cc.sequence(move,spawn,cc.callFunc(function(){
		        	this.glass.runAction(cc.spawn(cc.rotateTo(0.5,30),cc.moveTo(0.5,cc.p(100,260))));
		        },this),cc.delayTime(2),cc.callFunc(function(){
					var seq1 = cc.sequence(cc.spawn(cc.scaleTo(1,0.5),cc.moveTo(1,cc.p(425,240 -4))),cc.callFunc(function(){
						this.fire1.setOpacity(0);
					},this));
					this.fire1.runAction(seq1);
				},this),cc.delayTime(2),cc.callFunc(function(){
					var seq2 = cc.sequence(cc.spawn(cc.scaleTo(1,0.5),cc.moveTo(1,cc.p(395,330 -4))),cc.callFunc(function(){
						this.fire.setOpacity(0);
					},this));
					this.fire.runAction(seq2);
				},this),cc.delayTime(2),cc.callFunc(function(){
					p.setEnable(true);
					this.label1.setVisible(true);
					this.layout1.getChildByTag(TAG_BUTTON1).getChildByTag(TAG_BUTTON1).setString("复原");
				},this));
				this.bottle.runAction(seq);		
			}else{
				this.fire.setOpacity(250);
				this.fire.setScale(1);
				this.fire.setPosition(cc.p(395,340 -4));				
				this.fire1.setOpacity(250);
				this.fire1.setScale(1);
				this.fire1.setPosition(cc.p(425,250 -4));
				this.layout1.getChildByTag(TAG_BUTTON1).getChildByTag(TAG_BUTTON1).setString("播放");			
				this.bottle.setRotation(0);
				this.bottle.setPosition(cc.p(700,300));
				this.glass.setRotation(0);
				this.glass.setPosition(cc.p(80,240));
			}
		break;
		case TAG_BUTTON2:
			this.reset1 = !this.reset1;
			if(!this.reset1){
				p.setEnable(false);
				this.gaizi.runAction(cc.sequence(cc.moveBy(0.5,cc.p(0,30)),cc.moveBy(0.5,cc.p(80,0))));
				var move = cc.moveTo(1,cc.p(400,420));
				var rota = cc.rotateTo(1,70);
				var seq = cc.sequence(cc.delayTime(1),move,cc.callFunc(function(){
					this.water.runAction(cc.spawn(cc.moveTo(1,cc.p(160,210)),cc.rotateTo(1,-65),cc.scaleTo(1,3)));
				},this),rota,cc.callFunc(function(){
					this.pwater.setVisible(true);
					this.pwater.runAction(cc.moveTo(1,cc.p(70,250)));
				},this),cc.delayTime(1),cc.callFunc(function(){
					this.beaker.setVisible(false);
					this.beaker1.setVisible(false);
					this.gaizi.runAction(cc.sequence(cc.moveBy(0.5,cc.p(-80,0)),cc.moveBy(0.5,cc.p(0,-30))));
				},this),cc.delayTime(5),cc.callFunc(function(){
					p.setEnable(true);
					this.label2.setOpacity(250);
					this.layout2.getChildByTag(TAG_BUTTON2).getChildByTag(TAG_BUTTON2).setString("复原");
				},this));
				this.beaker.runAction(seq);
				this.beaker1.runAction(cc.sequence(cc.delayTime(1),move.clone(),rota.clone()));
				
				var rota = cc.rotateTo(0.2,10);
				var rota1 = cc.rotateTo(0.2,-10);
				var rota2 = cc.rotateTo(0.2,0);
				var se = cc.sequence(cc.delayTime(5),rota,rota1,rota,rota1,cc.callFunc(function(){
					this.pingzi1.runAction(cc.fadeIn(2));
					this.pingzi.runAction(cc.fadeOut(2));
					this.gaizi.runAction(cc.fadeOut(2));
					this.pwater.runAction(cc.fadeOut(2));
				},this),rota,rota1,rota,rota1,rota2);
				this.pingzi.runAction(se);		
			}else{
				this.layout2.getChildByTag(TAG_BUTTON2).getChildByTag(TAG_BUTTON2).setString("播放");
				this.beaker.setPosition(cc.p(300,350));
				this.beaker1.setPosition(cc.p(300,350));
				this.water.setPosition(cc.p(130,250));
				this.water.setScale(2);
				this.water.setRotation(0);
				this.beaker.setVisible(true);
				this.beaker1.setVisible(true);
				this.beaker.setRotation(0);
				this.beaker1.setRotation(0);			
				this.pwater.setVisible(false);
				this.pwater.setOpacity(255);
				this.pwater.setPosition(cc.p(70,35));
				this.water.setPosition(cc.p(130,250));
				this.pingzi.setOpacity(255);
				this.gaizi.setOpacity(255);				
				this.pingzi1.setOpacity(0);
			}
		break;
		case TAG_BUTTON3:
			this.reset2 = !this.reset2;
			if(!this.reset2){
				p.setEnable(false);
				var action = this.addAction("drop/", 5, 0.1)
				var move = cc.moveTo(1,cc.p(350,480));
				var move1 = cc.moveTo(1,cc.p(500,480));
				var move2 = cc.moveTo(1,cc.p(350,350));
				var move3 = cc.moveTo(1,cc.p(600,480));
				var seq = cc.sequence(action,move,move1,cc.callFunc(function(){
					this.schedule(function(){
						this.addShuidi(cc.p(500,350), cc.p(500,200))
					}, 0.2, 3);
				},this),action.reverse(),cc.callFunc(function(){
					this.zise.runAction(cc.fadeIn(1));
				},this),cc.delayTime(1),move,move2,action,move,move3,cc.callFunc(function(){
					this.schedule(function(){
						this.addShuidi(cc.p(600,350), cc.p(600,200))
					}, 0.2, 3);
				},this),action.reverse(),cc.callFunc(function(){
					this.red.runAction(cc.fadeIn(1));
				},this),cc.delayTime(1),move,move2,cc.delayTime(1),cc.callFunc(function(){
					this.lamp.runAction(cc.moveTo(0.5,cc.p(600,180)));
					this.sg1.runAction(cc.moveTo(0.5,cc.p(600,350)));
					this.red.runAction(cc.moveTo(0.5,cc.p(600,270)));
				},this),cc.delayTime(1),cc.callFunc(function(){
					this.zise1.runAction(cc.fadeIn(1));
					this.red.runAction(cc.fadeOut(1));
				},this),cc.delayTime(2),cc.callFunc(function(){
					p.setEnable(true);
					this.layout3.getChildByTag(TAG_BUTTON3).getChildByTag(TAG_BUTTON3).setString("复原");
					this.label3.setOpacity(255);
				},this));
				this.drop.runAction(seq);
			}else{
				this.sg1.setPosition(cc.p(600,250));
				this.red.setPosition(cc.p(600,170));
				this.zise.setOpacity(0);
				this.zise1.setOpacity(0);
				this.red.setOpacity(0);
				this.lamp.setPosition(cc.p(800,200));
				this.layout3.getChildByTag(TAG_BUTTON3).getChildByTag(TAG_BUTTON3).setString("播放");
			}
		break;
		}
	}
});
