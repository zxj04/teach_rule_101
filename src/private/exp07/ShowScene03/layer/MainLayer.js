exp07.ShowMainLayer03 = cc.Layer.extend({
	lead:null,
	real:null,
	reset:true,
	reset1:true,
	ctor:function () {
		this._super();
		this.init();
		return true;
	},
	init:function(){
		var sv = new pub.ScrollView(this);
		sv.setOffestPos(cc.p(152,45 + 15));

		this.addlayout1(sv);
	},	
	addlayout1:function(parent){
		this.layout1 = new ccui.Layout();//第一页
		this.layout1.setContentSize(cc.size(996, 598));//设置layout的大小
		parent.addLayer(this.layout1);

		var text = $.format("　　植物的光合作用需要大量的二氧化碳，所以可以吧二氧化碳作为气体肥料。二氧化碳也可用于制纯碱、尿素和汽水。" +
				"由于二氧化碳既不能燃烧也不支持燃烧，因此，它还可用于灭火。" +
				"\n　　干冰（固态二氧化碳）升华变成气体时，需要吸收大量热，会使周围的空气温度迅速降低，空气里的水蒸气就凝结成雾。", 960, 30);
		label1 = new cc.LabelTTF(text,gg.fontName,gg.fontSize4);
		label1.setAnchorPoint(0,0.5);
		label1.setPosition(0,this.layout1.height-label1.height/2);
		label1.setColor(cc.color(0, 0, 0, 250));
		this.layout1.addChild(label1);	

		this.addshowimage(this.layout1,"#feiliao.png",cc.p(150,230),"气体肥料");
		this.addshowimage(this.layout1,"#ganbing.png",cc.p(500,230),"干冰升华");			
		this.addshowimage(this.layout1,"#jiangyu.png",cc.p(850,230),"人工降雨");	
	},

	addAction:function(str1,num,time){
		var frames = [];
		for(var i =1 ;i<=num;i++){
			var str = str1+i+".png";
			var frame=cc.spriteFrameCache.getSpriteFrame(str);
			frames.push(frame);		
		}
		var animation = new cc.Animation(frames,time);//负责动画序列
		var action = new cc.Animate(animation);//帧动画的动作创建
		return action.repeatForever();
	},
	moveSg:function(){
		this.layout2.getChildByTag(TAG_BUTTON1).setEnable(false);
		var move = cc.moveTo(1,cc.p(625,485));
		var rota = cc.rotateTo(1,-100);
		var seq = cc.sequence(move,cc.callFunc(function(){
			this.shiguan.runAction(cc.rotateTo(0.5,10));
			this.water.runAction(cc.rotateTo(0.5,-10));
			this.mg.runAction(cc.spawn(cc.rotateTo(0.5,12),cc.moveTo(0.5,cc.p(425,160))));
			this.water1.runAction(cc.rotateTo(1,50),cc.scaleTo(1.2));
		},this),rota,cc.callFunc(function(){
			this.water.setOpacity(255);
			this.water.runAction(cc.moveTo(1.5,cc.p(18,100)));
			var se = cc.sequence(cc.moveTo(1,cc.p(18,25)),cc.spawn(cc.moveTo(0.5,cc.p(10,15)),cc.scaleTo(0.5,0)));
			this.water1.runAction(se);
		},this),cc.delayTime(1.5),cc.callFunc(function(){
			this.paomo1.setOpacity(255);
			this.paomo.runAction(cc.fadeIn(4));
			this.shiguan.runAction(cc.rotateTo(0.5,0));
			this.water.runAction(cc.rotateTo(0.5,0));
			var seq1 = cc.sequence(cc.spawn(cc.rotateTo(0.5,0),cc.moveTo(0.5,cc.p(450,160))),cc.delayTime(1),cc.scaleTo(80,0.2));
			this.mg.runAction(seq1);
			this.re.runAction(cc.fadeIn(4));
		},this),cc.delayTime(6),cc.callFunc(function(){
			this.layout2.getChildByTag(TAG_BUTTON1).getChildByTag(TAG_BUTTON1).setString("复原");
			this.layout2.getChildByTag(TAG_BUTTON1).setEnable(true);
			this.label1.setVisible(true);
		},this));
		this.shiguan1.runAction(seq);
	},
	movebaeker:function(){
		this.layout3.getChildByTag(TAG_BUTTON2).setEnable(false);
		var move = cc.moveBy(0.5,cc.p(0,50));
		var move1 = cc.moveBy(0.5,cc.p(0,-50));
		var seq = cc.sequence(move,cc.delayTime(0.5),move1);
		this.beaker.runAction(seq);
		this.beaker1.runAction(seq.clone());

		this.glass.runAction(cc.moveTo(1,cc.p(500,180)));
	},
	movePaper:function(){
		var move = cc.moveTo(1,cc.p(600,400));
		var rota = cc.rotateTo(0.5,-20);
		var seq = cc.sequence(move,rota,cc.callFunc(function(){
			this.nhcl1.runAction(cc.moveTo(0.5,cc.p(20,45)));
		},this),cc.delayTime(0.5),cc.callFunc(function(){
			this.paper.setVisible(false);
			this.nhcl.setOpacity(255);
			this.nhcl.runAction(cc.spawn(cc.moveTo(1,cc.p(500,230)),cc.scaleTo(1,0.45)));
		},this),cc.delayTime(1),cc.callFunc(function(){
			this.mohu.runAction(cc.fadeIn(5));
			this.yeti.runAction(cc.fadeIn(5));
			this.nhcl.runAction(cc.fadeOut(5));
			this.baoh.runAction(cc.fadeOut(5));
		},this));
		this.paper.runAction(seq);

		var rota1 = cc.rotateTo(0.5,-90);
		var rota2 = cc.rotateTo(0.5,-100);
		var mov = cc.moveTo(0.5,cc.p(190,300));
		var mov1 = cc.moveTo(0.5,cc.p(40,300));
		var se = cc.sequence(cc.delayTime(2),rota1,mov,mov1,mov,mov1,cc.callFunc(function(){
			this.glass2.runAction(cc.fadeIn(3));
		},this),mov,mov1,mov,mov1,mov,mov1,rota2);
		this.rod.runAction(se);
	},
	addshowimage:function(parent,str,pos,str2){
		var showimage = new cc.Sprite(str);
		showimage.setPosition(pos);
		parent.addChild(showimage,5);
		var label = new cc.LabelTTF(str2,gg.fontName,gg.fontSize);
		label.setColor(cc.color(0, 0, 0, 250));
		parent.addChild(label);
		label.setPosition(showimage.x,showimage.y - showimage.height * 0.5 -20);
	},
	addImage:function(parent,str,pos,scale){
		var image = new cc.Sprite(str);
		parent.addChild(image,5);
		image.setPosition(pos);
		if(scale !=null){
			image.setScale(scale);
		}else{
			image.setScale(1);
		}
	},
	addbutton:function(parent,tag,str,pos){
		var button = new ButtonScale(parent,"#unsel.png",this.callback,this);
		button.setPosition(pos);
		button.setLocalZOrder(5);
		button.setTag(tag);
		var label = new cc.LabelTTF(str,gg.fontName,30);
		label.setPosition(button.width/2,button.height/2);		
		label.setColor(cc.color(0, 0, 0, 250));
		button.addChild(label);
		label.setTag(tag);
	},
	callback:function(p){
		switch(p.getTag()){	
		case TAG_BUTTON1:
			this.reset = !this.reset;
			if(!this.reset){
				this.moveSg();
			}else{
				this.layout2.getChildByTag(TAG_BUTTON1).getChildByTag(TAG_BUTTON1).setString("播放");
				this.mg.stopAllActions();
				this.mg.setScale(1,0.6);
				this.water.setPosition(cc.p(18,20));
				this.water.setOpacity(0);
				this.shiguan1.setPosition(cc.p(600,300));
				this.shiguan1.setRotation(0);
				this.water1.setPosition(cc.p(18,100));
				this.water1.setRotation(0);
				this.water1.setScale(1);
				this.re.setOpacity(0);
				this.paomo.setOpacity(0);
				this.paomo1.setOpacity(0);
			}
			break;
		case TAG_BUTTON2:			
			this.reset1 = !this.reset1;
			if(!this.reset1){
				var seq = cc.sequence(cc.callFunc(function(){
					this.movebaeker();
				},this),cc.delayTime(2),cc.callFunc(function(){
					this.movePaper();
				},this),cc.delayTime(10),cc.callFunc(function(){
					var move = cc.moveBy(0.5,cc.p(0,50));
					this.beaker.runAction(move);
					this.beaker1.runAction(move.clone());
					this.glass.runAction(move.clone());
					this.mohu.runAction(move.clone());
					this.leng.setOpacity(255);
				},this),cc.delayTime(0.5),cc.callFunc(function(){
					this.layout3.getChildByTag(TAG_BUTTON2).getChildByTag(TAG_BUTTON2).setString("复原");
					this.layout3.getChildByTag(TAG_BUTTON2).setEnable(true);
					this.label2.setVisible(true);
				},this));
				p.runAction(seq);
			}else{
				this.layout3.getChildByTag(TAG_BUTTON2).getChildByTag(TAG_BUTTON2).setString("播放");
				this.paper.setVisible(true);
				this.paper.setRotation(0);
				this.paper.setPosition(cc.p(700,300));
				this.mohu.setOpacity(0);
				this.mohu.setPosition(cc.p(495,250));
				this.yeti.setOpacity(0);
				this.baoh.setOpacity(255);
				this.nhcl.setScale(0.35);
				this.nhcl.setPosition(cc.p(500,380));
				this.nhcl1.setPosition(cc.p(50,45));
				this.glass.setPosition(300,180);
				this.glass2.setOpacity(0);
				this.beaker.setPosition(cc.p(500,250));
				this.beaker1.setPosition(cc.p(505,240));
				this.leng.setOpacity(0);
			}
			break;
		}
	}	
});
