exp07.ShowMainLayer01 = cc.Layer.extend({
	lead:null,
	real:null,
	reset:true,
	reset1:false,
	open_flag:false,
	donum:0,
	ctor:function () {
		this._super();
		this.init();
		return true;
	},
	init:function(){	
		var sv = new pub.ScrollView(this);
		sv.setOffestPos(cc.p(152,45 + 15));

		this.addlayout1(sv);	
	
	},	
	addlayout1:function(parent){
		this.layout1 = new ccui.Layout();//第一页
		this.layout1.setContentSize(cc.size(996, 598));//设置layout的大小
		parent.addLayer(this.layout1);

		var text = $.format("　　二氧化碳\n　　二氧化碳是由大量的二氧化碳分子\n构成的，每个二氧化碳分子由1个碳原\n子" +
				"和2个氧原子构成，其分子结果模型\n如右图所示。", 980,30);
		label = new cc.LabelTTF(text,gg.fontName,gg.fontSize4);
		label.setColor(cc.color(0, 0, 0, 250));
		label.setAnchorPoint(0, 0.5);
		label.setPosition(0,this.layout1.height-label.height/2);
		this.layout1.addChild(label);	

		var co2 = new cc.Sprite("#CO2/1.png");
		co2.setPosition(cc.p(700,430));
		this.layout1.addChild(co2,5);
		co2.runAction(this.addAction("CO2/",30,0.1));

        var text = new pub.RichTTF(this.layout1,gg.fontName,30)
        .append(new pub.CF(this.layout1,"　　CO","2","").setColor(cc.color(0,0,0), cc.color(0,0,0)))
        .append(new pub.CF(this.layout1,"本身没有毒性，但当空气中的CO","2","").setColor(cc.color(0,0,0), cc.color(0,0,0)))
        .append("超过正常含量时，会对人体产",cc.color(0,0,0))
        .enter()
        .append("生有害的影响。",cc.color(0,0,0))
        .setPosition(0,300);
				cc.log(text);
//		label = new cc.LabelTTF("\n　　CO₂本身没有毒性，但当空气中的CO₂超过正常含量时，会对人体产\n生有害的影响。",gg.fontName,gg.fontSize4);
//		label.setColor(cc.color(0, 0, 0, 250));
//		label.setAnchorPoint(0, 0.5);
//		label.setPosition(0,300);
//		this.layout1.addChild(label);	

		this.addImage(this.layout1,"#biaoge.png",cc.p(500,150));
								
	},
	addImage:function(parent,str,pos){
		var image = new cc.Sprite(str);
		parent.addChild(image,5);
		image.setPosition(pos);
	},
	addAction:function(str1,num,time){
		var frames = [];
		for(var i =1 ;i<=num;i++){
			var str = str1+i+".png";
			var frame=cc.spriteFrameCache.getSpriteFrame(str);
			frames.push(frame);		
		}
		var animation = new cc.Animation(frames,time);//负责动画序列
		var action = new cc.Animate(animation);//帧动画的动作创建
		return action.repeatForever();
	},
	addbutton:function(parent,tag,str,pos){
		var button = new ButtonScale(parent,"#unsel.png",this.callback,this);
		button.setPosition(pos);
		button.setLocalZOrder(5);
		button.setTag(tag);
		var label = new cc.LabelTTF(str,gg.fontName,30);
		label.setPosition(button.width/2,button.height/2);		
		label.setColor(cc.color(0, 0, 0, 250));
		button.addChild(label);
		label.setTag(tag);
	},
	callback:function(p){
		switch(p.getTag()){	
		
		}
	}
});
