exp07.RunLayer2 = cc.Layer.extend({
	arr:null,
	scene:null,
	clock:null,
	ctor:function (parent) {
		this._super();
		this.scene = parent;
		this.scene.addChild(this, 10);
		gg.main = this;
		this.init();
	},
	init:function () {
		this.callNext = cc.callFunc(function(){
			gg.flow.next();
		}, this);
		this.callKill = cc.callFunc(function(p){
			var seq = cc.sequence(cc.fadeOut(0.5),cc.callFunc(function(){
				p.removeFromParent(true);	
			}, this));
			p.runAction(seq);
		}, this); 

		this.callNext.retain();
		this.callKill.retain();
		//时钟
		this.clock = new Clock(this);
		// 物品库
		this.lib = new Lib(this);

		this.lib.loadBg([{
			tag:TAG_LIB_NHCLBEAKER,
			checkright:true,	//实验所需器材添加checkright标签		
		},
		{
			tag:TAG_LIB_BOTTLE,
		},
		{
			tag:TAG_LIB_SANJIAOFLASK,
			checkright:true,
		},
		{
			tag:TAG_LIB_AMPULLA,
			//checkright:true,
		},
		{
			tag:TAG_LIB_XIDIJI,
			checkright:true,
		},
		{
			tag:TGA_LIB_GLASS,
		},
		{
			tag:TAG_LIB_TESTTUBE,
			checkright:true,
		},
		{
			tag:TAG_LIB_BEAKER,
			//checkright:true,
		},
		{
			tag:TAG_LIB_NNA2CO3BEAKER,
			checkright:true,
		},
		{
			tag:TAG_LIB_CONICAL,
		}
		]);


		this.xidiji = new exp07.Xidiji(this);
		this.xidiji.setVisible(false);
		this.xidiji.setPosition(370,550);

		this.conflask = new exp07.Conflask(this);
		this.conflask.setVisible(false);
		this.conflask.setPosition(650,220);

		this.nabeaker = new exp07.Na2Co3beaker(this);
		this.nabeaker.setVisible(false);
		this.nabeaker.setPosition(800,500);

		this.hbeaker = new exp07.Hclbeaker(this);
		this.hbeaker.setVisible(false);
		this.hbeaker.setPosition(1000,500);

	},
	checkVisible:function(next){
		//是否可见
		var checkVisible = [];
		var node1 = ll.run.xidiji;
		var node2 = ll.run.conflask;
		var node3 = ll.run.nabeaker;
		var node4 = ll.run.hbeaker;

		checkVisible.push(node1,node2,node3,node4);
		for(var i in checkVisible){
			if(checkVisible[i] !== null){				
				checkVisible[i].setVisible(next);				
			}			
		}
	},
	loadInLib:function(obj, pos, tarPos,delay){
		obj.setPosition(pos);
		if(delay == null){
			delay = 1;
		}
		var ber = $.bezier(pos, tarPos, delay);
		var seq = cc.sequence(ber, this.callNext);
		obj.runAction(seq);
	},
	kill:function(obj){
		var fade = cc.fadeTo(0.5,0);
		var func = cc.callfunc(function(){
			obj.removeFromParent(true);
		},this);
		var seq = cc.sequence(fade,func);
		obj.runAction(seq)
	},

	callback:function (p){
		var func = cc.callFunc(this.actionDone, this);
		var action=gg.flow.flow.action;
		switch(p.getTag()){

		}
	},
	actionDone:function(p){
		var func = cc.callFunc(this.actionDone, this);
		switch(p.getTag()){

		}
	},
	flowNext:function(){
		gg.flow.next();
	},
	onExit:function(){
		this._super();
		this.callNext.release();
		this.callKill.release();
	}
});