exp07.Xidiji = cc.Node.extend({
	ctor:function(p){
		this._super();
		p.addChild(this, 10, TAG_XIDIJI_NODE);
		this.init();
		//this.adddrip();
	},
	init : function(){
		this.setCascadeOpacityEnabled(true);
//		var x1111= new Button(this, 10, TAG_LINE2, "#11111.png",this.callback);
//		x1111.setScale(0.8);
//		x1111.setPosition(300,-150);
		
		var xidiji= new Button(this, 10, TAG_XIDIJI, "#exp10bottle/4.png",this.callback);
		xidiji.setScale(0.8);
	    xidiji.setCascadeOpacityEnabled(true);
		
		var xidijiline= new Button(this, 1, TAG_LINE1, "#exp10bottle/7.png",this.callback);
		xidijiline.setScale(0.8);
		xidijiline.setPosition(0,-30);
		
		this.drop= new Button(this, 1, TAG_DROP, "#exp10bottle/10.png",this.callback);
		this.drop.setScale(0.8);
		this.drop.setPosition(0,25);

		this.dropline = new cc.Sprite("#exp10bottle/6.png");
		this.drop.addChild(this.dropline,5);
		this.dropline.setScale(0.2);
		this.dropline.setVisible(false);
		this.dropline.setPosition(18,10);
		//this.dropline.setPosition(143,201);
		
		//"NH₃ + CO₂ + H₂O = NH₄HCO₃\n" 
		var label = new cc.LabelTTF("洗涤剂 ","微软雅黑",22);
		label.setPosition(145,105);
		label.setColor(cc.color(0,0,0));
		xidiji.addChild(label,11);


	},
	adddrip :function(pos,pos1,time,count){
		count --;
		if(count <= 0){
			return;
		}
		var drip = new cc.Sprite("#exp10bottle/wpoint.png");
		drip.setPosition(pos);
		drip.setScale(1.2);
		this.drop.addChild(drip,11);
		var move = cc.moveTo(time,cc.p(pos1));
		var seq = cc.sequence(move,cc.callFunc(function(){
			drip.removeFromParent(true);
			this.adddrip(pos, pos1, time, count);
		},this));
		drip.runAction(seq);
	},
	callback:function(p){
		var action = gg.flow.flow.action;
		var func = cc.callFunc(this.actionDone, this);	
		switch(p.getTag()){	
		case TAG_DROP:
			this.runAction(cc.moveTo(0.5,cc.p(370,500)));
			this.drop.setSpriteFrame("exp10bottle/11.png");
			this.dropline.setVisible(true);
			var move = cc.moveTo(0.5,cc.p(18,130));
			var sca = cc.scaleTo(0.5,1);
			var sp = cc.spawn(move,sca);
			this.dropline.runAction(sp);			
			
			var move1 = cc.moveTo(1,cc.p(0,150));
			var move2 = cc.moveTo(1,cc.p(230,-50));
			var move3 = cc.moveTo(1,cc.p(230,-150));
			var seq = cc.sequence(cc.delayTime(0.5),move1,move2,move3,cc.callFunc(function(){
				this.adddrip(cc.p(18,0), cc.p(18,-100), 0.5,5);
				this.dropline.runAction(cc.spawn(cc.moveTo(2,cc.p(18,10)),cc.scaleTo(2,0)));
			
			},this),cc.delayTime(2),cc.callFunc(function(){
				this.setOpacity(0);
				//this.removeFromParent(true);
				this.flowNext();
			},this));
			p.runAction(seq);
			break;


		}
	},
	flowNext:function(){
		gg.flow.next();
	},
});