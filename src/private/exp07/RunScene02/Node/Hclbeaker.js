exp07.Hclbeaker = cc.Node.extend({
	ctor:function(p){
		this._super();
		p.addChild(this, 10, TAG_BEAKER_NODE);
		this.init();
	},
	init : function(){
		this.setCascadeOpacityEnabled(true);

		this.beaker= new Button(this, 10, TAG_BEAKER, "#beaker1.png",this.callback);
		this.beaker.setScale(0.45);
		this.beaker.setCascadeOpacityEnabled(true);


		this.beakerline = new cc.Sprite("#beakerline.png");
		this.addChild(this.beakerline,5);
		this.beakerline.setScale(0.45);
		this.beakerline.setPosition(5,10);
		//this.dropline.setPosition(143,201);

		//"NH₃ + CO₂ + H₂O = NH₄HCO₃\n" 
		var label = new cc.LabelTTF("浓盐酸","微软雅黑",40);
		label.setPosition(155,170);
		label.setColor(cc.color(0,0,0));
		this.beaker.addChild(label,11);


	},
	bline:function(pos,scale,rotate,time){
		var move = cc.moveTo(time,cc.p(pos));
		var rota = cc.rotateTo(time,rotate);
		var sca = cc.scaleTo(time,scale);
		var sp = cc.spawn(move,rota,sca);
		this.beakerline.runAction(sp);
	},
	callback:function(p){
		var action = gg.flow.flow.action;
		var func = cc.callFunc(this.actionDone, this);	
		switch(p.getTag()){	

		case TAG_BEAKER:
			var move = cc.moveTo(1,cc.p(920,295));
			var rota = cc.rotateTo(1,-60);
			var sp = cc.spawn(move,rota);

			var seq = cc.sequence(cc.callFunc(function(){
				this.bline(cc.p(-12,-10), 0.65, 60, 1);
			},this),sp,cc.callFunc(function(){
				ll.run.conflask.sgline.setVisible(true);
				ll.run.conflask.sgline.runAction(cc.moveTo(1.5,cc.p(20,100)));
			},this),cc.delayTime(1.5),cc.callFunc(function(){
				this.setOpacity(0);
				//this.removeFromParent(true);
				this.flowNext();
			},this));
			this.runAction(seq);
	
			break;

		}
	},
	flowNext:function(){
		gg.flow.next();
	},
});