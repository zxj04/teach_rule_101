exp07.Conflask = cc.Node.extend({
	ctor:function(p){
		this._super();
		p.addChild(this, 10, TAG_CONFLASK_NODE);
		this.init();
	},
	init : function(){
		this.setCascadeOpacityEnabled(true);
		this.conflask1= new Button(this, 10, TAG_XCONFLASK, "#sanjiaoflask1.png",this.callback);
		this.conflask1.setScale(0.8);
		
		this.conflask= new cc.Sprite("#sanjiaoflask.png");
		this.conflask.setPosition(175,166);
		this.conflask1.addChild(this.conflask,10);

		var conflasklid= new Button(this.conflask1, 5, TAG_LID, "#sanjiaolid2.png",this.callback,this);
		conflasklid.setPosition(110,310);
		
		this.conflaskline = new cc.Sprite("#sanjiaoline1.png");
		this.conflask1.addChild(this.conflaskline,5);
		this.conflaskline.setScale(1.2,1.8);
		this.conflaskline.setVisible(false);
		this.conflaskline.setPosition(111,50);

		this.sg= new Button(this.conflask1, 5, TAG_SG, "#exp10sg.png",this.callback,this);
		this.sg.setPosition(400,150);
		
		this.line = new cc.Sprite("#exp10sgline1.png");
		this.sg.addChild(this.line,5);
		this.line.setRotationY(180);
		this.line.setPosition(-8,55);
		
//		this.line1 = new cc.Sprite("#exp10sgline2.png");
//		this.sg.addChild(this.line1,5);
//		//this.line1.setRotationY(180);
//		this.line1.setPosition(-8,55);
//		
//		this.line2 = new cc.Sprite("#exp10sgline3.png");
//		this.sg.addChild(this.line2,5);
//		//this.line2.setRotationY(180);
//		this.line2.setPosition(-8,55);
		
		this.sgline = new cc.Sprite("#sanjiaoline1.png");
		this.sg.addChild(this.sgline,5);
		this.sgline.setScale(0.15);
		this.sgline.setPosition(20,10);
		this.sgline.setVisible(false);				
	},
	addpaomo :function(){
		var paomo = new cc.Sprite("#paomo.png");
		paomo.setRotationX(180);
		paomo.setPosition(118,208);
		paomo.setOpacity(0);
		this.conflask1.addChild(paomo,10);
		var seq = cc.sequence(cc.delayTime(0.5),cc.fadeIn(0.5));
		paomo.runAction(seq);
		
		
		var paomo1 = new cc.Sprite("#paomo1.png");
		paomo1.setPosition(350,25);
		paomo1.setAnchorPoint(0, 0.5);
		paomo1.setScale(0);
		this.conflask.addChild(paomo1,10);
		var se = cc.sequence(cc.delayTime(0.5),cc.scaleTo(1,1));
		paomo1.runAction(se);
		
	},

	callback:function(p){
		var action = gg.flow.flow.action;
		var func = cc.callFunc(this.actionDone, this);	
		switch(p.getTag()){	
		case TAG_LID:
			if(action == ACTION_DO1){
				var move = cc.moveTo(0.5,cc.p(110,370));
				var move1 = cc.moveTo(0.5,cc.p(0,350));
				var move2 = cc.moveTo(1,cc.p(-50,60));
				var rota  = cc.rotateTo(1,-180);
				var sp = cc.spawn(move2,rota);
				var seq = cc.sequence(move,move1,sp,cc.callFunc(function(){
					p.setSpriteFrame("sanjiaolid1.png");
					p.setRotation(0);
					this.flowNext();
				},this));
			}else if(action == ACTION_DO2){
				p.setSpriteFrame("sanjiaolid2.png");
				var move = cc.moveTo(1,cc.p(0,350));
				var rota  = cc.rotateTo(1,0);
				var sp = cc.spawn(move,rota);
				var move1 = cc.moveTo(0.5,cc.p(110,370));				
				var move2 = cc.moveTo(0.5,cc.p(110,310));
				var seq = cc.sequence(sp,move1,move2,cc.callFunc(function(){				
					this.flowNext();
				},this));				
			}			
			p.runAction(seq);
		break;
		case TAG_SG:
			var move = cc.moveTo(1.5,cc.p(105,450));
			var move1 = cc.moveTo(1,cc.p(105,380));
			var rota = cc.rotateTo(1,-23);
			var sp = cc.spawn(move1,rota);
			
			var move2 = cc.moveTo(0.5,cc.p(105,290));
			var move3 = cc.moveTo(0.5,cc.p(120,250));
			var move4 = cc.moveTo(1,cc.p(100,100));
			var seq = cc.sequence(move,sp,cc.callFunc(function(){
				this.line.setSpriteFrame("exp10sgline2.png");
				this.line.setPosition(100,280);
				this.line.setRotation(23);
			},this),move2,move3,move4,cc.callFunc(function(){
				this.line.setSpriteFrame("exp10sgline3.png");
				this.line.setPosition(25,218);
				this.line.setRotation(23);
				this.flowNext();
			},this));
			p.runAction(seq);
			
			var sq =cc.sequence(cc.delayTime(1.5),cc.rotateTo(1,23)); 
			this.sgline.runAction(sq);
		break;
		case TAG_XCONFLASK:
			this.conflask.setSpriteFrame("sanjiaoflask2.png");
			this.conflask.setRotationX(180);
			this.conflask.setPosition(190,166);
			
			var rota  = cc.rotateTo(1,180);
			var seq = cc.sequence(cc.callFunc(this.addpaomo(),this),rota,cc.callFunc(function(){
				
				
				this.conflask1.getChildByTag(TAG_LID).setSpriteFrame("sanjiaolid1.png");
				this.conflask1.getChildByTag(TAG_LID).setRotation(180);
								
				this.line.setSpriteFrame("exp10sgline4.png");
				this.line.setPosition(85,185);
				this.line.setRotation(203);
				
				this.conflaskline.setRotationX(180);
			},this),cc.delayTime(3),cc.callFunc(this.flowNext ,this));
			this.runAction(seq);
			this.sg.runAction(cc.moveTo(1,cc.p(130,230)));
			this.conflaskline.runAction(cc.spawn(cc.moveTo(1,cc.p(110,155)),cc.scaleTo(1,0.8)));
		break;

		}
	},
	flowNext:function(){
		gg.flow.next();
	},
});