exp03.ShowMainLayer01 = cc.Layer.extend({
	curIndex: 0,
	ctor:function () {
		this._super();
		this.init();
		return true;
	},
	init:function(){
		var sv = new pub.ScrollView(this);
		sv.setOffestPos(cc.p(152,45 + 15));
		this.sv = sv;
	
		this.addlayout1();
		this.addlayout2();				
		this.addlayout3();
	},	
	addlayout1:function(){
		layout = new ccui.Layout();// 第一页
		layout.setContentSize(cc.size(996, 650));// 设置layout的大小
		this.sv.addLayer(layout);

		// º¹²³ ₄ ₃ ₂
		var text = $.format("　　元素符号(symbols for elements)是国际上统一采用的符号,通常用该元素拉丁文的第一个字母大写来表示。当两种元素首字母相同时,则再附上一个小写字母作为元素符号。", 800, 30);
		this.addText(layout, text, cc.p(50, 510));
		
		this.addRichText(layout, "氢        H        ", "H", "ydrogenium", cc.p(300, 400));
		this.addRichText(layout, "碳        C        ", "C", "arbon", cc.p(260, 350));
		this.addRichText(layout, "氧        O        ", "O", "xygenium", cc.p(285, 300));
		this.addRichText(layout, "钠        Na       ", "Na", "trium", cc.p(270, 250));
		this.addRichText(layout, "硫        S        ", "S", "ulphur", cc.p(260, 200));
		this.addRichText(layout, "钙        Ca       ", "Ca", "lcium", cc.p(265, 150));
		this.addRichText(layout, "铜        Cu       ", "Cu", "prum", cc.p(265, 100));
		
		new exp03.CloudLabel(layout, "元素符号一般表示:\n    一种元素\n    该元素的一个原子", 
				exp03.CLOUD_RIGHT_DOWN).setPosition(700, 200);
	},
	addRichText:function(p, t1, t2, t3, pos){
		new pub.RichText(p)
			.append(t1, cc.color(0,0,0))
			.append(t2, cc.color(255,0,0))
			.append(t3, cc.color(0,0,0))
			.setPosition(pos);
	},
	addText:function(p, text, pos){
		var label = new cc.LabelTTF(text, gg.fontName, gg.fontSize4);
		label.setPosition(pos);
		label.setAnchorPoint(0, 0.5);
		label.setColor(cc.color(0, 0, 0, 250));
		p.addChild(label);
		return label;
	},
	addlayout2:function(parent){
		layout = new ccui.Layout();// 第一页
		layout.setContentSize(cc.size(996, 650));// 设置layout的大小
		this.sv.addLayer(layout);

		this.addText(layout, "翻翻看下面的元素", cc.p(50, 580));
		
		var posArr = [];
		for(var i = 0; i < 5; i++){
			var pos = cc.p(100 + 200 * i, 420);
			posArr.push(pos);
		}
		for(var i = 0; i < 5; i++){
			var pos = cc.p(100 + 200 * i, 160);
			posArr.push(pos);
		}
		var nameArr = ["氢","氦","锂","铍","硼","碳","氮","氧","氟","氖"];
		var letterArr = ["H","He","Li","Be","B","C","N","O","F", "Ne"];
		for(var i = 0; i < 10; i++){
			var card = new exp03.Card(layout);
			card.addContent(card.TYPE_STR, letterArr[i], nameArr[i]).setPosition(posArr[i]);	
		}
	},
	addlayout3:function(parent){
		layout = new ccui.Layout();// 第一页
		layout.setContentSize(cc.size(996, 650));// 设置layout的大小
		this.sv.addLayer(layout);
		
		var posArr = [];
		for(var i = 0; i < 5; i++){
			var pos = cc.p(100 + 200 * i, 420);
			posArr.push(pos);
		}
		for(var i = 0; i < 5; i++){
			var pos = cc.p(100 + 200 * i, 160);
			posArr.push(pos);
		}
		var nameArr = ["钠","镁","铝","硅","磷","硫","氯","氩"];
		var letterArr = ["Na","Mg","Al","Si","P","S","Cl","Ar"];
		for(var i = 0; i < 8; i++){
			var card = new exp03.Card(layout);
			card.addContent(card.TYPE_STR, letterArr[i], nameArr[i]).setPosition(posArr[i]);	
		}
	},
	addbutton:function(parent,tag,str,pos){
		var button = new ButtonScale(parent,"#unsel.png",this.callback,this);
		button.setPosition(pos);
		button.setTag(tag);
		var label = new cc.LabelTTF(str,gg.fontName,30);
		label.setColor(cc.color(0, 0, 0, 250));
		label.setPosition(button.width/2,button.height/2);		
		button.addChild(label);
	},
	
	callback:function(p){
		switch(p.getTag()){	
		case TAG_BUTTON:
		break;
		case TAG_BUTTON1:
			break;
		case TAG_MENU2:
			gg.runNext(p.getTag());
			break;
		
		}
	}

});
