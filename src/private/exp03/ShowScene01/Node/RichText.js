exp03.RichText = cc.Node.extend({
	ctor:function(p){
		this._super();
		p.addChild(this);
		this.init();
		return this;
	},
	init:function(){
		this.node = new cc.Node();
		this.addChild(this.node);
		this.fontName = gg.fontName; 
		this.fontSize = gg.fontSize;
	},
	setAPoint:function(x, y){
		this.richText.setAnchorPoint(cc.p(x, y));
	},
	append:function(text, color){
		var label = new cc.LabelTTF(text, this.fontName, this.fontSize);
		label.setColor(color);
		this.node.addChild(label);
	}
});