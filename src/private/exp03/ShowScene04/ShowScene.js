exp03.ShowLayer04 = cc.Layer.extend({
	backgroundLayer : null,
	mainLayar : null,
	ctor:function (menuTag) {
		this._super();
		this.initFrames();
		this.loadBackground(menuTag);
		this.loadMainLayer();
	},
	initFrames : function(){
		cc.spriteFrameCache.addSpriteFrames(exp03.res_show.show_p);
		gg.curRunSpriteFrame.push(exp03.res_show.show_p);
	},
	loadBackground : function(menuTag){
		this.backgroundLayer = new ShowBackgroundLayer(menuTag);
		this.addChild(this.backgroundLayer);
	},
	loadMainLayer : function(){
		this.mainLayar = new exp03.ShowMainLayer04();
		this.addChild(this.mainLayar);
	}
});
exp03.ShowScene04 = PScene.extend({
	ctor:function(menuTag){
		this._super();
		this.menuTag=menuTag;
	},
	onEnter:function () {
		this._super();
		var layer = new exp03.ShowLayer04(this.menuTag);
		this.addChild(layer);
	}
});
