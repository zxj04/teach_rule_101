TAG_PROBLEM1 = 6001;
TAG_PROBLEM2 = 6002;
exp03.ShowMainLayer04 = cc.Layer.extend({
	lead:null,
	real:null,
	ctor:function () {
		this._super();
		this.init();
		return true;
	},
	init:function(){//
		this.sv = new pub.ScrollView(this);
		this.sv.setOffestPos(cc.p(152,45 + 15));
		
		this.addlayout();
		this.addlayout2();
	},
	addlayout:function(){
		layout = new ccui.Layout();// 第一页
		layout.setContentSize(cc.size(996, 650));// 设置layout的大小
		this.sv.addLayer(layout);

		var text1 = $.format("下列元素符号书写正确的是", 980,30);
		var label1 = new cc.LabelTTF(text1,gg.fontName,gg.fontSize4);
		label1.setColor(cc.color(0, 0, 0, 250));
		label1.setAnchorPoint(0, 0.5);
		label1.setPosition(50, layout.height - label1.height / 2-50);
		layout.addChild(label1);
		
		var card = new exp03.Card(layout);
		var node1 = new cc.Node();
		var xushu = new cc.LabelTTF("12", gg.fontName, gg.fontSize);
		xushu.setPosition(-40, 70);
		xushu.setColor(cc.color(0,0,0));
		node1.addChild(xushu);
		var fuhao = new cc.LabelTTF("Mg", gg.fontName, gg.fontSize);
		fuhao.setPosition(40, 70);
		fuhao.setColor(cc.color(0,0,0));
		node1.addChild(fuhao);
		var name = new cc.LabelTTF("镁", gg.fontName, gg.fontSize);
		name.setColor(cc.color(0,0,0));
		node1.addChild(name);
		var yuanziliang = new cc.LabelTTF("24", gg.fontName, gg.fontSize);
		yuanziliang.setPosition(0, -70);
		yuanziliang.setColor(cc.color(0,0,0));
		node1.addChild(yuanziliang);
		var node2 = new cc.LabelTTF("沃赛出品\n必属精品", gg.fontName, gg.fontSize);
		node2.setColor(cc.color(0,0,0));
		card.addContent(card.TYPE_NODE, node1, node2).setPosition(800, 400);

		this.objArr1 = [];
		this.loadSelect(layout, "A．MG","#wrong.png", cc.p(50,500), TAG_PROBLEM1, this.objArr1);
		this.loadSelect(layout, "B．mG","#wrong.png", cc.p(50,460), TAG_PROBLEM1, this.objArr1);
		this.loadSelect(layout, "C．mg","#wrong.png", cc.p(50,420), TAG_PROBLEM1, this.objArr1);
		this.loadSelect(layout, "D．Mg","#right.png", cc.p(50,380), TAG_PROBLEM1, this.objArr1);

		var text2 = $.format("解答:元素符号(symbols for elements)是国际上统一采用的符号,通常用该元素拉丁文的第一个字母大写来表示。" +
				"当两种元素首字母相同时,则再附上一个小写字母作为元素符号。" +
				"题中只有D是首字母大写,第二个字母小写。" +
				"故选：D", 800,30);
		this.loadResult(layout, text2, cc.p(50,250), this.objArr1);
	},
	addlayout2:function(){
		layout = new ccui.Layout();// 第一页
		layout.setContentSize(cc.size(996, 650));// 设置layout的大小
		this.sv.addLayer(layout);

		var text1 = $.format("下列各组元素中,元素符号的小写字母相同", 980,30);
		var label1 = new cc.LabelTTF(text1,gg.fontName,gg.fontSize4);
		label1.setColor(cc.color(0, 0, 0, 250));
		label1.setAnchorPoint(0, 0.5);
		label1.setPosition(50, layout.height - label1.height / 2-50);
		layout.addChild(label1);

		this.objArr2 = [];
		this.loadSelect(layout, "A．钠、镁、铝","#wrong.png", cc.p(50,500), TAG_PROBLEM2, this.objArr2);
		this.loadSelect(layout, "B．氮、氖、汞","#wrong.png", cc.p(50,460), TAG_PROBLEM2, this.objArr2);
		this.loadSelect(layout, "C．镁、银、汞","#right.png", cc.p(50,420), TAG_PROBLEM2, this.objArr2);
		this.loadSelect(layout, "D．锰、铜、钠","#wrong.png", cc.p(50,380), TAG_PROBLEM2, this.objArr2);

		var text2 = $.format("解答:A的元素符号是Na Mg Al  " +
				"B的元素符号是N Ne Hg  " +
				"C的元素符号是Mg Ag Hg  " +
				"D的元素符号是Mn Cu Na  " +
				"C中所有的小写字母都是g,故选：C", 800,30);
		this.loadResult(layout, text2, cc.p(50,250), this.objArr2);
	},
	loadSelect:function(layout, str,answer, pos, tag, objArr){
		var button=new Angel(layout,"#select.png",this.callback,this);
		button.setPosition(pos);
		button.setTag(tag);
		var label = new cc.LabelTTF(str,"微软雅黑",30);
		label.setPosition(pos.x+button.width*0.5+label.width*0.5+10,pos.y);
		label.setColor(cc.color(0, 0, 0, 250));
		layout.addChild(label);
		var answer=new cc.Sprite(answer);
		answer.setPosition(pos);
		answer.setVisible(false);
		layout.addChild(answer);
		objArr.push(answer)
	},
	loadResult:function(layout, text, pos, objArr){
		var label = new cc.LabelTTF(text, gg.fontName, gg.fontSize4);
		label.setVisible(false);
		label.setColor(cc.color(0, 0, 0, 300));
		label.setAnchorPoint(0, 1);
		label.setPosition(pos);
		layout.addChild(label);
		objArr.push(label)
	},
	callback:function(p){
		var objArr = [];
		switch (p.getTag()) {
		case TAG_PROBLEM1:
			objArr = this.objArr1;
			break;
		case TAG_PROBLEM2:
			objArr = this.objArr2;
			break;
		default:
			break;
		}
		for(var i = 0; i < objArr.length; i++){
			var obj = objArr[i];
			obj.setVisible(true);
		}
	}
});
