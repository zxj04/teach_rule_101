exp03.ShowMainLayer02 = cc.Layer.extend({
	curIndex: 0,
	ctor:function () {
		this._super();
		this.init();
		return true;
	},
	init:function(){
		var sv = new pub.ScrollView(this);
		sv.setOffestPos(cc.p(152,45 + 15));
		this.sv = sv;

		this.addlayout1(sv);
		this.addlayout2(sv);				
		this.addlayout3(sv);
	},
	addlayout1:function(parent){
		layout = new ccui.Layout();// 第一页
		layout.setContentSize(cc.size(996, 650));// 设置layout的大小
		this.sv.addLayer(layout);
		
		var text = $.format("　　为了便于研究元素的性质和用途," +
				"科学家把所有已知元素科学有序地\n排列起来," +
				"这样就得到了元素周期表(periodic table if elements)", 980, 30);
		this.addText(layout, text, cc.p(50, 550));
		
		var element = new cc.Sprite("#element.png");
		element.setPosition(500, 256);
		layout.addChild(element);

		text = "我有7行18列";
		new exp03.CloudLabel(layout, text, exp03.CLOUD_LEFT_UP).setPosition(250, 400);

		text = "一行一周期\n一列一大族";
		new exp03.CloudLabel(layout, text, exp03.CLOUD_RIGHT_UP).setPosition(760, 400);

		text = "为什么我只有15族?";
		new exp03.CloudLabel(layout, text, exp03.CLOUD_LEFT_DOWN).setPosition(250, 100);

		text = "8,9,10三列\n合并成一组了";
		new exp03.CloudLabel(layout, text, exp03.CLOUD_RIGHT_DOWN).setPosition(760, 100);

	},
	addText:function(p, text, pos){
		var label = new cc.LabelTTF(text, gg.fontName, gg.fontSize4);
		label.setPosition(pos);
		label.setAnchorPoint(0, 0.5);
		label.setColor(cc.color(0, 0, 0, 250));
		p.addChild(label);
		return label;
	},
	addlayout2:function(parent){
		layout = new ccui.Layout();// 第一页
		layout.setContentSize(cc.size(996, 650));// 设置layout的大小
		this.sv.addLayer(layout);
		
		
		var card = new exp03.Card(layout);
		var node1 = new cc.Node();
		var xushu = new cc.LabelTTF("92", gg.fontName, gg.fontSize);
		xushu.setPosition(-40, 70);
		xushu.setColor(cc.color(0,0,0));
		node1.addChild(xushu);
		var fuhao = new cc.LabelTTF("U", gg.fontName, gg.fontSize);
		fuhao.setPosition(40, 70);
		fuhao.setColor(cc.color(0,0,0));
		node1.addChild(fuhao);
		var name = new cc.LabelTTF("铀", gg.fontName, gg.fontSize);
		name.setColor(cc.color(0,0,0));
		node1.addChild(name);
		var yuanziliang = new cc.LabelTTF("238", gg.fontName, gg.fontSize);
		yuanziliang.setPosition(0, -70);
		yuanziliang.setColor(cc.color(0,0,0));
		node1.addChild(yuanziliang);
		var node2 = new cc.LabelTTF("沃赛出品\n必属精品", gg.fontName, gg.fontSize);
		node2.setColor(cc.color(0,0,0));
		card.addContent(card.TYPE_NODE, node2, node1).setPosition(500, 300);
		
		
		this.addText(layout, "原子序数", cc.p(280, 370));
		this.addText(layout, "元素符号、红色指放射性元素", cc.p(600, 370));
		this.addText(layout, "元素名称(注*的是人造元素)", cc.p(40, 300));
		this.addText(layout, "原子量", cc.p(600, 230));
		
		this.addText(layout, "元素周期表中的信息", cc.p(370, 100));
	},
	addlayout3:function(parent){
		layout = new ccui.Layout();// 第一页
		layout.setContentSize(cc.size(996, 650));// 设置layout的大小
		this.sv.addLayer(layout);

		text = "元素周期表的排\n列告诉我们什么?";
		new exp03.CloudLabel(layout, text, exp03.CLOUD_LEFT_UP).setPosition(150, 350);
		
		this.addText(layout, "元素单质的物理状态", cc.p(350, 500));
		this.addText(layout, "是人工合成还是自然存在", cc.p(350, 400));
		this.addText(layout, "是金属、非金属还是稀有气体", cc.p(350, 300));
		this.addText(layout, "元素周期数等于核外电子层数", cc.p(350, 200));
		this.addText(layout, "主族元素的序数等于最外层电子数", cc.p(350, 100));
		
	},
	
	addimage:function(parent,str,pos){
		var image = new cc.Sprite(str);
		parent.addChild(image);
		image.setPosition(pos);
	},
	addlabel:function(parent,str,pos){
		var label = new  cc.LabelTTF(str,gg.fontName,gg.fontSize4);
		label.setPositon(pos);
		parent.addChild(label,5);		
	},
	addbutton:function(parent,tag,str,pos,labelx){
		var button = new ButtonScale(parent,"#unsel.png",this.callback,this);
		button.setPosition(pos);
		button.setTag(tag);
		var label = new cc.LabelTTF(str,gg.fontName,30);
		label.setPosition(button.width/2,button.height/2);		
		button.addChild(label);
	},
	callback:function(p){
		switch(p.getTag()){	
		case TAG_MENU3:
			gg.runNext(p.getTag());
		break;
		
		}
	}

});
