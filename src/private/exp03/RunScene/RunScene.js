exp03.RunLayer01 = cc.Layer.extend({
	backgroundLayer : null,
	mainLayar : null,
	ctor:function () {
		this._super();
		this.initFrames();
		this.loadBackground();
		this.loadMainLayer();
	},
	initFrames : function(){
		cc.spriteFrameCache.addSpriteFrames(res_run03.run_p);
		gg.curRunSpriteFrame.push(res_run03.run_p);
	},
	loadBackground : function(){
		this.backgroundLayer = new RunBackgroundLayer();
		this.addChild(this.backgroundLayer);
	},
	loadMainLayer : function(){
		this.mainLayar = new exp03.RunMainLayer01();
		this.addChild(this.mainLayar);
	}
});

exp03.RunScene01 = PScene.extend({
	onEnter:function () {
		this._super();
		gg.initTeach();
		gg.scoreCheck = true;
		var layer = new exp03.RunLayer01();
		this.addChild(layer);
	}
});
