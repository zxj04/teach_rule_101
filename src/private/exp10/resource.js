var exp10 = exp10||{};
//start
exp10.res_start = {
	//	start_png :"res/private/exp10/start/startpng.png"
};

exp10.g_resources_start = [];
for (var i in exp10.res_start) {
	exp10.g_resources_start.push(exp10.res_start[i]);
}

// run
exp10.res_run = {
		run_p1 : "res/private/exp10/run.plist",
		run_g1 : "res/private/exp10/run.png"
};

exp10.g_resources_run = [];
for (var i in exp10.res_run) {
	exp10.g_resources_run.push(exp10.res_run[i]);
}

// show02
exp10.res_show = {
		show_p : "res/private/exp10/show.plist",
		show_g : "res/private/exp10/show.png",
		show1 : "res/private/exp10/show1.jpg",
		show2 : "res/private/exp10/show2.jpg",
		show3 : "res/private/exp10/show3.jpg",
		show11 : "res/private/exp10/show11.jpg",
		show12 : "res/private/exp10/show12.jpg",
		show13 : "res/private/exp10/show13.jpg",
		show14 : "res/private/exp10/show14.jpg",
		show15 : "res/private/exp10/show15.jpg"
};

exp10.g_resources_show = [];
for (var i in exp10.res_show) {
	exp10.g_resources_show.push(exp10.res_show[i]);
}

