var exp10 = exp10||{};
cc.log("exp10 RunLayer01_2");
exp10.RunLayer01_2 = cc.Layer.extend({
		arr:null,
		scene:null,
		clock:null,
		ctor:function (parent) {
			this._super();
			this.scene = parent;
			this.scene.addChild(this, 10);
			gg.main = this;
			this.init();
		},
		init:function () {
			this.callNext = cc.callFunc(function(){
				gg.flow.next();
			}, this);
			this.callKill = cc.callFunc(function(p){
				var seq = cc.sequence(cc.fadeOut(0.5),cc.callFunc(function(){
					p.removeFromParent(true);	
				}, this));
				p.runAction(seq);
			}, this); 

			this.callNext.retain();
			this.callKill.retain();
			//时钟
			this.clock = new Clock(this);
			// 物品库
			this.lib = new Lib(this);
			this.lib.loadBg([{tag:TAG_LIB_TURANG, name:"土壤",checkright:true},
			                 {tag:TAG_LIB_BOTTLE, name:"塑料瓶"},
			                 {tag:TAG_LIB_SANJIAOFLASK, name:"三角烧瓶"},
			                 {tag:TAG_LIB_LAMP, name:"酒精灯",checkright:true},
			                 {tag:TAG_LIB_ZHENGFA, name:"蒸发装置",checkright:true},
			                 {tag:TGA_LIB_GLASS, name:"玻璃片"},
			                 {tag:TAG_LIB_TESTTUBE, name:"试管"},
			                 {tag:TAG_LIB_BEAKER, name:"烧杯",checkright:true},
			                 {tag:TAG_LIB_GUOLV, name:"过滤装置",checkright:true},
			                 {tag:TAG_LIB_CONICAL, name:"锥形瓶"}
							]);

			this.zhengfa = new exp10.Zhengfa(this);
			this.zhengfa.setVisible(false);
			this.zhengfa.setPosition(1000,300);

			this.guolv = new exp10.Guolv(this);
			this.guolv.setVisible(false);
			this.guolv.setPosition(350,300);
			//
			this.lamp = new exp10.Lamp(this);
			this.lamp.setVisible(false);
			this.lamp.setPosition(940,160);

			this.beaker = new exp10.Beaker(this);
			this.beaker.setVisible(false);
			this.beaker.setPosition(400,150);
		},
		checkVisible:function(next){
			//是否可见
			var checkVisible = [];
			var node1 = ll.run.zhengfa;
			var node2 = ll.run.guolv;
			var node3 = ll.run.lamp;
			var node4 = ll.run.beaker;

			checkVisible.push(node1,node2,node3,node4);

			for(var i in checkVisible){
				if(checkVisible[i] !== null){				
					checkVisible[i].setVisible(next);				
				}			
			}
		},
		loadInLib:function(obj, pos, tarPos,delay){
			obj.setPosition(pos);
			if(delay == null){
				delay = 1;
			}
			var ber = $.bezier(pos, tarPos, delay);
			var seq = cc.sequence(ber, this.callNext);
			obj.runAction(seq);
		},
		kill:function(obj){
			var fade = cc.fadeTo(0.5,0);
			var func = cc.callfunc(function(){
				obj.removeFromParent(true);
			},this);
			var seq = cc.sequence(fade,func);
			obj.runAction(seq)
		},

		callback:function (p){
			var func = cc.callFunc(this.actionDone, this);
			var action=gg.flow.flow.action;
			switch(p.getTag()){

			}
		},
		actionDone:function(p){
			var func = cc.callFunc(this.actionDone, this);
			switch(p.getTag()){

			}
		},
		flowNext:function(){
			gg.flow.next();
		},
		onExit:function(){
			this._super();
			this.callNext.release();
			this.callKill.release();
		}

});