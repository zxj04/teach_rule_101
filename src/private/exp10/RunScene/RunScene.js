var exp10 = exp10||{};
cc.log("exp10 RunLayer01");
exp10.RunLayer01 =  cc.Layer.extend({
	backgroundLayer : null,
	mainLayar : null,
	ctor:function () {
		this._super();
		this.initFrames();
		this.loadBackground();
		this.loadMainLayer();
	},
	initFrames : function(){
		cc.spriteFrameCache.addSpriteFrames(exp10.res_run.run_p1);
		gg.curRunSpriteFrame.push(exp10.res_run.run_p1);
	},
	loadBackground : function(){
		this.backgroundLayer = new RunBackgroundLayer();
		this.addChild(this.backgroundLayer);
	},
	loadMainLayer : function(){
		this.mainLayar = new exp10.RunMainLayer01();
		this.addChild(this.mainLayar);
	}
});

exp10.RunScene01 = PScene.extend({
	onEnter:function () {
		this._super();
		gg.initTeach();
		var layer = new exp10.RunLayer01();
		this.addChild(layer);
	}
});
