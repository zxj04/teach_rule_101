exp10.Beaker = cc.Node.extend({
	ctor:function(p){
		this._super();
		p.addChild(this, 10, TAG_BEAKER_NODE);
		this.init();
	},
	init : function(){
		this.setCascadeOpacityEnabled(true);
		this.beaker= new Button(this, 5, TAG_BEAKER, "#beaker153.png",this.callback);
		this.beaker.setScale(0.6);
		this.beaker.setPosition(10,0);
		
		this.beaker1= new cc.Sprite("#beaker154.png");
		this.beaker.addChild(this.beaker1,5);
		this.beaker1.setPosition(103,106);
		
		
		this.beaker2= new Button(this, 1, TAG_BEAKER2, "#beaker151.png",this.callback);
		this.beaker2.setScale(0.6);
		this.beaker2.setPosition(280,100);
		
		this.beaker3= new cc.Sprite("#beaker152.png");
		this.addChild(this.beaker3,10);
		this.beaker3.setPosition(280,101);
		this.beaker3.setScale(0.6);

		this.beakerline= new cc.Sprite("#exp15line.png");		
		this.beakerline.setPosition(102,100);
		this.beaker2.addChild(this.beakerline,10);
		
		this.beakerline1= new cc.Sprite("#exp15line.png");		
		this.beakerline1.setPosition(95,20);
		this.beakerline1.setVisible(false);
		this.beaker.addChild(this.beakerline1,10);

		this.rod= new Button(this, 3, TAG_ROD, "#rod.png",this.callback);
		this.rod.setScale(0.5);
		this.rod.setRotation(80);
		this.rod.setPosition(250,156);
		
		this.paper= new cc.Sprite("#zhi15.png");		
		this.paper.setPosition(180,350);
		this.addChild(this.paper,1,TAG_PAPER);
		
		this.turang= new Button(this, 5, TAG_TURANG, "#turang.png",this.callback);
		this.turang.setScale(0.5);
		this.turang.setPosition(180,360);
		

	},
	addturang :function(){
		var turang = new cc.Sprite("#turang1.png");
		turang.setPosition(cc.p(95,70));
		turang.setOpacity(0);
		this.beaker3.addChild(turang,1,TAG_SHOW);
		var move = cc.moveTo(1,cc.p(95,50));
		var fadein = cc.fadeIn(0.6);
		var sp = cc.spawn(move,fadein);
		var seq = cc.sequence(sp,cc.delayTime(1),cc.fadeOut(0.5));
		turang.runAction(seq);
	},
	addturang1 :function(){
		var turang = new cc.Sprite("#turang2.png");
		turang.setPosition(cc.p(95,20));
		turang.setOpacity(0);
		this.beaker3.addChild(turang,1,TAG_SHOW1);
		var fadein = cc.fadeIn(1);
		turang.runAction(fadein);
	},
	addhunzuo :function(){
		var hunzuo = new cc.Sprite("#turanghuzuo.png");
		hunzuo.setPosition(cc.p(95,60));
		hunzuo.setOpacity(0);
		this.beaker3.addChild(hunzuo,2,TAG_SHOW2);
		var fade = cc.fadeTo(1,80);
		var seq = cc.sequence(cc.delayTime(0.5),fade);
		hunzuo.runAction(seq);
	},
	addhunzuo1 :function(){
		var hunzuo = new cc.Sprite("#turang4.png");
	    hunzuo.setPosition(cc.p(95,40));
	    hunzuo.setOpacity(0);
	    this.beaker3.addChild(hunzuo,1,TAG_SHOW3);
	    var fade = cc.fadeTo(1,80);
    	var seq = cc.sequence(cc.delayTime(0.5),fade);
    	hunzuo.runAction(seq);
	},
	daoshui:function(){
		var frames=[];
		for(var i=1;i<12;i++){
			var str ="exp15beaker1/"+i+".png";
			var frame=cc.spriteFrameCache.getSpriteFrame(str);
			frames.push(frame);
		}

		var animation = new cc.Animation(frames,0.1);//负责动画序列
		var action = new cc.Animate(animation);//帧动画的动作创建
		return action;
	},
	callback:function(p){
		var action = gg.flow.flow.action;
		var func = cc.callFunc(this.actionDone, this);	
		switch(p.getTag()){		
		case TAG_TURANG:
			var move = cc.moveTo(0.5,cc.p(280,300));
			var move1 = cc.moveTo(1,cc.p(285,65));
			var fade = cc.fadeTo(1,200);
			var seq = cc.sequence(move,move1,cc.callFunc(function(){
				this.addturang();
				this.addturang1();
				this.addhunzuo();				
			},this),fade,cc.callFunc(function(){
				this.paper.removeFromParent(true);
				this.flowNext(); 
			},this));
			p.runAction(seq);
            break;
		case TAG_ROD:			
			if(action ==ACTION_DO1){
				var rota = cc.rotateTo(0.3,90);
				var move = cc.moveTo(0.3,cc.p(260,165));
				var sp = cc.spawn(rota,move);
				var move1 = cc.moveTo(0.3,cc.p(300,165));
				var sp1 = cc.spawn(cc.moveTo(0.3,cc.p(250,156)),cc.rotateTo(0.3,80));
				var seq = cc.sequence(sp,cc.callFunc(function(){
					this.addhunzuo1();
					this.beaker3.getChildByTag(TAG_SHOW2).runAction(cc.fadeTo(2,200));					
					this.turang.runAction(cc.fadeTo(2,0));
					this.beaker3.getChildByTag(TAG_SHOW).runAction(cc.fadeTo(2,0));
					this.beaker3.getChildByTag(TAG_SHOW1).runAction(cc.fadeTo(2,0));					
				},this),move1,move,move1,move,move1,sp1,cc.callFunc(function(){
					ll.run.clock.setSpriteFrame("clockcd.png");
					ll.run.clock.doing();
					this.beaker3.getChildByTag(TAG_SHOW2).runAction(cc.fadeTo(4,150));
					this.beaker3.getChildByTag(TAG_SHOW3).runAction(cc.fadeTo(4,200));					
				},this),cc.delayTime(4),cc.callFunc(function(){
					ll.run.clock.stop();
					this.flowNext();
				},this));				
			}else if(action ==ACTION_DO2){
				this.showtip = new ShowTip("使用玻璃棒搅拌，防止局" +
						"\n部温度过高导致液体飞溅",cc.p(750,450));
				var rota = cc.rotateTo(1,90);
				var move = cc.moveTo(1,cc.p(500,280));
				var sp = cc.spawn(rota,move);
				var move1 = cc.moveTo(0.3,cc.p(550,266));
				var move2 = cc.moveTo(0.3,cc.p(575,262));
				var move3 = cc.moveTo(0.3,cc.p(600,260));
				var move4 = cc.moveTo(0.3,cc.p(625,265));
				var seq = cc.sequence(sp,move1,move2,move3,move4,move3,move2,move1,move2,move3,move4,move3,move2,cc.callFunc(function(){
					this.removeFromParent(true);
					this.flowNext();
				},this));				
			}					
			p.runAction(seq);
	     	break;
		case TAG_BEAKER2:
			var move = cc.moveTo(0.5,cc.p(220,280));
			var move1 = cc.moveTo(1,cc.p(0,350));
			var move2 = cc.moveTo(0.5,cc.p(-80,300));
			var rota = cc.rotateTo(0.5,70);
			var sp = cc.spawn(move2,rota);
			var move3 = cc.moveTo(1,cc.p(250,-50));
			var rota1 = cc.rotateTo(1,0);
			var sp1 =cc.spawn(move3,rota1);
			var seq = cc.sequence(move,move1,sp,cc.delayTime(3),move1,sp1,cc.callFunc(function(){
				this.flowNext();
			},this));
			this.rod.runAction(seq);
			
			this.beaker2.removeFromParent(true);
			this.beaker3.removeFromParent(true);
			var beaker = new cc.Sprite("#exp15beaker1/1.png");
			beaker.setScale(0.6);
			beaker.setPosition(280,101);
			this.addChild(beaker);
			var mov = cc.moveTo(1.5,cc.p(-20,320));
			var rot = cc.rotateTo(1,-100);
			var action = this.daoshui();
			var sp2 = cc.spawn(rot,action);
			var se = cc.sequence(cc.delayTime(1.5),mov,sp2,cc.callFunc(function(){
				this.beakerline1.setVisible(true);
				this.beakerline1.runAction(cc.moveTo(1,cc.p(95,70)));
			},this),cc.delayTime(1),cc.callFunc(function(){
				beaker.removeFromParent(true);
			},this));
		    beaker.runAction(se);						
		break;
		case TAG_BEAKER:
			var move = cc.moveTo(0.5,cc.p(30,120));		
			ll.run.guolv.guolv.runAction(move);
			
			var mov = cc.moveTo(1.5,cc.p(470,240));
			var rota = cc.rotateTo(1,90);
			var seq = cc.sequence(mov,cc.callFunc(function(){
				var rot = cc.rotateTo(0.5,-40);
				var move1 = cc.moveTo(1,cc.p(150,100));
				var rot1 = cc.rotateTo(1,-90);
				var sca = cc.scaleTo(1,1.2,1);
				var sp = cc.spawn(move1,rot1,sca);
				
				var move2 = cc.moveTo(0.5,cc.p(150,180));
				var sca1 = cc.scaleTo(0.5,0.1);
				var sp1 = cc.spawn(move2,sca1);
				var se = cc.sequence(rot,sp,sp1);
				this.beakerline1.runAction(se);
			},this),rota,cc.delayTime(1),cc.callFunc(function(){
				p.removeFromParent(true);
				this.flowNext();
			},this));
			p.runAction(seq);
		break;
			
		}
	},
	flowNext:function(){
		gg.flow.next();
	},
});