exp10.Guolv = cc.Node.extend({
	ctor:function(p){
		this._super();
		p.addChild(this, 10, TAG_GUOLV_NODE);
		this.init();
	},
	init : function(){
		this.setCascadeOpacityEnabled(true);
		this.iron= new Button(this, 5, TAG_IRON, "#exp15iron1.png",this.callback);
		this.iron.setScale(0.8);

		this.guolv= new Button(this, 7, TAG_GUOLV, "#guolv2.png",this.callback);
		this.guolv.setScale(0.8);
		this.guolv.setPosition(30,60);

		this.guolv1= new cc.Sprite("#guolv1.png");
		this.guolv1.setPosition(15,-113.2);
		this.guolv.addChild(this.guolv1,5);

	},

	callback:function(p){
		var action = gg.flow.flow.action;
		var func = cc.callFunc(this.actionDone, this);	
		switch(p.getTag()){		
		}
	},
	flowNext:function(){
		gg.flow.next();
	},
});