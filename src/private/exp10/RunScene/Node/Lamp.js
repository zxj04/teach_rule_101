exp10.Lamp = cc.Node.extend({
	ctor:function(p){
		this._super();
		p.addChild(this, 10, TAG_LAMP_NODE);
		this.init();
	},
	init : function(){
		this.setCascadeOpacityEnabled(true);
		this.lamplid= new Button(this, 11, TAG_LAMP_LID, "#exp15lamp/lampLid.png",this.callback);
		this.lamplid.setPosition(0,40);
		this.lamplid.setScale(0.4);


		this.lamp= new Button(this, 10, TAG_LAMP, "#exp15lamp/lampBottle.png",this.callback);
		this.lamp.setPosition(0,0);
		this.lamp.setScale(0.4);


		this.match= new Button(this, 1, TAG_MATCH, "#exp15lamp/libmatch.png",this.callback);
		this.match.setScale(0.3);
		this.match.setPosition(100,-40);
		
		var match = new cc.Sprite("#exp15lamp/match1.png");
		match.setPosition(400,375);
		//match.setScale(0.3);
		match.setAnchorPoint(1,1);
		match.setVisible(false);
		this.match.addChild(match,2,TAG_SHOW);
		

	},
	fire:function(){
		var frames=[];
		for (i=1;i<=3;i++){
			var str ="exp15lamp/"+i+".png";
			var frame=cc.spriteFrameCache.getSpriteFrame(str);
			frames.push(frame);
		}
		var animation = new cc.Animation(frames,0.05);//负责动画序列
		var action = new cc.Animate(animation);//帧动画的动作创建
		
		var fire = new cc.Sprite("#exp15lamp/1.png");
		fire.setPosition(165,295);
		fire.setAnchorPoint(0.5,0);
		fire.setScale(3.3);
		ll.run.lamp.lamp.addChild(fire,1,TAG_SHOW);
		fire.runAction(action.repeatForever());
	},

	callback:function(p){
		var action = gg.flow.flow.action;
		var func = cc.callFunc(this.actionDone, this);	
		switch(p.getTag()){		
		case TAG_MATCH:
			var match = new cc.Sprite("#exp15lamp/match1.png");
			match.setPosition(-20,170);
			match.setScale(0.3);
			match.setAnchorPoint(1,1);
			this.addChild(match,2);
			var seq = cc.sequence(cc.moveTo(0.3,cc.p(570,290)),cc.callFunc(function(){
				match.setSpriteFrame("exp15lamp/match2.png");
			},this),cc.moveTo(0.8,cc.p(75,90)),cc.callFunc(function(){
				this.fire();
				match.removeFromParent(true);
				gg.flow.next();
			},this));

			match.runAction(seq);
			break;
		case TAG_LAMP:
			if (action == ACTION_DO1){
				var se = cc.sequence(cc.moveTo(1,cc.p(750,200))); //整体动
				this.runAction(se);

				var seq = cc.sequence(cc.delayTime(1),cc.moveTo(0.5,cc.p(0,80)),cc.moveTo(0.5,cc.p(-80,40)),cc.moveTo(0.5,cc.p(-90,-40)));	
				this.lamplid.runAction(seq);

				var sq = cc.sequence(cc.delayTime(2.5),cc.callFunc(function(){
					this.match.getChildByTag(TAG_SHOW).setVisible(true);
				},this),cc.moveTo(0.3,cc.p(570,290)),cc.callFunc(function(){
					this.match.getChildByTag(TAG_SHOW).setSpriteFrame("exp15lamp/match2.png");
				},this),cc.moveTo(0.8,cc.p(75,560)),cc.callFunc(function(){
					this.fire();
					this.match.removeFromParent(true);
				},this));
				this.match.getChildByTag(TAG_SHOW).runAction(sq);

			    var sq1 = cc.sequence(cc.delayTime(3.6),cc.moveTo(1,cc.p(230,-40)));
			    p.runAction(sq1);
			    
			    var sq2 = cc.sequence(cc.delayTime(4.6),cc.moveTo(0.5,cc.p(30,0)),cc.callFunc(this.flowNext, this));
			    ll.run.zhengfa.zhengfa.runAction(sq2);
				
				
			}else if(action == ACTION_DO2){
				var move = cc.moveTo(1,cc.p(0,0));
				p.runAction(move);
				
				var seq = cc.sequence(cc.delayTime(1),cc.moveTo(0.5,cc.p(-80,40)),cc.moveTo(0.5,cc.p(0,120)),cc.callFunc(function(){
					this.lamp.getChildByTag(TAG_SHOW).runAction(cc.scaleTo(0.5,0));
				},this),cc.moveTo(0.5,cc.p(0,40)),cc.moveTo(0.5,cc.p(0,120)),cc.moveTo(0.5,cc.p(0,40)),
				cc.callFunc(function(){
					//this.removeFromParent(true);
					this.flowNext();
				},this));
				
				this.lamplid.runAction(seq);
			}

		
			break;

		}
	},
	flowNext:function(){
		gg.flow.next();
	},
});