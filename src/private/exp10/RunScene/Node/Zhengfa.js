exp10.Zhengfa = cc.Node.extend({
	ctor:function(p){
		this._super();
		p.addChild(this, 10, TAG_ZHENGFA_NODE);
		this.init();
	},
	init : function(){
		this.setCascadeOpacityEnabled(true);
		this.iron= new Button(this, 5, TAG_IRON, "#exp15iron2.png",this.callback);
		this.iron.setScale(0.8);

		this.zhengfa= new Button(this, 7, TAG_ZHENGFA, "#zhengfa1.png",this.callback);
		this.zhengfa.setScale(0.8);
		this.zhengfa.setPosition(30,25);	
	},
	callback:function(p){
		var action = gg.flow.flow.action;
		var func = cc.callFunc(this.actionDone, this);	
		switch(p.getTag()){	
		case TAG_ZHENGFA:
			var wujiyan = new cc.Sprite("#wujiyan.png");
			wujiyan.setPosition(-150,50);
			wujiyan.setScale(0.6);
			this.addChild(wujiyan);
			this.showtip = new ShowTip("白色晶体为无机盐,说" +
					"\n明土壤中含有无机盐",cc.p(850,450));
			var seq = cc.sequence(cc.delayTime(5),cc.callFunc(this.flowNext ,this));
			p.runAction(seq);
		break;
		}
	},
	flowNext:function(){
		gg.flow.next();
	},
});