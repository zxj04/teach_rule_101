var exp10 = exp10||{};
exp10.startRunNext = function(tag){
	switch(tag){// 具体哪个菜单执行什么操作，这里修改
	case TAG_MENU1:
		pub.ch.gotoShow(TAG_EXP_10, tag, exp10.g_resources_show, null, new exp10.ShowScene01(tag));
		break;
	case TAG_MENU2:
		pub.ch.gotoShow(TAG_EXP_10, tag, exp10.g_resources_show, null, new exp10.ShowScene02(tag));
		break;
	case TAG_MENU3:
		pub.ch.gotoShow(TAG_EXP_10, tag, exp10.g_resources_show, null, new exp10.ShowScene03(tag));
		break;
	case TAG_MENU4:
		gg.teach_type = TAG_LEAD;
		gg.teach_flow = exp10.teach_flow01;
		pub.ch.gotoRun(TAG_EXP_10, tag, exp10.g_resources_run, null, new exp10.RunScene01());
		break;
	case TAG_MENU5:
		pub.ch.gotoShow(TAG_EXP_10, tag, exp10.g_resources_show, null, new exp10.ShowScene04(tag));
		break;
	case TAG_ABOUT:
		pub.ch.gotoAbout(TAG_EXP_10, g_resources_public_about, null, res_public_about.about_j);
		break;
	}
}

// 任务流
exp10.teach_flow01 = 
	[
	 {
		 tip:"选择土壤",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG1,
		      ],
		      canClick:true		 
	 },
	 {
		 tip:"选择酒精灯",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG4,	   
		      ],
		      canClick:true		 
	 },
	 {
		 tip:"选择蒸发装置",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG5,	   
		      ],
		      canClick:true		 
	 },
	 {
		 tip:"选择过滤装置",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG9,	   
		      ],
		      canClick:true		 
	 },
	 {
		 tip:"选择烧杯",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG8,	   
		      ],
		      canClick:true		 
	 },


	 {
		 tip:"点击确定，开始实验",
		 tag:[
		      TAG_LIB,
		      TAG_SURE
		      ]
	 },
	 {
		 tip:"将土块放到烧杯中",
		 tag:[
		      TAG_BEAKER_NODE,
		      TAG_TURANG
		      ]
	 },	 
	 {
		 tip:"玻璃棒搅拌，使土块溶解，静置片刻",
		 tag:[
		      TAG_BEAKER_NODE,
		      TAG_ROD
		      ],action:ACTION_DO1
	 },	
	 {
		 tip:"使用玻璃棒引流，通过过滤漏斗过滤",
		 tag:[
		      TAG_BEAKER_NODE,
		      TAG_BEAKER2
		      ]
	 },	
	 {
		 tip:"取出烧杯，将溶液倒至蒸发皿",
		 tag:[
		      TAG_BEAKER_NODE,
		      TAG_BEAKER
		      ]
	 },	
	 {
		 tip:"点燃酒精灯，加热",
		 tag:[
		      TAG_LAMP_NODE,
		      TAG_LAMP
		      ],action:ACTION_DO1
	 },

	 {
		 tip:"使用玻璃棒搅拌，防止局部温度过高导致液体飞溅",
		 tag:[
		      TAG_BEAKER_NODE,
		      TAG_ROD
		      ],action:ACTION_DO2
	 },
	 {
		 tip:"待蒸发皿中较多固体时，停止加热",
		 tag:[
		      TAG_LAMP_NODE,
		      TAG_LAMP
		      ],action:ACTION_DO2
	 },
	 {
		 tip:"观察蒸发皿",
		 tag:[
		      TAG_ZHENGFA_NODE,
		      TAG_ZHENGFA
		      ]
	 },

	 {
		 tip:"恭喜过关",
		 over:true

	 }];

