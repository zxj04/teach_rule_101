exp10.ShowLayer04 = cc.Layer.extend({
	backgroundLayer : null,
	mainLayar : null,
	ctor:function (menuTag) {
		this._super();
		this.initFrames();
		this.loadBackground(menuTag);
		this.loadMainLayer();
	},
	initFrames : function(){
		cc.spriteFrameCache.addSpriteFrames(exp10.res_show.show_p);
		gg.curRunSpriteFrame.push(exp10.res_show.show_p);
	},
	loadBackground : function(menuTag){
		this.backgroundLayer = new ShowBackgroundLayer(menuTag);
		this.addChild(this.backgroundLayer);
	},
	loadMainLayer : function(){
		this.mainLayar = new exp10.ShowMainLayer04();
		this.addChild(this.mainLayar);
	}
});
exp10.ShowScene04 = PScene.extend({
	ctor:function(menuTag){
		this._super();
		this.menuTag=menuTag;
	},
	onEnter:function () {
		this._super();
		var layer = new exp10.ShowLayer04(this.menuTag);
		this.addChild(layer);
	}
});
