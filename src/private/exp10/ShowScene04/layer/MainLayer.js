TAG_PROBLEM1 = 6001;
TAG_PROBLEM2 = 6002;
exp10.ShowMainLayer04 = cc.Layer.extend({
	lead:null,
	real:null,
	ctor:function () {
		this._super();
		this.init();
		return true;
	},
	init:function(){//
		this.sv = new pub.ScrollView(this);
		this.sv.setOffestPos(cc.p(152,45 + 15));
		
		this.addlayout();
		this.addlayout2();
	},
	addlayout:function(){
		layout = new ccui.Layout();// 第一页
		layout.setContentSize(cc.size(996, 650));// 设置layout的大小
		this.sv.addLayer(layout);

		var text1 = $.format("土壤的本质属性是(  )", 980,30);
		var label1 = new cc.LabelTTF(text1,gg.fontName,gg.fontSize4);
		label1.setColor(cc.color(0, 0, 0, 250));
		label1.setAnchorPoint(0, 0.5);
		label1.setPosition(50, layout.height - label1.height / 2-50);
		layout.addChild(label1);

		this.objArr1 = [];
		this.loadSelect(layout, "A．具有孔隙,能够渗透水和空气","#wrong.png", cc.p(50,500), TAG_PROBLEM1, this.objArr1);
		this.loadSelect(layout, "B．具有肥力,能够生长植物","#right.png", cc.p(50,460), TAG_PROBLEM1, this.objArr1);
		this.loadSelect(layout, "C．具有有机质的合成和分解过程","#wrong.png", cc.p(50,420), TAG_PROBLEM1, this.objArr1);
		this.loadSelect(layout, "D．对植物的生长具有指示作用","#wrong.png", cc.p(50,380), TAG_PROBLEM1, this.objArr1);

		var text2 = $.format("解答:A,这不是土壤的本质属性;" +
				"B,这是土壤的本质属性;" +
				"C,是生物体的功能;" +
				"D一定植物适应于一定的土壤,这是植物对环境的指示作用。" +
				"故选：B", 800,30);
		this.loadResult(layout, text2, cc.p(50,250), this.objArr1);
		
		var cloud = new exp10.CloudLabel(layout, "土壤最大作用是使植\n物生长,它的出现使地\n球上出现了陆生植物", 
				exp10.CLOUD_RIGHT_UP);
		cloud.setPosition(800, 400);
		cloud.setVisible(false);
		this.objArr1.push(cloud);
	},
	addlayout2:function(){
		layout = new ccui.Layout();// 第一页
		layout.setContentSize(cc.size(996, 650));// 设置layout的大小
		this.sv.addLayer(layout);

		var text1 = $.format("土壤中空气过多而水分缺少时,会导致(  )", 980,30);
		var label1 = new cc.LabelTTF(text1,gg.fontName,gg.fontSize4);
		label1.setColor(cc.color(0, 0, 0, 250));
		label1.setAnchorPoint(0, 0.5);
		label1.setPosition(50, layout.height - label1.height / 2-50);
		layout.addChild(label1);

		this.objArr2 = [];
		this.loadSelect(layout, "A．土壤温度过高","#wrong.png", cc.p(50,500), TAG_PROBLEM2, this.objArr2);
		this.loadSelect(layout, "B．保水性能破坏","#wrong.png", cc.p(50,460), TAG_PROBLEM2, this.objArr2);
		this.loadSelect(layout, "C．透水性能破坏","#wrong.png", cc.p(50,420), TAG_PROBLEM2, this.objArr2);
		this.loadSelect(layout, "D．养分供应不足","#right.png", cc.p(50,380), TAG_PROBLEM2, this.objArr2);

		var text2 = $.format("解答:无机盐需溶于水和水一起被根吸收,缺水后养分供应就会不足" +
				",故选：D", 800,30);
		this.loadResult(layout, text2, cc.p(50,250), this.objArr2);
	},
	loadSelect:function(layout, str,answer, pos, tag, objArr){
		var button=new Angel(layout,"#select.png",this.callback,this);
		button.setPosition(pos);
		button.setTag(tag);
		var label = new cc.LabelTTF(str,"微软雅黑",30);
		label.setPosition(pos.x+button.width*0.5+label.width*0.5+10,pos.y);
		label.setColor(cc.color(0, 0, 0, 250));
		layout.addChild(label);
		var answer=new cc.Sprite(answer);
		answer.setPosition(pos);
		answer.setVisible(false);
		layout.addChild(answer);
		objArr.push(answer);
	},
	loadResult:function(layout, text, pos, objArr){
		var label = new cc.LabelTTF(text, gg.fontName, gg.fontSize4);
		label.setVisible(false);
		label.setColor(cc.color(0, 0, 0, 300));
		label.setAnchorPoint(0, 1);
		label.setPosition(pos);
		layout.addChild(label);
		objArr.push(label);
	},
	callback:function(p){
		var objArr = [];
		switch (p.getTag()) {
		case TAG_PROBLEM1:
			objArr = this.objArr1;
			break;
		case TAG_PROBLEM2:
			objArr = this.objArr2;
			break;
		default:
			break;
		}
		for(var i = 0; i < objArr.length; i++){
			var obj = objArr[i];
			obj.setVisible(true);
		}
	}
});
