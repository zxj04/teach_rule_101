exp10.ShowMainLayer02 = cc.Layer.extend({
	lead:null,
	real:null,
	curIndex: 0,
	cur:0,
	cell:50,
	ctor:function () {
		this._super();
		this.init();		
		return true;
	},
	init:function(){
		this.sv = new pub.ScrollView(this);
		this.sv.setOffestPos(cc.p(152,45 + 15));

		this.addlayout1();
	},
	addlayout1:function(){
		var layout = new ccui.Layout();// 第一页
		layout.setContentSize(cc.size(996, 598));// 设置layout的大小
		this.sv.addLayer(layout);
		
		var e1 = new pub.ShowButton(layout, "#bt_yellow.png", "空气").setPos(350, 300);
		var e2 = new pub.ShowButton(layout, "#bt_green.png", "水").setPos(350, 300);
		var e3 = new pub.ShowButton(layout, "#bt_blue.png", "无机盐").setPos(350, 300);
		var e4 = new pub.ShowButton(layout, "#bt_pink.png", "有机物").setPos(350, 300);
		var e5 = new pub.ShowButton(layout, "#bt_empty.png", "土壤")
					.setLabelColor(cc.color(62, 62, 62)).setPos(350, 300);
		var eList = [e1, e2, e3, e4];
		for(var i = 0; i < eList.length; i++){
			var e = eList[i];
			e.setVisible(false);
			e.index = i;
			this.scheduleOnce(function(){
				var moveList = [cc.moveBy(1, cc.p(150, 0)), cc.moveBy(1, cc.p(-150, 0)), 
				                cc.moveBy(1, cc.p(0, -150)), cc.moveBy(1, cc.p(0, 150))];
				var seq = cc.sequence(
						cc.show(), 
						moveList[this.index], 
						cc.delayTime(3), 
						cc.hide(), 
						cc.callFunc(function(){this.setPosition(350, 300)},this));
				this.runAction(seq.repeatForever());
			}.bind(e), i * 1);
		}
		new exp10.CloudLabel(layout, "土壤中应有空\n气、水、无机\n盐、有机物等", 
				exp10.CLOUD_RIGHT_UP).setPosition(800, 400);

	},
	addText:function(p, text, pos, isDefaultAP){
		var label = new cc.LabelTTF(text, gg.fontName, gg.fontSize4);
		label.setPosition(pos);
		if(!isDefaultAP){
			label.setAnchorPoint(0, 0.5);
		}
		label.setColor(cc.color(0, 0, 0, 250));
		p.addChild(label);
		return label;
	},
	addRichText:function(p, t1, t2, t3, pos){
		new exp10.RichTTF(p)
			.append(t1, cc.color(0,0,0))
			.append(t2, cc.color(255,0,0))
			.append(t3, cc.color(0,0,0))
			.setPosition(pos);
	},
	addbutton:function(parent,tag,str,pos,labelx){
		var button = new ButtonScale(parent,"#unsel.png",this.callback,this);
		button.setPosition(pos);
		button.setLocalZOrder(5);
		button.setTag(tag);
		var label = new cc.LabelTTF(str,gg.fontName,30);
		label.setPosition(button.width/2,button.height/2);		
		label.setColor(cc.color(0, 0, 0, 250));
		button.addChild(label);
	},
	addimage:function(parent,str,pos,str2,pos2){
		var image = new cc.Sprite(str);
		parent.addChild(image);
		image.setPosition(pos);
		var label = new cc.LabelTTF(str2,gg.fontName,gg.fontSize);
		label.setColor(cc.color(0, 0, 0, 250));
		parent.addChild(label);
		label.setPosition(pos2);
	},
	callback:function(p){		
		switch(p.getTag()){
		case TAG_BUTTON1:
			break;
		case TAG_BUTTON2:
			break;
	}
	}
});
