exp10.RichTTF = cc.Node.extend({
	focus: null,
	lineSpace: 5,// 行间距
	curObj:null,// 当前对象
	ctor:function(parent, fontName, fontSize){
		this._super();
		parent.addChild(this);
		this.init(fontName, fontSize);
		return this;
	},
	init:function(fontName, fontSize){
		if(fontName){
			this.fontName = fontName;
		} else {
			this.fontName = gg.fontName;
		}
		if(fontSize){
			this.fontSize = fontSize;
		} else {
			this.fontSize = gg.fontSize;
		}
		this.focus = cc.p(0, 0);
	},
	setAPoint:function(x, y){
		this.richText.setAnchorPoint(cc.p(x, y));
	},
	refreshRect:function(){
		// 刷新大小
	},
	append:function(strOrObj, color){
		if(typeof strOrObj == "object"){
			this.appendObject(strOrObj);
		} else {
			this.appendLabel(strOrObj, color);
		}
		this.refreshFocus();
		return this;
	},
	appendLabel:function(str, color){
		var label = new cc.LabelTTF(str, this.fontName, this.fontSize)
		label.setColor(color);
		label.setAnchorPoint(0, 0.5);
		label.setPosition(this.focus);
		this.addChild(label);
		this.curObj = label;
	},
	appendObject:function(obj){
		obj.removeFromParent();
		obj.setAnchorPoint(0, 0.5);
		obj.setPosition(this.focus);
		this.addChild(obj)
		this.curObj = obj;
	},
	refreshFocus:function(){
		var pos = this.curObj.getPosition();
		var box = this.curObj.getBoundingBoxToWorld();
		this.focus = cc.p(pos.x + box.width, pos.y);
	},
	enter:/**
			 * 回车换行
			 */
	function(){
		this.focus = cc.p(0, this.focus.y - this.fontSize - this.lineSpace);
	}
});