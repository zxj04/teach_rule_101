exp10.ShowMainLayer03 = cc.Layer.extend({
	lead:null,
	real:null,
	objArr:null,
	ctor:function () {
		this._super();
		this.init();
		return true;
	},
	init:function(){
		this.objArr = [];
		this.sv = new pub.ScrollView(this);
		this.sv.setOffestPos(cc.p(152,45 + 15));

		this.addlayout1();
		this.addlayout2();
	},	
	addlayout1:function(){
		var layout = new ccui.Layout();// 第一页
		layout.setContentSize(cc.size(996, 598));// 设置layout的大小
		this.sv.addLayer(layout);

		var text = $.format("　　不同原子构成分子时,各种原子的个数用\"化合价\"来标识。氢的化合价为+1,氧为-2,氢或氧与其他元素组成化合物时,其遵守的规则是化合物中所有元素化合价的代数和为零。", 820, 30);
		this.addText(layout, text, cc.p(50, 520));

		var text = $.format("岩石风化", 980, 30);
		var t1 = this.addText(layout, text, cc.p(300, 50), true);
		var show1 = this.addSprite(layout, exp10.res_show.show11);
		$.up(t1, show1, 10);

		var text = $.format("水流裂石", 980, 30);
		var t2 = this.addText(layout, text, cc.p(750, 50), true);
		var show2 = this.addSprite(layout, exp10.res_show.show12);
		$.up(t2, show2, 10);

	},
	addlayout2:function(){
		var layout = new ccui.Layout();// 第一页
		layout.setContentSize(cc.size(996, 598));// 设置layout的大小
		this.sv.addLayer(layout);
		
		var text = $.format("　　岩石就是在长期的风吹雨打、冷热交替和生物的作用下,逐渐风化成石砾和沙粒等矿物质颗粒,最后经各种生物和气候的长期作用才形成了土壤。", 820, 30);
		this.addText(layout, text, cc.p(50, 480));
		
		var text = $.format("冷热骤变,岩石爆裂", 980, 30);
		var t1 = this.addText(layout, text, cc.p(166, 50), true);
		var show1 = this.addSprite(layout, exp10.res_show.show13);
		$.up(t1, show1, 10);

		var text = $.format("冰能裂石", 980, 30);
		var t2 = this.addText(layout, text, cc.p(498, 50), true);
		var show2 = this.addSprite(layout, exp10.res_show.show14);
		$.up(t2, show2, 10);

		var text = $.format("地衣促进岩石风化", 980, 30);
		var t3 = this.addText(layout, text, cc.p(830, 50), true);
		var show3 = this.addSprite(layout, exp10.res_show.show15);
		$.up(t3, show3, 10);
		
		var draw = new cc.DrawNode();
		layout.addChild(draw);
		draw.drawSegment(cc.p(10, 350),cc.p(986, 350), 2,cc.color(62, 62, 62));
	},
	addText:function(p, text, pos, defaultAp){
		var label = new cc.LabelTTF(text, gg.fontName, gg.fontSize4);
		label.setPosition(pos);
		if(!defaultAp){
			label.setAnchorPoint(0, 0.5);
		}
		label.setColor(cc.color(0, 0, 0, 250));
		p.addChild(label);
		return label;
	},
	addSprite:function(p, img, pos){
		var sprite = new cc.Sprite(img);
		if(pos){
			sprite.setPosition(pos);
		}
// sprite.setAnchorPoint(0, 0.5);
		p.addChild(sprite);
		return sprite;
	},
	callback:function(p){
		switch(p.getTag()){
		case TAG_BUTTON1:
			break;
		case TAG_BUTTON2:
			break;
		}
	}
	
});
