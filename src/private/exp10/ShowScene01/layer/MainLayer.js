exp10.ShowMainLayer01 = cc.Layer.extend({
	lead:null,
	real:null,
	ctor:function () {
		this._super();
		this.init();
		return true;
	},
	init:function(){//
		this.sv = new pub.ScrollView(this);
		this.sv.setOffestPos(cc.p(152,45 + 15));

		this.addlayout();
		this.addlayout2();
	},
	addlayout:function(){
		var layout = new ccui.Layout();// 第一页
		layout.setContentSize(cc.size(996, 598));// 设置layout的大小
		this.sv.addLayer(layout);
		
		var text = $.format("　　土壤是植物生长的摇篮,除了植物还有什么?", 800, 30);
		this.addText(layout, text, cc.p(50, 510));

		var text = $.format("动物", 800, 30);
		var t1 = this.addText(layout, text, cc.p(166, 100), true);
		
		var text = $.format("植物", 800, 30);
		var t2 = this.addText(layout, text, cc.p(498, 100), true);
		
		var text = $.format("微生物", 800, 30);
		var t3 = this.addText(layout, text, cc.p(830, 100), true);
		
		var show1 = new cc.Sprite(exp10.res_show.show1);
		var show2 = new cc.Sprite(exp10.res_show.show2);
		var show3 = new cc.Sprite(exp10.res_show.show3);
		$.up(t1, show1, 10);
		$.up(t2, show2, 10);
		$.up(t3, show3, 10);
		layout.addChild(show1);
		layout.addChild(show2);
		layout.addChild(show3);

	},
	addlayout2:function(){
		var layout = new ccui.Layout();//
		layout.setContentSize(cc.size(996, 598));// 设置layout的大小
		this.sv.addLayer(layout);

		var text = $.format("土壤中除了能观察到的一些动物,还有其他的生物", 800, 30);
		this.addText(layout, text, cc.p(50, 550));

		new pub.ShowButton(layout, "#bt_yellow.png", "动物").setPosition(100, 350);
		new pub.ShowButton(layout, "#bt_green.png", "植物").setPosition(250, 350);
		new pub.ShowButton(layout, "#bt_blue.png", "细菌").setPosition(400, 350);
		new pub.ShowButton(layout, "#bt_pink.png", "真菌").setPosition(550, 350);
		new pub.ShowButton(layout, "#bt_empty.png", "土壤\n生物")
		.setLabelColor(cc.color(62, 62, 62)).setPosition(325, 100);
		
		new exp10.CloudLabel(layout, "生活在土壤中的各类\n生物统称为土壤生物\n(soil organism)", 
				exp10.CLOUD_RIGHT_UP).setPosition(800, 300);
		
		var draw = new cc.DrawNode();
		layout.addChild(draw);
		draw.drawSegment(cc.p(50, 250),cc.p(600, 250), 2,cc.color(62, 62, 62));
		draw.drawSegment(cc.p(325, 250),cc.p(325, 150), 2,cc.color(62, 62, 62));
	},
	addRichText:function(p, t1, t2, t3, pos){
		new pub.RichText(p)
		.append(t1, cc.color(0,0,0))
		.append(t2, cc.color(255,0,0))
		.append(t3, cc.color(0,0,0))
		.setPosition(pos);
	},
	addText:function(p, text, pos, defaultAp){
		var label = new cc.LabelTTF(text, gg.fontName, gg.fontSize4);
		label.setPosition(pos);
		if(!defaultAp){
			label.setAnchorPoint(0, 0.5);
		}
		label.setColor(cc.color(0, 0, 0, 250));
		p.addChild(label);
		return label;
	},
	addButton:function(parent,tag,str,pos){
		var button = new ButtonScale(parent,"#unsel.png",this.callback,this);
		button.setPosition(pos);
		button.setTag(tag);
		var label = new cc.LabelTTF(str,gg.fontName,30);
		label.setColor(cc.color(0, 0, 0, 250));
		label.setPosition(button.width/2,button.height/2);		
		button.addChild(label);
		return button;
	},
	callback:function(p){
		switch(p.getTag()){
		case TAG_BUTTON1:
			p.label.setVisible(true);
			break;
		case TAG_KOH:
			if(p.collision.getTag() == TAG_DISSOLUTION){
				p.setVisible(false);
				this.showTip("Very Good");
			} else {
				this.showTip("Error");
			}
			break;
		case TAG_BASO4:
			if(p.collision.getTag() == TAG_UNDISSOLUTION){
				p.setVisible(false);
				this.showTip("Very Good");
			} else {
				this.showTip("Error");
			}
			break;
		case TAG_AGCL:
			if(p.collision.getTag() == TAG_UNDISSOLUTION){
				p.setVisible(false);
				this.showTip("Very Good");
			} else {
				this.showTip("Error");
			}
			break;
		case TAG_NAOH:
			if(p.collision.getTag() == TAG_DISSOLUTION){
				p.setVisible(false);
				this.showTip("Very Good");
			} else {
				this.showTip("Error");
			}
			break;
		case TAG_RESET:
			this.resetShow();
			break;
		default:
			break;
		}
	}
});
