exp12.ShowMainLayer03 = cc.Layer.extend({
	lead:null,
	real:null,
	objArr:null,
	ctor:function () {
		this._super();
		this.init();
		return true;
	},
	init:function(){
		this.objArr = [];
		this.sv = new pub.ScrollView(this);
		this.sv.setOffestPos(cc.p(152,45 + 15));

		this.addlayout1();
		this.addlayout2();
	},	
	addlayout1:function(){
		var layout = new ccui.Layout();// 第一页
		layout.setContentSize(cc.size(996, 598));// 设置layout的大小
		this.sv.addLayer(layout);
		
		var text = $.format("　　植物的叶通过光合作用制造了有机物,这些有机物除小部分留在叶肉细胞外," +
				"大部分需转送到茎、根、果实、种子等部位去。", 820, 30);
		this.addText(layout, text, cc.p(50, 520));

		var sp1 = this.addSprite(layout, "#bt_yellow.png", cc.p(500, 250));
		sp1.runAction(cc.sequence(cc.spawn(cc.moveTo(2, cc.p(190, 150)), cc.scaleTo(2, 0.1)), cc.callFunc(this.reset.bind(sp1), this)).repeatForever());
		new pub.ShowButton(layout, "#bt_yellow.png", "茎").setPosition(190, 150);
		
		var sp2 = this.addSprite(layout, "#bt_green.png", cc.p(500, 250));
		sp2.runAction(cc.sequence(cc.spawn(cc.moveTo(2, cc.p(190, 400)), cc.scaleTo(2, 0.1)), cc.callFunc(this.reset.bind(sp2), this)).repeatForever());
		new pub.ShowButton(layout, "#bt_green.png", "根").setPosition(190, 400);
		
		var sp3 = this.addSprite(layout, "#bt_blue.png", cc.p(500, 250));
		sp3.runAction(cc.sequence(cc.spawn(cc.moveTo(2, cc.p(810, 150)), cc.scaleTo(2, 0.1)), cc.callFunc(this.reset.bind(sp3), this)).repeatForever());
		new pub.ShowButton(layout, "#bt_blue.png", "果实").setPosition(810, 150);
		
		var sp4 = this.addSprite(layout, "#bt_pink.png", cc.p(500, 250));
		sp4.runAction(cc.sequence(cc.spawn(cc.moveTo(2, cc.p(810, 400)), cc.scaleTo(2, 0.1)), cc.callFunc(this.reset.bind(sp4), this)).repeatForever());
		new pub.ShowButton(layout, "#bt_pink.png", "种子").setPosition(810, 400);
		
		new pub.ShowButton(layout, "#bt_empty.png", "叶子")
		.setLabelColor(cc.color(62,62,62)).setPosition(500, 250);
		
		this.addText(layout, "无机盐运输目的", cc.p(498, 50), true);
	},
	reset:function(){
		this.setPosition(this.origPos);
		this.setScale(1);
	},
	addlayout2:function(){
		var layout = new ccui.Layout();// 第一页
		layout.setContentSize(cc.size(996, 598));// 设置layout的大小
		this.sv.addLayer(layout);
		
		var node = new cc.Node();
		node.setPosition(450, 250);
		layout.addChild(node);
		var s1 = this.addSprite(node, "#shoot1.png");
		var s2 = this.addSprite(node, "#shoot2.png");
		var s3 = this.addSprite(node, "#shoot3.png");
		var s4 = this.addSprite(node, "#shoot4.png");
		var k =this.addSprite(node, "#apparatus/knife.png", cc.p(350, 0));
		var f =this.addSprite(node, "#fanshilin.png", cc.p(35, 0));
		var cloud = new exp12.CloudLabel(layout, "切口上方的树皮\n胀大而形成枝瘤", 
				exp12.CLOUD_RIGHT_UP).setPos(850, 250);
		this.objArr2 = [s1, s2, s3, s4, k, f, cloud];
		for(var i = 1; i < this.objArr2.length; i++){
			this.objArr2[i].setVisible(false);
		}
		this.addButton(layout, TAG_BUTTON1, "开始\n实验", cc.p(100, 300));
		
		this.tip = this.addText(layout, "", cc.p(250, 520));
		this.time = this.addText(layout, "", cc.p(750, 400));
		
	},
	addButton:function(parent,tag,str,pos){
		var s9 = new pub.S9Button(parent, str,this.callback,this);
		s9.setPosition(pos);
		s9.setTag(tag);
		return s9;
	},
	addText:function(p, text, pos, defaultAp){
		var label = new cc.LabelTTF(text, gg.fontName, gg.fontSize4);
		label.setPosition(pos);
		if(!defaultAp){
			label.setAnchorPoint(0, 0.5);
		}
		label.setColor(cc.color(0, 0, 0, 250));
		p.addChild(label);
		return label;
	},
	addSprite:function(p, img, pos){
		var sprite = new cc.Sprite(img);
		if(pos){
			sprite.setPosition(pos);
			sprite.origPos = pos;
		}
// sprite.setAnchorPoint(0, 0.5);
		p.addChild(sprite);
		return sprite;
	},
	step: 1,
	week: 1,
	doNext:function(){
		switch(this.step){
			case 1:
				this.tip.setString("用小刀在枝条上削去一段树皮");
				var knife = this.objArr2[4];
				knife.setVisible(true);
				var seq = cc.sequence(cc.moveBy(1, cc.p(-260 ,0)),
						cc.moveBy(1, cc.p(50 ,0)),
						cc.callFunc(function(){
							this.objArr2[1].setVisible(true);
							knife.runAction(cc.sequence(cc.fadeOut(0.5), cc.callFunc(function(){
								knife.setVisible(false);
								knife.setOpacity(255);
								knife.setPosition(350, 0);
							},this)));
						}, this), cc.delayTime(2), cc.callFunc(this.doNext, this));
				knife.runAction(seq);
				this.step ++;
				break;
			case 2:
				this.tip.setString("在缺口上,涂抹少许凡士林");
				var f = this.objArr2[5];
				f.setVisible(true);
				var rep = new cc.Repeat(cc.sequence(cc.moveBy(0.5, cc.p(0, -60)), 
						cc.callFunc(function(){
							var pos = f.getPosition();
							f.setPosition(pos.x, pos.y + 60);
						},this)), 5);
				var seq = cc.sequence(rep, cc.hide(), cc.delayTime(2), cc.callFunc(this.doNext, this));
				f.runAction(seq);
				this.step ++;
			break;
			case 3:
				this.tip.setString("观察枝条环剥部位的变化");
				this.schedule(function(){
					this.time.setString("第" + this.week++ + "周");
					if(this.week == 3){
						this.objArr2[2].setVisible(true);
					} else if(this.week == 5){
						this.objArr2[3].setVisible(true);
					} else if(this.week == 6){
						this.objArr2[6].setVisible(true);
					}
				}, 2, 4, 1);
				this.step ++;
				break;
		}
	},
	callback:function(p){
		switch(p.getTag()){
		case TAG_BUTTON1:
			if(this.step == 1){
				this.doNext();
			} else if(this.step == 4
					&& this.week == 6) {
				this.step = 1;
				this.week = 1;
				this.time.setString("");
				for(var i = 1; i < this.objArr2.length; i++){
					this.objArr2[i].setVisible(false);
				}
				this.doNext();
			}
			break;
		case TAG_BUTTON2:
			break;
		}
	}
	
});
