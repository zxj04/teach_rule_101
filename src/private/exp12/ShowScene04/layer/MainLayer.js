TAG_PROBLEM1 = 6001;
TAG_PROBLEM2 = 6002;
exp12.ShowMainLayer04 = cc.Layer.extend({
	lead:null,
	real:null,
	ctor:function () {
		this._super();
		this.init();
		return true;
	},
	init:function(){//
		this.sv = new pub.ScrollView(this);
		this.sv.setOffestPos(cc.p(152,45 + 15));
		
		this.addlayout();
		this.addlayout2();
	},
	addlayout:function(){
		layout = new ccui.Layout();// 第一页
		layout.setContentSize(cc.size(996, 650));// 设置layout的大小
		this.sv.addLayer(layout);

		var text1 = $.format("玉米的茎长成后不能增粗,而桃树的茎能年年变粗,从茎的结构分析,能不能变粗的根本原因是(  )", 980,30);
		var label1 = new cc.LabelTTF(text1,gg.fontName,gg.fontSize4);
		label1.setColor(cc.color(0, 0, 0, 250));
		label1.setAnchorPoint(0, 0.5);
		label1.setPosition(50, layout.height - label1.height / 2-50);
		layout.addChild(label1);

		this.objArr1 = [];
		this.loadSelect(layout,"A．茎内有无韧皮部","#wrong.png", cc.p(50,500), TAG_PROBLEM1, this.objArr1);
		this.loadSelect(layout, "B．茎中有无形成层","#right.png", cc.p(50,460), TAG_PROBLEM1, this.objArr1);
		this.loadSelect(layout, "C．茎内有无木质部","#wrong.png", cc.p(50,420), TAG_PROBLEM1, this.objArr1);
		this.loadSelect(layout, "D．茎内有无髓","#wrong.png", cc.p(50,380), TAG_PROBLEM1, this.objArr1);

		var text2 = $.format("解答:形成层细胞能够分裂增生,属于分生组织,他决定茎能否增粗。" +
				"桃树是双子叶植物,有形成层,所以能变粗;" +
				"玉米是单子叶植物,没有形成层,所以不能变粗," +
				"故选：B", 800,30);
		this.loadResult(layout, text2, cc.p(50,250), this.objArr1);
	},
	addlayout2:function(){
		layout = new ccui.Layout();// 第一页
		layout.setContentSize(cc.size(996, 650));// 设置layout的大小
		this.sv.addLayer(layout);

		var text1 = $.format("解答:爬山虎的茎能产生不定根,能在竖直的墙壁上生长,按生长方式分析,爬山虎的茎属于(  )", 980,30);
		var label1 = new cc.LabelTTF(text1,gg.fontName,gg.fontSize4);
		label1.setColor(cc.color(0, 0, 0, 250));
		label1.setAnchorPoint(0, 0.5);
		label1.setPosition(50, layout.height - label1.height / 2-50);
		layout.addChild(label1);

		this.objArr2 = [];
		this.loadSelect(layout,"A．攀缘茎", "#right.png", cc.p(50,500), TAG_PROBLEM2, this.objArr2);
		this.loadSelect(layout, "B．缠绕茎","#wrong.png", cc.p(50,460), TAG_PROBLEM2, this.objArr2);
		this.loadSelect(layout, "C．直立茎","#wrong.png", cc.p(50,420), TAG_PROBLEM2, this.objArr2);
		this.loadSelect(layout, "D．匍匐茎","#wrong.png", cc.p(50,380), TAG_PROBLEM2, this.objArr2);

		var text2 = $.format("解答:爬山虎能够向上生长,所以排除匍匐茎;" +
				"向上生长需要借助墙壁,所以排除直立茎;" +
				"借助墙壁向上生长没有利用变态茎、叶,所以排除缠绕茎;剩下的是攀缘茎" +
				",故选：A", 820,30);
		this.loadResult(layout, text2, cc.p(50,250), this.objArr2);
	},
	loadSelect:function(layout,str, answer, pos, tag, objArr){
		var button=new Angel(layout,"#select.png",this.callback,this);
		button.setPosition(pos);
		button.setTag(tag);
		var label = new cc.LabelTTF(str,"微软雅黑",30);
		label.setPosition(pos.x+button.width*0.5+label.width*0.5+10,pos.y);
		label.setColor(cc.color(0, 0, 0, 250));
		layout.addChild(label);
		var answer=new cc.Sprite(answer);
		answer.setPosition(pos);
		answer.setVisible(false);
		layout.addChild(answer);
		objArr.push(answer);
	},
	loadResult:function(layout, text, pos, objArr){
		var label = new cc.LabelTTF(text, gg.fontName, gg.fontSize4);
		label.setVisible(false);
		label.setColor(cc.color(0, 0, 0, 300));
		label.setAnchorPoint(0, 1);
		label.setPosition(pos);
		layout.addChild(label);
		objArr.push(label);
	},
	callback:function(p){
		var objArr = [];
		switch (p.getTag()) {
		case TAG_PROBLEM1:
			objArr = this.objArr1;
			break;
		case TAG_PROBLEM2:
			objArr = this.objArr2;
			break;
		default:
			break;
		}
		for(var i = 0; i < objArr.length; i++){
			var obj = objArr[i];
			obj.setVisible(true);
		}
	}
});
