exp12.startRunNext = function(tag){
	switch(tag){// 具体哪个菜单执行什么操作，这里修改
		case TAG_MENU1:
			pub.ch.gotoShow(TAG_EXP_11, tag, exp12.g_resources_show, null, new exp12.ShowScene01(tag));
			break;
		case TAG_MENU2:
			pub.ch.gotoShow(TAG_EXP_11, tag, exp12.g_resources_show, null, new exp12.ShowScene02(tag));
			break;
		case TAG_MENU3:
			pub.ch.gotoShow(TAG_EXP_11, tag, exp12.g_resources_show, null, new exp12.ShowScene03(tag));
			break;
		case TAG_MENU4:
			gg.teach_type = TAG_LEAD;
			gg.teach_flow = exp12.teach_flow01;
			pub.ch.gotoRun(TAG_EXP_11, tag, exp12.g_resources_run, null, new exp12.RunScene01(tag));
			break;
		case TAG_MENU5:
			pub.ch.gotoShow(TAG_EXP_11, tag, exp12.g_resources_show, null, new exp12.ShowScene04(tag));
			break;
		case TAG_ABOUT:
			pub.ch.gotoAbout(TAG_EXP_11, g_resources_public_about, null, res_public_about.about_j);
			break;
	}
}

// 任务流
exp12.teach_flow01 = 
	[
	 {
		 tip:"选择植株",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG1,	   
		      ],
		      canClick:true		 
	 },
	 {
		 tip:"选择刀子",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG2,
		      ],
		      canClick:true	
	 },
	 {
		 tip:"选择红墨水",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG3,	   
		      ],
		      canClick:true		 
	 },
	 {
		 tip:"选择放大镜",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG6,	   
		      ],
		      canClick:true		 
	 },
	 {
		 tip:"点击确定，开始实验",
		 tag:[
		      TAG_LIB,
		      TAG_SURE
		      ]
	 },
	 {
		 tip:"用刀子将植物①的表皮去掉",
		 tag:[
		      TAG_KNIFE,
		      TAG_KNIFE1
		      ],
		      action:ACTION_DO1
	 }, {
		 tip:"用刀子将植物②的木质部去掉",
		 tag:[
		      TAG_KNIFE,
		      TAG_KNIFE1
		      ],
		      action:ACTION_DO2
	 },	
	 {
		 tip:"将植物①放入装有红墨水的烧杯中",
		 tag:[
		      TAG_PLANT,
		      TAG_PLANT1]
	 },
	 {
		 tip:"将植物②放入装有红墨水的烧杯中",
		 tag:[
		      TAG_PLANT,
		      TAG_PLANT2],
	 },
	 {
		 tip:"将植物③放入装有红墨水的烧杯中",
		 tag:[
		      TAG_PLANT,
		      TAG_PLANT3],
	 },
	 {
		 tip:"将装有红墨水和植物的烧杯放在温暖光线充足的地方",
		 tag:[
		      TAG_BEAKER,
		      TAG_BEAKER1
		      ],
	 },
	 {
		 tip:"将植物①从装有红墨水的烧杯中取出,用刀切除植物枝条中下部位", 
		 tag:[
		      TAG_PLANT,
		      TAG_PLANT1],
		      action:ACTION_DO1
	 },
	 {
		 tip:"用放大镜观察植物枝条切面处的现象",
		 tag:[
		      TAG_MAGNIFIER,
		      TAG_MAGNIFIER1],
		      action:ACTION_DO1
	 },
	 {
		 tip:"将植物②从装有红墨水的烧杯中取出,用刀切除植物枝条中下部位", 
		 tag:[
		      TAG_PLANT,
		      TAG_PLANT2],
		      action:ACTION_DO1
	 },
	 {
		 tip:"用放大镜观察植物枝条切面处的现象",
		 tag:[
		      TAG_MAGNIFIER,
		      TAG_MAGNIFIER1],
		      action:ACTION_DO2
	 },
	 {
		 tip:"将植物③从装有红墨水的烧杯中取出,用刀切除植物枝条中下部位", 
		 tag:[
		      TAG_PLANT,
		      TAG_PLANT3],
		      action:ACTION_DO1
	 },
	 {
		 tip:"用放大镜观察植物枝条切面处的现象",
		 tag:[
		      TAG_MAGNIFIER,
		      TAG_MAGNIFIER1],
		      action:ACTION_DO3
	 },
	 {
		 tip:"恭喜过关",
		 over:true

	 }
	 ];
