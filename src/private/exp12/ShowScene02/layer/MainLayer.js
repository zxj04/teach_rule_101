exp12.ShowMainLayer02 = cc.Layer.extend({
	lead:null,
	real:null,
	curIndex: 0,
	cur:0,
	cell:50,
	ctor:function () {
		this._super();
		this.init();		
		return true;
	},
	init:function(){
		this.sv = new pub.ScrollView(this);
		this.sv.setOffestPos(cc.p(152,45 + 15));

		this.addlayout1();
	},
	addlayout1:function(){
		var layout = new ccui.Layout();// 第一页
		layout.setContentSize(cc.size(996, 598));// 设置layout的大小
		this.sv.addLayer(layout);
	
		var text = $.format("　　根从土壤中吸收来的水和无机盐是通过茎运输的。水分和无机盐被根尖吸收后,会进入根部导管,最后进入到茎中央的导管,运输向枝端。", 820, 30);
		this.addText(layout, text, cc.p(50, 520));

		var show = new cc.Node();
		show.setPosition(0, 300);
		layout.addChild(show);
		
		var water = new pub.ShowButton(show, "#bt_blue.png", "水").setPos(100, 0);
		var seq = cc.sequence(cc.moveBy(4, cc.p(800, 0)), cc.callFunc(function(){
			water.setPosition(100, 0);
		}, this));
		water.runAction(seq.repeatForever());
		
		var salt = new pub.ShowButton(show, "#bt_green.png", "无机盐").setPos(200, 0);
		var seq = cc.sequence(cc.moveBy(4, cc.p(800, 0)), cc.callFunc(function(){
			salt.setPosition(200, 0);
		}, this));
		salt.runAction(seq.repeatForever());
		
		new pub.ShowButton(show, "#bt_empty.png", "根尖").setPos(300, 0).setLabelColor(cc.color(62,62,62));
		new pub.ShowButton(show, "#bt_empty.png", "根部\n导管").setPos(450, 0).setLabelColor(cc.color(62,62,62));
		new pub.ShowButton(show, "#bt_empty.png", "茎中央\n  导管").setPos(600, 0).setLabelColor(cc.color(62,62,62));
		new pub.ShowButton(show, "#bt_empty.png", "枝端").setPos(750, 0).setLabelColor(cc.color(62,62,62));
		
		this.addText(layout, "水和无机盐的流向", cc.p(498, 120), true);
	},
	setShowString:function(str){
		var t = $.format(str, 550, 30);
		this.show.setString(t);
	},
	addText:function(p, text, pos, isDefaultAP){
		var label = new cc.LabelTTF(text, gg.fontName, gg.fontSize4);
		label.setPosition(pos);
		if(!isDefaultAP){
			label.setAnchorPoint(0, 0.5);
		}
		label.setColor(cc.color(0, 0, 0, 250));
		p.addChild(label);
		return label;
	},
	addSprite:function(p, img, pos, rect){
		var sprite = new cc.Sprite(img, rect);
		if(pos){
			sprite.setPosition(pos);
		}
// sprite.setAnchorPoint(0, 0.5);
		p.addChild(sprite);
		return sprite;
	},
	addRichText:function(p, t1, t2, t3, pos){
		new exp12.RichTTF(p)
			.append(t1, cc.color(0,0,0))
			.append(t2, cc.color(255,0,0))
			.append(t3, cc.color(0,0,0))
			.setPosition(pos);
	},
	addButton:function(parent,tag,str,pos){
		var s9 = new pub.S9Button(parent, str,this.callback,this);
		s9.setPosition(pos);
		s9.setTag(tag);
		return s9;
	},
	addimage:function(parent,str,pos,str2,pos2){
		var image = new cc.Sprite(str);
		parent.addChild(image);
		image.setPosition(pos);
		var label = new cc.LabelTTF(str2,gg.fontName,gg.fontSize);
		label.setColor(cc.color(0, 0, 0, 250));
		parent.addChild(label);
		label.setPosition(pos2);
	},
	callback:function(p){
		var seq = cc.sequence(cc.fadeOut(1), cc.fadeIn(1));
		for(var i = 0; i < this.genArr.length; i++){
			this.genArr[i].setOpacity(255);
			this.genArr[i].stopAllActions();
		}
		switch(p.getTag()){
		case TAG_BUTTON1:
			this.genArr[0].runAction(seq.repeatForever());
			this.setShowString(this.textArr[0]);
			break;
		case TAG_BUTTON2:
			this.genArr[1].runAction(seq.repeatForever());
			this.setShowString(this.textArr[1]);
			break;
		case TAG_BUTTON3:
			this.genArr[2].runAction(seq.repeatForever());
			this.setShowString(this.textArr[2]);
			break;
		case TAG_BUTTON4:
			this.genArr[3].runAction(seq.repeatForever());
			this.setShowString(this.textArr[3]);
			break;
	}
	}
});
