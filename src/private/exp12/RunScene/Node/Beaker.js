Beaker18 = cc.Node.extend({
	ctor:function(p){
		this._super();
		p.addChild(this, 10, TAG_BEAKER);
		this.init();
	},
	init : function(){
		this.setCascadeOpacityEnabled(true);

		this.beaker=new Button(this,15,TAG_BEAKER1,"#beaker1.png",this.callback,this);
		
	},
	callback:function(p){
		var func=cc.callFunc(function() {
			gg.flow.next();
		}, this);
		var action=gg.flow.flow.action;
		switch (p.getTag()) {
		case TAG_BEAKER1:
			var move=cc.moveBy(1,cc.p(380, 0));
			this.runAction(cc.sequence(cc.spawn(move,cc.callFunc(function() {
				ll.run.plant.movePlant();
			}, this)),cc.callFunc(function() {
				ll.run.clock.doing();
				this.showTip=new ShowTip("将装有红墨水和植物的烧杯放在温暖光线充足的地方",cc.p(650,650),true);
			}, this),cc.delayTime(4),cc.callFunc(function() {
				ll.run.clock.stop();
			}, this),func));
			break;
		default:
			break;
		}
	}
});