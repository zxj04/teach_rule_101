Plant18 = cc.Node.extend({
	ctor:function(p){
		this._super();
		p.addChild(this, 10, TAG_PLANT);
		this.init();
	},
	init : function(){
		this.setCascadeOpacityEnabled(true);

		this.plant=new Button(this,12,TAG_PLANT1,"#botany1.png",this.callback,this);
		
		this.plant1=new Button(this,11,TAG_PLANT2,"#botany1.png",this.callback,this);
		this.plant1.setPosition(200,0);
		
		this.plant2=new Button(this,10,TAG_PLANT3,"#botany1.png",this.callback,this);
		this.plant2.setPosition(400,0);
		
		this.beaker1=new Button(this,9,TAG_BEAKER2,"#beaker2.png",this.callback,this);
		this.beaker1.setPosition(-223,-102);
	},
	movePlant:function(){
		var move=cc.moveBy(1,cc.p(380,0));
		this.runAction(move);
	},
	callback:function(p){
		var func=cc.callFunc(function(){
			gg.flow.next();
		}, this);
		var action=gg.flow.flow.action;
		switch (p.getTag()) {
		case TAG_PLANT1:
			if(action==ACTION_DO1){
				var ber =cc.bezierBy(1, [cc.p(0, 180),cc.p(280,200),cc.p(280,0)]);
				var rotate=cc.rotateBy(1,5);
				p.runAction(cc.sequence(ber,rotate,cc.callFunc(function() {
					ll.run.knife.moveKnife(p);
				}, this),cc.delayTime(2),func));
			}else{
				var ber = cc.bezierBy(1, [cc.p(-50,80),cc.p(-268,430),cc.p(-268,60)]);
				var rotate=cc.rotateBy(1,-5);
				p.runAction(cc.sequence(ber,rotate,func));
			}
			break;
		case TAG_PLANT2:
			if(action==ACTION_DO1){
				var ber =cc.bezierBy(1, [cc.p(0, 180),cc.p(280,200),cc.p(240,0)]);
				p.runAction(cc.sequence(ber,cc.callFunc(function() {
					ll.run.knife.moveKnife(p);
				}, this),cc.delayTime(2),func));
			}else{
				var ber = cc.bezierBy(1, [cc.p(-50, 80),cc.p(-430,430),cc.p(-430,60)]);
				p.runAction(cc.sequence(ber,func));
			}
			break;
		case TAG_PLANT3:
			if(action==ACTION_DO1){
				var ber =cc.bezierBy(1, [cc.p(0, 180),cc.p(280,200),cc.p(200,0)]);
				var rotate=cc.rotateBy(1,-5);
				p.runAction(cc.sequence(ber,rotate,cc.callFunc(function() {
					ll.run.knife.moveKnife(p);
				}, this),cc.delayTime(2),func));
			}else{
				var ber = cc.bezierBy(1, [cc.p(-50,80),cc.p(-585, 430),cc.p(-585,60)]);
				var rotate=cc.rotateBy(1,5)
				p.runAction(cc.sequence(ber,rotate,cc.callFunc(function() {
					this.plant.setSpriteFrame("botany3_1.png");
					this.plant1.setSpriteFrame("botany2_1.png");
					this.plant2.setSpriteFrame("botany1_1.png");
				}, this),func));
			}
			break;
		default:
			break;
		}
	}
});