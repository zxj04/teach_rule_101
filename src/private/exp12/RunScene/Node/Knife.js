Knife18 = cc.Node.extend({
	ctor:function(p){
		this._super();
		p.addChild(this, 10, TAG_KNIFE);
		this.init();
	},
	init : function(){
		this.setCascadeOpacityEnabled(true);

		this.knife=new Button(this,10,TAG_KNIFE1,"#knife.png",this.callback,this);
		this.knife.setAnchorPoint(0.65,0.65);

	},
	moveKnife:function(p){
		var move=cc.moveBy(1,cc.p(-188,0));
		var skew=cc.skewBy(1,35,0);
		var move1=cc.moveBy(1,cc.p(0, 5));
		var move2=cc.moveBy(1,cc.p(188, 0));
		this.knife.runAction(cc.sequence(move,skew,move1,skew.reverse(),cc.callFunc(function() {
			if(TAG_PLANT1==p.getTag()){
				ll.run.plant.plant.setSpriteFrame("botany.png");
			}else if(TAG_PLANT2==p.getTag()){
				ll.run.plant.plant1.setSpriteFrame("botany.png");
			}else if(TAG_PLANT3==p.getTag()){
				ll.run.plant.plant2.setSpriteFrame("botany.png");
			}
		}, this),move2,cc.callFunc(function() {
			if(TAG_PLANT3==p.getTag()){
				this.knife.removeFromParent(true);
			}
		}, this)));
	},
	callback:function(p){
		var func=cc.callFunc(function() {
			gg.flow.next();
		}, this);
		var action=gg.flow.flow.action;
		switch (p.getTag()) {
		case TAG_KNIFE1:
			if(action==ACTION_DO1){
				cc.log(p.getPosition());
				var ber=cc.bezierBy(1, [cc.p(-100,50),cc.p(-230, 80),cc.p(-480,25)]);
				var move=cc.moveBy(1,cc.p(0, -80));
				var move1=cc.moveBy(1,cc.p(480, 50));
				p.runAction(cc.sequence(ber,move,cc.callFunc(function() {
					cc.log(p.getPosition());
					ll.run.plant.plant.setSpriteFrame("botany3.png");
				}, this),move1,func));
			}else if(action==ACTION_DO2){
				var ber=cc.bezierBy(1, [cc.p(-80,20),cc.p(-230,-50),cc.p(-346, -155)]);
				var rotate=cc.rotateBy(0.5,140);
				var skew=cc.skewBy(0.3,35,0);
				var skew1=cc.skewBy(0.3,-35,0);
				p.setAnchorPoint(0,0);
				p.runAction(cc.sequence(ber,rotate,cc.sequence(skew,skew.reverse(),skew1,skew1.reverse()).repeat(3),cc.callFunc(function() {
					ll.run.plant.plant1.setSpriteFrame("botany2.png");
				}, this),rotate.reverse(),ber.reverse(),func));
			}
			break;
		default:
			break;
		}
	}
});