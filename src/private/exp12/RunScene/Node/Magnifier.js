Magnifier = cc.Node.extend({
	ctor:function(p){
		this._super();
		p.addChild(this, 10, TAG_MAGNIFIER);
		this.init();
//		cc.KEY
	},
	init : function(){
		this.setCascadeOpacityEnabled(true);

		this.magnifier=new Button(this,15,TAG_MAGNIFIER1,"#Magnifier.png",this.callback,this);
		
		this.change=new Button(this,14,TAG_CHANGE,"#change2.png",this.callback,this);
		this.change.setPosition(-35,75);
		this.change.setVisible(false);

	},
	callback:function(p){
		var func=cc.callFunc(function() {
			gg.flow.next();
		}, this);
		var action=gg.flow.flow.action;
		switch (p.getTag()) {
		case TAG_MAGNIFIER1:
			var ber=cc.bezierBy(1, [cc.p(-80, 0),cc.p(-180,30),cc.p(-268,45)]);
			if(action==ACTION_DO1){l
				this.runAction(cc.sequence(ber,cc.callFunc(function() {
					this.change.setVisible(true);
				},this),cc.delayTime(3),cc.callFunc(function() {
					this.change.setVisible(false);
					ll.run.plant.plant.removeFromParent(true);
				}, this),ber.reverse(),func));
			}else if(action==ACTION_DO2){
				this.runAction(cc.sequence(ber,cc.callFunc(function() {
					this.change.setSpriteFrame("change3.png");
					this.change.setVisible(true);
				},this),cc.delayTime(3),cc.callFunc(function() {
					this.change.setVisible(false);
					ll.run.plant.plant1.removeFromParent(true);
				}, this),ber.reverse(),func));
			}else if(action==ACTION_DO3){
				this.runAction(cc.sequence(ber,cc.callFunc(function() {
					this.change.setSpriteFrame("change2.png");
					this.change.setVisible(true);
				},this),cc.delayTime(3),cc.callFunc(function() {
					this.change.setVisible(false);
					ll.run.plant.plant2.removeFromParent(true);
				}, this),ber.reverse(),cc.callFunc(function() {
					this.showTip=new ShowTip("切去木质部的植物枝条没有变化,其他的都变红,说明" +
							"\n植物通过其木质部中的导管进行水和无机盐的运输",cc.p(650,450));
				}, this),cc.delayTime(9),func));
			}
			break;
		default:
			break;
		}
	}
});