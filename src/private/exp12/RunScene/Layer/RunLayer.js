exp12.RunLayer01_2 = cc.Layer.extend({
	arr:null,
	scene:null,
	clock:null,
	ctor:function (parent) {
		this._super();
		this.scene = parent;
		this.scene.addChild(this, 10);
		gg.main = this;
		this.init();
	},
	init:function () {
		this.callNext = cc.callFunc(function(){
			gg.flow.next();
		}, this);
		this.callKill = cc.callFunc(function(p){
			var seq = cc.sequence(cc.fadeOut(0.5),cc.callFunc(function(){
				p.removeFromParent(true);	
			}, this));
			p.runAction(seq);
		}, this); 

		this.callNext.retain();
		this.callKill.retain();
		//时钟
		this.clock = new Clock(this);
		// 物品库
		this.lib = new Lib(this);

		this.lib.loadBg([{
			tag:TAG_LIB_PLANT1,
			checkright:true,	//实验所需器材添加checkright标签		
		},
		{
			tag:TAG_LIB_KNIFE,
			checkright:true,
		},
		{
			tag:TAG_LIB_REDINK,
			checkright:true,
		},
		{
			tag:TAG_LIB_AMPULLA,
		},
		{
			tag:TAG_LIB_DROPPER,
		},
		{
			tag:TAG_LIB_MAGNIFIER,
			checkright:true,
		},
		{
			tag:TAG_LIB_GLASS,
		},
		{
			tag:TAG_LIB_BEAKER,
		},
		{
			tag:TAG_LIB_TWEEZER,
		},
		{
			tag:TAG_LIB_CONICAL,
		}
		]);

		this.plant=new Plant18(this);
		this.plant.setVisible(false);
		this.plant.setPosition(450,300);

		this.knife=new Knife18(this);
		this.knife.setVisible(false);
		this.knife.setPosition(1000,250);

		this.beaker=new Beaker18(this);
		this.beaker.setVisible(false);
		this.beaker.setPosition(230,200);

		this.magnifier=new Magnifier(this);
		this.magnifier.setVisible(false);
		this.magnifier.setPosition(1150,150);
	},

	checkVisible:function(next){
		//是否可见
		var checkVisible = [];
		var node1 = ll.run.plant;
		var node2 = ll.run.knife;
		var node3 = ll.run.beaker;
		var node4 = ll.run.magnifier;

		checkVisible.push(node1,node2,node3,node4);
		for(var i in checkVisible){
			if(checkVisible[i] !== null){				
				checkVisible[i].setVisible(next);				
			}			
		}
	},
	loadInLib:function(obj, pos, tarPos,delay){
		obj.setPosition(pos);
		if(delay == null){
			delay = 1;
		}
		var ber = $.bezier(pos, tarPos, delay);
		var seq = cc.sequence(ber, this.callNext);
		obj.runAction(seq);
	},
	kill:function(obj){
		var fade = cc.fadeTo(0.5,0);
		var func = cc.callfunc(function(){
			obj.removeFromParent(true);
		},this);
		var seq = cc.sequence(fade,func);
		obj.runAction(seq)
	},

	callback:function (p){
		var func = cc.callFunc(this.actionDone, this);
		var action=gg.flow.flow.action;
		switch(p.getTag()){

		}
	},
	actionDone:function(p){
		var func = cc.callFunc(this.actionDone, this);
		switch(p.getTag()){

		}
	},
	flowNext:function(){
		gg.flow.next();
	},
	onExit:function(){
		this._super();
		this.callNext.release();
		this.callKill.release();
	}
});