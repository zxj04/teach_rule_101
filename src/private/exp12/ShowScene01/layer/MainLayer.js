exp12.ShowMainLayer01 = cc.Layer.extend({
	lead:null,
	real:null,
	ctor:function () {
		this._super();
		this.init();
		return true;
	},
	init:function(){//
		this.sv = new pub.ScrollView(this);
		this.sv.setOffestPos(cc.p(152,45 + 15));

		this.addlayout();
		this.addlayout2();
	},
	addlayout:function(){
		var layout = new ccui.Layout();// 第一页
		layout.setContentSize(cc.size(996, 598));// 设置layout的大小
		this.sv.addLayer(layout);
		
		var text = $.format("　　自然界中,植物的茎是千姿百态的,按形态分,直立茎、攀缘茎、匍匐茎和缠绕茎," +
				"虽然形态不同,但结果却基本相同。", 800, 30);
		this.addText(layout, text, cc.p(50, 500));

		var t1 = this.addText(layout, "匍匐茎", cc.p(120.5, 100), true);
		var show1 = this.addSprite(layout, exp12.res_show.show1);
		$.up(t1, show1, 10);

		var t2 = this.addText(layout, "直立茎", cc.p(381.5, 100), true);
		var show2 = this.addSprite(layout, exp12.res_show.show2);
		$.up(t2, show2, 10);
		
		var t2 = this.addText(layout, "缠绕茎", cc.p(642.5, 100), true);
		var show2 = this.addSprite(layout, exp12.res_show.show3);
		$.up(t2, show2, 10);
		
		var t2 = this.addText(layout, "攀缘茎", cc.p(903.5, 100), true);
		var show2 = this.addSprite(layout, exp12.res_show.show4);
		$.up(t2, show2, 10);
		
		var draw = new cc.DrawNode();
		layout.addChild(draw);
		draw.drawSegment(cc.p(50, 380),cc.p(950, 380), 2,cc.color(62, 62, 62));
	},
	addlayout2:function(){
		var layout = new ccui.Layout();//
		layout.setContentSize(cc.size(996, 598));// 设置layout的大小
		this.sv.addLayer(layout);

		var text = $.format("　　植物的茎按组成可分为草质茎和木质茎。木质茎一般具有树皮、形成层、" +
				"木质部和髓等结构", 550, 30);
		this.addText(layout, text, cc.p(50, 550));
		
		new exp12.CloudLabel(layout, "  草质茎没有形成层\n长成后不能加粗生长", 
				exp12.CLOUD_RIGHT_UP).setPosition(845, 500);
		
		var m1 = this.addSprite(layout, "#mood1.png", cc.p(500, 250));
		var m2 = this.addSprite(layout, "#mood2.png", cc.p(500, 250));
		var m3 = this.addSprite(layout, "#mood3.png", cc.p(500, 250));
		var m4 = this.addSprite(layout, "#mood4.png", cc.p(500, 250));
		var m5 = this.addSprite(layout, "#mood5.png", cc.p(500, 250));
		this.objArr2 = [m1, m2, m3, m4, m5];
		
		var button1 = this.addButton(layout, TAG_BUTTON1, "表　皮",cc.p(60, 400));
		var button2 = this.addButton(layout, TAG_BUTTON2, "韧皮部",cc.p(60, 320));
		var button3 = this.addButton(layout, TAG_BUTTON3, "形成层",cc.p(60, 240));
		var button4 = this.addButton(layout, TAG_BUTTON4, "木质层",cc.p(60, 160));
		var button5 = this.addButton(layout, TAG_BUTTON5, "　髓　",cc.p(60, 80));
	},
	addRichText:function(p, t1, t2, t3, pos){
		new pub.RichText(p)
		.append(t1, cc.color(0,0,0))
		.append(t2, cc.color(255,0,0))
		.append(t3, cc.color(0,0,0))
		.setPosition(pos);
	},
	addSprite:function(p, img, pos){
		var sprite = new cc.Sprite(img);
		if(pos){
			sprite.setPosition(pos);
		}
		p.addChild(sprite);
		return sprite;
	},
	addText:function(p, text, pos, defaultAp){
		var label = new cc.LabelTTF(text, gg.fontName, gg.fontSize4);
		label.setPosition(pos);
		if(!defaultAp){
			label.setAnchorPoint(0, 0.5);
		}
		label.setColor(cc.color(0, 0, 0, 250));
		p.addChild(label);
		return label;
	},
	addButton:function(parent,tag,str,pos){
		var s9 = new pub.S9Button(parent, str,this.callback,this);
		s9.setPosition(pos);
		s9.setTag(tag);
		return s9;
	},
	callback:function(p){
		for(var i = 0; i < this.objArr2.length; i++){
			var obj = this.objArr2[i];
			obj.stopAllActions();
			obj.setOpacity(30);
		}
		var seq = cc.sequence(cc.fadeTo(1.2, 255));
		switch(p.getTag()){
		case TAG_BUTTON1:
			this.objArr2[0].runAction(seq);
			break;
		case TAG_BUTTON2:
			this.objArr2[1].runAction(seq);
			break;
		case TAG_BUTTON3:
			this.objArr2[2].runAction(seq);
			break;
		case TAG_BUTTON4:
			this.objArr2[3].runAction(seq);
			break;
		case TAG_BUTTON5:
			this.objArr2[4].runAction(seq);
			break;
		default:
			break;
		}
	}
});
