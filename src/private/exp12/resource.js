var exp12 = exp12||{};
//start
exp12.res_start = {
	//	start_png :"res/private/exp12/start/startpng.png"
};

exp12.g_resources_start = [];
for (var i in exp12.res_start) {
	exp12.g_resources_start.push(exp12.res_start[i]);
}

// run
exp12.res_run = {
	run_p1 : "res/private/exp12/run.plist",
	run_g1 : "res/private/exp12/run.png"
};

exp12.g_resources_run = [];
for (var i in exp12.res_run) {
	exp12.g_resources_run.push(exp12.res_run[i]);
}

// show02
exp12.res_show = {
	show_p : "res/private/exp12/show.plist",
	show_g : "res/private/exp12/show.png",
	show1 : "res/private/exp12/show1.jpg",
	show2 : "res/private/exp12/show2.jpg",
	show3 : "res/private/exp12/show3.jpg",
	show4 : "res/private/exp12/show4.jpg"
};

exp12.g_resources_show = [];
for (var i in exp12.res_show) {
	exp12.g_resources_show.push(exp12.res_show[i]);
}

