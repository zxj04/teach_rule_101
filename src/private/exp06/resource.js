var exp06 = exp06||{};
//start
exp06.res_start = {
		//start_png :"res/private/exp06/start/startpng.png"
};

exp06.g_resources_start = [];
for (var i in exp06.res_start) {
	exp06.g_resources_start.push(exp06.res_start[i]);
}

//run
exp06.res_run = {
		run_p : "res/private/exp06/run.plist",
		run_g : "res/private/exp06/run.png",
};

exp06.g_resources_run = [];
for (var i in exp06.res_run) {
	exp06.g_resources_run.push(exp06.res_run[i]);
}

//show02
exp06.res_show = {
		show_p : "res/private/exp06/show.plist",
		show_g : "res/private/exp06/show.png"
};

exp06.g_resources_show = [];
for (var i in exp06.res_show) {
	exp06.g_resources_show.push(exp06.res_show[i]);
}


