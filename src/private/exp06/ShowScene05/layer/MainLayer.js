exp06.ShowMainLayer05 = cc.Layer.extend({
	lead:null,
	real:null,
	curIndex: 0,
	ctor:function () {
		this._super();
		this.init();
		return true;
	},
	init:function(){
		var sv = new pub.ScrollView(this);
		sv.setOffestPos(cc.p(152,45 + 15));

		this.addlayout1(sv);
		this.addlayout2(sv);
	},	
	addlayout1:function(parent){
		this.layout1 = new ccui.Layout();//第一页
		this.layout1.setContentSize(cc.size(996, 598));//设置layout的大小
		parent.addLayer(this.layout1);

		var text = $.format("　　下列说法正确的是（）", 860, 30);

		label1 = new cc.LabelTTF(text,gg.fontName,gg.fontSize4);
		label1.setAnchorPoint(0,0.5);
		label1.setPosition(0,this.layout1.height-label1.height/2);
		label1.setColor(cc.color(0, 0, 0, 250));
		this.layout1.addChild(label1);	

		this.addSelImage(this.layout1, "#select.png", TAG_BUTTON, cc.p(60,480 ),
				"A.缓慢氧化是氧化反应，燃烧不是氧化反应","#wrong.png",TAG_SEL);

		this.addSelImage(this.layout1, "#select.png", TAG_BUTTON1, cc.p(60,430),
				"B.物质与氧发生的反应都能产生燃烧现象","#wrong.png",TAG_SEL1);

		this.addSelImage(this.layout1, "#select.png", TAG_BUTTON2, cc.p(60,380),
				"C.物质与氧发生的反应是氧化反应","#right.png",TAG_SEL2);

		this.addSelImage(this.layout1, "#select.png", TAG_BUTTON3, cc.p(60,330), 
				"D.只有物质与氧气发生的反应才是氧化反应","#wrong.png",TAG_SEL3);
		var text = $.format("　　解析：氧化反应是物质与氧发生反应，但不能将氧理解为氧气，可以使氧的化合物。所以D错误。另外，缓慢氧化、燃烧都是" +
				"氧化反应，缓慢氧化是一种不易察觉的氧化反应，而燃烧是发光、发热的剧烈氧化反应，所以A、B错误，正确答案是C", 920, 25);

		this.label1 = new cc.LabelTTF(text,gg.fontName,25);
		this.label1.setAnchorPoint(0,0.5);
		this.label1.setPosition(0,180);
		this.label1.setColor(cc.color(0, 0, 0, 250));
		this.label1.setVisible(false);
		this.layout1.addChild(this.label1);	
	},
	addlayout2:function(parent){
		this.layout2 = new ccui.Layout();//第四页
		this.layout2.setContentSize(cc.size(996, 598));
		parent.addLayer(this.layout2);

		var text = $.format("　　姚明在进行篮球比赛时，运动量大。他的能量直接来自（）", 960, 30);
		label1 = new cc.LabelTTF(text ,gg.fontName,gg.fontSize4);
		label1.setAnchorPoint(0,0.5);
		label1.setPosition(0,this.layout2.height-label1.height/2);
		label1.setColor(cc.color(0, 0, 0, 250));
		this.layout2.addChild(label1);	

		this.addSelImage(this.layout2, "#select.png", TAG_BUTTON4, cc.p(60,480 ),
				"A.比赛前进食的所有食物","#wrong.png",TAG_SEL4);

		this.addSelImage(this.layout2, "#select.png", TAG_BUTTON5, cc.p(60,430 ),
				"B.体内细胞的呼吸作用","#right.png",TAG_SEL5);

		this.addSelImage(this.layout2, "#select.png", TAG_BUTTON6, cc.p(60,380 ),
				"C.每时每刻吸入体内的氧气","#wrong.png",TAG_SEL6);

		this.addSelImage(this.layout2, "#select.png", TAG_BUTTON7, cc.p(60,330 ), 
				"D.以上都是","#wrong.png",TAG_SEL7);
		var text = $.format("　　分析：错选答案A的直接原因是没有注意道题目中的“直接”二字，A不是直接能量，需要消化吸收。" +
				"人体内吸入氧气，氧化分解体内的有机物，释放出能量，供生命活动所需。所以正确答案是B。", 920, 25);

		this.label2 = new cc.LabelTTF(text,gg.fontName,25);
		this.label2.setAnchorPoint(0,0.5);
		this.label2.setPosition(0,180);
		this.label2.setColor(cc.color(0, 0, 0, 250));
		this.label2.setVisible(false);
		this.layout2.addChild(this.label2);				
	},
	addSelImage:function(parent,str,tag,pos,str2,str3,tag2){
		var image = new ButtonScale(parent,str,this.callback,this);
		image.setPosition(pos);
		image.setTag(tag);

		var sel = new cc.Sprite(str3);
		sel.setPosition(15,15);
		image.addChild(sel);
		sel.setVisible(false);
		sel.setTag(tag2);

		var label = new cc.LabelTTF(str2,gg.fontName,gg.fontSize);
		label.setColor(cc.color(0, 0, 0, 250));
		label.setAnchorPoint(0,0.5);
		parent.addChild(label);
		label.setPosition(image.x + 20,image.y);
	},
	callback:function(p){
		switch(p.getTag()){	
		case TAG_BUTTON:
		case TAG_BUTTON1:
		case TAG_BUTTON2:
		case TAG_BUTTON3:
			this.layout1.getChildByTag(TAG_BUTTON).getChildByTag(TAG_SEL).setVisible(true);
			this.layout1.getChildByTag(TAG_BUTTON1).getChildByTag(TAG_SEL1).setVisible(true);
			this.layout1.getChildByTag(TAG_BUTTON2).getChildByTag(TAG_SEL2).setVisible(true);
			this.layout1.getChildByTag(TAG_BUTTON3).getChildByTag(TAG_SEL3).setVisible(true);
			this.label1.setVisible(true);	
			break;

		case TAG_BUTTON4:
		case TAG_BUTTON5:
		case TAG_BUTTON6:
		case TAG_BUTTON7:
			this.layout2.getChildByTag(TAG_BUTTON4).getChildByTag(TAG_SEL4).setVisible(true);
			this.layout2.getChildByTag(TAG_BUTTON5).getChildByTag(TAG_SEL5).setVisible(true);
			this.layout2.getChildByTag(TAG_BUTTON6).getChildByTag(TAG_SEL6).setVisible(true);
			this.layout2.getChildByTag(TAG_BUTTON7).getChildByTag(TAG_SEL7).setVisible(true);
			this.label2.setVisible(true);
			break;		
		
		}
	}	
});
