exp06.startRunNext = function(tag){
	switch(tag){//具体哪个菜单执行什么操作，这里修改
	case TAG_MENU1:
		pub.ch.gotoShow(TAG_EXP_06,tag , exp06.g_resources_show, null, new exp06.ShowScene01(tag));
		break;
	case TAG_MENU2:

		pub.ch.gotoShow(TAG_EXP_06,tag , exp06.g_resources_show, null, new exp06.ShowScene02(tag));
		break;
	case TAG_MENU3:
		pub.ch.gotoShow(TAG_EXP_06,tag , exp06.g_resources_show, null, new exp06.ShowScene03(tag));
		break;
	case TAG_MENU4:
		pub.ch.gotoShow(TAG_EXP_06,tag , exp06.g_resources_show, null, new exp06.ShowScene04(tag));
		break;
	case TAG_MENU5:
		gg.teach_type = TAG_LEAD;
		gg.teach_flow = exp06.teach_flow01;
		pub.ch.gotoRun(TAG_EXP_06, tag,exp06.g_resources_run, null, new exp06.RunScene01());
		break;
	case TAG_MENU6:
		pub.ch.gotoShow(TAG_EXP_06,tag , exp06.g_resources_show, null, new exp06.ShowScene05(tag));
		break;
	case TAG_ABOUT:
		ch.gotoAbout(TAG_EXP_06, g_resources_public_about, null, res_public_about.about_j);
		break;
	}
}

// 任务流
exp06.teach_flow01 = 
	[
	 {
		 tip:"选择灵敏电流表",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG3,	   
		      ],
		      canClick:true		 
	 },
	 {
		 tip:"选择铜锌装置",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG10,	   
		      ],
		      canClick:true		 
	 },
	 {
		 tip:"选择导线",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG6,	   
		      ],
		      canClick:true		 
	 },



	 {
		 tip:"点击确定，开始实验",
		 tag:[
		      TAG_LIB,
		      TAG_SURE
		      ]
	 },
	 {
		 tip:"将铜片锌片插到稀硫酸中",
		 tag:[
		      TAG_BATTERY_NODE,
		      TAG_CU
		      ]
	 },
	 {
		 tip:"连接导线，观察现象",
		 tag:[
		      TAG_BATTERY_NODE,
		      TAG_LINE1
		      ]
	 },

	 {
		 tip:"恭喜过关",
		 over:true

	 }
	 ]