exp06.ShowMainLayer03 = cc.Layer.extend({
	lead:null,
	real:null,
	curIndex: 0,
	reset:true,
	actionnum:1,
	ctor:function () {
		this._super();
		this.init();
		return true;
	},
	init:function(){
		var sv = new pub.ScrollView(this);
		sv.setOffestPos(cc.p(152,45 + 15));

		this.addlayout1(sv);
		this.addlayout2(sv);
		this.addlayout3(sv);
	},	
	addlayout1:function(parent){
		this.layout1 = new ccui.Layout();//第一页
		this.layout1.setContentSize(cc.size(996, 598));//设置layout的大小
		parent.addLayer(this.layout1);

		var text = $.format("　　在自然界里，缓慢氧化和剧烈氧化都可能导致火灾。因此我们需要采取相应的防范措施，一旦发生火灾就要" +
				"迅速采取灭火措施。\n我们可以根据燃烧条件来灭火，以下两图分别用了什么方法？", 960, 30);
		
		label1 = new cc.LabelTTF(text,gg.fontName,gg.fontSize4);
		label1.setAnchorPoint(0,0.5);
		label1.setPosition(0,550);
		label1.setColor(cc.color(0, 0, 0, 250));
		this.layout1.addChild(label1);	
		
		this.addshowimage(this.layout1,"#huozai/huojing.png",cc.p(250,300),"使周围温度低于\n物体的着火点");
		this.addshowimage(this.layout1,"#huozai/maotan.png",cc.p(750,300),"隔绝空气，使燃烧体\n缺少助燃剂");		
	},
	addlayout2:function(parent){
		this.layout2 = new ccui.Layout();//第四页
		this.layout2.setContentSize(cc.size(996, 598));
		parent.addLayer(this.layout2);

		label1 = new cc.LabelTTF("　　发生火灾时，要保持镇定，先了解火源的准确位置，然后拨打“119”\n报警求助。" +
				"在发生火灾时要这么做：",gg.fontName,gg.fontSize4);
		label1.setAnchorPoint(0,0.5);
		label1.setPosition(0,this.layout2.height-label1.height/2);
		label1.setColor(cc.color(0, 0, 0, 0));
		this.layout2.addChild(label1,2);	
		
		this.addshowimage(this.layout2,"#huozai/119.png",cc.p(150,300),"条件允许下，拨打\n“119”报警求助");
		this.addshowimage(this.layout2,"#huozai/wanyao.png",cc.p(500,300),"屋内浓烟密布时用\n湿毛巾捂住口鼻\n府伏在地上爬行");
		this.addshowimage(this.layout2,"#huozai/yfzh.png",cc.p(850,300),"当衣物着火时，\n用衣物拍打或在\n地上翻滚灭火");		
	},
	addlayout3:function(parent){
		this.layout3 = new ccui.Layout();//第四页
		this.layout3.setContentSize(cc.size(996, 598));
		parent.addLayer(this.layout3);

		label1 = new cc.LabelTTF("　　发生火灾时，要第一时间玩外跑，千万不要坐电梯，因为火灾会毁坏电\n路，导致电梯故障，把人关在电梯里面；" +
				"遇到火灾不要随意跳楼逃生，因为\n楼层过高往下跳会摔死。",gg.fontName,gg.fontSize4);
		label1.setAnchorPoint(0,0.5);
		label1.setPosition(0,this.layout3.height-label1.height/2);
		label1.setColor(cc.color(0, 0, 0, 0));
		this.layout3.addChild(label1,2);
		
		this.addshowimage(this.layout3,"#huozai/caiwu.png",cc.p(150,300),"火灾发生时，第一时间\n往外跑不要留恋财物");
		this.addshowimage(this.layout3,"#huozai/dianti.png",cc.p(500,300),"千万不要做电梯\n我们还是走楼梯吧");
		this.addshowimage(this.layout3,"#huozai/tiaolou.png",cc.p(850,300),"不要随意跳楼逃生\n先看清火势和楼层");
	},
	addshowimage:function(parent,str,pos,str2){
		var showimage = new cc.Sprite(str);
		showimage.setPosition(pos);
		parent.addChild(showimage,5);
		var label = new cc.LabelTTF(str2,gg.fontName,gg.fontSize);
		label.setColor(cc.color(0, 0, 0, 250));
		parent.addChild(label);
		label.setPosition(showimage.x,showimage.y - showimage.height * 0.5 - label.height/2 - 10);
	},
	addbutton:function(parent,tag,str,pos){
		var button = new ButtonScale(parent,"#unsel.png",this.callback,this);
		button.setPosition(pos);
		button.setLocalZOrder(5);
		button.setTag(tag);
		var label = new cc.LabelTTF(str,gg.fontName,30);
		label.setPosition(button.width/2,button.height/2);		
		label.setColor(cc.color(0, 0, 0, 250));
		button.addChild(label);
		label.setTag(tag);
	},
	callback:function(p){
		switch(p.getTag()){	
		
		    	
		}
	}	
});
