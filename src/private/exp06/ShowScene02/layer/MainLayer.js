exp06.ShowMainLayer02 = cc.Layer.extend({
	lead:null,
	real:null,
	reset:true,
	reset1:true,
	reset2:true,
	ctor:function () {
		this._super();
		this.init();
		return true;
	},
	init:function(){	
		var sv = new pub.ScrollView(this);
		sv.setOffestPos(cc.p(152,45 + 15));
	
		this.addlayout1(sv);	
		this.addlayout2(sv);	
		this.addlayout3(sv);
		this.addlayout4(sv);
	},	
	addlayout1:function(parent){
		this.layout1 = new ccui.Layout();//第一页
		this.layout1.setContentSize(cc.size(996, 598));//设置layout的大小
		parent.addLayer(this.layout1);

		var text = $.format("　　分别将木条和粉笔放到酒精灯燃烧。", 980, 30);
		label1 = new cc.LabelTTF(text,gg.fontName,gg.fontSize4);
		label1.setAnchorPoint(0,0.5);
		label1.setPosition(0,this.layout1.height-label1.height/2);
		label1.setColor(cc.color(255, 255, 255, 250));
		this.layout1.addChild(label1,10);	
		
		var bg = new cc.Sprite(res_public.run_back);
		this.layout1.addChild(bg);
		bg.setScale(0.8);
		bg.setPosition(cc.p(this.layout1.width /2, this.layout1.height / 2 +30));
	
		this.lamp = new cc.Sprite("#lampBottle.png");
		this.lamp.setPosition(cc.p(500,200));
		this.layout1.addChild(this.lamp,7);
		this.lamp.setScale(0.5);
	
		var fire = new cc.Sprite("#fire/1.png");
		fire.setPosition(cc.p(168,360));
		this.lamp.addChild(fire);
		fire.setScale(3);
		fire.runAction(this.addAction("fire/", 3, 0.05));
			
		this.mutiao = new cc.Sprite("#mutiao.png");
		this.mutiao.setPosition(cc.p(700,400));
		this.layout1.addChild(this.mutiao,5);
		this.mutiao.setScale(0.8);
		
		this.mfire =  new cc.Sprite("#fire2/1.png");
		this.mfire.setPosition(cc.p(10,40));
		this.mutiao.addChild(this.mfire);
		this.mfire.runAction(this.addAction("fire2/", 2, 0.1));
		this.mfire.setOpacity(0);
		
		this.pen = new cc.Sprite("#pen.png");
		this.pen.setPosition(cc.p(300,400));
		this.layout1.addChild(this.pen,5);
		this.pen.setRotation(50);
		
		this.black =  new cc.Sprite("#black.png");
		this.black.setPosition(cc.p(228,13));
		this.pen.addChild(this.black);
		this.black.setOpacity(0);
	
		this.addbutton(this.layout1, TAG_BUTTON1, "播放", cc.p(250,180));
		this.layout1.getChildByTag(TAG_BUTTON1).getChildByTag(TAG_BUTTON1).setColor(cc.color(255, 255, 255, 250));
				
		this.label1 = new cc.LabelTTF("证明燃烧的条件之一是：燃烧物质是可燃物",gg.fontName,gg.fontSize4);
		this.label1.setAnchorPoint(0,0.5);
		this.label1.setPosition(80,80);
		this.label1.setColor(cc.color(255, 0, 0, 250));
		this.layout1.addChild(this.label1,10);
		this.label1.setVisible(false);
		
	},
	addlayout2:function(parent){
		this.layout2 = new ccui.Layout();//第四页
		this.layout2.setContentSize(cc.size(996, 598));
		parent.addLayer(this.layout2);
		var text = $.format("　　将烧杯盖在点燃的蜡烛上方", 980, 30);
		label1 = new cc.LabelTTF(text,gg.fontName,gg.fontSize4);
		label1.setAnchorPoint(0,0.5);
		label1.setPosition(0,this.layout2.height-label1.height/2);
		label1.setColor(cc.color(255, 255, 255, 250));
		this.layout2.addChild(label1,10);	
		
		var bg = new cc.Sprite(res_public.run_back);
		this.layout2.addChild(bg);
		bg.setScale(0.8);
		bg.setPosition(cc.p(this.layout2.width /2, this.layout2.height / 2 +30));

		
		var lazhu = new cc.Sprite("#lazu.png");
		lazhu.setPosition(cc.p(450,250));
		this.layout2.addChild(lazhu);
		lazhu.setScale(1.5);

		this.lfire = new cc.Sprite("#fire/1.png");
		this.lfire.setPosition(cc.p(42,114));
		lazhu.addChild(this.lfire,5);
		this.lfire.setAnchorPoint(0.5,0);
		this.lfire.runAction(this.addAction("fire/", 3, 0.05));
		
		this.beaker = new cc.Sprite("#beaker2.png");
		this.beaker.setPosition(cc.p(650,300));
		this.layout2.addChild(this.beaker,2);
		
		this.addbutton(this.layout2, TAG_BUTTON2, "播放", cc.p(250,180));
		this.layout2.getChildByTag(TAG_BUTTON2).getChildByTag(TAG_BUTTON2).setColor(cc.color(255, 255, 255, 250));
		
		this.label2 = new cc.LabelTTF("证明燃烧的条件之二是：燃烧需要助燃剂，常见的是氧气。",gg.fontName,gg.fontSize4);
		this.label2.setAnchorPoint(0,0.5);
		this.label2.setPosition(80,100);
		this.label2.setColor(cc.color(255, 0, 0, 250));
		this.layout2.addChild(this.label2,5);	
		this.label2.setOpacity(0);
		
	},
	addlayout3:function(parent){
		this.layout3 = new ccui.Layout();//第四页
		this.layout3.setContentSize(cc.size(996, 598));
		parent.addLayer(this.layout3);

		var text = $.format("　　将火柴头和火柴梗放在铜片上，用酒精灯加热。", 980, 30);
		label1 = new cc.LabelTTF(text,gg.fontName,gg.fontSize4);
		label1.setAnchorPoint(0,0.5);
		label1.setPosition(0,this.layout2.height-label1.height/2);
		label1.setColor(cc.color(255, 255, 255, 250));
		this.layout3.addChild(label1,10);	
		
		var bg = new cc.Sprite(res_public.run_back);
		this.layout3.addChild(bg);
		bg.setScale(0.8);
		bg.setPosition(cc.p(this.layout3.width /2, this.layout3.height / 2 +30));
		
		this.lamp2 = new cc.Sprite("#lampBottle.png");
		this.lamp2.setPosition(cc.p(700,220));
		this.layout3.addChild(this.lamp2,7);
		this.lamp2.setScale(0.5);

		var fire = new cc.Sprite("#fire/1.png");
		fire.setPosition(cc.p(168,360));
		this.lamp2.addChild(fire);
		fire.setScale(3);
		fire.runAction(this.addAction("fire/", 3, 0.05));
		
		var iron = new cc.Sprite("#Iron.png");
		iron.setPosition(cc.p(500,240));
		this.layout3.addChild(iron,3);
		
		var tongpian = new cc.Sprite("#tongpian.png");
		tongpian.setPosition(cc.p(500,330));
		this.layout3.addChild(tongpian,7);

		this.huochai = new cc.Sprite("#huochai.png");
		this.huochai.setPosition(cc.p(450,338));
		this.layout3.addChild(this.huochai ,8);
		this.huochai.setScale(1.5);
		
		this.huochai1 = new cc.Sprite("#huochai1.png");
		this.huochai1.setPosition(cc.p(452,340));
		this.layout3.addChild(this.huochai1 ,8);
		this.huochai1.setScale(1.5);
		this.huochai1.setOpacity(0);
		
		this.hfire = new cc.Sprite("#fire2/1.png");
		this.hfire.setPosition(cc.p(440,340));
		this.layout3.addChild(this.hfire,10);
		this.hfire.setScale(0.5);
		this.hfire.runAction(this.addAction("fire2/", 2, 0.1));
		this.hfire.setOpacity(0);
		
		this.huochaigeng = new cc.Sprite("#huochaigeng.png");
		this.huochaigeng.setPosition(cc.p(530,338));
		this.layout3.addChild(this.huochaigeng ,8);
		this.huochaigeng.setScale(1.5);

		
		this.huochaigeng1 = new cc.Sprite("#huochaigeng1.png");
	    this.huochaigeng1.setPosition(cc.p(530,338));
		this.layout3.addChild(this.huochaigeng1 ,8);
		this.huochaigeng1.setScale(1.5);
		this.huochaigeng1.setOpacity(0);

		this.gfire = new cc.Sprite("#fire2/1.png");
		this.gfire.setPosition(cc.p(500,332));
		this.layout3.addChild(this.gfire,10);
		this.gfire.setScale(0.5);
		this.gfire.runAction(this.addAction("fire2/", 2, 0.1));
		this.gfire.setOpacity(0);

		this.addbutton(this.layout3, TAG_BUTTON3, "播放", cc.p(250,180));
		this.layout3.getChildByTag(TAG_BUTTON3).getChildByTag(TAG_BUTTON3).setColor(cc.color(255, 255, 255, 250));
		
		this.label3 = new cc.LabelTTF("证明燃烧的条件之三是：燃烧物质达到着火点",gg.fontName,gg.fontSize4);
		this.label3.setAnchorPoint(0,0.5);
		this.label3.setPosition(80,80);
		this.label3.setColor(cc.color(255, 0, 0, 250));
		this.layout3.addChild(this.label3);
		this.label3.setOpacity(0);
	},
	addlayout4:function(parent){
		this.layout4 = new ccui.Layout();//第一页
		this.layout4.setContentSize(cc.size(996, 598));//设置layout的大小
		parent.addLayer(this.layout4);

		var text = $.format("　　物质在缓慢氧化过程中产生的热量如果不能及时散失，就会使温度逐渐升高，达到着火点时，不经点火，" +
				"物质也会自发的燃烧起来。这种由于缓慢氧化而引起的自发燃烧叫自燃。" +
				"\n　　如果燃烧以极快的速度在有限的空间里发生，瞬间积累大量的热，使气体体积急剧地膨胀，就会爆炸。爆炸也可以为人类服务。" , 980,30);
		label = new cc.LabelTTF(text,gg.fontName,gg.fontSize4);
		label.setColor(cc.color(0, 0, 0, 250));
		label.setAnchorPoint(0, 0.5);
		label.setPosition(0,this.layout4.height-label.height/2);
		this.layout4.addChild(label);	
		
		var meitan = new cc.Sprite("#meitan.png");
		meitan.setPosition(cc.p(250,230));
		this.layout4.addChild(meitan);

		var baopo = new cc.Sprite("#baopo/1.png");
		baopo.setPosition(cc.p(700,230));
		this.layout4.addChild(baopo);
		baopo.runAction(this.addAction("baopo/", 11, 0.1));
		
		label = new cc.LabelTTF("煤炭自燃",gg.fontName,gg.fontSize4);
		label.setColor(cc.color(0, 0, 0, 250));
		label.setAnchorPoint(0.5, 0.5);
		label.setPosition(200,60);
		this.layout4.addChild(label);	

		label = new cc.LabelTTF("定向爆破",gg.fontName,gg.fontSize4);
		label.setColor(cc.color(0, 0, 0, 250));
		label.setAnchorPoint(0.5, 0.5);
		label.setPosition(700,60);
		this.layout4.addChild(label);
								
	},
	addAction:function(str1,num,time){
		var frames = [];
		for(var i =1 ;i<=num;i++){
			var str = str1+i+".png";
			var frame=cc.spriteFrameCache.getSpriteFrame(str);
			frames.push(frame);		
		}
		var animation = new cc.Animation(frames,time);//负责动画序列
		var action = new cc.Animate(animation);//帧动画的动作创建
		return action.repeatForever();
	},
	addshowimage:function(parent,str,pos,str2){
		var showimage = new cc.Sprite(str);
		showimage.setPosition(pos);
		parent.addChild(showimage,5);
		var label = new cc.LabelTTF(str2,gg.fontName,gg.fontSize);
		label.setColor(cc.color(0, 0, 0, 250));
		parent.addChild(label);
		label.setPosition(showimage.x,showimage.y - showimage.height * 0.5 -20);
	},
	addbutton:function(parent,tag,str,pos){
		var button = new ButtonScale(parent,"#unsel.png",this.callback,this);
		button.setPosition(pos);
		button.setLocalZOrder(5);
		button.setTag(tag);
		var label = new cc.LabelTTF(str,gg.fontName,30);
		label.setPosition(button.width/2,button.height/2);		
		label.setColor(cc.color(0, 0, 0, 250));
		button.addChild(label);
		label.setTag(tag);
	},
	callback:function(p){
		switch(p.getTag()){	
		case TAG_BUTTON1:
			this.reset = !this.reset;
			if(!this.reset){
				p.setEnable(false);
				var move = cc.moveTo(1,cc.p(540,390));
				var move1 = cc.moveTo(1,cc.p(700,400));
				var seq = cc.sequence(move,cc.delayTime(1),cc.callFunc(function(){
					this.mfire.setOpacity(255);
				},this),move1);
				this.mutiao.runAction(seq);
				
				var mov = cc.moveTo(1,cc.p(430,380));
				var mov1 = cc.moveTo(1,cc.p(300,400));
				var seq = cc.sequence(cc.delayTime(3),mov,cc.delayTime(1),cc.callFunc(function(){
					this.black.runAction(cc.fadeIn(1));
				},this),cc.delayTime(1),mov1,cc.callFunc(function(){
					p.setEnable(true);
					this.label1.setVisible(true);
					this.layout1.getChildByTag(TAG_BUTTON1).getChildByTag(TAG_BUTTON1).setString("复原");
				},this));
				this.pen.runAction(seq);		
			}else{
				this.mfire.setOpacity(0);
				this.black.setOpacity(0);
				this.layout1.getChildByTag(TAG_BUTTON1).getChildByTag(TAG_BUTTON1).setString("播放");
			}

		break;
		case TAG_BUTTON2:
			this.reset1 = !this.reset1;
			if(!this.reset1){
				p.setEnable(false);
				var move = cc.moveTo(1,cc.p(600,500));
				var move1 = cc.moveTo(1,cc.p(460,500));
				var move2 = cc.moveTo(1,cc.p(460,290));
				var seq = cc.sequence(move,move1,move2,cc.callFunc(function(){
					this.lfire.runAction(cc.scaleTo(5,0.4));
				},this),cc.delayTime(5),cc.callFunc(function(){
					this.lfire.setOpacity(0);
					p.setEnable(true);
					this.label2.setOpacity(255);
					this.layout2.getChildByTag(TAG_BUTTON2).getChildByTag(TAG_BUTTON2).setString("复原");
				},this));
				this.beaker.runAction(seq);
			}else{
				this.layout2.getChildByTag(TAG_BUTTON2).getChildByTag(TAG_BUTTON2).setString("播放");
				this.beaker.setPosition(cc.p(650,300));
				this.lfire.setScale(1);
				this.lfire.setOpacity(255);
			}
		break;
		case TAG_BUTTON3:
			this.reset2 = !this.reset2;
			if(!this.reset2){
				p.setEnable(false);
				var move = cc.moveTo(1,cc.p(500,220));
				var seq = cc.sequence(move,cc.delayTime(0.5),cc.callFunc(function(){
					this.hfire.setOpacity(255);
					var se = cc.sequence(cc.spawn(cc.moveTo(2,cc.p(460,360)),cc.scaleTo(2,0.4)),cc.fadeOut(1));
					this.hfire.runAction(se);
					this.huochai.runAction(cc.fadeOut(2));
					this.huochai1.runAction(cc.fadeIn(2));
				},this),cc.delayTime(5),cc.callFunc(function(){
					this.gfire.setOpacity(255);
					var se = cc.sequence(cc.spawn(cc.moveTo(4,cc.p(560,365)),cc.scaleTo(4,0.4)),cc.fadeOut(1));
					this.gfire.runAction(se);
					this.huochaigeng.runAction(cc.fadeOut(4));
					this.huochaigeng1.runAction(cc.fadeIn(4));
				},this),cc.delayTime(3),cc.callFunc(function(){
					p.setEnable(true);
					this.label3.setVisible(true);
					this.layout3.getChildByTag(TAG_BUTTON3).getChildByTag(TAG_BUTTON3).setString("复原");
					this.label3.setOpacity(255);
				},this));
				this.lamp2.runAction(seq);
			}else{
				this.gfire.setPosition(cc.p(500,332));
				this.hfire.setPosition(cc.p(440,340));
				this.huochai.setOpacity(255);
				this.huochai1.setOpacity(0);
				this.huochaigeng.setOpacity(255);
				this.huochaigeng1.setOpacity(0);
				this.lamp2.setPosition(cc.p(700,220));
				this.layout3.getChildByTag(TAG_BUTTON3).getChildByTag(TAG_BUTTON3).setString("播放");
			}
		break;
		}
	}
});
