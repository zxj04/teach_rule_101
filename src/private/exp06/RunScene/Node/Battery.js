exp06.Battery = cc.Node.extend({
	ctor:function(p){
		this._super();
		p.addChild(this, 10, TAG_BATTERY_NODE);
		this.init();
		//this.addbubble();
	},
	init : function(){
		this.setCascadeOpacityEnabled(true);
		var ammeter= new Button(this, 1, TAG_AMMETER, "#ammeter.png",this.callback);
		ammeter.setScale(0.8);

		var blackline= new Button(this, 8, TAG_LINE1, "#blackline.png",this.callback);
		blackline.setScale(0.3);
		blackline.setPosition(250,-250);
		
		var redline= new Button(this, 8, TAG_LINE2, "#redline.png",this.callback);
		redline.setScale(0.3);
		redline.setPosition(250,-250);

		ammeter1 = new cc.Sprite("#ammeter1.png");
		this.addChild(ammeter1,10);
		ammeter1.setScale(0.8);
		
		this.ammeterpoint = new cc.Sprite("#ammeterpoint.png");
		this.addChild(this.ammeterpoint,5);
		this.ammeterpoint.setScale(0.8);
		this.ammeterpoint.setPosition(2.5,0);

		this.h2so4 = new cc.Sprite("#h2so41.png");
		this.addChild(this.h2so4,5);
		this.h2so4.setScale(0.8);
		this.h2so4.setPosition(484,-79);
				
		this.hline = new cc.Sprite("#hline1.png");
		this.addChild(this.hline,10);
		this.hline.setScale(0.8);
		this.hline.setPosition(481,-67);
		
		this.zn= new Button(this, 7, TAG_ZN, "#znn.png",this.callback);
		this.zn.setScale(0.8);
		//this.zn.setPosition(522,-66);
		this.zn.setPosition(680,-100);
		this.zn.setRotation(90);
		
		this.cu= new Button(this, 6, TAG_CU, "#cuu.png",this.callback);
		this.cu.setScale(0.8);
		//this.cu.setPosition(444,-57.5);
		this.cu.setPosition(280,-100);
        this.cu.setRotation(90);




	},
	addbubble:function(){
//		var posx = 448 - Math.random(1)*5;
//		var posy = -75 - Math.random(1)*45;
		this.bubble = new cc.Sprite("#bubble2.png");
		this.bubble.setPosition(443,-80);
		this.bubble.setScale(0.3);
		this.addChild(this.bubble,11);
		var seq = cc.sequence(cc.delayTime(0.2),cc.scaleTo(1,0.6),cc.delayTime(0.5),cc.callFunc(function(){
			this.bubble.setScale(0.3);
		},this));
		this.bubble.runAction(seq.repeatForever());
		
		this.bubble1 = new cc.Sprite("#bubble2.png");
		this.bubble1.setPosition(448,-85);
		this.bubble1.setScale(0.3);
		this.addChild(this.bubble1,11);
		var seq1 = cc.sequence(cc.delayTime(0.4),cc.scaleTo(1,0.6),cc.delayTime(0.5),cc.callFunc(function(){
			this.bubble1.setScale(0.3);
		},this));
		this.bubble1.runAction(seq1.repeatForever());
		
		this.bubble2 = new cc.Sprite("#bubble2.png");
		this.bubble2.setPosition(443,-90);
		this.bubble2.setScale(0.3);
		this.addChild(this.bubble2,11);
		var seq2 = cc.sequence(cc.delayTime(0.6),cc.scaleTo(1,0.6),cc.delayTime(0.5),cc.callFunc(function(){
			this.bubble2.setScale(0.3);
		},this));
		this.bubble2.runAction(seq2.repeatForever());
		
		this.bubble3 = new cc.Sprite("#bubble2.png");
		this.bubble3.setPosition(448,-95);
		this.bubble3.setScale(0.3);
		this.addChild(this.bubble3,11);
		var seq3 = cc.sequence(cc.delayTime(0.8),cc.scaleTo(1,0.6),cc.delayTime(0.5),cc.callFunc(function(){
			this.bubble3.setScale(0.3);
		},this));
		this.bubble3.runAction(seq3.repeatForever());
		
		this.bubble4 = new cc.Sprite("#bubble2.png");
		this.bubble4.setPosition(443,-100);
		this.bubble4.setScale(0.3);
		this.addChild(this.bubble4,11);
		var seq4 = cc.sequence(cc.delayTime(0.2),cc.scaleTo(1,0.6),cc.delayTime(0.5),cc.callFunc(function(){
			this.bubble4.setScale(0.3);
		},this));
		this.bubble4.runAction(seq4.repeatForever());
		
		this.bubble5= new cc.Sprite("#bubble2.png");
		this.bubble5.setPosition(448,-105);
		this.bubble5.setScale(0.3);
		this.addChild(this.bubble5,11);
		var seq5 = cc.sequence(cc.delayTime(0.4),cc.scaleTo(1,0.6),cc.delayTime(0.5),cc.callFunc(function(){
			this.bubble5.setScale(0.3);
		},this));
		this.bubble5.runAction(seq5.repeatForever());
		
		this.bubble6 = new cc.Sprite("#bubble2.png");
		this.bubble6.setPosition(443,-110);
		this.bubble6.setScale(0.3);
		this.addChild(this.bubble6,11);
		var seq6 = cc.sequence(cc.delayTime(0.6),cc.scaleTo(1,0.6),cc.delayTime(0.5),cc.callFunc(function(){
			this.bubble6.setScale(0.3);
		},this));
		this.bubble6.runAction(seq6.repeatForever());
		
		this.bubble7 = new cc.Sprite("#bubble2.png");
		this.bubble7.setPosition(448,-115);
		this.bubble7.setScale(0.3);
		this.addChild(this.bubble7,11);
		var seq7 = cc.sequence(cc.delayTime(0.8),cc.scaleTo(1,0.6),cc.delayTime(0.5),cc.callFunc(function(){
			this.bubble7.setScale(0.3);
		},this));
		this.bubble7.runAction(seq7.repeatForever());
		
		this.bubble8 = new cc.Sprite("#bubble2.png");
		this.bubble8.setPosition(443,-120);
		this.bubble8.setScale(0.3);
		this.addChild(this.bubble8,11);
		var seq8 = cc.sequence(cc.delayTime(0.4),cc.scaleTo(1,0.6),cc.delayTime(0.5),cc.callFunc(function(){
			this.bubble8.setScale(0.3);
		},this));
		this.bubble8.runAction(seq8.repeatForever());
		
		this.bubble9 = new cc.Sprite("#bubble2.png");
		this.bubble9.setPosition(443,-130);
		this.bubble9.setScale(0.3);
		this.addChild(this.bubble9,11);
		var seq9 = cc.sequence(cc.delayTime(0.4),cc.scaleTo(1,0.6),cc.delayTime(0.5),cc.callFunc(function(){
			this.bubble9.setScale(0.3);
		},this));
		this.bubble9.runAction(seq9.repeatForever());

		

	},
	removebubble:function(){
		this.bubble.runAction(cc.fadeTo(1,0));
		this.bubble1.runAction(cc.fadeTo(1,0));
		this.bubble2.runAction(cc.fadeTo(1,0));
		this.bubble3.runAction(cc.fadeTo(1,0));
		this.bubble4.runAction(cc.fadeTo(1,0));
		this.bubble5.runAction(cc.fadeTo(1,0));
		this.bubble6.runAction(cc.fadeTo(1,0));
		this.bubble7.runAction(cc.fadeTo(1,0));
		this.bubble8.runAction(cc.fadeTo(1,0));
		this.bubble9.runAction(cc.fadeTo(1,0));
		

	},
	callback:function(p){
		var action = gg.flow.flow.action;
		var func = cc.callFunc(this.actionDone, this);	
		switch(p.getTag()){	
		case TAG_CU:
			var move = cc.moveTo(2,cc.p(444,102));
			var rota = cc.rotateTo(1,0);
			var sp = cc.spawn(move,rota);
			var move1 = cc.moveBy(0.5,cc.p(0,-95));
			var move2 = cc.moveTo(1,cc.p(444,-57.5));
			var seq = cc.sequence(sp,move1,cc.callFunc(function(){
				this.hline.setSpriteFrame("hline2.png");
			},this),move2,cc.callFunc(this.flowNext ,this));
			p.runAction(seq);
						
			var mov = cc.moveTo(2,cc.p(522,100));
			var rot = cc.rotateTo(1,0);
			var sp1 = cc.spawn(mov,rot);
			var mov1 = cc.moveTo(1,cc.p(522,-66));
			var seq1 = cc.sequence(sp1,move1.clone(),mov1);
			this.getChildByTag(TAG_ZN).runAction(seq1);						
		break;
		case TAG_LINE1:
			var move = cc.moveTo(1,cc.p(295,-152));
			var sca = cc.scaleTo(1,0.8);
			var sp = cc.spawn(move,sca);
			var seq = cc.sequence(cc.delayTime(0.8),sp,cc.callFunc(function(){
				p.setSpriteFrame("blackline1.png");				
			},this),cc.delayTime(0.5),cc.callFunc(function(){
				this.addbubble();
				this.showtip = new ShowTip("1、铜片附近有气泡产生" +
						"\n2、灵敏电流表指针偏移",cc.p(650,550));
			},this));
			p.runAction(seq);
						
			var move1 = cc.moveTo(1,cc.p(250,-90));
			var sp1 = cc.spawn(move1,sca.clone());	
			var seq1 = cc.sequence(sp1,cc.callFunc(function(){
				this.getChildByTag(TAG_LINE2).setSpriteFrame("redline1.png");
			},this));
            this.getChildByTag(TAG_LINE2).runAction(seq1);
                        
            var rota = cc.rotateBy(0.2,14);42
            var rota1 = cc.rotateBy(0.4,14);
            var rota2 = cc.rotateBy(0.6,14);
            var rota3 = cc.rotateTo(2,0);
            var sq = cc.sequence(cc.delayTime(2.5),rota,rota1,rota2,cc.delayTime(5),cc.callFunc(function(){
            	this.removebubble();
            },this),rota3,cc.callFunc(function(){
            	this.flowNext();
            },this));
            this.ammeterpoint.runAction(sq);
            break;
            
            
		}
	},
	flowNext:function(){
		gg.flow.next();
	},
});