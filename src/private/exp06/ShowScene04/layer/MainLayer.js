exp06.ShowMainLayer04 = cc.Layer.extend({
	lead:null,
	real:null,
	reset:true,
	reset1:true,
	ctor:function () {
		this._super();
		this.init();
		return true;
	},
	init:function(){
		var sv = new pub.ScrollView(this);
		sv.setOffestPos(cc.p(152,45 + 15));

		this.addlayout1(sv);
		this.addlayout2(sv);
		this.addlayout3(sv);
	},	
	addlayout1:function(parent){
		this.layout1 = new ccui.Layout();//第一页
		this.layout1.setContentSize(cc.size(996, 598));//设置layout的大小
		parent.addLayer(this.layout1);

		var text = $.format("　　我们常见的天然气燃烧和木柴燃烧都是放出热量的，那其他化学反应也能放出热量吗？" +
				"有没有能使反应温度降低的化学反应呢？", 960, 30);
		label1 = new cc.LabelTTF(text,gg.fontName,gg.fontSize4);
		label1.setAnchorPoint(0,0.5);
		label1.setPosition(0,this.layout1.height-label1.height/2);
		label1.setColor(cc.color(0, 0, 0, 250));
		this.layout1.addChild(label1);	

		this.addshowimage(this.layout1,"#tianranqi.png",cc.p(250,300),"天然气燃烧");
		this.addshowimage(this.layout1,"#shaohua.png",cc.p(750,300),"木柴燃烧");				
	},
	addlayout2:function(parent){
		this.layout2 = new ccui.Layout();//第一页
		this.layout2.setContentSize(cc.size(996, 598));//设置layout的大小
		parent.addLayer(this.layout2);

		var text = $.format("　　将一段镁条放入试管中，往试管里加入少量稀盐酸，观察现象，用手触摸试管外壁。", 960, 30);
		label1 = new cc.LabelTTF(text,gg.fontName,gg.fontSize4);
		label1.setAnchorPoint(0,0.5);
		label1.setPosition(0,this.layout2.height-label1.height/2);
		label1.setColor(cc.color(255, 255, 255, 250));
		this.layout2.addChild(label1,5);	

		var bg = new cc.Sprite(res_public.run_back);
		this.layout2.addChild(bg);
		bg.setScale(0.8);
		bg.setPosition(cc.p(this.layout2.width /2, this.layout2.height / 2 +30));
		
		this.shiguan = new cc.Sprite("#2/sg.png");
		this.shiguan.setPosition(cc.p(450,300));
		this.layout2.addChild(this.shiguan,5);
		
		this.mg = new cc.Sprite("#2/mg.png");
		this.mg.setPosition(cc.p(450,160));
		this.mg.setAnchorPoint(0.5,0);
		this.mg.setScale(1,0.6);
		this.layout2.addChild(this.mg,4);
		
		this.water = new cc.Sprite("#2/water.png");
		this.water.setPosition(cc.p(18,20));
		this.shiguan.addChild(this.water,4);
		this.water.setOpacity(0);
		
		this.re = new cc.Sprite("#2/re.png");
		this.re.setPosition(cc.p(530,210));
		this.layout2.addChild(this.re,4);
		this.re.setScale(0.5);
		this.re.setOpacity(0);
		
		this.paomo = new cc.Sprite("#2/paomo.png");
		this.paomo.setPosition(cc.p(15,82));
		this.shiguan.addChild(this.paomo,5);
		this.paomo.setScale(0.65,1);
		this.paomo.setOpacity(0);
		
		this.paomo1 = new cc.Sprite("#2/paomo.png");
		this.paomo1.setPosition(cc.p(16,62));
		this.shiguan.addChild(this.paomo1,4);
		this.paomo1.setScale(0.8);
		this.paomo1.setOpacity(0);
		this.paomo1.runAction(this.addAction("2/", 2, 0.1));
			
		this.shiguan1 = new cc.Sprite("#2/sg.png");
		this.shiguan1.setPosition(cc.p(600,300));
		this.layout2.addChild(this.shiguan1,5);
	
		this.water1 = new cc.Sprite("#2/water.png");
		this.water1.setPosition(cc.p(18,100));
		this.shiguan1.addChild(this.water1,4);
		
		this.addbutton(this.layout2, TAG_BUTTON1, "播放", cc.p(250,180));
		this.layout2.getChildByTag(TAG_BUTTON1).getChildByTag(TAG_BUTTON1).setColor(cc.color(255, 255, 255, 250));

		this.label1 = new cc.LabelTTF("现象：用手触摸试管外壁，感觉试管壁温度升高。" +
				"\n结论：说明有些化学反应是放出热量的。",gg.fontName,gg.fontSize4);
		this.label1.setAnchorPoint(0,0.5);
		this.label1.setPosition(80,80);
		this.label1.setColor(cc.color(255, 0, 0, 250));
		this.layout2.addChild(this.label1,10);
		this.label1.setVisible(false);
	},
	addlayout3:function(parent){
		this.layout3 = new ccui.Layout();//第一页
		this.layout3.setContentSize(cc.size(996, 598));//设置layout的大小
		parent.addLayer(this.layout3);

		var text = $.format("　　在玻璃片上撒上少量水，并将装有氢氧化钡粉末的小烧杯放上玻璃片上，" +
				"往烧杯中加入氯化铵晶体，并用玻璃棒迅速搅拌，静置片刻，提起小烧杯，观察现象。", 960, 30);
		label1 = new cc.LabelTTF(text,gg.fontName,gg.fontSize4);
		label1.setAnchorPoint(0,0.5);
		label1.setPosition(0,this.layout3.height-label1.height/2);
		label1.setColor(cc.color(255, 255, 255, 250));
		this.layout3.addChild(label1,5);	
		
		var bg = new cc.Sprite(res_public.run_back);
		this.layout3.addChild(bg);
		bg.setScale(0.8);
		bg.setPosition(cc.p(this.layout3.width /2, this.layout3.height / 2 +30));

		this.beaker = new cc.Sprite("#3/beaker.png");
		this.beaker.setPosition(cc.p(500,250));
		this.layout3.addChild(this.beaker,5);
		this.beaker.setScale(0.65);
		
		this.beaker1 = new cc.Sprite("#3/beaker1.png");
		this.beaker1.setPosition(cc.p(505,240));
		this.layout3.addChild(this.beaker1,5);
		this.beaker1.setScale(0.65);
		
		this.rod = new cc.Sprite("#3/rod.png");
		this.rod.setPosition(cc.p(40,300));
		this.beaker.addChild(this.rod,5);
		this.rod.setRotation(-100);
		this.rod.setScale(0.7);
		
		this.nhcl = new cc.Sprite("#3/nhcl.png");
		this.nhcl.setPosition(cc.p(500,380));
		this.layout3.addChild(this.nhcl,5);
		this.nhcl.setScale(0.35);
		this.nhcl.setOpacity(0);
		
		this.mohu = new cc.Sprite("#3/mohu.png");
		this.mohu.setPosition(cc.p(495,250));
		this.layout3.addChild(this.mohu,15);
		this.mohu.setScale(0.48,0.5);
		this.mohu.setOpacity(0);

		this.baoh = new cc.Sprite("#3/baoh.png");
		this.baoh.setPosition(cc.p(110,95));
		this.beaker.addChild(this.baoh,5);
		this.baoh.setScale(0.73);

		this.yeti = new cc.Sprite("#3/yeti.png");
		this.yeti.setPosition(cc.p(110,103));
		this.beaker.addChild(this.yeti,5);
		this.yeti.setScale(0.74);
		this.yeti.setOpacity(0);
				
		this.glass = new cc.Sprite("#3/glass.png");
		this.glass.setPosition(cc.p(300,180));
		this.layout3.addChild(this.glass,3);
		this.glass.setScale(0.65);
		
		this.glass2 = new cc.Sprite("#3/glass2.png");
		this.glass2.setPosition(cc.p(179,50));
		this.glass.addChild(this.glass2,5);
		this.glass2.setOpacity(0);
		
		this.gwater = new cc.Sprite("#3/gwater.png");
		this.gwater.setPosition(cc.p(170,50));
		this.glass.addChild(this.gwater,5);
		
		this.paper = new cc.Sprite("#3/paper1.png");
		this.paper.setPosition(cc.p(750,300));
		this.layout3.addChild(this.paper,4);
		this.paper.setScale(2);
		
		this.nhcl1 = new cc.Sprite("#3/nhcl1.png");
		this.nhcl1.setPosition(cc.p(50,45));
		this.paper.addChild(this.nhcl1,5);
		this.nhcl1.setScale(0.25);
		
		this.leng = new cc.Sprite("#3/leng.png");
		this.leng.setPosition(cc.p(380,250));
		this.layout3.addChild(this.leng,4);
		this.leng.setScale(0.5);
		this.leng.setOpacity(0);
		
		this.addbutton(this.layout3, TAG_BUTTON2, "播放", cc.p(700,180));
		this.layout3.getChildByTag(TAG_BUTTON2).getChildByTag(TAG_BUTTON2).setColor(cc.color(255, 255, 255, 250));

		this.label2 = new cc.LabelTTF("现象：氢氧化钡与氯化铵反应吸收热量，" +
				"\n　　　玻璃片上的水将烧杯与玻璃片凝结住。" +
				"\n结论：说明有些化学反应是吸收热量的。",gg.fontName,gg.fontSize4);
		this.label2.setAnchorPoint(0,0.5);
		this.label2.setPosition(80,95);
		this.label2.setColor(cc.color(255, 0, 0, 250));
		this.layout3.addChild(this.label2,10);
		this.label2.setVisible(false);
	},
	addAction:function(str1,num,time){
		var frames = [];
		for(var i =1 ;i<=num;i++){
			var str = str1+i+".png";
			var frame=cc.spriteFrameCache.getSpriteFrame(str);
			frames.push(frame);		
		}
		var animation = new cc.Animation(frames,time);//负责动画序列
		var action = new cc.Animate(animation);//帧动画的动作创建
		return action.repeatForever();
	},
	moveSg:function(){
		this.layout2.getChildByTag(TAG_BUTTON1).setEnable(false);
		var move = cc.moveTo(1,cc.p(625,485));
		var rota = cc.rotateTo(1,-100);
		var seq = cc.sequence(move,cc.callFunc(function(){
			this.shiguan.runAction(cc.rotateTo(0.5,10));
			this.water.runAction(cc.rotateTo(0.5,-10));
			this.mg.runAction(cc.spawn(cc.rotateTo(0.5,12),cc.moveTo(0.5,cc.p(425,160))));
			this.water1.runAction(cc.sequence(cc.rotateTo(1,50),cc.scaleTo(1,1.2)));
		},this),rota,cc.callFunc(function(){
			this.water.setOpacity(255);
			this.water.runAction(cc.moveTo(1.5,cc.p(18,100)));
			var se = cc.sequence(cc.moveTo(1,cc.p(18,25)),cc.spawn(cc.moveTo(0.5,cc.p(10,15)),cc.scaleTo(0.5,0)));
			this.water1.runAction(se);
		},this),cc.delayTime(1.5),cc.callFunc(function(){
			this.paomo1.setOpacity(255);
			this.paomo.runAction(cc.fadeIn(4));
			this.shiguan.runAction(cc.rotateTo(0.5,0));
			this.water.runAction(cc.rotateTo(0.5,0));
			var seq1 = cc.sequence(cc.spawn(cc.rotateTo(0.5,0),cc.moveTo(0.5,cc.p(450,160))),cc.delayTime(1),cc.scaleTo(80,0.2));
			this.mg.runAction(seq1);
			this.re.runAction(cc.fadeIn(4));
		},this),cc.delayTime(6),cc.callFunc(function(){
			this.layout2.getChildByTag(TAG_BUTTON1).getChildByTag(TAG_BUTTON1).setString("复原");
			this.layout2.getChildByTag(TAG_BUTTON1).setEnable(true);
			this.label1.setVisible(true);
		},this));
		this.shiguan1.runAction(seq);
	},
	movebaeker:function(){
		this.layout3.getChildByTag(TAG_BUTTON2).setEnable(false);
		var move = cc.moveBy(0.5,cc.p(0,50));
		var move1 = cc.moveBy(0.5,cc.p(0,-50));
		var seq = cc.sequence(move,cc.delayTime(0.5),move1);
		this.beaker.runAction(seq);
		this.beaker1.runAction(seq.clone());
		
		this.glass.runAction(cc.moveTo(1,cc.p(500,180)));
	},
	movePaper:function(){
		var move = cc.moveTo(1,cc.p(600,400));
		var rota = cc.rotateTo(0.5,-20);
		var seq = cc.sequence(move,rota,cc.callFunc(function(){
			this.nhcl1.runAction(cc.moveTo(0.5,cc.p(20,45)));
		},this),cc.delayTime(0.5),cc.callFunc(function(){
			this.paper.setVisible(false);
			this.nhcl.setOpacity(255);
			this.nhcl.runAction(cc.spawn(cc.moveTo(1,cc.p(500,230)),cc.scaleTo(1,0.45)));
		},this),cc.delayTime(1),cc.callFunc(function(){
			this.mohu.runAction(cc.fadeIn(5));
			this.yeti.runAction(cc.fadeIn(5));
			this.nhcl.runAction(cc.fadeOut(5));
			this.baoh.runAction(cc.fadeOut(5));
		},this));
		this.paper.runAction(seq);
		
		var rota1 = cc.rotateTo(0.5,-90);
		var rota2 = cc.rotateTo(0.5,-100);
		var mov = cc.moveTo(0.5,cc.p(190,300));
		var mov1 = cc.moveTo(0.5,cc.p(40,300));
		var se = cc.sequence(cc.delayTime(2),rota1,mov,mov1,mov,mov1,cc.callFunc(function(){
			this.glass2.runAction(cc.fadeIn(3));
		},this),mov,mov1,mov,mov1,mov,mov1,rota2);
		this.rod.runAction(se);
	},
	addshowimage:function(parent,str,pos,str2){
		var showimage = new cc.Sprite(str);
		showimage.setPosition(pos);
		parent.addChild(showimage,5);
		var label = new cc.LabelTTF(str2,gg.fontName,gg.fontSize);
		label.setColor(cc.color(0, 0, 0, 250));
		parent.addChild(label);
		label.setPosition(showimage.x,showimage.y - showimage.height * 0.5 -20);
	},
	addImage:function(parent,str,pos,scale){
		var image = new cc.Sprite(str);
		parent.addChild(image,5);
		image.setPosition(pos);
		if(scale !=null){
			image.setScale(scale);
		}else{
			image.setScale(1);
		}
	},
	addbutton:function(parent,tag,str,pos){
		var button = new ButtonScale(parent,"#unsel.png",this.callback,this);
		button.setPosition(pos);
		button.setLocalZOrder(5);
		button.setTag(tag);
		var label = new cc.LabelTTF(str,gg.fontName,30);
		label.setPosition(button.width/2,button.height/2);		
		label.setColor(cc.color(0, 0, 0, 250));
		button.addChild(label);
		label.setTag(tag);
	},
	callback:function(p){
		switch(p.getTag()){	
		case TAG_BUTTON1:
			this.reset = !this.reset;
			if(!this.reset){
				this.moveSg();
			}else{
				this.layout2.getChildByTag(TAG_BUTTON1).getChildByTag(TAG_BUTTON1).setString("播放");
				this.mg.stopAllActions();
				this.mg.setScale(1,0.6);
				this.water.setPosition(cc.p(18,20));
				this.water.setOpacity(0);
				this.shiguan1.setPosition(cc.p(600,300));
				this.shiguan1.setRotation(0);
				this.water1.setPosition(cc.p(18,100));
				this.water1.setRotation(0);
				this.water1.setScale(1);
				this.re.setOpacity(0);
				this.paomo.setOpacity(0);
				this.paomo1.setOpacity(0);
			}
			break;
		case TAG_BUTTON2:			
			this.reset1 = !this.reset1;
			if(!this.reset1){
				var seq = cc.sequence(cc.callFunc(function(){
					this.movebaeker();
				},this),cc.delayTime(2),cc.callFunc(function(){
					this.movePaper();
				},this),cc.delayTime(10),cc.callFunc(function(){
					var move = cc.moveBy(0.5,cc.p(0,50));
					this.beaker.runAction(move);
					this.beaker1.runAction(move.clone());
					this.glass.runAction(move.clone());
					this.mohu.runAction(move.clone());
					this.leng.setOpacity(255);
				},this),cc.delayTime(0.5),cc.callFunc(function(){
					this.layout3.getChildByTag(TAG_BUTTON2).getChildByTag(TAG_BUTTON2).setString("复原");
					this.layout3.getChildByTag(TAG_BUTTON2).setEnable(true);
					this.label2.setVisible(true);
				},this));
				p.runAction(seq);
			}else{
				this.layout3.getChildByTag(TAG_BUTTON2).getChildByTag(TAG_BUTTON2).setString("播放");
				this.paper.setVisible(true);
				this.paper.setRotation(0);
				this.paper.setPosition(cc.p(700,300));
				this.mohu.setOpacity(0);
				this.mohu.setPosition(cc.p(495,250));
				this.yeti.setOpacity(0);
				this.baoh.setOpacity(255);
				this.nhcl.setScale(0.35);
				this.nhcl.setPosition(cc.p(500,380));
				this.nhcl1.setPosition(cc.p(50,45));
				this.glass.setPosition(300,180);
				this.glass2.setOpacity(0);
				this.beaker.setPosition(cc.p(500,250));
				this.beaker1.setPosition(cc.p(505,240));
				this.leng.setOpacity(0);
			}
			break;
		}
	}	
});
