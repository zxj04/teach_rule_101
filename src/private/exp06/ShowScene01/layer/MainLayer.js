exp06.ShowMainLayer01 = cc.Layer.extend({
	lead:null,
	real:null,
	reset:true,
	reset1:false,
	open_flag:false,
	donum:0,
	ctor:function () {
		this._super();
		this.init();
		return true;
	},
	init:function(){	
		var sv = new pub.ScrollView(this);
		sv.setOffestPos(cc.p(152,45 + 15));

		this.addlayout1(sv);	
		this.addlayout2(sv);	
	},	
	addlayout1:function(parent){
		this.layout1 = new ccui.Layout();//第一页
		this.layout1.setContentSize(cc.size(996, 598));//设置layout的大小
		parent.addLayer(this.layout1);
	
		var text = $.format("　　为什么切开的苹果，放一会儿表面变成褐色；铁钉放在暴露的空气中，时间长了会生锈，这是为什么呢？" , 980,30);
		label = new cc.LabelTTF(text,gg.fontName,gg.fontSize4);
		label.setColor(cc.color(0, 0, 0, 250));
		label.setAnchorPoint(0, 0.5);
		label.setPosition(0,this.layout1.height-label.height/2);
		this.layout1.addChild(label);	

		this.addImage(this.layout1,"#pingguo.png",cc.p(250,360));
		this.addImage(this.layout1,"#shengxiu.png",cc.p(750,360));
		
		this.yanghua = new cc.Sprite("#pingguo1.png");
		this.yanghua.setPosition(cc.p(248,326));
		this.layout1.addChild(this.yanghua,5);
		this.yanghua.setOpacity(0);

		this.addbutton(this.layout1, TAG_BUTTON, "播放", cc.p(500,200));

		var text = $.format("　　原因是苹果细胞内的一些物质与氧气发生反应，生成了某些褐色物质；铁钉生锈也是因为铁与氧气等物质放生化学反应。", 980,30);
		label = new cc.LabelTTF(text,gg.fontName,gg.fontSize4);
		label.setColor(cc.color(0, 0, 0, 250));
		label.setAnchorPoint(0, 0.5);
		label.setPosition(0,100);
		this.layout1.addChild(label);							
	},
	addlayout2:function(parent){
	    this.layout2 = new ccui.Layout();//第一页
	    this.layout2.setContentSize(cc.size(996, 598));//设置layout的大小
	    parent.addLayer(this.layout2);

	    var text = $.format("　　塑料和橡胶制品的老化、铜和铁等金属表面生锈，这些氧化反应进行的非常缓慢，甚至在短期内不易察觉，称之为缓慢氧化。" +
	    		"有些物质反应剧烈，比如物质燃烧等，可称之为剧烈氧化。" +
	    		"\n　　氧化反应中，氧气具有氧化性。", 980,30);
	    label = new cc.LabelTTF(text,gg.fontName,gg.fontSize4);
	    label.setColor(cc.color(0, 0, 0, 250));
	    label.setAnchorPoint(0, 0.5);
	    label.setPosition(0,this.layout2.height-label.height/2);
	    this.layout2.addChild(label);	
	     
	    this.addshowimage(this.layout2,"#xiangjiao.png",cc.p(250,250),"缓慢氧化");
	    this.addshowimage(this.layout2,"#ranshao.png",cc.p(750,250),"剧烈氧化");
        

	},
	addImage:function(parent,str,pos){
		var image = new cc.Sprite(str);
		parent.addChild(image,5);
		image.setPosition(pos);
	},
	addshowimage:function(parent,str,pos,str2){
		var showimage = new cc.Sprite(str);
		showimage.setPosition(pos);
		parent.addChild(showimage,5);
		var label = new cc.LabelTTF(str2,gg.fontName,gg.fontSize);
		label.setColor(cc.color(0, 0, 0, 250));
		parent.addChild(label);
		label.setPosition(showimage.x,showimage.y - showimage.height * 0.5 -20);
	},
	addbutton:function(parent,tag,str,pos){
		var button = new ButtonScale(parent,"#unsel.png",this.callback,this);
		button.setPosition(pos);
		button.setLocalZOrder(5);
		button.setTag(tag);
		var label = new cc.LabelTTF(str,gg.fontName,30);
		label.setPosition(button.width/2,button.height/2);		
		label.setColor(cc.color(0, 0, 0, 250));
		button.addChild(label);
		label.setTag(tag);
	},
	callback:function(p){
		switch(p.getTag()){	
		case TAG_BUTTON:
			this.reset = !this.reset;
			if(!this.reset){
				p.setEnable(false);
				var seq = cc.sequence(cc.fadeIn(3),cc.callFunc(function(){
					p.setEnable(true);
					this.layout1.getChildByTag(TAG_BUTTON).getChildByTag(TAG_BUTTON).setString("复原");
				},this));
				this.yanghua.runAction(seq);
			}else{
				this.layout1.getChildByTag(TAG_BUTTON).getChildByTag(TAG_BUTTON).setString("播放");
				this.yanghua.setOpacity(0);
			}


			break;
		}
	}
});
