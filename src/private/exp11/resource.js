var exp11 = exp11||{};
//start
exp11.res_start = {
	//	start_png :"res/private/exp11/start/startpng.png"
};

exp11.g_resources_start = [];
for (var i in exp11.res_start) {
	exp11.g_resources_start.push(exp11.res_start[i]);
}

// run
exp11.res_run = {
		run_p1 : "res/private/exp11/run.plist",
		run_g1 : "res/private/exp11/run.png",
		run_p2 : "res/private/exp11/run2.plist",
		run_g2 : "res/private/exp11/run2.png"
};

exp11.g_resources_run = [];
for (var i in exp11.res_run) {
	exp11.g_resources_run.push(exp11.res_run[i]);
}

// show02
exp11.res_show = {
		show_p : "res/private/exp11/show.plist",
		show_g : "res/private/exp11/show.png",
		show1 : "res/private/exp11/show1.jpg",
		show2 : "res/private/exp11/show2.jpg",
		show3 : "res/private/exp11/show3.jpg",
		show11 : "res/private/exp11/show11.jpg",
		show12 : "res/private/exp11/show12.jpg",
		show13 : "res/private/exp11/show13.jpg",
		show14 : "res/private/exp11/show14.jpg",
		show15 : "res/private/exp11/show15.jpg",
		gen : "res/private/exp11/gen.png"
};

exp11.g_resources_show = [];
for (var i in exp11.res_show) {
	exp11.g_resources_show.push(exp11.res_show[i]);
}

