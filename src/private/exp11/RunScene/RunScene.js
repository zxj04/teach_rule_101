exp11.RunLayer01 =  cc.Layer.extend({
	backgroundLayer : null,
	mainLayar : null,
	ctor:function () {
		this._super();
		this.initFrames();
		this.loadBackground();
		this.loadMainLayer();
	},
	initFrames : function(){
		cc.spriteFrameCache.addSpriteFrames(exp11.res_run.run_p1);
		gg.curRunSpriteFrame.push(exp11.res_run.run_p1);
	},
	loadBackground : function(){
		this.backgroundLayer = new RunBackgroundLayer();
		this.addChild(this.backgroundLayer);
	},
	loadMainLayer : function(){
		this.mainLayar = new exp11.RunMainLayer01();
		this.addChild(this.mainLayar);
	}
});

exp11.RunScene01 = PScene.extend({
	onEnter:function () {
		this._super();
		gg.initTeach();
		var layer = new exp11.RunLayer01();
		this.addChild(layer);
	}
});
