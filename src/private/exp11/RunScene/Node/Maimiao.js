exp11.Maimiao = cc.Node.extend({
	ctor:function(p){
		this._super();
		p.addChild(this, 10, TAG_MAIMIAO_NODE);
		this.init();
	},
	init : function(){
		this.setCascadeOpacityEnabled(true);
		this.beaker= new Button(this, 1, TAG_BEAKER, "#beaker2.png",this.callback);
		this.beaker.setScale(0.8);

		
		this.beaker1= new cc.Sprite("#beaker1.png");
		this.addChild(this.beaker1,10);
		this.beaker1.setPosition(0,0.5);
		this.beaker1.setScale(0.8);
		
		this.beakerline= new cc.Sprite("#beakerline.png");		
		this.beakerline.setPosition(8,35);
		this.beakerline.setScale(0.8);
		this.addChild(this.beakerline,8);
		
		this.maimiao= new Button(this, 7, TAG_MAIMIAO, "#maimiao1.png",this.callback);
		this.maimiao.setScale(0.6);
		this.maimiao.setPosition(0,110);
		this.gen = new cc.Sprite("#gen.png");
		this.gen.setPosition(161,6.5);
		this.maimiao.addChild(this.gen);
		
		this.label = new cc.LabelTTF("①","微软雅黑",30);
		this.label.setPosition(100,160);
		this.beaker1.addChild(this.label);

				
		this.beaker2= new Button(this, 1, TAG_BEAKER2, "#beaker2.png",this.callback);
		this.beaker2.setScale(0.8);
		this.beaker2.setPosition(300,0);
		
		this.beaker3= new cc.Sprite("#beaker1.png");
		this.addChild(this.beaker3,10);
		this.beaker3.setPosition(300,0.5);
		this.beaker3.setScale(0.8);
		
		this.beakerline1 = new cc.Sprite("#beakerline.png");		
		this.beakerline1.setPosition(308,35);
		this.beakerline1.setScale(0.8);
		this.addChild(this.beakerline1,8);
		
		this.maimiao1= new Button(this, 7 , TAG_MAIMIAO1, "#maimiao1.png",this.callback);
		this.maimiao1.setScale(0.6);
		this.maimiao1.setPosition(300,110);
		this.gen1 = new cc.Sprite("#gen.png");
		this.gen1.setPosition(161,6.5);
		this.maimiao1.addChild(this.gen1);
		
		this.label1 = new cc.LabelTTF("②","微软雅黑",30);
		this.label1.setPosition(100,160);
		this.beaker3.addChild(this.label1);
		
		this.clipper= new Button(this, 1 , TAG_CLIPPER, "#clipper1.png",this.callback);
		this.clipper.setScale(0.5);
		this.clipper.setRotation(180);
		this.clipper.setPosition(200,350);
		
		this.clipper1 = new cc.Sprite("#clipper2.png");
		this.clipper1.setScale(0.5);
		this.clipper1.setRotation(180);
		this.clipper1.setPosition(200,350);
		this.addChild(this.clipper1,10);				
	},
	bianhua:function(time){	
		var maimiao = new cc.Sprite("#maimiao2.png");
		maimiao.setOpacity(0);
		maimiao.setScale(0.6);
		maimiao.setPosition(5,76);
		this.addChild(maimiao,7);
		maimiao.runAction(cc.fadeIn(time));		
		this.maimiao.runAction(cc.fadeOut(time));
		
		var maimiao1 = new cc.Sprite("#maimiao3.png");
		maimiao1.setOpacity(0);
		maimiao1.setScale(0.6);
		maimiao1.setPosition(312,120);
		this.addChild(maimiao1,7);
		maimiao1.runAction(cc.fadeIn(time));		
		this.maimiao1.runAction(cc.fadeOut(time));
		this.gen1.runAction(cc.fadeOut(time));
		
		
	},
	callback:function(p){
		var action = gg.flow.flow.action;
		var func = cc.callFunc(this.actionDone, this);	
		switch(p.getTag()){		
		case TAG_MAIMIAO:
			if(action == ACTION_DO1){
				var move = cc.moveTo(1,cc.p(-15,300));
				p.runAction(move);
				
				var mov = cc.moveTo(1,cc.p(80,170));
				var rota = cc.rotateTo(1,160);
				var sp = cc.spawn(mov,rota);
				var rota1 = cc.rotateTo(0.5,180);
				
				var seq = cc.sequence(sp,cc.delayTime(0.2),rota1,cc.callFunc(function(){
					this.gen.removeFromParent(true);
					this.clipper.removeFromParent(true);
					this.clipper1.removeFromParent(true);
					this.flowNext();
				},this));
				this.clipper.runAction(mov.clone());
				this.clipper1.runAction(seq);			
			}else if(action == ACTION_DO2){
				var move = cc.moveTo(1,cc.p(0,110));
				var seq = cc.sequence(cc.callFunc(function(){
					this.showTip = new ShowTip("在麦苗的剪切面涂上石蜡，防" +
							"\n止剪切面吸水影响实验结果",cc.p(250,300));
				},this),cc.delayTime(0.5),move,cc.callFunc(function(){
					this.flowNext();					
				},this));
				p.runAction(seq);
				
				
			}
		break;
		case TAG_BEAKER:
			var seq = cc.sequence(cc.callFunc(function(){
				this.bianhua(5);
			},this),cc.delayTime(3),cc.callFunc(function(){
				this.showTip = new ShowTip("1、1号烧杯中的麦苗萎蔫" +
						"\n2、2号烧杯中的麦苗生长良好" +
						"\n3、证明根尖是植物根吸水的主要部分",cc.p(450,450));
			},this),cc.delayTime(5),cc.callFunc(this.flowNext ,this));
			p.runAction(seq);
		break;
		
		}
	},
	flowNext:function(){
		gg.flow.next();
	},
});