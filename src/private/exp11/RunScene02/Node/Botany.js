exp11.Botany = cc.Node.extend({
	runningAction:null,
	ctor:function(p){
		this._super();
		p.addChild(this, 9,TAG_BOTANY);
		this.init();
		this.initAction();
	},
	init : function(){
		this.setCascadeOpacityEnabled(true);

		var fontDef = new cc.FontDefinition();
		fontDef.fontName = "Arial";
		fontDef.fontSize = "20";
		fontDef.fillStyle=cc.color(255, 252, 0,255);

		this.botany=new Button(this,5,TAG_BOTANY1,"#botany1.png",this.callback,this);
		this.botany.setPosition(0,0);
		
		this.label=new cc.LabelTTF("植物一",fontDef);
		this.botany.addChild(this.label);
		this.label.setPosition(this.botany.width*0.5,-this.botany.height*0.05);
		
		this.botany1=new Button(this,5,TAG_BOTANY2,"#botany1.png",this.callback,this);
		this.botany1.setPosition(150,0);
		
		this.label1=new cc.LabelTTF("植物二",fontDef);
		this.botany1.addChild(this.label1);
		this.label1.setPosition(this.botany1.width*0.5,-this.botany1.height*0.05);
		
		this.botany2=new Button(this,5,TAG_BOTANY3,"#botany1.png",this.callback,this);
		this.botany2.setPosition(300,0);
		
		this.label2=new cc.LabelTTF("植物三",fontDef);
		this.botany2.addChild(this.label2);
		this.label2.setPosition(this.botany2.width*0.5,-this.botany2.height*0.05);
		
		this.botany3=new Button(this,5,TAG_BOTANY4,"#botany1.png",this.callback,this);
		this.botany3.setPosition(450,0);
		
		this.label3=new cc.LabelTTF("植物四",fontDef);
		this.botany3.addChild(this.label3);
		this.label3.setPosition(this.botany3.width*0.5,-this.botany3.height*0.05);
	},
	initAction:function(){
		var animFrames=[];
		for(var i=1;i<5;i++){
			var str="botany"+i+".png";
			var frame=cc.spriteFrameCache.getSpriteFrame(str);
			animFrames.push(frame);
		}
		var animation=new cc.Animation(animFrames,0.3)
		this.runningAction=cc.repeat(cc.animate(animation),1);
		this.runningAction.retain();
	},
	runBotany3:function(){
		this.botany2.runAction(this.runningAction);
	},
	callback:function(p){
		var func=cc.callFunc(function() {
			gg.flow.next();
		}, this);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           
		switch (p.getTag()) {
		case TAG_BOTANY1:
			var ber=cc.bezierBy(1, [cc.p(-50, 380),cc.p(-300,380),cc.p(-310,120)]);
			var rotate=cc.rotateBy(1,-3);
			p.runAction(cc.sequence(cc.callFunc(function() {
				this.label.removeFromParent(true);
			},this),ber,rotate,func));
			break;                                                                                                                                                                                                                                       
		case TAG_BOTANY2:
			var ber=cc.bezierBy(1, [cc.p(-50, 380),cc.p(-443,380),cc.p(-443,120)]);
			var rotate=cc.rotateBy(1,3);
			p.runAction(cc.sequence(cc.callFunc(function() {
				this.label1.removeFromParent(true);
			},this),ber,rotate,func));
			break;
		case TAG_BOTANY3:
			var ber=cc.bezierBy(1, [cc.p(-50, 380),cc.p(-460,380),cc.p(-460,120)]);
			var rotate=cc.rotateBy(1,-3);
			p.runAction(cc.sequence(cc.callFunc(function() {
				this.label2.removeFromParent(true);
			},this),ber,rotate,func));
			break;
		case TAG_BOTANY4:
			var ber=cc.bezierBy(1, [cc.p(-50, 380),cc.p(-593,380),cc.p(-593,120)]);
			var rotate=cc.rotateBy(1,3);
			p.runAction(cc.sequence(cc.callFunc(function() {
				this.label3.removeFromParent(true);
			},this),ber,rotate,cc.spawn(cc.spawn(this.runningAction.clone(),cc.callFunc(function() {
				this.runBotany3();
				this.showTip=new ShowTip("装有食盐水的锥形瓶中的植物枯焉",cc.p(680,660),true);
			}, this)),cc.sequence(cc.callFunc(function() {
				ll.run.clock.doing();
			}, this),cc.delayTime(4),cc.callFunc(function() {
				ll.run.clock.stop();
			}, this))),cc.delayTime(2),func));
			break;
		default:
			break;
		}
	}
});