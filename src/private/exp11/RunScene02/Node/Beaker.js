exp11.Beaker = cc.Node.extend({
	ctor:function(p){
		this._super();
		p.addChild(this, 10,TAG_BEAKER_1);
		this.init();
	},
	init : function(){
		this.setCascadeOpacityEnabled(true);

		var fontDef = new cc.FontDefinition();
		fontDef.fontName = "Arial";
		fontDef.fontSize = "25";
		fontDef.fillStyle=cc.color(255, 252, 0,255);

		this.beaker=new Button(this,5,TAG_BEAKER1,"#beaker.png",this.callback,this);
		this.beaker.setPosition(0,0);
		
		this.line=new Button(this.beaker,5,TAG_LINE1,"#line.png",this.callback,this);
		this.line.setPosition(this.beaker.width*0.5,this.beaker.height*0.6);
		
		this.label=new cc.LabelTTF("蒸馏水" +
				"\n①",fontDef);
		this.beaker.addChild(this.label);
		this.label.setPosition(this.beaker.width*0.5,-this.beaker.height*0.15);
		
		this.beaker1=new Button(this,5,TAG_BEAKER2,"#beaker.png",this.callback,this);
		this.beaker1.setPosition(150,0);
		
		this.line1=new Button(this.beaker1,5,TAG_LINE2,"#line.png",this.callback,this);
		this.line1.setPosition(this.beaker1.width*0.5,this.beaker1.height*0.6);

		this.label1=new cc.LabelTTF("食盐水" +
				"\n②",fontDef);
		this.beaker1.addChild(this.label1);
		this.label1.setPosition(this.beaker1.width*0.5,-this.beaker1.height*0.15);

	},
	callback:function(p){
		var func=cc.callFunc(function() {
			gg.flow.next();
		}, this);
		switch (p.getTag()) {
		default:
			break;
		}
	}
});