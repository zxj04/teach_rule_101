exp11.ShowMainLayer01 = cc.Layer.extend({
	lead:null,
	real:null,
	ctor:function () {
		this._super();
		this.init();
		return true;
	},
	init:function(){//
		this.sv = new pub.ScrollView(this);
		this.sv.setOffestPos(cc.p(152,45 + 15));

		this.addlayout();
		this.addlayout2();
	},
	addlayout:function(){
		var layout = new ccui.Layout();// 第一页
		layout.setContentSize(cc.size(996, 598));// 设置layout的大小
		this.sv.addLayer(layout);
		
		var text = $.format("　　陆生植物是通过根来吸收土壤中各种营养物质的", 800, 30);
		this.addText(layout, text, cc.p(50, 570));

		var text = $.format("　　许多植物的根系十分发达,生长的范围比枝叶大,对植物起到固定作用。组成根系的每条根均能吸收土壤中的水分和无机盐等营养物质。", 800, 30);
		this.addText(layout, text, cc.p(50, 490));

		var text = $.format("水仙", 980, 30);
		var t1 = this.addText(layout, text, cc.p(300, 50), true);
		var show1 = this.addSprite(layout, exp11.res_show.show1);
		$.up(t1, show1, 10);

		var text = $.format("白菜", 980, 30);
		var t2 = this.addText(layout, text, cc.p(750, 50), true);
		var show2 = this.addSprite(layout, exp11.res_show.show2);
		$.up(t2, show2, 10);
		
		var draw = new cc.DrawNode();
		layout.addChild(draw);
		draw.drawSegment(cc.p(50, 400),cc.p(950, 400), 2,cc.color(62, 62, 62));
	},
	addlayout2:function(){
		var layout = new ccui.Layout();//
		layout.setContentSize(cc.size(996, 598));// 设置layout的大小
		this.sv.addLayer(layout);

		var text = $.format("　　植物的根在土壤中的分布,与植物的种类和外界环境等密切相关。根根据有无主根、侧根等分为直根系和须根系。", 550, 30);
		this.addText(layout, text, cc.p(50, 500));
		
// text =
// $.format("许多植物的根系十分发达,生长的范围比枝叶大,对植物起到固定作用。组成根系的每条根均能吸收土壤中的水分和无机盐等营养物质。", 800,
// 30);
// this.addText(layout, text, cc.p(50, 470));
		
		new exp11.CloudLabel(layout, "不定根是指从植物的\n 茎或叶上长出的根", 
				exp11.CLOUD_RIGHT_UP).setPosition(800, 450);
		
		
		var show1 = this.addSprite(layout, "#zhigen.png", cc.p(150, 200));
		var t1 = this.addText(layout, "直根系", cc.p(300, 50), true);
		$.down(show1, t1);
		
		var show2 = this.addSprite(layout, "#xugen.png", cc.p(600, 200));
		var t2 = this.addText(layout, "须根系", cc.p(300, 50), true);
		$.down(show2, t2);
		
		var draw = new cc.DrawNode();
		layout.addChild(draw);
		draw.drawSegment(cc.p(155, 240),cc.p(320, 240), 2,cc.color(62, 62, 62));
		this.addText(layout, "主根", cc.p(320 + 30, 240), true);
		draw.drawSegment(cc.p(180, 200),cc.p(320, 200), 2,cc.color(62, 62, 62));
		this.addText(layout, "侧根", cc.p(320 + 30, 200), true);
		draw.drawSegment(cc.p(600, 240),cc.p(800, 240), 2,cc.color(62, 62, 62));
		this.addText(layout, "不定根", cc.p(800 + 45, 240), true);
	},
	addRichText:function(p, t1, t2, t3, pos){
		new pub.RichText(p)
		.append(t1, cc.color(0,0,0))
		.append(t2, cc.color(255,0,0))
		.append(t3, cc.color(0,0,0))
		.setPosition(pos);
	},
	addSprite:function(p, img, pos){
		var sprite = new cc.Sprite(img);
		if(pos){
			sprite.setPosition(pos);
		}
// sprite.setAnchorPoint(0, 0.5);
		p.addChild(sprite);
		return sprite;
	},
	addText:function(p, text, pos, defaultAp){
		var label = new cc.LabelTTF(text, gg.fontName, gg.fontSize4);
		label.setPosition(pos);
		if(!defaultAp){
			label.setAnchorPoint(0, 0.5);
		}
		label.setColor(cc.color(0, 0, 0, 250));
		p.addChild(label);
		return label;
	},
	addButton:function(parent,tag,str,pos){
		var button = new ButtonScale(parent,"#unsel.png",this.callback,this);
		button.setPosition(pos);
		button.setTag(tag);
		var label = new cc.LabelTTF(str,gg.fontName,30);
		label.setColor(cc.color(0, 0, 0, 250));
		label.setPosition(button.width/2,button.height/2);		
		button.addChild(label);
		return button;
	},
	callback:function(p){
		switch(p.getTag()){
		case TAG_BUTTON1:
			p.label.setVisible(true);
			break;
		case TAG_KOH:
			if(p.collision.getTag() == TAG_DISSOLUTION){
				p.setVisible(false);
				this.showTip("Very Good");
			} else {
				this.showTip("Error");
			}
			break;
		case TAG_BASO4:
			if(p.collision.getTag() == TAG_UNDISSOLUTION){
				p.setVisible(false);
				this.showTip("Very Good");
			} else {
				this.showTip("Error");
			}
			break;
		case TAG_AGCL:
			if(p.collision.getTag() == TAG_UNDISSOLUTION){
				p.setVisible(false);
				this.showTip("Very Good");
			} else {
				this.showTip("Error");
			}
			break;
		case TAG_NAOH:
			if(p.collision.getTag() == TAG_DISSOLUTION){
				p.setVisible(false);
				this.showTip("Very Good");
			} else {
				this.showTip("Error");
			}
			break;
		case TAG_RESET:
			this.resetShow();
			break;
		default:
			break;
		}
	}
});
