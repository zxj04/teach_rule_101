var exp11 = exp11||{};
exp11.startRunNext = function(tag){
	switch(tag){// 具体哪个菜单执行什么操作，这里修改
	case TAG_MENU1:
		pub.ch.gotoShow(TAG_EXP_11, tag, exp11.g_resources_show, null, new exp11.ShowScene01(tag));
		break;
	case TAG_MENU2:
		pub.ch.gotoShow(TAG_EXP_11, tag, exp11.g_resources_show, null, new exp11.ShowScene02(tag));
		break;
	case TAG_MENU3:
		pub.ch.gotoShow(TAG_EXP_11, tag, exp11.g_resources_show, null, new exp11.ShowScene03(tag));
		break;
	case TAG_MENU4:
		gg.teach_type = TAG_LEAD;
		gg.teach_flow = exp11.teach_flow01;
		pub.ch.gotoRun(TAG_EXP_11, tag, exp11.g_resources_run, null, new exp11.RunScene01(tag));
		break;
	case TAG_MENU5:
		gg.teach_type = TAG_LEAD;
		gg.teach_flow = exp11.teach_flow02;
		pub.ch.gotoRun(TAG_EXP_11, tag, exp11.g_resources_run, null, new exp11.RunScene02(tag));
		break;
	case TAG_MENU6:
		pub.ch.gotoShow(TAG_EXP_11, tag, exp11.g_resources_show, null, new exp11.ShowScene04(tag));
		break;
	case TAG_ABOUT:
		pub.ch.gotoAbout(TAG_EXP_11, g_resources_public_about, null, res_public_about.about_j);
		break;
	}
}

// 任务流
exp11.teach_flow01 = 
	[
	 {
		 tip:"选择麦苗",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG1,	   
		      ],
		      canClick:true		 
	 },
	 {
		 tip:"选择剪刀",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG4,	   
		      ],
		      canClick:true		 
	 },
	 {
		 tip:"选择烧杯",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG8,	   
		      ],
		      canClick:true		 
	 },
	 {
		 tip:"点击确定，开始实验",
		 tag:[
		      TAG_LIB,
		      TAG_SURE
		      ]
	 },
	 {
		 tip:"取出1号烧杯中的麦苗，用剪刀剪去根尖",
		 tag:[
		      TAG_MAIMIAO_NODE,
		      TAG_MAIMIAO
		      ],action:ACTION_DO1
	 },	 
	 {
		 tip:"在麦苗剪切部位涂上石蜡，并放回烧杯1中",
		 tag:[
		      TAG_MAIMIAO_NODE,
		      TAG_MAIMIAO
		      ],action:ACTION_DO2
	 },	
	 {
		 tip:"观察2个烧杯中麦苗生长情况",
		 tag:[
		      TAG_MAIMIAO_NODE,
		      TAG_BEAKER
		      ]
	 },	

	 {
		 tip:"恭喜过关",
		 over:true

	 }]

exp11.teach_flow02 = 
	[
	 {
		 tip:"将植物①放入装有蒸馏水的锥形瓶①中",
		 tag:[TAG_BOTANY,
		      TAG_BOTANY1],
	 },
	 {
		 tip:"将植物②放入装有蒸馏水的锥形瓶①中",
		 tag:[TAG_BOTANY,
		      TAG_BOTANY2],
	 },
	 {
		 tip:"将植物③放入装有食盐水的锥形瓶②中",
		 tag:[TAG_BOTANY,
		      TAG_BOTANY3],
	 },
	 {
		 tip:"将植物④放入装有食盐水的锥形瓶②中",
		 tag:[TAG_BOTANY,
		      TAG_BOTANY4],
	 },
	 {
		 tip:"恭喜过关",
		 over:true
	 }
	 ]
