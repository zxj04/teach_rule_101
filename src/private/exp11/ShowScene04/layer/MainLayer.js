TAG_PROBLEM1 = 6001;
TAG_PROBLEM2 = 6002;
exp11.ShowMainLayer04 = cc.Layer.extend({
	lead:null,
	real:null,
	ctor:function () {
		this._super();
		this.init();
		return true;
	},
	init:function(){//
		this.sv = new pub.ScrollView(this);
		this.sv.setOffestPos(cc.p(152,45 + 15));
		
		this.addlayout();
		this.addlayout2();
	},
	addlayout:function(){
		layout = new ccui.Layout();// 第一页
		layout.setContentSize(cc.size(996, 650));// 设置layout的大小
		this.sv.addLayer(layout);

		var text1 = $.format("小丽想要移栽一株月季花,她的做法应该(  )", 980,30);
		var label1 = new cc.LabelTTF(text1,gg.fontName,gg.fontSize4);
		label1.setColor(cc.color(0, 0, 0, 250));
		label1.setAnchorPoint(0, 0.5);
		label1.setPosition(50, layout.height - label1.height / 2-50);
		layout.addChild(label1);

		this.objArr1 = [];
		this.loadSelect(layout, "A．将月季花小心地连根拔起后移栽","#wrong.png", cc.p(50,500), TAG_PROBLEM1, this.objArr1);
		this.loadSelect(layout, "B．将月季花小心地连根拔起,将根洗净后移栽","#wrong.png", cc.p(50,460), TAG_PROBLEM1, this.objArr1);
		this.loadSelect(layout, "C．小心地将月季花连根带土挖起后移栽,尽量不要损伤根系","#right.png", cc.p(50,420), TAG_PROBLEM1, this.objArr1);
		this.loadSelect(layout, "D．以上方法均可以","#wrong.png", cc.p(50,380), TAG_PROBLEM1, this.objArr1);

		var text2 = $.format("解答:移栽植物时,会损伤根部,如果损伤过多," +
				"会影响植物对水和无机盐的吸收,对植物生长不利,甚至造成植物死亡。" +
				"所以移栽时也要带土移栽,尽量不要损伤根毛,有利于移栽后植物的成活," +
				"故选：C", 800,30);
		this.loadResult(layout, text2, cc.p(50,250), this.objArr1);
		
	},
	addlayout2:function(){
		layout = new ccui.Layout();// 第一页
		layout.setContentSize(cc.size(996, 650));// 设置layout的大小
		this.sv.addLayer(layout);

		var text1 = $.format("根毛区细胞吸水时,水分渗入经过的细胞结构依次是(  )", 980,30);
		var label1 = new cc.LabelTTF(text1,gg.fontName,gg.fontSize4);
		label1.setColor(cc.color(0, 0, 0, 250));
		label1.setAnchorPoint(0, 0.5);
		label1.setPosition(50, layout.height - label1.height / 2-50);
		layout.addChild(label1);

		this.objArr2 = [];
		this.loadSelect(layout, "A．液泡、细胞质、细胞膜、细胞壁","#wrong.png", cc.p(50,500), TAG_PROBLEM2, this.objArr2);
		this.loadSelect(layout, "B．液泡、细胞壁、细胞膜、细胞质","#wrong.png", cc.p(50,460), TAG_PROBLEM2, this.objArr2);
		this.loadSelect(layout, "C．细胞壁、细胞质、细胞膜、液泡","#wrong.png", cc.p(50,420), TAG_PROBLEM2, this.objArr2);
		this.loadSelect(layout, "D．细胞壁、细胞膜、细胞质、液泡","#right.png", cc.p(50,380), TAG_PROBLEM2, this.objArr2);

		var text2 = $.format("解答:水分由外界进入细胞内的通道,由外到内通过细胞壁、细胞膜、细胞质、液泡" +
				",故选：D", 800,30);
		this.loadResult(layout, text2, cc.p(50,150), this.objArr2);
		
		var show = new cc.Node();
		show.setPosition(-50, 250);
		show.setVisible(false);
		layout.addChild(show);
		this.objArr2.push(show);
		
		var water = new pub.ShowButton(show, "#bt_blue.png", "水").setPos(100, 0);
		var seq = cc.sequence(cc.moveBy(4, cc.p(800, 0)), cc.callFunc(function(){
			water.setPosition(100, 0);
		}, this));
		water.runAction(seq.repeatForever());
		new pub.ShowButton(show, "#bt_empty.png", "细胞壁").setPos(300, 0).setLabelColor(cc.color(62,62,62));
		new pub.ShowButton(show, "#bt_empty.png", "细胞膜").setPos(450, 0).setLabelColor(cc.color(62,62,62));
		new pub.ShowButton(show, "#bt_empty.png", "细胞质").setPos(600, 0).setLabelColor(cc.color(62,62,62));
		new pub.ShowButton(show, "#bt_empty.png", "液泡").setPos(750, 0).setLabelColor(cc.color(62,62,62));
	},
	loadSelect:function(layout, str,answer, pos, tag, objArr){
		var button=new Angel(layout,"#select.png",this.callback,this);
		button.setPosition(pos);
		button.setTag(tag);
		var label = new cc.LabelTTF(str,"微软雅黑",30);
		label.setPosition(pos.x+button.width*0.5+label.width*0.5+10,pos.y);
		label.setColor(cc.color(0, 0, 0, 250));
		layout.addChild(label);		var answer=new cc.Sprite(answer);
		answer.setPosition(pos);
		answer.setVisible(false);
		layout.addChild(answer);
		objArr.push(answer);
	},
	loadResult:function(layout, text, pos, objArr){
		var label = new cc.LabelTTF(text, gg.fontName, gg.fontSize4);
		label.setVisible(false);
		label.setColor(cc.color(0, 0, 0, 300));
		label.setAnchorPoint(0, 1);
		label.setPosition(pos);
		layout.addChild(label);
		objArr.push(label);
	},
	callback:function(p){
		var objArr = [];
		switch (p.getTag()) {
		case TAG_PROBLEM1:
			objArr = this.objArr1;
			break;
		case TAG_PROBLEM2:
			objArr = this.objArr2;
			break;
		default:
			break;
		}
		for(var i = 0; i < objArr.length; i++){
			var obj = objArr[i];
			obj.setVisible(true);
		}
	}
});
