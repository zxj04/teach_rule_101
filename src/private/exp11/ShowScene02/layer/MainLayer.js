exp11.ShowMainLayer02 = cc.Layer.extend({
	lead:null,
	real:null,
	curIndex: 0,
	cur:0,
	cell:50,
	ctor:function () {
		this._super();
		this.init();		
		return true;
	},
	init:function(){
		this.sv = new pub.ScrollView(this);
		this.sv.setOffestPos(cc.p(152,45 + 15));

		this.addlayout1();
		this.addlayout2();
	},
	addlayout1:function(){
		var layout = new ccui.Layout();// 第一页
		layout.setContentSize(cc.size(996, 598));// 设置layout的大小
		this.sv.addLayer(layout);
	
		var button1 = this.addButton(layout, TAG_BUTTON1, "根毛区",cc.p(60, 450));
		var button2 = this.addButton(layout, TAG_BUTTON2, "伸长区",cc.p(60, 370));
		var button3 = this.addButton(layout, TAG_BUTTON3, "分生区",cc.p(60, 290));
		var button4 = this.addButton(layout, TAG_BUTTON4, "根　冠",cc.p(60, 210));
		
		var gen1 = this.addSprite(layout, exp11.res_show.gen, cc.p(250, 450), cc.rect(0, 0, 175, 92));
		var gen2 = this.addSprite(layout, exp11.res_show.gen, cc.p(250, 305), cc.rect(0, 92, 175, 199));
		var gen3 = this.addSprite(layout, exp11.res_show.gen, cc.p(250, 189), cc.rect(0, 291, 175, 34));
		var gen4 = this.addSprite(layout, exp11.res_show.gen, cc.p(250, 139), cc.rect(0, 325, 175, 67));
		this.genArr = [gen1, gen2, gen3, gen4];
		
		this.textArr = ["　　它的细胞的细胞壁比较厚，液泡比较大，所以是是吸收水分和无机盐的主要部位，根毛扩大了吸收的面积。",
		                "　　由于伸长区细胞迅速同时伸长，再加上分生区细胞的分裂、增大，致使根尖向土层深处生长，有利根的吸收作用。",
		                "　　细胞小，排列密而整齐，细胞壁薄，细胞核大，细胞质浓，无液泡。具有很强的分裂能力，能够不断的分裂产生新细胞，再由这些细胞分化形成其他组织。",
		                "　　由于根冠表层细胞受磨损而脱落，从而起到了保护分生区的作用，同时因为根冠细胞破坏时形成粘液，还可以减少根尖伸长时与土壤的摩擦力。根冠表皮细胞脱落后，由于分生区附近的根冠细胞能分裂产生新细胞，所以根冠能始终维持一定的形状和厚度。"
		                ];

		var show = this.addText(layout, "", cc.p(400, 300));
		this.show = show;
		this.callback(button1);
		
	},
	addlayout2:function(){
		var layout = new ccui.Layout();// 第一页
		layout.setContentSize(cc.size(996, 598));// 设置layout的大小
		this.sv.addLayer(layout);
		
		var text = $.format("　　植物从土壤中吸收水分进入根毛细胞后,根据细胞液溶质质量分数的差异,水分由一个皮层细胞进入另一个皮层细胞,然后进入根的木质部导管,最后上升到植物体的结构。", 820, 30);
		this.addText(layout, text, cc.p(50, 520));
		
		var water = this.addSprite(layout, "#water_into.png", cc.p(500, 250));
		var w1 = new pub.ShowButton(layout, "#bt_blue.png", "").setPos(250, 220);
		w1.setScale(0.15);
		var seq = cc.sequence(cc.moveTo(2, cc.p(400, 250)), 
			cc.bezierTo(2, [cc.p(500, 250),cc.p(400, 280),cc.p(550, 280)]), 
			cc.moveTo(2, cc.p(630, 260)), 
			cc.callFunc(function(){
			w1.setPosition(250, 220);
		}, this));
		w1.runAction(seq.repeatForever());
		
		var w2 = new pub.ShowButton(layout, "#bt_blue.png", "").setPos(360, 120);
		w2.setScale(0.15);
		var seq = cc.sequence(cc.moveTo(2, cc.p(420, 200)),
			cc.moveTo(2, cc.p(560, 240)), 
			cc.moveTo(1, cc.p(600, 250)), 
			cc.callFunc(function(){
			w2.setPosition(360, 120);
		}, this));
		w2.runAction(seq.repeatForever());
	},
	setShowString:function(str){
		var t = $.format(str, 550, 30);
		this.show.setString(t);
	},
	addText:function(p, text, pos, isDefaultAP){
		var label = new cc.LabelTTF(text, gg.fontName, gg.fontSize4);
		label.setPosition(pos);
		if(!isDefaultAP){
			label.setAnchorPoint(0, 0.5);
		}
		label.setColor(cc.color(0, 0, 0, 250));
		p.addChild(label);
		return label;
	},
	addSprite:function(p, img, pos, rect){
		var sprite = new cc.Sprite(img, rect);
		if(pos){
			sprite.setPosition(pos);
		}
		p.addChild(sprite);
		return sprite;
	},
	addRichText:function(p, t1, t2, t3, pos){
		new exp11.RichTTF(p)
			.append(t1, cc.color(0,0,0))
			.append(t2, cc.color(255,0,0))
			.append(t3, cc.color(0,0,0))
			.setPosition(pos);
	},
	addButton:function(parent,tag,str,pos){
		var s9 = new pub.S9Button(parent, str,this.callback,this);
		s9.setPosition(pos);
		s9.setTag(tag);
		return s9;
	},
	addimage:function(parent,str,pos,str2,pos2){
		var image = new cc.Sprite(str);
		parent.addChild(image);
		image.setPosition(pos);
		var label = new cc.LabelTTF(str2,gg.fontName,gg.fontSize);
		label.setColor(cc.color(0, 0, 0, 250));
		parent.addChild(label);
		label.setPosition(pos2);
	},
	callback:function(p){
		var seq = cc.sequence(cc.fadeOut(1), cc.fadeIn(1));
		for(var i = 0; i < this.genArr.length; i++){
			this.genArr[i].setOpacity(255);
			this.genArr[i].stopAllActions();
		}
		switch(p.getTag()){
		case TAG_BUTTON1:
			this.genArr[0].runAction(seq.repeatForever());
			this.setShowString(this.textArr[0]);
			break;
		case TAG_BUTTON2:
			this.genArr[1].runAction(seq.repeatForever());
			this.setShowString(this.textArr[1]);
			break;
		case TAG_BUTTON3:
			this.genArr[2].runAction(seq.repeatForever());
			this.setShowString(this.textArr[2]);
			break;
		case TAG_BUTTON4:
			this.genArr[3].runAction(seq.repeatForever());
			this.setShowString(this.textArr[3]);
			break;
	}
	}
});
