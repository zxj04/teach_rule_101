exp11.ShowMainLayer03 = cc.Layer.extend({
	lead:null,
	real:null,
	objArr:null,
	ctor:function () {
		this._super();
		this.init();
		return true;
	},
	init:function(){
		this.objArr = [];
		this.sv = new pub.ScrollView(this);
		this.sv.setOffestPos(cc.p(152,45 + 15));

		this.addlayout1();
		this.addlayout2();
	},	
	addlayout1:function(){
		var layout = new ccui.Layout();// 第一页
		layout.setContentSize(cc.size(996, 598));// 设置layout的大小
		this.sv.addLayer(layout);
		
		this.layout1 = layout;
		
		var text = $.format("　　植物的生长还需要无机盐,不同的无机盐对植物生长会起不同的作用。", 820, 30);
		this.addText(layout, text, cc.p(50, 520));

		var bg = this.addSprite(layout, exp11.res_show.show15, cc.p(500, 200));
		var show = this.addSprite(bg, exp11.res_show.show11, cc.p(265, 197));/*- 179 + 86 = 265*/
		this.scheduleOnce(this.changeShow.bind(bg), 0.5);
		this.schedule(this.changeShow.bind(bg), 5);
		
		var spawn = cc.spawn(cc.moveTo(2, cc.p(500, 180)), cc.scaleTo(2, 0.1));
		
		var sp1 = this.addSprite(layout, "#bt_yellow.png", cc.p(190, 100));
		sp1.runAction(cc.sequence(spawn.clone(), cc.callFunc(this.reset.bind(sp1), this)).repeatForever());
		new pub.ShowButton(layout, "#bt_yellow.png", "氮盐").setPosition(190, 100);
		
		var sp2 = this.addSprite(layout, "#bt_green.png", cc.p(190, 350));
		sp2.runAction(cc.sequence(spawn.clone(), cc.callFunc(this.reset.bind(sp2), this)).repeatForever());
		new pub.ShowButton(layout, "#bt_green.png", "镁盐").setPosition(190, 350);
		
		var sp3 = this.addSprite(layout, "#bt_blue.png", cc.p(810, 100));
		sp3.runAction(cc.sequence(spawn.clone(), cc.callFunc(this.reset.bind(sp3), this)).repeatForever());
		new pub.ShowButton(layout, "#bt_blue.png", "硫盐").setPosition(810, 100);
		
		var sp4 = this.addSprite(layout, "#bt_pink.png", cc.p(810, 350));
		sp4.runAction(cc.sequence(spawn.clone(), cc.callFunc(this.reset.bind(sp4), this)).repeatForever());
		new pub.ShowButton(layout, "#bt_pink.png", "钾盐").setPosition(810, 350);
		
	},
	reset:function(){
		this.setPosition(this.origPos);
		this.setScale(1);
	},
	changeShow:function(){
		/*- this is bg*/
		var show = this.getChildren()[0];

		var arr = [exp11.res_show.show11, 
		           exp11.res_show.show12, 
		           exp11.res_show.show13, 
		           exp11.res_show.show14];
		this.index = !this.index ? 0 : this.index;
		this.index ++;
		this.index = this.index >= arr.length ? 0 : this.index;
		var newShow = new cc.Sprite(arr[this.index])
		newShow.setPosition(show.getPosition());
		this.addChild(newShow);
		
		if(this.index == 0){
			show.runAction(cc.sequence(cc.delayTime(3), cc.callFunc(function(){
				show.removeFromParent(true);
			}, this)));
			newShow.setVisible(false);
			newShow.runAction(cc.sequence(cc.delayTime(3), cc.show()));
		} else {
			show.runAction(cc.sequence(cc.fadeOut(3), cc.callFunc(function(){
				show.removeFromParent(true);
			}, this)));
			newShow.setOpacity(0);
			newShow.runAction(cc.fadeIn(3));
		}
	},
	addlayout2:function(){
		var layout = new ccui.Layout();// 第一页
		layout.setContentSize(cc.size(996, 598));// 设置layout的大小
		this.sv.addLayer(layout);
		
		var text = $.format("　　无土栽培(soilless culture)不用土壤，用其它东西培养植物的方法，" +
				"包括水培、雾（气）培、基质栽培。19世纪中，W．克诺普等发明了这种方法。" +
				"到20世纪30年代开始把这种技术应用到农业生产上。" +
				"在二十一世纪人们进一步改进技术，使得无土栽培发展起来。", 820, 30);
		this.addText(layout, text, cc.p(50, 500));
		
		this.addSprite(layout, exp11.res_show.show3, cc.p(500, 200));
	},
	addText:function(p, text, pos, defaultAp){
		var label = new cc.LabelTTF(text, gg.fontName, gg.fontSize4);
		label.setPosition(pos);
		if(!defaultAp){
			label.setAnchorPoint(0, 0.5);
		}
		label.setColor(cc.color(0, 0, 0, 250));
		p.addChild(label);
		return label;
	},
	addSprite:function(p, img, pos){
		var sprite = new cc.Sprite(img);
		if(pos){
			sprite.setPosition(pos);
			sprite.origPos = pos;
		}
// sprite.setAnchorPoint(0, 0.5);
		p.addChild(sprite);
		return sprite;
	},
	callback:function(p){
		switch(p.getTag()){
		case TAG_BUTTON1:
			break;
		case TAG_BUTTON2:
			break;
		}
	}
	
});
