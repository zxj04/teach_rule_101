exp04.ShowMainLayer01 = cc.Layer.extend({
	lead:null,
	real:null,
	ctor:function () {
		this._super();
		this.init();
		return true;
	},
	init:function(){//
		this.sv = new pub.ScrollView(this);
		this.sv.setOffestPos(cc.p(152,45 + 15));

		this.addlayout();
		this.addlayout2();
		this.addlayout3();
		this.addlayout4();
	},
	addlayout:function(){
		var layout = new ccui.Layout();// 第一页
		layout.setContentSize(cc.size(996, 598));// 设置layout的大小
		this.sv.addLayer(layout);
		
		var text = $.format("　　物质可以用所组成元素的符号来表示。这种用元素符号来标识物质组成的式子称化学式(chemical formula)。", 800, 30);
		this.addText(layout, text, cc.p(50, 510));

		//text = $.format("CO₂", 800, 30);
		var text = new exp04.CF(layout,"C0","2","").setColor(cc.color(0,0,0))
		text.setPosition(cc.p(250,100));
		//this.addText(layout, text, cc.p(250, 100));
		
		new pub.ShowButton(layout, "#bt_yellow.png", "C").setPosition(250, 300);
		new pub.ShowButton(layout, "#bt_green.png", "O").setPosition(310, 330);
		new pub.ShowButton(layout, "#bt_green.png", "O").setPosition(310, 270);
		
//		text = $.format("O₂", 800, 30);
//		this.addText(layout, text, cc.p(750, 100));
		var text = new exp04.CF(layout,"0","2","").setColor(cc.color(0,0,0))
		text.setPosition(cc.p(750,100));

		new pub.ShowButton(layout, "#bt_green.png", "O").setPosition(750, 330);
		new pub.ShowButton(layout, "#bt_green.png", "O").setPosition(750, 270);
	},
	addlayout2:function(){
		var layout = new ccui.Layout();//
		layout.setContentSize(cc.size(996, 598));// 设置layout的大小
		this.sv.addLayer(layout);

		var text = $.format("单质化学式的写法:", 800, 30);
		this.addText(layout, text, cc.p(50, 550));

		text = $.format("写出组成单质的元素符号", 800, 30);
		this.addText(layout, text, cc.p(50, 500));
		
		text = $.format("在元素符号右下角用数字\n写出构成一个单质分子的原子个数", 800, 30);
		this.addText(layout, text, cc.p(50, 450));
		
		new pub.ShowButton(layout, "#bt_yellow.png", "稀有\n气体").setLabelSize(20).setPosition(200, 50);
		new pub.ShowButton(layout, "#bt_green.png", "金属\n单质").setLabelSize(20).setPosition(100, 100);
		new pub.ShowButton(layout, "#bt_pink.png", " 固态\n非金属\n 单质").setLabelSize(20).setPosition(300, 100);
		
		new exp04.CloudLabel(layout, "我们的化学式都用\n元素符号来表示", exp04.CLOUD_RIGHT_UP).setPosition(300, 250);
		
		var x = 600, y = 550, i = 0, m = 40;
		this.addRichText(layout, "氦气　　", "He", "　稀有气体", cc.p(x, y - m * i++));
		this.addRichText(layout, "氖气　　", "Ne", "　稀有气体", cc.p(x, y - m * i++));
		this.addRichText(layout, "氩气　　", "Ar", "　稀有气体", cc.p(x, y - m * i++));
		i++;
		
		new exp04.RichTTF(layout,gg.fontName,25)
		.append("氧气　　", cc.color(0,0,0))
		.append(new exp04.CF(layout,"0","2","").setColor(cc.color(255,0,0)), cc.color(255,0,0))
		.append( "　　非稀有气体", cc.color(0,0,0))
		.setPosition(cc.p(x, y - m * i++));
		
		new exp04.RichTTF(layout,gg.fontName,25)
		.append("氮气　　", cc.color(0,0,0))
		.append(new exp04.CF(layout,"N","2","").setColor(cc.color(255,0,0)), cc.color(255,0,0))
		.append( "　　非稀有气体", cc.color(0,0,0))
		.setPosition(cc.p(x, y - m * i++));
		
		new exp04.RichTTF(layout,gg.fontName,25)
		.append("氯气　　", cc.color(0,0,0))
		.append(new exp04.CF(layout,"Cl","2","").setColor(cc.color(255,0,0)), cc.color(255,0,0))
		.append( "　　非稀有气体", cc.color(0,0,0))
		.setPosition(cc.p(x, y - m * i++));
		
		new exp04.RichTTF(layout,gg.fontName,25)
		.append("臭氧　　", cc.color(0,0,0))
		.append(new exp04.CF(layout,"O","3","").setColor(cc.color(255,0,0)), cc.color(255,0,0))
		.append( "　　非稀有气体", cc.color(0,0,0))
		.setPosition(cc.p(x, y - m * i++));
		
		//this.addRichText(layout, "氧气　　", "O₂", "　非稀有气体", cc.p(x, y - m * i++));
//		this.addRichText(layout, "氮气　　", "N₂", "　非稀有气体", cc.p(x, y - m * i++));
//		this.addRichText(layout, "氯气　　", "Cl₂", "　非稀有气体", cc.p(x, y - m * i++));
//		this.addRichText(layout, "臭氧　　", "O₃", "　非稀有气体", cc.p(x, y - m * i++));
		i++;
		this.addRichText(layout, "金属铜　 ", "Cu", "　金属单质", cc.p(x, y - m * i++));
		this.addRichText(layout, "金属铝　 ", "Al", "　金属单质", cc.p(x, y - m * i++));
		this.addRichText(layout, "金刚石　 ", "C", "　固态非金属单质", cc.p(x, y - m * i++));
		this.addRichText(layout, "固态磷　 ", "P", "　固态非金属单质", cc.p(x, y - m * i++));
	},
	addRichText:function(p, t1, t2, t3, pos){
		new exp04.RichTTF(p)
		.append(t1, cc.color(0,0,0))
		.append(t2, cc.color(255,0,0))
		.append(t3, cc.color(0,0,0))
		.setPosition(pos);
	},
	addText:function(p, text, pos){
		var label = new cc.LabelTTF(text, gg.fontName, gg.fontSize4);
		label.setPosition(pos);
		label.setAnchorPoint(0, 0.5);
		label.setColor(cc.color(0, 0, 0, 250));
		p.addChild(label);
		return label;
	},
	addlayout3:function(){
		var layout = new ccui.Layout();//
		layout.setContentSize(cc.size(996, 598));// 设置layout的大小
		this.sv.addLayer(layout);

		var text = $.format("化合物化学式的写法:", 800, 30);
		this.addText(layout, text, cc.p(50, 550));

		text = $.format("1.按一定顺序写出组成化合物的所有元素符号", 800, 30);
		this.addText(layout, text, cc.p(50, 500));
		
		text = $.format("    氧元素与另一元素组成的化合物,\n    一般要把氧元素符号写在右边", 800, 30);
		this.addText(layout, text, cc.p(50, 450));
		
		text = $.format("    氢元素与另一元素组成的化合物,\n    一般要把氢元素符号写在左边", 800, 30);
		this.addText(layout, text, cc.p(50, 350));

		text = $.format("    金属元素、氢元素与非金属元素组成的化合物,\n    一般要把非金属元素符号写在右边", 800, 30);
		this.addText(layout, text, cc.p(50, 250));
		
		text = $.format("2.在每种元素符号的右下角用数字写出每个化合物分子中该元素的原子个数。直接由离子构成的化合物,其化学式常用其离子最简单整数比表示", 800, 30);
		this.addText(layout, text, cc.p(50, 120));
	},
	addlayout4:function(){
		var layout = new ccui.Layout();//
		layout.setContentSize(cc.size(996, 598));// 设置layout的大小
		this.sv.addLayer(layout);
		
		var posArr = [];
		for(var i = 0; i < 5; i++){
			var pos = cc.p(100 + 200 * i, 420);
			posArr.push(pos);
		}
		for(var i = 0; i < 5; i++){
			var pos = cc.p(100 + 200 * i, 160);
			posArr.push(pos);
		}
		var nameArr = ["一氧化碳","一氧化氮","氯化氢","二氧化硫","二氧化碳","水","氯化钠","硫化氢","磷"," 四氧\n化三铁"];
		var text = new exp04.CF(layout,"S0","2","").setColor(cc.color(0,0,0));
		var text1 = new exp04.CF(layout,"C0","2","").setColor(cc.color(0,0,0));
	    var text2 =	new exp04.RichTTF(layout,gg.fontName,30)
		.append(new exp04.CF(layout,"H","2","").setColor(cc.color(0,0,0)), cc.color(0,0,0))
		.append( "O", cc.color(0,0,0));
	    var text3 =	new exp04.RichTTF(layout,gg.fontName,30)
	    .append(new exp04.CF(layout,"H","2","").setColor(cc.color(0,0,0)), cc.color(0,0,0))
	    .append( "S", cc.color(0,0,0));
	    var text4 =	new exp04.RichTTF(layout,gg.fontName,30)
	    .append(new exp04.CF(layout,"Fe","3","").setColor(cc.color(0,0,0)), cc.color(0,0,0))
	    .append(new exp04.CF(layout,"O","4","").setColor(cc.color(0,0,0)), cc.color(0,0,0));
	    var letterArr = ["CO","NO","HCl",text,text1,text2,"NaCl",text3,"P",text4];
		for(var i = 0; i < 10; i++){
			var card = new exp04.Card(layout);
			card.addContent(letterArr[i], nameArr[i]).setPosition(posArr[i]);	
		}
	},
	addButton:function(parent,tag,str,pos){
		var button = new ButtonScale(parent,"#unsel.png",this.callback,this);
		button.setPosition(pos);
		button.setTag(tag);
		var label = new cc.LabelTTF(str,gg.fontName,30);
		label.setColor(cc.color(0, 0, 0, 250));
		label.setPosition(button.width/2,button.height/2);		
		button.addChild(label);
		return button;
	},
	callback:function(p){
		switch(p.getTag()){
		case TAG_BUTTON1:
			p.label.setVisible(true);
			break;
		case TAG_KOH:
			if(p.collision.getTag() == TAG_DISSOLUTION){
				p.setVisible(false);
				this.showTip("Very Good");
			} else {
				this.showTip("Error");
			}
			break;
		case TAG_BASO4:
			if(p.collision.getTag() == TAG_UNDISSOLUTION){
				p.setVisible(false);
				this.showTip("Very Good");
			} else {
				this.showTip("Error");
			}
			break;
		case TAG_AGCL:
			if(p.collision.getTag() == TAG_UNDISSOLUTION){
				p.setVisible(false);
				this.showTip("Very Good");
			} else {
				this.showTip("Error");
			}
			break;
		case TAG_NAOH:
			if(p.collision.getTag() == TAG_DISSOLUTION){
				p.setVisible(false);
				this.showTip("Very Good");
			} else {
				this.showTip("Error");
			}
			break;
		case TAG_RESET:
			this.resetShow();
			break;
		default:
			break;
		}
	}
});
