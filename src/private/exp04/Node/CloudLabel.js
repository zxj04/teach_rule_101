exp04.CLOUD_LEFT_UP = 0;
exp04.CLOUD_RIGHT_UP = 1;
exp04.CLOUD_LEFT_DOWN = 2;
exp04.CLOUD_RIGHT_DOWN = 3;
exp04.CloudLabel = cc.Node.extend({
	ctor:function(p, str, direction){
		this._super();
		p.addChild(this);
		this.init(str, direction);
		return this;
	},
	init:function(str, direction){
		var img = "";
		if(direction == exp04.CLOUD_LEFT_UP
			|| direction == exp04.CLOUD_LEFT_DOWN){
			img = "#cloud1.png";
		} else if(direction == exp04.CLOUD_RIGHT_UP
				|| direction == exp04.CLOUD_RIGHT_DOWN){
			img = "#cloud2.png";
		} else {
			cc.error("exp04.CloudLabel direction is undefinition");
		}
		
		var cloud = new cc.Sprite(img);
		if(direction == exp04.CLOUD_LEFT_DOWN
			|| direction == exp04.CLOUD_RIGHT_DOWN){
			cloud.setRotationX(180);
		}
		this.addChild(cloud);
		
		var label = new cc.LabelTTF(str,gg.fontName,gg.fontSize3);
		label.setColor(cc.color(0, 0, 0, 250));
		this.addChild(label);
	}
});