//Chemical formula化学式
exp04.CF = cc.Node.extend({
	size: 30,
	ctor:function(p, name, down, up, isAdd){
		this._super();
		if(p){
			p.addChild(this);
		}
		this.init(name, down, up, isAdd);
		return this;
	},
	init:/**
			 * @param name
			 *            元素符号
			 * @param down
			 *            下标
			 * @param up
			 *            上标
			 * @param isAdd
			 *            是否阳离子
			 */
	function(name, down, up, isAdd){
		var size = this.size;
		var nameL = this.addText(name, size);
		nameL.setAnchorPoint(0, 0.5);
		this.nameL = nameL;
		
		var margin = 5;
		var downL = this.addText(down, size * 0.5);
		$.right(nameL, downL, margin);
		downL.setPosition(downL.getPosition().x, downL.getPosition().y - downL.height * 0.5);
		this.downL = downL;
		
		var upL = this.addText(up, size * 0.5);
		$.up(downL, upL);
		this.upL = upL;
		
		var addStr = "";
		if(isAdd ==true){
			addStr = "+";
		} else if(isAdd ==false) {
			addStr = "-";
		}else{
			addStr = "";
		}
		var addL = this.addText(addStr, size * 0.5);
		if(up == "" 
			|| up == null
			|| up == undefined){
			//addL.setPosition(upL.getPosition())
			$.up(this.downL, addL);			
		} else {
			$.right(upL, addL, margin);
		}
		this.addL = addL;
		
		this.setAnchorPoint(0, 0.5);
	},
	addText:function(text, size){
		var label = new cc.LabelTTF(text, gg.fontName, size);
		label.setColor(cc.color(0, 0, 0, 250));
		this.addChild(label);
		return label;
	},
	setColor:function(color){
		this.nameL.setColor(color);
		this.downL.setColor(color);
		this.upL.setColor(color);
		this.addL.setColor(color);
		return this;
	},
	setSize:function(size){
		this.nameL.setFontSize(size);
		this.downL.setFontSize(size * 0.5);
		this.upL.setFontSize(size * 0.5);
		this.addL.setFontSize(size *0.5);
		return this;
	}
});