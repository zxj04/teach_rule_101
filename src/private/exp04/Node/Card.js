exp04.Card = cc.Node.extend({
	TAG_POSITIVE: 1001,
	TAG_OTHER : 1002,
	TYPE_STR : 1101,
	TYPE_NODE : 1102,
	TYPE_LABEL : 1103,
	TYPE_IMAGE : 1104,
	TYPE_SPRITE : 1105,
	autoAction : true,
	actionFlag : false,
	enable : true,
	positiveShow : false,
	ctor:function(p, cardCallback, cardBack){
		this._super();
		p.addChild(this);
		this.cardCallback = cardCallback;
		this.cardBack = cardBack;
		this.init();
		return this;
	},
	init:function(){
		var roundTime = 0.2;// 翻转时间，正面卡牌得等背面卡牌翻转90度之后才能显示
		var kInAngleZ = 270;
		var kInDeltaZ = 90;
		var kOutAngleZ = 0;
		var kOutDeltaZ = 90;
		// 正面
		var positive  = new Angel(this, "#card_positive.png", this.callback, this);
		positive.setVisible(false);
		positive.setTag(this.TAG_POSITIVE);
		this.positive = positive;
		// 反面
		var otherSide  = new Angel(this, "#card_other_side.png", this.callback, this);
		otherSide.setTag(this.TAG_OTHER);
		this.otherSide = otherSide;
		// 进入动画
		this.cardIn = cc.sequence(cc.delayTime(roundTime),cc.show(), cc.orbitCamera(roundTime, 1, 0, kInAngleZ, kInDeltaZ, 1,0),
				cc.callFunc(this.releaseActionFlag, this));
		this.cardIn.retain();
		// 出去动画
		this.cardOut = cc.sequence(cc.orbitCamera(roundTime, 1, 0, kOutAngleZ, kOutDeltaZ, 1,0), cc.hide(), cc.delayTime(roundTime));
		this.cardOut.retain();
	},
	isPositive:/**
				 * 是否正面
				 * 
				 * @returns {Boolean}
				 */
	function(){
		return this.positiveShow;
	},
	addContent:/**
				 * @param arg1正面
				 * @param arg2背面
				 * @returns {___anonymous_Card}
				 */
	function(arg1, arg2){
		this.addPositive(arg1);
		this.addOhterSide(arg2);
		return this;
	},
	addPositive:function(arg){
		switch(typeof arg){
		case "string":
			var label = new cc.LabelTTF(arg, gg.fontName, 40);
			this.positive.addChild(label);
			label.setColor(cc.color(0, 0, 0));
			label.setPosition(this.positive.width * 0.5, this.positive.height * 0.5);
			this.cardName = arg;
			break;
		case "object":
			if(arg.getParent()){
				arg.removeFromParent();
			}
			this.positive.addChild(arg);
			var box = arg.getBoundingBoxToWorld();
			var ap = arg.getAnchorPoint();
			arg.setPosition(this.positive.width * 0.5 - box.width * (0.5 - ap.x), this.positive.height * 0.5);
			break;
		}
	},
	addOhterSide:function(arg){
		switch(typeof arg){
		case "string":
			var label = new cc.LabelTTF(arg, gg.fontName, 40);
			this.otherSide.addChild(label);
			label.setColor(cc.color(0, 0, 0));
			label.setPosition(this.otherSide.width * 0.5, this.otherSide.height * 0.5);
			break;
		case "object":
			if(arg.getParent()){
				arg.removeFromParent();
			}
			this.otherSide.addChild(arg);
			var box = arg.getBoundingBoxToWorld();
			var ap = arg.getAnchorPoint();
			arg.setPosition(this.otherSide.width * 0.5 - box.width * (0.5 - ap.x), this.otherSide.height * 0.5);
			break;
		}
	},
	sayCardName:function(info){
		cc.log("name is " + this.cardName + " || " + info);
	},
	openCard:/**
				 * 打卡卡牌
				 * 
				 * @param force
				 */
	function(force){
		if(this.actionFlag && !force){
			return;
		}
		this.retainActionFlag();
		this.positiveShow = true;
		this.positive.stopAllActions();
		this.otherSide.stopAllActions();
		this.positive.runAction(this.cardIn);
		this.otherSide.runAction(this.cardOut);
	},
	closeCard:/**
				 * 关闭卡牌
				 * 
				 * @param force
				 *            是否强制执行
				 */
	function(force){// 关闭卡牌
		if(this.actionFlag && !force){
			return;
		}
		this.retainActionFlag();
		this.positiveShow = false;
		this.positive.stopAllActions();
		this.otherSide.stopAllActions();
		this.positive.runAction(this.cardOut);
		this.otherSide.runAction(this.cardIn);
// this.scheduleOnce(function(){
// }, 0.5);
	},
	releaseActionFlag:function(){
		if(this.actionFlag){
			this.actionFlag = false;
		}
	},
	retainActionFlag:function(){
		this.actionFlag = true;
		this.unschedule(this.closeCard);
	},
	setEnable:function(enable){
		this.enable = enable;
	},
	overAction:/**
				 * 结束动作,被翻开了
				 */
	function(){
		this.unschedule(this.closeCard);
		this.autoAction = false;
		this.positive.setOpacity(100);
	},
	callback:function(p){
		if(!this.enable){
			return;
		}
		switch(p.getTag()){
		case this.TAG_POSITIVE:
			this.closeCard();
			break;
		case this.TAG_OTHER:
			this.openCard();
			if(this.autoAction){
				this.scheduleOnce(this.closeCard, 3);
			}
			break;
		}
		if(this.cardCallback){
			this.cardCallback.call(this.cardBack, this);
		}
	},
	onExit:function(){
		this._super();
		this.cardIn.release();
		this.cardOut.release();
	}
})