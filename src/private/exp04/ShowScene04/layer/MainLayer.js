TAG_PROBLEM1 = 6001;
TAG_PROBLEM2 = 6002;
exp04.ShowMainLayer04 = cc.Layer.extend({
	lead:null,
	real:null,
	ctor:function () {
		this._super();
		this.init();
		return true;
	},
	init:function(){//
		this.sv = new pub.ScrollView(this);
		this.sv.setOffestPos(cc.p(152,45 + 15));
		
		this.addlayout();
		this.addlayout2();
	},
	addlayout:function(){
		layout = new ccui.Layout();// 第一页
		layout.setContentSize(cc.size(996, 650));// 设置layout的大小
		this.sv.addLayer(layout);

//		var text1 = $.format("化学式书写错误的是(  )\n" +
//			"\n      A．CuO" +
//			"\n      B．KNO₃" +
//			"\n      C．CaCl" +
//			"\n      D．Zn", 980,30);
		var text1 = $.format("化学式书写错误的是(  )", 980,30);
		var label1 = new cc.LabelTTF(text1,gg.fontName,gg.fontSize4);
		label1.setColor(cc.color(0, 0, 0, 250));
		label1.setAnchorPoint(0, 0.5);
		label1.setPosition(50, layout.height - label1.height / 2-50);
		layout.addChild(label1);

		this.objArr1 = [];
		this.loadSelect(layout, "A．CuO", "#wrong.png", cc.p(50,500), TAG_PROBLEM1, this.objArr1);
		this.loadSelect(layout, "B．KNO₃", "#wrong.png", cc.p(50,460), TAG_PROBLEM1, this.objArr1);
		this.loadSelect(layout, "C．CaCl", "#right.png", cc.p(50,420), TAG_PROBLEM1, this.objArr1);
		this.loadSelect(layout, "D．Zn", "#wrong.png", cc.p(50,380), TAG_PROBLEM1, this.objArr1);

		var text2 = $.format("解答:金属的化学式用元素符号来表示，化合物的化学式是否正确，" +
				"看其化合物中各元素的化合价代数和是否为零，为零则化学式正确。" +
				"题中C的化合价代数和不等于零，因此化学式错误。" +
				"故选：C", 800,30);
		this.loadResult(layout, text2, cc.p(50,250), this.objArr1);
		
		var ttf1 = new exp04.RichTTF(layout,gg.fontName,30)
		.append("CuO化合价代数和    (", cc.color(0,0,0))
		.append("+2", cc.color(255,0,0))
		.append(") * 1 + (", cc.color(0,0,0))
		.append("-2", cc.color(255,0,0))
		.append(") * 1 = 0", cc.color(0,0,0));
		ttf1.setPosition(cc.p(300, 500));
		ttf1.setVisible(false);
		this.objArr1.push(ttf1);
		
		var ttf2 = new exp04.RichTTF(layout,gg.fontName,30)
		.append("KNO₃化合价代数和    (", cc.color(0,0,0))
		.append("+1", cc.color(255,0,0))
		.append(") * 1 + (", cc.color(0,0,0))
		.append("+5", cc.color(255,0,0))
		.append(") * 1 + (", cc.color(0,0,0))
		.append("-2", cc.color(255,0,0))
		.append(") * 3 = 0", cc.color(0,0,0));
		ttf2.setPosition(cc.p(300, 460));
		ttf2.setVisible(false);
		this.objArr1.push(ttf2);
		
		var ttf3 = new exp04.RichTTF(layout,gg.fontName,30)
		.append("CaCl化合价代数和    (", cc.color(0,0,0))
		.append("+2", cc.color(255,0,0))
		.append(") * 1 + (", cc.color(0,0,0))
		.append("-1", cc.color(255,0,0))
		.append(") * 1 ≠ 0", cc.color(0,0,0));
		ttf3.setPosition(cc.p(300, 420));
		ttf3.setVisible(false);
		this.objArr1.push(ttf3);
	},
	addlayout2:function(){
		layout = new ccui.Layout();// 第一页
		layout.setContentSize(cc.size(996, 650));// 设置layout的大小
		this.sv.addLayer(layout);

//		var text1 = $.format("新型净水剂铁酸钠(Na₂FeO₄)中铁元素的化合价是(  )\n" +
//				"\n      A．+2" +
//				"\n      B．+3" +
//				"\n      C．+5" +
//				"\n      D．+6", 980,30);
		var text1 = $.format("新型净水剂铁酸钠(Na₂FeO₄)中铁元素的化合价是(  )", 980,30);
		var label1 = new cc.LabelTTF(text1,gg.fontName,gg.fontSize4);
		label1.setColor(cc.color(0, 0, 0, 250));
		label1.setAnchorPoint(0, 0.5);
		label1.setPosition(50, layout.height - label1.height / 2-50);
		layout.addChild(label1);

		this.objArr2 = [];
		this.loadSelect(layout, "A．+2", "#wrong.png", cc.p(50,500), TAG_PROBLEM2, this.objArr2);
		this.loadSelect(layout, "B．+3", "#wrong.png", cc.p(50,460), TAG_PROBLEM2, this.objArr2);
		this.loadSelect(layout, "C．+5", "#wrong.png", cc.p(50,420), TAG_PROBLEM2, this.objArr2);
		this.loadSelect(layout, "D．+6", "#right.png", cc.p(50,380), TAG_PROBLEM2, this.objArr2);

		var text2 = $.format("解答:铁元素的化合价不是唯一的，确定化合物中变价元素的化合价方法是：" +
				"首先标出化合物中不变化元素的化合价。" +
				"然后利用化合物中各元素化合价的代数和为零，求出变化元素的化合价。" +
				"因此该化合物中铁元素化合价为+6" +
				",故选：D", 800,30);
		this.loadResult(layout, text2, cc.p(50,250), this.objArr2);
		
		var ttf1 = new exp04.RichTTF(layout)
		.append("Na₂FeO₄化合价代数和    (", cc.color(0,0,0))
		.append("+1", cc.color(255,0,0))
		.append(") * 2 + (", cc.color(0,0,0))
		.append("x", cc.color(255,0,0))
		.append(") * 1 + (", cc.color(0,0,0))
		.append("-2", cc.color(255,0,0))
		.append(") * 4 = 0 → ", cc.color(0,0,0))
		.append("x", cc.color(255,0,0))
		.append(" = ", cc.color(0,0,0))
		.append("+6", cc.color(255,0,0));
		ttf1.setPosition(cc.p(50, 300));
		ttf1.setVisible(false);
		this.objArr2.push(ttf1);
	},
	loadSelect:function(layout,str, answer, pos, tag, objArr){
		var button=new Angel(layout,"#select.png",this.callback,this);
		button.setPosition(pos);
		button.setTag(tag);
		var label = new cc.LabelTTF(str,"微软雅黑",30);
		label.setPosition(pos.x+button.width*0.5+label.width*0.5+10,pos.y);
		label.setColor(cc.color(0, 0, 0, 250));
		layout.addChild(label);
		var answer=new cc.Sprite(answer);
		answer.setPosition(pos);
		answer.setVisible(false);
		layout.addChild(answer);
		objArr.push(answer);
		
		
	},
	loadSelect1:function(layout, answer, pos, tag, objArr){
		var button=new Angel(layout,"#select.png",this.callback,this);
		button.setPosition(pos);
		button.setTag(tag);
		var answer=new cc.Sprite(answer);
		answer.setPosition(pos);
		answer.setVisible(false);
		layout.addChild(answer);
		objArr.push(answer)

	},
	loadResult:function(layout, text, pos, objArr){
		var label = new cc.LabelTTF(text, gg.fontName, gg.fontSize4);
		label.setVisible(false);
		label.setColor(cc.color(0, 0, 0, 300));
		label.setAnchorPoint(0, 1);
		label.setPosition(pos);
		layout.addChild(label);
		objArr.push(label)
	},
	callback:function(p){
		var objArr = [];
		switch (p.getTag()) {
		case TAG_PROBLEM1:
			objArr = this.objArr1;
			break;
		case TAG_PROBLEM2:
			objArr = this.objArr2;
			break;
		default:
			break;
		}
		for(var i = 0; i < objArr.length; i++){
			var obj = objArr[i];
			obj.setVisible(true);
		}
	}
});
