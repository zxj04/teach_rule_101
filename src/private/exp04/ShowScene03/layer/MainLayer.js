exp04.ShowMainLayer03 = cc.Layer.extend({
	lead:null,
	real:null,
	objArr:null,
	ctor:function () {
		this._super();
		this.init();
		return true;
	},
	init:function(){
		this.objArr = [];
		this.sv = new pub.ScrollView(this);
		this.sv.setOffestPos(cc.p(152,45 + 15));

		this.addlayout1();
		this.addlayout2();
	},	
	addlayout1:function(){
		var layout = new ccui.Layout();// 第一页
		layout.setContentSize(cc.size(996, 598));// 设置layout的大小
		this.sv.addLayer(layout);

		var text = $.format("不同原子构成分子时,各种原子的个数用\"化合价\"来标识。氢的化合价为\n+1,氧为-2,氢或氧与其他元素组成化合物时,其遵守的规则是化合物中所有元素化合价的代数和为零。", 980, 30);
		this.addText(layout, text, cc.p(50, 500));
		
		var label = new exp04.CF(layout,"H","2","").setColor(cc.color(0,0,0))
		var label1 = new exp04.CF(layout,"O化合价代数和　(","","").setColor(cc.color(0,0,0))
		new exp04.RichTTF(layout,gg.fontName,30)
		.append(label, cc.color(0,0,0))
		.append(label1, cc.color(0,0,0))
		.append("+1", cc.color(255,0,0))
		.append(") * 2 + (", cc.color(0,0,0))
		.append("-2", cc.color(255,0,0))
		.append(") * 1 = 0", cc.color(0,0,0))
		.setPosition(cc.p(200, 300));
		
		var label2 = new exp04.CF(layout,"CH","4","").setColor(cc.color(0,0,0))
		new exp04.RichTTF(layout,gg.fontName,30)
		.append(label2, cc.color(0,0,0))
		.append("化合价代数和　(", cc.color(0,0,0))
		.append("-4", cc.color(255,0,0))
		.append(") * 1 + (", cc.color(0,0,0))
		.append("+1", cc.color(255,0,0))
		.append(") * 4 = 0", cc.color(0,0,0))
		.setPosition(cc.p(200, 250));
		
		var label3 = new exp04.CF(layout,"CO","2","").setColor(cc.color(0,0,0))
		new exp04.RichTTF(layout,gg.fontName,30)
		.append(label3, cc.color(0,0,0))
		.append("化合价代数和　(", cc.color(0,0,0))
		.append("+4", cc.color(255,0,0))
		.append(") * 1 + (", cc.color(0,0,0))
		.append("-2", cc.color(255,0,0))
		.append(") * 2 = 0", cc.color(0,0,0))
		.setPosition(cc.p(200, 200));
	},
	addlayout2:function(){
		var layout = new ccui.Layout();// 第一页
		layout.setContentSize(cc.size(996, 598));// 设置layout的大小
		this.sv.addLayer(layout);
		
		//new exp04.CloudLabel(layout, " 一种元素可能\n会有多个化合价", exp04.CLOUD_RIGHT_UP).setPosition(880, 420);
		this.addText(layout,"一种元素可能会有多个化合价",cc.p(0,590));
		
		var posArr = [];
		for(var i = 0; i < 5; i++){
			var pos = cc.p(100 + 200 * i, 420);
			posArr.push(pos);
		}
		for(var i = 0; i < 5; i++){
			var pos = cc.p(100 + 200 * i, 160);
			posArr.push(pos);
		}
		var nameArr = ["铜\nCu","铁\nFe","锰\nMn","氯\nCl","硫\nS","碳\nC","氧\nO","氢\nH","磷\nP","氮\nN"];
		var letterArr = ["+1\n+2","+2\n+3","+2\n+4\n+6\n+7","-1\n+5\n+7","-2\n+4\n+6","-4\n+2\n+4","-2","+1","-3\n+5","-3\n+5"];
		for(var i = 0; i < 10; i++){
			var card = new exp04.Card(layout);
			card.addContent(letterArr[i], nameArr[i]).setPosition(posArr[i]);	
		}
	},
	addText:function(p, text, pos){
		var label = new cc.LabelTTF(text, gg.fontName, gg.fontSize4);
		label.setPosition(pos);
		label.setAnchorPoint(0, 0.5);
		label.setColor(cc.color(0, 0, 0, 250));
		p.addChild(label);
		return label;
	},
	callback:function(p){
		switch(p.getTag()){
		case TAG_BUTTON1:
			break;
		case TAG_BUTTON2:
			break;
		}
	}
	
});
