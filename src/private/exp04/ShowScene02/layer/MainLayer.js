exp04.ShowMainLayer02 = cc.Layer.extend({
	lead:null,
	real:null,
	curIndex: 0,
	cur:0,
	cell:50,
	ctor:function () {
		this._super();
		this.init();		
		return true;
	},
	init:function(){
		this.sv = new pub.ScrollView(this);
		this.sv.setOffestPos(cc.p(152,45 + 15));

		this.addlayout1();
		this.addlayout2();
	},
	addlayout1:function(){
		var layout = new ccui.Layout();// 第一页
		layout.setContentSize(cc.size(996, 598));// 设置layout的大小
		this.sv.addLayer(layout);

		var text = $.format("原子失去或获得电子后,成为离子。离子符号由原子的元素符号右上角标出该粒子所带的电荷数组成", 800, 30);
		this.addText(layout, text, cc.p(50, 510));
		
		this.addRichText(layout, "钠离子    ", 
				new exp04.CF(layout,"Na","","", true).setColor(cc.color(255,0,0)), 
				"    带一个单位正电荷", cc.p(100, 300));
		this.addRichText(layout, "硫离子    ", 
				new exp04.CF(layout,"S","","2", false).setColor(cc.color(255,0,0)), 
				"    带两个单位负电荷", cc.p(100, 250));
		this.addRichText(layout, "氢氧根离子    ",
				new exp04.CF(layout,"OH","","", false).setColor(cc.color(255,0,0)),
				"    带一个单位负电荷", cc.p(100, 200));
		this.addRichText(layout, "硫酸根离子    ",
				new exp04.CF(layout,"SO","4","2", false).setColor(cc.color(255,0,0)), 
				"    带一两单位负电荷", cc.p(100, 150));
		
		new exp04.CloudLabel(layout, "作为一个整体参\n加反应的原子集\n团叫做原子团。", 
				exp04.CLOUD_RIGHT_DOWN).setPosition(800, 300);
		
	},
	addRichText:function(p, t1, t2, t3, pos){
		new exp04.RichTTF(p)
		.append(t1, cc.color(0,0,0))
		.append(t2, cc.color(255,0,0))
		.append(t3, cc.color(0,0,0))
		.setPosition(pos);
	},
	addlayout2:function(){
		var layout = new ccui.Layout();// 第二页
		layout.setContentSize(cc.size(996, 598));
		this.sv.addLayer(layout);

		var text = $.format("带电原子团", 980, 30);
		this.addText(layout, text, cc.p(50, 580));

		text = $.format("翻动0次", 980, 30);
		var cLabel = this.addText(layout, text, cc.p(750, 580));
		this.cLabel = cLabel;

		var posArr = [];
		for(var i = 0; i < 5; i++){
			var pos = cc.p(100 + 200 * i, 420);
			posArr.push(pos);
		}
		for(var i = 0; i < 5; i++){
			var pos = cc.p(100 + 200 * i, 160);
			posArr.push(pos);
		}
		var nameArr = ["氢氧根\n 离子","硫酸根\n 离子","硝酸根\n 离子",
		               "碳酸根\n 离子","碳酸氢根\n  离子","铵根\n离子"];
		var letterArr = [new exp04.CF(null,"OH","","", false),
		                 new exp04.CF(null,"SO","4","2", false),
		                 new exp04.CF(null,"NO","3","", false),
		                 new exp04.CF(null,"CO","3","2", false),
		                 new exp04.CF(null,"HCO","3","", false),
		                 new exp04.CF(null,"NH","4","", true),];

		var sel = [];
		// 获得五个随机数
		for(var i = 0; i < 5; i++){
			var r = Math.round(Math.random() * (letterArr.length - 1));
			sel.push({tag: i, name: nameArr[r]}, {tag: i, name: letterArr[r]});
			nameArr.splice(r, 1);
			letterArr.splice(r, 1);
		}
		sel.sort(function(){ return 0.5 - Math.random();});// 打乱数组

		this.cardArr = [];
		for(var i = 0; i < 10; i++){
			var card = new exp04.Card(layout, this.cardCallback, this);
			card.addContent(sel[i].name, "沃赛\n精品").setPosition(posArr[i]);
			card.setTag(sel[i].tag);
			this.cardArr.push(card);
		}
	},
	count: 0,
	refreshCLabel:function(){
		this.count ++;
		this.cLabel.setString("翻动" + this.count + "次");
	},
	cardCallback:function(card){
		// 卡牌回调
		this.refreshCLabel();
		var cardIsPositive = card.isPositive();
		if(!cardIsPositive){
			return;
		}
		for(var i = 0; i < this.cardArr.length; i++){
			var obj = this.cardArr[i];
			if(obj === card){
				continue;
			}
			if(cardIsPositive
					&& obj.isPositive()){
				this.enableAllCard(false);
				this.scheduleOnce(function(){
					if(card.getTag() == obj.getTag()){
						card.overAction();
						obj.overAction();
						this.removeFromArray(this.cardArr, card);
						this.removeFromArray(this.cardArr, obj);
						new ShowTip("你真棒!", cc.p(640, 600), true, this);
					} else {
						card.closeCard();
						obj.closeCard();
					}
					this.scheduleOnce(function(){
						this.enableAllCard(true);
					}, 0.5);
				}, 1);
				break;
			} 
		}
	},
	removeFromArray:function(array, e){
		for(var i = 0; i < array.length; i ++){
			var a = array[i];
			if(a === e){
				array.splice(i, 1);
				i = i - 1;
			}
		}
	},                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      
	enableAllCard:function(enable){
		for(var i = 0; i < this.cardArr.length; i++){
			var obj = this.cardArr[i];
			obj.setEnable(enable);
		}
	},
	addText:function(p, text, pos){
		var label = new cc.LabelTTF(text, gg.fontName, gg.fontSize4);
		label.setPosition(pos);
		label.setAnchorPoint(0, 0.5);
		label.setColor(cc.color(0, 0, 0, 250));
		p.addChild(label);
		return label;
	},
	addbutton:function(parent,tag,str,pos,labelx){
		var button = new ButtonScale(parent,"#unsel.png",this.callback,this);
		button.setPosition(pos);
		button.setLocalZOrder(5);
		button.setTag(tag);
		var label = new cc.LabelTTF(str,gg.fontName,30);
		label.setPosition(button.width/2,button.height/2);		
		label.setColor(cc.color(0, 0, 0, 250));
		button.addChild(label);
	},
	addimage:function(parent,str,pos,str2,pos2){
		var image = new cc.Sprite(str);
		parent.addChild(image);
		image.setPosition(pos);
		var label = new cc.LabelTTF(str2,gg.fontName,gg.fontSize);
		label.setColor(cc.color(0, 0, 0, 250));
		parent.addChild(label);
		label.setPosition(pos2);

	},
	callback:function(p){		
		switch(p.getTag()){
		case TAG_BUTTON1:
			p.setEnable(false);
			this.label.setVisible(true);
			var move = cc.moveTo(0.3,cc.p(980,150));
			var move1 = cc.moveTo(0.5,cc.p(1050,100));
			var seq= cc.sequence(move,move1,cc.delayTime(1),cc.callFunc(function(){
				p.setEnable(true);
				this.zhi.setPosition(800,150);
				this.zhi.setRotation(0);
			},this));
			this.zhi.runAction(seq);
			break;
		case TAG_BUTTON2:
			p.setEnable(false);
			var move = cc.moveTo(0.3,cc.p(250,300));
			var move1 = cc.moveTo(0.5,cc.p(400,300));
			var move2 = cc.moveTo(0.5,cc.p(450,200));
			var seq = cc.sequence(move,cc.callFunc(function(){
				var rota = cc.rotateTo(0.2,-90);
				var mov = cc.moveTo(0.2,cc.p(115,260));
				var sp = cc.spawn(rota,mov);

				var mov1 = cc.moveTo(0.4,cc.p(115,180));
				var mov2 = cc.moveTo(0.8,cc.p(115,100));

				var mov3 = cc.moveTo(0.5,cc.p(115,50));
				var rota1 = cc.rotateTo(0.6,0);
				var sp1= cc.spawn(rota1,mov3);
				var se = cc.sequence(sp,mov1,mov2,sp1);
				this.yingbi.runAction(se);

			},this),move1,move2,cc.delayTime(1.5),cc.callFunc(function(){
				p.setEnable(true);
				this.yingbi.setPosition(115,300);
				this.zhi1.setPosition(130,300);
			},this));
			this.zhi1.runAction(seq);
			break;
	}
	}
});
