exp09.ShowMainLayer03 = cc.Layer.extend({
	lead:null,
	real:null,
    donum:0,
	ctor:function () {
		this._super();
		this.init();
		return true;
	},
	init:function(){
		var sv = new pub.ScrollView(this);
		sv.setOffestPos(cc.p(152,45 + 15));

		this.addlayout1(sv);

	},	
	addlayout1:function(parent){
		this.layout1 = new ccui.Layout();//第一页
		this.layout1.setContentSize(cc.size(996, 598));//设置layout的大小
		parent.addLayer(this.layout1);

		var text = $.format("　　现将相同两棵绿色植物放在黑暗处一晚，用透明塑料袋将植物罩住，左边是清水，右边放入碱石灰。" +
				"然后在阳光下照射4小时后，各取一片叶子检验淀粉。", 960, 30);
		
		label1 = new cc.LabelTTF(text,gg.fontName,gg.fontSize4);
		label1.setAnchorPoint(0,0.5);
		label1.setPosition(0,550);
		label1.setColor(cc.color(255, 255, 255, 250));
		this.layout1.addChild(label1,10);	
		
		var bg = new cc.Sprite(res_public.run_back);
		this.layout1.addChild(bg);
		bg.setScale(0.8);
		bg.setPosition(cc.p(this.layout1.width /2, this.layout1.height / 2 +30));
		
		this.addImage(this.layout1, 6, "#jianshihui.png", cc.p(790,285), 0.9);	
		this.addImage(this.layout1, 5, "#zhiwu.png", cc.p(300,350), 1);		
		this.addImage(this.layout1, 5, "#zhiwu.png", cc.p(750,350), 1);
		
		this.yezi = new cc.Sprite("#yezi.png");
		this.yezi.setPosition(cc.p(330,330));
		this.layout1.addChild(this.yezi,4);
		
		this.yezi1 = new cc.Sprite("#yezi.png");
		this.yezi1.setPosition(cc.p(700,350));
		this.layout1.addChild(this.yezi1,4);
		
		this.yezi2 = new cc.Sprite("#yezi1.png");
		this.yezi2.setPosition(cc.p(470,250));
		this.layout1.addChild(this.yezi2,5);
		this.yezi2.setOpacity(0);
		
		this.yezi3 = new cc.Sprite("#yezi1.png");
		this.yezi3.setPosition(cc.p(570,250));
		this.layout1.addChild(this.yezi3,5);
		this.yezi3.setOpacity(0);
		
		this.addbutton(this.layout1, TAG_BUTTON1, "播放", cc.p(500,150));
		this.layout1.getChildByTag(TAG_BUTTON1).getChildByTag(TAG_BUTTON1).setColor(cc.color(255, 255, 255, 250));
	
		var text = $.format("现象：左边叶子淀粉检验呈蓝色，右边叶子呈黄色。" +
				"\n结论：植物进行光合作用需要二氧化碳。", 980,30);
		this.label = new cc.LabelTTF(text,gg.fontName,gg.fontSize4);
		this.label.setColor(cc.color(255, 0, 0, 250));
		this.label.setAnchorPoint(0, 0.5);
		this.label.setPosition(80,80);
		this.layout1.addChild(this.label,10);	
		this.label.setVisible(false);
	},
	
	addImage:function(parent,index,str,pos,scale){
		var image = new cc.Sprite(str);
		parent.addChild(image,5);
		image.setPosition(pos);
		if(scale !=null){
			image.setScale(scale);
		}else{
			image.setScale(1);
		}
		image.setLocalZOrder(index);
	},
	addbutton:function(parent,tag,str,pos){
		var button = new ButtonScale(parent,"#unsel.png",this.callback,this);
		button.setPosition(pos);
		button.setLocalZOrder(5);
		button.setTag(tag);
		var label = new cc.LabelTTF(str,gg.fontName,30);
		label.setPosition(button.width/2,button.height/2);		
		label.setColor(cc.color(0, 0, 0, 250));
		button.addChild(label);
		label.setTag(tag);
	},
	callback:function(p){
		switch(p.getTag()){	
		case TAG_BUTTON1:
			if(this.donum == 0){
				p.setEnable(false);
				var seq = cc.sequence(cc.callFunc(function(){
					this.yezi.runAction(cc.moveTo(1,cc.p(470,250)));
					this.yezi1.runAction(cc.moveTo(1,cc.p(570,250)));
				},this),cc.delayTime(1),cc.callFunc(function(){
					this.layout1.getChildByTag(TAG_BUTTON1).getChildByTag(TAG_BUTTON1).setString("水浴");
					p.setEnable(true);
					this.donum = 1;
				},this));
				p.runAction(seq);
			}else if(this.donum == 1){
				p.setEnable(false);
				var seq = cc.sequence(cc.callFunc(function(){
					this.yezi.runAction(cc.fadeOut(2));
					this.yezi1.runAction(cc.fadeOut(2));
					this.yezi2.runAction(cc.fadeIn(2));
					this.yezi3.runAction(cc.fadeIn(2));
				},this),cc.delayTime(2),cc.callFunc(function(){
					this.layout1.getChildByTag(TAG_BUTTON1).getChildByTag(TAG_BUTTON1).setString("检验");
					p.setEnable(true);
					this.donum = 2;
				},this));
				p.runAction(seq);
			}else if(this.donum == 2){
				p.setEnable(false);
				var seq = cc.sequence(cc.callFunc(function(){
					this.yezi.setSpriteFrame("yezi2.png");
					this.yezi.runAction(cc.fadeIn(2));
					this.yezi2.runAction(cc.fadeOut(2));					
				},this),cc.delayTime(2),cc.callFunc(function(){
					this.layout1.getChildByTag(TAG_BUTTON1).getChildByTag(TAG_BUTTON1).setString("复原");
					this.label.setVisible(true);
					p.setEnable(true);
					this.donum = 3;
				},this));
				p.runAction(seq);
			}else if(this.donum == 3){		
					this.yezi.setSpriteFrame("yezi.png");
					this.yezi.setPosition(cc.p(330,330));
					this.yezi1.setPosition(cc.p(700,350));
					this.yezi1.setOpacity(255);
					this.yezi2.setOpacity(0);
					this.yezi3.setOpacity(0);										
					this.layout1.getChildByTag(TAG_BUTTON1).getChildByTag(TAG_BUTTON1).setString("复原");	
					this.donum = 0;	
			}
		break;
		
		    	
		}
	}	
});
