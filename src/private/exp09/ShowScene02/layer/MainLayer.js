exp09.ShowMainLayer02 = cc.Layer.extend({
	lead:null,
	real:null,
    donum:0,
	ctor:function () {
		this._super();
		this.init();
		return true;
	},
	init:function(){	
		var sv = new pub.ScrollView(this);
		sv.setOffestPos(cc.p(152,45 + 15));
	
		this.addlayout1(sv);	

	},	
	addlayout1:function(parent){
		this.layout1 = new ccui.Layout();//第一页
		this.layout1.setContentSize(cc.size(996, 598));//设置layout的大小
		parent.addLayer(this.layout1);

		var text = $.format("　　将下图装置放在阳光下，验证光合作用是否能够产生氧气？", 980, 30);
		label1 = new cc.LabelTTF(text,gg.fontName,gg.fontSize4);
		label1.setAnchorPoint(0,0.5);
		label1.setPosition(0,this.layout1.height-label1.height/2);
		label1.setColor(cc.color(255, 255, 255, 250));
		this.layout1.addChild(label1,10);	
		
		var text = $.format("　　用大拇指在水中盖住试管口，将试管取出，用带火星的火柴放在试管口，观察现象。", 980, 30);
		this.label = new cc.LabelTTF(text,gg.fontName,gg.fontSize4);
		this.label.setAnchorPoint(0,0.5);
		this.label.setPosition(0,520);
		this.label.setColor(cc.color(255, 255, 255, 250));
		this.layout1.addChild(this.label,10);
		this.label.setVisible(false);
		
		var bg = new cc.Sprite(res_public.run_back);
		this.layout1.addChild(bg);
		bg.setScale(0.8);
		bg.setPosition(cc.p(this.layout1.width /2, this.layout1.height / 2 +30));
	
		this.addImage(this.layout1, 2, "#match/match.png", cc.p(750,250), 0.4);
		
		this.match = new cc.Sprite("#match/match2.png");
		this.match.setPosition(cc.p(800,300));
		this.layout1.addChild(this.match,7);
		this.match.setScale(0.35);
	
		
		this.addImage(this.layout1, 2, "#guanghe/9.png", cc.p(300,500 -50), 0.8);
		
//		this.addImage(this.layout1, 2, "#guanghe/1.png", cc.p(500,300 -50), 0.8);
//		this.addImage(this.layout1, 10, "#guanghe/2.png", cc.p(500,300 -50), 0.8);
//		this.addImage(this.layout1, 5, "#guanghe/3.png", cc.p(498,215 -50), 0.8);
//		this.addImage(this.layout1, 3, "#guanghe/4.png", cc.p(500,310 -50), 0.8);
//		this.addImage(this.layout1, 7, "#guanghe/5.png", cc.p(500,310 -50), 0.8);
//		this.addImage(this.layout1, 8, "#guanghe/7.png", cc.p(500,400 -50), 0.8);
		
		
		this.beaker = new cc.Sprite("#guanghe/1.png");
		this.beaker.setPosition(cc.p(500,300 -50));
		this.layout1.addChild(this.beaker,2);
		this.beaker.setScale(0.8);
		
		this.beaker1 = new cc.Sprite("#guanghe/2.png");
		this.beaker1.setPosition(cc.p(500,300 -50));
		this.layout1.addChild(this.beaker1,10);
		this.beaker1.setScale(0.8);
		
		this.haizao = new cc.Sprite("#guanghe/3.png");
		this.haizao.setPosition(cc.p(498,215 -50));
		this.layout1.addChild(this.haizao,5);
		this.haizao.setScale(0.8);
		
		this.loudou = new cc.Sprite("#guanghe/4.png");
		this.loudou.setPosition(cc.p(500,310 -50));
		this.layout1.addChild(this.loudou,3);
		this.loudou.setScale(0.8);
		
		this.loudou1 = new cc.Sprite("#guanghe/5.png");
		this.loudou1.setPosition(cc.p(500,310 -50));
		this.layout1.addChild(this.loudou1,7);
		this.loudou1.setScale(0.8);
		
		this.sg = new cc.Sprite("#guanghe/7.png");
		this.sg.setPosition(cc.p(500,400 -50));
		this.layout1.addChild(this.sg,8);
		this.sg.setScale(0.8);
		
		this.water =  new cc.Sprite("#guanghe/8.png");
		this.water.setPosition(cc.p(500,515 -50));
		this.layout1.addChild(this.water,7);
		this.water.setScale(0.4);
		
		this.water1 =  new cc.Sprite("#guanghe/6.png");
		this.water1.setPosition(cc.p(505,300));
		this.layout1.addChild(this.water1,7);
		this.water1.setScale(0.8);
	
	
		this.addbutton(this.layout1, TAG_BUTTON1, "播放", cc.p(250,180));
		this.layout1.getChildByTag(TAG_BUTTON1).getChildByTag(TAG_BUTTON1).setColor(cc.color(255, 255, 255, 250));
				
		this.label1 = new cc.LabelTTF("现象：光合作用的产生的气体使带火星的火柴复燃。" +
				"\n结论：说明光合作用有一个产物是氧气。",gg.fontName,gg.fontSize4);
		this.label1.setAnchorPoint(0,0.5);
		this.label1.setPosition(80,80);
		this.label1.setColor(cc.color(255, 0, 0, 250));
		this.layout1.addChild(this.label1,10);
		this.label1.setVisible(false);
		
	},
	
	addAction:function(str1,num,time){
		var frames = [];
		for(var i =1 ;i<=num;i++){
			var str = str1+i+".png";
			var frame=cc.spriteFrameCache.getSpriteFrame(str);
			frames.push(frame);		
		}
		var animation = new cc.Animation(frames,time);//负责动画序列
		var action = new cc.Animate(animation);//帧动画的动作创建
		return action.repeatForever();
	},
	addImage:function(parent,index,str,pos,scale){
		var image = new cc.Sprite(str);
		parent.addChild(image,5);
		image.setPosition(pos);
		if(scale !=null){
			image.setScale(scale);
		}else{
			image.setScale(1);
		}
		image.setLocalZOrder(index);
	},
	addbutton:function(parent,tag,str,pos){
		var button = new ButtonScale(parent,"#unsel.png",this.callback,this);
		button.setPosition(pos);
		button.setLocalZOrder(5);
		button.setTag(tag);
		var label = new cc.LabelTTF(str,gg.fontName,30);
		label.setPosition(button.width/2,button.height/2);		
		label.setColor(cc.color(0, 0, 0, 250));
		button.addChild(label);
		label.setTag(tag);
	},
	addbubble:function(time,posy){
		var bubble = new cc.Sprite("#guanghe/bubble2.png");
		bubble.setPosition(cc.p(500,390));
		this.layout1.addChild(bubble,8);
		bubble.setScale(0.8);
		var seq = cc.sequence(cc.spawn(cc.moveTo(time,cc.p(500,posy)),cc.scaleTo(time,1)),cc.callFunc(function(){
			bubble.removeFromParent(true);
		},this));
		bubble.runAction(seq);
	},
	callback:function(p){
		switch(p.getTag()){	
		case TAG_BUTTON1:
			if(this.donum ==0){
				p.setEnable(false);			
				var spawn = cc.spawn(cc.moveTo(0.2,cc.p(500,510 -50)),cc.scaleTo(0.2,0.7));
				var move = cc.moveTo(8,cc.p(500,290 -50));
				var seq = cc.sequence(spawn,move,cc.callFunc(function(){
					p.setEnable(true);
					this.layout1.getChildByTag(TAG_BUTTON1).getChildByTag(TAG_BUTTON1).setString("验证");
					this.label.setVisible(true);
					this.donum =1;
				},this));
				this.water.runAction(seq);
			}else if(this.donum ==1){
				p.setEnable(false);
				this.water.setVisible(false);
				this.water1.setVisible(false);
				this.beaker.setVisible(false);
				this.beaker1.setVisible(false);
				this.haizao.setVisible(false);
				this.loudou.setVisible(false);
				this.loudou1.setVisible(false);				
				this.match.setVisible(true);
				var move = cc.moveTo(0.5,cc.p(855,270));
				var move1 = cc.moveTo(0.5,cc.p(780,350));
				var move2 = cc.moveTo(1.5,cc.p(525,430));
				var seq = cc.sequence(move,cc.callFunc(function(){
					this.match.setSpriteFrame("match/match1.png");
				},this),move1,cc.callFunc(function(){
					this.match.setSpriteFrame("match/match3.png");
				},this),move2,cc.callFunc(function(){
					this.match.setSpriteFrame("match/match4.png");
				},this),cc.callFunc(function(){
					p.setEnable(true);
					this.layout1.getChildByTag(TAG_BUTTON1).getChildByTag(TAG_BUTTON1).setString("复原");
					this.label1.setVisible(true);
					this.donum =2;
				},this));
                this.match.runAction(seq);                             
                this.sg.runAction(cc.spawn(cc.moveTo(1,cc.p(500,300)),cc.rotateTo(1,180)));
			}else if(this.donum ==2){
				this.layout1.getChildByTag(TAG_BUTTON1).getChildByTag(TAG_BUTTON1).setString("播放");
				this.match.setVisible(false);
				this.match.setPosition(cc.p(800,300));
				this.match.setSpriteFrame("match/match2.png");
				this.donum = 0;
				this.water.setVisible(true);
				this.water.setPosition(cc.p(500,515 -50));
				this.water.setScale(0.4);
				this.water1.setVisible(true);
				this.beaker.setVisible(true);
				this.beaker1.setVisible(true);
				this.haizao.setVisible(true);
				this.loudou.setVisible(true);
				this.loudou1.setVisible(true);
				this.sg.setPosition(cc.p(500,350));
				this.sg.setRotation(0);
			}

		break;
	
		}
	}
});
