exp09.startRunNext = function(tag){
	switch(tag){//具体哪个菜单执行什么操作，这里修改
	case TAG_MENU1:
		pub.ch.gotoShow(TAG_EXP_09,tag , exp09.g_resources_show, null, new exp09.ShowScene01(tag));
		break;
	case TAG_MENU2:
		gg.teach_type = TAG_LEAD;
		gg.teach_flow = exp09.teach_flow01;
		pub.ch.gotoRun(TAG_EXP_09, tag,exp09.g_resources_run, null, new exp09.RunScene01());
		break;
	case TAG_MENU3:
		pub.ch.gotoShow(TAG_EXP_09,tag , exp09.g_resources_show, null, new exp09.ShowScene02(tag));
		break;
	case TAG_MENU4:
		gg.teach_type = TAG_LEAD;
		gg.teach_flow = exp09.teach_flow02;
		pub.ch.gotoRun(TAG_EXP_09, tag,exp09.g_resources_run, null, new exp09.RunScene02());
		break;
	case TAG_MENU5:
		pub.ch.gotoShow(TAG_EXP_09,tag , exp09.g_resources_show, null, new exp09.ShowScene03(tag));
		break;
	case TAG_MENU6:
		pub.ch.gotoShow(TAG_EXP_09,tag , exp09.g_resources_show, null, new exp09.ShowScene04(tag));
		break;
	case TAG_MENU7:
		pub.ch.gotoShow(TAG_EXP_09,tag , exp09.g_resources_show, null, new exp09.ShowScene05(tag));
		break;
	case TAG_ABOUT:
		ch.gotoAbout(TAG_EXP_09, g_resources_public_about, null, res_public_about.about_j);
		break;
	}
}

// 任务流
exp09.teach_flow01 = 
	[
	 {
		 tip:"提供光照",
		 tag:[
		      TAG_SUN,
		      TAG_SUN1  
		      ]
	 },
	 {
		 tip:"提供二氧化碳",
		 tag:[
		      TAG_BOTANY,
		      TAG_BOTANY1,
		      TAG_CO2
		      ]
	 },
	 {
		 tip:"提供水分",
		 tag:[
		      TAG_BOTANY,
		      TAG_BOTANY1,
		      TAG_H2O
		      ]
	 },
	 {
		 tip:"恭喜过关",
		 over:true

	 }
	 ]

exp09.teach_flow02 = 
	[
	 {
		 tip:"选择盆栽",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG1,	   
		      ],
		      canClick:true		 
	 },
	 {
		 tip:"选择酒精灯",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG2,
		      ],
		      canClick:true	
	 },
	 {
		 tip:"选择三脚架",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG3,	   
		      ],
		      canClick:true		 
	 },
	 {
		 tip:"选择胶头滴管",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG5,	   
		      ],
		      canClick:true		 
	 },
	 {
		 tip:"选择烧杯",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG8,	   
		      ],
		      canClick:true		 
	 },
	 {
		 tip:"选择玻璃片",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG7,	   
		      ],
		      canClick:true		 
	 },
	 {
		 tip:"选择镊子",
		 tag:[
		      TAG_LIB,
		      TAG_LIB_BG9,	   
		      ],
		      canClick:true		 
	 },
	 {
		 tip:"点击确定，开始实验",
		 tag:[
		      TAG_LIB,
		      TAG_SURE
		      ]
	 },
	 {
		 tip:"将盆栽放到阴暗处放置一昼夜",
		 tag:[
		      TAG_BOTANY,
		      TAG_BOTANY1],
		      action:ACTION_DO1
	 },
	 {
		 tip:"把铝箔放在植物的一个叶片的底面",
		 tag:[
		      TAG_BOTANY,
		      TAG_FOIL2
		      ]
	 },
	 {
		 tip:"把另一片铝箔放在刚才铝箔的上方遮盖叶子的上部",
		 tag:[
		      TAG_FOIL,
		      TAG_FOIL1	
		      ],
		      action:ACTION_DO1
	 },
	 {
		 tip:"将图钉钉在铝箔上用于固定铝箔",
		 tag:[
		      TAG_NAIL,
		      TAG_NAIL1
		      ],
		      action:ACTION_DO1
	 },
	 {
		 tip:"将盆栽放到阳光下一段时间",
		 tag:[
		      TAG_BOTANY,
		      TAG_BOTANY1],
		      action:ACTION_DO2
	 },
	 {
		 tip:"将图钉取下",
		 tag:[
		      TAG_NAIL,
		      TAG_NAIL1
		      ],
		      action:ACTION_DO2
	 },
	 {
		 tip:"把两片铝箔从植物上取下",
		 tag:[
		      TAG_FOIL,
		      TAG_FOIL1	
		      ],
		      action:ACTION_DO2
	 },
	 {
		 tip:"将叶子取下",
		 tag:[ TAG_BOTANY,
		       TAG_BOTANY1,
		       TAG_LEAF]
	 },
	 {
		 tip:"将叶子取下放入装有酒精的小烧杯中",
		 tag:[
		      TAG_LEAFS,
		      TAG_LEAFS1
		      ]
	 },
	 {
		 tip:"打开酒精灯盖",
		 tag:[
		      TAG_LAMP,
		      TAG_LID],
		      action:ACTION_DO1
	 },
	 {
		 tip:"用火柴将酒精灯点燃",
		 tag:[
		      TAG_MATCH,
		      TAG_MATCH1
		      ],
	 },
	 {
		 tip:"将酒精灯放在三脚架下方",
		 tag:[
		      TAG_LAMP,
		      TAG_LAMP1
		      ],
		      action:ACTION_DO1
	 },
	 {
		 tip:"将酒精灯从三脚架下面拿出并将酒精灯帽盖上",
		 tag:[
		      TAG_LAMP,
		      TAG_LAMP1
		      ],
		      action:ACTION_DO2
	 },
	 {
		 tip:"用镊子将浸泡在酒精中的叶子夹到玻璃片上",
		 tag:[
		      TAG_TWEEZER,
		      TAG_TWEEZER1
		      ],
	 },
	 {
		 tip:"用胶头滴管吸取碘液滴在叶子上",
		 tag:[
		      TAG_BOTTLE,
		      TAG_DROP
		      ],
	 },
	 {
		 tip:"恭喜过关",
		 over:true

	 }
	 ]

