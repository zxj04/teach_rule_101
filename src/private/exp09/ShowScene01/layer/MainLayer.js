exp09.ShowMainLayer01 = cc.Layer.extend({
	lead:null,
	real:null,
	reset:true,
	reset1:false,
	open_flag:false,
	donum:0,
	ctor:function () {
		this._super();
		this.init();
		return true;
	},
	init:function(){	
		var sv = new pub.ScrollView(this);
		sv.setOffestPos(cc.p(152,45 + 15));

		this.addlayout1(sv);	
		this.addlayout2(sv);	
	},	
	addlayout1:function(parent){
		this.layout1 = new ccui.Layout();//第一页
		this.layout1.setContentSize(cc.size(996, 598));//设置layout的大小
		parent.addLayer(this.layout1);
	
		var text = $.format("　　光合作用是指绿色植物通过叶绿体，利用光能，把二氧化碳和水转化成储存着能量的有机物，并释放氧气的过程。" +
				"\n它可以用下列反应式表示：" , 980,30);
		label = new cc.LabelTTF(text,gg.fontName,gg.fontSize4);
		label.setColor(cc.color(0, 0, 0, 250));
		label.setAnchorPoint(0, 0.5);
		label.setPosition(0,this.layout1.height-label.height/2);
		this.layout1.addChild(label);	
		
		this.addImage(this.layout1,"#gongshi.png",cc.p(500,370));
		
		var text = $.format("　　光合作用主要发生在叶肉细胞的叶绿体中，它产生的葡萄糖经转化后形成了淀粉。" +
				"光合作用所需要的二氧化碳大部分是从空气吸收的，水则主要是从土壤中吸收，然后与疏导叶片。叶绿体内含有的叶绿素" +
				"等色素，能吸收阳光。", 980,30);
		label = new cc.LabelTTF(text,gg.fontName,gg.fontSize4);
		label.setColor(cc.color(0, 0, 0, 250));
		label.setAnchorPoint(0, 0.5);
		label.setPosition(0,200);
		this.layout1.addChild(label);							
	},
	addlayout2:function(parent){
	    this.layout2 = new ccui.Layout();//第一页
	    this.layout2.setContentSize(cc.size(996, 598));//设置layout的大小
	    parent.addLayer(this.layout2);

	    var text = $.format("　　植物光合作用的过程是十分复杂的，它包括许多化学反应，但主要包含了以下两个方面的变化：" +
	    		"\n1、把简单的无机物制成了复杂的有机物，并释放出氧气，发生了物质的转换。" +
	    		"\n2、把光能变成储存在有机物里的化学能，实现了能量的转化。", 980,30);
	    label = new cc.LabelTTF(text,gg.fontName,gg.fontSize4);
	    label.setColor(cc.color(0, 0, 0, 250));
	    label.setAnchorPoint(0, 0.5);
	    label.setPosition(0,this.layout2.height-label.height/2);
	    this.layout2.addChild(label);	
	     
	    this.addImage(this.layout2,"#tu.png",cc.p(500,250));
        
	    var text = $.format("　　绿色植物通过光合作用制造的与偶记无不仅满足了自身生长、发育、繁殖的需要，" +
	    		"还为其他生物提供了基本的食物来源。", 980,30);
	    label = new cc.LabelTTF(text,gg.fontName,gg.fontSize4);
	    label.setColor(cc.color(0, 0, 0, 250));
	    label.setAnchorPoint(0, 0.5);
	    label.setPosition(0,60);
	    this.layout2.addChild(label);	

	},
	addImage:function(parent,str,pos){
		var image = new cc.Sprite(str);
		parent.addChild(image,5);
		image.setPosition(pos);
	},
	addshowimage:function(parent,str,pos,str2){
		var showimage = new cc.Sprite(str);
		showimage.setPosition(pos);
		parent.addChild(showimage,5);
		var label = new cc.LabelTTF(str2,gg.fontName,gg.fontSize);
		label.setColor(cc.color(0, 0, 0, 250));
		parent.addChild(label);
		label.setPosition(showimage.x,showimage.y - showimage.height * 0.5 -20);
	},
	addbutton:function(parent,tag,str,pos){
		var button = new ButtonScale(parent,"#unsel.png",this.callback,this);
		button.setPosition(pos);
		button.setLocalZOrder(5);
		button.setTag(tag);
		var label = new cc.LabelTTF(str,gg.fontName,30);
		label.setPosition(button.width/2,button.height/2);		
		label.setColor(cc.color(0, 0, 0, 250));
		button.addChild(label);
		label.setTag(tag);
	},
	callback:function(p){
		switch(p.getTag()){	
		
		}
	}
});
