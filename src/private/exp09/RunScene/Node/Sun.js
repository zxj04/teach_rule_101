exp09.Sun = cc.Node.extend({
	ctor:function(p){
		this._super();
		p.addChild(this, 10, TAG_SUN);
		this.init();
	},
	init : function(){
		this.setCascadeOpacityEnabled(true);
		
		var fontDef = new cc.FontDefinition();
		fontDef.fontName = "Arial";
		fontDef.fontSize = "20";
		fontDef.fillStyle=cc.color(240, 129, 6,255);
		
		this.sun=new Button(this,5,TAG_SUN1,"#exp13/sun.png",this.callback);
		
		this.right=new Button(this.sun,5,TAG_RIGHT,"#exp13/right.png",this.callback);
		this.right.setPosition(cc.p(this.sun.width*1.5, -this.sun.height));
		this.right.setCascadeOpacityEnabled(true);
		this.right.setOpacity(0);
		
		this.label=new cc.LabelTTF("光能",fontDef);
		this.label.setRotation(30);
		this.right.addChild(this.label);
		this.label.setPosition(cc.p(this.right.width*0.7,this.right.height*0.88));
	},
	action:function(){
		var fadein=cc.fadeIn(0.5);
		var fadeout=cc.fadeOut(0.5);
		var right=this.getChildByTag(TAG_SUN1).getChildByTag(TAG_RIGHT);
		right.runAction(cc.sequence(cc.sequence(fadein,fadeout).repeat(3),fadein));
	},
	callback:function(p){
		var func=cc.callFunc(function() {
			gg.flow.next();
		}, this);
		switch (p.getTag()) {
		case TAG_SUN1:
			p.runAction(cc.sequence(cc.callFunc(function() {
				this.action();
			}, this),cc.delayTime(4),func));
			ll.tip.mdScore(10);
			break;
		default:
			break;
		}
	}
});