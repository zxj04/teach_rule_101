exp09.Botany = cc.Node.extend({
	ctor:function(p){
		this._super();
		p.addChild(this, 10, TAG_BOTANY);
		this.init();
	},
	init : function(){
		this.setCascadeOpacityEnabled(true);
		
		var fontDef = new cc.FontDefinition();
		fontDef.fontName = "Arial";
		fontDef.fontSize = "35";
		fontDef.fillStyle=cc.color(250,250,210,255);
		
		this.botany=new Button(this,5,TAG_BOTANY1,"#exp13/botany.png",this.callback,this);
		
		this.title=new Button(this,5,TAG_TITLE,"#exp13/label.png",this.callback,this);
		this.title.setPosition(cc.p(-350, -200));
		this.title.setVisible(false);
		
		this.co2=new Label(this.botany,"CO₂",this.callback,this);
		this.co2.setTag(TAG_CO2);
		this.co2.setPosition(cc.p(this.botany.width*1.1, this.botany.height*0.75));
		
		this.left=new Button(this.botany,5,TAG_LEFT,"#exp13/left.png",this.callback,this);
		this.left.setPosition(cc.p(this.botany.width*0.85, this.botany.height*0.75));
		this.left.setCascadeOpacityEnabled(true);
		this.left.setOpacity(0);
		
		this.label=new cc.LabelTTF("二氧化碳",fontDef);
		this.left.addChild(this.label);
		this.label.setPosition(cc.p(this.left.width*0.5,this.left.height*1.4 + 20));
		
		this.left1=new Button(this.botany,5,TAG_LEFT1,"#exp13/left1.png",this.callback,this);
		this.left1.setPosition(cc.p(0,this.botany.height*0.65));
		this.left1.setCascadeOpacityEnabled(true);
		this.left1.setOpacity(0);
		
		this.label1=new cc.LabelTTF("氧气","Arial",20);
		this.left1.addChild(this.label1);
		this.label1.setPosition(cc.p(this.left1.width*0.6,this.left1.height*1.4 + 10));
		
		this.o2=new Label(this.botany,"O₂",this.callback,this);
		this.o2.setTag(TAG_O2);
		this.o2.setPosition(cc.p(-this.botany.width*0.2, this.botany.height*0.73));
		this.o2.setVisible(false);
		
		this.h2o=new Label(this.botany,"H₂O",this.callback,this);
		this.h2o.setTag(TAG_H2O);
		this.h2o.setEnable(false);
		this.h2o.setPosition(cc.p(this.botany.width*0.55, -this.botany.height*0.25));
		
		this.up=new Button(this.botany,5,TAG_UP,"#exp13/up.png",this.callback,this);
		this.up.setPosition(cc.p(this.botany.width*0.55,this.botany.height*0.1));
		this.up.setCascadeOpacityEnabled(true);
		this.up.setOpacity(0);
		
		this.label2=new cc.LabelTTF("水",fontDef);
		this.up.addChild(this.label2);
		this.label2.setPosition(cc.p(this.up.width*1.2 + 20,this.up.height*0.5));
		
		this.down=new Button(this.botany,5,TAG_DOWN,"#exp13/down.png",this.callback,this);
		this.down.setPosition(cc.p(this.botany.width*0.45,this.botany.height*0.3));
		this.down.setCascadeOpacityEnabled(true);
		this.down.setOpacity(0);
		
		this.label3=new cc.LabelTTF("有" +
									"\n机" +
									"\n物",fontDef);
		this.down.addChild(this.label3);
		this.label3.setPosition(cc.p(-this.down.width*0.2 - 20,this.down.height*0.5));
		
	},
	action:function(){
		var fadein=cc.fadeIn(0.5);
		var fadeout=cc.fadeOut(0.5);
		var left=this.getChildByTag(TAG_BOTANY1).getChildByTag(TAG_LEFT);
		left.runAction(cc.sequence(cc.sequence(fadein,fadeout).repeat(3),fadein));
	},
	action1:function(){
		var fadein=cc.fadeIn(0.5);
		var fadeout=cc.fadeOut(0.5);
		var left1=this.getChildByTag(TAG_BOTANY1).getChildByTag(TAG_LEFT1);
		left1.runAction(cc.sequence(cc.sequence(fadein,fadeout).repeat(3),fadein));
	},
	action2:function(){
		var fadein=cc.fadeIn(0.5);
		var fadeout=cc.fadeOut(0.5);
		var up=this.getChildByTag(TAG_BOTANY1).getChildByTag(TAG_UP);
		up.runAction(cc.sequence(cc.sequence(fadein,fadeout).repeat(3),fadein));
	},
	action3:function(){
		var fadein=cc.fadeIn(0.5);
		var fadeout=cc.fadeOut(0.5);
		var down=this.getChildByTag(TAG_BOTANY1).getChildByTag(TAG_DOWN);
		down.runAction(cc.sequence(cc.sequence(fadein,fadeout).repeat(3),fadein));
	},
	callback:function(p){
		var func=cc.callFunc(function() {
			gg.flow.next();
		}, this);
		switch (p.getTag()) {
		case TAG_CO2:
			p.runAction(cc.sequence(cc.callFunc(function() {
				p.setEnable(false);
				this.action();
				ll.tip.arr.out();
			},this),cc.delayTime(4),cc.callFunc(function() {
				this.h2o.setEnable(true);
			}, this),func));
			ll.tip.mdScore(10);
			break;
		case TAG_H2O:
			p.runAction(cc.sequence(cc.callFunc(function() {
				p.setEnable(false);
				this.action2();
				ll.tip.arr.out();
			},this),cc.delayTime(4),cc.callFunc(function() {
				this.action1();
				this.action3();
				this.getChildByTag(TAG_BOTANY1).getChildByTag(TAG_O2).setVisible(true);
				this.getChildByTag(TAG_TITLE).setVisible(true);
			},this),cc.delayTime(4),func));
			ll.tip.mdScore(10);
			break;
		default:
			break;
		}
	}
});