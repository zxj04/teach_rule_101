exp09.ShowMainLayer04 = cc.Layer.extend({
	lead:null,
	real:null,
	reset:true,
	reset1:true,
	ctor:function () {
		this._super();
		this.init();
		return true;
	},
	init:function(){
		var sv = new pub.ScrollView(this);
		sv.setOffestPos(cc.p(152,45 + 15));

		this.addlayout1(sv);
		this.addlayout2(sv);
	},	
	addlayout1:function(parent){
		this.layout1 = new ccui.Layout();//第一页
		this.layout1.setContentSize(cc.size(996, 598));//设置layout的大小
		parent.addLayer(this.layout1);

		var text = $.format("　　下图是二氧化碳和氧气的生产和消耗途径。" +
				"\n光合作用必须有光才能进行，呼吸作用不管白天、黑夜都在生物体内进行。", 960, 30);
		label1 = new cc.LabelTTF(text,gg.fontName,gg.fontSize4);
		label1.setAnchorPoint(0,0.5);
		label1.setPosition(0,this.layout1.height-label1.height/2);
		label1.setColor(cc.color(0, 0, 0, 250));
		this.layout1.addChild(label1);	
		
		this.addImage(this.layout1,  "#guanghe.png", cc.p(500,250), 1);	
		
		var arrow = new cc.Sprite("#arrow/1.png");
		arrow.setPosition(cc.p(500,250));
		this.layout1.addChild(arrow,10);
		arrow.runAction(this.addAction("arrow/", 2, 0.5));
	
	},
	addlayout2:function(parent){
		this.layout2 = new ccui.Layout();//第一页
		this.layout2.setContentSize(cc.size(996, 598));//设置layout的大小
		parent.addLayer(this.layout2);

		var text = $.format("　　光合作用和呼吸作用是相互依存的。呼吸作用消耗的有机物是光合作用的产物，所放出的能量是光合作用" +
				"储存在有机物中的能量。没有光合作用，呼吸作用就没有基础。光合作用对原料的我吸收利用和对产物的输导，" +
				"所需要的能量又是呼吸作用所释放出来的。所以，没有呼吸作用，光合作用也无法进行。", 960, 30);
		label1 = new cc.LabelTTF(text,gg.fontName,gg.fontSize4);
		label1.setAnchorPoint(0,0.5);
		label1.setPosition(0,this.layout2.height-label1.height/2);
		label1.setColor(cc.color(0, 0, 0, 250));
		this.layout2.addChild(label1,5);	

		this.addImage(this.layout2,  "#biaoge.png", cc.p(500,230), 1);	
	},
	
	addAction:function(str1,num,time){
		var frames = [];
		for(var i =1 ;i<=num;i++){
			var str = str1+i+".png";
			var frame=cc.spriteFrameCache.getSpriteFrame(str);
			frames.push(frame);		
		}
		var animation = new cc.Animation(frames,time);//负责动画序列
		var action = new cc.Animate(animation);//帧动画的动作创建
		return action.repeatForever();
	},
	
	addshowimage:function(parent,str,pos,str2){
		var showimage = new cc.Sprite(str);
		showimage.setPosition(pos);
		parent.addChild(showimage,5);
		var label = new cc.LabelTTF(str2,gg.fontName,gg.fontSize);
		label.setColor(cc.color(0, 0, 0, 250));
		parent.addChild(label);
		label.setPosition(showimage.x,showimage.y - showimage.height * 0.5 -20);
	},
	addImage:function(parent,str,pos,scale){
		var image = new cc.Sprite(str);
		parent.addChild(image,5);
		image.setPosition(pos);
		if(scale !=null){
			image.setScale(scale);
		}else{
			image.setScale(1);
		}
	},
	addbutton:function(parent,tag,str,pos){
		var button = new ButtonScale(parent,"#unsel.png",this.callback,this);
		button.setPosition(pos);
		button.setLocalZOrder(5);
		button.setTag(tag);
		var label = new cc.LabelTTF(str,gg.fontName,30);
		label.setPosition(button.width/2,button.height/2);		
		label.setColor(cc.color(0, 0, 0, 250));
		button.addChild(label);
		label.setTag(tag);
	},
	callback:function(p){
		switch(p.getTag()){	
		case TAG_BUTTON1:
			this.reset = !this.reset;
			if(!this.reset){
		
			}else{
				
			}
			break;
		case TAG_BUTTON2:			
			this.reset1 = !this.reset1;
			if(!this.reset1){
				
			}else{
				
			}
			break;
		}
	}	
});
