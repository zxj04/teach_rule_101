exp09.RunLayer02 = cc.Layer.extend({
	backgroundLayer : null,
	mainLayar : null,
	ctor:function () {
		this._super();
		this.initFrames();
		this.loadBackground();
		this.loadMainLayer();
	},
	initFrames : function(){
		cc.spriteFrameCache.addSpriteFrames(exp09.res_run.run_p2);
		gg.curRunSpriteFrame.push(exp09.res_run.run_p2);
	},
	loadBackground : function(){
		this.backgroundLayer = new RunBackgroundLayer();
		this.addChild(this.backgroundLayer);
	},
	loadMainLayer : function(){
		this.mainLayar = new exp09.RunMainLayer02();
		this.addChild(this.mainLayar);
	}
});

exp09.RunScene02 = PScene.extend({
	onEnter:function () {
		this._super();
		gg.initTeach();
		var layer = new exp09.RunLayer02();
		this.addChild(layer);
	},
	
});
