exp09.Nail = cc.Node.extend({
	ctor:function(p){
		this._super();
		p.addChild(this, 10, TAG_NAIL);
		this.init();
	},
	init : function(){
		this.setCascadeOpacityEnabled(true);

		this.nail=new Button(this,10,TAG_NAIL1,"#nail.png",this.callback);
	},
	callback:function(p){
		var func=cc.callFunc(function() {
			gg.flow.next();
		}, this);
		var action=gg.flow.flow.action;
		switch (p.getTag()) {
		case TAG_NAIL1:
			var ber=cc.bezierBy(1, [cc.p(-100,35),cc.p(-450,55),cc.p(-440, 25)]);
			var move=cc.moveBy(0.5,cc.p(0, -5));
			if(action==ACTION_DO1){
				p.runAction(cc.sequence(ber,cc.spawn(move,cc.callFunc(function() {
					p.setSpriteFrame("nail1.png");
				}, this)),func));
			}else if(action==ACTION_DO2){
				p.runAction(cc.sequence(cc.spawn(move.reverse(),cc.callFunc(function() {
					p.setSpriteFrame("nail.png");
				}, this)),ber.reverse(),cc.callFunc(function() {
					p.removeFromParent(true);
				}, this),func));
			}
			break;
		default:
			break;
		}
	}
});