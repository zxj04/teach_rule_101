exp09.Leaf = cc.Node.extend({
	ctor:function(p){
		this._super();
		p.addChild(this, 9, TAG_LEAFS);
		this.init();
	},
	init : function(){
		this.setCascadeOpacityEnabled(true);
		this.leaf=new Button(this,10,TAG_LEAFS1,"#leaf.png",this.callback,this);
	},
	callback:function(p){
		var func=cc.callFunc(function() {
			gg.flow.next();
		}, this);
		var action=gg.flow.flow.action;
		switch (p.getTag()) {
		case TAG_LEAFS1:
			var ber = cc.bezierBy(1, [cc.p(0, 80),cc.p(80,295),cc.p(194,300)]);
			var scale=cc.scaleBy(1,0.6);
			p.runAction(cc.sequence(cc.spawn(ber,scale),cc.callFunc(function(){
				p.setVisible(false);
				ll.run.tripod.moveLeaf();
			}, this),cc.delayTime(1),cc.callFunc(function() {
				ll.run.lamp.setVisible(true);
				ll.run.match.setVisible(true);
			}, this),func));
			break;
		default:
			break;
		}
	}
});