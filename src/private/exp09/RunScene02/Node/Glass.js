exp09.Glass = cc.Node.extend({
	ctor:function(p){
		this._super();
		p.addChild(this, 10, TAG_GLASS);
		this.init();
	},
	init : function(){
		this.setCascadeOpacityEnabled(true);

		this.glass=new Button(this,10,TAG_GLASS1,"#glass.png",this.callback);
		
		this.leaf=new Button(this.glass,10,TAG_LEAF2,"#leaf4.png",this.callback);
		this.leaf.setPosition(this.glass.width*0.5,this.glass.height*0.5);
		this.leaf.setVisible(false);
		
		this.leaf1=new Button(this.glass,10,TAG_LEAF3,"#leaf5.png",this.callback);
		this.leaf1.setPosition(this.glass.width*0.5,this.glass.height*0.5);
		this.leaf1.setOpacity(0);
	},
	fadeinLeaf:function(){
		this.leaf1.runAction(cc.fadeIn(3));
	},
	changeLeaf:function(){
//		this.leaf.setSpriteFrame("leaf5.png");
		this.leaf.runAction(cc.spawn(cc.fadeOut(3),cc.callFunc(function() {
			this.fadeinLeaf();
		}, this)));
	},
	callback:function(p){
		var func=cc.callFunc(function() {
			gg.flow.next();
		}, this);
		var action=gg.flow.flow.action;
		switch (p.getTag()) {
		default:
			break;
		}
	}
});