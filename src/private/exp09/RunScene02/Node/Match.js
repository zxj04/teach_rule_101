exp09.Match = cc.Node.extend({
	ctor:function(p){
		this._super();
		p.addChild(this, 10, TAG_MATCH);
		this.init();
	},
	init : function(){
		this.setCascadeOpacityEnabled(true);

		this.match=new Button(this,10,TAG_MATCH1,"#libmatch.png",this.callback);
		this.match.setScale(0.4);
		
		this.match1=new Button(this,10,TAG_MATCH2,"#match1.png",this.callback);
		this.match1.setVisible(false);
		this.match1.setScale(0.4);
		this.match1.setPosition(cc.p(28,75));
		
	},
	moveMatch:function(){
		var move=cc.moveBy(1,cc.p(80,-43));
		var ber=cc.bezierBy(1, [cc.p(-50, 25),cc.p(-130, 45),cc.p(-275,42)]);
		this.match1.runAction(cc.sequence(move,cc.callFunc(function() {
			this.match1.setSpriteFrame("match2.png");
		},this),ber));
	},
	callback:function(p){
		var func=cc.callFunc(function() {
			gg.flow.next();
		}, this);
		var action=gg.flow.flow.action;
		switch (p.getTag()) {
		case TAG_MATCH1:
			p.runAction(cc.sequence(cc.callFunc(function() {
				this.match1.setVisible(true);
				this.moveMatch();
			}, this),cc.delayTime(3),cc.callFunc(function() {
				this.match1.removeFromParent(true);
				ll.run.lamp.initfire();
			}, this),cc.callFunc(function() {
				this.setOpacity(0);
				//this.removeFromParent(true);
			}, this),func));
			break;
		default:
			break;
		}
	}
});