exp09.Beaker = cc.Node.extend({
	ctor:function(p){
		this._super();
		p.addChild(this, 10, TAG_BEAKERS);
		this.init();
	},
	init : function(){
		this.setCascadeOpacityEnabled(true);

		this.beaker=new Button(this,10,TAG_BEAKER2,"#beaker.png",this.callback);
		
		this.line=new Button(this.beaker,10,TAG_LINE1,"#line.png",this.callback);
		this.line.setPosition(cc.p(this.beaker.width*0.48,this.beaker.height*0.65));
	},
	callback:function(p){
		var func=cc.callFunc(function() {
			gg.flow.next();
		}, this);
		var action=gg.flow.flow.action;
		switch (p.getTag()) {
		default:
			break;
		}
	}
});