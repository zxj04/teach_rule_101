exp09.Lamp = cc.Node.extend({
	runningAction:null,
	ctor:function(p){
		this._super();
		p.addChild(this, 10, TAG_LAMP);
		this.init();
		this.initAction();
	},
	init : function(){
		this.setCascadeOpacityEnabled(true);

		this.lamp=new Button(this,10,TAG_LAMP1,"#lampBottle.png",this.callback);
		this.lamp.setScale(0.4);
		
		this.lid=new Button(this,10,TAG_LID,"#lampLid.png",this.callback,this);
		this.lid.setScale(0.4);
		this.lid.setPosition(cc.p(this.width*0.5,35));
		
		this.fire=new Button(this.lamp,10,TAG_FIRE,"#fire/1.png",this.callback);
		this.fire.setVisible(false);
		this.fire.setScale(2.5);
		this.fire.setPosition(cc.p(this.lamp.width*0.5,350 ));
	},
	initAction:function(){
		var animFrames=[];
		for(var i=1;i<4;i++){
			var str="fire/"+i+".png";
			var frame=cc.spriteFrameCache.getSpriteFrame(str);
			animFrames.push(frame);
		}
		var animation=new cc.Animation(animFrames,0.05);
		this.runningAction=cc.animate(animation);
		this.runningAction.retain();
	},
	closeLid:function(){
		var ber=cc.bezierBy(1, [cc.p(5,80),cc.p(50,100),cc.p(100,-65)]);
		var ber1=cc.bezierBy(1, [cc.p(5,40),cc.p(5,135),cc.p(0,0)]);
		this.lid.runAction(cc.sequence(cc.spawn(ber.reverse(),cc.sequence(cc.delayTime(0.8),cc.callFunc(function() {
			this.fire.removeFromParent(true);
		}, this))),ber1));
	},
	initfire:function(){
		this.fire.setVisible(true);
		this.fire.runAction(this.runningAction.repeatForever());
	},
	callback:function(p){
		var func=cc.callFunc(function() {
			gg.flow.next();
		}, this);
		var action=gg.flow.flow.action;
		switch (p.getTag()) {
		case TAG_LID:
			var ber=cc.bezierBy(1, [cc.p(5,80),cc.p(50,100),cc.p(100,-65)]);
			p.runAction(cc.sequence(ber,func));
			break;
		case TAG_LAMP1:
			var move=cc.moveBy(1,cc.p(-290,-35));
			if(action==ACTION_DO1){
				p.runAction(cc.sequence(move,cc.spawn(cc.callFunc(function() {
					ll.run.tripod.fadeinBeaker();
				}, this),cc.sequence(cc.callFunc(function() {
					ll.run.clock.doing();
					this.showTip=new ShowTip("用酒精灯加热一段时间",cc.p(650,665));
				}, this),cc.delayTime(4),cc.callFunc(function() {
					ll.run.clock.stop();
					ll.run.tripod.changeLeaf();
				}, this))),func));
			}else if(action==ACTION_DO2){
				p.runAction(cc.sequence(move.reverse(),cc.callFunc(function() {
					this.closeLid();
				}, this),cc.delayTime(3),cc.callFunc(function() {
					this.setVisible(false);
					ll.run.tweezer.setVisible(true);
					ll.run.glass.setVisible(true);
				}, this),func));
			}
			
			break;
		default:
			break;
		}
	}
});