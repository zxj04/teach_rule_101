exp09.Botany02 = cc.Node.extend({
	ctor:function(p){
		this._super();
		p.addChild(this, 10, TAG_BOTANY);
		this.init();
	},
	init : function(){
		this.setCascadeOpacityEnabled(true);

		this.botany=new Button(this,10,TAG_BOTANY1,"#botany1.png",this.callback,this);
		
		this.leaf1=new Button(this.botany,10,TAG_LEAF,"#leaf.png",this.callback,this);
		this.leaf1.setPosition(this.botany.width*0.15,this.botany.height*0.58);
		
		this.foil=new Button(this,9,TAG_FOIL2,"#foil.png",this.callback,this);
		this.foil.setPosition(250,0);
		
	},
	moveFoil:function(){
		var ber=cc.bezierBy(1,[cc.p(-150, 20),cc.p(-300, 35),cc.p(-365,30)]);
		var foil=this.getChildByTag(TAG_FOIL2);
		foil.runAction(ber.reverse());
	},
	callback:function(p){
		var func=cc.callFunc(function() {
			gg.flow.next();
		}, this);
		var action=gg.flow.flow.action;
		switch (p.getTag()) {
		case TAG_BOTANY1:
			if(action==ACTION_DO1){
				p.runAction(cc.sequence(cc.callFunc(function() {
					ll.run.clock.doing();
					this.showTip=new ShowTip("放在黑暗处一昼夜",cc.p(650,665));
				},this),cc.delayTime(2),cc.callFunc(function() {
					ll.run.clock.stop();
					this.showTip.kill();
				},this),func));
			}else if(action==ACTION_DO2){
				p.runAction(cc.sequence(cc.callFunc(function() {
					ll.run.clock.doing();
					this.showTip=new ShowTip("放在阳光下直射一段时间",cc.p(650,665));
				},this),cc.delayTime(2),cc.callFunc(function() {
					ll.run.clock.stop();
					this.showTip.kill();
				},this),func));
			}
			break;
		case TAG_FOIL2:
			var ber=cc.bezierBy(1,[cc.p(-150, 20),cc.p(-300, 35),cc.p(-365,30)]);
			p.runAction(cc.sequence(ber,func));
			break;
		case TAG_LEAF:
			var ber=cc.bezierBy(1, [cc.p(-50,50),cc.p(-60, 120),cc.p(-80,-180)]);
				p.runAction(cc.sequence(ber,cc.callFunc(function() {
					cc.log(this.convertToWorldSpace(p.getPosition()));
					this.setOpacity(0);
					p.setVisible(false);
					ll.run.leaf.setVisible(true);
					//this.removeFromParent(true);
					ll.run.tripod.setVisible(true);
				}, this),func));
			break;
		default:
			break;
		}
	}
});