exp09.Foil = cc.Node.extend({
	ctor:function(p){
		this._super();
		p.addChild(this, 10, TAG_FOIL);
		this.init();
	},
	init : function(){
		this.setCascadeOpacityEnabled(true);

		this.foil=new Button(this,10,TAG_FOIL1,"#foil.png",this.callback);
	},
	callback:function(p){
		var func=cc.callFunc(function() {
			gg.flow.next();
		}, this);
		var action=gg.flow.flow.action;
		switch (p.getTag()) {
		case TAG_FOIL1:
			var ber=cc.bezierBy(1,[cc.p(-150, 20),cc.p(-300, 35),cc.p(-290,30)]);
			if(action==ACTION_DO1){
				p.runAction(cc.sequence(ber,func));
			}else if(action==ACTION_DO2){
				p.runAction(cc.sequence(cc.spawn(ber.reverse(),cc.callFunc(function() {
					ll.run.botany.moveFoil();
				}, this)),cc.callFunc(function() {
					p.removeFromParent(true);
					if(ll.run.botany.getChildByTag(TAG_FOIL2)!=null){
						ll.run.botany.getChildByTag(TAG_FOIL2).removeFromParent(true);
					}
				},this),func));
			}
			break;
		default:
			break;
		}
	}
});