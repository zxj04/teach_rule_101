exp09.Bottle = cc.Node.extend({
	ctor:function(p){
		this._super();
		p.addChild(this, 10, TAG_BOTTLE);
		this.init();
	},
	init : function(){
		this.setCascadeOpacityEnabled(true);

		this.bottle=new Button(this,10,TAG_BOTTLE1,"#bottle.png",this.callback);
		
		this.dropper=new Button(this,9,TAG_DROP,"#dropper.png",this.callback);
		this.dropper.setPosition(this.width*0.5,80);
		
		this.line=new Button(this.dropper,10,TAG_LINE1,"#line1.png",this.callback);
		this.line.setPosition(this.dropper.width*0.5,this.dropper.height*0.5);
	},
	addWater:function(){
		var water=new Button(this.dropper,10,TAG_WATER,"#water.png",this.callback);
		water.setPosition(cc.p(this.dropper.width*0.5,this.dropper.height*0.05));
		var move=cc.moveBy(0.5,cc.p(0, -80));
		water.runAction(cc.sequence(move,cc.callFunc(function() {
			water.removeFromParent(true);
			this.addWater();
		},this)));

	},
	callback:function(p){
		var func=cc.callFunc(function() {
			gg.flow.next();
		}, this);
		var action=gg.flow.flow.action;
		switch (p.getTag()) {
			case TAG_DROP:
				var ber =cc.bezierBy(1, [cc.p(0,180),cc.p(-150,200),cc.p(-300,120)]);
				p.runAction(cc.sequence(ber,cc.callFunc(function() {
					this.addWater();
				}, this),cc.spawn(cc.delayTime(3),cc.sequence(cc.delayTime(2.8),cc.callFunc(function() {
					ll.run.glass.changeLeaf();
					this.showTip=new ShowTip("植物中的淀粉遇碘变蓝",cc.p(350,650),true);
				}, this))),cc.callFunc(function() {
					if(this.dropper.getChildByTag(TAG_WATER)!=null){
						this.dropper.getChildByTag(TAG_WATER).removeFromParent(true);
					}
				}, this),ber.reverse(),cc.delayTime(4),cc.callFunc(function() {
					this.setOpacity(0);
					//this.removeFromParent(true);
				}, this),func));
			break;
		default:
			break;
		}
	}
});