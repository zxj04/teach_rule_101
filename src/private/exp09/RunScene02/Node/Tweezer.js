exp09.Tweezer = cc.Node.extend({
	ctor:function(p){
		this._super();
		p.addChild(this, 10, TAG_TWEEZER);
		this.init();
	},
	init : function(){
		this.setCascadeOpacityEnabled(true);

		this.tweezer=new Button(this,10,TAG_TWEEZER1,"#Tweezers.png",this.callback,this);
		
		this.tweezer1=new Button(this.tweezer,10,TAG_TWEEZER2,"#Tweezers1.png",this.callback,this);
		this.tweezer1.setPosition(this.tweezer.width*0.5,this.tweezer.height*0.5);
		
		this.leaf=new Button(this.tweezer,10,TAG_LEAF1,"#leaf2.png",this.callback,this);
		this.leaf.setPosition(this.tweezer.width*0.15,this.tweezer.height*0.01);
		this.leaf.setVisible(false);
	},
	scaleLeaf:function(){
		this.leaf.runAction(cc.scaleBy(1,3));
	},
	callback:function(p){
		var func=cc.callFunc(function() {
			gg.flow.next();
		}, this);
		var action=gg.flow.flow.action;
		switch (p.getTag()) {
		case TAG_TWEEZER1:
			var ber=cc.bezierBy(1, [cc.p(-80,180),cc.p(-283,580),cc.p(-283,180)]);
			var ber1=cc.bezierBy(1, [cc.p(-10,180),cc.p(-50,350),cc.p(-260,-135)]);
			p.runAction(cc.sequence(ber,cc.callFunc(function() {
				ll.run.tripod.leaf.removeFromParent(true);
				this.leaf.setVisible(true);
			}, this),cc.spawn(ber1,cc.callFunc(function() {
				this.scaleLeaf();
			}, this)),cc.callFunc(function() {
				ll.run.tripod.removeFromParent(true);
				p.removeFromParent(true);
				ll.run.glass.leaf.setVisible(true);
				ll.run.bottle.setVisible(true);
			}, this),func));
			break;
		default:
			break;
		}
	}
});