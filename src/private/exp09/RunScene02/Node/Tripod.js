exp09.Tripod = cc.Node.extend({
	ctor:function(p){
		this._super();
		p.addChild(this, 10, TAG_TRIPOD);
		this.init();
	},
	init : function(){
		this.setCascadeOpacityEnabled(true);

		this.tripod=new Button(this,10,TAG_TRIPOD1,"#Tripod.png",this.callback,this);
		
		this.net=new Button(this.tripod,10,TAG_NET,"#net.png",this.callback,this);
		this.net.setPosition(cc.p(this.tripod.width*0.5, this.tripod.height*0.88));
		
		this.beaker=new Button(this.tripod,11,TAG_BEAKER,"#beaker.png",this.callback,this);
		this.beaker.setPosition(cc.p(this.tripod.width*0.5,this.tripod.height*1.2));
		
		this.beaker1=new Button(this.beaker,10,TAG_BEAKER1,"#beaker1.png",this.callback,this);
		this.beaker1.setPosition(cc.p(this.beaker.width*0.5, this.beaker.height*0.35));
		
		this.beaker2=new Button(this.beaker,10,TAG_BEAKER1,"#beaker2.png",this.callback,this);
		this.beaker2.setPosition(cc.p(this.beaker.width*0.5, this.beaker.height*0.35));
		this.beaker2.setOpacity(0);
		
		this.line=new Button(this.beaker,10,TAG_LINE1,"#line.png",this.callback,this);
		this.line.setPosition(this.beaker.width*0.46,this.beaker.height*0.45);
		
		this.leaf=new Button(this.beaker,10,TAG_LEAF1,"#leaf1.png",this.callback,this);
		this.leaf.setPosition(this.beaker.width*0.46,this.beaker.height*1.26);
		this.leaf.setVisible(false);
	},
	changeLeaf:function(){
		this.leaf.setSpriteFrame("leaf2.png");
	},
	fadeinBeaker:function(){
		var fadein=cc.fadeIn(4);
		this.beaker2.runAction(fadein);
	},
	moveLeaf:function(){
		var move=cc.moveBy(1,cc.p(0, -170));
		this.leaf.runAction(cc.sequence(cc.callFunc(function() {
			this.leaf.setVisible(true);
		}, this),move));
	},
	callback:function(p){
		var func=cc.callFunc(function() {
			gg.flow.next();
		}, this);
		switch (p.getTag()) {
		default:
			break;
		}
	}
});