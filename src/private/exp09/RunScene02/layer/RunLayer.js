exp09.RunLayer2 = cc.Layer.extend({
	arr:null,
	scene:null,
	clock:null,
	ctor:function (parent) {
		this._super();
		this.scene = parent;
		this.scene.addChild(this, 10);
		gg.main = this;
		this.init();
	},
	init:function () {
		this.callNext = cc.callFunc(function(){
			gg.flow.next();
		}, this);
		this.callKill = cc.callFunc(function(p){
			var seq = cc.sequence(cc.fadeOut(0.5),cc.callFunc(function(){
				p.removeFromParent(true);	
			}, this));
			p.runAction(seq);
		}, this); 

		this.callNext.retain();
		this.callKill.retain();
		//时钟
		this.clock = new Clock(this);
		// 物品库
		this.lib = new Lib(this);

		this.lib.loadBg([{
			tag:TAG_LIB_BOTANY,
			checkright:true,	//实验所需器材添加checkright标签		
		},
		{
			tag:TAG_LIB_LAMP,
			checkright:true,
		},
		{
			tag:TAG_LIB_TRIPOD,
			checkright:true,
		},
		{
			tag:TAG_LIB_AMPULLA,
		},
		{
			tag:TAG_LIB_DROPPER,
			checkright:true,
		},
		{
			tag:TAG_LIB_SANJIAOFLASK,
		},
		{
			tag:TAG_LIB_GLASS,
			checkright:true,
		},
		{
			tag:TAG_LIB_BEAKER,
			checkright:true,
		},
		{
			tag:TAG_LIB_TWEEZER,
			checkright:true,
		},
		{
			tag:TAG_LIB_CONICAL,
		}
		]);

		this.botany=new exp09.Botany02(this);
		this.botany.setVisible(false);
		this.botany.setPosition(650,350);//402  147

		this.foil=new exp09.Foil(this);
		this.foil.setVisible(false);
		this.foil.setPosition(820,350);//170

		this.nail=new exp09.Nail(this);
		this.nail.setVisible(false);
		this.nail.setPosition(970,350);//320

		this.tripod=new exp09.Tripod(this);
		this.tripod.setVisible(false);
		this.tripod.setPosition(650,230);

		this.lamp=new exp09.Lamp(this);
		this.lamp.setVisible(false);
		this.lamp.setPosition(950,230);

		this.match=new exp09.Match(this);
		this.match.setVisible(false);
		this.match.setPosition(1150,230);

		this.glass=new exp09.Glass(this);
		this.glass.setVisible(false);
		this.glass.setPosition(350,200);

		this.tweezer=new exp09.Tweezer(this);
		this.tweezer.setVisible(false);
		this.tweezer.setPosition(950,230);

		this.bottle=new exp09.Bottle(this);
		this.bottle.setVisible(false);
		this.bottle.setPosition(650,230);

		this.leaf=new exp09.Leaf(this);
		this.leaf.setVisible(false);
		this.leaf.setPosition(450,204);

		this.beaker=new exp09.Beaker(this);
		this.beaker.setVisible(false);
		this.beaker.setPosition(1150,230);

	},
	checkVisible:function(next){
		//是否可见
		var checkVisible = [];
		var node1 = ll.run.botany;
		var node2 = ll.run.foil;
		var node3 = ll.run.nail;
		var node4 = ll.run.tripod;
		var node5 = ll.run.lamp;
		var node6 = ll.run.match;
		var node7 = ll.run.glass;
		var node8 = ll.run.tweezer;
		var node9 = ll.run.bottle;

		checkVisible.push(node1,node2,node3
//				,node4,node5,node6,node7,node8,node9
		);
		for(var i in checkVisible){
			if(checkVisible[i] !== null){				
				checkVisible[i].setVisible(next);				
			}			
		}
	},
	loadInLib:function(obj, pos, tarPos,delay){
		obj.setPosition(pos);
		if(delay == null){
			delay = 1;
		}
		var ber = $.bezier(pos, tarPos, delay);
		var seq = cc.sequence(ber, this.callNext);
		obj.runAction(seq);
	},
	kill:function(obj){
		var fade = cc.fadeTo(0.5,0);
		var func = cc.callfunc(function(){
			obj.removeFromParent(true);
		},this);
		var seq = cc.sequence(fade,func);
		obj.runAction(seq)
	},

	callback:function (p){
		var func = cc.callFunc(this.actionDone, this);
		var action=gg.flow.flow.action;
		switch(p.getTag()){

		}
	},
	actionDone:function(p){
		var func = cc.callFunc(this.actionDone, this);
		switch(p.getTag()){

		}
	},
	flowNext:function(){
		gg.flow.next();
	},
	onExit:function(){
		this._super();
		this.callNext.release();
		this.callKill.release();
	}
});