exp08.RunMainLayer01 = cc.Layer.extend({
	lead:null,
	real:null,
	ctor:function () {
		this._super();
		ll.run = null;
		ll.main = this;
		this.loadTip();		
		//this.loadTool();
		this.loadFlow();
		return true;
	},	
	loadTip:function(){
		ll.tip = new TipLayer(this);
	},
	loadTool:function(){
		ll.tool = new ToolLayer(this);
	},
	loadRun:function(start){	
		if(start){
			gg.flow.start();
			ll.run.removeFromParent(true);
			ll.run = null;
		}
		if(ll.run != null){
			ll.run.destroy();
		}	
		var no = gg.flow.flow.nodeNo;
		switch(no){
		case 1:
			ll.run = new exp08.RunLayer1(this);
			break;

		}
	},
	loadFlow:function(){
		gg.flow.setMain(this);
		gg.flow.start();
	},
	over: function (){
		ll.tip.over();
		ll.tip.back_frame.open();
		// 提交成绩
		net.saveScore();
	},
});
