exp08.ShowMainLayer03 = cc.Layer.extend({
	lead:null,
	real:null,
	curIndex: 0,
	reset:true,
	actionnum:1,
	ctor:function () {
		this._super();
		this.init();
		return true;
	},
	init:function(){
		var sv = new pub.ScrollView(this);
		sv.setOffestPos(cc.p(152,45 + 15));

		this.addlayout1(sv);

	},	
	addlayout1:function(parent){
		this.layout1 = new ccui.Layout();//第一页
		this.layout1.setContentSize(cc.size(996, 598));//设置layout的大小
		parent.addLayer(this.layout1);

		var text = $.format("　　动物体内也在进行呼吸作用?动物呼出和吸入的气体成分有没有变化呢" +
				"　　碱石灰可以吸收二氧化碳", 960, 30);
		
		label1 = new cc.LabelTTF(text,gg.fontName,gg.fontSize4);
		label1.setAnchorPoint(0,0.5);
		label1.setPosition(0,550);
		label1.setColor(cc.color(255, 255, 255, 250));
		this.layout1.addChild(label1,10);	
		
		var bg = new cc.Sprite(res_public.run_back);
		this.layout1.addChild(bg);
		bg.setScale(0.8);
		bg.setPosition(cc.p(this.layout1.width /2, this.layout1.height / 2 +30));
		
		this.addImage(this.layout1, 5, "#1.png", cc.p(500,450), 1);
		
		this.red = new cc.Sprite("#2.png");
		this.red.setPosition(cc.p(660,450));
		this.layout1.addChild(this.red,6);
		
		this.kunchong = new cc.Sprite("#dongwu/1.png");
		this.kunchong.setPosition(cc.p(320,450));
		this.layout1.addChild(this.kunchong,6);
		
		this.addImage(this.layout1, 5, "#1.png", cc.p(500,200), 1);

		this.red1 = new cc.Sprite("#2.png");
		this.red1.setPosition(cc.p(660,200));
		this.layout1.addChild(this.red1,6);
		
		this.addshow("碱石灰",cc.p(435,324),1.6);
		this.addshow("红水滴",cc.p(660,324),2);

		this.addbutton(this.layout1, TAG_BUTTON1, "播放", cc.p(800,320));
		this.layout1.getChildByTag(TAG_BUTTON1).getChildByTag(TAG_BUTTON1).setColor(cc.color(255, 255, 255, 250));
		
		this.label1 = new cc.LabelTTF("现象：A试管中，红色液体向左移了一段距离；" +
				"\n　　　而B试管中，红色液体没有移动。" +
				"\n结论：动物也需要呼吸，呼吸时消耗了氧气，产生了二氧化碳。",gg.fontName,gg.fontSize4);
		this.label1.setAnchorPoint(0,0.5);
		this.label1.setPosition(80,100);
		this.label1.setColor(cc.color(255, 0, 0, 250));
		this.layout1.addChild(this.label1,10);
		this.label1.setVisible(false);		
	},
	addAction:function(str1,num,time){
		var frames = [];
		for(var i =1 ;i<=num;i++){
			var str = str1+i+".png";
			var frame=cc.spriteFrameCache.getSpriteFrame(str);
			frames.push(frame);		
		}
		var animation = new cc.Animation(frames,time);//负责动画序列
		var action = new cc.Animate(animation);//帧动画的动作创建
		return action.repeatForever();
	},
	addImage:function(parent,index,str,pos,scale){
		var image = new cc.Sprite(str);
		parent.addChild(image,5);
		image.setPosition(pos);
		if(scale !=null){
			image.setScale(scale);
		}else{
			image.setScale(1);
		}
		image.setLocalZOrder(index);
	},
	addshow:function(str,pos,scalex){
		var label = new cc.LabelTTF(str,gg.fontName,30);
		this.layout1.addChild(label,5);
		label.setPosition(pos);
		label.setColor(cc.color(255,255,255,250));		
		var line = new cc.Sprite("#line.png");
		line.setAnchorPoint(0,0.5);
		line.setRotation(-90);
		line.setScale(scalex,4);
		line.setPosition(label.width/2 ,label.height);
		label.addChild(line);
		
		var line1 = new cc.Sprite("#line.png");
		line1.setAnchorPoint(0,0.5);
		line1.setRotation(90);
		line1.setScale(scalex,4);
		line1.setPosition(label.width/2 ,0);
		label.addChild(line1);
	},
	addbutton:function(parent,tag,str,pos){
		var button = new ButtonScale(parent,"#unsel.png",this.callback,this);
		button.setPosition(pos);
		button.setLocalZOrder(5);
		button.setTag(tag);
		var label = new cc.LabelTTF(str,gg.fontName,30);
		label.setPosition(button.width/2,button.height/2);		
		label.setColor(cc.color(0, 0, 0, 250));
		button.addChild(label);
		label.setTag(tag);
	},
	callback:function(p){
		switch(p.getTag()){	
		case TAG_BUTTON1:
			this.reset = !this.reset;
			if(!this.reset){
				p.setEnable(false);
				this.kunchong.runAction(this.addAction("dongwu/", 3, 0.5));
				var move = cc.moveTo(10,cc.p(600,450));
				var seq = cc.sequence(move,cc.callFunc(function(){
					p.setEnable(true);
					this.layout1.getChildByTag(TAG_BUTTON1).getChildByTag(TAG_BUTTON1).setString("复原");
					this.label1.setVisible(true);
				},this));
				this.red.runAction(seq);
			}else{
				this.red.setPosition(cc.p(660,450));
				this.kunchong.stopAllActions();
				this.layout1.getChildByTag(TAG_BUTTON1).getChildByTag(TAG_BUTTON1).setString("播放");
			}
			break;
		
		    	
		}
	}	
});
