exp08.RunLayer02 = cc.Layer.extend({
	backgroundLayer : null,
	mainLayar : null,
	ctor:function () {
		this._super();
		this.initFrames();
		this.loadBackground();
		this.loadMainLayer();
	},
	initFrames : function(){
		cc.spriteFrameCache.addSpriteFrames(exp08.res_run.run_p);
		gg.curRunSpriteFrame.push(exp08.res_run.run_p);
	},
	loadBackground : function(){
		this.backgroundLayer = new RunBackgroundLayer();
		this.addChild(this.backgroundLayer);
	},
	loadMainLayer : function(){
		this.mainLayar = new exp08.RunMainLayer02();
		this.addChild(this.mainLayar);
	}
});

exp08.RunScene02 = PScene.extend({
	onEnter:function () {
		this._super();
		gg.initTeach();
		var layer = new exp08.RunLayer02();
		this.addChild(layer);
	},
	
});
