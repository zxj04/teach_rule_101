exp08.Gemo = cc.Node.extend({
	ctor:function(p){
		this._super();
		p.addChild(this, 10, TAG_GEMO_NODE);
		this.init();
	},
	init : function(){
		this.setCascadeOpacityEnabled(true);

		this.gemo= new Button(this, 10, TAG_GEMO, "#gemo.png",this.callback);
		this.gemo.setScale(0.5);
		this.gemo.setPosition(0,0);

		this.xiangpimo= new Button(this, 9, TAG_XIANGPIMO, "#xiangpimo.png",this.callback);
		this.xiangpimo.setScale(0.5);
		this.xiangpimo.setPosition(350,0);
		
		this.qiqiu= new Button(this,8, TAG_QIQIU, "#qiqiu.png",this.callback);
		this.qiqiu.setScale(0.5);
		this.qiqiu.setPosition(350,50);
		
		this.airin = new Button(this,8, TAG_AIRIN, "#unsel.png",this.callback);
		this.airin.setScale(0.6);
		this.airin.setPosition(650,50);
		this.airin.setEnable(true);
		
		this.inlabel  = new cc.LabelTTF("空气进入","微软雅黑",40);
		this.inlabel.setPosition(this.airin.width/2,this.airin.height/2);
		this.inlabel.setColor(cc.color(0,0,0,0));
		this.airin.addChild(this.inlabel);
		
		this.airout = new Button(this,8, TAG_AIROUT, "#unsel.png",this.callback);
		this.airout.setScale(0.6);
		this.airout.setPosition(650,-50);
		this.airout.setEnable(true);
		
		this.outlabel  = new cc.LabelTTF("空气排出","微软雅黑",40);
		this.outlabel.setPosition(this.airout.width / 2 , this.airout.height / 2);
		this.outlabel.setColor(cc.color(0,0,0,0));
		this.airout.addChild(this.outlabel);	
		
		this.beizhu = new exp08.Beizhu(this);
		this.beizhu1= new exp08.Beizhu1(this);
	},
	callback:function(p){
		var action = gg.flow.flow.action;
		var func = cc.callFunc(this.actionDone, this);	
		switch(p.getTag()){	
		case TAG_AIRIN:
			p.setSpriteFrame("sel.png");
			this.airout.setSpriteFrame("unsel.png");
			this.gemo.setSpriteFrame("gemo1.png");
			this.xiangpimo.setSpriteFrame("xiangpimo1.png");
			this.qiqiu.setSpriteFrame("qiqiu1.png");
			this.airout.setEnable(true);		
			this.beizhu.setVisible(true);
			this.beizhu1.setVisible(false);
			break;
		case TAG_AIROUT:
			p.setSpriteFrame("sel.png");
			this.airin.setSpriteFrame("unsel.png");
			this.gemo.setSpriteFrame("gemo.png");
			this.xiangpimo.setSpriteFrame("xiangpimo.png");
			this.qiqiu.setSpriteFrame("qiqiu.png");
			this.airin.setEnable(true);
			this.beizhu.setVisible(false);
			this.beizhu1.setVisible(true);
			break;
		}
	},
	flowNext:function(){
		gg.flow.next();
	},
});