exp08.Beizhu = cc.Node.extend({
	ctor:function(parent){
		this._super();	
		parent.addChild(this,10);
		this.setVisible(false);
		this.init();
	},
	init:function(){
		this.beizhu1 = this.addlinename(0.5,0,cc.p(420,-20),"气球(肺)\n扩大",cc.p(500,-20));
		this.beizhu2 = this.addlinename(0.5,0,cc.p(110,90),"脊柱",cc.p(190,90));
		this.beizhu3 = this.addlinename(0.5,0,cc.p(-110,110),"肋骨",cc.p(-190,110));	
		this.beizhu4 = this.addlinename(0.3,0,cc.p(425,-90),"橡皮膜",cc.p(500,-90));	
		this.beizhu5 = this.addlinename(0.5,-45,cc.p(-120,-80),"横膈膜的\n肌肉收缩",cc.p(-150,-145));
		this.beizhu6 = this.addlinename(0.5,0,cc.p(60,30),"气压低于\n大气压强",cc.p(150,30));	
		
		this.arrow1 = this.addarrow(0.5, 0, cc.p(350,-150));
		this.name1 = this.addname("橡皮膜向下拉",cc.p(350,-180));
		
		this.arrow2 = this.addarrow(0.5, 0, cc.p(350,150));
		this.name2 = this.addname("空气进入",cc.p(350,200));
		
		this.arrow3 = this.addarrow(0.5, 40, cc.p(-35,80));
		this.arrow4 = this.addarrow(0.5, 20, cc.p(-70,30));
		this.arrow5 = this.addarrow(0.5, -30, cc.p(20,70));
		this.arrow6 = this.addarrow(0.5, -10, cc.p(35,0));
		
		this.arrow7 = this.addarrow(0.5, 0, cc.p(0,150));
		this.name3 = this.addname("空气进入",cc.p(0,200));
		
		this.arrow8 = this.addarrow(0.5, 10, cc.p(-30,-110));
		this.arrow9 = this.addarrow(0.5, 20, cc.p(-50,-100));
		this.arrow10 = this.addarrow(0.5, -5, cc.p(-10,-110));
	
		
		
		
		
		this.name4 = this.addname("横膈膜下降\n吸气",cc.p(-25,-160));
		this.name5 = this.addname("肺部扩张",cc.p(-20,10));		
		
	},
	addlinename:function(scax,rota,pos,str,pos2){
		var line = new cc.Sprite("#line.png");
		line.setScale(scax,0.3);
		line.setRotation(rota);
		line.setPosition(pos);
		this.addChild(line);

		var name = new cc.LabelTTF(str,"微软雅黑",20);
		name.setPosition(pos2);
		this.addChild(name);
	},
	addarrowname:function(scax,rota,pos,str,pos2){
		var arrow = new cc.Sprite("#arrow.png");
		arrow.setScale(scax);
		arrow.setRotation(rota);
		arrow.setPosition(pos);
		this.addChild(arrow);

		var name = new cc.LabelTTF(str,"微软雅黑",25);
		name.setPosition(pos2);
		this.addChild(name);	
	},
	addname:function(str,pos){
		var name = new cc.LabelTTF(str,"微软雅黑",20);
		name.setPosition(pos);
		this.addChild(name);		
	},
	addarrow:function(scax,rota,pos){
		var arrow = new cc.Sprite("#arrow.png");
		arrow.setScale(scax,0.5);
		arrow.setRotation(rota);
		arrow.setPosition(pos);
		this.addChild(arrow);
	},
	fadein:function(){
		this.setVisible(true);
	},
	fadeout:function(){
		this.setVisible(false);
	}
})