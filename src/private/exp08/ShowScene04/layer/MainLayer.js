exp08.ShowMainLayer04 = cc.Layer.extend({
	lead:null,
	real:null,
	reset:true,
	reset1:true,
	reset2:true,
	ctor:function () {
		this._super();
		this.init();
		return true;
	},
	init:function(){
		var sv = new pub.ScrollView(this);
		sv.setOffestPos(cc.p(152,45 + 15));

		this.addlayout1(sv);
		this.addlayout2(sv);
		this.addlayout3(sv);
	},	
	addlayout1:function(parent){
		this.layout1 = new ccui.Layout();//第一页
		this.layout1.setContentSize(cc.size(996, 598));//设置layout的大小
		parent.addLayer(this.layout1);

		var text = $.format("　　植物也是生物，它是否和动物一样能进行呼吸作用呢？" +
				"\n　　试管中是澄清石灰水。", 960, 30);
		label1 = new cc.LabelTTF(text,gg.fontName,gg.fontSize4);
		label1.setAnchorPoint(0,0.5);
		label1.setPosition(0,this.layout1.height-label1.height/2);
		label1.setColor(cc.color(255, 255, 255, 250));
		this.layout1.addChild(label1,10);	

		var bg = new cc.Sprite(res_public.run_back);
		this.layout1.addChild(bg);
		bg.setScale(0.8);
		bg.setPosition(cc.p(this.layout1.width /2, this.layout1.height / 2 +30));

		this.addImage(this.layout1, 5, "#zhiwu1/1.png", cc.p(300,300), 1);	
		this.addImage(this.layout1, 5, "#zhiwu1/3.png", cc.p(300,235), 1);

		this.hunzhuo = new cc.Sprite("#zhiwu1/2.png");
		this.hunzhuo.setPosition(cc.p(300,180));
		this.layout1.addChild(this.hunzhuo,6);
		this.hunzhuo.setOpacity(0);
		
		this.addImage(this.layout1, 5, "#zhiwu1/1.png", cc.p(700,300), 1);	
		this.addImage(this.layout1, 5, "#zhiwu1/3.png", cc.p(700,235), 1);
		
		this.addshow("澄清石灰水",cc.p(500,180),2);
		
		this.addshow("已浸水\n及消毒\n的种子",cc.p(150,310),2,2);
		
		this.addshow("已煮熟\n及消毒\n的种子",cc.p(850,310),2,1);

		this.addbutton(this.layout1, TAG_BUTTON1, "播放", cc.p(500,320));
		this.layout1.getChildByTag(TAG_BUTTON1).getChildByTag(TAG_BUTTON1).setColor(cc.color(255, 255, 255, 250));

		this.label1 = new cc.LabelTTF("现象：A试管中液体变浑浊；而B试管中没有变化。" +
				"\n结论：植物也需要呼吸，呼吸时消耗了氧气，产生了二氧化碳。",gg.fontName,gg.fontSize4);
		this.label1.setAnchorPoint(0,0.5);
		this.label1.setPosition(80,80);
		this.label1.setColor(cc.color(255, 0, 0, 250));
		this.layout1.addChild(this.label1,10);
		this.label1.setVisible(false);				
	},
	addlayout2:function(parent){
		this.layout2 = new ccui.Layout();//第一页
		this.layout2.setContentSize(cc.size(996, 598));//设置layout的大小
		parent.addLayer(this.layout2);

		var text = $.format("　　左边黑色塑料袋中装有新鲜菠菜，右边塑料袋中装有开水烫过的新鲜菠菜，" +
				"试管中装有澄清石灰水，观察现象。", 960, 30);
		label1 = new cc.LabelTTF(text,gg.fontName,gg.fontSize4);
		label1.setAnchorPoint(0,0.5);
		label1.setPosition(0,this.layout2.height-label1.height/2);
		label1.setColor(cc.color(255, 255, 255, 250));
		this.layout2.addChild(label1,5);	

		var bg = new cc.Sprite(res_public.run_back);
		this.layout2.addChild(bg);
		bg.setScale(0.8);
		bg.setPosition(cc.p(this.layout2.width /2, this.layout2.height / 2 +30));
			
		var daizi = new cc.Sprite("#zhiwu2/1.png");
		daizi.setPosition(cc.p(300 -50,300 +50));
		this.layout2.addChild(daizi,6);
		daizi.setScale(0.8);
		daizi.setRotation(20);
		
		this.addImage(this.layout2, 5, "#zhiwu2/4.png", cc.p(470 -50,220 +50), 0.8);
		this.addImage(this.layout2, 4, "#zhiwu2/5.png", cc.p(400 -50,250 +50), 0.8);
		this.addImage(this.layout2, 5, "#zhiwu2/3.png", cc.p(470 -50,180 +50), 0.8);
		
		this.hunzhuo1 = new cc.Sprite("#zhiwu2/2.png");
		this.hunzhuo1.setPosition(cc.p(470 -50,135 +50));
		this.layout2.addChild(this.hunzhuo1,5);
		this.hunzhuo1.setScale(0.8);
		this.hunzhuo1.setOpacity(0);
				
		var daizi1 = new cc.Sprite("#zhiwu2/1.png");
		daizi1.setPosition(cc.p(700,300 +50));
		this.layout2.addChild(daizi1,6);
		daizi1.setScale(0.8);
		daizi1.setRotation(20);

		this.addImage(this.layout2, 5, "#zhiwu2/4.png", cc.p(470 + 400,220 +50), 0.8);
		this.addImage(this.layout2, 4, "#zhiwu2/5.png", cc.p(400+ 400,250 +50), 0.8);
		this.addImage(this.layout2, 5, "#zhiwu2/3.png", cc.p(470+ 400,180 +50), 0.8);
		
		this.addbutton(this.layout2, TAG_BUTTON2, "播放", cc.p(550,180));
		this.layout2.getChildByTag(TAG_BUTTON2).getChildByTag(TAG_BUTTON2).setColor(cc.color(255, 255, 255, 250));

		this.label2 = new cc.LabelTTF("现象：左边试管中的石灰水变浑浊，右边没反应。" +
				"\n结论：有生命的植物需要呼吸作用，并且释放二氧化碳。",gg.fontName,gg.fontSize4);
		this.label2.setAnchorPoint(0,0.5);
		this.label2.setPosition(80,80);
		this.label2.setColor(cc.color(255, 0, 0, 250));
		this.layout2.addChild(this.label2,10);
		this.label2.setVisible(false);
	},
	addlayout3:function(parent){
		this.layout3 = new ccui.Layout();//第一页
		this.layout3.setContentSize(cc.size(996, 598));//设置layout的大小
		parent.addLayer(this.layout3);

		var text = $.format("　　左边瓶中装有新鲜菠菜，右边瓶中装有开水烫过的新鲜菠菜，" +
				"用盖子盖紧，放在暗处一晚上，然后打开瓶盖，将点燃的蜡烛放入瓶内，观察现象。", 960, 30);
		label1 = new cc.LabelTTF(text,gg.fontName,gg.fontSize4);
		label1.setAnchorPoint(0,0.5);
		label1.setPosition(0,this.layout3.height-label1.height/2);
		label1.setColor(cc.color(255, 255, 255, 250));
		this.layout3.addChild(label1,5);	

		var bg = new cc.Sprite(res_public.run_back);
		this.layout3.addChild(bg);
		bg.setScale(0.8);
		bg.setPosition(cc.p(this.layout3.width /2, this.layout3.height / 2 +30));



		this.addImage(this.layout3, 5, "#bottle.png", cc.p(300,270), 0.6);
		this.addImage(this.layout3, 2, "#bocai.png", cc.p(300,230), 0.6);
		
		this.lid = new cc.Sprite("#lid.png");
		this.lid.setPosition(cc.p(300,370));
		this.layout3.addChild(this.lid,10);
		this.lid.setScale(0.6);
		
		this.lazhu = new cc.Sprite("#lazu.png");
		this.lazhu.setPosition(cc.p(500,200));
		this.layout3.addChild(this.lazhu,4);
		this.lazhu.setScale(0.8);
		this.lazhu.setVisible(false);
	
		this.fire = new cc.Sprite("#fire/1.png");
		this.fire.setPosition(cc.p(42,140));
		this.lazhu.addChild(this.fire);
		this.fire.runAction(this.addAction("fire/",3,0.05));
		
		this.addImage(this.layout3, 5, "#bottle.png", cc.p(700,270), 0.6);
		this.addImage(this.layout3, 2, "#bocai1.png", cc.p(700,200), 0.6);

		this.lid1 = new cc.Sprite("#lid.png");
		this.lid1.setPosition(cc.p(700,370));
		this.layout3.addChild(this.lid1,10);
		this.lid1.setScale(0.6);

		this.lazhu1 = new cc.Sprite("#lazu.png");
		this.lazhu1.setPosition(cc.p(900,200));
		this.layout3.addChild(this.lazhu1,4);
		this.lazhu1.setScale(0.8);
		this.lazhu1.setVisible(false);

		this.fire1 = new cc.Sprite("#fire/1.png");
		this.fire1.setPosition(cc.p(42,140));
		this.lazhu1.addChild(this.fire1);
		this.fire1.runAction(this.addAction("fire/",3,0.05));

		this.addbutton(this.layout3, TAG_BUTTON3, "播放", cc.p(500,180));
		this.layout3.getChildByTag(TAG_BUTTON3).getChildByTag(TAG_BUTTON3).setColor(cc.color(255, 255, 255, 250));

		this.label3 = new cc.LabelTTF("现象：左边瓶中的蜡烛很快熄灭，右边蜡烛没明显变化。" +
				"\n结论：有生命的植物需要呼吸作用，并且消耗氧气。",gg.fontName,gg.fontSize4);
		this.label3.setAnchorPoint(0,0.5);
		this.label3.setPosition(80,80);
		this.label3.setColor(cc.color(255, 0, 0, 250));
		this.layout3.addChild(this.label3,10);
		this.label3.setVisible(false);
	},
	moveLid:function(obj){
		var move = cc.moveBy(0.5,cc.p(0,40));
		var move1 = cc.moveBy(0.5,cc.p(-100,40));
		var seq = cc.sequence(move,move1,cc.callFunc(function(){
			obj.setVisible(false);
		},this));
		obj.runAction(seq);
	},
	moveLazhu:function(obj){
		var move = cc.moveBy(1.5,cc.p(-100,250));
		var move1 = cc.moveBy(1,cc.p(-100,0));
		var move2 = cc.moveBy(1.5,cc.p(0,-240));
		var seq = cc.sequence(move,move1,move2);
		obj.runAction(seq);
	},
	addAction:function(str1,num,time){
		var frames = [];
		for(var i =1 ;i<=num;i++){
			var str = str1+i+".png";
			var frame=cc.spriteFrameCache.getSpriteFrame(str);
			frames.push(frame);		
		}
		var animation = new cc.Animation(frames,time);//负责动画序列
		var action = new cc.Animate(animation);//帧动画的动作创建
		return action.repeatForever();
	},	
	addshow:function(str,pos,scalex,num){
		var label = new cc.LabelTTF(str,gg.fontName,30);
		this.layout1.addChild(label,5);
		label.setPosition(pos);
		label.setColor(cc.color(255,255,255,250));		
		var line = new cc.Sprite("#line.png");
		line.setAnchorPoint(1,0.5);
		line.setScale(scalex,4);
		line.setPosition(0 ,label.height/2);
		label.addChild(line);
		if(num == null){
			var line1 = new cc.Sprite("#line.png");
			line1.setAnchorPoint(0,0.5);
			line1.setScale(scalex,4);
			line1.setPosition(label.width ,label.height/2);
			label.addChild(line1);
		}else if(num ==2){
			line.removeFromParent(true);			
			var line1 = new cc.Sprite("#line.png");
			line1.setAnchorPoint(0,0.5);
			line1.setScale(scalex,4);
			line1.setPosition(label.width ,label.height/2);
			label.addChild(line1);			
		}		
	},
	addshowimage:function(parent,str,pos,str2){
		var showimage = new cc.Sprite(str);
		showimage.setPosition(pos);
		parent.addChild(showimage,5);
		var label = new cc.LabelTTF(str2,gg.fontName,gg.fontSize);
		label.setColor(cc.color(0, 0, 0, 250));
		parent.addChild(label);
		label.setPosition(showimage.x,showimage.y - showimage.height * 0.5 -20);
	},
	addImage:function(parent,index,str,pos,scale){
		var image = new cc.Sprite(str);
		parent.addChild(image,5);
		image.setPosition(pos);
		if(scale !=null){
			image.setScale(scale);
		}else{
			image.setScale(1);
		}
		image.setLocalZOrder(index);
	},
	addbutton:function(parent,tag,str,pos){
		var button = new ButtonScale(parent,"#unsel.png",this.callback,this);
		button.setPosition(pos);
		button.setLocalZOrder(5);
		button.setTag(tag);
		var label = new cc.LabelTTF(str,gg.fontName,30);
		label.setPosition(button.width/2,button.height/2);		
		label.setColor(cc.color(0, 0, 0, 250));
		button.addChild(label);
		label.setTag(tag);
	},
	callback:function(p){
		switch(p.getTag()){	
		case TAG_BUTTON1:
			this.reset = !this.reset;
			if(!this.reset){
				p.setEnable(false);
				var seq = cc.sequence(cc.fadeIn(3),cc.callFunc(function(){
					p.setEnable(true);
					this.layout1.getChildByTag(TAG_BUTTON1).getChildByTag(TAG_BUTTON1).setString("复原");
					this.label1.setVisible(true);		
				},this));
				this.hunzhuo.runAction(seq);				
			}else{
				this.layout1.getChildByTag(TAG_BUTTON1).getChildByTag(TAG_BUTTON1).setString("播放");
				this.hunzhuo.setOpacity(0);			
			}
			break;
		case TAG_BUTTON2:			
			this.reset1 = !this.reset1;
			if(!this.reset1){
				p.setEnable(false);
				var seq = cc.sequence(cc.fadeIn(3),cc.callFunc(function(){
					p.setEnable(true);
					this.layout2.getChildByTag(TAG_BUTTON2).getChildByTag(TAG_BUTTON2).setString("复原");
					this.label2.setVisible(true);		
				},this));
				this.hunzhuo1.runAction(seq);	
			}else{
				this.layout2.getChildByTag(TAG_BUTTON2).getChildByTag(TAG_BUTTON2).setString("播放");
				this.hunzhuo1.setOpacity(0);	
			}
			break;
		case TAG_BUTTON3:			
			this.reset2 = !this.reset2;
			if(!this.reset2){
				p.setEnable(false);
				var seq = cc.sequence(cc.callFunc(function(){
					this.moveLid(this.lid);
					this.moveLid(this.lid1);
				},this),cc.delayTime(1),cc.callFunc(function(){
				    this.lazhu.setVisible(true);
				    this.lazhu1.setVisible(true);
				    this.moveLazhu(this.lazhu);
				    this.moveLazhu(this.lazhu1);
				},this),cc.delayTime(5),cc.callFunc(function(){
					var se = cc.sequence(cc.spawn(cc.scaleTo(1,0.5),cc.moveTo(1,cc.p(42,130))),cc.callFunc(function(){
						this.fire.setVisible(false);
					},this));
					this.fire.runAction(se);
				},this),cc.delayTime(2),cc.callFunc(function(){
					p.setEnable(true);
					this.layout3.getChildByTag(TAG_BUTTON3).getChildByTag(TAG_BUTTON3).setString("复原");
					this.label3.setVisible(true);		
				},this));
			p.runAction(seq);
			}else{
				this.lid.setPosition(cc.p(300,370));
				this.lid.setVisible(true);
				this.lid1.setPosition(cc.p(700,370));
				this.lid1.setVisible(true);
				this.lazhu.setPosition(cc.p(500,200));
				this.lazhu.setVisible(false);
				this.lazhu1.setPosition(cc.p(900,200));
				this.lazhu1.setVisible(false);
				this.fire.setPosition(cc.p(42,140));
				this.fire.setScale(1);
				this.fire.setVisible(true);
				this.layout3.getChildByTag(TAG_BUTTON3).getChildByTag(TAG_BUTTON3).setString("播放");				
			}
			break;
		}
	}	
});
