var exp08 = exp08||{};
//start
exp08.res_start = {
		//start_png :"res/private/exp08/start/startpng.png"
};

exp08.g_resources_start = [];
for (var i in exp08.res_start) {
	exp08.g_resources_start.push(exp08.res_start[i]);
}

//run
exp08.res_run = {
		run_p : "res/private/exp08/run.plist",
		run_g : "res/private/exp08/run.png",
};

exp08.g_resources_run = [];
for (var i in exp08.res_run) {
	exp08.g_resources_run.push(exp08.res_run[i]);
}

//show02
exp08.res_show = {
		show_p : "res/private/exp08/show.plist",
		show_g : "res/private/exp08/show.png"
};

exp08.g_resources_show = [];
for (var i in exp08.res_show) {
	exp08.g_resources_show.push(exp08.res_show[i]);
}


