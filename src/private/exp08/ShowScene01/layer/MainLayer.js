exp08.ShowMainLayer01 = cc.Layer.extend({
	lead:null,
	real:null,
	reset:true,
	reset1:false,
	open_flag:false,
	donum:0,
	ctor:function () {
		this._super();
		this.init();
		return true;
	},
	init:function(){	
		var sv = new pub.ScrollView(this);
		sv.setOffestPos(cc.p(152,45 + 15));

		this.addlayout1(sv);	
		this.addlayout2(sv);	
	},	
	addlayout1:function(parent){
		this.layout1 = new ccui.Layout();//第一页
		this.layout1.setContentSize(cc.size(996, 598));//设置layout的大小
		parent.addLayer(this.layout1);
	
		var text = $.format("　　人体与外界环境进行气体交换的整个过程称为呼吸。肺与外界环境的气体交换是由呼吸系统来完成的。" , 980,30);
		label = new cc.LabelTTF(text,gg.fontName,gg.fontSize4);
		label.setColor(cc.color(0, 0, 0, 250));
		label.setAnchorPoint(0, 0.5);
		label.setPosition(0,this.layout1.height-label.height/2);
		this.layout1.addChild(label);	

		this.addImage(this.layout1,"#huxi.png",cc.p(500,360));

		var text = $.format("　　人体呼吸系统由呼吸道和肺组成。鼻、咽、喉、气管、支气管是气体进出肺的通道，统称为呼吸道。" +
				"肺使气体交换的器官，是呼吸系统最重要的部分。" +
				"\n　　在进行气体交换时，空气经鼻的过滤、温暖和湿润后，通过气管和支气管，到达肺，最后进入肺泡，同时把肺内部的气体呼出。", 980,30);
		label = new cc.LabelTTF(text,gg.fontName,gg.fontSize4);
		label.setColor(cc.color(0, 0, 0, 250));
		label.setAnchorPoint(0, 0.5);
		label.setPosition(0,100);
		this.layout1.addChild(label);							
	},
	addlayout2:function(parent){
	    this.layout2 = new ccui.Layout();//第一页
	    this.layout2.setContentSize(cc.size(996, 598));//设置layout的大小
	    parent.addLayer(this.layout2);

	    var text = $.format("　　吸气和呼气是依靠膈肌和肋间肌等的活动而产生的。在膈肌收缩、横膈变得扁平的同时，肋间外肌收缩，肋间内肌舒张，肋骨向上、" +
	    		"向外移动。此时，胸腔体积增大，内压力减小，人就吸气。反之，当膈肌和肋间外肌舒张，肋间内肌收缩时，人就呼气。" +
	    		"\n 人体呼吸时，体内气体有明显的差异。", 980,30);
	    label = new cc.LabelTTF(text,gg.fontName,gg.fontSize4);
	    label.setColor(cc.color(0, 0, 0, 250));
	    label.setAnchorPoint(0, 0.5);
	    label.setPosition(0,this.layout2.height-label.height/2);
	    this.layout2.addChild(label);	
	     
	    this.addImage(this.layout2,"#biaoge.png",cc.p(500,200));

	},
	addImage:function(parent,str,pos){
		var image = new cc.Sprite(str);
		parent.addChild(image,5);
		image.setPosition(pos);
	},
	addshowimage:function(parent,str,pos,str2){
		var showimage = new cc.Sprite(str);
		showimage.setPosition(pos);
		parent.addChild(showimage,5);
		var label = new cc.LabelTTF(str2,gg.fontName,gg.fontSize);
		label.setColor(cc.color(0, 0, 0, 250));
		parent.addChild(label);
		label.setPosition(showimage.x,showimage.y - showimage.height * 0.5 -20);
	},
	addbutton:function(parent,tag,str,pos){
		var button = new ButtonScale(parent,"#unsel.png",this.callback,this);
		button.setPosition(pos);
		button.setLocalZOrder(5);
		button.setTag(tag);
		var label = new cc.LabelTTF(str,gg.fontName,30);
		label.setPosition(button.width/2,button.height/2);		
		label.setColor(cc.color(0, 0, 0, 250));
		button.addChild(label);
		label.setTag(tag);
	},
	callback:function(p){
		switch(p.getTag()){	
		}
	}
});
