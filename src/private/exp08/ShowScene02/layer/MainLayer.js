exp08.ShowMainLayer02 = cc.Layer.extend({
	lead:null,
	real:null,
	reset:true,
	reset1:true,
	reset2:true,
	ctor:function () {
		this._super();
		this.init();
		return true;
	},
	init:function(){	
		var sv = new pub.ScrollView(this);
		sv.setOffestPos(cc.p(152,45 + 15));
	
		this.addlayout1(sv);	
		this.addlayout2(sv);	
	},	
	addlayout1:function(parent){
		this.layout1 = new ccui.Layout();//第一页
		this.layout1.setContentSize(cc.size(996, 598));//设置layout的大小
		parent.addLayer(this.layout1);

		var text = $.format("　　人体进行各种各样的生命活动都需要消耗能量。那么这些能量是从哪里来的呢？", 980, 30);
		label1 = new cc.LabelTTF(text,gg.fontName,gg.fontSize4);
		label1.setAnchorPoint(0,0.5);
		label1.setPosition(0,this.layout1.height-label1.height/2);
		label1.setColor(cc.color(0,0, 0, 250));
		this.layout1.addChild(label1,10);	
		
		this.addImage(this.layout1,"#huxi1.png",cc.p(500,280));				
		this.addshowimage(this.layout1,"#bt_blue.png",cc.p(150,450),"有机物");
		
		var O2 = new cc.Sprite("#bt_green.png");
		O2.setPosition(cc.p(150,100));
		this.layout1.addChild(O2,5);
		var text1 = new exp08.RichTTF(O2)
		.append(new exp08.CF(O2,"O","2","").setColor(cc.color(0,0,0)), cc.color(0,0,0))
		.setPos(O2.width*0.5,O2.height*0.5);
		
		var CO2 = new cc.Sprite("#bt_green.png");
		CO2.setPosition(cc.p(850,450));
		this.layout1.addChild(CO2,5);
		var text1 = new exp08.RichTTF(CO2)
		.append(new exp08.CF(CO2,"CO","2","").setColor(cc.color(0,0,0)), cc.color(0,0,0))
		.setPos(CO2.width*0.5,CO2.height*0.5);
		//this.addshowimage(this.layout1,"#bt_green.png",cc.p(150,100),"O₂");
		//this.addshowimage(this.layout1,"#bt_pink.png",cc.p(850,450),"CO₂");
		this.addshowimage(this.layout1,"#bt_purple.png",cc.p(850,280),"水");
		this.addshowimage(this.layout1,"#bt_yellow.png",cc.p(850,100),"能量");
		
		this.blue = new cc.Sprite("#bt_blue.png");
		this.blue.setPosition(150,450);
		this.layout1.addChild(this.blue,4);
		this.blue.runAction(cc.sequence(cc.spawn(cc.moveTo(2,cc.p(cc.p(500,280))),cc.scaleTo(2,0)),cc.callFunc(function(){
			this.blue.setScale(1);
			this.blue.setPosition(cc.p(150,450));
		},this)).repeatForever());
		
		this.green = new cc.Sprite("#bt_green.png");
		this.green.setPosition(150,100);
		this.layout1.addChild(this.green,4);
		this.green.runAction(cc.sequence(cc.spawn(cc.moveTo(2,cc.p(cc.p(500,280))),cc.scaleTo(2,0)),cc.callFunc(function(){
			this.green.setScale(1);
			this.green.setPosition(cc.p(150,100));
		},this)).repeatForever());
		
		this.pink = new cc.Sprite("#bt_pink.png");
		this.pink.setPosition(500,280);
		this.pink.setScale(0);
		this.layout1.addChild(this.pink,4);
		this.pink.runAction(cc.sequence(cc.spawn(cc.moveTo(2,cc.p(cc.p(850,450))),cc.scaleTo(2,1)),cc.callFunc(function(){
			this.pink.setScale(0);
			this.pink.setPosition(cc.p(500,280));
		},this)).repeatForever());
		
		this.purple = new cc.Sprite("#bt_purple.png");
		this.purple.setPosition(500,280);
		this.purple.setScale(0);
		this.layout1.addChild(this.purple,4);
		this.purple.runAction(cc.sequence(cc.spawn(cc.moveTo(2,cc.p(cc.p(850,280))),cc.scaleTo(2,1)),cc.callFunc(function(){
			this.purple.setScale(0);
			this.purple.setPosition(cc.p(500,280));
		},this)).repeatForever());
		
		this.yellow = new cc.Sprite("#bt_yellow.png");
		this.yellow.setPosition(500,280);
		this.yellow.setScale(0);
		this.layout1.addChild(this.yellow,4);
		this.yellow.runAction(cc.sequence(cc.spawn(cc.moveTo(2,cc.p(cc.p(850,100))),cc.scaleTo(2,1)),cc.callFunc(function(){
			this.yellow.setScale(0);
			this.yellow.setPosition(cc.p(500,280));
		},this)).repeatForever());		
	},
	addlayout2:function(parent){
		this.layout2 = new ccui.Layout();//第四页
		this.layout2.setContentSize(cc.size(996, 598));
		parent.addLayer(this.layout2);
		var text = $.format("　　人体生命活动需要的能量是由人体细胞内的有机物与氧气发生氧化反应所提供的。当有机物为葡萄糖时，人体细胞内" +
				"氧化反应的过程可以表示为：", 980, 30);
		label1 = new cc.LabelTTF(text,gg.fontName,gg.fontSize4);
		label1.setAnchorPoint(0,0.5);
		label1.setPosition(0,this.layout2.height-label1.height/2);
		label1.setColor(cc.color(0,0,0,250));
		this.layout2.addChild(label1,10);	
		
		this.addImage(this.layout2,"#gongshi.png",cc.p(500,360));	
		
		var text = $.format("　　人体细胞内的有机物与氧气反应，最终生成二氧化碳和水或其他产物，同时把有机物中的能量释放出来，" +
				"满足生命活动的需要，这个过程称为呼吸作用。呼吸作用是人体内的一种缓慢进行的氧化反应。", 980, 30) 
				this.label2 = new cc.LabelTTF(text,gg.fontName,gg.fontSize4);
		this.label2.setAnchorPoint(0,0.5);
		this.label2.setPosition(0,150);
		this.label2.setColor(cc.color(0, 0, 0, 250));
		this.layout2.addChild(this.label2,5);	
		
	},
	
	addImage:function(parent,str,pos){
		var image = new cc.Sprite(str);
		parent.addChild(image,2);
		image.setPosition(pos);
	},
	addshowimage:function(parent,str,pos,str2){
		var showimage = new cc.Sprite(str);
		showimage.setPosition(pos);
		parent.addChild(showimage,5);
		var label = new cc.LabelTTF(str2,gg.fontName,gg.fontSize);
		label.setColor(cc.color(0, 0, 0, 250));
		showimage.addChild(label);
		label.setPosition(showimage.width/2,showimage.height/2);
	},
	addbutton:function(parent,tag,str,pos){
		var button = new ButtonScale(parent,"#unsel.png",this.callback,this);
		button.setPosition(pos);
		button.setLocalZOrder(5);
		button.setTag(tag);
		var label = new cc.LabelTTF(str,gg.fontName,30);
		label.setPosition(button.width/2,button.height/2);		
		label.setColor(cc.color(0, 0, 0, 250));
		button.addChild(label);
		label.setTag(tag);
	},
	callback:function(p){
		switch(p.getTag()){	
		
		}
	}
});
