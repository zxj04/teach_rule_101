exp08.startRunNext = function(tag){
	switch(tag){//具体哪个菜单执行什么操作，这里修改
	case TAG_MENU1:
		pub.ch.gotoShow(TAG_EXP_08,tag , exp08.g_resources_show, null, new exp08.ShowScene01(tag));
		break;
	case TAG_MENU2:
		gg.teach_type = TAG_LEAD;
		gg.teach_flow = exp08.teach_flow01;
		pub.ch.gotoRun(TAG_EXP_08, tag,exp08.g_resources_run, null, new exp08.RunScene01());
		
		break;
	case TAG_MENU3:
		gg.teach_type = TAG_LEAD;
		gg.teach_flow = exp08.teach_flow02;
		pub.ch.gotoRun(TAG_EXP_08, tag,exp08.g_resources_run, null, new exp08.RunScene02());

		break;
	case TAG_MENU4:
		pub.ch.gotoShow(TAG_EXP_08,tag , exp08.g_resources_show, null, new exp08.ShowScene02(tag));
		break;
	case TAG_MENU5:
		pub.ch.gotoShow(TAG_EXP_08,tag , exp08.g_resources_show, null, new exp08.ShowScene03(tag));
		break;
	case TAG_MENU6:
		pub.ch.gotoShow(TAG_EXP_08,tag , exp08.g_resources_show, null, new exp08.ShowScene04(tag));
		break;
	case TAG_MENU7:
		pub.ch.gotoShow(TAG_EXP_08,tag , exp08.g_resources_show, null, new exp08.ShowScene05(tag));
		break;
	case TAG_ABOUT:
		ch.gotoAbout(TAG_EXP_08, g_resources_public_about, null, res_public_about.about_j);
		break;
	}
}

// 任务流
exp08.teach_flow01 = [
                {
                	tip:"呼吸系统",
                	tag:TAG_HUXI,
                	create:[],
                	destroy:[],
                	node:true,
                	frameInfo : [
                	             {ex:1,name:"鼻孔",tag:TAG_HUXI_BIKONG,px:240,py:675,img:"#huxi/bikong.png",st:"阻挡和粘住灰尘、细菌，还能温暖、湿润空气，感受气味刺激。",posx:170,posy:525,next:true,scale:4},
                	             {ex:1,name:"鼻腔",tag:TAG_HUXI_BIQIANG,px:232,py:759,img:"#huxi/biqiang.png",st:"阻挡和粘住灰尘、细菌，还能温暖、湿润空气，感受气味刺激。",posx:280,posy:550,next:true,scale:2.2},
                	             {ex:1,name:"肺",tag:TAG_HUXI_FEI,px:305,py:555,img:"#huxi/fei.png",st:"左、右支气管分别进入左、右两肺，形成树状分\n枝，最后形成肺泡管，每一肺泡管附有很多肺泡。",posx:760,posy:400,next:false,scale:4},
                	             {ex:1,name:"肺泡",tag:TAG_HUXI_FEIPAO,px:550,py:300,img:"#huxi/feipao.png",st:"布满毛细血管和弹性纤维，由于气体交换和使肺具有良好的弹性。",posx:750,posy:298,next:false,scale:2.9},
                	             //{ex:1,name:"横膈",tag:TAG_HUXI_HENGGE,px:225,py:320,img:"#huxi/hengge.png",st:"",posx:800,posy:250,next:false,scale:4},
                	             {ex:1,name:"喉",tag:TAG_HUXI_HOU,px:330,py:635,img:"#huxi/hou.png",st:"由软骨做支架，保持气体畅通。",posx:300,posy:480,next:true,scale:2.8},
                	             //{ex:1,name:"肋骨",tag:TAG_HUXI_LEIGU,px:210,py:538,img:"#huxi/leigu.png",st:"",posx:200,posy:300,next:true,scale:3.6},
                	             //{ex:1,name:"肋间肌",tag:TAG_HUXI_LEIJIANJI,px:215,py:535,img:"#huxi/leijianji.png",st:"",posx:200,posy:350,next:true,scale:3.6},
                	             {ex:1,name:"气管",tag:TAG_HUXI_QIGUAN,px:360,py:570,img:"#huxi/qiguan.png",st:"有C形软骨环做支架的环状结构。",posx:280,posy:430,next:true,scale:3.6},
                	             // {ex:1,name:"心脏",tag:TAG_HUXI_XINZANG,px:390,py:422,img:"#huxi/xinzang.png",st:"。",posx:750,posy:330,next:false,scale:4.6},
                	             {ex:1,name:"咽",tag:TAG_HUXI_YAN,px:365,py:660,img:"#huxi/yan.png",st:"前后略扁的管道，是空气和实物的共同通道。",posx:630,posy:510,next:false,scale:2.9},
                	             {ex:1,name:"支气管",tag:TAG_HUXI_ZHIQIGUAN,px:390,py:465,img:"#huxi/zhiqiguan.png",st:"管壁覆盖着有纤毛的黏膜，能分泌黏液，粘住灰尘和细菌。",posx:750,posy:360,next:false,scale:3.6},

                	             ],
                	             nodeName:"呼吸系统"
                },
                {
                	tip:"恭喜过关",
                	over:true
                }
                ]
exp08.teach_flow02 = [
                {
                	tip:"膈膜升降与呼吸",
                },
                {
                	tip:"恭喜过关",
                	over:true
                }
                ]