exp08.ShowMainLayer05 = cc.Layer.extend({
	lead:null,
	real:null,
	curIndex: 0,
	ctor:function () {
		this._super();
		this.init();
		return true;
	},
	init:function(){
		var sv = new pub.ScrollView(this);
		sv.setOffestPos(cc.p(152,45 + 15));

		this.addlayout1(sv);
		this.addlayout2(sv);
	},	
	addlayout1:function(parent){
		this.layout1 = new ccui.Layout();//第一页
		this.layout1.setContentSize(cc.size(996, 598));//设置layout的大小
		parent.addLayer(this.layout1);

		var text = $.format("　　下列不是呼吸道对空气作用的是（）", 860, 30);

		label1 = new cc.LabelTTF(text,gg.fontName,gg.fontSize4);
		label1.setAnchorPoint(0,0.5);
		label1.setPosition(0,this.layout1.height-label1.height/2);
		label1.setColor(cc.color(0, 0, 0, 250));
		this.layout1.addChild(label1);	

		this.addSelImage(this.layout1, "#select.png", TAG_BUTTON, cc.p(60,480 ),
				"A.杀死空气中的细菌","#right.png",TAG_SEL);

		this.addSelImage(this.layout1, "#select.png", TAG_BUTTON1, cc.p(60,430),
				"B.湿润空气","#wrong.png",TAG_SEL1);

		this.addSelImage(this.layout1, "#select.png", TAG_BUTTON2, cc.p(60,380),
				"C.清洁空气","#wrong.png",TAG_SEL2);

		this.addSelImage(this.layout1, "#select.png", TAG_BUTTON3, cc.p(60,330), 
				"D.温暖空气","#wrong.png",TAG_SEL3);
		var text = $.format("　　解析：鼻腔内的黏液能杀灭一些细菌并能湿润吸入的空气，但不能将空气中的细菌杀死。所以选择A。", 920, 25);

		this.label1 = new cc.LabelTTF(text,gg.fontName,25);
		this.label1.setAnchorPoint(0,0.5);
		this.label1.setPosition(0,180);
		this.label1.setColor(cc.color(0, 0, 0, 250));
		this.label1.setVisible(false);
		this.layout1.addChild(this.label1);	
	},
	addlayout2:function(parent){
		this.layout2 = new ccui.Layout();//第四页
		this.layout2.setContentSize(cc.size(996, 598));
		parent.addLayer(this.layout2);

		var text = $.format("　　人们经常在草坪上行走，会造成土壤板结，从而影响草的生长。土壤板结影响植物生长的主要原因是（）", 960, 30);
		label1 = new cc.LabelTTF(text ,gg.fontName,gg.fontSize4);
		label1.setAnchorPoint(0,0.5);
		label1.setPosition(0,this.layout2.height-label1.height/2);
		label1.setColor(cc.color(0, 0, 0, 250));
		this.layout2.addChild(label1);	

		this.addSelImage(this.layout2, "#select.png", TAG_BUTTON4, cc.p(60,480 ),
				"A.植物缺少无机盐，影响生长","#wrong.png",TAG_SEL4);

		this.addSelImage(this.layout2, "#select.png", TAG_BUTTON5, cc.p(60,430 ),
				"B.植物缺少水，影响生光合作用","#wrong.png",TAG_SEL5);

		this.addSelImage(this.layout2, "#select.png", TAG_BUTTON6, cc.p(60,380 ),
				"C.土壤缺少氧气，影响根的呼吸","#right.png",TAG_SEL6);

		this.addSelImage(this.layout2, "#select.png", TAG_BUTTON7, cc.p(60,330 ), 
				"D.气孔关闭，影响蒸腾作用","#wrong.png",TAG_SEL7);
		var text = $.format("　　分析：活着的生物体时刻需要呼吸，植物的根也要时刻吸入氧气、放出二氧化碳，" +
				"土壤板结会使土壤内缺少氧气，影响根的呼吸。所以选择C。", 920, 25);

		this.label2 = new cc.LabelTTF(text,gg.fontName,25);
		this.label2.setAnchorPoint(0,0.5);
		this.label2.setPosition(0,180);
		this.label2.setColor(cc.color(0, 0, 0, 250));
		this.label2.setVisible(false);
		this.layout2.addChild(this.label2);				
	},
	addSelImage:function(parent,str,tag,pos,str2,str3,tag2){
		var image = new ButtonScale(parent,str,this.callback,this);
		image.setPosition(pos);
		image.setTag(tag);

		var sel = new cc.Sprite(str3);
		sel.setPosition(15,15);
		image.addChild(sel);
		sel.setVisible(false);
		sel.setTag(tag2);

		var label = new cc.LabelTTF(str2,gg.fontName,gg.fontSize);
		label.setColor(cc.color(0, 0, 0, 250));
		label.setAnchorPoint(0,0.5);
		parent.addChild(label);
		label.setPosition(image.x + 20,image.y);
	},
	callback:function(p){
		switch(p.getTag()){	
		case TAG_BUTTON:
		case TAG_BUTTON1:
		case TAG_BUTTON2:
		case TAG_BUTTON3:
			this.layout1.getChildByTag(TAG_BUTTON).getChildByTag(TAG_SEL).setVisible(true);
			this.layout1.getChildByTag(TAG_BUTTON1).getChildByTag(TAG_SEL1).setVisible(true);
			this.layout1.getChildByTag(TAG_BUTTON2).getChildByTag(TAG_SEL2).setVisible(true);
			this.layout1.getChildByTag(TAG_BUTTON3).getChildByTag(TAG_SEL3).setVisible(true);
			this.label1.setVisible(true);	
			break;

		case TAG_BUTTON4:
		case TAG_BUTTON5:
		case TAG_BUTTON6:
		case TAG_BUTTON7:
			this.layout2.getChildByTag(TAG_BUTTON4).getChildByTag(TAG_SEL4).setVisible(true);
			this.layout2.getChildByTag(TAG_BUTTON5).getChildByTag(TAG_SEL5).setVisible(true);
			this.layout2.getChildByTag(TAG_BUTTON6).getChildByTag(TAG_SEL6).setVisible(true);
			this.layout2.getChildByTag(TAG_BUTTON7).getChildByTag(TAG_SEL7).setVisible(true);
			this.label2.setVisible(true);
			break;		
		
		}
	}	
});
