var SelectLayer = cc.Layer.extend({
	backgroundLayer : null,
	mainLayar : null,
	ctor:function () {
		this._super();
		this.stopMusic();
		this.initFrames();
		this.loadBackground();
		this.loadMainLayer();
		this.loadLoginMainLayer();
	},
	stopMusic : function() {
		_.cPause();
	},
	initFrames : function(){
// cc.spriteFrameCache.addSpriteFrames(res_start.start_p);
	},
	loadBackground : function(){
		this.backgroundLayer = new SelectBackgroundLayer();
		this.addChild(this.backgroundLayer);
	},
	loadMainLayer : function(){
		this.mainLayar = new SelectMainLayer();
		this.addChild(this.mainLayar);
	},
	loadLoginMainLayer :function(){
		this.login = new LoginMainLayer02();
		this.addChild(this.login,100);
		this.login.setVisible(false);				
	}
});

var SelectScene = PScene.extend({
	onEnter:function () {
		this._super();
		var layer = new SelectLayer();
		this.addChild(layer);
	}
});
