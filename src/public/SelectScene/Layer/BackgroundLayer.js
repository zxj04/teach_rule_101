var SelectBackgroundLayer = cc.Layer.extend({
	ctor:function () {
		this._super();
		this.loadBg();
//		this.loadLogo();
		return true;
	},
	loadBg : function(){
		var winSize = cc.winSize; 
		var Bg = new cc.Sprite("#login/bg.png");  
		Bg.setPosition(winSize.width/2,winSize.height/2);	
		this.addChild(Bg); 

		var Bg1 = new cc.Sprite(res_public.all_exp);  
		//Bg1.setPosition(winSize.width/2,winSize.height-100);	
		Bg1.setPosition(winSize.width/2,winSize.height-150);	
		this.addChild(Bg1,2); 
	},
	
	loadLogo : function(){
		var logo1 = new cc.Sprite(res_public.logo1_png);
		logo1.setPosition(480, 550);
		this.addChild(logo1, 10);
	},
	loadBack: function(){
		var menu = new cc.Menu();
		menu.setPosition( cc.p(0, 0) );
		this.addChild(menu, 1);
		var backItem = new cc.MenuItemImage("#button/back.png","#button/back.png",this.eventMenuCallback,this);
		menu.addChild(backItem, 1, TAG_CLOSE);
	}
});