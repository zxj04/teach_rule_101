var LoginMainLayer02 = cc.Layer.extend({
	lead:null,
	real:null,
	typeStudent:true,
	ctor:function () {
		this._super();
		this.loadTip();
		this.loadEditBox();
		this.loadLoginButton();
		this.loadKey();
		this.loadBg();
		this.loadVerification();
		this.loadClose();
		this.loadType();
		return true;
	},
	loadKey:function(){
		// 监听回车键
		var self = this;
		if ('keyboard' in cc.sys.capabilities){
			var listener_key = cc.EventListener.create({
				event: cc.EventListener.KEYBOARD,
				onKeyReleased: function (key, event) {
					cc.log( "Key up:" + key);
					if(key == cc.KEY.enter){
						self.postLogin();
					}
				}
			});
			cc.eventManager.addListener(listener_key, this);
		}
	},
	loadBg:function(){
		var login_bg = new cc.Sprite("#login/bg.png");
		this.addChild(login_bg);
		login_bg.setPosition(gg.c_p);
	},
	loadClose:function(){
		var close = new ButtonScale(this,"#login2/close.png",function(){
			this.setVisible(false);
			this.getParent().mainLayar.setVisible(true);
		});
		close.setAnchorPoint(1,1);
		close.setLocalZOrder(10);
		close.setPosition(gg.width - 60 ,gg.height - 60);
	},
	loadType:function(){
		var sprite = new cc.Sprite(res_public.login_edit);
		sprite.setAnchorPoint(0.5,0);
		sprite.setScale(0.23,1);
		sprite.setPosition(gg.c_width,90);
		this.addChild(sprite,10);
		sprite.setOpacity(150);
		
		this.type = new Label(this,"切换教师端",function(){
			cc.log(this.type.string);
			this.typeStudent = !this.typeStudent;
			if(this.typeStudent ){//当前为学生端			
				this.type.setString("切换教师端");
				this.loginstring.setString("开启赛学霸");
			}else{//当前为教师端
				this.type.setString("切换学生端");
				this.loginstring.setString("教师账号登录");
				
			}
		});
		this.type.setAnchorPoint(0.5,0);
		this.type.setPosition(gg.c_width,95);
		this.type.setLocalZOrder(10);
		this.type.setColor(cc.color(127,127,129,255));
		this.type.setFontSize(27);
	},
	loadTip:function(){
		this._tipL = new cc.LabelTTF("", gg.fontName, 25);
		this._tipL.setPosition(gg.c_width, gg.c_height);
		this._tipL.setColor(cc.color(255, 255, 255, 255));
		this._tipL.fillStyle=cc.color(0,0,0,125);
		this._tipL._textFillColor=cc.color(0, 0, 0, 255);
		this.addChild(this._tipL, 11);
	},
	loadEditBox:function (){
		if(cc.sys.os == "Windows" && cc.sys.isNative){
			var bg1=new cc.Sprite(res_public.login_edit);
			bg1.setAnchorPoint(0, 0);
			this.addChild(bg1,10);
			bg1.setPosition(356,gg.height-266);
			var _workCode = new EditBox(this);
			_workCode.setMaxLength(11);
			_workCode.setHit("点击输入手机号码");
			_workCode.setAnchorPoint(0, 1);
			_workCode.setPos(394,gg.height-246);
			_workCode.setTag(TAG_XH);
			_workCode.setLocalZOrder(gg.d_z_index);
		} else {
			var bg1=new cc.Sprite(res_public.login_edit);
			bg1.setAnchorPoint(0, 0);
			this.addChild(bg1,10);
			bg1.setPosition(356,gg.height-266);
			var _workCode = new cc.EditBox(
					cc.size(480, 40)
					,new cc.Scale9Sprite(res_public.login_edit1)//修改背景图
			);
			_workCode.setAnchorPoint(0, 1);
			_workCode.setPosition(cc.p(394,gg.height-206));
			_workCode.setPlaceHolder("请输入手机号码"); 
			_workCode._placeholderColor=cc.color(255, 255, 255, 255);//修改提示语颜色
			_workCode.setPlaceholderFont("微软雅黑",32);//修改提示语字体样式
			_workCode.setFontColor(cc.color(0, 0, 0, 255));
			_workCode.setMaxLength(11);
			_workCode.setFontSize(30);
			_workCode.setInputMode(cc.EDITBOX_INPUT_MODE_SINGLELINE);
			_workCode.setPlaceholderFontSize(29);
			this.addChild(_workCode, gg.d_z_index, TAG_XH);
		}

		if(cc.sys.os == "Windows" && cc.sys.isNative){
			var bg2=new cc.Sprite(res_public.login_edit);
			this.addChild(bg2,10);
			bg2.setAnchorPoint(0, 0);
			bg2.setPosition(cc.p(356,gg.height-356));
			var _passwd = new EditBox(this);
			_passwd.setMaxLength(11);
			_passwd.setHit("点击输入验证码");
			_passwd.setAnchorPoint(0,1);
			_passwd.setPos(394,gg.height-338);
			_passwd.setTag(TAG_MM);
			_passwd.setLocalZOrder(gg.d_z_index);
			_passwd.setPasswordEnabled(true);
		} else {
			var bg2=new cc.Sprite(res_public.login_edit);
			this.addChild(bg2,10);
			bg2.setAnchorPoint(0, 0);
			bg2.setPosition(cc.p(356,gg.height-356))
			var _passwd = new cc.EditBox(
					cc.size(320, 40), 
					new cc.Scale9Sprite(res_public.login_edit1));//修改背景图
			_passwd.setAnchorPoint(0,1);
			_passwd.setPosition(394,gg.height-298);
			_passwd.setPlaceHolder("请输入验证码"); 
			_passwd._placeholderColor=cc.color(255, 255, 255, 255);//修改提示语颜色
			_passwd.setPlaceholderFont("微软雅黑",32);//修改提示语字体样式
			_passwd.setInputFlag(cc.EDITBOX_INPUT_FLAG_PASSWORD);
			_passwd.setFontColor(cc.color(0, 0, 0, 255));
			_passwd.setFontSize(30);
			_passwd.setInputMode(cc.EDITBOX_INPUT_MODE_SINGLELINE);
			_passwd.setPlaceholderFontSize(29);
			this.addChild(_passwd, gg.d_z_index, TAG_MM);
		}
		
		var userName = cc.sys.localStorage.getItem("userName");
		var password = cc.sys.localStorage.getItem("password");
		if(!!userName){
			_workCode.setString(userName);
			_passwd.setString(password);
		}
	},
	loadVerification :function(){		
		var sprite = new cc.Sprite("#login2/line.png");
		sprite.setAnchorPoint(0,1);
		sprite.setScale(1,0.6);
		sprite.setPosition(718,this.height - 298);
		this.addChild(sprite,10);
		this.date = new cc.LabelTTF("60",gg.fontName,25);
		this.date.setPosition(810,450);
		this.addChild(this.date,10);
		this.date.setColor(cc.color(125,125,125,255));
		this.date.setVisible(false);
		
		this.date1 = new cc.LabelTTF("s",gg.fontName,22);
		this.date1.setPosition(830,450);
		this.addChild(this.date1,10);
		this.date1.setColor(cc.color(125,125,125,255));
		this.date1.setVisible(false);
		
		this.Verilabel = new Label(this,"获取验证码",function(){		
			this.oneLogin();
		});
		this.Verilabel.setAnchorPoint(0,1);
		this.Verilabel.setPosition(750,gg.height-298 -5);
		this.Verilabel.setLocalZOrder(10);
		this.Verilabel.setColor(cc.color(245,128,71,255));
		this.Verilabel.setFontSize(29);
	},
	loadLoginButton : function(){
		this._loginItem = new Angel(this,"#login/finish_login.png",this.eventMenuCallback);
		this._loginItem.setAnchorPoint(0,1);
		this._loginItem.setPosition(358, gg.height-433);
		this._loginItem.setTag(TAG_LOGIN);
		this._loginItem.setLocalZOrder(10);
		
		this.loginstring = new cc.LabelTTF("开启赛学霸",gg.fontName,40);
		this.loginstring.setPosition(this._loginItem.width/2,this._loginItem.height/2 +3);
		this._loginItem.addChild(this.loginstring);
	},
	login: function(url){
		var xhr = cc.loader.getXMLHttpRequest();
		xhr.open("GET", url);
		result_layer = this;
		xhr.onreadystatechange = function () {
			if (xhr.readyState == 4 && (xhr.status >= 200 && xhr.status <= 207)) {
				var result = xhr.responseText;
				var jObject = JSON.parse(result);
				login_layer.loginResult(jObject);
			}
		};
		xhr.send();
	},
	logining:false,
	eventMenuCallback: function(pSender) {
		if(this.logining){
			return;
		}
		switch (pSender.getTag()){
		case TAG_LOGIN:
			this.postLogin();
			break;
		default:
			break;
		}
	},
	oneLogin:function(){//获取验证码
		var workCode = this.getChildByTag(TAG_XH).getString();
		cc.sys.localStorage.setItem("userName", workCode);
		if($.checkNull(workCode)){
			this._tipL.setString("手机号码不正确！");
			return;
		}		
		this._tipL.setString(" ");
		net.oneLogin(workCode,this.oneLoginResult, this);
	},
	oneLoginResult:function(json){//获取验证码结果
		this.logining = false;
		cc.log(json);
		if(json == null || json.result == 1 || json.result == 2){
			if(json.result == 2){
				this._tipL.setString(json.message);
			}else{
				this._tipL.setString(json.message);
			}
		} else if(json.result == 0){
			this.date.setVisible(true);
			this.date1.setVisible(true);
			this.Verilabel.setVisible(false);
			var date = 60;
			this.schedule(function(){
				this.date.setString(date);
				date -=1;
			},1,60);
			this.schedule(function(){
				this.date.setVisible(false);
				this.date1.setVisible(false);
				this.Verilabel.setVisible(true);
			},1,1,60);
		}
		
	},
	postLogin:function(){
		// 登录
		var workCode = this.getChildByTag(TAG_XH).getString();
		var passwd = this.getChildByTag(TAG_MM).getString();
		cc.sys.localStorage.setItem("userName", workCode);
		cc.sys.localStorage.setItem("password", passwd);
		if($.checkNull(workCode) || $.checkNull(passwd)){
			this._tipL.setString("手机号或验证码不能为空！");
			return;
		}
		cc.log("进入登录" + "学号" + workCode+ "    密码" + passwd);
		this._tipL.setString("登录中...");
		this.logining = true;
		if(this.typeStudent){
			cc.log("this.typeStudent:"+this.typeStudent);
			net.login2("student",workCode, passwd, this.loginResult, this);
		}
		else{
			net.login2("teacher",workCode, passwd, this.loginResult, this);
		}

	},
	loginResult: function(json){//登入结果
		this.logining = false;
		cc.log(json);
		if(json == null || json.result == 1 || json.result == 2){
			if(json.result == 2){
				this._tipL.setString(json.message);
			}else{
				this._tipL.setString(json.message);
			}

		} else {
			uu.userId = json.object.userId;
			if(json.type == "teacher"){
				gg.typeStudent = false;
				cc.log("json.object.authentication is " + json.object.authentication);
				if(json.object.authentication == 2){//教师					
					for(var i = 1;i< expInfo.length ;i++){
						expInfo[i].isOrder = 1;
					}
				}else{
					expInfo[1].isOrder = 1;
					for(var i = 2;i< expInfo.length ;i++){
						expInfo[i].isOrder = 0;
					}
				}
				pub.ch.howToGo(OP_TYPE_SELECT);
			}else{
				gg.typeStudent = true;
				net.openExp(uu.userId, gg.userBagId, this.selPower ,this);//
			}
			

		}
	},
	selPower:function(json){
		cc.log(json);
		if(json == null || json.result == 1){
			cc.log("json error");		
		} else {
			for(var i = 1;i< expInfo.length ;i++){
				var exp = expInfo[i];
				for(var j = 0;j< json.object.length ;j++){
					var object = json.object[j];
					if(exp.expId == object.expId){
						exp.isOrder = object.isOrder;
						cc.log(exp.expName + exp.isOrder );
						break;
					}
				}
			}
			
			pub.ch.howToGo(OP_TYPE_SELECT);
		}
	}
});
