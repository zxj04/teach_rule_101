// 帮助检测碰撞,辅助数组
pub.showHandArr = [];
pub.ShowButton = ButtonScale.extend({
	canMove:false,
	listener:null,
	ctor:function(parent, fileName, name, callback, back){
		this._super(parent, fileName, callback, back);
		this.init(name);
	},
	init:function(name){
		this.showName = name;
		this.setCascadeOpacityEnabled(true);
		var label = new cc.LabelTTF(name, gg.fontName, 28);
		label.setPosition(this.width * 0.5, this.height * 0.5)
		this.addChild(label);
		this.label = label;
		this.canMove = false;
	},
	setLabelColor:function(color){
		this.label.setColor(color);
		return this;
	},
	setLabelSize:function(size){
		this.label.setFontSize(size);
		return this;
	},
	setPos:function(newPosOrxValue, yValue){
		if (yValue === undefined) {
			this.setPosition(newPosOrxValue);
		} else {
			this.setPosition(newPosOrxValue, yValue);
		}
		return this;
	},
	addArr:function(arr){
		arr = arr || [];
		arr.push(this);
	},
	getShowName:function(){
		return this.showName;
	},
	addMove:/**
			 * 添加移动功能
			 * 
			 * @param bool
			 */
	function(bool, callback, back){
		this.canMove = bool;
		if(!!callback){
			this.callback = callback;
		}
		if(!!back){
			this.back = back;
		}
		if(this.listener){
		} else {
			this.listener = cc.EventListener.create({
				event: cc.EventListener.TOUCH_ONE_BY_ONE,
				swallowTouches: true,
				onTouchBegan:this.onTouchBegan,
				onTouchMoved:this.onTouchMoved,
				onTouchCancelled:this.onTouchEnded,
				onTouchEnded:this.onTouchEnded});
			// 添加触摸事件
			cc.eventManager.addListener(this.listener, this);
		}
		this.listener.setEnabled(bool);
		this.setEnable(false);
		return this;
	},
	onTouchBegan:function(touch, event){
		var target = event.getCurrentTarget();
		var pos = cc.p(touch.getLocationX(),touch.getLocationY());
		if(cc.rectContainsPoint(
				target.getBoundingBoxToWorld(),pos)){
			this.beginPos = target.getPosition();// 获取初始坐标
			this.beginTouch = pos;
			return true;
		}
		return false;
	},
	onTouchMoved: function(touch, event){
		if(this.beginPos){// 如果点击了
			var target = event.getCurrentTarget();
			var pos = cc.p(touch.getLocationX(),touch.getLocationY());
			var mX = this.beginTouch.x - pos.x;
			var mY = this.beginTouch.y - pos.y;
			target.setPosition(cc.p(this.beginPos.x - mX, this.beginPos.y - mY));
			
			for(var i = 0; i < pub.showHandArr.length; i++){
				var obj = pub.showHandArr[i];
				if(cc.rectIntersectsRect(target.getBoundingBox(), obj.getBoundingBox())){
					obj.flash();
				} else {
					obj.stop();
				}
			}
		}
	},
	onTouchEnded: function(touch, event){
		if(this.beginPos){
			var target = event.getCurrentTarget();
			for(var i = 0; i < pub.showHandArr.length; i++){
				var obj = pub.showHandArr[i];
				obj.stop();
				if(!target.callback){
					return;
				}
				if(cc.rectIntersectsRect(target.getBoundingBox(), obj.getBoundingBox())){
					// 触碰 回调
					target.collisionTag = obj.getTag();
					target.collision = obj;
					target.callback.call(target.back, target);
				}
			}
			target.setPosition(this.beginPos);
			this.beginPos = null;
		}
	}
});