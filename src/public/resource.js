var res_public = {
  
    login_jpg : "res/login_run_bg.jpg",
   // select_jpg : "res/Common/login/bg.png",
    common_p : "res/common.plist",
    common_g : "res/common.png",
    login_p : "res/login.plist",
    login_g : "res/login.png",
	bg_mp3 : "res/Music/bg.mp3",
	ok_mp3 : "res/Music/ok.mp3",
	ka_mp3 : "res/Music/ka.wav",
	error_mp3 : "res/Music/error.wav",
	clock_mp3 : "res/Music/clock.wav",
	over_mp3 : "res/Music/over.mp3",
	bt1 : "res/bt1.png",
	bt2 : "res/bt2.png",
	run_back:"res/login_run_bg.png",
	all_exp :"res/allexp.png",
	leftbutton:"res/left.png",
	rightbutton:"res/right.png",
	yuanli:"res/yuanli.png",
	mudi:"res/mudi.png",
	show_tip:"res/show_tip.png",
	start_back:"res/start_bg.jpg",
	wosai : "res/wosai.png",
	login_edit:"res/login_edit.png"
};
var g_resources_public = [];
for (var i in res_public) {
	g_resources_public.push(res_public[i]);
}

//start
var res_public_start = {
		start_g : "res/start.png",
		start_p : "res/start.plist"
};

var g_resources_public_start = [];
for (var i in res_public_start) {
	g_resources_public_start.push(res_public_start[i]);
}

//run
var res_public_run = {
		run_g : "res/run.png",
		run_p : "res/run.plist",
};

var g_resources_public_run = [];
for (var i in res_public_run) {
	g_resources_public_run.push(res_public_run[i]);
}

//finish
var res_public_finish = {
		finish_g : "res/finish.png",
		finish_p : "res/finish.plist"
};

var g_resources_public_finish = [];
for (var i in res_public_finish) {
	g_resources_public_finish.push(res_public_finish[i]);
}

//about
var res_public_about = {
		about_g : "res/about.png",
		about_p : "res/about.plist",
		//about_j : "res/JsonLib/about.json"
};

var g_resources_public_about = [];
for (var i in res_public_about) {
	g_resources_public_about.push(res_public_about[i]);
}

//test
var res_public_test = {
		test_g : "res/test.png",
		test_p : "res/test.plist"
};
var g_resources_public_test = [];
for (var i in res_public_test) {
	g_resources_public_test.push(res_public_test[i]);
}

//game
var res_public_game = {
		game_g : "res/game.png",
		game_p : "res/game.plist"
};

var g_resources_public_game = [];
for (var i in res_public_game) {
	g_resources_public_game.push(res_public_game[i]);
}