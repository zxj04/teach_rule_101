var /**
 * 
 */
net = {         
		server_url:'http://121.40.195.52:8080/k12platform/service',
		getUrl:/**
		 * 获取服务器URL
		 */
			function(){
			var url = this.server_url;
			if(md.server_url != null){
				url = md.server_url;
			}
			return url;
		},
		saveScore:function(){
			// /expRec/upload/{userId}/{expVer}/{expId}/{isFinished}/{lastStep}/{score}/{level}/{beginTime}/{endTime}/{totalStep}/{rightStep}
			var url = this.getUrl();
			url += "/expRec/upload";
			url += "/"+uu.userId;// userId
			url += "/"+gg.expVer;// expVer
			url += "/"+gg.expId;// expId

			url += "/"+1;// isFinished,1已过关
			url += "/"+gg.lastStep;// lastStep
			url += "/"+gg.score;// score
			url += "/"+gg.teach_type;// level
			url += "/"+$.sdf(gg.begin_time,'yyyyMMddhhmmss');// beginTime
			url += "/"+$.sdf(gg.end_time,'yyyyMMddhhmmss');// endTime
			url += "/"+gg.totalStep;// totalStep
			url += "/"+gg.totalStep;// rightStep
			cc.log("Network gg.homeworkId:"+gg.homeworkId);
			if(!gg.homeworkId){
				cc.log("homeworkId is null, so not commit");
				return;
			}
			var data = {homeworkId: gg.homeworkId};
			this.ajax(url, function(){
				if (this.readyState == 4 && (this.status >= 200 && this.status <= 207)) {
					var result = this.responseText;
					cc.log("say result : " + result);
				}
			}, "POST", JSON.stringify(data));
		},
		login: function(workCode, passwd, func, target){
			var url = this.getUrl() + "/fileBag/login";
			var data = {loginName: workCode ,password: passwd};
			this.ajax(url, function(){
				if (this.readyState == 4 && (this.status >= 200 && this.status <= 207)) {
					var result = this.responseText;
					var jObject = JSON.parse(result);				
					func.call(target, jObject);				
				}
			},"POST",JSON.stringify(data),function(){			
				var jObject = {result:1,message:"加载失败，请检查网络后重试！"}
				func.call(target, jObject);						
			});
		},
		oneLogin : function(workCode, func,target){//获取验证码接口
			var url = this.getUrl() + "/login/oneLogin/randomCode/{workCode}";
			url = url.replace("{workCode}",workCode);
			this.ajax(url, function(){
				if (this.readyState == 4 && (this.status >= 200 && this.status <= 207)) {
					var result = this.responseText;
					var jObject = JSON.parse(result);				
					func.call(target, jObject);				
				}
			},"GET", null,function(){			
				cc.log("网络连接失败 " + this.readyState); 
				var jObject = {result:1,message:"加载失败，请检查网络后重试！"}
				func.call(target, jObject);						
			});
		},
		login2: function(type, workCode, passwd, func, target){//登入接口
			var url = this.getUrl() + "/login/oneLogin/{type}/{mobile}/{randomCode}";
			url = url.replace("{type}",type).replace("{mobile}",workCode).replace("{randomCode}",passwd);
			cc.log("url is " + url); 
			this.ajax(url, function(){
				cc.log("log " + this.readyState); 
				if (this.readyState == 4 && (this.status >= 200 && this.status <= 207)) {
					var result = this.responseText;
					var jObject = JSON.parse(result);	
					jObject.type = type;
					func.call(target, jObject);				
				}
			},"GET", null,function(){			
				cc.log("网络连接失败 " + this.readyState); 
				var jObject = {result:1,message:"加载失败，请检查网络后重试！"}
				func.call(target, jObject);						
			});
		},
		getSubject:function(cb){
			var url = "http://teach-platform.oss-cn-hangzhou.aliyuncs.com/experiment/jsonLib/subject.json";
			this.ajax(url, function(a1, a2, a3){
				cc.log(a1);
			});
		},
		openExp:function(userId,userBagId,func ,target){
			var url = this.getUrl()+"/fileBag/openExp/{userId}/{userBagId}";
			url = url.replace("{userId}",userId).replace("{userBagId}",userBagId);
			this.ajax(url, function(){
				if (this.readyState == 4 && (this.status >= 200 && this.status <= 207)) {
					var result = this.responseText;
					var jObject = JSON.parse(result);
					func.call(target, jObject);	
				}
			});
		},
		ajax:function(url,func,method,params,errorFunc){
			var xhr = cc.loader.getXMLHttpRequest();
			if(method != null){
				xhr.open(method, url);
			} else {
				xhr.open("GET", url);	
			}
			if(func != null){
				xhr.onreadystatechange = func.bind(xhr);	
			}
			if(errorFunc!=null){
				xhr.onerror = errorFunc;
			}
			if(params){
				xhr.send(params);
			} else {
				xhr.send();
			}
		}
};