/**
 *实验配置信息
 */
expInfo = [
{
	expName:"公共部分",
	expTag:TAG_EXP_PUBLIC,
	expId:-1,
	loadRes:[{
		expTag:TAG_EXP_PUBLIC,
		type:TAG_LOAD_TYPE_START,
		finish:false,
		res:g_resources_public_start
	},{
		expTag:TAG_EXP_PUBLIC,
		type:TAG_LOAD_TYPE_RUN,
		finish:false,
		res:g_resources_public_run
	},{
		expTag:TAG_EXP_PUBLIC,
		type:TAG_LOAD_TYPE_ABOUT,
		finish:false,
		res:g_resources_public_about
	},{
		expTag:TAG_EXP_PUBLIC,
		type:TAG_LOAD_TYPE_GAME,
		finish:false,
		res:g_resources_public_game
	},{
		expTag:TAG_EXP_PUBLIC,
		type:TAG_LOAD_TYPE_FINISH,
		finish:false,
		res:g_resources_public_finish
	},{
		expTag:TAG_EXP_PUBLIC,
		type:TAG_LOAD_TYPE_TEST,
		finish:false,
		res:g_resources_public_test
	},]
},
{
	expName: "移液管的使用",
	expTag: TAG_EXP_01,
	expId: 201,
	expTitle:"移液管的使用",
	expimage:"#exp/exp01.png",
	chapter:"",
	startPng:res_start01.start_png,
	expDemo:TAG_TYPE_CESHI,
	run: function(){
		gg.runNext = exp01.startRunNext;
		gg.initExp(this.expId,this.expName,this.expDemo, exp01.teach_flow01);
		pub.ch.gotoStart(this.expTag, this.loadRes[0].res, res_start01.start_p);
	},
	menuItem :[
	           {
	        	   menu:"实验介绍",
	        	   tag:TAG_MENU1
	           },	          
	           {
	        	   menu:"洗涤",
	        	   tag:TAG_MENU2
	           },
	           {
	        	   menu:"移液",
	        	   tag:TAG_MENU3
	           },
	           {
	        	   menu:"吸液",
	        	   tag:TAG_MENU4 
	           },
	           {
	        	   menu:"用液",
	        	   tag:TAG_MENU5
	           },
	           {
	        	   menu:"放液",
	        	   tag:TAG_MENU6 
	           }	   

	           ],
	           loadRes:[{
	        	   expTag:TAG_EXP_01,
	        	   type:TAG_LOAD_TYPE_START,
	        	   finish:false,
	        	   res:g_resources_start01
	           },{
	        	   expTag:TAG_EXP_01,
	        	   type:TAG_LOAD_TYPE_RUN,
	        	   finish:false,
	        	   res:g_resources_run01
	           },{
	        	   expTag:TAG_EXP_01,
	        	   type:TAG_LOAD_TYPE_FINISH,
	        	   finish:false,
	        	   res:[]
	           },{
	        	   expTag:TAG_EXP_01,
	        	   type:TAG_LOAD_TYPE_SHOW,
	        	   finish:false,
	        	   res:g_resources_show01
	           },{
	        	   expTag:TAG_EXP_01,
	        	   type:TAG_LOAD_TYPE_ABOUT,
	        	   finish:false,
	        	   res:[]
	           },{
	        	   expTag:TAG_EXP_01,
	        	   type:TAG_LOAD_TYPE_TEST,
	        	   finish:false,
	        	   res:[]
	           }]
},
{
	expName: "电磁铁的应用",
	expTag: TAG_EXP_02,
	expId: 172,
	expTitle:"电磁铁的应用",
	expimage:"#exp/exp02.png",
	chapter:"第一章　第三节",
	startPng:res_start01.start_png,
	expDemo : null,
	run: function(){
		gg.runNext = exp02.startRunNext;
		gg.initExp(this.expId,this.expName,this.expDemo, exp02.teach_flow01);
		pub.ch.gotoStart(this.expTag, this.loadRes[0].res, exp02.res_start.start_p);
	},
	menuItem :[
	           {
	        	   menu:"电磁铁的应用",
	        	   tag:TAG_MENU1 
	           },
	           {
	        	   menu:"电磁继电器",
	        	   tag:TAG_MENU2 
	           },
	           {
	        	   menu:"水位自动报警器",
	        	   tag:TAG_MENU3 
	           },
	           {
	        	   menu:"电铃",
	        	   tag:TAG_MENU4 
	           },
	        
	           {
	        	   menu:"经典例题",
	        	   tag:TAG_MENU5 
	           }      
	           
	           ],
	           loadRes:[{
	        	   expTag:TAG_EXP_02,
	        	   type:TAG_LOAD_TYPE_START,
	        	   finish:false,
	        	   res:exp02.g_resources_start
	           },{
	        	   expTag:TAG_EXP_02,
	        	   type:TAG_LOAD_TYPE_RUN,
	        	   finish:false,
	        	   res:exp02.g_resources_run
	           },{
	        	   expTag:TAG_EXP_02,
	        	   type:TAG_LOAD_TYPE_FINISH,
	        	   finish:false,
	        	   res:[]
	           },{
	        	   expTag:TAG_EXP_02,
	        	   type:TAG_LOAD_TYPE_SHOW,
	        	   finish:false,
	        	   res:exp02.g_resources_show
	           }]
},
{
	expName: "表示元素的符号",
	expTag: TAG_EXP_03,
	expId: 193,
	expTitle:"表示元素的符号",
	expimage:"#exp/exp03.png",
	chapter:"第二章　第五节",
	startPng:res_start01.start_png,
	expDemo : null,
	run: function(){
		gg.runNext = exp03.startRunNext;
		gg.initExp(this.expId,this.expName,this.expDemo, exp03.teach_flow01);
		pub.ch.gotoStart(this.expTag, this.loadRes[0].res, exp03.res_start.start_p);
	},
	menuItem :[
	           {
	        	   menu:"元素符号",
	        	   tag:TAG_MENU1 
	           },
	           {
	        	   menu:"元素周期表",
	        	   tag:TAG_MENU2 
	           },
	           {
	        	   menu:"元素翻翻看",
	        	   tag:TAG_MENU3 
	           },
	           {
	        	   menu:"经典例题",
	        	   tag:TAG_MENU4 
	           }      
	           ],
	           loadRes:[{
	        	   expTag:TAG_EXP_01,
	        	   type:TAG_LOAD_TYPE_START,
	        	   finish:false,
	        	   res:exp03.g_resources_start
	           },{
	        	   expTag:TAG_EXP_01,
	        	   type:TAG_LOAD_TYPE_RUN,
	        	   finish:false,
	        	   res:exp03.g_resources_run
	           },{
	        	   expTag:TAG_EXP_01,
	        	   type:TAG_LOAD_TYPE_FINISH,
	        	   finish:false,
	        	   res:[]
	           },{
	        	   expTag:TAG_EXP_01,
	        	   type:TAG_LOAD_TYPE_SHOW,
	        	   finish:false,
	        	   res:exp03.g_resources_show
	           }]
},
{
	expName: "表示物质的符号" ,
	expTag: TAG_EXP_04,
	expId: 194,
	expTitle:"表示物质的符号",
	expimage:"#exp/exp04.png",
	chapterImg:"#chapter/chapter3.png",
	startPng:exp04.res_start.start_png,
	chapter:"第二章　第六节",
	run: function(){
		gg.runNext = exp04.startRunNext;
		gg.initExp(this.expId,this.expName,this.expDemo, exp04.teach_flow01);
		pub.ch.gotoStart(this.expTag, this.loadRes[0].res, exp04.res_start.start_p);
	},
	menuItem :[
	           {
	        	   menu:"化学式",
	        	   tag:TAG_MENU1 
	           },
	           {
	        	   menu:"离子的符号",
	        	   tag:TAG_MENU2 
	           },
	           {
	        	   menu:"化合价",
	        	   tag:TAG_MENU3 
	           },
	           {
	        	   menu:"经典例题",
	        	   tag:TAG_MENU4 
	           }   
	           ],
	           loadRes:[{
	        	   expTag:TAG_EXP_01,
	        	   type:TAG_LOAD_TYPE_START,
	        	   finish:false,
	        	   res:exp04.g_resources_start
	           },{
	        	   expTag:TAG_EXP_01,
	        	   type:TAG_LOAD_TYPE_RUN,
	        	   finish:false,
	        	   res:exp04.g_resources_run
	           },{
	        	   expTag:TAG_EXP_01,
	        	   type:TAG_LOAD_TYPE_FINISH,
	        	   finish:false,
	        	   res:[]
	           },{
	        	   expTag:TAG_EXP_01,
	        	   type:TAG_LOAD_TYPE_SHOW,
	        	   finish:false,
	        	   res:exp04.g_resources_show
	           }]
},
{
	expName: "空气与氧气",
	expTag: TAG_EXP_05,
	expId: 173,
	expTitle:"空气与氧气",
	expimage:"#exp/exp05.png",
	chapter:"第三章　第一节",
	startPng:exp05.res_start.start_png,
	expDemo : null,
	run: function(){
		gg.runNext = exp05.startRunNext;
		gg.initExp(this.expId,this.expName,this.expDemo, exp05.teach_flow01);
		pub.ch.gotoStart(this.expTag, this.loadRes[0].res, exp05.res_start.start_p);
	},
	menuItem :[
	           {
	        	   menu:"空气的成分",
	        	   tag:TAG_MENU1 
	           },
	           {
	        	   menu:"空气成分的测定实验",
	        	   tag:TAG_MENU2 
	           }, {
	        	   menu:"空气中氧气含量的测定",
	        	   tag:TAG_MENU3 
	           },
	           {
	        	   menu:"空气的利用",
	        	   tag:TAG_MENU4 
	           },
	           {
	        	   menu:"氧气的性质",
	        	   tag:TAG_MENU5 
	           },
	           {
	        	   menu:"氧气的制取",
	        	   tag:TAG_MENU6 
	           },
	           {
	        	   menu:"双氧水制取氧气",
	        	   tag:TAG_MENU7 
	           },
	           {
	        	   menu:"高锰酸钾制取氧气",
	        	   tag:TAG_MENU8 
	           },
	           {
	        	   menu:"化合反应和分解",
	        	   tag:TAG_MENU9
	           },
	           {
	        	   menu:"经典例题",
	        	   tag:TAG_MENU10
	           }      

	           ],
	           loadRes:[{
	        	   expTag:TAG_EXP_05,
	        	   type:TAG_LOAD_TYPE_START,
	        	   finish:false,
	        	   res:exp05.g_resources_start
	           },{
	        	   expTag:TAG_EXP_05,
	        	   type:TAG_LOAD_TYPE_RUN,
	        	   finish:false,
	        	   res:exp05.g_resources_run
	           },{
	        	   expTag:TAG_EXP_05,
	        	   type:TAG_LOAD_TYPE_FINISH,
	        	   finish:false,
	        	   res:[]
	           },{
	        	   expTag:TAG_EXP_05,
	        	   type:TAG_LOAD_TYPE_SHOW,
	        	   finish:false,
	        	   res:exp05.g_resources_show
	           }]
},
{
	expName: "氧化与燃烧",
	expTag: TAG_EXP_06,
	expId: 174,
	expTitle:"氧化与燃烧",
	expimage:"#exp/exp06.png",
	chapter:"第三章　第二节",
	startPng:exp06.res_start.start_png,
	expDemo : null,
	run: function(){
		gg.runNext = exp06.startRunNext;
		gg.initExp(this.expId,this.expName,this.expDemo, exp06.teach_flow01);
		pub.ch.gotoStart(this.expTag, this.loadRes[0].res, exp06.res_start.start_p);
	},
	menuItem :[
	           {
	        	   menu:"氧化反应",
	        	   tag:TAG_MENU1 
	           },
	           {
	        	   menu:"燃烧的条件",
	        	   tag:TAG_MENU2 
	           }, {
	        	   menu:"灭火与火灾自救",
	        	   tag:TAG_MENU3 
	           },
	           {
	        	   menu:"化学反应中能量的变化",
	        	   tag:TAG_MENU4 
	           },
	           {
	        	   menu:"原电池实验",
	        	   tag:TAG_MENU5 
	           },
	           {
	        	   menu:"经典例题",
	        	   tag:TAG_MENU6
	           }      

	           ],
	           loadRes:[{
	        	   expTag:TAG_EXP_06,
	        	   type:TAG_LOAD_TYPE_START,
	        	   finish:false,
	        	   res:exp06.g_resources_start
	           },{
	        	   expTag:TAG_EXP_06,
	        	   type:TAG_LOAD_TYPE_RUN,
	        	   finish:false,
	        	   res:exp06.g_resources_run
	           },{
	        	   expTag:TAG_EXP_06,
	        	   type:TAG_LOAD_TYPE_FINISH,
	        	   finish:false,
	        	   res:[]
	           },{
	        	   expTag:TAG_EXP_06,
	        	   type:TAG_LOAD_TYPE_SHOW,
	        	   finish:false,
	        	   res:exp06.g_resources_show

	           }]
},
{
	expName: "二氧化碳",
	expTag: TAG_EXP_07,
	expId: 175,
	expTitle:"二氧化碳",
	expimage:"#exp/exp07.png",
	chapter:"第三章　第四节",
	startPng:exp07.res_start.start_png,
	expDemo : null,
	run: function(){
		gg.runNext = exp07.startRunNext;
		gg.initExp(this.expId,this.expName,this.expDemo, exp07.teach_flow01);
		pub.ch.gotoStart(this.expTag, this.loadRes[0].res, exp07.res_start.start_p);
	},
	menuItem :[
	           {
	        	   menu:"自然界中的二氧化碳",
	        	   tag:TAG_MENU1 
	           },
	           {
	        	   menu:"二氧化碳的性质",
	        	   tag:TAG_MENU2 
	           }, {
	        	   menu:"二氧化碳的制取",
	        	   tag:TAG_MENU3 
	           },
	           {
	        	   menu:"二氧化碳的应用",
	        	   tag:TAG_MENU4 
	           },
	           {
	        	   menu:"简易酸碱灭火器",
	        	   tag:TAG_MENU5 
	           },
	           {
	        	   menu:"经典例题",
	        	   tag:TAG_MENU6
	           }      

	           ],
	           loadRes:[{
	        	   expTag:TAG_EXP_07,
	        	   type:TAG_LOAD_TYPE_START,
	        	   finish:false,
	        	   res:exp07.g_resources_start
	           },{
	        	   expTag:TAG_EXP_07,
	        	   type:TAG_LOAD_TYPE_RUN,
	        	   finish:false,
	        	   res:exp07.g_resources_run
	           },{
	        	   expTag:TAG_EXP_07,
	        	   type:TAG_LOAD_TYPE_FINISH,
	        	   finish:false,
	        	   res:[]
	           },{
	        	   expTag:TAG_EXP_07,
	        	   type:TAG_LOAD_TYPE_SHOW,
	        	   finish:false,
	        	   res:exp07.g_resources_show

	           }]
},
{
	expName: "生物的呼吸和呼吸作用",
	expTag: TAG_EXP_08,
	expId: 176,
	expTitle:"　　生物的\n呼吸和呼吸作用",
	expimage:"#exp/exp08.png",
	chapter:"第三章　第五节",
	startPng:exp08.res_start.start_png,
	expDemo : null,
	run: function(){
		gg.runNext = exp08.startRunNext;
		gg.initExp(this.expId,this.expName,this.expDemo, exp08.teach_flow01);
		pub.ch.gotoStart(this.expTag, this.loadRes[0].res, exp08.res_start.start_p);
	},
	menuItem :[
	           {
	        	   menu:"人体呼吸系统的结构和气体交换",
	        	   tag:TAG_MENU1 
	           },
	           {
	        	   menu:"呼吸系统",
	        	   tag:TAG_MENU2 
	           }, 
	           {
	        	   menu:"膈的升降与呼吸",
	        	   tag:TAG_MENU3 
	           }, 
	           {
	        	   menu:"呼吸作用",
	        	   tag:TAG_MENU4 
	           }, {
	        	   menu:"动物的呼吸作用",
	        	   tag:TAG_MENU5 
	           },
	           {
	        	   menu:"植物的呼吸作用",
	        	   tag:TAG_MENU6
	           },
	           {
	        	   menu:"经典例题",
	        	   tag:TAG_MENU7
	           }      

	           ],
	           loadRes:[{
	        	   expTag:TAG_EXP_08,
	        	   type:TAG_LOAD_TYPE_START,
	        	   finish:false,
	        	   res:exp08.g_resources_start
	           },{
	        	   expTag:TAG_EXP_08,
	        	   type:TAG_LOAD_TYPE_RUN,
	        	   finish:false,
	        	   res:exp08.g_resources_run
	           },{
	        	   expTag:TAG_EXP_08,
	        	   type:TAG_LOAD_TYPE_FINISH,
	        	   finish:false,
	        	   res:[]
	           },{
	        	   expTag:TAG_EXP_08,
	        	   type:TAG_LOAD_TYPE_SHOW,
	        	   finish:false,
	        	   res:exp08.g_resources_show

	           }]
},
{
	expName: "光合作用",
	expTag: TAG_EXP_09,
	expId: 177,
	expTitle:"光合作用",
	expimage:"#exp/exp09.png",
	chapter:"第三章　第六节",
	startPng:exp09.res_start.start_png,
	expDemo : null,
	run: function(){
		gg.runNext = exp09.startRunNext;
		gg.initExp(this.expId,this.expName,this.expDemo, exp09.teach_flow01);
		pub.ch.gotoStart(this.expTag, this.loadRes[0].res, exp09.res_start.start_p);
	},
	menuItem :[
	           {
	        	   menu:"光合作用原理",
	        	   tag:TAG_MENU1 
	           },
	           {
	        	   menu:"光合作用实验",
	        	   tag:TAG_MENU2 
	           }, {
	        	   menu:"光合作用的条件和产物",
	        	   tag:TAG_MENU3 
	           },
	           
	           {
	        	   menu:"植物制造淀粉实验",
	        	   tag:TAG_MENU4 
	           },
	           {
	        	   menu:"光合作用原料",
	        	   tag:TAG_MENU5
	           },     
	           {
	        	   menu:"光合作用和呼吸作用的相互关系",
	        	   tag:TAG_MENU6
	           },
	           {
	        	   menu:"经典例题",
	        	   tag:TAG_MENU7
	           }   
	           ],
	           loadRes:[{
	        	   expTag:TAG_EXP_09,
	        	   type:TAG_LOAD_TYPE_START,
	        	   finish:false,
	        	   res:exp09.g_resources_start
	           },{
	        	   expTag:TAG_EXP_09,
	        	   type:TAG_LOAD_TYPE_RUN,
	        	   finish:false,
	        	   res:exp09.g_resources_run
	           },{
	        	   expTag:TAG_EXP_09,
	        	   type:TAG_LOAD_TYPE_FINISH,
	        	   finish:false,
	        	   res:[]
	           },{
	        	   expTag:TAG_EXP_09,
	        	   type:TAG_LOAD_TYPE_SHOW,
	        	   finish:false,
	        	   res:exp09.g_resources_show

	           }]
},
{
	expName: "土壤的成分" ,
	expTag: TAG_EXP_10,
	expId: 178,
	expTitle:"土壤的成分",
	expimage:"#exp/exp10.png",
	chapterImg:"#chapter/chapter4.png",
	startPng:exp10.res_start.start_png,
	chapter:"第四章　第一节",
	run: function(){
		gg.runNext = exp10.startRunNext;
		gg.initExp(this.expId,this.expName,this.expDemo, exp10.teach_flow01);
		pub.ch.gotoStart(this.expTag, this.loadRes[0].res, exp10.res_start.start_p);
	},
	menuItem :[
	           {
	        	   menu:"土壤中的生命--土壤生物",
	        	   tag:TAG_MENU1 
	           },
	           {
	        	   menu:"土壤中的非生命物质",
	        	   tag:TAG_MENU2 
	           },
	           {
	        	   menu:"从岩石到土壤",
	        	   tag:TAG_MENU3 
	           },
	           {
	        	   menu:"土壤成分分析",
	        	   tag:TAG_MENU4 
	           },
	           {
	        	   menu:"经典例题",
	        	   tag:TAG_MENU5 
	           }   
	           ],
	           loadRes:[{
	        	   expTag:TAG_EXP_01,
	        	   type:TAG_LOAD_TYPE_START,
	        	   finish:false,
	        	   res:exp10.g_resources_start
	           },{
	        	   expTag:TAG_EXP_01,
	        	   type:TAG_LOAD_TYPE_RUN,
	        	   finish:false,
	        	   res:exp10.g_resources_run
	           },{
	        	   expTag:TAG_EXP_01,
	        	   type:TAG_LOAD_TYPE_FINISH,
	        	   finish:false,
	        	   res:[]
	           },{
	        	   expTag:TAG_EXP_01,
	        	   type:TAG_LOAD_TYPE_SHOW,
	        	   finish:false,
	        	   res:exp10.g_resources_show
	           }]
},
{
	expName: "植物的根与物质吸收" ,
	expTag: TAG_EXP_11,
	expId: 179,
	expTitle:"植物的根与物质吸收",
	expimage:"#exp/exp11.png",
	chapterImg:"#chapter/chapter4.png",
	startPng:exp11.res_start.start_png,
	chapter:"第四章　第三节",
	run: function(){
		gg.runNext = exp11.startRunNext;
		gg.initExp(this.expId,this.expName,this.expDemo, exp11.teach_flow01);
		pub.ch.gotoStart(this.expTag, this.loadRes[0].res, exp11.res_start.start_p);
	},
	menuItem :[
	           {
	        	   menu:"植物的根系",
	        	   tag:TAG_MENU1 
	           },
	           {
	        	   menu:"根的吸水和失水",
	        	   tag:TAG_MENU2 
	           },
	           {
	        	   menu:"植物生长需要无机盐",
	        	   tag:TAG_MENU3 
	           },
	           {
	        	   menu:"根的吸水实验",
	        	   tag:TAG_MENU4 
	           },
	           {
	        	   menu:"植物的失水实验",
	        	   tag:TAG_MENU5 
	           },
	           {
	        	   menu:"经典例题",
	        	   tag:TAG_MENU6 
	           }
	           ],
	           loadRes:[{
	        	   expTag:TAG_EXP_11,
	        	   type:TAG_LOAD_TYPE_START,
	        	   finish:false,
	        	   res:exp11.g_resources_start
	           },{
	        	   expTag:TAG_EXP_11,
	        	   type:TAG_LOAD_TYPE_RUN,
	        	   finish:false,
	        	   res:exp11.g_resources_run
	           },{
	        	   expTag:TAG_EXP_11,
	        	   type:TAG_LOAD_TYPE_FINISH,
	        	   finish:false,
	        	   res:[]
	           },{
	        	   expTag:TAG_EXP_11,
	        	   type:TAG_LOAD_TYPE_SHOW,
	        	   finish:false,
	        	   res:exp11.g_resources_show
	           }]
},
{
	expName: "植物的茎与物质运输" ,
	expTag: TAG_EXP_12,
	expId: 180,
	expTitle:"植物的茎与物质运输",
	expimage:"#exp/exp12.png",
	chapterImg:"#chapter/chapter4.png",
	startPng:exp12.res_start.start_png,
	chapter:"第四章　第四节",
	run: function(){
		gg.runNext = exp12.startRunNext;
		gg.initExp(this.expId,this.expName,this.expDemo, exp12.teach_flow01);
		pub.ch.gotoStart(this.expTag, this.loadRes[0].res, exp12.res_start.start_p);
	},
	menuItem :[
	           {
	        	   menu:"茎的结构",
	        	   tag:TAG_MENU1 
	           },
	           {
	        	   menu:"水分和无机盐的运输",
	        	   tag:TAG_MENU2 
	           },
	           {
	        	   menu:"有机物的运输",
	        	   tag:TAG_MENU3 
	           },
	           {
	        	   menu:"水分和无机盐的运输实验",
	        	   tag:TAG_MENU4 
	           },
	           {
	        	   menu:"经典例题",
	        	   tag:TAG_MENU5 
	           }
	           ],
	           loadRes:[{
	        	   expTag:TAG_EXP_12,
	        	   type:TAG_LOAD_TYPE_START,
	        	   finish:false,
	        	   res:exp12.g_resources_start
	           },{
	        	   expTag:TAG_EXP_12,
	        	   type:TAG_LOAD_TYPE_RUN,
	        	   finish:false,
	        	   res:exp12.g_resources_run
	           },{
	        	   expTag:TAG_EXP_12,
	        	   type:TAG_LOAD_TYPE_FINISH,
	        	   finish:false,
	        	   res:[]
	           },{
	        	   expTag:TAG_EXP_12,
	        	   type:TAG_LOAD_TYPE_SHOW,
	        	   finish:false,
	        	   res:exp12.g_resources_show
	           }]
},
{
	expName: "植物的叶与蒸腾作用",
	expTag: TAG_EXP_13,
	expId: 181,
	expTitle:"植物的叶与蒸腾作用",
	expimage:"#exp/exp02.png",
	chapter:"第四章　第五节",
	startPng:res_start12.start_png,
	expDemo:TAG_TYPE_CESHI,
	run: function(){
		gg.runNext = exp13.startRunNext;
		gg.initExp(this.expId,this.expName,this.expDemo, exp13.teach_flow01);
		pub.ch.gotoStart(this.expTag, this.loadRes[0].res, res_start12.start_p);
	},
	menuItem :[
	           {
	        	   menu:"叶的结构",
	        	   tag:TAG_MENU1
	           },	          
	           {
	        	   menu:"蒸腾作用",
	        	   tag:TAG_MENU2
	           },
	           {
	        	   menu:"探究蒸腾作用",
	        	   tag:TAG_MENU3
	           },
	           {
	        	   menu:"课外拓展",
	        	   tag:TAG_MENU5 
	           },
	           {
	        	   menu:"经典例题",
	        	   tag:TAG_MENU6 
	           }	  
	           ],
	           loadRes:[{
	        	   expTag:TAG_EXP_13,
	        	   type:TAG_LOAD_TYPE_START,
	        	   finish:false,
	        	   res:g_resources_start12
	           },{
	        	   expTag:TAG_EXP_13,
	        	   type:TAG_LOAD_TYPE_RUN,
	        	   finish:false,
	        	   res:g_resources_run12
	           },{
	        	   expTag:TAG_EXP_13,
	        	   type:TAG_LOAD_TYPE_FINISH,
	        	   finish:false,
	        	   res:[]
	           },{
	        	   expTag:TAG_EXP_13,
	        	   type:TAG_LOAD_TYPE_SHOW,
	        	   finish:false,
	        	   res:g_resources_show12
	           },{
	        	   expTag:TAG_EXP_13,
	        	   type:TAG_LOAD_TYPE_ABOUT,
	        	   finish:false,
	        	   res:[]
	           },{
	        	   expTag:TAG_EXP_13,
	        	   type:TAG_LOAD_TYPE_TEST,
	        	   finish:false,
	        	   res:[]
	           }]
}
]
