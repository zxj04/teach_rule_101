TAG_LIB_MIN = 30000;

TAG_CANCEL=30081;
TAG_SURE=30082;
TAG_WORD_BG = 30083;
TAG_LABEL = 30084;
TAG_LIB_TIP_CLOSE = 30085;

TAG_LIB_BG=30050;
TAG_LIB_BG1=30051;
TAG_LIB_BG2=30052;
TAG_LIB_BG3=30053;
TAG_LIB_BG4=30054;
TAG_LIB_BG5=30055;
TAG_LIB_BG6=30056;
TAG_LIB_BG7=30057;
TAG_LIB_BG8=30058;
TAG_LIB_BG9=30059;
TAG_LIB_BG10=30060;


TAG_LIB_AMPULLA = 30001;
TAG_LIB_BEAKER=30002;
TAG_LIB_BOTTLE=30003;
TAG_LIB_CONICAL=30004;
TAG_LIB_BOOT=30005;
TAG_LIB_NET=30006;
TAG_LIB_CITIE=30007;
TAG_LIB_DAOXIAN=30008;
TAG_LIB_TESTTUBE=30009;
TAG_LIB_DIANLING=30010;
TAG_LIB_TANPIAN=30011;


TAG_LIB_BULB=30012;
TAG_LIB_JIDIANQI=30013;
TAG_LIB_DIANJI=30014;
TAG_LIB_DIANYUAN=30015;
TAG_LIB_DIANYUAN1=30016;
TAG_LIB_WATERPIPE=30017;

TGA_LIB_GLASS = 30018;
TAG_LIB_NA = 30019;
TAG_LIB_LVQI = 30020;
TAG_LIB_MATCH = 30021;
TAG_LIB_LAMP = 30022;
TAG_LIB_SPOON = 30023;

TAG_LIB_OXYGEN = 30024;
TAG_LIB_AIR = 30025;
TAG_LIB_TONGQI = 30026;
TAG_LIB_INOCULATOR = 30027;
TAG_LIB_CUSPOON = 30028;
TAG_LIB_PBOTTLE = 30029;

TAG_LIB_H2O2 = 30030;
TAG_LIB_MNO2 = 30031;
TAG_LIB_FENYE = 30032;
TAG_LIB_FLASK = 30033;
TAG_LIB_FLUME = 30034;

TAG_LIB_KMNO4 = 30035;
TAG_LIB_IRON = 30036;

TAG_LIB_AMMETER = 30037;
TAG_LIB_ZNCU = 30038;

TAG_LIB_NHCLBEAKER = 30039;
TAG_LIB_NNA2CO3BEAKER = 30040;
TAG_LIB_XIDIJI = 30041;
TAG_LIB_SANJIAOFLASK = 30042;
TAG_LIB_ZHENGFA = 30043;
TAG_LIB_GUOLV = 30045;
TAG_LIB_TURANG = 30046;

TAG_LIB_MACHINE = 30047;
TAG_LIB_PLANT = 30048;
TAG_LIB_PRESSURE = 30049;
TAG_LIB_BOTANY = 30050;
TAG_LIB_TRIPOD = 30051;
TAG_LIB_DROPPER = 30052;
TAG_LIB_GLASS = 30053;
//TAG_LIB_TRIPOD = 50054;


TAG_LIB_MAIMIAO = 30055;
TAG_LIB_CLIPPER = 30056;
TAG_LIB_TWEEZER = 30057;


TAG_LIB_MACHINE=30047;
TAG_LIB_PLANT=30048;
TAG_LIB_PRESSURE=30049;

TAG_LIB_BOTANY=30050;
TAG_LIB_FOIL=30051;
TAG_LIB_NAIL=30052;

TAG_LIB_PLANT1=30058;
TAG_LIB_KNIFE=30059;
TAG_LIB_REDINK=30060;

TAG_LIB_MAGNIFIER=30061;
TAG_LIB_CHANGJING = 30062;
TAG_LIB_HCL = 30063;

libRelArr = [
             {tag:TAG_LIB_MIN, name:""},  
             {tag:TAG_LIB_AMPULLA, name:"集气瓶",img:"#apparatus/ampulla.png"},
             {tag:TAG_LIB_BEAKER,name:"烧杯",img:"#apparatus/beaker.png"},
             {tag:TAG_LIB_BOTTLE,name:"塑料瓶",img:"#apparatus/bottle.png"},
             {tag:TAG_LIB_CONICAL,name:"锥形瓶",img:"#apparatus/conical.png"},
             {tag:TAG_LIB_BOOT,name:"开关",img:"#apparatus/boot.png"},
             {tag:TAG_LIB_NET, name:"石棉网",img:"#apparatus/net.png"},
             {tag:TAG_LIB_CITIE,name:"磁铁",img:"#apparatus/citie.png"},
             {tag:TAG_LIB_DAOXIAN,name:"导线",img:"#apparatus/daoxian.png"},
             {tag:TAG_LIB_TESTTUBE,name:"试管",img:"#apparatus/testTube.png"},
             {tag:TAG_LIB_DIANLING,name:"电铃",img:"#apparatus/dianling.png"},
             {tag:TAG_LIB_TANPIAN,name:"弹片",img:"#apparatus/tanpian.png"},

             {tag:TAG_LIB_BULB,name:"电灯泡",img:"#apparatus/dengpao.png"},
             {tag:TAG_LIB_JIDIANQI,name:"电磁继电器",img:"#apparatus/dianci.png"},
             {tag:TAG_LIB_DIANJI, name:"电动机",img:"#apparatus/dianji.png"},
             {tag:TAG_LIB_DIANYUAN,name:"电源",img:"#apparatus/dianyuan.png"},
             {tag:TAG_LIB_DIANYUAN1,name:"电源",img:"#apparatus/dianyuan1.png"},
             {tag:TAG_LIB_WATERPIPE,name:"水管",img:"#apparatus/waterpipe.png"},

             {tag:TGA_LIB_GLASS,name:"玻璃片",img:"#apparatus/libglass.png"},
             {tag:TAG_LIB_NA,name:"钠",img:"#apparatus/libna.png"},
             {tag:TAG_LIB_LVQI,name:"氯气",img:"#apparatus/liblvqi.png"},
             {tag:TAG_LIB_MATCH,name:"火柴",img:"#apparatus/libmatch.png"},
             {tag:TAG_LIB_LAMP,name:"酒精灯",img:"#apparatus/liblamp.png"},
             {tag:TAG_LIB_SPOON,name:"玻璃燃烧匙",img:"#apparatus/libspoon.png"},
             {tag:TAG_LIB_OXYGEN,name:"氧气",img:"#apparatus/liboxygen.png"},
             {tag:TAG_LIB_AIR,name:"空气",img:"#apparatus/libair.png"},
             {tag:TAG_LIB_TONGQI,name:"通气装置",img:"#apparatus/libtongqi.png"},
             {tag:TAG_LIB_INOCULATOR,name:"注射器",img:"#apparatus/libinoculator.png"},
             {tag:TAG_LIB_CUSPOON,name:"燃烧匙",img:"#apparatus/libcuspoon.png"},
             {tag:TAG_LIB_PBOTTLE,name:"红磷",img:"#apparatus/libbottle.png"},

             {tag:TAG_LIB_H2O2,name:"过氧化氢",img:"#apparatus/libH2O2.png"},
             {tag:TAG_LIB_MNO2,name:"二氧化锰",img:"#apparatus/libMnO2.png"},
             {tag:TAG_LIB_FENYE,name:"分液漏斗",img:"#apparatus/libfenye.png"},

             {tag:TAG_LIB_FLASK,name:"烧瓶",img:"#apparatus/libflask.png"},
             {tag:TAG_LIB_FLUME,name:"水槽",img:"#apparatus/libflume.png"},

             {tag:TAG_LIB_KMNO4,name:"高锰酸钾",img:"#apparatus/libKMnO4.png"},
             {tag:TAG_LIB_IRON,name:"铁架台",img:"#apparatus/libiron.png"},

             {tag:TAG_LIB_AMMETER,name:"灵敏电流表",img:"#apparatus/libammeter.png"},
             {tag:TAG_LIB_ZNCU,name:"铜锌装置",img:"#apparatus/libzncu.png"},


             {tag:TAG_LIB_NHCLBEAKER,name:"浓盐酸",img:"#apparatus/libHclbealer.png"},
             {tag:TAG_LIB_NNA2CO3BEAKER,name:"饱和碳酸钠",img:"#apparatus/libNA2CO3beaker.png"},

             {tag:TAG_LIB_XIDIJI,name:"洗涤剂",img:"#apparatus/libxidiji.png"}, 
             {tag:TAG_LIB_SANJIAOFLASK,name:"三角烧瓶",img:"#apparatus/libsanjiaoflask.png"},

             {tag:TAG_LIB_ZHENGFA,name:"蒸发装置",img:"#apparatus/libzhengfa.png"},
             {tag:TAG_LIB_GUOLV,name:"过滤装置",img:"#apparatus/libguolv.png"},
             {tag:TAG_LIB_TURANG,name:"土壤",img:"#apparatus/libturang.png"},


             {tag:TAG_LIB_MAIMIAO,name:"麦苗",img:"#apparatus/libmaimiao.png"},
             {tag:TAG_LIB_CLIPPER,name:"剪刀",img:"#apparatus/libclipper.png"},


             {tag:TAG_LIB_MACHINE,name:"铁架台",img:"#apparatus/machine.png"},
             {tag:TAG_LIB_PLANT,name:"植物",img:"#apparatus/plant1.png"},
             {tag:TAG_LIB_PRESSURE,name:"压力计",img:"#apparatus/pressure.png"},


             {tag:TAG_LIB_BOTANY,name:"盆栽",img:"#apparatus/botany.png"},
             {tag:TAG_LIB_DROPPER,name:"胶头滴管",img:"#apparatus/dropper.png"},
             {tag:TAG_LIB_TRIPOD,name:"三脚架",img:"#apparatus/Tripod.png"},
             {tag:TAG_LIB_GLASS,name:"玻璃片",img:"#apparatus/glass.png"},
             {tag:TAG_LIB_TWEEZER,name:"镊子",img:"#apparatus/Tweezers.png"},

             {tag:TAG_LIB_PLANT1,name:"植株",img:"#apparatus/plant.png"},
             {tag:TAG_LIB_KNIFE,name:"刀子",img:"#apparatus/knife.png"},
             {tag:TAG_LIB_REDINK,name:"红墨水",img:"#apparatus/redInk.png"},
             {tag:TAG_LIB_MAGNIFIER,name:"放大镜",img:"#apparatus/Magnifier.png"},
             {tag:TAG_LIB_CHANGJING,name:"长颈漏斗",img:"#apparatus/libchangjing.png"},
             {tag:TAG_LIB_HCL,name:"稀盐酸",img:"#apparatus/libhcl.png"},
             ];

