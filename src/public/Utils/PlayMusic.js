var PlayMusic = Music.extend({
	bg_flag:false,
	ctor:function(){
		this.bg_flag = false;
		this.effFlag = false;
	},
	clever:function(){
		this.playEffect(res_public.ok_mp3);
	},
	error:function(){
		this.playEffect(res_public.error_mp3);
	},
	playBg:function(){
		// this.playMusic(res_play.home);
	},
	ka:function(){
		this.playEffect(res_public.ka_mp3);
	},
	cPause:function(){
		this.ban();
	},
	cResume:function(){
		this.lift();
	},
	isStop:function(){
		return !this.effFlag;
	},
	clock:function(){
		this.playEffect(res_public.clock_mp3, true);
	},
	stopClock:function(){
		cc.audioEngine.stopAllEffects();
	},
	over:function(){
//		this.playEffect(res_public.over_mp3, true);
	}
});