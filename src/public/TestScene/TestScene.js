var TestLayer = cc.Layer.extend({
	backgroundLayer : null,
	mainLayar : null,
	ctor:function (p) {
		this._super();
		p.addChild(this);
		this.initFrames();
		this.loadBackground();
		this.loadMainLayer();
	},
	initFrames : function(){
		cc.spriteFrameCache.addSpriteFrames(res_public_test.test_p);
	},
	loadBackground : function(){
		this.backgroundLayer = new TestBackgroundLayer();
		this.addChild(this.backgroundLayer);
	},
	loadMainLayer : function(){
		this.mainLayar = new TestMainLayer(this);
	}
});

var TestScene = PScene.extend({
	url: null,
	ctor:function(url){
		this._super();
		this.url = url;
	},
	onEnter:function () {
		this._super();
		var layer = new TestLayer(this);
	}
});
