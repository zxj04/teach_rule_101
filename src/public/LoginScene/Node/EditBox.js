/**

 * 自定义的输入框

 */
EditBox = cc.Node.extend({
	hit:/**
	 * 提示文字
	 */
		null,
		ctor: function (p, placeholder, fontName, fontSize) {
			this._super();
			p.addChild(this);
			this.initBackGround();
			this.initTextField(placeholder, fontName, fontSize);
			this.initLight();
			this.addListener();
		},
		initBackGround:/**
		 * 初始化背景
		 */
			function(){
			this.backGround = new cc.Sprite(res_public.login_edit);
			this.backGround.setAnchorPoint(0, 0.5);
			this.backGround.setScale(0.8,1);
			this.addChild(this.backGround);
		},
		initTextField:/**
		 * 初始化输入框
		 */
			function(placeholder, fontName, fontSize){
			this.tf = new ccui.TextField(placeholder, fontName, fontSize);
			this.tf.setPlaceHolderColor(cc.color(255, 255, 255, 255));
			this.tf.setAnchorPoint(0, 0.5);
			this.tf.setMaxLengthEnabled(true);
			// 屏蔽默认点击,使用自定义点击

			this.tf.setTouchEnabled(false);
			this.tf.fontName = "Marker Felt";
			this.tf.fontSize = 30;
			this.tf.setTextColor(cc.color(0, 0, 0, 255));//修改文字颜色
			this.tf.setPosition(0,10);//修改文字位于白色线条上方的距离
			this.addChild(this.tf);
		},
		initLight:/**
		 * 初始化光标
		 */
			function(){
			this.light = new cc.LabelTTF("|",gg.fontName, 25);
			this.light.setColor(cc.color(0,0,0));
			this.light.setAnchorPoint(0, 0.5);
			this.addChild(this.light);
			var seq = cc.sequence(cc.fadeIn(0.3),cc.delayTime(0.3),cc.fadeOut(0.3));
			var forever = cc.repeatForever(seq);
			this.light.runAction(forever);
			this.light.setPosition(0, 10);//修改光标位置
			this.hiddenLight();
		},
		setMaxLength:/**
		 * 设置最大长度
		 * 
		 * @param length
		 */
			function(length){
			this.tf.setMaxLength(length);
		},
		setHit:/**
		 * 设置提示
		 * 
		 * @param strHit
		 */
			function(strHit){
			this.hit = strHit;
			this.tf.setPlaceHolder(this.hit)
		},
		showHit:/**
		 * 展示提示
		 */
			function(){
			this.tf.setPlaceHolder(this.hit)
		},
		hiddenHit:/**
		 * 隐藏提示
		 */
			function(){
			this.tf.setPlaceHolder("")
		},
		setPasswordEnabled:/**
		 * 是否作为密码展示
		 * 
		 * @param enable
		 */
			function(enable){
			this.tf.setPasswordEnabled(enable);
		},
		setString:/**
		 * 设置文字
		 * 
		 * @param str
		 */
			function(str){
			this.tf.setString(str);
		},
		getString:function(){
			return this.tf.getString();
		},
		setPos:/**
		 * 设置位置
		 * 
		 * @param width
		 * @param height
		 */
			function(width, height){
			this.x = width;
			this.y = height;
		},
		addListener:function(){
			// TextField 事件

			this.tf.addEventListener(this.textFieldEvent, this);
			// 点击事件

			cc.eventManager.addListener(cc.EventListener.create({
				event: cc.EventListener.TOUCH_ONE_BY_ONE,
				onTouchBegan: this.onTouchBegan.bind(this)
			}), this.backGround);
		},
		onTouchBegan:function(touch, event){
			var pos = cc.p(touch.getLocationX(),touch.getLocationY());
			var target = event.getCurrentTarget();
			if(cc.rectContainsPoint(target.getBoundingBoxToWorld(),pos)){
				setTimeout(function(){
					cc.log("获取焦点");
					this.tf.getVirtualRenderer().attachWithIME();
				}.bind(this), 0);
				this.showLight();
				return true;
			} else {
				setTimeout(function(){
					cc.log("失去焦点");
					this.tf.getVirtualRenderer().detachWithIME();
					this.showHit();
				}.bind(this), 0);
				this.hiddenLight();
				return false;
			}
		},
		showLight:/**
		 * 出现光标闪烁
		 */
			function(){
			this.light.setVisible(true);
			var sv = this.tf.getString();
			if(!!sv && sv.length > 0){
				this.light.setPositionX(this.tf.width - this.light.width * 0.5 + 1);
			} else {
				// 存在焦点

				this.light.setPositionX(this.light.width * 0.5);
				this.hiddenHit();
			}	
		},
		hiddenLight:/**
		 * 隐藏光标
		 */
			function(){
			this.light.setVisible(false);
		},
		textFieldEvent:/**
		 * 各种事件
		 */
			function(textField, type){
			switch (type) {
			case ccui.TextField.EVENT_ATTACH_WITH_IME:
				cc.log("attach with IME");
				this.showLight();
				break;
			case ccui.TextField.EVENT_DETACH_WITH_IME:
				cc.log("detach with IME");
				this.hiddenLight();
				break;
			case ccui.TextField.EVENT_INSERT_TEXT:
				cc.log("insert words");
				this.showLight();
				break;
			case ccui.TextField.EVENT_DELETE_BACKWARD:
				cc.log("delete word");
				this.showLight();
				break;
			default:
				break;
			}
			cc.log(type);
		}
})