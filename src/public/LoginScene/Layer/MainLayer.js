var LoginMainLayer = cc.Layer.extend({
	lead:null,
	real:null,
	ctor:function () {
		this._super();
		this.loadTip();
		this.loadEditBox();
		this.loadLoginButton();
		this.loadKey();
		this.loadTitle();//登录界面最下方文字提示
		return true;
	},
	loadKey:function(){
		// 监听回车键
		var self = this;
		if ('keyboard' in cc.sys.capabilities){
			var listener_key = cc.EventListener.create({
				event: cc.EventListener.KEYBOARD,
				onKeyReleased: function (key, event) {
					cc.log( "Key up:" + key);
					if(key == cc.KEY.enter){
						self.postLogin();
					}
				}
			});
			cc.eventManager.addListener(listener_key, this);
		}
	},
	loadTip:function(){
		this._tipL = new cc.LabelTTF("", gg.fontName, 25);
		this._tipL.setPosition(gg.c_width, gg.height*0.5);
		this._tipL.setColor(cc.color(0, 0, 0, 255));
		this.addChild(this._tipL, 11);
	},
	loadEditBox:function (){
		if(cc.sys.os == "Windows" && cc.sys.isNative){
			var bg1=new cc.Sprite("#login_edit.png");
			bg1.setAnchorPoint(0, 0);
			this.addChild(bg1);
			bg1.setPosition(356,gg.height-265);
			var _workCode = new EditBox(this);
			_workCode.setMaxLength(11);
			_workCode.setHit("       点击输入注册账号");
			_workCode.setAnchorPoint(0, 0);
			_workCode.setPos(430,gg.height-245);
			_workCode.setTag(TAG_XH);
			_workCode.setLocalZOrder(gg.d_z_index);
		} else {
			var bg1=new cc.Sprite("#login_edit.png");
			bg1.setAnchorPoint(0, 0);
			this.addChild(bg1);
			bg1.setPosition(356,gg.height-265);
			var _workCode = new cc.EditBox(
					cc.size(480, 40)
					,new cc.Scale9Sprite("#login_edit1.png")//修改背景图
			);
			_workCode.setAnchorPoint(0, 0);
			_workCode.setPosition(cc.p(430,gg.height-255));
			_workCode.setPlaceHolder("                  请输入注册账号"); 
			_workCode._placeholderColor=cc.color(255, 255, 255, 255);//修改提示语颜色
			_workCode.setPlaceholderFont("微软雅黑",32);//修改提示语字体样式
			_workCode.setFontColor(cc.color(0, 0, 0, 255));
			_workCode.setMaxLength(11);
			_workCode.setFontSize(30);
			_workCode.setInputMode(cc.EDITBOX_INPUT_MODE_SINGLELINE);
			_workCode.setPlaceholderFontSize(25);
			this.addChild(_workCode, gg.d_z_index, TAG_XH);
		}
		var account = new cc.Sprite("#login_01.png");
		account.setAnchorPoint(0,1);
		this.addChild(account, gg.d_z_index);
		account.setPosition(cc.p(374,gg.height-204));

		if(cc.sys.os == "Windows" && cc.sys.isNative){
			var bg2=new cc.Sprite("#login_edit.png");
			this.addChild(bg2);
			bg2.setAnchorPoint(0, 0);
			bg2.setPosition(cc.p(356,gg.height-356));
			var _passwd = new EditBox(this);
			_passwd.setMaxLength(11);
			_passwd.setHit("         点击输入密码");
			_passwd.setAnchorPoint(0,0);
			_passwd.setPos(430,gg.height-336);
			_passwd.setTag(TAG_MM);
			_passwd.setLocalZOrder(gg.d_z_index);
			_passwd.setPasswordEnabled(true);
		} else {
			var bg2=new cc.Sprite("#login_edit.png");
			this.addChild(bg2);
			bg2.setAnchorPoint(0, 0);
			bg2.setPosition(cc.p(356,gg.height-356))
			var _passwd = new cc.EditBox(
					cc.size(480, 40), 
					new cc.Scale9Sprite("#login_edit1.png"));//修改背景图
			_passwd.setAnchorPoint(0,0);
			_passwd.setPosition(430,gg.height-346);
			_passwd.setPlaceHolder("                  请输入登录密码"); 
			_passwd._placeholderColor=cc.color(255, 255, 255, 255);//修改提示语颜色
			_passwd.setPlaceholderFont("微软雅黑",32);//修改提示语字体样式
			_passwd.setInputFlag(cc.EDITBOX_INPUT_FLAG_PASSWORD);
			_passwd.setFontColor(cc.color(0, 0, 0, 255));
			_passwd.setFontSize(30);
			_passwd.setInputMode(cc.EDITBOX_INPUT_MODE_SINGLELINE);
			_passwd.setPlaceholderFontSize(25);
			this.addChild(_passwd, gg.d_z_index, TAG_MM);
		}

		var passwd = new cc.Sprite("#login_02.png");
		passwd.setAnchorPoint(0,1);
		passwd.setPosition(cc.p(374,gg.height-295));
		this.addChild(passwd, gg.d_z_index);

		var userName = cc.sys.localStorage.getItem("userName");
		var password = cc.sys.localStorage.getItem("password");
		if(!!userName){
			_workCode.setString(userName);
			_passwd.setString(password);
		}
	},
	loadLoginButton : function(){
		var _loginItem = new cc.MenuItemImage(
				"#finish_login.png","#finish_login.png",
				this.eventMenuCallback,this);
		_loginItem.setAnchorPoint(0,1);
		_loginItem.setPosition(358, gg.height-433);
		
		var menu = new cc.Menu();
		menu.addChild(_loginItem, 1, TAG_LOGIN);
		menu.setPosition(0, 0);
		this.addChild(menu, 1);
	},
	login: function(url){
		var xhr = cc.loader.getXMLHttpRequest();
		xhr.open("GET", url);
		result_layer = this;
		xhr.onreadystatechange = function () {
			if (xhr.readyState == 4 && (xhr.status >= 200 && xhr.status <= 207)) {
				var result = xhr.responseText;
				var jObject = JSON.parse(result);
				login_layer.loginResult(jObject);
			}
		};
		xhr.send();
	},
	logining:false,
	eventMenuCallback: function(pSender) {
		if(this.logining){
			return;
		}
		switch (pSender.getTag()){
		case TAG_LOGIN:
			this.postLogin();
			break;
		default:
			break;
		}
	},
	postLogin:function(){
		// 登录
		var workCode = this.getChildByTag(TAG_XH).getString();
		var passwd = this.getChildByTag(TAG_MM).getString();
		cc.sys.localStorage.setItem("userName", workCode);
		cc.sys.localStorage.setItem("password", passwd);
		if($.checkNull(workCode) || $.checkNull(passwd)){
			this._tipL.setString("手机号或密码不能为空！");
			return;
		}
		else if(workCode==1&&passwd==1){
			pub.ch.howToGo(OP_TYPE_SELECT);
			return;
		}
		cc.log("进入登录" + "学号" + workCode+ "    密码" + passwd);
		this._tipL.setString("登录中...");
		this.logining = true;
		net.login(workCode, passwd, this.loginResult, this);
	},
	loginResult: function(json){
		this.logining = false;
		cc.log(json);
		if(json == null || json.result == 1){
			this._tipL.setString("手机号或密码错误请重新输入！");
		} else {
			uu.userId = json.object.userId;
			pub.ch.howToGo(OP_TYPE_SELECT);
		}
	},
	loadTitle:function(){
		var label=new cc.LabelTTF("账号注册、密码重置等操作请至赛学霸教学大厅完成","微软雅黑",30);
		label.setAnchorPoint(0,0);
		label.setPosition(312,64);
		label.setColor(cc.color(117,120,128,255));  
		this.addChild(label);
	},
	right:function (standard, target, margin){
		if(!margin){
			margin = 5;
		}
		var sap = standard.getAnchorPoint();
		var ap = target.getAnchorPoint();
		// 标准物的x - 标准物的宽度 * 缩放 * 锚点 + 本身的宽度 * 缩放 * (1-锚点x) - 所需间隔
		var x = standard.x - standard.width * standard.getScaleX() * sap.x  - target.width * (1-ap.x) * target.getScaleX() - margin;
		target.setPosition(x, standard.y);
	}
});