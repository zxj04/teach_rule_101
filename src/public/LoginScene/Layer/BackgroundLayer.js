var LoginBackgroundLayer = cc.Layer.extend({
    ctor:function () {
        this._super();
        this.loadBg();
        this.loadLogo();
        return true;
    },
    loadBg : function(){
    	var login_bg = new cc.Sprite("#login_bg.png");
    	this.addChild(login_bg);
    	login_bg.setPosition(gg.c_p);
    },
    loadLogo : function(){
    	var logo1 = new cc.Sprite(res_public.logo1_png);
    	logo1.setAnchorPoint(0,1);
    	logo1.setPosition(470, 700);
    	this.addChild(logo1, 10);
    }
});