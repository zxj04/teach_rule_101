var LabelMenu = cc.Node.extend({
	margin: 0,
	menu:null,
	flash_name:null,
	ctor: function (parent,labelname,normalImage, callback, back) {
		this._super();
		parent.addChild(this);
		this.check = false;
		this.loadbg(normalImage);
		this.loadlabel(labelname);

		this.listener();
		this.callback = callback;
		if(back == null){
			this.back = this.p;
		} else {
			this.back = back;
		}

	},
	loadlabel:function(name){
		var label =new cc.LabelTTF(name,gg.fontName,gg.menufont);
		label.setAnchorPoint(0,0.5);
		label.setColor(cc.color(255,255,255, 250));
		label.setPosition(120,this.bg.height/2 + 4);
		this.addChild(label,5);
	},

	loadbg:function(bgname){
		this.bg = new cc.Sprite(bgname);
		this.bg.setPosition(this.bg.width / 2,this.bg.height / 2);
		this.addChild(this.bg);
		//this.bg.setVisible(false);
		this.bg.setOpacity(0);
	},
	checkClick:function(pos){
		// 判断触碰 getBoundingBoxToWorld
		if(cc.rectContainsPoint(
				this.getBoundingBoxToWorld(),pos)){
			return true;
		} else {
			return false;
		}
	},
	onTouchBegan:function(touch, event){
		if(!this.getParent().isVisible()){
			return false;
		}
		if( gg.synch_l||!this.isVisible()){
			return false;
		}
		var target = event.getCurrentTarget();
		var pos = cc.p(touch.getLocationX(),touch.getLocationY());
		// 被点击则，更换点击图片
		if(target.checkClick(pos)){
			this.hover = true;
			//this.bg.setVisible(true);
			this.bg.setOpacity(255);
		}
		return true;
	},
	onTouchEnded:function(touch, event){
		var target = event.getCurrentTarget();
		var pos = cc.p(touch.getLocationX(),touch.getLocationY());
		if(this.hover
				&&target.checkClick(pos)){
			// 点击结束，更换常态图片
			this.hover = false;
			//this.bg.setVisible(false);
			this.bg.setOpacity(0);
			if(target.callback != null){
				// 有回调函数，则调用回调函数
				target.callback.call(target.back,target);
			}
		}else{
			this.hover = false;
			//this.bg.setVisible(false);
			this.bg.setOpacity(0);
		}
	},
	listener:function(){
		var listener_touch = cc.EventListener.create({
			event: cc.EventListener.TOUCH_ONE_BY_ONE,
			swallowTouches: false,
			hover: false,
			onTouchBegan:this.onTouchBegan.bind(this),
			onTouchEnded:this.onTouchEnded.bind(this)});
		cc.eventManager.addListener(listener_touch, this);
	},
	left:function (standard, margin){
		margin = 180;
		if(margin != null){
			this.margin = margin;
		}
		var sap = standard.getAnchorPoint();
		var ap = this.getAnchorPoint();
		// 标准物的x - 标准物的宽度 * 缩放 * 锚点 + 本身的宽度 * 缩放 * (1-锚点x) - 所需间隔
		var x = standard.x - standard.width * standard.getScaleX() * sap.x  - this.width * (1-ap.x) * this.getScaleX() - this.margin;
		this.setPosition(x, standard.y);
	},	
	right:function (standard, margin){
		margin = 180;
		if(margin != null){
			this.margin = margin;
		}
		var sap = standard.getAnchorPoint();
		var ap = this.getAnchorPoint();
		// 左边的x + 左边的宽度 * 缩放 * 锚点 + 本身的宽度 * 缩放 * 锚点
		var x = standard.x + standard.width * standard.getScaleX() * sap.x  + this.width * ap.x * this.getScaleX() + this.margin;
		this.setPosition(x, standard.y);
	},
	down:function (standard, margin){
		margin = 201;
		if(margin != null){
			this.margin = margin;
		}
		var sap = standard.getAnchorPoint();
		var ap = this.getAnchorPoint();
		// 标准物的y - 标准物的高度 * 缩放 * 锚点y - 本身的高度 * 缩放 * (1-锚点y) - 所需间隔
		var y = standard.y - standard.height * standard.getScaleY() * sap.y  - this.height * (1-ap.y) * this.getScaleY() - this.margin;
		this.setPosition(standard.x, y);
	},	
})