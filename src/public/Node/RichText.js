pub.RichText = cc.Node.extend({
	ctor:function(parent){
		this._super();
		parent.addChild(this);
		this.init();
	},
	init:function(){
		this.richText = new ccui.RichText();
		this.richText.width = 300;
		this.richText.height = 200;
		this.fontName = gg.fontName;
		this.fontSize = gg.fontSize;
		this.addChild(this.richText);
	},
	setAPoint:function(x, y){
		this.richText.setAnchorPoint(cc.p(x, y));
	},
	refreshRect:function(){
		// 刷新大小
	},
	append:function(str, color){
		var re = new ccui.RichElementText(1, color, 255, str, this.fontName, this.fontSize);
		this.richText.pushBackElement(re);
		return this;
	}
});