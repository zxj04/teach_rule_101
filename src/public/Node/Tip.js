Tip = cc.LabelTTF.extend({
	fSize:0,
	d_width:null,
	ctor:function (text, width, fontName,fontSize) {
		this.d_width = width;
		if(fontName == null){
			fontName = gg.fontName;
		}
		if(fontSize == null){
			this.fSize = gg.fontSize;
		} else {
			this.fSize = fontSize;
		}
		text = $.format(text, this.d_width, this.fSize);
		this._super(text, fontName, this.fSize);
		this.setAnchorPoint(0, 1);
//		this.setColor(cc.color(0,0,0,0));
	},
	doTip:/**
	 * 更改提示
	 * 
	 * @param text
	 */
		function(text){
		text = $.format(text, this.d_width, this.fSize);
		this.setString(gg.flow.step+" ) " + text);
	}
});

TipFrame = cc.Layer.extend({
	cur : 0,
	tip : [],
	lineMarign:0,
	posX: 0,
	posY: 0,
	open_flag: false,
	tipLayer:null,
	cell:null,
	ctor:function (parent,fileName) {
		this._super();
		parent.addChild(this,10001);
		this.setPosition(0, gg.height);
		this.init(fileName);
		var tipRunBg = new cc.LayerColor(cc.color(0, 0, 0, 200),1280,768);
		parent.addChild(tipRunBg,100, TAG_TIPRUNBG);
		tipRunBg.setOpacity(0);
	},
	init:function (fileName){				
		var bg = new cc.Sprite("#button/tip_frame_bg.png");
		bg.setPosition(cc.p(gg.width * 0.5 ,gg.height - 55 - bg.height * 0.5));
		this.addChild(bg,2);

		var frame = new cc.Sprite(fileName);
		frame.setPosition(gg.width * 0.5 ,gg.height - 40 - frame.height * 0.5);
		this.addChild(frame,3);

		var close = new ButtonScale(this,"#button/tip_frame_close.png",this.callback);
		close.setPosition(cc.p(gg.width - 108 - close.width*0.5 , gg.height - 60 - close.height*0.5));
		close.setLocalZOrder(3);
		close.setTag(TAG_TIP_CLOSE);

		//增加遮罩
		var stencil = new cc.DrawNode()
		stencil.drawRect(cc.p(280,190),cc.p(1024,524),cc.color(248,232,176,0),0,cc.color(248,232,176,0));
		var clipping = new cc.ClippingNode(stencil);
//		clipping.setInverted(true); 
		this.addChild(clipping, 20,TAG_CLIPPING);
		clipping.setStencil(stencil);
		clipping.stencil = stencil;

		this.node = new cc.Node();
		clipping.addChild(this.node);
		this.posX = 352;
		this.posY = gg.height-244;
		this.node.setPosition(this.posX, this.posY);
		var prev = null;
		var mg = 25;
		this.cur = 0;
		this.tip = [];
		for(var i = 0;i< gg.teach_flow.length - 1; i++){
			if(gg.teach_flow[i].hidden){
				// 不显示的条目 qiumy 20151124
				continue;
			}
			this.middle = new Label(this.node, i - -1+" )",this.callback,this);
			this.middle.setAnchorPoint(1,1);
			this.middle.setColor(cc.color(205,157,100));
			this.middle.setFontSize(36);
			
		
			if(prev != null){
				//存在换行(一行高度47)
				var line =tip.height / this.middle.height;
				if(line >1 && line<=2){					
					this.middle.down(prev, tip.height*0.5)
				}else if(line > 2){
					this.middle.down(prev, tip.height*0.75)
				}else{
					this.middle.down(prev, 0)
				}
			}
			prev = this.middle;

			var text = $.format(gg.teach_flow[i].tip, 630,36);
			tip =  new Label(this.node,text,this.callback,this);
			tip.setFontSize(36);
			tip.setAnchorPoint(0, 1);
			tip.right(this.middle,mg);
			this.tip[i] = tip;
			if(this.cell == null){
				this.cell = tip.height + this.lineMarign;
			}			
		}
	},

	open:function (){
		ll.tip.tipItem.setEnable(false);
		ll.tip.backItem.setEnable(false);
		if(ll.tool !=null){
			ll.tool.lib.setEnable(false);
		}	
		this.stopAllActions();
		var tipRunBg = this.getParent().getChildByTag(TAG_TIPRUNBG);
		tipRunBg.runAction(cc.fadeTo(0.4, 200));		
		var move = cc.moveTo(0.3,cc.p(0, 0));
		var move2 = cc.moveTo(0.1,cc.p(0, 50));
		var move3 = cc.moveTo(0.1,cc.p(0, 0));
		var sequence = cc.sequence(move, move2, move3);
		this.runAction(sequence);
		AngelListener.setEnable(false);
		this.open_flag = true;
	},
	close:function (){
		ll.tip.tipItem.setEnable(true);
		ll.tip.backItem.setEnable(true);
		if(ll.tool !=null){
			ll.tool.lib.setEnable(true);
		}		
		this.stopAllActions();
		var tipRunBg = this.getParent().getChildByTag(TAG_TIPRUNBG);
		tipRunBg.runAction(cc.fadeTo(0.4, 0));
		var move = cc.moveTo(0.3,cc.p(0, gg.height));
		var sequence = cc.sequence(move);
		this.runAction(sequence);
		AngelListener.setEnable(true);
		this.open_flag = false;
	},
	libSetEnable:function(next){
		if(ll.tool !=null){
			// 增加空指针判断 20151124 qiumy
			var lib2 = ll.run.getChildByTag(TAG_LIB);
			if(lib2){
				var cancel = lib2.getChildByTag(TAG_CANCEL);
				var sure = lib2.getChildByTag(TAG_SURE);	
				cancel.setEnable(next);
				sure.setEnable(next);
			}
			var lib = ll.tool.getChildByTag(TAG_BUTTON_LIB);
			lib.setEnable(next);
		}	
		
	},
	isOpen:function (){
		return this.open_flag;
	},
	up:function (){
		if(this.cur <= 0){
			return;
		}
		var curTip = this.tip[--this.cur];
		this.node.runAction(cc.moveBy(0.2,cc.p(0, - curTip.height - this.lineMarign)));
	},
	down:function (){
		if(this.cur >= this.tip.length - 1){
			return;
		}
		var curTip = this.tip[this.cur++];
		this.node.runAction(cc.moveBy(0.2,cc.p(0, curTip.height + this.lineMarign)));
	},
	local:function(step){
		var last = this.cur;
		this.cur = step - 1;
		if(this.cur < 0){
			this.cur = 0;
		}
		var result = 0;
		if(this.cur == last){
			return;
		} else if(this.cur > last){
			for(var i = last; i < this.cur; i ++){
				result += this.tip[i].height;
				result += this.lineMarign;
			}
		} else {
			for(var i = this.cur; i < last; i ++){
				result += this.tip[i].height;
				result += this.lineMarign;
			}
			result = -result;
		}
		var pos = this.node.getPosition();
		this.node.setPosition(pos.x,pos.y + result);
	},
	refresh:function (){
		for(var i=0;i<this.tip.length;i++){
			if(gg.teach_flow[i].cur){					
				this.tip[i].setColor(cc.color(137,64,0));	
				this.middle.setColor(cc.color(137,64,0));
			} else {
				this.tip[i].setColor(cc.color(205,157,100));
				this.middle.setColor(cc.color(205,157,100));	
			}
		}	
	},
	callback:function(p){
		switch (p.getTag()) {
		case TAG_TIP_CLOSE:
			if(this.isOpen()){
				cc.log("关闭提示");
				gg.synch_l = false;
				this.close();
				this.libSetEnable(true);
			}
			break;
		default:
			break;
		}

	},
	loadSlide:/**
	 * 滑动
	 */
		function(){
		var listener_touch = cc.EventListener.create({
			event: cc.EventListener.TOUCH_ONE_BY_ONE,
			swallowTouches: false,
			hover: false,
			onTouchBegan:this.onTouchBegan,
			onTouchMoved:this.onTouchMoved,
			onTouchEnded:this.onTouchEnded});
		cc.eventManager.addListener(listener_touch, this);
		if ('mouse' in cc.sys.capabilities){
			var listener_mouse = cc.EventListener.create({
				event: cc.EventListener.MOUSE,
				swallowTouches: false,
				onMouseScroll:UpperLowerSliding.onMouseScroll
			});
			cc.eventManager.addListener(listener_mouse, this);
		}
	},
	onTouchBegan: function(touch, event){
		var target = event.getCurrentTarget();
		var pos = cc.p(touch.getLocationX(),touch.getLocationY());
		if(cc.rectContainsPoint(
				target.getBoundingBoxToWorld(),pos)){
			this.click = true;
			return true;
		}
		return false;
	},
	onTouchMoved: function(touch, event){
		if(this.click){
			var target = event.getCurrentTarget();
			var pos = cc.p(touch.getLocationX(),touch.getLocationY());
			if(this.last == null){
				this.last = pos;
			} else {
				var margin = pos.y - this.last.y;
				var t = margin > 0 ? margin / target.cell : -margin / target.cell;
				if(t >= 1 && margin > 0){
					for(var i = 0; i < t; i++){
						target.down();
					}
					this.last = pos;
				} else if(t >= 1 && margin < 0){
					for(var i = 0; i < t; i++){
						target.up();
					}
					this.last = pos;
				}

			}
		}
	},
	onTouchEnded: function(touch, event){
		if(this.click){
			this.click = false;
			this.last = null;
		}
	}
});
