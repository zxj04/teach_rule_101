pub.S9Button = cc.Node.extend({
	p:null,
	callback:null,
	back:null,
	enable:true,
	ctor:function (parent, str, callback, back, fileName, hoverName) {
		this._super();
		this.p = parent;
		this.callback = callback;
		if(back == null){
			this.back = this.p;
		} else {
			this.back = back;
		}
		// 添加到界面
		this.p.addChild(this);
		this.init(str, fileName, hoverName, callback, back);
		this.addListener();
		return this;
	},
	init:function(str, fileName, hoverName, callback, back){
		this.fileName = fileName;
		this.hoverName = hoverName;
		var label = new cc.LabelTTF(str,gg.fontName,32);
		this.addChild(label, 2);
		var rect = label.getBoundingBoxToWorld();
		
		var bg = this.createSprite(res_public.bt1, rect);
		this.bg = bg;
		var bg2 = this.createSprite(res_public.bt2, rect);
		bg2.setVisible(false);
		this.bg2 = bg2;
	},
	createSprite:function(fileName, rect){
		var m = 10, w = 122, h = 39;
		var sx = rect.width / (w - 2 * m) > 1 ? rect.width / (w - 2 * m) : 1;
		var sy = rect.height / (h - 2 * m) > 1 ? rect.height / (h - 2 * m) : 1;
		var arr = [cc.rect(0, 0, m, m), cc.rect(m, 0, w - 2 * m, m), cc.rect(w - m, 0, m, m),
		           cc.rect(0, m, m, h - 2 * m), cc.rect(m, m, w - 2 * m, h - 2 * m), cc.rect(w - m, m, m, h - 2 * m),
		           cc.rect(0, h - m, m, m), cc.rect(m, h - m, w - 2 * m, m), cc.rect(w - m, h - m, m, m)];
		var bg = new cc.Node();
		this.addChild(bg);
		var s5 = new cc.Sprite(fileName, arr[4]);
		s5.setScale(sx, sy);
		bg.addChild(s5);
		
		// 第一行
		var s2 = new cc.Sprite(fileName, arr[1]);
		s2.setScaleX(sx);
		bg.addChild(s2);
		$.up(s5, s2);
		var s1 = new cc.Sprite(fileName, arr[0]);
		bg.addChild(s1);
		$.left(s2, s1);
		var s3 = new cc.Sprite(fileName, arr[2]);
		$.right(s2, s3);
		// 第二行
		bg.addChild(s3);
		var s4 = new cc.Sprite(fileName, arr[3]);
		s4.setScaleY(sy);
		bg.addChild(s4);
		$.left(s5, s4);
		var s6 = new cc.Sprite(fileName, arr[5]);
		s6.setScaleY(sy);
		bg.addChild(s6);
		$.right(s5, s6);
		// 第三行
		var s8 = new cc.Sprite(fileName, arr[7]);
		s8.setScaleX(sx);
		bg.addChild(s8);
		$.down(s5, s8);
		var s7 = new cc.Sprite(fileName, arr[6]);
		bg.addChild(s7);
		$.left(s8, s7);
		var s9 = new cc.Sprite(fileName, arr[8]);
		bg.addChild(s9);
		$.right(s8, s9);
		return bg;
	},
	setPos:function(x, y){
		this.setPosition(x, y);
		return this;
	},
	preClick:function(){
		this.bg.setVisible(false);
		this.bg2.setVisible(true);
	},
	preCall:function(){
		this.bg.setVisible(true);
		this.bg2.setVisible(false);
	},
	setEnable:function(enable){
		this.enable = enable;
	},
	isEnable:function(){
		return this.enable;
	},
	addListener:function(){
		cc.eventManager.addListener(cc.EventListener.create({
			event: cc.EventListener.TOUCH_ONE_BY_ONE,
			swallowTouches: true,
			state: ANGEL_LISTENER_WAITING,
			onTouchBegan: this.onTouchBegan,
			onTouchEnded: this.onTouchEnded,
			onTouchCancelled: this.onTouchEnded
		}), this);
	},
	onTouchBegan: function(touch, event){
		if(this.state != ANGEL_LISTENER_WAITING){
			return false;
		}
		var target = event.getCurrentTarget();
		if(!target.enable){
			return false;
		}
		var pos = touch.getLocation();
		if(cc.rectContainsPoint(target.getBoundingBoxToWorld(),pos)){
			this.state = ANGEL_LISTENER_TRACKING_TOUCH;
			target.preClick();
			return true;
		}
		return false;
	},
	onTouchEnded: function(touch, event){
		if(this.state != ANGEL_LISTENER_TRACKING_TOUCH){
			return;
		}
		var target = event.getCurrentTarget();
		var pos = touch.getLocation();
		if(target.callback != null
				&& cc.rectContainsPoint(target.getBoundingBoxToWorld(),pos)){
			target.preCall();
			// 有回调函数，则调用回调函数
			target.callback.call(target.back,target);
		}
		this.state = ANGEL_LISTENER_WAITING;
	}
})