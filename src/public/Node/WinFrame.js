WinFrame = cc.Layer.extend({
	cur : 0,
	tip : [],
	lineMarign:10,
	posX: 0,
	posY: 0,
	open_flag: false,
	cell:null,
	ctor:function (parent,number,str,fileName) {
		this._super();
		parent.addChild(this,10000);
		this.setPosition(0, gg.height);
		if(number == 1){
			//实验目的 原理 提示的提示框
			this.init(str,fileName);
			var tipbg = new cc.LayerColor(cc.color(0, 0, 0, 200),1280,768);
			parent.addChild(tipbg,10, TAG_TIPSTARTBG);
			tipbg.setOpacity(0);
		}else if(number == 2){
			//按确定的提示框（选错）
			this.init2(str);
		}else if(number == 3){
			//进入实验的提示（有开始按钮）
			this.init3(str);
		}else if(number == 4)
		{
			this.init4(str);
		}


	},
	init:function (str,fileName){
		var bg = new cc.Sprite("#button/tip_frame_bg.png");
		bg.setPosition(cc.p(gg.width * 0.5 ,gg.height - 55 - bg.height * 0.5));
		this.addChild(bg,2);

		var frame = new cc.Sprite(fileName);
		frame.setPosition(cc.p(gg.width * 0.5 ,gg.height - 40 - frame.height * 0.5));
		this.addChild(frame,3);

		var close = new ButtonScale(this,"#button/tip_frame_close.png",this.callback);
		close.setPosition(cc.p(gg.width - 108 - close.width*0.5 , gg.height - 60 - close.height*0.5));
		close.setLocalZOrder(3);
		close.setTag(TAG_TIP_FRAME_CLOSE);

		var word = new cc.LabelTTF(str,gg.fontName2,gg.fontSize11);
		this.addChild(word,3);
		word.setAnchorPoint(0, 1);
		word.setPosition(cc.p(306,gg.height - 244 ));
		word.setColor(cc.color(111,65,9));
	},
	init2:function(str){
		var tipbg = new cc.LayerColor(cc.color(0, 0, 0, 200),1280,768);
		ll.tip.addChild(tipbg,999, TAG_TIPSTARTBG);
		tipbg.setOpacity(0);

		var bg = new cc.Sprite("#button/lib_tip.png");
		bg.setPosition(cc.p(gg.width * 0.5 ,gg.height - 55 - bg.height * 0.5 +30));
		this.addChild(bg,2);		

		var close = new ButtonScale(this,"#button/lib_tip_close.png",this.callback);
		close.setPosition(cc.p(811,692 +30));
		close.setLocalZOrder(3);
		close.setTag(TAG_LIB_TIP_CLOSE);

		var word = new cc.LabelTTF(str,"Arial",36);
		bg.addChild(word,3);
		word.setAnchorPoint(0.5,0.5);
		word.setPosition(cc.p(bg.width*0.5,bg.height*0.5));
		word.setColor(cc.color(111,65,9));
	},
	init3:function(str){
		var tipbg = new cc.LayerColor(cc.color(0, 0, 0, 200),1280,768);
		ll.tip.addChild(tipbg,999, TAG_TIPSTARTBG);
		tipbg.setOpacity(0);

		var bg = new cc.Sprite("#button/lib_tip.png");
		bg.setPosition(cc.p(gg.width * 0.5 ,gg.height - 55 - bg.height * 0.5 +30));
		this.addChild(bg,2);		

		var start = new ButtonScale(this,"#button/sure.png",this.callback);
		start.setPosition(cc.p(gg.width*0.5,568));
		start.setLocalZOrder(3);
		start.setTag(TAG_RUN_TIPSTART);

		var word = new cc.LabelTTF(str,"Arial",36);
		bg.addChild(word,3);
		word.setAnchorPoint(0.5,0.5);
		word.setPosition(cc.p(bg.width*0.5,bg.height*0.5 + 20));
		word.setColor(cc.color(111,65,9));
	},
	init4:function(str){
		var tipbg = new cc.LayerColor(cc.color(0, 0, 0, 200),1280,768);
		ll.tip.addChild(tipbg,999, TAG_TIPSTARTBG);
		tipbg.setOpacity(0);

		var bg = new cc.Sprite("#button/lib_tip.png");
		bg.setPosition(cc.p(gg.width * 0.5 ,gg.height - 55 - bg.height * 0.5 +30));
		this.addChild(bg,2);		

		var close = new ButtonScale(this,"#button/lib_tip_close.png",this.callback);
		close.setPosition(cc.p(811,692 +30));
		close.setLocalZOrder(3);
		close.setTag(TAG_LIB_TIP_CLOSE);

		var word = new cc.LabelTTF(str,gg.fontName2,gg.fontSize11);
		bg.addChild(word,3);
		word.setAnchorPoint(0.5,0.5);
		word.setPosition(cc.p(bg.width*0.5,bg.height*0.5));
		word.setColor(cc.color(111,65,9));
	},
	open:function (){
		gg.synch_l = true;
		this.stopAllActions();
		var tipbg = this.getParent().getChildByTag(TAG_TIPSTARTBG);
		tipbg.runAction(cc.fadeTo(0.4, 200));	
		this.buttonCanClick(this.isOpen());
		var move = cc.moveTo(0.3,cc.p(0, 0));
		var move2 = cc.moveTo(0.1,cc.p(0, 50));
		var move3 = cc.moveTo(0.1,cc.p(0, 0));
		var sequence = cc.sequence(move, move2, move3);
		this.runAction(sequence);
		AngelListener.setEnable(false);
		this.open_flag = true;
	},
	close:function (){
		gg.synch_l = false;
		this.stopAllActions();
		var tipbg = this.getParent().getChildByTag(TAG_TIPSTARTBG);
		tipbg.runAction(cc.fadeTo(0.4, 0));
		this.buttonCanClick(this.isOpen());
		var move = cc.moveTo(0.3,cc.p(0, gg.height));
		var sequence = cc.sequence(move);
		this.runAction(sequence);
		AngelListener.setEnable(true);
		this.open_flag = false;
	},
	open2:function (){
		gg.synch_l = true;
		this.stopAllActions();
		var tipbg = this.getParent().getChildByTag(TAG_TIPSTARTBG);
		tipbg.runAction(cc.fadeTo(0.4, 200));		
		this.buttonSetEnable2(false);
		var move = cc.moveTo(0.3,cc.p(0, 0-gg.height*0.25));
		var move2 = cc.moveTo(0.1,cc.p(0, 50-gg.height*0.25));
		var move3 = cc.moveTo(0.1,cc.p(0, 0-gg.height*0.25));
		var sequence = cc.sequence(move, move2, move3);
		this.runAction(sequence);
		AngelListener.setEnable(false);
		this.open_flag = true;
	},
	close2:function (){
		gg.synch_l = false;
		this.stopAllActions();
		var tipbg = this.getParent().getChildByTag(TAG_TIPSTARTBG);
		tipbg.runAction(cc.fadeTo(0.4, 0));
		this.buttonSetEnable2(true);
		ll.tip.tipItem.setEnable(true);
		ll.tip.backItem.setEnable(true);
		var move = cc.moveTo(0.3,cc.p(0, gg.height));
		var sequence = cc.sequence(move,cc.callFunc(function() {
			AngelListener.setEnable(true);
			this.open_flag = false;
			var bg = ll.tip.getChildByTag(TAG_TIPSTARTBG);
			bg.removeFromParent();
			this.removeFromParent();
		}, this));
		this.runAction(sequence);	
	},
	//lib中取消 确定 药品库按钮 tip按钮
	buttonSetEnable2:function(next){
		if(ll.run.lib !=null){var cancel = ll.run.getChildByTag(TAG_LIB).getChildByTag(TAG_CANCEL);
		var sure = ll.run.getChildByTag(TAG_LIB).getChildByTag(TAG_SURE);
		var lib = ll.tool.getChildByTag(TAG_BUTTON_LIB);
		cancel.setEnable(next);
		sure.setEnable(next);
		lib.setEnable(next);
		}

		var tip = ll.tip.getChildByTag(TAG_TIP);	
		var back = ll.tip.getChildByTag(TAG_CLOSE);		
		tip.setEnable(next);
		back.setEnable(next);
	},
	buttonCanClick:function(Enable){
		for(var i in gg.buttonEnableArr){
			if(gg.buttonEnableArr[i] != null){
				gg.buttonEnableArr[i].setEnable(Enable);
			}
		}		
	},
	isOpen:function (){
		return this.open_flag;
	},
	callback:function(p){
		switch (p.getTag()) {
		case TAG_TIP_FRAME_CLOSE:
			//实验原理 目的 弹框
			if(this.isOpen()){
				cc.log("关闭提示");
				this.close();
			}
			break;
		case TAG_LIB_TIP_CLOSE:
			//药品库选择仪器弹框
			if(this.isOpen()){
				cc.log("关闭错误提示");
				this.close2();
			}
			break;
		case TAG_RUN_TIPSTART:
			//演示类实验开始实验指导弹框
			if(this.isOpen()){
				cc.log("开始实验");
				this.close2();
			}
			break;
		default:
			break;
		}

	}
});
