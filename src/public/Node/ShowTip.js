/**
 * 文字自带边框的实体
 */
ShowTip = cc.Node.extend({
	ctor:function(name,pos,next, p){
		this._super();
		if(p){
			p.addChild(this, 100, TAG_SHOW);
		} else {
			ll.run.addChild(this, 100, TAG_SHOW);
		}
		this.setCascadeOpacityEnabled(true);
		this.init(name,pos,next);
	},
	init:function(name,pos,next){
		this.content = new cc.LabelTTF(name, gg.fontName, gg.fontSize);
		var mr = 10;
		var rect = this.content.getBoundingBoxToWorld();
		var bg = new cc.Scale9Sprite(res_public.show_tip);
		bg.width = rect.width + mr * 3;
		bg.height = rect.height + mr * 3;
		bg.setPosition(pos );
		// rect.width * 0.5, rect.height * 0.5
		this.addChild(bg);
		bg.addChild(this.content,2);
		this.content.setColor(cc.color(38,41,52));
		this.content.setPosition(cc.p(bg.width*0.5,bg.height*0.5));

		if(next == null){
			next = false;
		}
		if(next){
			this.scheduleOnce(this.kill, 1);	
		}
		if(ll 
			&&ll.run 
			&&ll.run.lib != null){
			if(ll.run.lib.isOpen()){
				this.fadeout();
			}
		}
		
	},
	kill:function(){
		var seq = cc.sequence(cc.fadeOut(0.5),cc.callFunc(function(){
			this.removeFromParent(true);	
		}, this))
		this.runAction(seq);
	},
	fadein:function(){
		this.setVisible(true);
	},
	fadeout:function(){
		this.setVisible(false);
	}
})