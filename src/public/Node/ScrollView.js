pub.ScrollView = cc.Node.extend({
	curIndex : 0,
	maxIndex : 0,
	limit : 3,
	layerArr : [],
	pointArr : [],
	origPos : null,
	offestPos : null,
	pointMargin :10,
	runing_flag : false,
	ctor:function(parent){
		this._super();
		parent.addChild(this);
		this.init();
		this.addListener();
	},
	init:function(){
		this.layerArr = [];
		this.pointArr = [];
		this.curIndex = 0;
		this.limit = 3;
		this.pointMargin = 16;
		this.runing_flag = false;
		this.node = new cc.Node();
		this.origPos = this.node.getPosition();
		this.loadSide();
		this.loadClip();
	},
	loadSide:function(){
		// 添加侧边栏
		this.side = new cc.Sprite("#tool/side_mid.png");//中间部分，可以缩放
		this.addChild(this.side);
		this.side.setPosition(gg.width - this.side.width * 0.5,gg.c_height);
		this.sideNode = new cc.Node();
		this.addChild(this.sideNode);
		this.sideNode.setPosition(gg.width - this.side.width * 0.5,gg.c_height);

		this.sideUp = new cc.Sprite("#tool/side_up.png");//上部分圆角，不做缩放
		this.addChild(this.sideUp);
		this.sideUp.setPosition(gg.width - this.sideUp.width *0.5,gg.c_height + this.side.height * 0.5 + this.sideUp.height * 0.5);

		this.sideDown = new cc.Sprite("#tool/side_down.png");//下部分圆角,不做缩放
		this.addChild(this.sideDown);
		this.sideDown.setPosition(gg.width - this.sideDown.width *0.5,gg.c_height - this.side.height * 0.5 - this.sideDown.height * 0.5);
	},
	addPoint:function(){//侧边栏添加空心圆
		var point = new cc.Sprite("#tool/vir_point.png");
		this.sideNode.addChild(point);
		this.pointArr.push(point);
	},
	refreshSide:function(){//刷新侧边栏的高度
		if(!this.pointArr){
			return;
		}
		var length = this.pointMargin * (this.pointArr.length + 1) + 16 * this.pointArr.length;//16为空心圆的高度
		var scaleY = length / this.side.height;//算出缩放值
		this.side.setScaleY(scaleY); 
	
		this.sideUp.setPositionY(gg.c_height + this.side.height * 0.5 * scaleY + this.sideUp.height * 0.5 -3);//更新上侧栏位置
		this.sideDown.setPositionY(gg.c_height - this.side.height * 0.5 * scaleY  - this.sideDown.height * 0.5 +3);//更新下侧栏位置

		for(var i = 0; i < this.pointArr.length; i++){//更新每个空心圆的位置
			var point = this.pointArr[i];
			if(i == 0){
				point.setPosition(0, this.side.height * scaleY * 0.5 - point.height * 0.5 -this.pointMargin);//第一个位置
			} else {
				var prevPoint = this.pointArr[i - 1];
				point.setPosition(0, prevPoint.y - point.height - this.pointMargin);
			}
			if(this.curIndex == i){
				point.setSpriteFrame("tool/real_point.png");//当前页的空心圆变实心
			} else {
				point.setSpriteFrame("tool/vir_point.png");//非当前页的空心圆变空心
			}
		}
	},
	loadClip:function(){//添加遮罩
		this.stencil = new cc.DrawNode()
		//模板范围，左下角坐标，右上角坐标，构成的矩形，默认可见true
//		this.stencil.drawRect(cc.p(152,50),cc.p(1148,648),cc.color(248,232,176,0),1,cc.color(248,232,176,0));
		var clipping = new cc.ClippingNode(this.stencil);
		this.addChild(clipping);
		clipping.addChild(this.node);
	},
	setOffestPos:function(pos){//设置起始位置和遮罩位置
		this.offestPos = pos;
		this.stencil.drawRect(cc.p(152,50),cc.p(1148,648 +15),cc.color(248,232,176,0),1,cc.color(248,232,176,0));
	},
	addLayer:function(layer){//添加页
		this.node.addChild(layer);
		this.layerArr.push(layer);
		this.maxIndex = this.layerArr.length;
		this.refreshPosition();//刷新每页的位置
		this.addPoint();//添加空心圆
		this.refreshSide();//对侧边栏更新
	},
	refreshPosition:function(){//刷新页的位置
		if(!this.layerArr){
			return;
		}
		for(var i = 0; i< this.layerArr.length; i++){
			var layer = this.layerArr[i];
			if(i == 0){//第一页位置
				if(!!this.offestPos){
					layer.setPosition(this.offestPos);
				} else {
					layer.setPosition((gg.width - layer.width) / 2, (gg.height - layer.height) / 2);
				}
			} else {
				var prevPos = this.layerArr[i - 1].getPosition();//获取前一页的位置
				layer.setPosition(prevPos.x, prevPos.y - gg.height);//以一个屏的高度为距离
			}
		}
	},
	up:function(){//上滑		
		if(this.curIndex >= this.maxIndex - 1){//如果是最后一页
			this.resetOrigPos();
			return;
		}
		this.runing_flag =true;
		this.curIndex ++;//当前页+1
		var move = cc.moveTo(0.3,cc.p(this.origPos.x,this.origPos.y + gg.height));
		var seq = cc.sequence(move, cc.callFunc(function(){
			this.refreshOrigPos();
			this.runing_flag =false;
		},this));
		this.node.runAction(seq);
	},
	down:function(){//下滑
		if(this.curIndex <= 0){//如果是第一页
			this.resetOrigPos();
			return;
		}

		this.runing_flag =true;
		this.curIndex --;
		var move = cc.moveTo(0.3,cc.p(this.origPos.x,this.origPos.y - gg.height));
		var seq = cc.sequence(move, cc.callFunc(function(){
			this.refreshOrigPos();
			this.runing_flag =false;
		},this));
		this.node.runAction(seq);
	},
	refreshOrigPos:function(){//刷新初始位置
		this.origPos = this.node.getPosition();
		this.refreshSide();//侧边栏更新
	},
	resetOrigPos:function(){//设置this.node起始位置
		this.node.setPosition(this.origPos);
	},
	addListener:function(){
		var listenerTouch = cc.EventListener.create({
			event: cc.EventListener.TOUCH_ONE_BY_ONE,
			swallowTouches: false,
			onTouchBegan:this.onTouchBegan,
			onTouchMoved:this.onTouchMoved,
			onTouchCancelled:this.onTouchEnded,
			onTouchEnded:this.onTouchEnded});
		// 添加触摸事件
		cc.eventManager.addListener(listenerTouch, this);

		if ('mouse' in cc.sys.capabilities){
			var listener_mouse = cc.EventListener.create({
				event: cc.EventListener.MOUSE,
				swallowTouches: false,
				onMouseScroll:this.onMouseScroll
			});
			cc.eventManager.addListener(listener_mouse, this);
		}
	},
	onTouchBegan: function(touch, event){

		var target = event.getCurrentTarget();
		if(target.runing_flag){
			return;
		}
		var pos = cc.p(touch.getLocationX(),touch.getLocationY());
		if(cc.rectContainsPoint(
				target.node.getBoundingBoxToWorld(),pos)){
			this.beginPos = pos;//获取点击开始坐标
			return true;
		}
		return false;
	},
	onTouchMoved: function(touch, event){
		if(this.beginPos){//如果点击了
			var target = event.getCurrentTarget();
			var pos = cc.p(touch.getLocationX(),touch.getLocationY());
			if((this.beginPos.y - pos.y) > gg.height / target.limit){//gg.height / target.limit为滑动的一个临界点
				target.down();//下滑
				this.beginPos = null;//初始化点击开始坐标
				return;
			} else if((this.beginPos.y - pos.y) < -gg.height / target.limit){
				target.up();//上滑
				this.beginPos = null;//初始化点击开始坐标
				return;
			} else{//跟随鼠标移动而滑动
				var margin = target.origPos.y - (this.beginPos.y - pos.y);
				target.node.setPositionY(margin);
			}
		}
	},
	onTouchEnded: function(touch, event){
		if(this.beginPos){
			var target = event.getCurrentTarget();
			var pos = cc.p(touch.getLocationX(),touch.getLocationY());
			if((this.beginPos.y - pos.y) > gg.height / target.limit){
				target.down();
			} else if((this.beginPos.y - pos.y) < -gg.height / target.limit){
				target.up();
			} else {
				target.resetOrigPos();//重新设置到初始位置
			}
			this.beginPos = null;
		}
	},
	onMouseScroll: function(event){
		var target = event.getCurrentTarget();
		if(target.runing_flag){
			return;
		}
		if(event.getScrollY() > 0){
			if(cc.sys.isNative
					&& cc.sys.os == "Windows"){
				target.up();
			} else {
				target.down();	
			}
		} else if(event.getScrollY() < 0){
			if(cc.sys.isNative
					&& cc.sys.os == "Windows"){
				target.down();
			} else {
				target.up();
			}		
		}
	}
});