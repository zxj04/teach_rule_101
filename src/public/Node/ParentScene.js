PScene = cc.Scene.extend({
	onEnter:function () {
		this._super();
		this.preLoad();
	},
	preLoad:function(){
		if (cc.sys.platform == cc.sys.DESKTOP_BROWSER) {
			// html5 浏览器
			cc.log("html5");
		} else{
			// cc.sys.os native 不需要预加载资源
			gg.run_load = true;
			gg.finish_load = true;
			gg.game_load = true;
			gg.about_load = true;
			cc.log("jsb");
		}
	}
});