var ToolLayer= cc.Layer.extend({
	scene:null,
	ctor:function (parent) {
		this._super();
		this.scene = parent;
		this.scene.addChild(this, 20);
		this.init();
	},
	init: function (){
		this.lib = new  ButtonScale(this,"#button/lib.png",this.callback);
		this.lib.setTag(TAG_BUTTON_LIB);
		this.lib.setPosition(cc.p(30 + this.lib.width*0.5, 30 + this.lib.height * 0.5));
		
		//如果存在物品库，怎调整tip按钮坐标
		ll.tip.tipItem.setPosition(40 + ll.tip.tipItem.width*0.5,164 + ll.tip.tipItem.height * 0.5 );
	},
	callback: function(pSender) {
		pSender.setEnable(false);
		switch (pSender.getTag()){
		case TAG_BUTTON_LIB:
			if(ll.run.lib.isOpen()){				
				ll.run.lib.close();
				ll.tip.arr.fadein();
				var show = ll.run.getChildByTag(TAG_SHOW);
				if(show !== null){
					ll.run.getChildByTag(TAG_SHOW).fadein();
				}
			} else {
				ll.run.lib.open();					
				ll.tip.arr.fadeout();
				var show = ll.run.getChildByTag(TAG_SHOW);
				if(show !== null){
					ll.run.getChildByTag(TAG_SHOW).fadeout();
				}	
			}
			pSender.setEnable(true);
		}
	}
})