var BackLayer= cc.Layer.extend({
	open_flag: false,
	ctor:function (parent) {
		this._super();
		parent.addChild(this, 10002);
		this.setPosition(0, gg.height);
		this.initFrames();
		this.init();	
		var tipRunBg = new cc.LayerColor(cc.color(0, 0, 0, 230),1280,768);
		parent.addChild(tipRunBg,100, TAG_TIPRUNBG1);
		tipRunBg.setOpacity(0);
		//this.loadNextButton();
	},
	initFrames : function(){
		cc.spriteFrameCache.addSpriteFrames(res_public_finish.finish_p);
	},
	init: function (){
		
		//完成实验时的背景
		this.bg = new cc.Sprite("#finish/bg.png");
		this.bg.setPosition(cc.p(374 + this.bg.width * 0.5 ,gg.height - 158 - this.bg.height * 0.5));
		this.addChild(this.bg,2);

		this.expover = new cc.Sprite("#finish/expover.png");//引导模式不需要
		this.expover.setPosition(cc.p(gg.width * 0.5 ,gg.height - 176 - this.expover.height * 0.5));
		this.addChild(this.expover,5);
		
		//完成时的分数(实战模式)
		this.score =new cc.LabelTTF(gg.score,"微软雅黑",55);
		this.score.setPosition(gg.width * 0.5 -1, this.bg.y);
		this.score.setColor(cc.color(122,73,17));
		this.score.setVisible(false);
		this.addChild(this.score, 5);

		this.scorelabel =new cc.LabelTTF("分","微软雅黑",28);
		this.scorelabel.setPosition(gg.width * 0.5+ this.score.width * 0.5 + this.scorelabel.width * 0.5 + 30, this.score.y - 10);
		this.scorelabel.setColor(cc.color(122,73,17));
		this.scorelabel.setVisible(false);
		this.addChild(this.scorelabel, 5);
		
		//引导模式
		this.overyd = new cc.LabelTTF("实 验 完 成","微软雅黑",45);
		this.overyd.setPosition(gg.width * 0.5 -1, this.bg.y);
		this.overyd.setColor(cc.color(122,73,17));
		this.overyd.setVisible(false);
		this.addChild(this.overyd, 5);

		
		//未完成实验时的背景		
		this.unbg = new cc.Sprite("#finish/unover.png");
		this.unbg.setPosition(cc.p(409 + this.unbg.width * 0.5 ,gg.height - 190 - this.unbg.height * 0.5));
		this.addChild(this.unbg,2);

		this.unover = new cc.LabelTTF("未 完 成！","微软雅黑",60); 
		this.unover.setPosition(gg.width - 448 - this.unover.width * 0.5,gg.height - this.unover.height *0.5 -220);
		this.addChild(this.unover ,2);
		
		if(gg.expdemo == TAG_TYPE_YANSHI || gg.expdemo == TAG_TYPE_CESHI){//判断是什么模式
			this.unbg.setOpacity(0);
			this.unover.setString(gg.Title);
			this.unover.setPosition(gg.c_width,gg.height - this.unover.height *0.5 -208);

		}
		
		//按钮		
		this.continueexp = new ButtonScale(this,"#finish/continueexp.png",this.callback);
		this.continueexp.setPosition(296 + this.continueexp.width * 0.5 , 202 + this.continueexp.height * 0.5);
		this.continueexp.setTag(TAG_CONTINUEEXP);
		
		this.restart = new ButtonScale(this,"#finish/restart.png",this.callback);
		this.restart.setPosition(532 + this.continueexp.width * 0.5 , 202 + this.continueexp.height * 0.5);
		this.restart.setTag(TAG_RESTART);

		this.platform = new ButtonScale(this,"#finish/back_platform.png",this.callback);
		this.platform.setPosition(770 + this.continueexp.width * 0.5 , 202 + this.continueexp.height * 0.5);
		this.platform.setTag(TAG_PLATFORM);										
	},
	loadNextButton:function(){	
		for(var i =1;i < expInfo.length; i++){
			var exp =expInfo[i];
			if(exp.expTag == gg.runExpTag){
				for(var j =0; j< gg.menuArr.length ;j++){
					var menu = gg.menuArr[j];
					if(menu.tag == gg.runMenuTag){
						if(j < gg.menuArr.length -1){//不是最后一个menu
							var label = new Label(this,gg.menuArr[j+1].menu+">>",this.nextMenu);
							label.setLocalZOrder(5);
							label.setTag(gg.menuArr[j+1].tag);
							label.setAnchorPoint(1, 0);
							label.setColor(cc.color(19, 98, 27, 250));
							label.setPosition(gg.width - 40,25);

						}else{//如果是最后一个menu，则跳到下一节
							if(!!expInfo[i+1]){
								var label = new Label(this,expInfo[i+1].expName+">>",this.nextChapter);
								label.setLocalZOrder(5);
								label.setTag(expInfo[i+1].expTag);  
								label.setAnchorPoint(1, 0);
								label.setColor(cc.color(19, 98, 27, 250));
								label.setPosition(gg.width - 40,25);
							}   						
						}

						break;
					}

				}
				break;
			}
		}
	},
	freshen :function(){
		if(gg.flow.over_flag){//实验结束
			this.bg.setVisible(true);
			this.expover.setVisible(true);
			
			this.unbg.setVisible(false);
			this.unover.setVisible(false);
			
			if(gg.teach_type == TAG_REAL){//实战模式
				this.score.setVisible(true);
				this.score.setString(gg.score);
				this.scorelabel.setVisible(true);
				this.scorelabel.setPosition(gg.width * 0.5+ this.score.width * 0.5 + this.scorelabel.width * 0.5 + 30, this.score.y - 10);
				
			}else {//引导模式
				this.overyd.setVisible(true);
				this.expover.setVisible(false);
			}
			
			ll.tip.pause();//结束后时间暂停
			
		}else{//实验中
			this.bg.setVisible(false);
			this.expover.setVisible(false);

			this.unbg.setVisible(true);
			this.unover.setVisible(true);	
			
		}
	},
	open:function (){	
		this.freshen();					
		this.SetEnable(!this.isOpen());
		if(ll.tip.tip_frame.isOpen()){//结束时，如果提示打开先对其关闭
			ll.tip.tip_frame.close();
			if(ll.run.getChildByTag(TAG_LIB) != null){
				ll.run.getChildByTag(TAG_LIB).getChildByTag(TAG_CANCEL).setEnable(false);//物品库取消按钮
				ll.run.getChildByTag(TAG_LIB).getChildByTag(TAG_SURE).setEnable(false);//物品库确定按钮
				ll.tool.lib.setEnable(false);//实验中物品库按钮				
			}
			ll.tip.tipItem.setEnable(false);//实验中提示按钮
			ll.tip.backItem.setEnable(false);//实验中返回按钮	
		}		
		this.stopAllActions();
		var tipRunBg = this.getParent().getChildByTag(TAG_TIPRUNBG1);
		tipRunBg.runAction(cc.fadeTo(0.4, 230));		
		var move = cc.moveTo(0.3,cc.p(0, 0));
		var move2 = cc.moveTo(0.1,cc.p(0, 50));
		var move3 = cc.moveTo(0.1,cc.p(0, 0));
		var sequence = cc.sequence(move, move2, move3);
		this.runAction(sequence);
		AngelListener.setEnable(false);
		this.open_flag = true;
		
	},
	close:function (){	
		this.stopAllActions();		
		var tipRunBg = this.getParent().getChildByTag(TAG_TIPRUNBG1);
		var sequence = cc.sequence(cc.fadeTo(0.4, 0),cc.callFunc(function(){
			this.SetEnable(this.isOpen());
		},this));
		tipRunBg.runAction(sequence);
		
		var move = cc.moveTo(0.3,cc.p(0, gg.height));
		this.runAction(move);
		AngelListener.setEnable(true);
		this.open_flag = false;
	},
	isOpen:function (){
		return this.open_flag;
	},
    SetEnable:function(open_flag){	
		if(open_flag == true){	
			if(ll.run.getChildByTag(TAG_LIB) != null){
				ll.run.getChildByTag(TAG_LIB).getChildByTag(TAG_CANCEL).setEnable(false);//物品库取消按钮
				ll.run.getChildByTag(TAG_LIB).getChildByTag(TAG_SURE).setEnable(false);//物品库确定按钮
				ll.tool.lib.setEnable(false);//实验中物品库按钮				
			}			
			ll.tip.tipItem.setEnable(false);//实验中提示按钮
			ll.tip.backItem.setEnable(false);//实验中返回按钮		
			this.PausedTargets = cc.director.getActionManager().pauseAllRunningActions();//暂停所有执行中的动作
			cc.audioEngine.pauseMusic();//暂停音效	
			ll.tip.pause();//暂停时间
		}else{
			if(ll.run.getChildByTag(TAG_LIB) != null){
				ll.run.getChildByTag(TAG_LIB).getChildByTag(TAG_CANCEL).setEnable(true);
				ll.run.getChildByTag(TAG_LIB).getChildByTag(TAG_SURE).setEnable(true);
				ll.tool.lib.setEnable(true);
			}					
			ll.tip.tipItem.setEnable(true);
			ll.tip.backItem.setEnable(true);			
			cc.director.getActionManager().resumeTargets(this.PausedTargets);//继续被暂停的动作
			cc.audioEngine.resumeMusic();//恢复音效
			ll.tip.resume();//恢复时间
			if(gg.flow.over_flag){
				ll.tip.unschedule(ll.tip.updateTime);
			}
		}
		
	},
	callback: function(pSender) {
		switch (pSender.getTag()){		
		case TAG_CONTINUEEXP:
			ll.tip.pausetime = ll.tip.pausetime + (new Date() - ll.tip.pretime);//总暂停时间 =前面暂停时间 +（当前时间 - 暂停前时间）
			this.close();
			break;
		case TAG_RESTART:
			cc.log("重新开始");
			cc.log("gg.expId:"+gg.expId);
			gg.runNext(gg.runMenuTag);
			break;
		case TAG_PLATFORM:
			gg.synch_l = false;
		
			_.stop();
		
			if(ll.run.lib != null){
				ll.run.lib.removeFromParent(true);
			}

			pub.ch.gotoStart(gg.runExpTag, g_resources_public_start, res_public_start.start_p );//返回开始界面	
//			_.stop();
//			cc.log("返回大厅");
//			if(ll.run.lib != null){
//				ll.run.lib.removeFromParent(true);
//			}
//			if (cc.sys.platform == cc.sys.DESKTOP_BROWSER) {
//				//history.go(-1);
//				pub.ch.howToGo(OP_TYPE_BACK);
//			} else if(cc.sys.os == "IOS"){
//			} else if(cc.sys.os == "Android"
//				|| cc.sys.os == "Windows"){
//				cc.director.end();		
//			}
			break;
		}
	},
	nextMenu:function(p){
		gg.runNext(p.getTag());
	},
	nextChapter:function(p){
		ch.run(p.getTag());
	}
})