TAG_YINDAO = 1001;
TAG_SHIZHAN = 1002;
TAG_QUWEI = 1003;
TAG_QITA = 1004;
TAG_ABOUT = 1401;
TAG_TEST = 1402;
TAG_LIANX = 1403;
TAG_YUANL = 1404;
TAG_MUDE = 1405;
TAG_DTL = 1406;
TAG_PLATFORM = 1407;
TAG_WIN=1408;

var StartMainLayer = cc.Layer.extend({
	lead:null,
	real:null,
	title:null,
	YuanLi:null,
	MuDi:null,
	chapterImg:null,
	ctor:function () {
		this._super();
		// 加载回退按钮
		this.loadButton();
		//获取配置信息
		this.loadInfo();
		//加载章节
		this.loadChapter();
		// 加载标题
		this.loadTitle();
		//加载左侧图片
		this.loadimg();
		//右侧菜单
		this.loadMenu();
		//添加滚动
		this.loadSlide();
		return true;
	},
	loadInfo:function(){//获取实验配置信息
		gg.menuArr = [];
		for(var i in expInfo){
			var exp = expInfo[i];
			if(gg.expId == exp.expId){
				this.chapter = exp.chapter;
//				this.title = exp.expName;
				this.startImg = exp.expimage;				
				for(j=0;j<exp.menuItem.length;j++){
					gg.menuArr.push(exp.menuItem[j]);
				}
				break;
			}		  
		}		
	},
	loadChapter: function(){//章节		
		var chapter = new cc.LabelTTF(this.chapter,gg.fontName,25);
		chapter.setPosition(235 + 40,gg.height - chapter.height/2 -140);
		this.addChild(chapter,5);
	},

	loadTitle: function(){//标题
		var title = new cc.LabelTTF(gg.Title,gg.fontName,45);
		title.setPosition(235 + 40,gg.height - title.height/2 -180);
		this.addChild(title,5);
	},
	loadimg:function(){//左侧图片
		var startImg = new cc.Sprite(this.startImg);
		startImg.setPosition(235  + 40,278);
		startImg.setScale(0.66);
		this.addChild(startImg,5);

	},
	loadButton:function(){//返回按钮
		this.backButton = new ButtonScale(this,"#button/st_back.png", this.callback);
		this.backButton.setLocalZOrder(5);
		this.backButton.setPosition(cc.p(40 + this.backButton.width * 0.5, gg.height - 40 - this.backButton.height * 0.5));
		this.backButton.setTag(TAG_BACK);
	},
	loadMenu :function(){//右侧菜单
		this.node = new cc.Node();
		this.node.setPosition(0,0);
		var prev = null;
		this.cur = 0;
		var marign = 108 +8;

		for(i=0;i<gg.menuArr.length;i++){

			var label = new LabelMenu(this.node,gg.menuArr[i].menu,"#menu_bg.png",this.callback,this);
			label.setTag(gg.menuArr[i].tag);				
			if(!prev){
				label.setPosition(440 + 80 - 2,616);
			} else {
				this.posDown(prev, label, marign);
			}
			prev = label;
			if(!this.cell){ 
				this.cell = label.height + marign;
			}				
		}
		//添加遮罩
		var stencil = new cc.DrawNode();
		stencil.drawRect(cc.p(440 + 80,36),cc.p(1280,732),cc.color(0,0,0,0),0,cc.color(0,0,0,0));
		var clip = new cc.ClippingNode(stencil);
		this.addChild(clip,10);
		clip.addChild(this.node, 1);
	},
	posDown:function (standard, target, margin){
		if(!margin){
			margin = 0;
		}
		var sap = standard.getAnchorPoint();
		var ap = target.getAnchorPoint();
		// 标准物的y - 标准物的高度 * 缩放 * 锚点y - 本身的高度 * 缩放 * (1-锚点y) - 所需间隔
		var y = standard.y
		- standard.height * standard.getScaleY() * sap.y
		- target.height * (1-ap.y) * target.getScaleY() - margin;
		target.setPosition(standard.x, y);
	},
	callback:function(pSend){
		switch(pSend.getTag()){
		case TAG_BACK:
			cc.log("结束");
			pub.ch.howToGo(OP_TYPE_BACK);			
			break;
		case TAG_MENU1:
		case TAG_MENU2:
		case TAG_MENU3:
		case TAG_MENU4:
		case TAG_MENU5:
		case TAG_MENU6:
		case TAG_MENU7:
		case TAG_MENU8:
		case TAG_MENU9:
		case TAG_MENU10:
			gg.runNext(pSend.getTag());
			break;
		}
	},
	down:/**
	 * 向下滑动
	 */
		function(){
		if(this.cur >= gg.menuArr.length - 6){
			return;
		}
		this.cur++;
		this.node.runAction(cc.moveBy(0.2,cc.p(0, this.cell)))
	},
	up:/**
	 * 向上滑动
	 */
		function(){
		if(this.cur <= 0){
			return;
		}
		this.cur--;
		this.node.runAction(cc.moveBy(0.2,cc.p(0, -this.cell)))
	},
	loadSlide:/**
	 * 滑动
	 */
		function(){
		var listener_touch = cc.EventListener.create({
			event: cc.EventListener.TOUCH_ONE_BY_ONE,
			swallowTouches: false,
			hover: false,
			onTouchBegan:UpperLowerSliding.onTouchBegan,
			onTouchMoved:UpperLowerSliding.onTouchMoved,
			onTouchEnded:UpperLowerSliding.onTouchEnded});
		cc.eventManager.addListener(listener_touch, this);
		if ('mouse' in cc.sys.capabilities){
			var listener_mouse = cc.EventListener.create({
				event: cc.EventListener.MOUSE,
				swallowTouches: false,
				onMouseScroll:UpperLowerSliding.onMouseScroll
			});
			cc.eventManager.addListener(listener_mouse, this);
		}
	}
});
