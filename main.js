// 管理相关的游戏参数
var gg = gg || {};
gg.init = function(){
	// 初始化固定参数
	this.width = cc.winSize.width;
	this.height = cc.winSize.height;
	this.c_width = this.width * 0.5;
	this.c_height = this.height * 0.5;
	this.c_p = cc.p(this.c_width, this.c_height);
	this.start_load = true;
	this.run_load = false;
	this.finish_load = false;
	this.game_load = false;
	this.test_load = false;
	this.about_load = false;
	this.isdemo = false;

	gg.lastStep = 1;
	gg.userId = 1;
	gg.expVer = 2;
	gg.version = ''
};
/**
 * 入口
 */
cc.game.onStart = function(){
	if(!cc.sys.isNative && document.getElementById("cocosLoading"))
		document.body.removeChild(document.getElementById("cocosLoading"));

	cc.view.enableRetina(false);
	cc.view.adjustViewPort(true);
	cc.view.setDesignResolutionSize(1280, 768, cc.ResolutionPolicy.SHOW_ALL);
	cc.view.resizeWithBrowserSize(true);

	gg.init();
	_ = new PlayMusic();
	// http://127.0.0.1:8000/?userId=16&expId=4/
	
	if (cc.sys.platform == cc.sys.DESKTOP_BROWSER) {
		// 浏览器
		uu.userId = $.getQueryString("userId");
		uu.expId = $.getQueryString("expId");
		gg.homeworkId = $.getQueryString("homeworkId");
	} else if(cc.sys.os == cc.sys.OS_ANDROID
			|| cc.sys.os == cc.sys.OS_IOS ){
		// Android IOS
		uu.userId = cc.sys.localStorage.getItem("userId");
		uu.expId = cc.sys.localStorage.getItem("expId");
		gg.homeworkId = cc.sys.localStorage.getItem("homeworkId");
		gg.fromApp = cc.sys.localStorage.getItem("fromApp");
		cc.sys.localStorage.removeItem("userId");
		cc.sys.localStorage.removeItem("expId");
		cc.sys.localStorage.removeItem("homeworkId");
		cc.sys.localStorage.removeItem("fromApp");
	} else if(cc.sys.os == cc.sys.OS_WINDOWS){
		// cc.log("js get from c++: " + osInfo());
		var lpCmdLine = cc.sys.localStorage.getItem("lpCmdLine");// expId=92
		cc.sys.localStorage.removeItem("lpCmdLine");
		var reg = new RegExp("(^|&)" + "expId" + "=([^&|/]*)(&|$|/)", "i");
		if(lpCmdLine){
			var r = lpCmdLine.match(reg);
			if(r){
				uu.expId = decodeURIComponent(r[2]);	
			}
		}
		uu.userId = -1;
	}

	if(!!uu.userId && !!uu.expId){
		uu.loginType = LOGIN_TYPE_PLATFORM;
	} else {
		uu.loginType = LOGIN_TYPE_APP;
	}
	
	var preRes = g_resources_public.concat(g_resources_public_finish);
	cc.LoaderScene.preload(preRes, function() {
		pub.ch.addPublicRes();
		pub.ch.howToGo(OP_TYPE_SELECT);
	}, this);
	// 热更新
// var scene = new HotUpdateScene();
// scene.run();
};
cc.game.run();
var pub = pub || {};// public命名空间
var exp01 = exp01||{};// 每一节一个命名空间
var exp02 = exp02||{};
var exp03 = exp03||{};
var exp04 = exp04||{};
var exp05 = exp05||{};
var exp06 = exp06||{};
var exp07 = exp07||{};
var exp08 = exp08||{};
var exp09 = exp09||{};
var exp10 = exp10||{};
var exp11 = exp11||{};
var exp12 = exp12||{};
var exp13 = exp13||{};
var exp14 = exp14||{};
var exp15 = exp15||{};
var exp16 = exp16||{};
var exp17 = exp17||{};
var exp18 = exp18||{};
var exp19 = exp19||{};
var exp20 = exp20||{};